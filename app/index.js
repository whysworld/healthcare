var app = angular.module('app', [
  'ui.router',
  'ngTouch',
  'ngAnimate',
  'ngMaterial',
  'ui.bootstrap',
  'ui.mask',
  'angular-md5',
  'angular-spinkit',
  'LocalStorageModule',
  'ngFileSaver',
  'signature'
])
// app.config(function($locationProvider) {
//   $locationProvider.html5Mode({
//     enabled: true
//   });
// });

app.config(function ($urlRouterProvider) {
  $urlRouterProvider.when('', '/')
})

app.config(function($mdDateLocaleProvider) {
  $mdDateLocaleProvider.formatDate = function(date) {
    return moment(date).format('YYYY-MM-DD');
  };
})

app.filter('tel', function () {
  return function (tel) {
    if (!tel) {
      return ''
    }

    var value = tel.toString().trim().replace(/^\+/, '')

    if (value.match(/[^0-9]/)) {
      return tel
    }

    // country,
    var city,
      number

    switch (value.length) {
      case 10: // +1PPP####### -> C (PPP) ###-####
        // country = "";
        city = value.slice(0, 3)
        number = value.slice(3)
        break

        // case 11:  +CPPP####### -> CCC (PP) ###-####
        //     country = value[0];
        //     city = value.slice(1, 4);
        //     number = value.slice(4);
        //     break;
        //
        // case 12:  +CCCPP####### -> CCC (PP) ###-####
        //     country = value.slice(0, 3);
        //     city = value.slice(3, 5);
        //     number = value.slice(5);
        //     break;

      default:
        return tel
    }

    // if (country == 1) {
    //     country = "";
    // }

    number = number.slice(0, 3) + '-' + number.slice(3)

    return ('(' + city + ') ' + number).trim()
  }
})

// function Ctrl($scope){
//     $scope.phoneNumber =  4085265552;
//
// }
