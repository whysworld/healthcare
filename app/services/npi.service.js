app.service('npiService', function($http) {

	var NPPES_API_url = 'https://npiregistry.cms.hhs.gov/api/';
	var npiData = [];

	npiData.getProvider = function(
		first_name, // Only for individual providers. Trailing wildcard permitted after 2 characters
		last_name, // Only for individual providers. Trailing wildcard permitted after 2 characters
		state, // The State abbreviation associated with the provider's address identified in address_purpose. Cannot be the only input criterion
		number,  // 10-digit NPI assigned to the provider
		enumeration_type, // 'NPI-1' for Individual Providers, 'NPI-2' for Organizational Providers
		taxonomy_description, // Search for providers by their taxonomy
		organization_name, // Only for organizational providers. Trailing wildcard permitted after 2 characters
		address_purpose, // 'LOCATION' or 'MAILING': whether the address information pertains to the provider's mailing address or the practice location address
		city, // The city associated with the provider's address identified in address_purpose. Cannot be the only input criterion
		postal_code, // The Postal Code associated with the provider's address identified in address_purpose
		country_code, // The country associated with the provider's address identified in address_purpose. 
		limit, // Default: 10. Can be set to any value from 1 to 200.
		skip, // The number of results to be bypassed and not included in the output
		pretty // When checked, the response will be displayed in an easy to read format. Not used for our scopes
		) {
		return $http({
			method: 'JSONP',
			url: NPPES_API_url,
			params: {
				number: number,
				enumeration_type: enumeration_type,
				taxonomy_description: taxonomy_description,
				first_name: first_name,
				last_name: last_name,
				organization_name: organization_name,
				address_purpose: address_purpose,
				city: city,
				state: state,
				postal_code: postal_code,
				country_code: country_code,
				limit: limit,
				skip: skip,
				pretty: pretty
			},
			transformResponse: function (data, headersGetter, status) {
			        //This was implemented since the REST service is returning a plain/text response
			        //and angularJS $http module can't parse the response like that.
			        return {data: data};
			    }
			})
	}

	return npiData;
});