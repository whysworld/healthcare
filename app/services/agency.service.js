app.factory('agencyService', function(localStorageService) {

  var selectedAgency = {};

  function set(data) {
    // selectedAgency = data;
    localStorageService.set('selAgency', data);
    console.log("set", data);
  }

  function get() {
    // return selectedAgency;
    return localStorageService.get('selAgency');
  }

  return {
    set: set,
    get: get
  };
});
