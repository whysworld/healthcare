app.factory('patientService', function(localStorageService) {

  function set(data) {
    // selectedPatient = data;
    localStorageService.set('selPatient', data);
    console.log("set",data);
  }

  function get() {
    // return selectedPatient;
    return localStorageService.get('selPatient');
  }

  function setPendingPatient(data) {
    // pendingPatient = data;
    localStorageService.set('pendingPatient', data);
  }

  function getPendingPatient() {
    // return pendingPatient;
    return localStorageService.get('pendingPatient');
  }

  return {
    set: set,
    get: get,
    setPendingPatient: setPendingPatient,
    getPendingPatient: getPendingPatient
  };
});
