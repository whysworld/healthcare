app.service('patientService', function($http, $q) {
  var base = 'http://mayahealthcare.com/dev/services/api.php';
  var patientData = [];

  patientData.getPatients = function() {
    var deferred = $q.defer();
    $http.get(base + '?x=getPatients').success (function(data) {
      deferred.resolve(data);
    }).error(function(error) {
      deferred.reject();
    });
    return deferred.promise;
  };

  patientData.getPatient = function() {
    var deferred = $q.defer();
    $http.get(base + '?x=getPatient').success (function(data) {
      deferred.resolve(data);
    }).error(function(error) {
      deferred.reject();
    });
    return deferred.promise;
  };

  patientData.postPatient = function(patient) {
    console.log(patient);
    $http.post(base + '?x=addPatient', patient).then(
      function (response) {
        console.log(response);
      },
      function (response) {
        console.log(response);
      });
  };
  return patientData;
});

app.factory('patientInfo', function($http, $q) {
  var deferred = $q.defer();
  $http.get('data/patients.json').success(function(data) {
      deferred.resolve(data);
  }).error(function(error) {
    deferred.reject();
  });
  return deferred.promise;
});
app.factory('employeeInfo', function($http, $q) {
  var deferred = $q.defer();
  $http.get('data/employees.json').success(function(data) {
      deferred.resolve(data);
  }).error(function(error) {
    deferred.reject();
  });
  return deferred.promise;
});
