app.service('auth', function($window, $injector, localStorageService) {

	localStorageService.set('loginModalOpen', false);

	function parseJwt(token) {
		var base64Url = token.split('.')[1];
		var base64 = base64Url.replace('-', '+').replace('_', '/');
		return JSON.parse($window.atob(base64));
	};
	
	function saveAccessToken(token) {
		console.log('saving access token', token);
		// $window.localStorage['jwtToken'] = token;
		localStorageService.set('user', parseJwt(token));
		localStorageService.set('access_token', token);
		localStorageService.set('refreshing_token', false);
	};

	// function checkAccessTokenExp() {
	// 	// If the access token is expired, we want to get a new one:
	// 	var token = localStorageService.get('access_token');
	// 	var refresh_token = localStorageService.get('refresh_token');
	// 	var user = localStorageService.get('user');



	// 	if (token && 
	// 		refresh_token &&
	// 		user && 
	// 		user.exp - 10 < moment(new Date().getTime()).unix() &&
	// 		!localStorageService.get('refreshing_token')) {

	// 		console.log("token almost expired! sending request to refresh it!");
	// 		// If the token is almost expired
			
	// 		// set refreshing_token to true so that only one async call exists
	// 		localStorageService.set('refreshing_token', true);
	// 		var httpService = $injector.get('$http');
	// 		return httpService({
	// 			url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + 'refresh_token',
	// 			method: 'POST',
	// 			data: $httpParamSerializerJQLike(
	// 				{
	// 					'u' : user.usr,
	// 					'refresh_tkn' : refresh_token
	// 				}
	// 			),
	// 			headers: {
	// 			  'Content-Type': 'application/x-www-form-urlencoded'
	// 			}
	// 		}).then(function(response) {
	// 			console.log('Saving token');
	// 			console.log('token data returned', response.data);
	// 			// auth.saveAccessToken(response.data['access_jwt']);
	// 			// auth.saveRefreshToken(response.data['']);
				
	// 			return localStorageService.get('access_token');
	// 	    }, function(response) {
	// 	    	console.log('Unable to refresh token', response);
	// 	    	return 0;
	// 	    });
	// 	}
	// 	return $q.when(localStorageService.get('access_token'));
	// }
	
	function getAccessToken() {
		// return $window.localStorage['jwtToken'];
		// checkAccessTokenExp().then(
		// 	function(data) {
		// 		return data;
		// 	}
		// )
		
		return localStorageService.get('access_token');
	};

	function saveRefreshToken(token) {
		console.log('saving refresh token', token);
		localStorageService.set('refresh_token', token);
	};

	function getRefreshToken(token) {
		return localStorageService.get('refresh_token');
	}

	function getUser() {
		return localStorageService.get('user');
	};
	
	function isAuthed() {
		var token = getAccessToken();
		if (token) {
			var params = parseJwt(token);
			if (typeof params.exp === "undefined") {
				return true
			} else {
				return Math.round(new Date().getTime() / 1000) <= params.exp;
			}
		} else {
			return false;
		}
	}
	
	function logout() {
		// $window.localStorage.removeItem('jwtToken');
		localStorageService.clearAll();
	}

	return {
		parseJwt: parseJwt,
		saveAccessToken: saveAccessToken,
		getAccessToken: getAccessToken,
		saveRefreshToken: saveRefreshToken,
		getRefreshToken: getRefreshToken,
		getUser: getUser,
		isAuthed: isAuthed,
		logout: logout
	};
});

app.factory('authInterceptor', function($q, $injector, CONSTANTS, auth, $httpParamSerializerJQLike, localStorageService, $location) {
	
	loginRequest = function() {
	  // var modalInstance = $injector.get('$uibModal').open({
	  //     controller: 'LoginRequestController',
	  //     ariaLabelledBy: 'modal-title',
	  //     ariaDescribedBy: 'modal-body',
	  //     templateUrl: './views/modals/loginRequest/loginRequest.html',
	  //     size: 'lg',
	  //     // resolve: {
	  //     //   getPatientMedicationList: function() {
	  //     //     return $scope.getPatientMedicationList;
	  //     //   }
	  //     // }
	  //     // resolve: {
	  //     //   // insurance: insurance
	  //     // }
	  //   });
	  // return modalInstance.result;
	  var modalOptions = {
	  	closeButtonText: 'Cancel',
	  	actionButtonText: 'Login',
	  	headerText: 'Login',
	  	loginname: auth.getUser(), 
	  	bodyText: 'Please enter your password'
	  };

	  return $injector.get('loginRequest').showModal({}, modalOptions);

	};

	var inFlightAuthRequest = null;

	return {
		// attach Authorization header
		request: function(config) {
			var token = auth.getAccessToken();
			if (config.url.indexOf(CONSTANTS.api.baseUrl) === 0 && token) {
				config.headers.Authorization = 'Bearer ' + token;
			}
			
			return config;
		},
		
		// save token - responseError does it on 401 code, login does it on 200 login
		// response: function(res) {
		// 	if (res.config.url.indexOf(CONSTANTS.api.baseUrl) === 0 && res.data.access_jwt) {
		// 		console.log("Saving the token!");
		// 		auth.saveAccessToken(res.data.access_jwt);
		// 	}

		// 	if (res.config.url.indexOf(CONSTANTS.api.baseUrl) === 0 && res.data.refresh_tkn) {
		// 		console.log("Saving the refresh token!");
		// 		auth.saveRefreshToken(res.data.refresh_tkn);
		// 	}
			
		// 	return res;
		// }

		responseError: function(response) {
			console.log('response error', response);
			switch (response.status) {
				case 401:
					console.log('401 received', response);
					var deferred = $q.defer();
					// var user = localStorageService.get('user');
					var refresh_token = localStorageService.get('refresh_token');
					if (refresh_token) {
						if(!inFlightAuthRequest) {
							var httpService = $injector.get('$http');
							inFlightAuthRequest = httpService({
								url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + 'refresh_token',
								method: 'POST',
								data: $httpParamSerializerJQLike(
								{
									// 'u' : user.usr,
									'refresh_tkn' : localStorageService.get('refresh_token')
								}
								),
								headers: {
									'Content-Type': 'application/x-www-form-urlencoded'
								}
							});
						} 
						inFlightAuthRequest.then(function(r) {
							inFlightAuthRequest = null;
							console.log('refresh request response', r);

							if (r.status != 200) {
								console.log('response not 200');
								inFlightAuthRequest = null;
								deferred.reject();
								// authService.clear();
								
								// Show a modal to request user login
								var loginreq = loginRequest();
								loginreq.then(function(tokendata) {
									console.log('success!');

									// We want to repeat whatever was expected to happen!
									console.log('Saving token');
									console.log('token data returned', tokendata);

									auth.saveAccessToken(tokendata['access_jwt']);
									auth.saveRefreshToken(tokendata['refresh_tkn']);

									$injector.get("$http")(response.config).then(function(resp) {
										console.log('resolving whatever');
										deferred.resolve(resp);
									},function(resp) {
										console.log('failed whatever');
										deferred.reject();
									});

									// console.log('Saving token');
									// console.log('token data returned', response.data);

									// auth.saveAccessToken(response.data['access_jwt']);
									// auth.saveRefreshToken(response.data['refresh_tkn']);
									
									// console.log('Moving to welcome page');
									// $location.path('welcomePage');
							    }, function(response) {
							    	console.log('Moving to the login page!');
							    	$location.path('/');
							    	// console.log('Unable to login', response);
							    	// console.log('Not moving to welcome page');
							    	// $scope.incorrectLogin = true;
							    	// $scope.password = '';
							    });

								// $injector.get("$location").path('/');
								return;
							}

							if (r.data.access_jwt && r.data.refresh_tkn) {
								auth.saveAccessToken(r.data.access_jwt);
								auth.saveRefreshToken(r.data.refresh_tkn);
								// authService.setExpiresIn(r.data.data.expiresin);
								$injector.get("$http")(response.config).then(function(resp) {
									deferred.resolve(resp);
								},function(resp) {
									deferred.reject();
								});
							} else {
								deferred.reject();
							}
						}, function(response) {
							// TODO: Show some warning here!
							inFlightAuthRequest = null;
							deferred.reject();
							// authService.clear();
							$injector.get("$location").path('/');
							return;
						});
						return deferred.promise;
					}
					break;
				default:
					// authService.clear();
					// $injector.get("$state").go('guest.login');
					break;
			}
			return response || $q.when(response);
		}
	}
});

app.config(function($provide, $httpProvider) {
	$httpProvider.interceptors.push('authInterceptor');
});