app.factory('navService', function(localStorageService) {

  // var selectedEmployee = {};

  function set(data) {
    // selectedEmployee = data;
    localStorageService.set('nav', data);
    // console.log("set", data);
  }

  function get() {
    // return selectedEmployee;
    return localStorageService.get('nav');
  }

  return {
    set: set,
    get: get
  };
});
