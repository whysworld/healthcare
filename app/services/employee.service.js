app.factory('employeeService', function(localStorageService) {

  var selectedEmployee = {};

  function set(data) {
    // selectedEmployee = data;
    localStorageService.set('selEmployee', data);
    console.log("set", data);
  }

  function get() {
    // return selectedEmployee;
    return localStorageService.get('selEmployee');
  }

  return {
    set: set,
    get: get
  };
});
