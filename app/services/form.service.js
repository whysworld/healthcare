app.factory('formService', function() {

  var selectedForm = {};

  function set(data) {
    selectedForm = data;
    console.log('set', selectedForm);
  }

  function get() {
    return selectedForm;
  }

  return {
    set: set,
    get: get
  };
});
