app.service('loginRequest', function($uibModal, $q, $http, localStorageService, CONSTANTS, $httpParamSerializerJQLike, md5, auth) {
  var modalDefaults = {
      backdrop: true,
      keyboard: true,
      modalFade: true,
      templateUrl: 'views/modals/loginRequest/loginRequest.html'
  };

  var userDetails = auth.getUser();

  var modalOptions = {
    closeButtonText: 'Cancel',
    actionButtonText: 'Login',
    headerText: 'Login',
    loginname: userDetails['usr'], 
    usernameText: 'Username:',
    passwordText: 'Password:',
    incorrectLoginText: 'Unable to log in. Please check your password.'
  };


  this.showModal = function (customModalDefaults, customModalOptions) {
    if (!localStorageService.get('loginModalOpen')) {
      localStorageService.set('loginModalOpen', true);

      if (!customModalDefaults) customModalDefaults = {};
      customModalDefaults.backdrop = 'static';
      return this.show(customModalDefaults, customModalOptions);

    } else {
      return $q(function(resolve, reject) {
        resolve('Modal already open');
      });
    }
  };

  this.show = function (customModalDefaults, customModalOptions) {
      //Create temp objects to work with since we're in a singleton service
      var tempModalDefaults = {};
      var tempModalOptions = {};

      //Map angular-ui modal custom defaults to modal defaults defined in service
      angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

      //Map modal.html $scope custom properties to defaults defined in service
      angular.extend(tempModalOptions, modalOptions, customModalOptions);

      if (!tempModalDefaults.controller) {
          tempModalDefaults.controller = function ($scope, $uibModalInstance) {
              $scope.modalOptions = tempModalOptions;

              $scope.username = userDetails['usr'];

              $scope.modalOptions.ok = function (username, password) {

                console.log('username', username);
                console.log('password', password);

                $http({
                  url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + 'login',
                  method: 'POST',
                  data: $httpParamSerializerJQLike(
                    {
                      'u' : username,
                      'p' : md5.createHash(password || '')
                    }
                  ),
                  headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                  }
                }).then(function(response) {
                  // console.log('Saving token');
                  // console.log('token data returned', response);

                  if (response.status == 200) {

                    // auth.saveAccessToken(response.data['access_jwt']);
                    // auth.saveRefreshToken(response.data['refresh_tkn']);

                    // Closing the modal and showing the window again
                    $uibModalInstance.close(response.data);
                  } else {
                    console.log('Unable to login', response);
                    $scope.incorrectLogin = true;
                    $scope.password = '';

                    // TODO: Show some message as an error!
                  }
                });

                  // $uibModalInstance.close(result);
              };
              $scope.modalOptions.close = function (result) {
                  $uibModalInstance.dismiss('cancel');

                  // TODO: Redirect to login page
              };
          }
      }

      return $uibModal.open(tempModalDefaults).result;
  };

});

// app.controller('LoginRequestController', function($scope, $location, auth, $http, CONSTANTS, $httpParamSerializerJQLike, $filter, $uibModal, $uibModalInstance, md5) {

//   console.log('loginRequest loaded');

//   $scope.close = function(data) {
//     // redirect to landing page
//     $uibModalInstance.close(data);
//   };

//   $scope.dismiss = function(data) {
//     $uibModalInstance.dismiss(data);
//   }

//   $scope.login = function() {
//     $http({
//       url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + 'login',
//       method: 'POST',
//       data: $httpParamSerializerJQLike(
//         {
//           'u' : $scope.username,
//           'p' : md5.createHash($scope.password || '')
//         }
//       ),
//       headers: {
//         'Content-Type': 'application/x-www-form-urlencoded'
//       }
//     }).then(function(response) {
//       console.log('Saving token');
//       console.log('token data returned', response);

//       if (response.status == 200) {

//         // auth.saveAccessToken(response.data['access_jwt']);
//         // auth.saveRefreshToken(response.data['refresh_tkn']);

//         // Closing the modal and showing the window again
//         $scope.close(1);
//       } else {
//         console.log('Unable to login', response);
//         $scope.incorrectLogin = true;
//         $scope.password = '';

//         // TODO: Show some message as an error!
//       }
//     });
//   };

// });
