app.config(function($stateProvider) {
  $stateProvider.state('patientChoice', {
    url: '/patientChoice',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/patientChoice/patientChoice.html',
        controller: 'PatientChoiceController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
