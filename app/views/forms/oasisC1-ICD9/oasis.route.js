app.config(function($stateProvider) {
  $stateProvider.state('oasis', {
    url: '/oasis',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/oasisC1-ICD9/oasis.html',
        controller: 'OasisController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
