app.controller('FaceController', function ($scope, $http, $httpParamSerializerJQLike, $filter, formService, dataService, patientService, $anchorScroll, $timeout, CONSTANTS, $state) {
  $scope.PREVIOUS = function () {
    window.history.back();
  };
  $scope.editform = 'no';
  $scope.imgvalidate = false;
  var imgvalidateerror = 1;
  var getFormValue = formService.get();
  $scope.visitTypes = [];
  $scope.employees = [];
  $scope.formDiv = true;
  $scope.PREVIOUS = function () {
    window.history.back();
  };
  $scope.showPatSignatureImage = false;
  $scope.showPatSignaturePad = true;


  $scope.globalPatientDetails = {};
  $scope.getDetails = function () {

    if (getFormValue.STATUS == 0) {
      var formData = {
        formId: getFormValue.FORMID
      }
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['faceSheet'].getAll,
        method: 'POST',
        data: $httpParamSerializerJQLike(formData),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }

      }).success(function (data) {

        console.log('==' + data.PATKEYID);
        $scope.patient_name = data.PATKEYID;
        $scope.patient_name_id = data.fn + ' ' + data.ln;
        $scope.patient_dob = data.dob;
        $scope.formId = data.formId;

      });
    }
  };
  $scope.getParticularDetail = function (modalValue) {
    if (modalValue && typeof (modalValue) != 'undefined') {
      $scope.globalPatientDetails.forEach(function (detail) {
        if (detail.patient_id === modalValue) {
          $scope.patient_dob = detail.patient_dob;

        }
      });
    } else {
      $scope.patient_dob = '';
    }
  }
  $scope.validationmsg = false;
  $scope.saveFacetoFaceSheet = function (form) {
    /* Checkbox validation */
    var criBchk1 = $scope.cribcheck1;
    var criBchk2 = $scope.cribcheck2;
    var criAchk1 = $scope.criteriaachk1;
    var criAchk2 = $scope.criteriaachk2;
    var radioset = $scope.step_2_radio;


    $scope.chk1 = false;
    $scope.chk2 = false;
    $scope.chk3 = false;
    $scope.chk4 = false;
    $scope.chk5 = false;
    if (typeof (criBchk1) != 'undefined' && criBchk1 != '') {
      $scope.chk1 = true;
    } else {
      $scope.validationmsg = true;
    }
    if (typeof (criBchk2) != 'undefined' && criBchk2 != '') {
      $scope.chk2 = true;
    } else {
      $scope.validationmsg = true;
    }
    if ((typeof (criAchk1) != 'undefined' && criAchk1 != '')) {
      $scope.chk3 = true;

    } else if ((typeof (criAchk2) != 'undefined' && criAchk2 != '')) {
      $scope.chk4 = true;

    } else {

      $scope.validationmsg1 = true;
    }
    if (radioset != '' && typeof (radioset) != 'undefined') {
      $scope.chk5 = true;

    } else {

      $scope.validationmsg2 = true;
    }

    if ($scope.editform == 'no') {
      $scope.si = $scope.acceptPat().dataUrl;
    } else if ($scope.editform == 'yes') {
      $scope.si = $scope.empSignature;
    }

    if (typeof $scope.si == 'undefined' || $scope.si == '') {
      $scope.imgvalidate = true;
      imgvalidateerror = 0;

    } else {
      if ($scope.acceptPat().dataUrl != 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjgAAADcCAQAAADXNhPAAAACIklEQVR42u3UIQEAAAzDsM+/6UsYG0okFDQHMBIJAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcCQADAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDkQAwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAegeayZAN3dLgwnAAAAAElFTkSuQmCC') {
        $scope.imgvalidate = false;
        imgvalidateerror = 1;
      }
    }
    console.log(imgvalidateerror);

    if (form.$valid && $scope.chk1 && $scope.chk2 && ($scope.chk3 || $scope.chk4) && $scope.chk5 && imgvalidateerror) { //form.$valid && !$scope.warning && $scope.validationmsg == true
      if (!confirm("No changes are allowed once submitted to Face-to-Face, are you sure you want to submit?")) { //no don't submit to qa
        console.log('no, do not submit to qa');
        event.preventDefault();
      } else { //yes submit to qa


        var newRouteSheet = {
          patient_id: $scope.patient_name,
          date_encounter: $filter("date")($scope.step_2, 'yyyy/MM/dd'),
          encounter_service: $scope.step_2_radio,
          step3_certify: $scope.step_3,
          step4_criteriaA_1: $scope.criteriaatext1,
          step4_criteriaA_2: $scope.criteriaatext2,
          step4_criteriaB_1: $scope.criteriabtext1,
          step4_criteriaB_2: $scope.criteriabtext2,
          step5_nurshingcare: $scope.step5_nurshingcare,
          step5_physical: $scope.step5_physical,
          step5_occupation: $scope.step5_occupation,
          step5_speech: $scope.step5_speech,
          step5_ceritified: $scope.step5_ceritified,
          step5_medical: $scope.step5_medical,
          step5_telehealth: $scope.step5_telehealth,
          // SIGREQUIRED_PAT: $scope.sigrequired_pat,
          physician_sign_path: $scope.si,
          date: $filter("date")($scope.date, 'yyyy/MM/dd'),
          physician_name: $scope.physician_name,
          FORMID: $scope.formId,
          edit_form: $scope.editform,
          currentTableId: $scope.currenttableId

        };

        $http({
          url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['faceSheet'].add,
          method: 'POST',
          data: $httpParamSerializerJQLike(newRouteSheet),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          }

        }).success(function (response) {
          console.log('saving routesheet', response);
          if (response.status === 'success') {
            $anchorScroll();
            $scope.successTextAlert = response.msg;
            $scope.showSuccessAlert = true;
            $timeout(function () {
              $state.go(response.goto);
            }, 2000);
          }
        }, function (data) {
          console.log(data);
        });
      }
    } else {
      $anchorScroll();
      console.log("invalid form");
    }
  };
  $scope.form = {};
  $scope.form1 = {};
  $scope.getDetails();
  if (getFormValue.STATUS == 1 || getFormValue.STATUS == 3) {
    $scope.patient_name = getFormValue.PATKEYID;
    $scope.patient_name_id = getFormValue.FNAME + ' ' + getFormValue.LNAME;
    $scope.formId = getFormValue.FORMID;
    $scope.form1.physician_sign_path = getFormValue.physician_sign_path;
    $scope.empSignature = $scope.form1.physician_sign_path;


    $scope.step_2_radio = getFormValue.encounter_service;
    $scope.step_3 = getFormValue.step3_certify;
    $scope.criteriaatext1 = getFormValue.step4_criteriaA_1;
    if ($scope.criteriaatext1 != '') {
      $scope.criteriaachk1 = true;
    }
    $scope.criteriaatext2 = getFormValue.step4_criteriaA_2;
    if ($scope.criteriaatext2 != '') {
      $scope.criteriaachk2 = true;
    }
    $scope.criteriabtext1 = getFormValue.step4_criteriaB_1;
    $scope.criteriabtext2 = getFormValue.step4_criteriaB_2;
    if ($scope.criteriabtext1 != '' && $scope.criteriabtext2 != '') {
      $scope.cribcheck1 = true;
      $scope.cribcheck2 = true;
    }
    $scope.step5_nurshingcare = getFormValue.step5_nurshingcare;
    $scope.step5_physical = getFormValue.step5_physical;
    $scope.step5_occupation = getFormValue.step5_occupation;
    $scope.step5_speech = getFormValue.step5_speech;
    $scope.step5_ceritified = getFormValue.step5_ceritified;
    $scope.step5_medical = getFormValue.step5_medical;
    $scope.step5_telehealth = getFormValue.step5_telehealth;
    $scope.physician_name = getFormValue.physician_name;
    $scope.date = getFormValue.date;
    $scope.patient_dob = getFormValue.DOB;
    var visitdate = $filter("date")(getFormValue.FORMDATE, 'yyyy/MM/dd');
    $scope.form.FORMDATE = new Date(visitdate);
    var igDate = $filter("date")(getFormValue.SIGDATE, 'yyyy/MM/dd');
    $scope.form.SIGDATE = new Date(igDate);

  }
  $scope.isQAMode = function () {
    if (getFormValue.ACTION === 'QAREVIEW') {
      $scope.currenttableId = getFormValue.formPrimaryId;
      $scope.approvedMsg = false; //form is not locked, allow editing
      $scope.enableQAEditBtn = true;
      $scope.enableQALockBtn = true;
      $scope.enableQARejectBtn = true;
      $scope.submitQAbtn = false;
      $scope.disableEdit = true;
      $scope.enableInput = false;
      $scope.showEmpSignatureImage = true;
      $scope.showEmpSignaturePad = false;
      $scope.showPatSignatureImage = true;
      $scope.showPatSignaturePad = false;
      var igDate = $filter("date")(getFormValue.date, 'yyyy/MM/dd');
      $scope.form.SIGDATE = new Date(igDate);
      var visitdate = $filter("date")(getFormValue.date_encounter, 'yyyy/MM/dd');
      $scope.form.FORMDATE = new Date(visitdate);

    } else {
      if ((getFormValue.STATUS === 1) || (getFormValue.STATUS === 3)) {
        $scope.disableEdit = true;
        $scope.enableInput = false;
        $scope.submitQAbtn = false;
        $scope.showEmpSignatureImage = true;
        $scope.showEmpSignaturePad = false;
        $scope.showPatSignatureImage = true;
        $scope.showPatSignaturePad = false;
      }
    }
  }
  //qa editing
  $scope.edit = function () {
    $scope.disableEdit = false;
    $scope.enableInput = true;
    $scope.enableQALockBtn = false;
    $scope.enableQAEditBtn = false;
    $scope.enableQARejectBtn = false;
    $scope.enableCancelBtn = true;
    $scope.enableSaveBtn = true;
    $scope.editform = "yes";

  };

  //cancel qa editing
  $scope.cancel = function () {
    $scope.disableEdit = true;
    $scope.enableInput = false;
    $scope.enableQALockBtn = true;
    $scope.enableQAEditBtn = true;
    $scope.enableQARejectBtn = true;
    $scope.enableCancelBtn = false;
    $scope.enableSaveBtn = false;
    $scope.getRouteSheet(); //get original route sheet info
    $scope.editform = "no";
  };
  $scope.reject = function () {
    //change form status to in progress, and attach note?
    var form = {
      KEYID: getFormValue.FORMID,
      STATUS: 4,
      // DETAIL1: 'Form has been rejected by QA'
    };

    dataService.edit('form', form).then(function (response) {

      $state.go('qa');
    });
  };
  //qa approves form
  $scope.approve = function () {

    dataService.edit('form', {
      KEYID: getFormValue.FORMID,
      STATUS: 3
    }).then(function (response) {
      console.log('editing form', response);
      $state.go('qa');
    });
  }
  $scope.removeEmpSignature = function () {

    $scope.showPatSignaturePad = true;
    $scope.showEmpSignatureImage = false;
  };
  $scope.isQAMode();
  $scope.clearPat1 = function () {
    $scope.imgvalidate = true;
    imgvalidateerror = 0;
  }
});
