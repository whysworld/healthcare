app.config(function ($stateProvider) {
  $stateProvider.state('lvn', {
    url: '/lvn',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/lvn/lvn.html',
        controller: 'lvnController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  })
})
