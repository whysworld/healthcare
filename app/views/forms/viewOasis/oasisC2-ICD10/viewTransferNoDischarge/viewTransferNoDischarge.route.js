app.config(function($stateProvider) {
  $stateProvider.state('viewOasisTransferNoDischarge', {
    url: '/oasisC2/viewOasisTransferNoDischarge',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/viewOasis/oasisC2-ICD10/viewTransferNoDischarge/viewTransferNoDischarge.html',
        controller: 'ViewTransferNoDischargeController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
