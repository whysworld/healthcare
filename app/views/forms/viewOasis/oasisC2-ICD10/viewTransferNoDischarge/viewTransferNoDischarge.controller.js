app.controller('ViewTransferNoDischargeController', function($scope, $state, $http, CONSTANTS, $httpParamSerializerJQLike, $filter, patientService,dataService, formService) {


  $scope.patient = patientService.get();


  $scope.currentForm = 'General';

  $scope.nextForm = function(current, destination) {
    if ($scope.currentForm != destination) {
      $scope.currentForm = destination;
      $('.' + current ).fadeOut('fast', function() {
        $('.' + destination ).fadeIn('fast');
      });
    }
  };

  $scope.activeForm = function(form) {
    if ($scope.currentForm == form) {
      return true;
    }
  };

  $scope.submitSection = function(form, destination) {
    console.log(form.$submitted);
    console.log(form.$valid);
    if(form.$valid) {
      $('#' + $scope.currentForm + ' i').css({color: 'green'});
      var section = $scope.currentForm + 'Complete';

      $scope.nextForm($scope.currentForm, destination);
      console.log(section);
      $scope[section] = true;
      console.log($scope[section]);
    }
    else {
      $('#' + $scope.currentForm + ' i').css({color: 'lightgrey'});
      console.log('Not Valid');
    }
  };

  //alerting msg if M0906 date is changed and different from form date
  $scope.checkDate=function () {
    if ($filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd') !==  $filter("date")($scope.oasis.FORMDATE, 'yyyy/MM/dd')) {
      alert("Note: Changing M0906 will automatically change date of visit.");
      $scope.changeVisitDate=true;
    } else {
      $scope.changeVisitDate=false;
    }
  };

  getOasis();
  function getOasis() {
    var oasis = formService.get();
    $scope.oasis=oasis;
    console.log(oasis);

    //show qa buttons if ACTION=QAREVIEW;
    if($scope.oasis.ACTION!==undefined){ //redirected from QA page
      console.log('enabling qa mode');
      //show/hide approved msg
      if (oasis.LOCKED===1){
        $scope.approvedMsg=true; //form is locked, show approved msg
        $scope.enableQAEditBtn=false;
        $scope.enableQALockBtn=false;
        $scope.enableQARejectBtn=false;
        // $scope.disableEditSOCDetails=false;
      } else {
        $scope.approvedMsg=false; //form is not locked, allow editing
        $scope.enableQAEditBtn=true;
        $scope.enableQALockBtn=true;
        $scope.enableQARejectBtn=true;
        // $scope.disableEditSOCDetails=true;
      }
    } else { //redirected from records page
      if ( oasis.LOCKED===1)  { //form is locked, show approved msg
        $scope.approvedMsg=true;
        $scope.enableQAEditBtn=false;
        $scope.enableQALockBtn=false;
        $scope.enableQARejectBtn=false;
        $scope.enableCancelBtn=false;

      } else { //form is not locked, show in review msg
        $scope.inReviewMsg=true;
        $scope.enableQAEditBtn=false;
        $scope.enableQALockBtn=false;
        $scope.enableQARejectBtn=false;
        $scope.enableCancelBtn=false;
      }
    }

    if (oasis.M0090_INFO_COMPLETED_DT===null){
      $scope.M0090_INFO_COMPLETED_DT="";
    } else {
      $scope.M0090_INFO_COMPLETED_DT=new Date($filter("date")(oasis.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd'));
    }
    if (oasis.M0903_LAST_HOME_VISIT !== null) {
      $scope.M0903_LAST_HOME_VISIT = new Date($filter("date")(oasis.M0903_LAST_HOME_VISIT, 'yyyy/MM/dd'))
    }
    if (oasis.M0906_DC_TRAN_DTH_DT !== null) {
      $scope.M0906_DC_TRAN_DTH_DT = new Date($filter("date")(oasis.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd'))
    }
    if (oasis.M0032_ROC_DT !== null) {
      $scope.M0032_ROC_DT = new Date($filter("date")(oasis.M0032_ROC_DT, 'yyyy/MM/dd'));
    } else {
      $scope.M0032_ROC_DT=''
    }

    $scope.oasisKEYID=oasis.KEYID;

    //******** GENERAL ********//
    $scope.M0010_CCN=oasis.M0010_CCN;
    $scope.M0014_BRANCH_STATE=oasis.M0014_BRANCH_STATE;
    $scope.M0016_BRANCH_ID=oasis.M0016_BRANCH_ID;
    $scope.M0018_PHYSICIAN_ID=oasis.M0018_PHYSICIAN_ID;
    $scope.M0018_PHYSICIAN_UK=oasis.M0018_PHYSICIAN_UK;
    $scope.M0020_PAT_ID=oasis.M0020_PAT_ID;
    $scope.M0030_START_CARE_DT= new Date($filter("date")(oasis.M0030_START_CARE_DT, 'yyyy/MM/dd'));
    // $scope.M0032_ROC_DT=new Date($filter("date")(oasis.M0032_ROC_DT, 'yyyy/MM/dd'));
    $scope.M0032_ROC_DT_NA=oasis.M0032_ROC_DT_NA;
    $scope.M0040_PAT_FNAME=oasis.M0040_PAT_FNAME;
    $scope.M0040_PAT_MI=oasis.M0040_PAT_MI;
    $scope.M0040_PAT_LNAME=oasis.M0040_PAT_LNAME;
    $scope.M0040_PAT_SUFFIX=oasis.M0040_PAT_SUFFIX;
    $scope.M0050_PAT_ST=oasis.M0050_PAT_ST;
    $scope.M0060_PAT_ZIP=oasis.M0060_PAT_ZIP;
    $scope.M0063_MEDICARE_NUM=oasis.M0063_MEDICARE_NUM;
    $scope.M0063_MEDICARE_NA=oasis.M0063_MEDICARE_NA;
    $scope.M0064_SSN=oasis.M0064_SSN;
    $scope.M0064_SSN_UK=oasis.M0064_SSN_UK;
    $scope.M0065_MEDICAID_NUM=oasis.M0065_MEDICAID_NUM;
    $scope.M0065_MEDICAID_NA=oasis.M0065_MEDICAID_NA;
    $scope.M0066_PAT_BIRTH_DT= new Date($filter("date")(oasis.M0066_PAT_BIRTH_DT, 'yyyy/MM/dd'));
    $scope.M0069_PAT_GENDER=oasis.M0069_PAT_GENDER;
    $scope.M0140_ETHNIC_AI_AN=oasis.M0140_ETHNIC_AI_AN;
    $scope.M0140_ETHNIC_ASIAN=oasis.M0140_ETHNIC_ASIAN;
    $scope.M0140_ETHNIC_BLACK=oasis.M0140_ETHNIC_BLACK;
    $scope.M0140_ETHNIC_HISP=oasis.M0140_ETHNIC_HISP;
    $scope.M0140_ETHNIC_NH_PI=oasis.M0140_ETHNIC_NH_PI;
    $scope.M0140_ETHNIC_WHITE=oasis.M0140_ETHNIC_WHITE;
    $scope.M0150_CPAY_NONE=oasis.M0150_CPAY_NONE;
    $scope.M0150_CPAY_MCARE_FFS=oasis.M0150_CPAY_MCARE_FFS;
    $scope.M0150_CPAY_MCARE_HMO=oasis.M0150_CPAY_MCARE_HMO;
    $scope.M0150_CPAY_MCAID_FFS=oasis.M0150_CPAY_MCAID_FFS;
    $scope.M0150_CPAY_MCAID_HMO=oasis.M0150_CPAY_MCAID_HMO;
    $scope.M0150_CPAY_WRKCOMP=oasis.M0150_CPAY_WRKCOMP;
    $scope.M0150_CPAY_TITLEPGMS=oasis.M0150_CPAY_TITLEPGMS;
    $scope.M0150_CPAY_OTH_GOVT=oasis.M0150_CPAY_OTH_GOVT;
    $scope.M0150_CPAY_PRIV_INS=oasis.M0150_CPAY_PRIV_INS;
    $scope.M0150_CPAY_PRIV_HMO=oasis.M0150_CPAY_PRIV_HMO;
    $scope.M0150_CPAY_SELFPAY=oasis.M0150_CPAY_SELFPAY;
    $scope.M0150_CPAY_OTHER=oasis.M0150_CPAY_OTHER;
    $scope.M0150_CPAY_UK=oasis.M0150_CPAY_UK;

    //clinical record
    $scope.M0080_ASSESSOR_DISCIPLINE=oasis.M0080_ASSESSOR_DISCIPLINE;
    // $scope.M0090_INFO_COMPLETED_DT=oasis.M0090_INFO_COMPLETED_DT;
    $scope.M0100_ASSMT_REASON=oasis.M0100_ASSMT_REASON;
    $scope.M1041_IN_INFLNZ_SEASON=oasis.M1041_IN_INFLNZ_SEASON;
    $scope.M1046_INFLNZ_RECD_CRNT_SEASON=oasis.M1046_INFLNZ_RECD_CRNT_SEASON;
    $scope.M1051_PVX_RCVD_AGNCY=oasis.M1051_PVX_RCVD_AGNCY;
    $scope.M1056_PVX_RSN_NOT_RCVD_AGNCY=oasis.M1056_PVX_RSN_NOT_RCVD_AGNCY;
    //cardiac
    $scope.M1501_SYMTM_HRT_FAILR_PTNTS=oasis.M1501_SYMTM_HRT_FAILR_PTNTS;
    $scope.M1511_HRT_FAILR_NO_ACTN=oasis.M1511_HRT_FAILR_NO_ACTN;
    $scope.M1511_HRT_FAILR_PHYSN_CNTCT=oasis.M1511_HRT_FAILR_PHYSN_CNTCT;
    $scope.M1511_HRT_FAILR_ER_TRTMT=oasis.M1511_HRT_FAILR_ER_TRTMT;
    $scope.M1511_HRT_FAILR_PHYSN_TRTMT=oasis.M1511_HRT_FAILR_PHYSN_TRTMT;
    $scope.M1511_HRT_FAILR_CLNCL_INTRVTN=oasis.M1511_HRT_FAILR_CLNCL_INTRVTN;
    $scope.M1511_HRT_FAILR_CARE_PLAN_CHG=oasis.M1511_HRT_FAILR_CARE_PLAN_CHG;
    $scope.M2005_MDCTN_INTRVTN=oasis.M2005_MDCTN_INTRVTN;
    $scope.M2016_DRUG_EDCTN_INTRVTN=oasis.M2016_DRUG_EDCTN_INTRVTN;
    //emergent care
    $scope.M2301_EMER_USE_AFTR_LAST_ASMT=oasis.M2301_EMER_USE_AFTR_LAST_ASMT;
    $scope.M2310_ECR_MEDICATION=oasis.M2310_ECR_MEDICATION;
    $scope.M2310_ECR_INJRY_BY_FALL=oasis.M2310_ECR_INJRY_BY_FALL;
    $scope.M2310_ECR_RSPRTRY_INFCTN=oasis.M2310_ECR_RSPRTRY_INFCTN;
    $scope.M2310_ECR_RSPRTRY_OTHR=oasis.M2310_ECR_RSPRTRY_OTHR;
    $scope.M2310_ECR_HRT_FAILR=oasis.M2310_ECR_HRT_FAILR;
    $scope.M2310_ECR_CRDC_DSRTHM=oasis.M2310_ECR_CRDC_DSRTHM;
    $scope.M2310_ECR_MI_CHST_PAIN=oasis.M2310_ECR_MI_CHST_PAIN;
    $scope.M2310_ECR_OTHR_HRT_DEASE=oasis.M2310_ECR_OTHR_HRT_DEASE;
    $scope.M2310_ECR_STROKE_TIA=oasis.M2310_ECR_STROKE_TIA;
    $scope.M2310_ECR_HYPOGLYC=oasis.M2310_ECR_HYPOGLYC;
    $scope.M2310_ECR_GI_PRBLM=oasis.M2310_ECR_GI_PRBLM;
    $scope.M2310_ECR_DHYDRTN_MALNTR=oasis.M2310_ECR_DHYDRTN_MALNTR;
    $scope.M2310_ECR_UTI=oasis.M2310_ECR_UTI;
    $scope.M2310_ECR_CTHTR_CMPLCTN=oasis.M2310_ECR_CTHTR_CMPLCTN;
    $scope.M2310_ECR_WND_INFCTN_DTRORTN=oasis.M2310_ECR_WND_INFCTN_DTRORTN;
    $scope.M2310_ECR_UNCNTLD_PAIN=oasis.M2310_ECR_UNCNTLD_PAIN;
    $scope.M2310_ECR_MENTL_BHVRL_PRBLM=oasis.M2310_ECR_MENTL_BHVRL_PRBLM;
    $scope.M2310_ECR_DVT_PULMNRY=oasis.M2310_ECR_DVT_PULMNRY;
    $scope.M2310_ECR_OTHER=oasis.M2310_ECR_OTHER;
    $scope.M2310_ECR_UNKNOWN=oasis.M2310_ECR_UNKNOWN;
    $scope.M2401_INTRVTN_SMRY_DBTS_FT=oasis.M2401_INTRVTN_SMRY_DBTS_FT;
    $scope.M2401_INTRVTN_SMRY_FALL_PRVNT=oasis.M2401_INTRVTN_SMRY_FALL_PRVNT;
    $scope.M2401_INTRVTN_SMRY_DPRSN=oasis.M2401_INTRVTN_SMRY_DPRSN;
    $scope.M2401_INTRVTN_SMRY_PAIN_MNTR=oasis.M2401_INTRVTN_SMRY_PAIN_MNTR;
    $scope.M2401_INTRVTN_SMRY_PRSULC_PRVN=oasis.M2401_INTRVTN_SMRY_PRSULC_PRVN;
    $scope.M2401_INTRVTN_SMRY_PRSULC_WET=oasis.M2401_INTRVTN_SMRY_PRSULC_WET;
    $scope.M2410_INPAT_FACILITY=oasis.M2410_INPAT_FACILITY;
    $scope.M2430_HOSP_MED=oasis.M2430_HOSP_MED;
    $scope.M2430_HOSP_INJRY_BY_FALL=oasis.M2430_HOSP_INJRY_BY_FALL;
    $scope.M2430_HOSP_RSPRTRY_INFCTN=oasis.M2430_HOSP_RSPRTRY_INFCTN;
    $scope.M2430_HOSP_RSPRTRY_OTHR=oasis.M2430_HOSP_RSPRTRY_OTHR;
    $scope.M2430_HOSP_HRT_FAILR=oasis.M2430_HOSP_HRT_FAILR;
    $scope.M2430_HOSP_CRDC_DSRTHM=oasis.M2430_HOSP_CRDC_DSRTHM;
    $scope.M2430_HOSP_MI_CHST_PAIN=oasis.M2430_HOSP_MI_CHST_PAIN;
    $scope.M2430_HOSP_OTHR_HRT_DEASE=oasis.M2430_HOSP_OTHR_HRT_DEASE;
    $scope.M2430_HOSP_STROKE_TIA=oasis.M2430_HOSP_STROKE_TIA;
    $scope.M2430_HOSP_HYPOGLYC=oasis.M2430_HOSP_HYPOGLYC;
    $scope.M2430_HOSP_GI_PRBLM=oasis.M2430_HOSP_GI_PRBLM;
    $scope.M2430_HOSP_DHYDRTN_MALNTR=oasis.M2430_HOSP_DHYDRTN_MALNTR;
    $scope.M2430_HOSP_UR_TRACT=oasis.M2430_HOSP_UR_TRACT;
    $scope.M2430_HOSP_CTHTR_CMPLCTN=oasis.M2430_HOSP_CTHTR_CMPLCTN;
    $scope.M2430_HOSP_WND_INFCTN=oasis.M2430_HOSP_WND_INFCTN;
    $scope.M2430_HOSP_PAIN=oasis.M2430_HOSP_PAIN;
    $scope.M2430_HOSP_MENTL_BHVRL_PRBLM=oasis.M2430_HOSP_MENTL_BHVRL_PRBLM;
    $scope.M2430_HOSP_DVT_PULMNRY=oasis.M2430_HOSP_DVT_PULMNRY;
    $scope.M2430_HOSP_SCHLD_TRTMT=oasis.M2430_HOSP_SCHLD_TRTMT;
    $scope.M2430_HOSP_OTHER=oasis.M2430_HOSP_OTHER;
    $scope.M2430_HOSP_UK=oasis.M2430_HOSP_UK;
    // $scope.M0903_LAST_HOME_VISIT=oasis.M0903_LAST_HOME_VISIT;
    // $scope.M0906_DC_TRAN_DTH_DT=oasis.M0906_DC_TRAN_DTH_DT;
    //
    // if (oasis.M0090_INFO_COMPLETED_DT !== null) {
    //   $scope.M0090_INFO_COMPLETED_DT = new Date($filter("date")(oasis.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd'));
    // }
    // if (oasis.M0903_LAST_HOME_VISIT !== null) {
    //   $scope.M0903_LAST_HOME_VISIT = new Date($filter("date")(oasis.M0903_LAST_HOME_VISIT, 'yyyy/MM/dd'));
    // }
    // if (oasis.M0906_DC_TRAN_DTH_DT !== null) {
    //   $scope.M0906_DC_TRAN_DTH_DT = new Date($filter("date")(oasis.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd'));
    // }
  }

  $scope.disableEditDetails = true;

  $scope.edit = function(){
    // $scope.disableEditDetails = false;
    // $scope.enableInput = true;
    // $scope.enableQALockBtn=false;
    // $scope.enableQAEditBtn=false;
    // $scope.enableQARejectBtn=false;
    // $scope.enableCancelBtn=true;
    var form = {
      ADMITKEYID: $scope.oasis.ADMITKEYID,
      EPIKEYID: $scope.oasis.EPIKEYID,
      FORMID: $scope.oasis.FORMID,
      OBJKEYID: $scope.oasis.OBJKEYID,
      VISITDATE: $scope.oasis.FORMDATE,
      VISITID: $scope.oasis.VISITID,
      ACTION: 'QAEDIT',
    };
    formService.set(form);
    // console.log($scope.oasis)
    $state.go('transferredNotDischarged')
  };

  $scope.cancel=function () {
    $scope.disableEditDetails = true;
    $scope.enableInput = false;
    $scope.enableQALockBtn = true;
    $scope.enableQAEditBtn=true;
    $scope.enableQARejectBtn=true;
    $scope.enableCancelBtn=false;
    getOasis();
  };

  //form approved, oasis LOCKED=1
  $scope.lock=function () {
    //reset current oasis
    dataService.get('oasisC2_2_20_1', {KEYID:$scope.oasisKEYID}).then(function (data) {
      // console.log(data.data[0]);
      var oasis=data.data[0];
      // formService.set(data.data[0])
      console.log(oasis);
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['oasisC2_2_20_1'].edit,
        method: 'POST',
        data: $httpParamSerializerJQLike({KEYID:oasis.KEYID, LOCKED:1}),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).then(function(response){
        console.log(response);
        if(response.data.status==='success') {
          $scope.approvedMsg=true;
          $scope.enableQAEditBtn=false;
          $scope.enableQALockBtn=false;

          //edit form status to 3=approved;
          dataService.edit('form', {KEYID:oasis.FORMID, STATUS:3}).then(function (response) {
            console.log(response);
            $state.go('qa');
          });
        }
      }, function(data) {
          console.log(data);
        }
      );
    });
  };

  $scope.save = function() {
    console.log("save")
    var editOasisForm = {
      KEYID: $scope.oasisKEYID,
      M0020_PAT_ID: $scope.oasis.PATKEYID,
      //clinical record
      M0080_ASSESSOR_DISCIPLINE: $scope.M0080_ASSESSOR_DISCIPLINE,
      M0090_INFO_COMPLETED_DT: $filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd'),
      M0100_ASSMT_REASON: $scope.M0100_ASSMT_REASON,
      M1041_IN_INFLNZ_SEASON: $scope.M1041_IN_INFLNZ_SEASON,
      M1046_INFLNZ_RECD_CRNT_SEASON: $scope.M1046_INFLNZ_RECD_CRNT_SEASON,
      M1051_PVX_RCVD_AGNCY: $scope.M1051_PVX_RCVD_AGNCY,
      M1056_PVX_RSN_NOT_RCVD_AGNCY: $scope.M1056_PVX_RSN_NOT_RCVD_AGNCY,
      //cardiac
      M1501_SYMTM_HRT_FAILR_PTNTS: $scope.M1501_SYMTM_HRT_FAILR_PTNTS,
      M1511_HRT_FAILR_NO_ACTN: $scope.M1511_HRT_FAILR_NO_ACTN,
      M1511_HRT_FAILR_PHYSN_CNTCT: $scope.M1511_HRT_FAILR_PHYSN_CNTCT,
      M1511_HRT_FAILR_ER_TRTMT: $scope.M1511_HRT_FAILR_ER_TRTMT,
      M1511_HRT_FAILR_PHYSN_TRTMT: $scope.M1511_HRT_FAILR_PHYSN_TRTMT,
      M1511_HRT_FAILR_CLNCL_INTRVTN: $scope.M1511_HRT_FAILR_CLNCL_INTRVTN,
      M1511_HRT_FAILR_CARE_PLAN_CHG: $scope.M1511_HRT_FAILR_CARE_PLAN_CHG,
      M2005_MDCTN_INTRVTN: $scope.M2005_MDCTN_INTRVTN,
      M2016_DRUG_EDCTN_INTRVTN: $scope.M2016_DRUG_EDCTN_INTRVTN,
      //emergent care
      M2301_EMER_USE_AFTR_LAST_ASMT: $scope.M2301_EMER_USE_AFTR_LAST_ASMT,
      M2310_ECR_MEDICATION: $scope.M2310_ECR_MEDICATION,
      M2310_ECR_INJRY_BY_FALL: $scope.M2310_ECR_INJRY_BY_FALL,
      M2310_ECR_RSPRTRY_INFCTN: $scope.M2310_ECR_RSPRTRY_INFCTN,
      M2310_ECR_RSPRTRY_OTHR: $scope.M2310_ECR_RSPRTRY_OTHR,
      M2310_ECR_HRT_FAILR: $scope.M2310_ECR_HRT_FAILR,
      M2310_ECR_CRDC_DSRTHM: $scope.M2310_ECR_CRDC_DSRTHM,
      M2310_ECR_MI_CHST_PAIN: $scope.M2310_ECR_MI_CHST_PAIN,
      M2310_ECR_OTHR_HRT_DEASE: $scope.M2310_ECR_OTHR_HRT_DEASE,
      M2310_ECR_STROKE_TIA: $scope.M2310_ECR_STROKE_TIA,
      M2310_ECR_HYPOGLYC: $scope.M2310_ECR_HYPOGLYC,
      M2310_ECR_GI_PRBLM: $scope.M2310_ECR_GI_PRBLM,
      M2310_ECR_DHYDRTN_MALNTR: $scope.M2310_ECR_DHYDRTN_MALNTR,
      M2310_ECR_UTI: $scope.M2310_ECR_UTI,
      M2310_ECR_CTHTR_CMPLCTN: $scope.M2310_ECR_CTHTR_CMPLCTN,
      M2310_ECR_WND_INFCTN_DTRORTN: $scope.M2310_ECR_WND_INFCTN_DTRORTN,
      M2310_ECR_UNCNTLD_PAIN: $scope.M2310_ECR_UNCNTLD_PAIN,
      M2310_ECR_MENTL_BHVRL_PRBLM: $scope.M2310_ECR_MENTL_BHVRL_PRBLM,
      M2310_ECR_DVT_PULMNRY: $scope.M2310_ECR_DVT_PULMNRY,
      M2310_ECR_OTHER: $scope.M2310_ECR_OTHER,
      M2310_ECR_UNKNOWN: $scope.M2310_ECR_UNKNOWN,
      M2401_INTRVTN_SMRY_DBTS_FT: $scope.M2401_INTRVTN_SMRY_DBTS_FT,
      M2401_INTRVTN_SMRY_FALL_PRVNT: $scope.M2401_INTRVTN_SMRY_FALL_PRVNT,
      M2401_INTRVTN_SMRY_DPRSN: $scope.M2401_INTRVTN_SMRY_DPRSN,
      M2401_INTRVTN_SMRY_PAIN_MNTR: $scope.M2401_INTRVTN_SMRY_PAIN_MNTR,
      M2401_INTRVTN_SMRY_PRSULC_PRVN: $scope.M2401_INTRVTN_SMRY_PRSULC_PRVN,
      M2401_INTRVTN_SMRY_PRSULC_WET: $scope.M2401_INTRVTN_SMRY_PRSULC_WET,
      M2410_INPAT_FACILITY: $scope.M2410_INPAT_FACILITY,
      M2430_HOSP_MED:$scope.M2430_HOSP_MED,
      M2430_HOSP_INJRY_BY_FALL: $scope.M2430_HOSP_INJRY_BY_FALL,
      M2430_HOSP_RSPRTRY_INFCTN: $scope.M2430_HOSP_RSPRTRY_INFCTN,
      M2430_HOSP_RSPRTRY_OTHR: $scope.M2430_HOSP_RSPRTRY_OTHR,
      M2430_HOSP_HRT_FAILR: $scope.M2430_HOSP_HRT_FAILR,
      M2430_HOSP_CRDC_DSRTHM: $scope.M2430_HOSP_CRDC_DSRTHM,
      M2430_HOSP_MI_CHST_PAIN: $scope.M2430_HOSP_MI_CHST_PAIN,
      M2430_HOSP_OTHR_HRT_DEASE: $scope.M2430_HOSP_OTHR_HRT_DEASE,
      M2430_HOSP_STROKE_TIA: $scope.M2430_HOSP_STROKE_TIA,
      M2430_HOSP_HYPOGLYC: $scope.M2430_HOSP_HYPOGLYC,
      M2430_HOSP_GI_PRBLM: $scope.M2430_HOSP_GI_PRBLM,
      M2430_HOSP_DHYDRTN_MALNTR: $scope.M2430_HOSP_DHYDRTN_MALNTR,
      M2430_HOSP_UR_TRACT: $scope.M2430_HOSP_UR_TRACT,
      M2430_HOSP_CTHTR_CMPLCTN: $scope.M2430_HOSP_CTHTR_CMPLCTN,
      M2430_HOSP_WND_INFCTN: $scope.M2430_HOSP_WND_INFCTN,
      M2430_HOSP_PAIN: $scope.M2430_HOSP_PAIN,
      M2430_HOSP_MENTL_BHVRL_PRBLM: $scope.M2430_HOSP_MENTL_BHVRL_PRBLM,
      M2430_HOSP_DVT_PULMNRY: $scope.M2430_HOSP_DVT_PULMNRY,
      M2430_HOSP_SCHLD_TRTMT: $scope.M2430_HOSP_SCHLD_TRTMT,
      M2430_HOSP_OTHER: $scope.M2430_HOSP_OTHER,
      M2430_HOSP_UK: $scope.M2430_HOSP_UK,
      M0903_LAST_HOME_VISIT: $filter("date")($scope.M0903_LAST_HOME_VISIT, 'yyyy/MM/dd'),
      M0906_DC_TRAN_DTH_DT: $filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')
    };
    console.log("editOasisForm", editOasisForm);
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['oasisC2_2_20_1'].edit,
      method: 'POST',
      data: $httpParamSerializerJQLike(editOasisForm),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function(response){
      console.log(response);
      if(response.data.status==='success') {
        $scope.disableEditSOCDetails = true;
        $scope.enableInput = false;
        $scope.enableQALockBtn = true;
        $scope.enableQAEditBtn=true;
        $scope.enableQARejectBtn=true;
        $scope.enableCancelBtn=false;
        // editAdmit();
        // editPatient();

        //if transfer date has been changed
        if($scope.changeVisitDate) {
          console.log('changing discharge dates...');
          // editForm();
          dataService.edit('form', {KEYID:$scope.oasis.FORMID, FORMDATE:$filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')}).then(function (response) {
            console.log(response);
          });
          // editVisit();
          dataService.edit('visit', {KEYID:$scope.oasis.VISITID, VISITDATE:$filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')}).then(function (response) {
            console.log(response);
          });
        }
      }
    }, function(data) {
        console.log(data);
      }
    );
  };

  $scope.reject=function () {
    //change form status to in progress, and attach note?
    var form={
      KEYID: $scope.oasis.FORMID,
      STATUS: 4,
      // DETAIL1: 'Form has been rejected by QA'
    };
    console.log(form);
    dataService.edit('form', form).then(function (response) {
      console.log(response);
      $state.go('qa');
    });
  };
})
