app.config(function($stateProvider) {
  $stateProvider.state('viewOasisOtherFollowUp', {
    url: '/oasisC2/viewOasisOtherFollowUp',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/viewOasis/oasisC2-ICD10/viewOtherFollowUp/viewOtherFollowUp.html',
        controller: 'ViewOtherFollowUpController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
