app.controller('ViewOtherFollowUpController', function($scope, patientService,dataService) {

  $scope.patient = patientService.get();


  $scope.currentForm = 'ClinicalRecordItems';

  $scope.nextForm = function(current, destination) {
    if ($scope.currentForm != destination) {
      $scope.currentForm = destination;
      $('.' + current ).fadeOut('fast', function() {
        $('.' + destination ).fadeIn('fast');
      });
    }
  };

  $scope.activeForm = function(form) {
    if ($scope.currentForm == form) {
      return true;
    }
  };

  $scope.submitSection = function(form, destination) {
    console.log(form.$submitted);
    console.log(form.$valid);
    if(form.$valid) {
      $('#' + $scope.currentForm + ' i').css({color: 'green'});
      var section = $scope.currentForm + 'Complete';

      $scope.nextForm($scope.currentForm, destination);
      console.log(section);
      $scope[section] = true;
      console.log($scope[section]);
    }
    else {
      $('#' + $scope.currentForm + ' i').css({color: 'lightgrey'});
      console.log('Not Valid');
    }
  };

  dataService.get('oasisC2_2_20_1', {'M0020_PAT_ID': $scope.patient.KEYID, 'M0100_ASSMT_REASON': '05'}).then(function(data){
    // console.log(data)
    var oasis = data.data;
    oasis = oasis.slice(-1).pop();
    console.log(oasis)
    $scope.oasisKEYID = oasis.KEYID;

    //clinical record
    $scope.M0080_ASSESSOR_DISCIPLINE=oasis.M0080_ASSESSOR_DISCIPLINE;
    $scope.M0090_INFO_COMPLETED_DT=oasis.M0090_INFO_COMPLETED_DT;
    $scope.M0100_ASSMT_REASON=oasis.M0100_ASSMT_REASON;
    $scope.M0110_EPISODE_TIMING=oasis.M0110_EPISODE_TIMING;
    //patient history
    $scope.M1011_14_DAY_INP1_ICD=oasis.M1011_14_DAY_INP1_ICD;
    $scope.M1011_14_DAY_INP2_ICD=oasis.M1011_14_DAY_INP2_ICD;
    $scope.M1011_14_DAY_INP3_ICD=oasis.M1011_14_DAY_INP3_ICD;
    $scope.M1011_14_DAY_INP4_ICD=oasis.M1011_14_DAY_INP4_ICD;
    $scope.M1011_14_DAY_INP5_ICD=oasis.M1011_14_DAY_INP5_ICD;
    $scope.M1011_14_DAY_INP6_ICD=oasis.M1011_14_DAY_INP6_ICD;
    $scope.M1021_PRIMARY_DIAG_ICD=oasis.M1021_PRIMARY_DIAG_ICD;
    $scope.M1021_PRIMARY_DIAG_SEVERITY=oasis.M1021_PRIMARY_DIAG_SEVERITY;
    $scope.M1023_OTH_DIAG1_ICD=oasis.M1023_OTH_DIAG1_ICD;
    $scope.M1023_OTH_DIAG1_SEVERITY=oasis.M1023_OTH_DIAG1_SEVERITY;
    $scope.M1023_OTH_DIAG2_ICD=oasis.M1023_OTH_DIAG2_ICD;
    $scope.M1023_OTH_DIAG2_SEVERITY=oasis.M1023_OTH_DIAG2_SEVERITY;
    $scope.M1023_OTH_DIAG3_ICD=oasis.M1023_OTH_DIAG3_ICD;
    $scope.M1023_OTH_DIAG3_SEVERITY=oasis.M1023_OTH_DIAG3_SEVERITY;
    $scope.M1023_OTH_DIAG4_ICD=oasis.M1023_OTH_DIAG4_ICD;
    $scope.M1023_OTH_DIAG4_SEVERITY=oasis.M1023_OTH_DIAG4_SEVERITY;
    $scope.M1023_OTH_DIAG5_ICD=oasis.M1023_OTH_DIAG5_ICD;
    $scope.M1023_OTH_DIAG5_SEVERITY=oasis.M1023_OTH_DIAG5_SEVERITY;
    $scope.M1025_OPT_DIAG_ICD_A3=oasis.M1025_OPT_DIAG_ICD_A3;
    $scope.M1025_OPT_DIAG_ICD_A4=oasis.M1025_OPT_DIAG_ICD_A4;
    $scope.M1025_OPT_DIAG_ICD_B3=oasis.M1025_OPT_DIAG_ICD_B3;
    $scope.M1025_OPT_DIAG_ICD_B4=oasis.M1025_OPT_DIAG_ICD_B4;
    $scope.M1025_OPT_DIAG_ICD_C3=oasis.M1025_OPT_DIAG_ICD_C3;
    $scope.M1025_OPT_DIAG_ICD_C4=oasis.M1025_OPT_DIAG_ICD_C4;
    $scope.M1025_OPT_DIAG_ICD_D3=oasis.M1025_OPT_DIAG_ICD_D3;
    $scope.M1025_OPT_DIAG_ICD_D4=oasis.M1025_OPT_DIAG_ICD_D4;
    $scope.M1025_OPT_DIAG_ICD_E3=oasis.M1025_OPT_DIAG_ICD_E3;
    $scope.M1025_OPT_DIAG_ICD_E4=oasis.M1025_OPT_DIAG_ICD_E4;
    $scope.M1025_OPT_DIAG_ICD_F3=oasis.M1025_OPT_DIAG_ICD_F3;
    $scope.M1025_OPT_DIAG_ICD_F4=oasis.M1025_OPT_DIAG_ICD_F4;
    $scope.M1030_THH_IV_INFUSION=oasis.M1030_THH_IV_INFUSION;
    $scope.M1030_THH_PAR_NUTRITION=oasis.M1030_THH_PAR_NUTRITION;
    $scope.M1030_THH_ENT_NUTRITION=oasis.M1030_THH_ENT_NUTRITION;
    $scope.M1030_THH_NONE_ABOVE=oasis.M1030_THH_NONE_ABOVE;
    //sensory
    $scope.M1200_VISION=oasis.M1200_VISION;
    $scope.M1242_PAIN_FREQ_ACTVTY_MVMT=oasis.M1242_PAIN_FREQ_ACTVTY_MVMT;
    //integumentary
    $scope.M1306_UNHLD_STG2_PRSR_ULCR=oasis.M1306_UNHLD_STG2_PRSR_ULCR;
    $scope.M1311_NBR_PRSULC_STG2_A1=oasis.M1311_NBR_PRSULC_STG2_A1;
    $scope.M1311_NBR_ULC_SOCROC_STG2_A2=oasis.M1311_NBR_ULC_SOCROC_STG2_A2;
    $scope.M1311_NBR_PRSULC_STG3_B1=oasis.M1311_NBR_PRSULC_STG3_B1;
    $scope.M1311_NBR_ULC_SOCROC_STG3_B2=oasis.M1311_NBR_ULC_SOCROC_STG3_B2;
    $scope.M1311_NBR_PRSULC_STG4_C1=oasis.M1311_NBR_PRSULC_STG4_C1;
    $scope.M1311_NBR_ULC_SOCROC_STG4_C2=oasis.M1311_NBR_ULC_SOCROC_STG4_C2;
    $scope.M1311_NSTG_DRSG_D1=oasis.M1311_NSTG_DRSG_D1;
    $scope.M1311_NSTG_DRSG_SOCROC_D2=oasis.M1311_NSTG_DRSG_SOCROC_D2;
    $scope.M1311_NSTG_CVRG_E1=oasis.M1311_NSTG_CVRG_E1;
    $scope.M1311_NSTG_CVRG_SOCROC_E2=oasis.M1311_NSTG_CVRG_SOCROC_E2;
    $scope.M1311_NSTG_DEEP_TSUE_F1=oasis.M1311_NSTG_DEEP_TSUE_F1;
    $scope.M1311_NSTG_DEEP_TSUE_SOCROC_F2=oasis.M1311_NSTG_DEEP_TSUE_SOCROC_F2;
    $scope.M1322_NBR_PRSULC_STG1=oasis.M1322_NBR_PRSULC_STG1;
    $scope.M1324_STG_PRBLM_ULCER=oasis.M1324_STG_PRBLM_ULCER;
    $scope.M1330_STAS_ULCR_PRSNT=oasis.M1330_STAS_ULCR_PRSNT;
    $scope.M1332_NBR_STAS_ULCR=oasis.M1332_NBR_STAS_ULCR;
    $scope.M1334_STUS_PRBLM_STAS_ULCR=oasis.M1334_STUS_PRBLM_STAS_ULCR;
    $scope.M1340_SRGCL_WND_PRSNT=oasis.M1340_SRGCL_WND_PRSNT;
    $scope.M1342_STUS_PRBLM_SRGCL_WND=oasis.M1342_STUS_PRBLM_SRGCL_WND;
    //respiratory
    $scope.M1400_WHEN_DYSPNEIC=oasis.M1400_WHEN_DYSPNEIC;
    $scope.M1410_RESPTX_OXYGEN=oasis.M1410_RESPTX_OXYGEN;
    $scope.M1410_RESPTX_VENTILATOR=oasis.M1410_RESPTX_VENTILATOR;
    $scope.M1410_RESPTX_AIRPRESS=oasis.M1410_RESPTX_AIRPRESS;
    $scope.M1410_RESPTX_NONE=oasis.M1410_RESPTX_NONE;
    //elimination
    $scope.M1610_UR_INCONT=oasis.M1610_UR_INCONT;
    $scope.M1620_BWL_INCONT=oasis.M1620_BWL_INCONT;
    $scope.M1630_OSTOMY=oasis.M1630_OSTOMY;
    //ADL
    $scope.M1810_CRNT_DRESS_UPPER=oasis.M1810_CRNT_DRESS_UPPER;
    $scope.M1820_CRNT_DRESS_LOWER=oasis.M1820_CRNT_DRESS_LOWER;
    $scope.M1830_CRNT_BATHG=oasis.M1830_CRNT_BATHG;
    $scope.M1840_CRNT_TOILTG=oasis.M1840_CRNT_TOILTG;
    $scope.M1850_CRNT_TRNSFRNG=oasis.M1850_CRNT_TRNSFRNG;
    $scope.M1860_CRNT_AMBLTN=oasis.M1860_CRNT_AMBLTN;
    $scope.M2030_CRNT_MGMT_INJCTN_MDCTN=oasis.M2030_CRNT_MGMT_INJCTN_MDCTN;
    //therapy
    $scope.M2200_THER_NEED_NBR=oasis.M2200_THER_NEED_NBR;
    $scope.M2200_THER_NEED_NA=oasis.M2200_THER_NEED_NA;

    if (oasis.M0090_INFO_COMPLETED_DT !== null) {
      $scope.M0090_INFO_COMPLETED_DT = new Date(oasis.M0090_INFO_COMPLETED_DT);
    }
  })

  $scope.disableEditDetails = true;

  $scope.edit = function(){
    $scope.disableEditDetails = false;
    $scope.enableInput = true;
  }
  $scope.save = function() {
    console.log("save")
    var editOasisOtherForm = {
      KEYID: $scope.oasisKEYID,
      M0020_PAT_ID: $scope.patient.KEYID,
      //clinical record
      M0080_ASSESSOR_DISCIPLINE: $scope.M0080_ASSESSOR_DISCIPLINE,
      M0090_INFO_COMPLETED_DT: $scope.M0090_INFO_COMPLETED_DT,
      M0100_ASSMT_REASON: $scope.M0100_ASSMT_REASON,
      M0110_EPISODE_TIMING: $scope.M0110_EPISODE_TIMING,
      //patient history
      M1011_14_DAY_INP1_ICD: $scope.M1011_14_DAY_INP1_ICD,
      M1011_14_DAY_INP2_ICD: $scope.M1011_14_DAY_INP2_ICD,
      M1011_14_DAY_INP3_ICD: $scope.M1011_14_DAY_INP3_ICD,
      M1011_14_DAY_INP4_ICD: $scope.M1011_14_DAY_INP4_ICD,
      M1011_14_DAY_INP5_ICD: $scope.M1011_14_DAY_INP5_ICD,
      M1011_14_DAY_INP6_ICD: $scope.M1011_14_DAY_INP6_ICD,
      M1011_14_DAY_INP_NA: $scope.M1011_14_DAY_INP_NA,
      M1021_PRIMARY_DIAG_ICD: $scope.M1021_PRIMARY_DIAG_ICD,
      M1021_PRIMARY_DIAG_SEVERITY: $scope.M1021_PRIMARY_DIAG_SEVERITY,
      M1023_OTH_DIAG1_ICD: $scope.M1023_OTH_DIAG1_ICD,
      M1023_OTH_DIAG1_SEVERITY: $scope.M1023_OTH_DIAG1_SEVERITY,
      M1023_OTH_DIAG2_ICD: $scope.M1023_OTH_DIAG2_ICD,
      M1023_OTH_DIAG2_SEVERITY: $scope.M1023_OTH_DIAG2_SEVERITY,
      M1023_OTH_DIAG3_ICD: $scope.M1023_OTH_DIAG3_ICD,
      M1023_OTH_DIAG3_SEVERITY: $scope.M1023_OTH_DIAG3_SEVERITY,
      M1023_OTH_DIAG4_ICD: $scope.M1023_OTH_DIAG4_ICD,
      M1023_OTH_DIAG4_SEVERITY: $scope.M1023_OTH_DIAG4_SEVERITY,
      M1023_OTH_DIAG5_ICD: $scope.M1023_OTH_DIAG5_ICD,
      M1023_OTH_DIAG5_SEVERITY: $scope.M1023_OTH_DIAG5_SEVERITY,
      M1025_OPT_DIAG_ICD_A3: $scope.M1025_OPT_DIAG_ICD_A3,
      M1025_OPT_DIAG_ICD_A4: $scope.M1025_OPT_DIAG_ICD_A4,
      M1025_OPT_DIAG_ICD_B3: $scope.M1025_OPT_DIAG_ICD_B3,
      M1025_OPT_DIAG_ICD_B4: $scope.M1025_OPT_DIAG_ICD_B4,
      M1025_OPT_DIAG_ICD_C3: $scope.M1025_OPT_DIAG_ICD_C3,
      M1025_OPT_DIAG_ICD_C4: $scope.M1025_OPT_DIAG_ICD_C4,
      M1025_OPT_DIAG_ICD_D3: $scope.M1025_OPT_DIAG_ICD_D3,
      M1025_OPT_DIAG_ICD_D4: $scope.M1025_OPT_DIAG_ICD_D4,
      M1025_OPT_DIAG_ICD_E3: $scope.M1025_OPT_DIAG_ICD_E3,
      M1025_OPT_DIAG_ICD_E4: $scope.M1025_OPT_DIAG_ICD_E4,
      M1025_OPT_DIAG_ICD_F3: $scope.M1025_OPT_DIAG_ICD_F3,
      M1025_OPT_DIAG_ICD_F4: $scope.M1025_OPT_DIAG_ICD_F4,
      M1030_THH_IV_INFUSION: $scope.M1030_THH_IV_INFUSION,
      M1030_THH_PAR_NUTRITION: $scope.M1030_THH_PAR_NUTRITION,
      M1030_THH_ENT_NUTRITION: $scope.M1030_THH_ENT_NUTRITION,
      M1030_THH_NONE_ABOVE: $scope.M1030_THH_NONE_ABOVE,
      //sensory
      M1200_VISION: $scope.M1200_VISION,
      M1242_PAIN_FREQ_ACTVTY_MVMT: $scope.M1242_PAIN_FREQ_ACTVTY_MVMT,
      //integumentary
      M1306_UNHLD_STG2_PRSR_ULCR: $scope.M1306_UNHLD_STG2_PRSR_ULCR,
      M1311_NBR_PRSULC_STG2_A1: $scope.M1311_NBR_PRSULC_STG2_A1,
      M1311_NBR_ULC_SOCROC_STG2_A2: $scope.M1311_NBR_ULC_SOCROC_STG2_A2,
      M1311_NBR_PRSULC_STG3_B1: $scope.M1311_NBR_PRSULC_STG3_B1,
      M1311_NBR_ULC_SOCROC_STG3_B2: $scope.M1311_NBR_ULC_SOCROC_STG3_B2,
      M1311_NBR_PRSULC_STG4_C1: $scope.M1311_NBR_PRSULC_STG4_C1,
      M1311_NBR_ULC_SOCROC_STG4_C2: $scope.M1311_NBR_ULC_SOCROC_STG4_C2,
      M1311_NSTG_DRSG_D1: $scope.M1311_NSTG_DRSG_D1,
      M1311_NSTG_DRSG_SOCROC_D2: $scope.M1311_NSTG_DRSG_SOCROC_D2,
      M1311_NSTG_CVRG_E1: $scope.M1311_NSTG_CVRG_E1,
      M1311_NSTG_CVRG_SOCROC_E2: $scope.M1311_NSTG_CVRG_SOCROC_E2,
      M1311_NSTG_DEEP_TSUE_F1: $scope.M1311_NSTG_DEEP_TSUE_F1,
      M1311_NSTG_DEEP_TSUE_SOCROC_F2: $scope.M1311_NSTG_DEEP_TSUE_SOCROC_F2,
      M1322_NBR_PRSULC_STG1: $scope.M1322_NBR_PRSULC_STG1,
      M1324_STG_PRBLM_ULCER: $scope.M1324_STG_PRBLM_ULCER,
      M1330_STAS_ULCR_PRSNT: $scope.M1330_STAS_ULCR_PRSNT,
      M1332_NBR_STAS_ULCR: $scope.M1332_NBR_STAS_ULCR,
      M1334_STUS_PRBLM_STAS_ULCR: $scope.M1334_STUS_PRBLM_STAS_ULCR,
      M1340_SRGCL_WND_PRSNT: $scope.M1340_SRGCL_WND_PRSNT,
      M1342_STUS_PRBLM_SRGCL_WND: $scope.M1342_STUS_PRBLM_SRGCL_WND,
      //respiratory
      M1400_WHEN_DYSPNEIC: $scope.M1400_WHEN_DYSPNEIC,
      M1410_RESPTX_OXYGEN: $scope.M1410_RESPTX_OXYGEN,
      M1410_RESPTX_VENTILATOR: $scope.M1410_RESPTX_VENTILATOR,
      M1410_RESPTX_AIRPRESS: $scope.M1410_RESPTX_AIRPRESS,
      M1410_RESPTX_NONE: $scope.M1410_RESPTX_NONE,
      //elimination
      M1610_UR_INCONT: $scope.M1610_UR_INCONT,
      M1620_BWL_INCONT: $scope.M1620_BWL_INCONT,
      M1630_OSTOMY: $scope.M1630_OSTOMY,
      //behavioural
      //ADL
      M1810_CRNT_DRESS_UPPER: $scope.M1810_CRNT_DRESS_UPPER,
      M1820_CRNT_DRESS_LOWER: $scope.M1820_CRNT_DRESS_LOWER,
      M1830_CRNT_BATHG: $scope.M1830_CRNT_BATHG,
      M1840_CRNT_TOILTG: $scope.M1840_CRNT_TOILTG,
      M1850_CRNT_TRNSFRNG: $scope.M1850_CRNT_TRNSFRNG,
      M1860_CRNT_AMBLTN: $scope.M1860_CRNT_AMBLTN,
      //medication
      M2030_CRNT_MGMT_INJCTN_MDCTN: $scope.M2030_CRNT_MGMT_INJCTN_MDCTN,
      //therapy
      M2200_THER_NEED_NBR: $scope.M2200_THER_NEED_NBR,
      M2200_THER_NEED_NA: $scope.M2200_THER_NEED_NA,
    };
    console.log("editOasisOtherForm", editOasisOtherForm);

    dataService.edit('oasisC2_2_20_1', editOasisOtherForm).then(function(response){
      console.log(response);
      $scope.disableEditDetails = true;
      $scope.enableInput = false;
    });
  };
})
