app.config(function($stateProvider) {
  $stateProvider.state('viewOasisResumptionOfCare', {
    url: '/oasisC2/viewResumptionOfCare',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/viewOasis/oasisC2-ICD10/viewResumptionOfCare/viewResumptionOfCare.html',
        controller: 'ViewResumptionOfCareController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
