app.config(function($stateProvider) {
  $stateProvider.state('viewOasisTransferAndDischarge', {
    url: '/oasisC2/viewOasisTransferAndDischarge',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/viewOasis/oasisC2-ICD10/viewTransferAndDischarge/viewTransferAndDischarge.html',
        controller: 'ViewTransferAndDischargeController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
