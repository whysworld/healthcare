app.controller('ViewDischargeController', function($scope, $http, CONSTANTS, $httpParamSerializerJQLike, $state, $filter, patientService,dataService, formService) {

  $scope.patient = patientService.get();


  $scope.currentForm = 'General';

  $scope.nextForm = function(current, destination) {
    if ($scope.currentForm != destination) {
      $scope.currentForm = destination;
      $('.' + current ).fadeOut('fast', function() {
        $('.' + destination ).fadeIn('fast');
      });
    }
  };

  $scope.activeForm = function(form) {
    if ($scope.currentForm == form) {
      return true;
    }
  };

  $scope.submitSection = function(form, destination) {
    console.log(form.$submitted);
    console.log(form.$valid);
    if(form.$valid) {
      $('#' + $scope.currentForm + ' i').css({color: 'green'});
      var section = $scope.currentForm + 'Complete';

      $scope.nextForm($scope.currentForm, destination);
      console.log(section);
      $scope[section] = true;
      console.log($scope[section]);
    }
    else {
      $('#' + $scope.currentForm + ' i').css({color: 'lightgrey'});
      console.log('Not Valid');
    }
  };

  //alerting msg if M0906 date is changed and different from form date
  $scope.checkDate=function () {
    if ($filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd') !==  $filter("date")($scope.oasis.FORMDATE, 'yyyy/MM/dd')) {
      alert("Note: Changing M0906 will automatically change date of visit.");
      $scope.changeVisitDate=true;
    } else {
      $scope.changeVisitDate=false;
    }
  };


  getOasis();
  function getOasis() {
    var oasis = formService.get();
    $scope.oasis=oasis;
    console.log(oasis);

    //show qa buttons if ACTION=QAREVIEW;
    if($scope.oasis.ACTION!==undefined){ //redirected from QA page
      console.log('enabling qa mode');
      //show/hide approved msg
      if (oasis.LOCKED===1){
        $scope.approvedMsg=true; //form is locked, show approved msg
        $scope.enableQAEditBtn=false;
        $scope.enableQALockBtn=false;
        $scope.enableQARejectBtn=false;
        // $scope.disableEditSOCDetails=false;
      } else {
        $scope.approvedMsg=false; //form is not locked, allow editing
        $scope.enableQAEditBtn=true;
        $scope.enableQALockBtn=true;
        $scope.enableQARejectBtn=true;
        // $scope.disableEditSOCDetails=true;
      }
    } else { //redirected from records page
      if ( oasis.LOCKED===1)  { //form is locked, show approved msg
        $scope.approvedMsg=true;
        $scope.enableQAEditBtn=false;
        $scope.enableQALockBtn=false;
        $scope.enableQARejectBtn=false;
        $scope.enableCancelBtn=false;

      } else { //form is not locked, show in review msg
        $scope.inReviewMsg=true;
        $scope.enableQAEditBtn=false;
        $scope.enableQALockBtn=false;
        $scope.enableQARejectBtn=false;
        $scope.enableCancelBtn=false;
      }
    }

    if (oasis.M0090_INFO_COMPLETED_DT !== null) {
      $scope.M0090_INFO_COMPLETED_DT = new Date($filter("date")(oasis.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd'))
    }
    if (oasis.M1307_OLDST_STG2_ONST_DT !== null) {
      $scope.M1307_OLDST_STG2_ONST_DT = new Date($filter("date")(oasis.M1307_OLDST_STG2_ONST_DT, 'yyyy/MM/dd'))
    }
    if (oasis.M0903_LAST_HOME_VISIT !== null) {
      $scope.M0903_LAST_HOME_VISIT = new Date($filter("date")(oasis.M0903_LAST_HOME_VISIT, 'yyyy/MM/dd'))
    }
    if (oasis.M0906_DC_TRAN_DTH_DT !== null) {
      $scope.M0906_DC_TRAN_DTH_DT = new Date($filter("date")(oasis.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd'))
    }

    $scope.oasisKEYID=oasis.KEYID;

    console.log('oasis', oasis);
    //******** GENERAL ********//
    $scope.M0010_CCN=oasis.M0010_CCN;
    $scope.M0014_BRANCH_STATE=oasis.M0014_BRANCH_STATE;
    $scope.M0016_BRANCH_ID=oasis.M0016_BRANCH_ID;
    $scope.M0018_PHYSICIAN_ID=oasis.M0018_PHYSICIAN_ID;
    $scope.M0018_PHYSICIAN_UK=oasis.M0018_PHYSICIAN_UK;
    $scope.M0020_PAT_ID=oasis.M0020_PAT_ID;
    $scope.M0030_START_CARE_DT=new Date($filter("date")(oasis.M0030_START_CARE_DT, 'yyyy/MM/dd'));
    $scope.M0032_ROC_DT=new Date($filter("date")(oasis.M0032_ROC_DT, 'yyyy/MM/dd'));
    $scope.M0032_ROC_DT_NA=oasis.M0032_ROC_DT_NA;
    $scope.M0040_PAT_FNAME=oasis.M0040_PAT_FNAME;
    $scope.M0040_PAT_MI=oasis.M0040_PAT_MI;
    $scope.M0040_PAT_LNAME=oasis.M0040_PAT_LNAME;
    $scope.M0040_PAT_SUFFIX=oasis.M0040_PAT_SUFFIX;
    $scope.M0050_PAT_ST=oasis.M0050_PAT_ST;
    $scope.M0060_PAT_ZIP=oasis.M0060_PAT_ZIP;
    $scope.M0063_MEDICARE_NUM=oasis.M0063_MEDICARE_NUM;
    $scope.M0063_MEDICARE_NA=oasis.M0063_MEDICARE_NA;
    $scope.M0064_SSN=oasis.M0064_SSN;
    $scope.M0064_SSN_UK=oasis.M0064_SSN_UK;
    $scope.M0065_MEDICAID_NUM=oasis.M0065_MEDICAID_NUM;
    $scope.M0065_MEDICAID_NA=oasis.M0065_MEDICAID_NA;
    $scope.M0066_PAT_BIRTH_DT=new Date($filter("date")(oasis.M0066_PAT_BIRTH_DT, 'yyyy/MM/dd'));
    $scope.M0069_PAT_GENDER=oasis.M0069_PAT_GENDER;
    $scope.M0090_INFO_COMPLETED_DT=new Date($filter("date")(oasis.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd'));
    $scope.M0140_ETHNIC_AI_AN=oasis.M0140_ETHNIC_AI_AN;
    $scope.M0140_ETHNIC_ASIAN=oasis.M0140_ETHNIC_ASIAN;
    $scope.M0140_ETHNIC_BLACK=oasis.M0140_ETHNIC_BLACK;
    $scope.M0140_ETHNIC_HISP=oasis.M0140_ETHNIC_HISP;
    $scope.M0140_ETHNIC_NH_PI=oasis.M0140_ETHNIC_NH_PI;
    $scope.M0140_ETHNIC_WHITE=oasis.M0140_ETHNIC_WHITE;
    $scope.M0150_CPAY_NONE=oasis.M0150_CPAY_NONE;
    $scope.M0150_CPAY_MCARE_FFS=oasis.M0150_CPAY_MCARE_FFS;
    $scope.M0150_CPAY_MCARE_HMO=oasis.M0150_CPAY_MCARE_HMO;
    $scope.M0150_CPAY_MCAID_FFS=oasis.M0150_CPAY_MCAID_FFS;
    $scope.M0150_CPAY_MCAID_HMO=oasis.M0150_CPAY_MCAID_HMO;
    $scope.M0150_CPAY_WRKCOMP=oasis.M0150_CPAY_WRKCOMP;
    $scope.M0150_CPAY_TITLEPGMS=oasis.M0150_CPAY_TITLEPGMS;
    $scope.M0150_CPAY_OTH_GOVT=oasis.M0150_CPAY_OTH_GOVT;
    $scope.M0150_CPAY_PRIV_INS=oasis.M0150_CPAY_PRIV_INS;
    $scope.M0150_CPAY_PRIV_HMO=oasis.M0150_CPAY_PRIV_HMO;
    $scope.M0150_CPAY_SELFPAY=oasis.M0150_CPAY_SELFPAY;
    $scope.M0150_CPAY_OTHER=oasis.M0150_CPAY_OTHER;
    $scope.M0150_CPAY_UK=oasis.M0150_CPAY_UK;

    //clinical record
    $scope.M0080_ASSESSOR_DISCIPLINE=oasis.M0080_ASSESSOR_DISCIPLINE;
    // $scope.M0090_INFO_COMPLETED_DT=oasis.M0090_INFO_COMPLETED_DT;
    $scope.M0100_ASSMT_REASON=oasis.M0100_ASSMT_REASON;
    $scope.M1041_IN_INFLNZ_SEASON=oasis.M1041_IN_INFLNZ_SEASON;
    $scope.M1046_INFLNZ_RECD_CRNT_SEASON=oasis.M1046_INFLNZ_RECD_CRNT_SEASON;
    $scope.M1051_PVX_RCVD_AGNCY=oasis.M1051_PVX_RCVD_AGNCY;
    $scope.M1056_PVX_RSN_NOT_RCVD_AGNCY=oasis.M1056_PVX_RSN_NOT_RCVD_AGNCY;
    $scope.M1230_SPEECH=oasis.M1230_SPEECH;
    $scope.M1242_PAIN_FREQ_ACTVTY_MVMT=oasis.M1242_PAIN_FREQ_ACTVTY_MVMT;
    //integumentary
    $scope.M1306_UNHLD_STG2_PRSR_ULCR=oasis.M1306_UNHLD_STG2_PRSR_ULCR;
    $scope.M1307_OLDST_STG2_ONST_DT=oasis.M1307_OLDST_STG2_ONST_DT;
    $scope.M1307_OLDST_STG2_AT_DSCHRG=oasis.M1307_OLDST_STG2_AT_DSCHRG;
    $scope.M1311_NBR_PRSULC_STG2_A1=oasis.M1311_NBR_PRSULC_STG2_A1;
    $scope.M1311_NBR_ULC_SOCROC_STG2_A2=oasis.M1311_NBR_ULC_SOCROC_STG2_A2;
    $scope.M1311_NBR_PRSULC_STG3_B1=oasis.M1311_NBR_PRSULC_STG3_B1;
    $scope.M1311_NBR_ULC_SOCROC_STG3_B2=oasis.M1311_NBR_ULC_SOCROC_STG3_B2;
    $scope.M1311_NBR_PRSULC_STG4_C1=oasis.M1311_NBR_PRSULC_STG4_C1;
    $scope.M1311_NBR_ULC_SOCROC_STG4_C2=oasis.M1311_NBR_ULC_SOCROC_STG4_C2;
    $scope.M1311_NSTG_DRSG_D1=oasis.M1311_NSTG_DRSG_D1;
    $scope.M1311_NSTG_DRSG_SOCROC_D2=oasis.M1311_NSTG_DRSG_SOCROC_D2;
    $scope.M1311_NSTG_CVRG_E1=oasis.M1311_NSTG_CVRG_E1;
    $scope.M1311_NSTG_CVRG_SOCROC_E2=oasis.M1311_NSTG_CVRG_SOCROC_E2;
    $scope.M1311_NSTG_DEEP_TSUE_F1=oasis.M1311_NSTG_DEEP_TSUE_F1;
    $scope.M1311_NSTG_DEEP_TSUE_SOCROC_F2=oasis.M1311_NSTG_DEEP_TSUE_SOCROC_F2;
    $scope.M1313_NW_WS_PRSULC_STG2_A=oasis.M1313_NW_WS_PRSULC_STG2_A;
    $scope.M1313_NW_WS_PRSULC_STG3_B=oasis.M1313_NW_WS_PRSULC_STG3_B;
    $scope.M1313_NW_WS_PRSULC_STG4_C=oasis.M1313_NW_WS_PRSULC_STG4_C;
    $scope.M1313_NW_WS_PRSULC_NSTG_DRSG_D=oasis.M1313_NW_WS_PRSULC_NSTG_DRSG_D;
    $scope.M1313_NW_WS_PRSULC_NSTG_CVRG_E=oasis.M1313_NW_WS_PRSULC_NSTG_CVRG_E;
    $scope.M1313_NW_WS_PRSULC_NSTG_TSUE_F=oasis.M1313_NW_WS_PRSULC_NSTG_TSUE_F;
    $scope.M1320_STUS_PRBLM_PRSR_ULCR=oasis.M1320_STUS_PRBLM_PRSR_ULCR;
    $scope.M1322_NBR_PRSULC_STG1=oasis.M1322_NBR_PRSULC_STG1;
    $scope.M1324_STG_PRBLM_ULCER=oasis.M1324_STG_PRBLM_ULCER;
    $scope.M1330_STAS_ULCR_PRSNT=oasis.M1330_STAS_ULCR_PRSNT;
    $scope.M1332_NBR_STAS_ULCR=oasis.M1332_NBR_STAS_ULCR;
    $scope.M1334_STUS_PRBLM_STAS_ULCR=oasis.M1334_STUS_PRBLM_STAS_ULCR;
    $scope.M1340_SRGCL_WND_PRSNT=oasis.M1340_SRGCL_WND_PRSNT;
    $scope.M1342_STUS_PRBLM_SRGCL_WND=oasis.M1342_STUS_PRBLM_SRGCL_WND;
    //respiratory
    $scope.M1400_WHEN_DYSPNEIC=oasis.M1400_WHEN_DYSPNEIC;
    //cardiac
    $scope.M1501_SYMTM_HRT_FAILR_PTNTS=oasis.M1501_SYMTM_HRT_FAILR_PTNTS;
    $scope.M1511_HRT_FAILR_NO_ACTN=oasis.M1511_HRT_FAILR_NO_ACTN;
    $scope.M1511_HRT_FAILR_PHYSN_CNTCT=oasis.M1511_HRT_FAILR_PHYSN_CNTCT;
    $scope.M1511_HRT_FAILR_ER_TRTMT=oasis.M1511_HRT_FAILR_ER_TRTMT;
    $scope.M1511_HRT_FAILR_PHYSN_TRTMT=oasis.M1511_HRT_FAILR_PHYSN_TRTMT;
    $scope.M1511_HRT_FAILR_CLNCL_INTRVTN=oasis.M1511_HRT_FAILR_CLNCL_INTRVTN;
    $scope.M1511_HRT_FAILR_CARE_PLAN_CHG=oasis.M1511_HRT_FAILR_CARE_PLAN_CHG;
    //elimination
    $scope.M1600_UTI=oasis.M1600_UTI;
    $scope.M1610_UR_INCONT=oasis.M1610_UR_INCONT;
    $scope.M1615_INCNTNT_TIMING=oasis.M1615_INCNTNT_TIMING;
    $scope.M1620_BWL_INCONT=oasis.M1620_BWL_INCONT;
    //behavioural
    $scope.M1700_COG_FUNCTION=oasis.M1700_COG_FUNCTION;
    $scope.M1710_WHEN_CONFUSED=oasis.M1710_WHEN_CONFUSED;
    $scope.M1720_WHEN_ANXIOUS=oasis.M1720_WHEN_ANXIOUS;
    $scope.M1740_BD_MEM_DEFICIT=oasis.M1740_BD_MEM_DEFICIT ;
    $scope.M1740_BD_IMP_DECISN=oasis.M1740_BD_IMP_DECISN;
    $scope.M1740_BD_VERBAL=oasis.M1740_BD_VERBAL;
    $scope.M1740_BD_PHYSICAL=oasis.M1740_BD_PHYSICAL;
    $scope.M1740_BD_SOC_INAPPRO=oasis.M1740_BD_SOC_INAPPRO;
    $scope.M1740_BD_DELUSIONS=oasis.M1740_BD_DELUSIONS;
    $scope.M1740_BD_NONE=oasis.M1740_BD_NONE;
    $scope.M1745_BEH_PROB_FREQ=oasis.M1745_BEH_PROB_FREQ;
    //ADL
    $scope.M1800_CRNT_GROOMING=oasis.M1800_CRNT_GROOMING;
    $scope.M1810_CRNT_DRESS_UPPER=oasis.M1810_CRNT_DRESS_UPPER;
    $scope.M1820_CRNT_DRESS_LOWER=oasis.M1820_CRNT_DRESS_LOWER;
    $scope.M1830_CRNT_BATHG=oasis.M1830_CRNT_BATHG;
    $scope.M1840_CRNT_TOILTG=oasis.M1840_CRNT_TOILTG;
    $scope.M1845_CRNT_TOILTG_HYGN=oasis.M1845_CRNT_TOILTG_HYGN ;
    $scope.M1850_CRNT_TRNSFRNG=oasis.M1850_CRNT_TRNSFRNG;
    $scope.M1860_CRNT_AMBLTN=oasis.M1860_CRNT_AMBLTN;
    $scope.M1870_CRNT_FEEDING=oasis.M1870_CRNT_FEEDING;
    $scope.M1880_CRNT_PREP_LT_MEALS=oasis.M1880_CRNT_PREP_LT_MEALS;
    $scope.M1890_CRNT_PHONE_USE=oasis.M1890_CRNT_PHONE_USE;
    //medication
    $scope.M2005_MDCTN_INTRVTN=oasis.M2005_MDCTN_INTRVTN;
    $scope.M2016_DRUG_EDCTN_INTRVTN=oasis.M2016_DRUG_EDCTN_INTRVTN;
    $scope.M2020_CRNT_MGMT_ORAL_MDCTN=oasis.M2020_CRNT_MGMT_ORAL_MDCTN;
    $scope.M2030_CRNT_MGMT_INJCTN_MDCTN=oasis.M2030_CRNT_MGMT_INJCTN_MDCTN;
    //careManagement
    $scope.M2102_CARE_TYPE_SRC_ADL=oasis.M2102_CARE_TYPE_SRC_ADL;
    $scope.M2102_CARE_TYPE_SRC_ADVCY=oasis.M2102_CARE_TYPE_SRC_ADVCY;
    $scope.M2102_CARE_TYPE_SRC_EQUIP=oasis.M2102_CARE_TYPE_SRC_EQUIP;
    $scope.M2102_CARE_TYPE_SRC_IADL=oasis.M2102_CARE_TYPE_SRC_IADL;
    $scope.M2102_CARE_TYPE_SRC_MDCTN=oasis.M2102_CARE_TYPE_SRC_MDCTN;
    $scope.M2102_CARE_TYPE_SRC_PRCDR=oasis.M2102_CARE_TYPE_SRC_PRCDR;
    $scope.M2102_CARE_TYPE_SRC_SPRVSN=oasis.M2102_CARE_TYPE_SRC_SPRVSN;
    //emergent care
    $scope.M2301_EMER_USE_AFTR_LAST_ASMT=oasis.M2301_EMER_USE_AFTR_LAST_ASMT;
    $scope.M2310_ECR_MEDICATION=oasis.M2310_ECR_MEDICATION;
    $scope.M2310_ECR_INJRY_BY_FALL=oasis.M2310_ECR_INJRY_BY_FALL;
    $scope.M2310_ECR_RSPRTRY_INFCTN=oasis.M2310_ECR_RSPRTRY_INFCTN;
    $scope.M2310_ECR_RSPRTRY_OTHR=oasis.M2310_ECR_RSPRTRY_OTHR;
    $scope.M2310_ECR_HRT_FAILR=oasis.M2310_ECR_HRT_FAILR;
    $scope.M2310_ECR_CRDC_DSRTHM=oasis.M2310_ECR_CRDC_DSRTHM;
    $scope.M2310_ECR_MI_CHST_PAIN=oasis.M2310_ECR_MI_CHST_PAIN;
    $scope.M2310_ECR_OTHR_HRT_DEASE=oasis.M2310_ECR_OTHR_HRT_DEASE;
    $scope.M2310_ECR_STROKE_TIA=oasis.M2310_ECR_STROKE_TIA;
    $scope.M2310_ECR_HYPOGLYC=oasis.M2310_ECR_HYPOGLYC;
    $scope.M2310_ECR_GI_PRBLM=oasis.M2310_ECR_GI_PRBLM;
    $scope.M2310_ECR_DHYDRTN_MALNTR=oasis.M2310_ECR_DHYDRTN_MALNTR;
    $scope.M2310_ECR_UTI=oasis.M2310_ECR_UTI;
    $scope.M2310_ECR_CTHTR_CMPLCTN=oasis.M2310_ECR_CTHTR_CMPLCTN;
    $scope.M2310_ECR_WND_INFCTN_DTRORTN=oasis.M2310_ECR_WND_INFCTN_DTRORTN;
    $scope.M2310_ECR_UNCNTLD_PAIN=oasis.M2310_ECR_UNCNTLD_PAIN;
    $scope.M2310_ECR_MENTL_BHVRL_PRBLM=oasis.M2310_ECR_MENTL_BHVRL_PRBLM;
    $scope.M2310_ECR_DVT_PULMNRY=oasis.M2310_ECR_DVT_PULMNRY;
    $scope.M2310_ECR_OTHER=oasis.M2310_ECR_OTHER;
    $scope.M2310_ECR_UNKNOWN=oasis.M2310_ECR_UNKNOWN;
    $scope.M2401_INTRVTN_SMRY_DBTS_FT=oasis.M2401_INTRVTN_SMRY_DBTS_FT;
    $scope.M2401_INTRVTN_SMRY_FALL_PRVNT=oasis.M2401_INTRVTN_SMRY_FALL_PRVNT;
    $scope.M2401_INTRVTN_SMRY_DPRSN=oasis.M2401_INTRVTN_SMRY_DPRSN;
    $scope.M2401_INTRVTN_SMRY_PAIN_MNTR=oasis.M2401_INTRVTN_SMRY_PAIN_MNTR;
    $scope.M2401_INTRVTN_SMRY_PRSULC_PRVN=oasis.M2401_INTRVTN_SMRY_PRSULC_PRVN;
    $scope.M2401_INTRVTN_SMRY_PRSULC_WET=oasis.M2401_INTRVTN_SMRY_PRSULC_WET;
    $scope.M2410_INPAT_FACILITY=oasis.M2410_INPAT_FACILITY;
    $scope.M2420_DSCHRG_DISP=oasis.M2420_DSCHRG_DISP;
    $scope.M0903_LAST_HOME_VISIT=new Date($filter("date")(oasis.M0903_LAST_HOME_VISIT, 'yyyy/MM/dd'));
  }

  $scope.disableEditDetails = true;

  $scope.edit = function(){
    // $scope.disableEditDetails = false;
    // $scope.enableInput = true;
    // $scope.enableQALockBtn=false;
    // $scope.enableQAEditBtn=false;
    // $scope.enableQARejectBtn=false;
    // $scope.enableCancelBtn=true;
    var form = {
      ADMITKEYID: $scope.oasis.ADMITKEYID,
      EPIKEYID: $scope.oasis.EPIKEYID,
      FORMID: $scope.oasis.FORMID,
      OBJKEYID: $scope.oasis.OBJKEYID,
      VISITDATE: $scope.oasis.FORMDATE,
      VISITID: $scope.oasis.VISITID,
      ACTION: 'QAEDIT',
    };
    formService.set(form);
    // console.log($scope.oasis)
    $state.go('discharge')
  };

  $scope.cancel=function () {
    $scope.disableEditDetails = true;
    $scope.enableInput = false;
    $scope.enableQALockBtn = true;
    $scope.enableQAEditBtn=true;
    $scope.enableQARejectBtn=true;
    $scope.enableCancelBtn=false;
    getOasis();
  };


  //form approved, oasis LOCKED=1
  $scope.lock=function () {
    //reset current oasis
    dataService.get('oasisC2_2_20_1', {KEYID:$scope.oasisKEYID}).then(function (data) {
      // console.log(data.data[0]);
      var oasis=data.data[0];
      // formService.set(data.data[0])
      console.log(oasis);
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['oasisC2_2_20_1'].edit,
        method: 'POST',
        data: $httpParamSerializerJQLike({KEYID:oasis.KEYID, LOCKED:1}),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).then(function(response){
        console.log(response);
        if(response.data.status==='success') {
          $scope.approvedMsg=true;
          $scope.enableQAEditBtn=false;
          $scope.enableQALockBtn=false;

          //edit form status to 3=approved;
          dataService.edit('form', {KEYID:oasis.FORMID, STATUS:3}).then(function (response) {
            console.log(response);
            $state.go('qa');
          });
        }
      }, function(data) {
          console.log(data);
        }
      );
    });
  };

  $scope.save = function() {
    console.log("save");
    var editOasisOtherForm = {
      KEYID: $scope.oasisKEYID,
      M0020_PAT_ID: $scope.oasis.PATKEYID,
      //clinical record
      M0080_ASSESSOR_DISCIPLINE: $scope.M0080_ASSESSOR_DISCIPLINE,
      M0090_INFO_COMPLETED_DT: $filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd'),
      M0100_ASSMT_REASON: '09',
      M1041_IN_INFLNZ_SEASON: $scope.M1041_IN_INFLNZ_SEASON,
      M1046_INFLNZ_RECD_CRNT_SEASON: $scope.M1046_INFLNZ_RECD_CRNT_SEASON,
      M1051_PVX_RCVD_AGNCY: $scope.M1051_PVX_RCVD_AGNCY,
      M1056_PVX_RSN_NOT_RCVD_AGNCY: $scope.M1056_PVX_RSN_NOT_RCVD_AGNCY,
      M1230_SPEECH: $scope.M1230_SPEECH,
      M1242_PAIN_FREQ_ACTVTY_MVMT: $scope.M1242_PAIN_FREQ_ACTVTY_MVMT,
      //integumentary
      M1306_UNHLD_STG2_PRSR_ULCR: $scope.M1306_UNHLD_STG2_PRSR_ULCR,
      M1307_OLDST_STG2_ONST_DT: $scope.M1307_OLDST_STG2_ONST_DT,
      M1307_OLDST_STG2_AT_DSCHRG: $scope.M1307_OLDST_STG2_AT_DSCHRG,
      M1311_NBR_PRSULC_STG2_A1: $scope.M1311_NBR_PRSULC_STG2_A1,
      M1311_NBR_ULC_SOCROC_STG2_A2: $scope.M1311_NBR_ULC_SOCROC_STG2_A2,
      M1311_NBR_PRSULC_STG3_B1: $scope.M1311_NBR_PRSULC_STG3_B1,
      M1311_NBR_ULC_SOCROC_STG3_B2: $scope.M1311_NBR_ULC_SOCROC_STG3_B2,
      M1311_NBR_PRSULC_STG4_C1: $scope.M1311_NBR_PRSULC_STG4_C1,
      M1311_NBR_ULC_SOCROC_STG4_C2: $scope.M1311_NBR_ULC_SOCROC_STG4_C2,
      M1311_NSTG_DRSG_D1: $scope.M1311_NSTG_DRSG_D1,
      M1311_NSTG_DRSG_SOCROC_D2: $scope.M1311_NSTG_DRSG_SOCROC_D2,
      M1311_NSTG_CVRG_E1: $scope.M1311_NSTG_CVRG_E1,
      M1311_NSTG_CVRG_SOCROC_E2: $scope.M1311_NSTG_CVRG_SOCROC_E2,
      M1311_NSTG_DEEP_TSUE_F1: $scope.M1311_NSTG_DEEP_TSUE_F1,
      M1311_NSTG_DEEP_TSUE_SOCROC_F2: $scope.M1311_NSTG_DEEP_TSUE_SOCROC_F2,
      M1313_NW_WS_PRSULC_STG2_A: $scope.M1313_NW_WS_PRSULC_STG2_A,
      M1313_NW_WS_PRSULC_STG3_B: $scope.M1313_NW_WS_PRSULC_STG3_B,
      M1313_NW_WS_PRSULC_STG4_C: $scope.M1313_NW_WS_PRSULC_STG4_C,
      M1313_NW_WS_PRSULC_NSTG_DRSG_D: $scope.M1313_NW_WS_PRSULC_NSTG_DRSG_D,
      M1313_NW_WS_PRSULC_NSTG_CVRG_E: $scope.M1313_NW_WS_PRSULC_NSTG_CVRG_E,
      M1313_NW_WS_PRSULC_NSTG_TSUE_F: $scope.M1313_NW_WS_PRSULC_NSTG_TSUE_F,
      M1320_STUS_PRBLM_PRSR_ULCR: $scope.M1320_STUS_PRBLM_PRSR_ULCR,
      M1322_NBR_PRSULC_STG1: $scope.M1322_NBR_PRSULC_STG1,
      M1324_STG_PRBLM_ULCER: $scope.M1324_STG_PRBLM_ULCER,
      M1330_STAS_ULCR_PRSNT: $scope.M1330_STAS_ULCR_PRSNT,
      M1332_NBR_STAS_ULCR: $scope.M1332_NBR_STAS_ULCR,
      M1334_STUS_PRBLM_STAS_ULCR: $scope.M1334_STUS_PRBLM_STAS_ULCR,
      M1340_SRGCL_WND_PRSNT: $scope.M1340_SRGCL_WND_PRSNT,
      M1342_STUS_PRBLM_SRGCL_WND: $scope.M1342_STUS_PRBLM_SRGCL_WND,
      //respiratory
      M1400_WHEN_DYSPNEIC: $scope.M1400_WHEN_DYSPNEIC,
      //cardiac
      M1501_SYMTM_HRT_FAILR_PTNTS: $scope.M1501_SYMTM_HRT_FAILR_PTNTS,
      M1511_HRT_FAILR_NO_ACTN: $scope.M1511_HRT_FAILR_NO_ACTN,
      M1511_HRT_FAILR_PHYSN_CNTCT: $scope.M1511_HRT_FAILR_PHYSN_CNTCT,
      M1511_HRT_FAILR_ER_TRTMT: $scope.M1511_HRT_FAILR_ER_TRTMT,
      M1511_HRT_FAILR_PHYSN_TRTMT: $scope.M1511_HRT_FAILR_PHYSN_TRTMT,
      M1511_HRT_FAILR_CLNCL_INTRVTN: $scope.M1511_HRT_FAILR_CLNCL_INTRVTN,
      M1511_HRT_FAILR_CARE_PLAN_CHG: $scope.M1511_HRT_FAILR_CARE_PLAN_CHG,
      //elimination
      M1600_UTI: $scope.M1600_UTI,
      M1610_UR_INCONT: $scope.M1610_UR_INCONT,
      M1615_INCNTNT_TIMING: $scope.M1615_INCNTNT_TIMING,
      M1620_BWL_INCONT: $scope.M1620_BWL_INCONT,
      //behavioural
      M1700_COG_FUNCTION: $scope.M1700_COG_FUNCTION,
      M1710_WHEN_CONFUSED: $scope.M1710_WHEN_CONFUSED,
      M1720_WHEN_ANXIOUS: $scope.M1720_WHEN_ANXIOUS,
      M1740_BD_MEM_DEFICIT: $scope.M1740_BD_MEM_DEFICIT ,
      M1740_BD_IMP_DECISN: $scope.M1740_BD_IMP_DECISN,
      M1740_BD_VERBAL: $scope.M1740_BD_VERBAL,
      M1740_BD_PHYSICAL: $scope.M1740_BD_PHYSICAL,
      M1740_BD_SOC_INAPPRO: $scope.M1740_BD_SOC_INAPPRO,
      M1740_BD_DELUSIONS: $scope.M1740_BD_DELUSIONS,
      M1740_BD_NONE: $scope.M1740_BD_NONE,
      M1745_BEH_PROB_FREQ: $scope.M1745_BEH_PROB_FREQ,
      //ADL
      M1800_CRNT_GROOMING: $scope.M1800_CRNT_GROOMING,
      M1810_CRNT_DRESS_UPPER: $scope.M1810_CRNT_DRESS_UPPER,
      M1820_CRNT_DRESS_LOWER: $scope.M1820_CRNT_DRESS_LOWER,
      M1830_CRNT_BATHG: $scope.M1830_CRNT_BATHG,
      M1840_CRNT_TOILTG: $scope.M1840_CRNT_TOILTG,
      M1845_CRNT_TOILTG_HYGN: $scope.M1845_CRNT_TOILTG_HYGN ,
      M1850_CRNT_TRNSFRNG: $scope.M1850_CRNT_TRNSFRNG,
      M1860_CRNT_AMBLTN: $scope.M1860_CRNT_AMBLTN,
      M1870_CRNT_FEEDING: $scope.M1870_CRNT_FEEDING,
      M1880_CRNT_PREP_LT_MEALS: $scope.M1880_CRNT_PREP_LT_MEALS,
      M1890_CRNT_PHONE_USE: $scope.M1890_CRNT_PHONE_USE,
      //medication
      M2005_MDCTN_INTRVTN: $scope.M2005_MDCTN_INTRVTN,
      M2016_DRUG_EDCTN_INTRVTN: $scope.M2016_DRUG_EDCTN_INTRVTN,
      M2020_CRNT_MGMT_ORAL_MDCTN: $scope.M2020_CRNT_MGMT_ORAL_MDCTN,
      M2030_CRNT_MGMT_INJCTN_MDCTN: $scope.M2030_CRNT_MGMT_INJCTN_MDCTN,
      //careManagement
      M2102_CARE_TYPE_SRC_ADL: $scope.M2102_CARE_TYPE_SRC_ADL,
      M2102_CARE_TYPE_SRC_ADVCY: $scope.M2102_CARE_TYPE_SRC_ADVCY,
      M2102_CARE_TYPE_SRC_EQUIP: $scope.M2102_CARE_TYPE_SRC_EQUIP,
      M2102_CARE_TYPE_SRC_IADL: $scope.M2102_CARE_TYPE_SRC_IADL,
      M2102_CARE_TYPE_SRC_MDCTN: $scope.M2102_CARE_TYPE_SRC_MDCTN,
      M2102_CARE_TYPE_SRC_PRCDR: $scope.M2102_CARE_TYPE_SRC_PRCDR,
      M2102_CARE_TYPE_SRC_SPRVSN: $scope.M2102_CARE_TYPE_SRC_SPRVSN,
      //emergent care
      M2301_EMER_USE_AFTR_LAST_ASMT: $scope.M2301_EMER_USE_AFTR_LAST_ASMT,
      M2310_ECR_MEDICATION: $scope.M2310_ECR_MEDICATION,
      M2310_ECR_INJRY_BY_FALL: $scope.M2310_ECR_INJRY_BY_FALL,
      M2310_ECR_RSPRTRY_INFCTN: $scope.M2310_ECR_RSPRTRY_INFCTN,
      M2310_ECR_RSPRTRY_OTHR: $scope.M2310_ECR_RSPRTRY_OTHR,
      M2310_ECR_HRT_FAILR: $scope.M2310_ECR_HRT_FAILR,
      M2310_ECR_CRDC_DSRTHM: $scope.M2310_ECR_CRDC_DSRTHM,
      M2310_ECR_MI_CHST_PAIN: $scope.M2310_ECR_MI_CHST_PAIN,
      M2310_ECR_OTHR_HRT_DEASE: $scope.M2310_ECR_OTHR_HRT_DEASE,
      M2310_ECR_STROKE_TIA: $scope.M2310_ECR_STROKE_TIA,
      M2310_ECR_HYPOGLYC: $scope.M2310_ECR_HYPOGLYC,
      M2310_ECR_GI_PRBLM: $scope.M2310_ECR_GI_PRBLM,
      M2310_ECR_DHYDRTN_MALNTR: $scope.M2310_ECR_DHYDRTN_MALNTR,
      M2310_ECR_UTI: $scope.M2310_ECR_UTI,
      M2310_ECR_CTHTR_CMPLCTN: $scope.M2310_ECR_CTHTR_CMPLCTN,
      M2310_ECR_WND_INFCTN_DTRORTN: $scope.M2310_ECR_WND_INFCTN_DTRORTN,
      M2310_ECR_UNCNTLD_PAIN: $scope.M2310_ECR_UNCNTLD_PAIN,
      M2310_ECR_MENTL_BHVRL_PRBLM: $scope.M2310_ECR_MENTL_BHVRL_PRBLM,
      M2310_ECR_DVT_PULMNRY: $scope.M2310_ECR_DVT_PULMNRY,
      M2310_ECR_OTHER: $scope.M2310_ECR_OTHER,
      M2310_ECR_UNKNOWN: $scope.M2310_ECR_UNKNOWN,
      M2401_INTRVTN_SMRY_DBTS_FT: $scope.M2401_INTRVTN_SMRY_DBTS_FT,
      M2401_INTRVTN_SMRY_FALL_PRVNT: $scope.M2401_INTRVTN_SMRY_FALL_PRVNT,
      M2401_INTRVTN_SMRY_DPRSN: $scope.M2401_INTRVTN_SMRY_DPRSN,
      M2401_INTRVTN_SMRY_PAIN_MNTR: $scope.M2401_INTRVTN_SMRY_PAIN_MNTR,
      M2401_INTRVTN_SMRY_PRSULC_PRVN: $scope.M2401_INTRVTN_SMRY_PRSULC_PRVN,
      M2401_INTRVTN_SMRY_PRSULC_WET: $scope.M2401_INTRVTN_SMRY_PRSULC_WET,
      M2410_INPAT_FACILITY: $scope.M2410_INPAT_FACILITY,
      M2420_DSCHRG_DISP: $scope.M2420_DSCHRG_DISP,
      M0903_LAST_HOME_VISIT: $scope.M0903_LAST_HOME_VISIT,
      M0906_DC_TRAN_DTH_DT: $filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd'),
    }
    console.log("editOasisOtherForm", editOasisOtherForm);
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['oasisC2_2_20_1'].edit,
      method: 'POST',
      data: $httpParamSerializerJQLike(editOasisOtherForm),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function(response){
      console.log(response);
      if(response.data.status==='success') {
        $scope.disableEditSOCDetails = true;
        $scope.enableInput = false;
        $scope.enableQALockBtn = true;
        $scope.enableQAEditBtn=true;
        $scope.enableQARejectBtn=true;
        $scope.enableCancelBtn=false;
        // editAdmit();
        // editPatient();

        //if discharge date has been changed
        if($scope.changeVisitDate) {
          console.log('changing discharge dates...');
          // editForm();
          dataService.edit('form', {KEYID:$scope.oasis.FORMID, FORMDATE:$filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')}).then(function (response) {
            console.log(response);
          });
          // editVisit();
          dataService.edit('visit', {KEYID:$scope.oasis.VISITID, VISITDATE:$filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')}).then(function (response) {
            console.log(response);
          });
          //edit admit();
          dataService.edit('admission', {KEYID:$scope.oasis.ADMITKEYID, DCDATE:$filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')}).then(function (response) {
            console.log(response);
          });
          //edit episode();
          dataService.edit('episode', {KEYID:$scope.oasis.EPIKEYID, DCDATE:$filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')}).then(function (response) {
            console.log(response);
          });
        }
      }
    }, function(data) {
        console.log(data);
      }
    );
  };

    $scope.reject=function () {
      //change form status to in progress, and attach note?
      var form={
        KEYID: $scope.oasis.FORMID,
        STATUS: 4,
        // DETAIL1: 'Form has been rejected by QA'
      };
      console.log(form);
      dataService.edit('form', form).then(function (response) {
        console.log(response);
        $state.go('qa');
      });
    };
});
