app.config(function($stateProvider) {
  $stateProvider.state('viewOasisDischarge', {
    url: '/oasisC2/viewOasisDischarge',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/viewOasis/oasisC2-ICD10/viewDischarge/viewDischarge.html',
        controller: 'ViewDischargeController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
