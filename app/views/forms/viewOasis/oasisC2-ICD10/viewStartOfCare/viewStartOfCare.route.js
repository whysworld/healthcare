app.config(function($stateProvider) {
  $stateProvider.state('viewOasisStartOfCare', {
    url: '/oasisC2/viewStartOfCare',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/viewOasis/oasisC2-ICD10/viewStartOfCare/viewStartOfCare.html',
        controller: 'ViewStartOfCareController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
