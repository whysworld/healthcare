app.controller('ViewDeathAtHomeController', function($scope, patientService,dataService) {

  $scope.patient = patientService.get();


  $scope.currentForm = 'ClinicalRecordItems';

  $scope.nextForm = function(current, destination) {
    if ($scope.currentForm != destination) {
      $scope.currentForm = destination;
      $('.' + current ).fadeOut('fast', function() {
        $('.' + destination ).fadeIn('fast');
      });
    }
  };

  $scope.activeForm = function(form) {
    if ($scope.currentForm == form) {
      return true;
    }
  };

  $scope.submitSection = function(form, destination) {
    console.log(form.$submitted);
    console.log(form.$valid);
    if(form.$valid) {
      $('#' + $scope.currentForm + ' i').css({color: 'green'});
      var section = $scope.currentForm + 'Complete';

      $scope.nextForm($scope.currentForm, destination);
      console.log(section);
      $scope[section] = true;
      console.log($scope[section]);
    }
    else {
      $('#' + $scope.currentForm + ' i').css({color: 'lightgrey'});
      console.log('Not Valid');
    }
  };

  dataService.get('oasisC2_2_20_1', {'M0020_PAT_ID': $scope.patient.KEYID, 'M0100_ASSMT_REASON': '08'}).then(function(data){
    // console.log(data)
    var oasis = data.data;
    oasis = oasis.slice(-1).pop();
    console.log(oasis);
    $scope.oasisKEYID = oasis.KEYID;

    $scope.M0080_ASSESSOR_DISCIPLINE=oasis.M0080_ASSESSOR_DISCIPLINE;
    $scope.M0090_INFO_COMPLETED_DT=oasis.M0090_INFO_COMPLETED_DT;
    $scope.M0100_ASSMT_REASON=oasis.M0100_ASSMT_REASON;
    $scope.M2005_MDCTN_INTRVTN=oasis.M2005_MDCTN_INTRVTN;
    $scope.M0903_LAST_HOME_VISIT=oasis.M0903_LAST_HOME_VISIT;
    $scope.M0906_DC_TRAN_DTH_DT=oasis.M0906_DC_TRAN_DTH_DT;

    if (oasis.M0090_INFO_COMPLETED_DT !== null) {
      $scope.M0090_INFO_COMPLETED_DT = new Date(oasis.M0090_INFO_COMPLETED_DT);
    }
    if (oasis.M0903_LAST_HOME_VISIT !== null) {
      $scope.M0903_LAST_HOME_VISIT = new Date(oasis.M0903_LAST_HOME_VISIT);
    }
    if (oasis.M0906_DC_TRAN_DTH_DT !== null) {
      $scope.M0906_DC_TRAN_DTH_DT = new Date(oasis.M0906_DC_TRAN_DTH_DT);
    }
  })

  $scope.disableEditDetails = true;

  $scope.edit = function(){
    $scope.disableEditDetails = false;
    $scope.enableInput = true;
  }
  $scope.save = function() {
    console.log("save")
    var editOasisOtherForm = {
      KEYID: $scope.oasisKEYID,
      M0020_PAT_ID: $scope.patient.KEYID,
      //clinical record
      M0080_ASSESSOR_DISCIPLINE: $scope.M0080_ASSESSOR_DISCIPLINE,
      M0090_INFO_COMPLETED_DT: $scope.M0090_INFO_COMPLETED_DT,
      M0100_ASSMT_REASON:$scope.M0100_ASSMT_REASON,
      M2005_MDCTN_INTRVTN: $scope.M2005_MDCTN_INTRVTN,
      M0903_LAST_HOME_VISIT: $scope.M0903_LAST_HOME_VISIT,
      M0906_DC_TRAN_DTH_DT: $scope.M0906_DC_TRAN_DTH_DT
    }
    console.log("editOasisOtherForm", editOasisOtherForm);

    dataService.edit('oasisC2_2_20_1', editOasisOtherForm).then(function(response){
      console.log(response);
      $scope.disableEditDetails = true;
      $scope.enableInput = false;
    });
  }
})
