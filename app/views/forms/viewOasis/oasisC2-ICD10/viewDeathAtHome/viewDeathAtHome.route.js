app.config(function($stateProvider) {
  $stateProvider.state('viewOasisDeathAtHome', {
    url: '/oasisC2/viewOasisDeathAtHome',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/viewOasis/oasisC2-ICD10/viewDeathAtHome/viewDeathAtHome.html',
        controller: 'ViewDeathAtHomeController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
