app.controller('ViewRecertificationController', function($scope, $state, $filter, $http, CONSTANTS, $httpParamSerializerJQLike,patientService,dataService, formService) {

  $scope.patient = patientService.get();


  $scope.currentForm = 'General';

  $scope.nextForm = function(current, destination) {
    if ($scope.currentForm != destination) {
      $scope.currentForm = destination;
      $('.' + current ).fadeOut('fast', function() {
        $('.' + destination ).fadeIn('fast');
      });
    }
  };

  $scope.activeForm = function(form) {
    if ($scope.currentForm == form) {
      return true;
    }
  };

  $scope.submitSection = function(form, destination) {
    console.log(form.$submitted);
    console.log(form.$valid);
    if(form.$valid) {
      $('#' + $scope.currentForm + ' i').css({color: 'green'});
      var section = $scope.currentForm + 'Complete';

      $scope.nextForm($scope.currentForm, destination);
      console.log(section);
      $scope[section] = true;
      console.log($scope[section]);
    }
    else {
      $('#' + $scope.currentForm + ' i').css({color: 'lightgrey'});
      console.log('Not Valid');
    }
  };


  //alerting msg if M0906 date is changed and different from form date
  $scope.checkDate=function () {
    if ($filter("date")($scope.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd') !==  $filter("date")($scope.oasis.FORMDATE, 'yyyy/MM/dd')) {
      alert("Note: Changing M0090 will automatically change date of visit.");
      $scope.changeVisitDate=true;
    } else {
      $scope.changeVisitDate=false;
    }
  };


  getOasis();
  function getOasis() {
    var oasis = formService.get();
    $scope.oasis=oasis;
    console.log(oasis);

    //show qa buttons if ACTION=QAREVIEW;
    if($scope.oasis.ACTION!==undefined){ //redirected from QA page
      console.log('enabling qa mode');
      //show/hide approved msg
      if (oasis.LOCKED===1){
        $scope.approvedMsg=true; //form is locked, show approved msg
        $scope.enableQAEditBtn=false;
        $scope.enableQALockBtn=false;
        $scope.enableQARejectBtn=false;
        // $scope.disableEditSOCDetails=false;
      } else {
        $scope.approvedMsg=false; //form is not locked, allow editing
        $scope.enableQAEditBtn=true;
        $scope.enableQALockBtn=true;
        $scope.enableQARejectBtn=true;
        // $scope.disableEditSOCDetails=true;
      }
    } else { //redirected from records page
      if ( oasis.LOCKED===1)  { //form is locked, show approved msg
        $scope.approvedMsg=true;
        $scope.enableQAEditBtn=false;
        $scope.enableQALockBtn=false;
        $scope.enableQARejectBtn=false;
        $scope.enableCancelBtn=false;

      } else { //form is not locked, show in review msg
        $scope.inReviewMsg=true;
        $scope.enableQAEditBtn=false;
        $scope.enableQALockBtn=false;
        $scope.enableQARejectBtn=false;
        $scope.enableCancelBtn=false;
      }
    }

    $scope.oasisKEYID=oasis.KEYID;
    var INPArr=[
      oasis.M1011_14_DAY_INP1_ICD,
      oasis.M1011_14_DAY_INP2_ICD,
      oasis.M1011_14_DAY_INP3_ICD,
      oasis.M1011_14_DAY_INP4_ICD,
      oasis.M1011_14_DAY_INP5_ICD,
      oasis.M1011_14_DAY_INP6_ICD
    ];
    function getM1011desc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1011_' + (i+1)]=codeDesc;
      });
    }
    for (var i = 0; i < INPArr.length; i++) {
      if (INPArr[i]!==undefined && INPArr[i]!=="" && INPArr[i]!==null) {
        var code=INPArr[i].replace('.', '');
        getM1011desc(i)
      }
    }


    var chgregArr=[
      oasis.M1017_CHGREG_ICD1,
      oasis.M1017_CHGREG_ICD2,
      oasis.M1017_CHGREG_ICD3,
      oasis.M1017_CHGREG_ICD4,
      oasis.M1017_CHGREG_ICD5,
      oasis.M1017_CHGREG_ICD6,
    ];
    function getM1017desc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1017_' + (i+1)]=codeDesc;
      });
    }
    for (var i = 0; i < chgregArr.length; i++) {
      if (chgregArr[i]!==undefined && chgregArr[i]!=="" && chgregArr[i]!==null) {
        var code=chgregArr[i].replace('.', '');
        getM1017desc(i)
      }
    }
    var m1021Arr=[
      oasis.M1021_PRIMARY_DIAG_ICD,
    ];
    function getM1021desc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1021']=codeDesc;
      });
    }
    for (var i = 0; i < m1021Arr.length; i++) {
      if (m1021Arr[i]!==undefined && m1021Arr[i]!=="" && m1021Arr[i]!==null) {
        var code=m1021Arr[i].replace('.', '');
        getM1021desc(i)
      }
    }
    var m1023Arr=[
      oasis.M1023_OTH_DIAG1_ICD,
      oasis.M1023_OTH_DIAG2_ICD,
      oasis.M1023_OTH_DIAG3_ICD,
      oasis.M1023_OTH_DIAG4_ICD,
      oasis.M1023_OTH_DIAG5_ICD,
    ];
    function getM1023desc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1023_' + (i+1)]=codeDesc;
      });
    }
    for (var i = 0; i < m1023Arr.length; i++) {
      if (m1023Arr[i]!==undefined && m1023Arr[i]!=="" && m1023Arr[i]!==null) {
        var code=m1023Arr[i].replace('.', '');
        getM1023desc(i)
      }
    }
    var m1025aArr=[
      oasis.M1025_OPT_DIAG_ICD_A3,
      oasis.M1025_OPT_DIAG_ICD_A4,
    ];
    function getM1025adesc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1025A_' + (i+3)]=codeDesc;
      });
    }
    for (var i = 0; i < m1025aArr.length; i++) {
      if (m1025aArr[i]!==undefined && m1025aArr[i]!=="" && m1025aArr[i]!==null) {
        var code=m1025aArr[i].replace('.', '');
        getM1025adesc(i)
      }
    }
    var m1025bArr=[
      oasis.M1025_OPT_DIAG_ICD_B3,
      oasis.M1025_OPT_DIAG_ICD_B4,
    ];
    function getM1025Bdesc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1025B_' + (i+3)]=codeDesc;
      });
    }
    for (var i = 0; i < m1025aArr.length; i++) {
      if (m1025bArr[i]!==undefined && m1025bArr[i]!=="" && m1025bArr[i]!==null) {
        var code=m1025bArr[i].replace('.', '');
        getM1025Bdesc(i)
      }
    }
    var m1025cArr=[
      oasis.M1025_OPT_DIAG_ICD_C3,
      oasis.M1025_OPT_DIAG_ICD_C4,
    ];
    function getM1025Cdesc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1025C_' + (i+3)]=codeDesc;
      });
    }
    for (var i = 0; i < m1025aArr.length; i++) {
      if (m1025cArr[i]!==undefined && m1025cArr[i]!=="" && m1025cArr[i]!==null) {
        var code=m1025cArr[i].replace('.', '');
        getM1025Cdesc(i)
      }
    }
    var m1025dArr=[
      oasis.M1025_OPT_DIAG_ICD_D3,
      oasis.M1025_OPT_DIAG_ICD_D4,
    ];
    function getM1025Ddesc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1025D_' + (i+3)]=codeDesc;
      });
    }
    for (var i = 0; i < m1025aArr.length; i++) {
      if (m1025dArr[i]!==undefined && m1025dArr[i]!=="" && m1025dArr[i]!==null) {
        var code=m1025dArr[i].replace('.', '');
        getM1025Ddesc(i)
      }
    }
    var m1025eArr=[
      oasis.M1025_OPT_DIAG_ICD_E3,
      oasis.M1025_OPT_DIAG_ICD_E4,
    ];
    function getM1025Edesc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1025E_' + (i+3)]=codeDesc;
      });
    }
    for (var i = 0; i < m1025aArr.length; i++) {
      if (m1025eArr[i]!==undefined && m1025eArr[i]!=="" && m1025eArr[i]!==null) {
        var code=m1025eArr[i].replace('.', '');
        getM1025Edesc(i)
      }
    }
    var m1025fArr=[
      oasis.M1025_OPT_DIAG_ICD_F3,
      oasis.M1025_OPT_DIAG_ICD_F4,
    ];
    function getM1025Fdesc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1025F_' + (i+3)]=codeDesc;
      });
    }
    for (var i = 0; i < m1025aArr.length; i++) {
      if (m1025fArr[i]!==undefined && m1025fArr[i]!=="" && m1025fArr[i]!==null) {
        var code=m1025fArr[i].replace('.', '');
        getM1025Fdesc(i)
      }
    }

    //******** GENERAL ********//
    $scope.M0010_CCN=oasis.M0010_CCN;
    $scope.M0014_BRANCH_STATE=oasis.M0014_BRANCH_STATE;
    $scope.M0016_BRANCH_ID=oasis.M0016_BRANCH_ID;
    $scope.M0018_PHYSICIAN_ID=oasis.M0018_PHYSICIAN_ID;
    $scope.M0018_PHYSICIAN_UK=oasis.M0018_PHYSICIAN_UK;
    $scope.M0020_PAT_ID=oasis.M0020_PAT_ID;
    $scope.M0030_START_CARE_DT=new Date($filter("date")(oasis.M0030_START_CARE_DT, 'yyyy/MM/dd'));
    $scope.M0032_ROC_DT=new Date($filter("date")(oasis.M0032_ROC_DT, 'yyyy/MM/dd'));
    $scope.M0032_ROC_DT_NA=oasis.M0032_ROC_DT_NA;
    $scope.M0040_PAT_FNAME=oasis.M0040_PAT_FNAME;
    $scope.M0040_PAT_MI=oasis.M0040_PAT_MI;
    $scope.M0040_PAT_LNAME=oasis.M0040_PAT_LNAME;
    $scope.M0040_PAT_SUFFIX=oasis.M0040_PAT_SUFFIX;
    $scope.M0050_PAT_ST=oasis.M0050_PAT_ST;
    $scope.M0060_PAT_ZIP=oasis.M0060_PAT_ZIP;
    $scope.M0063_MEDICARE_NUM=oasis.M0063_MEDICARE_NUM;
    $scope.M0063_MEDICARE_NA=oasis.M0063_MEDICARE_NA;
    $scope.M0064_SSN=oasis.M0064_SSN;
    $scope.M0064_SSN_UK=oasis.M0064_SSN_UK;
    $scope.M0065_MEDICAID_NUM=oasis.M0065_MEDICAID_NUM;
    $scope.M0065_MEDICAID_NA=oasis.M0065_MEDICAID_NA;
    $scope.M0066_PAT_BIRTH_DT=new Date($filter("date")(oasis.M0066_PAT_BIRTH_DT, 'yyyy/MM/dd'));
    $scope.M0069_PAT_GENDER=oasis.M0069_PAT_GENDER;
    $scope.M0090_INFO_COMPLETED_DT=new Date($filter("date")(oasis.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd'));
    $scope.M0140_ETHNIC_AI_AN=oasis.M0140_ETHNIC_AI_AN;
    $scope.M0140_ETHNIC_ASIAN=oasis.M0140_ETHNIC_ASIAN;
    $scope.M0140_ETHNIC_BLACK=oasis.M0140_ETHNIC_BLACK;
    $scope.M0140_ETHNIC_HISP=oasis.M0140_ETHNIC_HISP;
    $scope.M0140_ETHNIC_NH_PI=oasis.M0140_ETHNIC_NH_PI;
    $scope.M0140_ETHNIC_WHITE=oasis.M0140_ETHNIC_WHITE;
    $scope.M0150_CPAY_NONE=oasis.M0150_CPAY_NONE;
    $scope.M0150_CPAY_MCARE_FFS=oasis.M0150_CPAY_MCARE_FFS;
    $scope.M0150_CPAY_MCARE_HMO=oasis.M0150_CPAY_MCARE_HMO;
    $scope.M0150_CPAY_MCAID_FFS=oasis.M0150_CPAY_MCAID_FFS;
    $scope.M0150_CPAY_MCAID_HMO=oasis.M0150_CPAY_MCAID_HMO;
    $scope.M0150_CPAY_WRKCOMP=oasis.M0150_CPAY_WRKCOMP;
    $scope.M0150_CPAY_TITLEPGMS=oasis.M0150_CPAY_TITLEPGMS;
    $scope.M0150_CPAY_OTH_GOVT=oasis.M0150_CPAY_OTH_GOVT;
    $scope.M0150_CPAY_PRIV_INS=oasis.M0150_CPAY_PRIV_INS;
    $scope.M0150_CPAY_PRIV_HMO=oasis.M0150_CPAY_PRIV_HMO;
    $scope.M0150_CPAY_SELFPAY=oasis.M0150_CPAY_SELFPAY;
    $scope.M0150_CPAY_OTHER=oasis.M0150_CPAY_OTHER;
    $scope.M0150_CPAY_UK=oasis.M0150_CPAY_UK;
    //clinical record
    $scope.M0080_ASSESSOR_DISCIPLINE=oasis.M0080_ASSESSOR_DISCIPLINE;
    $scope.M0100_ASSMT_REASON=oasis.M0100_ASSMT_REASON;
    $scope.M0110_EPISODE_TIMING=oasis.M0110_EPISODE_TIMING;
    //patient history
    $scope.M1011_14_DAY_INP1_ICD=oasis.M1011_14_DAY_INP1_ICD;
    $scope.M1011_14_DAY_INP2_ICD=oasis.M1011_14_DAY_INP2_ICD;
    $scope.M1011_14_DAY_INP3_ICD=oasis.M1011_14_DAY_INP3_ICD;
    $scope.M1011_14_DAY_INP4_ICD=oasis.M1011_14_DAY_INP4_ICD;
    $scope.M1011_14_DAY_INP5_ICD=oasis.M1011_14_DAY_INP5_ICD;
    $scope.M1011_14_DAY_INP6_ICD=oasis.M1011_14_DAY_INP6_ICD;
    $scope.M1021_PRIMARY_DIAG_ICD=oasis.M1021_PRIMARY_DIAG_ICD;
    $scope.M1021_PRIMARY_DIAG_SEVERITY=oasis.M1021_PRIMARY_DIAG_SEVERITY;
    $scope.M1023_OTH_DIAG1_ICD=oasis.M1023_OTH_DIAG1_ICD;
    $scope.M1023_OTH_DIAG1_SEVERITY=oasis.M1023_OTH_DIAG1_SEVERITY;
    $scope.M1023_OTH_DIAG2_ICD=oasis.M1023_OTH_DIAG2_ICD;
    $scope.M1023_OTH_DIAG2_SEVERITY=oasis.M1023_OTH_DIAG2_SEVERITY;
    $scope.M1023_OTH_DIAG3_ICD=oasis.M1023_OTH_DIAG3_ICD;
    $scope.M1023_OTH_DIAG3_SEVERITY=oasis.M1023_OTH_DIAG3_SEVERITY;
    $scope.M1023_OTH_DIAG4_ICD=oasis.M1023_OTH_DIAG4_ICD;
    $scope.M1023_OTH_DIAG4_SEVERITY=oasis.M1023_OTH_DIAG4_SEVERITY;
    $scope.M1023_OTH_DIAG5_ICD=oasis.M1023_OTH_DIAG5_ICD;
    $scope.M1023_OTH_DIAG5_SEVERITY=oasis.M1023_OTH_DIAG5_SEVERITY;
    $scope.M1025_OPT_DIAG_ICD_A3=oasis.M1025_OPT_DIAG_ICD_A3;
    $scope.M1025_OPT_DIAG_ICD_A4=oasis.M1025_OPT_DIAG_ICD_A4;
    $scope.M1025_OPT_DIAG_ICD_B3=oasis.M1025_OPT_DIAG_ICD_B3;
    $scope.M1025_OPT_DIAG_ICD_B4=oasis.M1025_OPT_DIAG_ICD_B4;
    $scope.M1025_OPT_DIAG_ICD_C3=oasis.M1025_OPT_DIAG_ICD_C3;
    $scope.M1025_OPT_DIAG_ICD_C4=oasis.M1025_OPT_DIAG_ICD_C4;
    $scope.M1025_OPT_DIAG_ICD_D3=oasis.M1025_OPT_DIAG_ICD_D3;
    $scope.M1025_OPT_DIAG_ICD_D4=oasis.M1025_OPT_DIAG_ICD_D4;
    $scope.M1025_OPT_DIAG_ICD_E3=oasis.M1025_OPT_DIAG_ICD_E3;
    $scope.M1025_OPT_DIAG_ICD_E4=oasis.M1025_OPT_DIAG_ICD_E4;
    $scope.M1025_OPT_DIAG_ICD_F3=oasis.M1025_OPT_DIAG_ICD_F3;
    $scope.M1025_OPT_DIAG_ICD_F4=oasis.M1025_OPT_DIAG_ICD_F4;
    $scope.M1030_THH_IV_INFUSION=oasis.M1030_THH_IV_INFUSION;
    $scope.M1030_THH_PAR_NUTRITION=oasis.M1030_THH_PAR_NUTRITION;
    $scope.M1030_THH_ENT_NUTRITION=oasis.M1030_THH_ENT_NUTRITION;
    $scope.M1030_THH_NONE_ABOVE=oasis.M1030_THH_NONE_ABOVE;
    //sensory
    $scope.M1200_VISION=oasis.M1200_VISION;
    $scope.M1242_PAIN_FREQ_ACTVTY_MVMT=oasis.M1242_PAIN_FREQ_ACTVTY_MVMT;
    //integumentary
    $scope.M1306_UNHLD_STG2_PRSR_ULCR=oasis.M1306_UNHLD_STG2_PRSR_ULCR;
    $scope.M1311_NBR_PRSULC_STG2_A1=oasis.M1311_NBR_PRSULC_STG2_A1;
    $scope.M1311_NBR_ULC_SOCROC_STG2_A2=oasis.M1311_NBR_ULC_SOCROC_STG2_A2;
    $scope.M1311_NBR_PRSULC_STG3_B1=oasis.M1311_NBR_PRSULC_STG3_B1;
    $scope.M1311_NBR_ULC_SOCROC_STG3_B2=oasis.M1311_NBR_ULC_SOCROC_STG3_B2;
    $scope.M1311_NBR_PRSULC_STG4_C1=oasis.M1311_NBR_PRSULC_STG4_C1;
    $scope.M1311_NBR_ULC_SOCROC_STG4_C2=oasis.M1311_NBR_ULC_SOCROC_STG4_C2;
    $scope.M1311_NSTG_DRSG_D1=oasis.M1311_NSTG_DRSG_D1;
    $scope.M1311_NSTG_DRSG_SOCROC_D2=oasis.M1311_NSTG_DRSG_SOCROC_D2;
    $scope.M1311_NSTG_CVRG_E1=oasis.M1311_NSTG_CVRG_E1;
    $scope.M1311_NSTG_CVRG_SOCROC_E2=oasis.M1311_NSTG_CVRG_SOCROC_E2;
    $scope.M1311_NSTG_DEEP_TSUE_F1=oasis.M1311_NSTG_DEEP_TSUE_F1;
    $scope.M1311_NSTG_DEEP_TSUE_SOCROC_F2=oasis.M1311_NSTG_DEEP_TSUE_SOCROC_F2;
    $scope.M1322_NBR_PRSULC_STG1=oasis.M1322_NBR_PRSULC_STG1;
    $scope.M1324_STG_PRBLM_ULCER=oasis.M1324_STG_PRBLM_ULCER;
    $scope.M1330_STAS_ULCR_PRSNT=oasis.M1330_STAS_ULCR_PRSNT;
    $scope.M1332_NBR_STAS_ULCR=oasis.M1332_NBR_STAS_ULCR;
    $scope.M1334_STUS_PRBLM_STAS_ULCR=oasis.M1334_STUS_PRBLM_STAS_ULCR;
    $scope.M1340_SRGCL_WND_PRSNT=oasis.M1340_SRGCL_WND_PRSNT;
    $scope.M1342_STUS_PRBLM_SRGCL_WND=oasis.M1342_STUS_PRBLM_SRGCL_WND;
    //respiratory
    $scope.M1400_WHEN_DYSPNEIC=oasis.M1400_WHEN_DYSPNEIC;
    $scope.M1410_RESPTX_OXYGEN=oasis.M1410_RESPTX_OXYGEN;
    $scope.M1410_RESPTX_VENTILATOR=oasis.M1410_RESPTX_VENTILATOR;
    $scope.M1410_RESPTX_AIRPRESS=oasis.M1410_RESPTX_AIRPRESS;
    $scope.M1410_RESPTX_NONE=oasis.M1410_RESPTX_NONE;
    //elimination
    $scope.M1610_UR_INCONT=oasis.M1610_UR_INCONT;
    $scope.M1620_BWL_INCONT=oasis.M1620_BWL_INCONT;
    $scope.M1630_OSTOMY=oasis.M1630_OSTOMY;
    //ADL
    $scope.M1810_CRNT_DRESS_UPPER=oasis.M1810_CRNT_DRESS_UPPER;
    $scope.M1820_CRNT_DRESS_LOWER=oasis.M1820_CRNT_DRESS_LOWER;
    $scope.M1830_CRNT_BATHG=oasis.M1830_CRNT_BATHG;
    $scope.M1840_CRNT_TOILTG=oasis.M1840_CRNT_TOILTG;
    $scope.M1850_CRNT_TRNSFRNG=oasis.M1850_CRNT_TRNSFRNG;
    $scope.M1860_CRNT_AMBLTN=oasis.M1860_CRNT_AMBLTN;
    //medication
    $scope.M2030_CRNT_MGMT_INJCTN_MDCTN=oasis.M2030_CRNT_MGMT_INJCTN_MDCTN;
    //therapy
    $scope.M2200_THER_NEED_NBR=oasis.M2200_THER_NEED_NBR;
    $scope.M2200_THER_NEED_NA=oasis.M2200_THER_NEED_NA;

    if (oasis.M0090_INFO_COMPLETED_DT !== null) {
      $scope.M0090_INFO_COMPLETED_DT = new Date($filter("date")(oasis.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd'))
    }
  }

  $scope.disableEditDetails = true;

  $scope.edit = function(){
    // $scope.disableEditDetails = false;
    // $scope.enableInput = true;
    // $scope.enableQALockBtn=false;
    // $scope.enableQAEditBtn=false;
    // $scope.enableQARejectBtn=false;
    // $scope.enableCancelBtn=true;
    var form = {
      ADMITKEYID: $scope.oasis.ADMITKEYID,
      EPIKEYID: $scope.oasis.EPIKEYID,
      FORMID: $scope.oasis.FORMID,
      OBJKEYID: $scope.oasis.OBJKEYID,
      VISITDATE: $scope.oasis.FORMDATE,
      VISITID: $scope.oasis.VISITID,
      ACTION: 'QAEDIT',
    };
    formService.set(form);
    // console.log($scope.oasis)
    $state.go('recertificationAssessment')
  };

  $scope.cancel=function () {
    $scope.disableEditDetails = true;
    $scope.enableInput = false;
    $scope.enableQALockBtn = true;
    $scope.enableQAEditBtn=true;
    $scope.enableQARejectBtn=true;
    $scope.enableCancelBtn=false;
    getOasis();
  };

  //form approved, oasis LOCKED=1
  $scope.lock=function () {
    //reset current oasis
    dataService.get('oasisC2_2_20_1', {KEYID:$scope.oasisKEYID}).then(function (data) {
      // console.log(data.data[0]);
      var oasis=data.data[0];
      // formService.set(data.data[0])
      console.log(oasis);
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['oasisC2_2_20_1'].edit,
        method: 'POST',
        data: $httpParamSerializerJQLike({KEYID:oasis.KEYID, LOCKED:1}),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).then(function(response){
        console.log(response);
        if(response.data.status==='success') {
          $scope.approvedMsg=true;
          $scope.enableQAEditBtn=false;
          $scope.enableQALockBtn=false;

          //edit form status to 3=approved;
          dataService.edit('form', {KEYID:oasis.FORMID, STATUS:3}).then(function (response) {
            console.log(response);
            $state.go('qa');
          });
        }
      }, function(data) {
          console.log(data);
        }
      );
    });
  };

  $scope.save = function() {
    console.log("save")
    var editOasisRecertificationForm = {
      KEYID: $scope.oasisKEYID,
      M0020_PAT_ID: $scope.oasis.PATKEYID,
      //clinical record
      M0080_ASSESSOR_DISCIPLINE: $scope.M0080_ASSESSOR_DISCIPLINE,
      M0090_INFO_COMPLETED_DT: $filter("date")($scope.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd'),
      M0100_ASSMT_REASON: '04',
      M0110_EPISODE_TIMING: $scope.M0110_EPISODE_TIMING,
      //patient history
      M1011_14_DAY_INP1_ICD: $scope.M1011_14_DAY_INP1_ICD,
      M1011_14_DAY_INP2_ICD: $scope.M1011_14_DAY_INP2_ICD,
      M1011_14_DAY_INP3_ICD: $scope.M1011_14_DAY_INP3_ICD,
      M1011_14_DAY_INP4_ICD: $scope.M1011_14_DAY_INP4_ICD,
      M1011_14_DAY_INP5_ICD: $scope.M1011_14_DAY_INP5_ICD,
      M1011_14_DAY_INP6_ICD: $scope.M1011_14_DAY_INP6_ICD,
      M1011_14_DAY_INP_NA: $scope.M1011_14_DAY_INP_NA,
      M1021_PRIMARY_DIAG_ICD: $scope.M1021_PRIMARY_DIAG_ICD,
      M1021_PRIMARY_DIAG_SEVERITY: $scope.M1021_PRIMARY_DIAG_SEVERITY,
      M1023_OTH_DIAG1_ICD: $scope.M1023_OTH_DIAG1_ICD,
      M1023_OTH_DIAG1_SEVERITY: $scope.M1023_OTH_DIAG1_SEVERITY,
      M1023_OTH_DIAG2_ICD: $scope.M1023_OTH_DIAG2_ICD,
      M1023_OTH_DIAG2_SEVERITY: $scope.M1023_OTH_DIAG2_SEVERITY,
      M1023_OTH_DIAG3_ICD: $scope.M1023_OTH_DIAG3_ICD,
      M1023_OTH_DIAG3_SEVERITY: $scope.M1023_OTH_DIAG3_SEVERITY,
      M1023_OTH_DIAG4_ICD: $scope.M1023_OTH_DIAG4_ICD,
      M1023_OTH_DIAG4_SEVERITY: $scope.M1023_OTH_DIAG4_SEVERITY,
      M1023_OTH_DIAG5_ICD: $scope.M1023_OTH_DIAG5_ICD,
      M1023_OTH_DIAG5_SEVERITY: $scope.M1023_OTH_DIAG5_SEVERITY,
      M1025_OPT_DIAG_ICD_A3: $scope.M1025_OPT_DIAG_ICD_A3,
      M1025_OPT_DIAG_ICD_A4: $scope.M1025_OPT_DIAG_ICD_A4,
      M1025_OPT_DIAG_ICD_B3: $scope.M1025_OPT_DIAG_ICD_B3,
      M1025_OPT_DIAG_ICD_B4: $scope.M1025_OPT_DIAG_ICD_B4,
      M1025_OPT_DIAG_ICD_C3: $scope.M1025_OPT_DIAG_ICD_C3,
      M1025_OPT_DIAG_ICD_C4: $scope.M1025_OPT_DIAG_ICD_C4,
      M1025_OPT_DIAG_ICD_D3: $scope.M1025_OPT_DIAG_ICD_D3,
      M1025_OPT_DIAG_ICD_D4: $scope.M1025_OPT_DIAG_ICD_D4,
      M1025_OPT_DIAG_ICD_E3: $scope.M1025_OPT_DIAG_ICD_E3,
      M1025_OPT_DIAG_ICD_E4: $scope.M1025_OPT_DIAG_ICD_E4,
      M1025_OPT_DIAG_ICD_F3: $scope.M1025_OPT_DIAG_ICD_F3,
      M1025_OPT_DIAG_ICD_F4: $scope.M1025_OPT_DIAG_ICD_F4,
      M1030_THH_IV_INFUSION: $scope.M1030_THH_IV_INFUSION,
      M1030_THH_PAR_NUTRITION: $scope.M1030_THH_PAR_NUTRITION,
      M1030_THH_ENT_NUTRITION: $scope.M1030_THH_ENT_NUTRITION,
      M1030_THH_NONE_ABOVE: $scope.M1030_THH_NONE_ABOVE,
      //sensory
      M1200_VISION: $scope.M1200_VISION,
      M1242_PAIN_FREQ_ACTVTY_MVMT: $scope.M1242_PAIN_FREQ_ACTVTY_MVMT,
      //integumentary
      M1306_UNHLD_STG2_PRSR_ULCR: $scope.M1306_UNHLD_STG2_PRSR_ULCR,
      M1311_NBR_PRSULC_STG2_A1: $scope.M1311_NBR_PRSULC_STG2_A1,
      M1311_NBR_ULC_SOCROC_STG2_A2: $scope.M1311_NBR_ULC_SOCROC_STG2_A2,
      M1311_NBR_PRSULC_STG3_B1: $scope.M1311_NBR_PRSULC_STG3_B1,
      M1311_NBR_ULC_SOCROC_STG3_B2: $scope.M1311_NBR_ULC_SOCROC_STG3_B2,
      M1311_NBR_PRSULC_STG4_C1: $scope.M1311_NBR_PRSULC_STG4_C1,
      M1311_NBR_ULC_SOCROC_STG4_C2: $scope.M1311_NBR_ULC_SOCROC_STG4_C2,
      M1311_NSTG_DRSG_D1: $scope.M1311_NSTG_DRSG_D1,
      M1311_NSTG_DRSG_SOCROC_D2: $scope.M1311_NSTG_DRSG_SOCROC_D2,
      M1311_NSTG_CVRG_E1: $scope.M1311_NSTG_CVRG_E1,
      M1311_NSTG_CVRG_SOCROC_E2: $scope.M1311_NSTG_CVRG_SOCROC_E2,
      M1311_NSTG_DEEP_TSUE_F1: $scope.M1311_NSTG_DEEP_TSUE_F1,
      M1311_NSTG_DEEP_TSUE_SOCROC_F2: $scope.M1311_NSTG_DEEP_TSUE_SOCROC_F2,
      M1322_NBR_PRSULC_STG1: $scope.M1322_NBR_PRSULC_STG1,
      M1324_STG_PRBLM_ULCER: $scope.M1324_STG_PRBLM_ULCER,
      M1330_STAS_ULCR_PRSNT: $scope.M1330_STAS_ULCR_PRSNT,
      M1332_NBR_STAS_ULCR: $scope.M1332_NBR_STAS_ULCR,
      M1334_STUS_PRBLM_STAS_ULCR: $scope.M1334_STUS_PRBLM_STAS_ULCR,
      M1340_SRGCL_WND_PRSNT: $scope.M1340_SRGCL_WND_PRSNT,
      M1342_STUS_PRBLM_SRGCL_WND: $scope.M1342_STUS_PRBLM_SRGCL_WND,
      //respiratory
      M1400_WHEN_DYSPNEIC: $scope.M1400_WHEN_DYSPNEIC,
      M1410_RESPTX_OXYGEN: $scope.M1410_RESPTX_OXYGEN,
      M1410_RESPTX_VENTILATOR: $scope.M1410_RESPTX_VENTILATOR,
      M1410_RESPTX_AIRPRESS: $scope.M1410_RESPTX_AIRPRESS,
      M1410_RESPTX_NONE: $scope.M1410_RESPTX_NONE,
      //elimination
      M1610_UR_INCONT: $scope.M1610_UR_INCONT,
      M1620_BWL_INCONT: $scope.M1620_BWL_INCONT,
      M1630_OSTOMY: $scope.M1630_OSTOMY,
      //ADL
      M1810_CRNT_DRESS_UPPER: $scope.M1810_CRNT_DRESS_UPPER,
      M1820_CRNT_DRESS_LOWER: $scope.M1820_CRNT_DRESS_LOWER,
      M1830_CRNT_BATHG: $scope.M1830_CRNT_BATHG,
      M1840_CRNT_TOILTG: $scope.M1840_CRNT_TOILTG,
      M1850_CRNT_TRNSFRNG: $scope.M1850_CRNT_TRNSFRNG,
      M1860_CRNT_AMBLTN: $scope.M1860_CRNT_AMBLTN,
      //medication
      M2030_CRNT_MGMT_INJCTN_MDCTN: $scope.M2030_CRNT_MGMT_INJCTN_MDCTN,
      //therapy
      M2200_THER_NEED_NBR: $scope.M2200_THER_NEED_NBR,
      M2200_THER_NEED_NA: $scope.M2200_THER_NEED_NA
    };
    console.log("editOasisRecertificationForm", editOasisRecertificationForm);

    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['oasisC2_2_20_1'].edit,
      method: 'POST',
      data: $httpParamSerializerJQLike(editOasisRecertificationForm),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function(response){
      console.log(response);
      if(response.data.status==='success') {
        $scope.disableEditSOCDetails = true;
        $scope.enableInput = false;
        $scope.enableQALockBtn = true;
        $scope.enableQAEditBtn=true;
        $scope.enableQARejectBtn=true;
        $scope.enableCancelBtn=false;
        // editAdmit();
        // editPatient();

        //if resumption date has been changed
        if($scope.changeVisitDate) {
          console.log('changing resumption dates...');
          // editForm();
          dataService.edit('form', {KEYID:$scope.oasis.FORMID, FORMDATE:$filter("date")($scope.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd')}).then(function (response) {
            console.log(response);
          });
          // editVisit();
          dataService.edit('visit', {KEYID:$scope.oasis.VISITID, VISITDATE:$filter("date")($scope.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd')}).then(function (response) {
            console.log(response);
          });
          // //edit admit();
          // dataService.edit('admission', {KEYID:$scope.oasis.ADMITKEYID, DCDATE:$filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')}).then(function (response) {
          //   console.log(response);
          // });
          // //edit episode();
          // dataService.edit('episode', {KEYID:$scope.oasis.EPIKEYID, DCDATE:$filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')}).then(function (response) {
          //   console.log(response);
          // });
        }
      }
    }, function(data) {
        console.log(data);
      }
    );
  };

  $scope.reject=function () {
    //change form status to in progress, and attach note?
    var form={
      KEYID: $scope.oasis.FORMID,
      STATUS: 4,
      // DETAIL1: 'Form has been rejected by QA'
    };
    console.log(form);
    dataService.edit('form', form).then(function (response) {
      console.log(response);
      $state.go('qa');
    });
  }
})
