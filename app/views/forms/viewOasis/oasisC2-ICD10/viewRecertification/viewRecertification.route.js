app.config(function($stateProvider) {
  $stateProvider.state('viewOasisRecertification', {
    url: '/oasisC2/viewOasisRecertification',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/viewOasis/oasisC2-ICD10/viewRecertification/viewRecertification.html',
        controller: 'ViewRecertificationController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
