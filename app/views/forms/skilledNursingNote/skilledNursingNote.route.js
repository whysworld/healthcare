app.config(function($stateProvider) {
  $stateProvider.state('skilledNursingNote', {
    url: '/skilledNursingNote',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/skilledNursingNote/skilledNursingNote.html',
        controller: 'SkilledNursingNoteController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
