app.controller('SkilledNursingNoteController', function($scope, $filter, $state, $http, $uibModal, CONSTANTS, $httpParamSerializerJQLike, patientService, dataService, formService) {

  $scope.patient = patientService.get();

  $scope.dataSaved=false;

  $scope.form = formService.get();
  console.log($scope.form);

  $scope.currentForm = 'Vitals';

  var orders=[];

  $scope.nextForm = function(current, destination) {
    if ($scope.currentForm != destination) {
      $scope.currentForm = destination;
      $('.' + current ).fadeOut('fast', function() {
        $('.' + destination ).fadeIn('fast');
      });
    }
  };

  $scope.activeForm = function(form) {
    if ($scope.currentForm == form) {
      return true;
    }
  };

  $scope.submitSection = function(form, destination) {
    console.log(form.$submitted);
    console.log(form.$valid);
    if(form.$valid) {
      $('#' + $scope.currentForm + ' .glyphicon').css({color: 'green'});
      var section = $scope.currentForm + 'Complete'

      $scope.nextForm($scope.currentForm, destination);
      // console.log(section)
      $scope[section] = true;
      // console.log($scope[section]);
    }
    else {
      $('#' + $scope.currentForm + ' i').css({color: 'lightgrey'});
      console.log('Not Valid');
    }
  };

  $(document).ready(function(){
    $('[data-toggle="popover"]').popover();
  });

  $scope.checkGrip=function() {
    if ($scope.bilateral==='bilateral') {
      $scope.left=0;
      $scope.right=0;
    }
  }

  $scope.checkPupils=function() {
    if ($scope.pupils_bilateral==='bilateral') {
      $scope.pupils_left=0;
      $scope.pupils_right=0;
    }
  };

  $scope.orders=[];



  $scope.newOrder=function(currentForm) {
    console.log(currentForm)
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'NewOrderController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/newOrder/newOrder.html',
        size: 'md',
        // resolve: {
        //   getPatientMedicationList: function() {
        //     return $scope.getPatientMedicationList;
        //   }
        // }
        resolve: {
            // form: function() {
            //   return currentForm;
            // }
            form: $scope.form,
            formAdded: false,
        }
      });
      modalInstance.result.then(function (data) {
        console.log(data);
        getOrderList(data);
      });
  };


  function getOrderList(data) {
    orders.push(data);
    $scope.orders=orders;
  }


  //get form obj if exists
  dataService.get('skilledNursingNote', {FORMID: $scope.form.FORMID}).then(function(data){
    console.log(data.data[0]);
    if(data.data.length>0){
      var note=data.data[0];

      if (note.completeTemp===1) {
        $scope.addTemp=true;
        $scope.check_temp=true;
        $scope.completeTemp=note.completeTemp;
        $scope.count_temp=note.count_temp;
        $scope.temp_type=note.temp_type;
        $scope.tempNotebox=note.tempNotebox;
        if(note.completeTemp===1) {
          $scope.alert=true;
        }
      }
      if (note.completeVitPulse===1) {
        $scope.addVitPulse=true;
        $scope.check_vitPulse=true;
        $scope.completeVitPulse=note.completeVitPulse;
        $scope.count_pulse=note.count_pulse;
        $scope.vitPulse_type=note.vitPulse_type;
        $scope.vitPulse_unit=note.vitPulse_unit;
        $scope.pulseNotebox=note.pulseNotebox;
      }
      if (note.completeResp===1) {
        $scope.addResp=true;
        $scope.check_resp=true;
        $scope.completeResp=note.completeResp;
        $scope.count_resp=note.count_resp;
        $scope.resp_type=note.resp_type;
      }
      if (note.completeBP===1) {
        $scope.addBP=true;
        $scope.check_BP=true;
        $scope.completeBP=note.completeBP;
        if (note.sitting===1) {
          $scope.sitting=note.sitting;
          $scope.count_systolic_1=note.count_systolic_1;
          $scope.count_diastolic_1=note.count_diastolic_1;
          $scope.BP_type_1=note.BP_type_1;
        }
        if (note.standing===1) {
          $scope.standing=note.standing;
          $scope.count_systolic_2=note.count_systolic_2;
          $scope.count_diastolic_2=note.count_diastolic_2;
          $scope.BP_type_2=note.BP_type_2;
        }
        if (note.lying===1) {
          $scope.lying=note.lying;
          $scope.count_systolic_3=note.count_systolic_3;
          $scope.count_diastolic_3=note.count_diastolic_3;
          $scope.BP_type_3=note.BP_type_3;
        }
      }
      if (note.completeWeight===1) {
        $scope.addWeight=true;
        $scope.check_weight=true;
        $scope.completeWeight=note.completeWeight;
        $scope.count_weight=note.count_weight;
      }
      if (note.completeSugar===1) {
        $scope.addSugar=true;
        $scope.check_sugar=true;
        $scope.completeSugar=note.completeSugar;
        $scope.count_sugar=note.count_sugar;
        $scope.BP_type=note.BP_type;
      }
      if (note.completeINR===1){
        $scope.addINR=true;
        $scope.check_INR=true;
        $scope.completeINR=note.completeINR;
        $scope.count_INR=note.count_INR;
      }
      if (note.completePainLoc===1) {
        $scope.addPainLoc=true;
        $scope.check_painLoc=true;
        $scope.completePainLoc=note.completePainLoc;
        $scope.pain_location=note.pain_location;
      }
      if (note.completePainCause===1) {
        $scope.addPainCause=true;
        $scope.check_painCause=true;
        $scope.completePainCause=note.completePainCause;
        $scope.pain_cause=note.pain_cause;
      }
      if (note.completePainLevel===1) {
        $scope.addPainLevel=true;
        $scope.check_painLevel=true;
        $scope.completePainLevel=note.completePainLevel;
        $scope.painScaleType=note.painScaleType;
        $scope.intensity=parseInt(note.intensity);
        $scope.interventionNumeric=note.interventionNumeric;
        $scope.notifyNumeric=note.notifyNumeric;
      }
      if (note.completePainDescription===1) {
        $scope.addPainLevel=true;
        $scope.check_painDescription=true;
        $scope.completePainDescription=note.completePainDescription;
        $scope.description=note.description;
        $scope.painObservations=note.painObservations;
      }


	  $scope.capillary=note.capillary;
	  $scope.cyanosis = note.cyanosis;
	  $scope.palipitation = note.palipitation;
	  $scope.pulseIntensity = note.pulseIntensity;
	  $scope.syncope_when = note.syncope_when;
	  $scope.completeSyncope = note.completeSyncope;
	  $scope.weightBearing = note.weightBearing;
	  $scope.weightLocation = note.weightLocation;
	  $scope.contractures_location = note.contractures_location;
	  $scope.weakness_location = note.weakness_location;
	  $scope.fractureSlider = note.fractureSlider;
	  $scope.fracture_location = note.fracture_location;
	  $scope.bilateral = note.bilateral;
	  $scope.gripStrength = note.gripStrength;
	  $scope.motorSlider = note.motorSlider;
	  $scope.motorSkills = note.motorSkills;
	  $scope.ROM_location = note.ROM_location;
	  $scope.mobility = note.mobility;
	  $scope.device_cane = note.device_cane;
	  $scope.device_crutches = note.device_crutches;
	  $scope.device_walker = note.device_walker;
	  $scope.device_wheelchair = note.device_wheelchair;
	  $scope.device_other = note.device_other;
	  $scope.jointPain_location = note.jointPain_location;
	  $scope.amputation_location = note.amputation_location;
	  $scope.poorBalancelider = note.poorBalancelider;
	  $scope.jointStiffnesslider = note.jointStiffnesslider;
	  $scope.turgor = note.turgor;
	  $scope.count_ambulation = note.count_ambulation;
	  $scope.dyspnea_rest = note.dyspnea_rest;
	  $scope.dyspnea_ADL = note.dyspnea_ADL;
	  $scope.dyspnea_activity = note.dyspnea_activity;
	  $scope.breath_CTA = note.breath_CTA;
	  $scope.breath_wheezes = note.breath_wheezes;
	  $scope.breath_rales = note.breath_rales;
	  $scope.breath_rhonchi = note.breath_rhonchi;
	  $scope.breath_diminished = note.breath_diminished;
	  $scope.oxygen_type = note.oxygen_type;
	  $scope.cough_dry = note.cough_dry;
	  $scope.cough_chronic = note.cough_chronic;
	  $scope.cough_smoker = note.cough_smoker;
	  $scope.cough_productive = note.cough_productive;
	  $scope.cough_non_productive = note.cough_non_productive;
	  $scope.trach_size = note.trach_size;
	  $scope.trach_date = note.trach_date;
	  $scope.trach_ob = note.trach_ob;
	  $scope.dysphagiaSlider = note.dysphagiaSlider;
	  $scope.tubeCheckSlider = note.tubeCheckSlider;
	  $scope.appetiteSlider = note.appetiteSlider;
	  $scope.residualChecked = note.residualChecked;
	  $scope.residualAmt = note.residualAmt;
	  $scope.diet = note.diet;
      $scope.weight = note.weight;
      $scope.diet_type = note.diet_type;
      $scope.externalFeeding = note.externalFeeding;
      $scope.nutr_observation = note.nutr_observation;
      $scope.bowel = note.bowel;
      $scope.abdominal = note.abdominal;
      $scope.bowelIncSlider = note.bowelIncSlider;
      $scope.nauseaSlider = note.nauseaSlider;
      $scope.vomittingSlider = note.vomittingSlider;
      $scope.gerdSlider = note.gerdSlider;
      $scope.abd_girth = note.abd_girth;
      $scope.LBM = note.LBM;
      $scope.LBM_date = note.LBM_date;
      $scope.constipation = note.constipation;
      $scope.constipation_type = note.constipation_type;
      $scope.diarrhea = note.diarrhea;
      $scope.diarrhea_type = note.diarrhea_type;
      $scope.ostomyType = note.ostomyType;
      $scope.ostomyTypeDetails = note.ostomyTypeDetails;
      $scope.stoma = note.stoma;
      $scope.stomaAppearace = note.stomaAppearace;
      $scope.surrounding = note.surrounding;
      $scope.surroundingSkin = note.surroundingSkin;
      $scope.oriented = note.oriented;
      $scope.agitated = note.agitated;
      $scope.agitated_when = note.agitated_when;
      $scope.anxious = note.anxious;

      $scope.depressed = note.depressed;
      $scope.depressed_when = note.depressed_when;
      $scope.paranoid = note.paranoid;
      $scope.paranoid_when = note.paranoid_when;
      $scope.withdrawn = note.withdrawn;
      $scope.withdrawn_when = note.withdrawn_when;
      $scope.forgetful = note.forgetful;
      $scope.forgetful_when = note.forgetful_when;
      $scope.fearful = note.fearful;
      $scope.oriented12 = note.oriented12;
      $scope.phsoOther = note.phsoOther;
      $scope.incont_day = note.incont_day;
	  $scope.incont_night = note.incont_night;
	  $scope.incont_urgency = note.incont_urgency;
	  $scope.incont_freq = note.incont_freq;
	  $scope.incont_hematuria = note.incont_hematuria;
	  $scope.incont_burning = note.incont_burning;
	  $scope.completeColor = note.completeColor;
	  $scope.gen_odor = note.gen_odor;
	  $scope.foley_date = note.foley_date;
	  $scope.foley_ob = note.foley_ob;
	  $scope.exhibit = note.exhibit;
	  $scope.assistance = note.assistance;
	  $scope.severeDyspnea = note.severeDyspnea;
	  $scope.unassisted = note.unassisted;
	  $scope.unsafe = note.unsafe;
	  $scope.unableMedical = note.unableMedical;
	  $scope.homeBoundOther = note.homeBoundOther;
	  $scope.homeBoundStatusOther = note.homeBoundStatusOther;
	  $scope.homeEnvironemnt_note = note.homeEnvironemnt_note;
	  $scope.turgor = note.turgor;

	 $scope.condition_pink = note.condition_pink;
	 $scope.condition_cyanotic = note.condition_cyanotic;
	 $scope.condition_pallor = note.condition_pallor;
	 $scope.condition_jaundice = note.condition_jaundice;
	 $scope.condition_dry = note.condition_dry;
	 $scope.condition_disphoretic = note.condition_disphoretic;
	 $scope.condition_ulcer = note.condition_ulcer;
	 $scope.condition_rash = note.condition_rash;
	 $scope.condition_cellulitis = note.condition_cellulitis;
	 $scope.condition_pressure = note.condition_pressure;
	 $scope.condition_wound = note.condition_wound;
	 $scope.condition_incision = note.condition_incision;
	 $scope.condition_skin_tear = note.condition_skin_tear;
	 $scope.condition_cool = note.condition_cool;

	 $scope.LOCdetails = note.LOCdetails;

	 $scope.LOC_person = note.LOC_person;
	 $scope.LOC_place = note.LOC_place;
	 $scope.LOC_time = note.LOC_time;
	 $scope.behaviorStatus_WNL = note.behaviorStatus_WNL;
	 $scope.behaviorStatus_difficulty = note.behaviorStatus_difficulty;
	 $scope.behaviorStatus_withdrawn = note.behaviorStatus_withdrawn;
	 $scope.behaviorStatus_combative = note.behaviorStatus_combative;
	 $scope.behaviorStatus_depression = note.behaviorStatus_depression;
	 $scope.behaviorStatus_irritability = note.behaviorStatus_irritability;
	 $scope.behaviorStatus_impaired = note.behaviorStatus_impaired;
	$scope.behaviorStatus_other = note.behaviorStatus_other;

	$scope.pupils_bilateral = note.pupils_bilateral;
	$scope.pupils_left = note.pupils_left;

	$scope.vision_WNL  = note.vision_WNL;
	$scope.vision_blurred  = note.vision_blurred;
	$scope.vision_cataracts  = note.vision_cataracts;
	$scope.vision_lenses  = note.vision_lenses;
	$scope.vision_glaucoma  = note.vision_glaucoma;
	$scope.vision_blind  = note.vision_blind;
	$scope.speech = note.speech;
	$scope.paralysis_note = note.paralysis_note;
	$scope.quadriplegiaslider = note.quadriplegiaslider;
	$scope.seizuresslider = note.seizuresslider;
	$scope.tremors_note = note.tremors_note;
	$scope.dizzinesslider = note.dizzinesslider;
	$scope.headache_bilateral = note.headache_bilateral;
	$scope.headache_left = note.headache_left;
	$scope.headache_right = note.headache_right;
	$scope.headacheComments = note.headacheComments;
	$scope.IVAccess = note.IVAccess;
	$scope.ivLocationDetail = note.ivLocationDetail;
	$scope.IVCondition = note.IVCondition;
	$scope.IVDressing = note.IVDressing;
	$scope.IVChnaged = note.IVChnaged;
	$scope.flushslider = note.flushslider;
	$scope.flushedWithDetail = note.flushedWithDetail;
	$scope.flushedWith = note.flushedWith;
	$scope.progress_note = note.progress_note;
	$scope.medTeachingNote = note.medTeachingNote;
	$scope.woundTeachingNote = note.woundTeachingNote;
	$scope.otherTeachingNote = note.otherTeachingNote;
	$scope.labs_note = note.labs_note;

	$scope.numAttempts = note.numAttempts;
	$scope.site = note.site;
$scope.ga = note.ga;
$scope.minutes = note.minutes;
$scope.ptProcedure = note.ptProcedure;
$scope.bruising = note.bruising;
$scope.bleeding = note.bleeding;
$scope.disposal = note.disposal;
$scope.lab = note.lab;
$scope.hospital = note.hospital;
$scope.MD = note.MD;
$scope.phlebotomyOtherDetails = note.phlebotomyOtherDetails;
$scope.minutes = note.minutes;
$scope.phlebotomyOther = note.phlebotomyOther;
$scope.comments_note = note.comments_note;

$scope.skilledObservation = note.skilledObservation;
$scope.foleyChange = note.foleyChange;
$scope.prep = note.prep;
$scope.IVsite = note.IVsite;
$scope.woundCare = note.woundCare;
$scope.venipunctureLab = note.venipunctureLab;
$scope.safetyPrecautions = note.safetyPrecautions;
$scope.IVtubing = note.IVtubing;
$scope.adminsterEnteral = note.adminsterEnteral;
$scope.IMinjection = note.IMinjection;
$scope.IVsiteChange = note.IVsiteChange;
$scope.instructedMed = note.instructedMed;
$scope.medicationAdmin = note.medicationAdmin;
$scope.patientCG = note.patientCG;
$scope.glucometer = note.glucometer;
$scope.peg = note.peg;
$scope.foleyIrrigation = note.foleyIrrigation;
$scope.instructedDisease = note.instructedDisease;
$scope.diabeticMonitor = note.diabeticMonitor;
$scope.emergency = note.emergency;
$scope.trachea = note.trachea;
$scope.footCare = note.footCare;
$scope.dietTeaching = note.dietTeaching;
$scope.interventionComments_note = note.interventionComments_note;

$scope.skilledObservationDetail = note.skilledObservationDetail;
$scope.foleyChangeDetail = note.foleyChangeDetail;
$scope.prepDetail = note.prepDetail;
$scope.IVsiteDetail = note.IVsiteDetail;
$scope.woundCareDetail = note.woundCareDetail;
$scope.venipunctureLabDetail = note.venipunctureLabDetail;
$scope.safetyPrecautionsDetail = note.safetyPrecautionsDetail;
$scope.IVtubingDetail = note.IVtubingDetail;
$scope.adminsterEnteralDetail = note.adminsterEnteralDetail;
$scope.IMinjectionDetail = note.IMinjectionDetail;
$scope.IVsiteChangeDetail = note.IVsiteChange;
$scope.instructedMed = note.instructedMedDetail;
$scope.medicationAdminDetail = note.medicationAdminDetail;
$scope.patientCGDetail = note.patientCGDetail;
$scope.glucometerDetail = note.glucometerDetail;
$scope.pegDetail = note.pegDetail;
$scope.foleyIrrigationDetail = note.foleyIrrigationDetail;
$scope.instructedDiseaseDetail = note.instructedDiseaseDetail;
$scope.diabeticMonitorDetail = note.diabeticMonitorDetail;
$scope.emergencyDetail = note.emergencyDetail;
$scope.tracheaDetail = note.tracheaDetail;
$scope.footCareDetail = note.footCareDetail;
$scope.dietTeachingDetailDetail = note.dietTeachingDetailDetail;
$scope.instructedMed = note.instructedMed;
$scope.instructedMedDetail = note.instructedMedDetail;
$scope.newInfection = note.newInfection;
$scope.universal = note.universal;
$scope.sharpsWaste = note.sharpsWaste;
$scope.infectionLogDate  = note.infectionLogDate;
$scope.treatmentPrescribed = note.treatmentPrescribed;
$scope.infectionType  = note.infectionType;
$scope.infectionTreatment  = note.infectionTreatment;
$scope.infectionNarrative  = note.infectionNarrative;

$scope.WD = note.WD;
$scope.treatment_location = note.treatment_location;
$scope.treatment_stage = note.treatment_stage;
$scope.treatment_notes = note.treatment_notes;

$scope.bloodSugarAMCount = note.bloodSugarAMCount;
$scope.bloodSugarAM = note.bloodSugarAM;
$scope.bloodSugarNoonCount = note.bloodSugarNoonCount;
$scope.bloodSugarNoon = note.bloodSugarNoon;
$scope.bloodSugarPMCount = note.bloodSugarPMCount;
$scope.bloodSugarPM = note.bloodSugarPM;
$scope.bloodSugarHSCount = note.bloodSugarHSCount;
$scope.bloodSugarHS = note.bloodSugarHS;
$scope.bloodSugarPerformedBy = note.bloodSugarPerformedBy;
$scope.bloodSugarSiteLocation = note.bloodSugarSiteLocation;
$scope.bloodSugarSiteNote = note.bloodSugarSiteNote;

$scope.diaManagement_diet = note.diaManagement_diet;
$scope.diaManagement_exercise = note.diaManagement_exercise;
$scope.diaManagement_oral = note.diaManagement_oral;
$scope.diaManagement_insulin = note.diaManagement_insulin;
$scope.diaManagement_na = note.diaManagement_na;
$scope.diaManagement_patient = note.diaManagement_patient;
$scope.diaManagement_caregiver = note.diaManagement_caregiver;
$scope.diaManagement_SN = note.diaManagement_SN;

$scope.SShyperglycemia_fatigue = note.SShyperglycemia_fatigue;
$scope.SShyperglycemia_polydipsia = note.SShyperglycemia_polydipsia;
$scope.SShyperglycemia_polyphagia = note.SShyperglycemia_polyphagia;
$scope.SShyperglycemia_vision = note.SShyperglycemia_vision;
$scope.SShyperglycemia_polyuria = note.SShyperglycemia_polyuria;
$scope.SShyperglycemia_other = note.SShyperglycemia_other;
$scope.SShyperglycemia_note = note.SShyperglycemia_note;

$scope.SShypoglycemia_anxious = note.SShypoglycemia_anxious;
$scope.SShypoglycemia_fatigue = note.SShypoglycemia_fatigue;
$scope.SShypoglycemia_weakness  = note.SShypoglycemia_weakness;
$scope.SShypoglycemia_dizziness = note.SShypoglycemia_dizziness;
$scope.SShypoglycemia_perspiration  = note.SShypoglycemia_perspiration;
$scope.SShypoglycemia_other  = note.SShypoglycemia_other;
$scope.SShypoglycemia_note = note.SShypoglycemia_note;

$scope.insulinType1 = note.insulinType1;
$scope.insulinDose1 = note.insulinDose1;
$scope.insulinSite1 = note.insulinSite1;
$scope.insulinRoute1 = note.insulinRoute1;
$scope.insulinType2 = note.insulinType2;
$scope.insulinDose2 = note.insulinDose2;
$scope.insulinSite2 = note.insulinSite2;
$scope.insulinRoute2 = note.insulinRoute2;

$scope.verbalize = note.verbalize;
$scope.responseVerbal = note.responseVerbal;
$scope.returnSlider = note.returnSlider;
$scope.responseReturn = note.responseReturn;
$scope.tolerate = note.tolerate;
$scope.infusion = note.infusion;
$scope.responseOther = note.responseOther;
$scope.responseTeachingOther = note.responseTeachingOther;

$scope.COC_reason = note.COC_reason;
$scope.COC_MD = note.COC_MD;
$scope.COC_SN = note.COC_SN;
$scope.COC_LPN = note.COC_LPN;
$scope.COC_PT = note.COC_PT;
$scope.COC_OT = note.COC_OT;
$scope.COC_ST = note.COC_ST;
$scope.COC_HHA = note.COC_HHA;

$scope.POC_note = note.POC_note;
$scope.POC_order = note.POC_order;
$scope.POC_review = note.POC_review;
$scope.POC_understanding = note.POC_understanding;
$scope.discharge_note = note.discharge_note;
$scope.appetiteCheckSlider = note.appetiteCheckSlider;
$scope.count_meal = note.count_meal;
$scope.count_eaten = note.count_eaten;
$scope.count_fluid = note.count_fluid;
$scope.count_voids = note.count_voids;
$scope.count_voidAMT = note.count_voidAMT;
$scope.count_size = note.count_size;
$scope.count_FR = note.count_FR;
$scope.signature = note.signature;

if (note.signature_date) {
  $scope.signature_date = new Date($filter('date')(note.signature_date, 'yyyy/MM/dd'));
}







	  if (note.completeChestPain===1)
	  {
	  	$scope.count_duration=note.chestpain_duration;
	  	$scope.count_prn=note.chestpain_prn;
	  	$scope.completeChestPain = note.completeChestPain;
	  }

	  if (note.completeEdema===1)
	  {
		  $scope.completeEdema = note.completeEdema;
		  $scope.pitting = note.pitting;
		  $scope.pittingLocation = note.pittingLocation;
	  }



    }

  });

  $scope.saveAndContinueLater = function() {
    //check to see if form has objectkeyid
    dataService.get('form', {KEYID:$scope.form.FORMID}).then(function (data) {

      console.log(data.data[0]);
      var form=data.data[0];
      $scope.dataSaved=true;

      if (form.OBJKEYID===null) {  //  if form.objkeyid is null, then record has not been created
        createNursingNote(); //create record
      } else {
        editNursingNote(form.OBJKEYID); //if form.objkeyid exists, then record exists, edit record
      }
    });
  };


  function createNursingNote() {
    var newNursingNote = {
      FORMID: $scope.form.FORMID,
      completeTemp: $scope.completeTemp,
      completeVitPulse: $scope.completeVitPulse,
      completeResp: $scope.completeResp,
      completeBP: $scope.completeBP,
      completeWeight: $scope.completeWeight,
      completeSugar: $scope.completeSugar,
      completeINR: $scope.completeINR,
      completePainLoc: $scope.completePainLoc,
      completePainCause: $scope.completePainCause,
      completePainDescription: $scope.completePainDescription,
      signature: $scope.signature,
      signature_date: $filter('date')($scope.signature_date, 'yyyy/MM/dd')
    };

    if ($scope.completeTemp===1) {
      newNursingNote.count_temp=$scope.count_temp;
      newNursingNote.temp_type=$scope.temp_type;
      newNursingNote.tempNotebox=$scope.tempNotebox;
      newNursingNote.notify=$scope.notify;
    }

    if ($scope.completeVitPulse===1) {
      newNursingNote.count_pulse=$scope.count_pulse;
      newNursingNote.vitPulse_type=$scope.vitPulse_type;
      newNursingNote.vitPulse_unit=$scope.vitPulse_unit;
      newNursingNote.pulseNotebox=$scope.pulseNotebox;
    }

    if ($scope.completeResp===1) {
      newNursingNote.count_resp=$scope.count_resp;
      newNursingNote.resp_type=$scope.resp_type;
    }

    if ($scope.completeBP===1) {
      newNursingNote.sitting=$scope.sitting;
      newNursingNote.standing=$scope.standing;
      newNursingNote.lying=$scope.lying;

      if ($scope.sitting===1) {
        newNursingNote.count_systolic_1=$scope.count_systolic_1;
        newNursingNote.count_diastolic_1=$scope.count_diastolic_1;
        newNursingNote.BP_type_1=$scope.BP_type_1;
      }

      if ($scope.standing===1) {
        newNursingNote.count_systolic_2=$scope.count_systolic_2;
        newNursingNote.count_diastolic_2=$scope.count_diastolic_2;
        newNursingNote.BP_type_2=$scope.BP_type_2;
      }

      if ($scope.lying===1) {
        newNursingNote.count_systolic_3=$scope.count_systolic_3;
        newNursingNote.count_diastolic_3=$scope.count_diastolic_3;
        newNursingNote.BP_type_3=$scope.BP_type_3;
      }
    }

    if ($scope.completeWeight===1) {
      newNursingNote.count_weight=$scope.count_weight;
    }

    if ($scope.completeSugar===1) {
      newNursingNote.count_sugar=$scope.count_sugar;
      newNursingNote.BP_type=$scope.BP_type;
    }

    if ($scope.completeINR===1){
      newNursingNote.count_INR=$scope.count_INR;
    }

    if ($scope.completePainLoc===1) {
      newNursingNote.pain_location=$scope.pain_location;
    }

    if ($scope.completePainCause===1) {
      newNursingNote.pain_cause=$scope.pain_cause;
    }

    if ($scope.completePainLevel===1) {
      newNursingNote.painScaleType=$scope.painScaleType;
      newNursingNote.intensity=parseInt($scope.intensity);
      newNursingNote.interventionNumeric=$scope.interventionNumeric;
      newNursingNote.notifyNumeric=$scope.notifyNumeric;
    }

    if ($scope.completePainDescription===1) {
      newNursingNote.description=$scope.description;
      newNursingNote.painObservations=$scope.painObservations;
    }


    console.log("HERE " + newNursingNote);
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['skilledNursingNote'].add,
      method: 'POST',
      data: $httpParamSerializerJQLike(newNursingNote),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function(response){
      console.log(response);
      //if saving success, edit form to contain objectkeyid
      if (response.data.status==='success') {
        // var formkeyid = parseInt(response.data['form keyid']);
        //edit form to contain objectkeyid
        var form = {
          KEYID: $scope.form.FORMID,
          OBJKEYID: response.data.keyid,
          ASSIGNEDTO: $scope.form.ASSIGNEDTO,
          STATUS: 2,
        };

        console.log(form);

        dataService.edit('form', form).then(function (response) {
          console.log(response);

          if ($scope.submitQA===true) { //if submitting to qa
            $scope.submitToQA();
          } else {
            $state.go('records', {}, { reload: true })
          }
        });
      }
    }, function(data) {
      console.log(data);
    });

  }

  function editNursingNote(nursingNoteKeyId) {
    var nursingNoteEdits = {
      KEYID: nursingNoteKeyId,
      FORMID: $scope.form.FORMID,
      completeTemp: $scope.completeTemp,
      completeVitPulse: $scope.completeVitPulse,
      completeResp: $scope.completeResp,
      completeBP: $scope.completeBP,
      completeWeight: $scope.completeWeight,
      completeSugar: $scope.completeSugar,
      completeINR: $scope.completeINR,
      completePainLoc: $scope.completePainLoc,
      completePainCause: $scope.completePainCause,
      completePainLevel: $scope.completePainLevel,
      completePainDescription: $scope.completePainDescription,
      completeChestPain:$scope.completeChestPain,
      completeEdema:$scope.completeEdema,
      signature: $scope.signature,
      signature_date: $filter('date')($scope.signature_date, 'yyyy/MM/dd')
    };

    if ($scope.completeTemp) {
      nursingNoteEdits.completeTemp=$scope.completeTemp;
      nursingNoteEdits.count_temp=$scope.count_temp;
      nursingNoteEdits.temp_type=$scope.temp_type;
      nursingNoteEdits.tempNotebox=$scope.tempNotebox;
      nursingNoteEdits.notify=$scope.notify;
    }

    if ($scope.completeVitPulse) {
      nursingNoteEdits.count_pulse=$scope.count_pulse;
      nursingNoteEdits.vitPulse_type=$scope.vitPulse_type;
      nursingNoteEdits.vitPulse_unit=$scope.vitPulse_unit;
      nursingNoteEdits.pulseNotebox=$scope.pulseNotebox;
    }

    if ($scope.completeResp) {
      nursingNoteEdits.count_resp=$scope.count_resp;
      nursingNoteEdits.resp_type=$scope.resp_type;
    }

    if ($scope.completeBP) {
      nursingNoteEdits.sitting=$scope.sitting;
      nursingNoteEdits.standing=$scope.standing;
      nursingNoteEdits.lying=$scope.lying;

      if ($scope.sitting===1) {
        nursingNoteEdits.count_systolic_1=$scope.count_systolic_1;
        nursingNoteEdits.count_diastolic_1=$scope.count_diastolic_1;
        nursingNoteEdits.BP_type_1=$scope.BP_type_1;
      }

      if ($scope.standing===1) {
        nursingNoteEdits.count_systolic_2=$scope.count_systolic_2;
        nursingNoteEdits.count_diastolic_2=$scope.count_diastolic_2;
        nursingNoteEdits.BP_type_2=$scope.BP_type_2;
      }

      if ($scope.lying===1) {
        nursingNoteEdits.count_systolic_3=$scope.count_systolic_3;
        nursingNoteEdits.count_diastolic_3=$scope.count_diastolic_3;
        nursingNoteEdits.BP_type_3=$scope.BP_type_3;
      }
    }

    if ($scope.completeWeight) {
      nursingNoteEdits.count_weight=$scope.count_weight;
    }

    if ($scope.completeSugar) {
      nursingNoteEdits.count_sugar=$scope.count_sugar;
      nursingNoteEdits.BP_type=$scope.BP_type;
    }

    if ($scope.completeINR){
      nursingNoteEdits.count_INR=$scope.count_INR;
    }

    if ($scope.completePainLoc) {
      nursingNoteEdits.pain_location=$scope.pain_location;
    }

    if ($scope.completePainCause) {
      nursingNoteEdits.pain_cause=$scope.pain_cause;
    }

    if ($scope.completePainLevel) {
      nursingNoteEdits.painScaleType=$scope.painScaleType;
      nursingNoteEdits.intensity=$scope.intensity;
      nursingNoteEdits.interventionNumeric=$scope.interventionNumeric;
      nursingNoteEdits.notifyNumeric=$scope.notifyNumeric;
    }

    if ($scope.completePainDescription) {
      nursingNoteEdits.description=$scope.description;
      nursingNoteEdits.painObservations=$scope.painObservations;
    }


    nursingNoteEdits.capillary = $scope.capillary;
    nursingNoteEdits.cyanosis = $scope.cyanosis;
    nursingNoteEdits.pitting = $scope.pitting;
    nursingNoteEdits.palipitation = $scope.palipitation;
    nursingNoteEdits.pulseIntensity = $scope.pulseIntensity;
    nursingNoteEdits.syncope_when = $scope.syncope_when;
    nursingNoteEdits.completeSyncope = $scope.completeSyncope;
    nursingNoteEdits.pittingLocation = $scope.pittingLocation;
    nursingNoteEdits.weightBearing = $scope.weightBearing;
    nursingNoteEdits.weightLocation = $scope.weightLocation;
    nursingNoteEdits.contractures_location = $scope.contractures_location;
    nursingNoteEdits.weakness_location = $scope.weakness_location;
    nursingNoteEdits.fractureSlider = $scope.fractureSlider;
    nursingNoteEdits.fracture_location = $scope.fracture_location;
    nursingNoteEdits.bilateral = $scope.bilateral;
    nursingNoteEdits.gripStrength = $scope.gripStrength;
    nursingNoteEdits.motorSlider = $scope.motorSlider;
    nursingNoteEdits.motorSkills = $scope.motorSkills;
    nursingNoteEdits.ROM_location = $scope.ROM_location;
    nursingNoteEdits.mobility = $scope.mobility;
    nursingNoteEdits.device_cane = $scope.device_cane;
    nursingNoteEdits.device_crutches = $scope.device_crutches;
    nursingNoteEdits.device_walker = $scope.device_walker;
    nursingNoteEdits.device_wheelchair = $scope.device_wheelchair;
    nursingNoteEdits.device_other = $scope.device_other;
    nursingNoteEdits.jointPain_location = $scope.jointPain_location;
    nursingNoteEdits.amputation_location = $scope.amputation_location;
    nursingNoteEdits.poorBalancelider = $scope.poorBalancelider;
    nursingNoteEdits.jointStiffnesslider = $scope.jointStiffnesslider;
    nursingNoteEdits.turgor = $scope.turgor;
    nursingNoteEdits.count_ambulation = $scope.count_ambulation;
    nursingNoteEdits.dyspnea_rest = $scope.dyspnea_rest;
    nursingNoteEdits.dyspnea_ADL = $scope.dyspnea_ADL;
    nursingNoteEdits.dyspnea_activity = $scope.dyspnea_activity;
    nursingNoteEdits.breath_CTA = $scope.breath_CTA;
    nursingNoteEdits.breath_wheezes = $scope.breath_wheezes;
    nursingNoteEdits.breath_rales = $scope.breath_rales;
    nursingNoteEdits.breath_rhonchi = $scope.breath_rhonchi;
    nursingNoteEdits.breath_diminished = $scope.breath_diminished;
    nursingNoteEdits.oxygen_type = $scope.oxygen_type;
    nursingNoteEdits.cough_dry = $scope.cough_dry;
    nursingNoteEdits.cough_chronic = $scope.cough_chronic;
    nursingNoteEdits.cough_smoker = $scope.cough_smoker;
    nursingNoteEdits.cough_productive = $scope.cough_productive;
    nursingNoteEdits.cough_non_productive = $scope.cough_non_productive;
    nursingNoteEdits.trach_size = $scope.trach_size;
    nursingNoteEdits.trach_date = $scope.trach_date;
    nursingNoteEdits.trach_ob = $scope.trach_ob;
    nursingNoteEdits.dysphagiaSlider = $scope.dysphagiaSlider;
    nursingNoteEdits.tubeCheckSlider = $scope.tubeCheckSlider;
    nursingNoteEdits.appetiteSlider = $scope.appetiteSlider;
    nursingNoteEdits.residualChecked = $scope.residualChecked;
    nursingNoteEdits.residualAmt = $scope.residualAmt;
    nursingNoteEdits.diet = $scope.diet;
    nursingNoteEdits.weight = $scope.weight;
    nursingNoteEdits.diet_type = $scope.diet_type;
    nursingNoteEdits.externalFeeding = $scope.externalFeeding;
    nursingNoteEdits.nutr_observation = $scope.nutr_observation;
    nursingNoteEdits.bowel = $scope.bowel;
    nursingNoteEdits.abdominal = $scope.abdominal;
    nursingNoteEdits.bowelIncSlider = $scope.bowelIncSlider;
    nursingNoteEdits.nauseaSlider = $scope.nauseaSlider;
    nursingNoteEdits.vomittingSlider = $scope.vomittingSlider;
    nursingNoteEdits.gerdSlider = $scope.gerdSlider;
    nursingNoteEdits.abd_girth = $scope.abd_girth;
    nursingNoteEdits.LBM = $scope.LBM;
    nursingNoteEdits.LBM_date = $scope.LBM_date;
    nursingNoteEdits.constipation = $scope.constipation;
    nursingNoteEdits.constipation_type = $scope.constipation_type;
    nursingNoteEdits.diarrhea = $scope.diarrhea;
    nursingNoteEdits.diarrhea_type = $scope.diarrhea_type;
    nursingNoteEdits.ostomyType = $scope.ostomyType;
    nursingNoteEdits.ostomyTypeDetails = $scope.ostomyTypeDetails;
    nursingNoteEdits.stoma = $scope.stoma;
    nursingNoteEdits.stomaAppearace = $scope.stomaAppearace;
    nursingNoteEdits.surrounding = $scope.surrounding;
    nursingNoteEdits.surroundingSkin = $scope.surroundingSkin;
    nursingNoteEdits.oriented = $scope.oriented;
    nursingNoteEdits.anxious_when = $scope.anxious_when;
    nursingNoteEdits.agitated = $scope.agitated;
    nursingNoteEdits.agitated_when = $scope.agitated_when;
    nursingNoteEdits.anxious = $scope.anxious;

    nursingNoteEdits.depressed_when = $scope.depressed_when;
    nursingNoteEdits.depressed = $scope.depressed;

    nursingNoteEdits.paranoid_when = $scope.paranoid_when;
    nursingNoteEdits.paranoid = $scope.paranoid;

    nursingNoteEdits.withdrawn_when = $scope.withdrawn_when;
    nursingNoteEdits.withdrawn = $scope.withdrawn;

    nursingNoteEdits.forgetful_when = $scope.forgetful_when;
    nursingNoteEdits.forgetful = $scope.forgetful;

    nursingNoteEdits.fearful = $scope.fearful;
    nursingNoteEdits.oriented12 = $scope.oriented12;
    nursingNoteEdits.phsoOther = $scope.phsoOther;
    nursingNoteEdits.incont_day = $scope.incont_day;
	nursingNoteEdits.incont_night = $scope.incont_night;
	nursingNoteEdits.incont_urgency =  $scope.incont_urgency;
	nursingNoteEdits.incont_freq = $scope.incont_freq;
	nursingNoteEdits.incont_hematuria = $scope.incont_hematuria;
	nursingNoteEdits.incont_burning = $scope.incont_burning;
	nursingNoteEdits.completeColor = $scope.completeColor;
	nursingNoteEdits.gen_odor = $scope.gen_odor;
	nursingNoteEdits.foley_date = $scope.foley_date;
	nursingNoteEdits.foley_ob = $scope.foley_ob;
	nursingNoteEdits.exhibit = $scope.exhibit;
	nursingNoteEdits.assistance = $scope.assistance;
	nursingNoteEdits.severeDyspnea = $scope.severeDyspnea;
	nursingNoteEdits.unassisted = $scope.unassisted;
	nursingNoteEdits.unsafe = $scope.unsafe;
	nursingNoteEdits.unableMedical = $scope.unableMedical;
	nursingNoteEdits.homeBoundOther = $scope.homeBoundOther;
	nursingNoteEdits.homeBoundStatusOther = $scope.homeBoundStatusOther;
	nursingNoteEdits.homeEnvironemnt_note = $scope.homeEnvironemnt_note;
	nursingNoteEdits.turgor = $scope.turgor;

	nursingNoteEdits.condition_pink = $scope.condition_pink;
	nursingNoteEdits.condition_cyanotic = $scope.condition_cyanotic;
	nursingNoteEdits.condition_pallor = $scope.condition_pallor;
	nursingNoteEdits.condition_jaundice = $scope.condition_jaundice;
	nursingNoteEdits.condition_dry = $scope.condition_dry;
	nursingNoteEdits.condition_disphoretic = $scope.condition_disphoretic;
	nursingNoteEdits.condition_ulcer = $scope.condition_ulcer;
	nursingNoteEdits.condition_rash = $scope.condition_rash;
	nursingNoteEdits.condition_cellulitis = $scope.condition_cellulitis;
	nursingNoteEdits.condition_pressure = $scope.condition_pressure;
	nursingNoteEdits.condition_wound = $scope.condition_wound;
	nursingNoteEdits.condition_incision = $scope.condition_incision;
	nursingNoteEdits.condition_skin_tear = $scope.condition_skin_tear;
	nursingNoteEdits.condition_cool = $scope.condition_cool;
	nursingNoteEdits.LOCdetails = $scope.LOCdetails;

	nursingNoteEdits.LOC_person = $scope.LOC_person;
	nursingNoteEdits.LOC_place = $scope.LOC_place;
	nursingNoteEdits.LOC_time = $scope.LOC_time;

	nursingNoteEdits.behaviorStatus_WNL = $scope.behaviorStatus_WNL;
	nursingNoteEdits.behaviorStatus_difficulty = $scope.behaviorStatus_difficulty;
	nursingNoteEdits.behaviorStatus_withdrawn = $scope.behaviorStatus_withdrawn;
	nursingNoteEdits.behaviorStatus_combative = $scope.behaviorStatus_combative;
	nursingNoteEdits.behaviorStatus_depression = $scope.behaviorStatus_depression;
	nursingNoteEdits.behaviorStatus_irritability = $scope.behaviorStatus_irritability;
	nursingNoteEdits.behaviorStatus_impaired = $scope.behaviorStatus_impaired;
	nursingNoteEdits.behaviorStatus_other = $scope.behaviorStatus_other;

	nursingNoteEdits.pupils_bilateral = $scope.pupils_bilateral;
	nursingNoteEdits.pupils_left = $scope.pupils_left;
	nursingNoteEdits.pupils_right = $scope.pupils_right;
	nursingNoteEdits.pupil_details = $scope.pupil_details;

	nursingNoteEdits.vision_WNL  = $scope.vision_WNL;
	nursingNoteEdits.vision_blurred  = $scope.vision_blurred;
	nursingNoteEdits.vision_cataracts  = $scope.vision_cataracts;
	nursingNoteEdits.vision_lenses  = $scope.vision_lenses;
	nursingNoteEdits.vision_glaucoma  = $scope.vision_glaucoma;
	nursingNoteEdits.vision_blind  = $scope.vision_blind;

	nursingNoteEdits.speech = $scope.speech;
	nursingNoteEdits.paralysis_note = $scope.paralysis_note;
	nursingNoteEdits.quadriplegiaslider = $scope.quadriplegiaslider;
	nursingNoteEdits.seizuresslider = $scope.seizuresslider;

	nursingNoteEdits.tremors_note = $scope.tremors_note;
	nursingNoteEdits.dizzinesslider = $scope.dizzinesslider;
	nursingNoteEdits.headache_bilateral = $scope.headache_bilateral;
	nursingNoteEdits.headache_left = $scope.headache_left;
	nursingNoteEdits.headache_right = $scope.headache_right;
	nursingNoteEdits.headacheComments = $scope.headacheComments;

	nursingNoteEdits.IVAccess = $scope.IVAccess;
	nursingNoteEdits.ivLocationDetail = $scope.ivLocationDetail;
	nursingNoteEdits.IVCondition = $scope.IVCondition;
	nursingNoteEdits.IVDressing = $scope.IVDressing;
	nursingNoteEdits.IVChnaged = $scope.IVChnaged;
	nursingNoteEdits.flushslider = $scope.flushslider;
	nursingNoteEdits.flushedWithDetail = $scope.flushedWithDetail;
	nursingNoteEdits.flushedWith = $scope.flushedWith;
	nursingNoteEdits.progress_note = $scope.progress_note;

	nursingNoteEdits.medTeachingNote = $scope.medTeachingNote;
	nursingNoteEdits.woundTeachingNote = $scope.woundTeachingNote;
	nursingNoteEdits.otherTeachingNote = $scope.otherTeachingNote;
	nursingNoteEdits.labs_note = $scope.labs_note;

	nursingNoteEdits.numAttempts = $scope.numAttempts;
	nursingNoteEdits.site = $scope.site;
	nursingNoteEdits.ga = $scope.ga;
	nursingNoteEdits.minutes = $scope.minutes;
	nursingNoteEdits.ptProcedure = $scope.ptProcedure;
	nursingNoteEdits.bruising = $scope.bruising;
	nursingNoteEdits.bleeding = $scope.bleeding;
	nursingNoteEdits.disposal = $scope.disposal;
	nursingNoteEdits.lab = $scope.lab;
	nursingNoteEdits.hospital = $scope.hospital;
	nursingNoteEdits.MD = $scope.MD;
	nursingNoteEdits.phlebotomyOtherDetails = $scope.phlebotomyOtherDetails;
	nursingNoteEdits.minutes = $scope.minutes;
	nursingNoteEdits.phlebotomyOther = $scope.phlebotomyOther;
	nursingNoteEdits.comments_note = $scope.comments_note;

	nursingNoteEdits.skilledObservation = $scope.skilledObservation;
	nursingNoteEdits.foleyChange = $scope.foleyChange;
	nursingNoteEdits.prep = $scope.prep;
	nursingNoteEdits.IVsite = $scope.IVsite;
	nursingNoteEdits.woundCare = $scope.woundCare;
	nursingNoteEdits.venipunctureLab = $scope.venipunctureLab;
	nursingNoteEdits.safetyPrecautions = $scope.safetyPrecautions;
	nursingNoteEdits.IVtubing = $scope.IVtubing;
	nursingNoteEdits.adminsterEnteral = $scope.adminsterEnteral;
	nursingNoteEdits.IMinjection = $scope.IMinjection;
	nursingNoteEdits.IVsiteChange = $scope.IVsiteChange;
	nursingNoteEdits.instructedMed = $scope.instructedMed;
	nursingNoteEdits.medicationAdmin = $scope.medicationAdmin;
	nursingNoteEdits.patientCG = $scope.patientCG;
	nursingNoteEdits.glucometer = $scope.glucometer;
	nursingNoteEdits.peg = $scope.peg;
	nursingNoteEdits.foleyIrrigation = $scope.foleyIrrigation;
	nursingNoteEdits.instructedDisease = $scope.instructedDisease;
	nursingNoteEdits.diabeticMonitor = $scope.diabeticMonitor;
	nursingNoteEdits.emergency = $scope.emergency;
	nursingNoteEdits.trachea = $scope.trachea;
	nursingNoteEdits.footCare = $scope.footCare;
	nursingNoteEdits.dietTeaching = $scope.dietTeaching;
	nursingNoteEdits.interventionComments_note = $scope.interventionComments_note;

	nursingNoteEdits.skilledObservationDetail = $scope.skilledObservationDetail;
	nursingNoteEdits.foleyChangeDetail = $scope.foleyChangeDetail;
	nursingNoteEdits.prepDetail = $scope.prepDetail;
	nursingNoteEdits.IVsiteDetail = $scope.IVsiteDetail;
	nursingNoteEdits.woundCareDetail = $scope.woundCareDetail;
	nursingNoteEdits.venipunctureLabDetail = $scope.venipunctureLabDetail;
	nursingNoteEdits.safetyPrecautionsDetail = $scope.safetyPrecautionsDetail;
	nursingNoteEdits.IVtubingDetail = $scope.IVtubingDetail;
	nursingNoteEdits.adminsterEnteralDetail = $scope.adminsterEnteralDetail;
	nursingNoteEdits.IMinjectionDetail = $scope.IMinjectionDetail;
	nursingNoteEdits.IVsiteChangeDetail = $scope.IVsiteChange;
	nursingNoteEdits.instructedMed = $scope.instructedMedDetail;
	nursingNoteEdits.medicationAdminDetail = $scope.medicationAdminDetail;
	nursingNoteEdits.patientCGDetail = $scope.patientCGDetail;
	nursingNoteEdits.glucometerDetail = $scope.glucometerDetail;
	nursingNoteEdits.pegDetail = $scope.pegDetail;
	nursingNoteEdits.foleyIrrigationDetail = $scope.foleyIrrigationDetail;
	nursingNoteEdits.instructedDiseaseDetail = $scope.instructedDiseaseDetail;
	nursingNoteEdits.diabeticMonitorDetail = $scope.diabeticMonitorDetail;
	nursingNoteEdits.emergencyDetail = $scope.emergencyDetail;
	nursingNoteEdits.tracheaDetail = $scope.tracheaDetail;
	nursingNoteEdits.footCareDetail = $scope.footCareDetail;
	nursingNoteEdits.dietTeachingDetailDetail = $scope.dietTeachingDetailDetail;
	nursingNoteEdits.instructedMed = $scope.instructedMed;
	nursingNoteEdits.instructedMedDetail = $scope.instructedMedDetail;

	nursingNoteEdits.newInfection = $scope.newInfection;
	nursingNoteEdits.universal = $scope.universal;
	nursingNoteEdits.sharpsWaste = $scope.sharpsWaste;

	nursingNoteEdits.infectionLogDate  = $scope.infectionLogDate;
	nursingNoteEdits.treatmentPrescribed = $scope.treatmentPrescribed;
	nursingNoteEdits.infectionType  = $scope.infectionType;
	nursingNoteEdits.infectionTreatment  = $scope.infectionTreatment;
	nursingNoteEdits.infectionNarrative  = $scope.infectionNarrative;

	nursingNoteEdits.WD = $scope.WD;
	nursingNoteEdits.treatment_location = $scope.treatment_location;
	nursingNoteEdits.treatment_stage = $scope.treatment_stage;
	nursingNoteEdits.treatment_notes = $scope.treatment_notes;

	nursingNoteEdits.bloodSugarAMCount = $scope.bloodSugarAMCount;
	nursingNoteEdits.bloodSugarAM = $scope.bloodSugarAM;
	nursingNoteEdits.bloodSugarNoonCount = $scope.bloodSugarNoonCount;
	nursingNoteEdits.bloodSugarNoon = $scope.bloodSugarNoon;
	nursingNoteEdits.bloodSugarPMCount = $scope.bloodSugarPMCount;
	nursingNoteEdits.bloodSugarPM = $scope.bloodSugarPM;
	nursingNoteEdits.bloodSugarHSCount = $scope.bloodSugarHSCount;
	nursingNoteEdits.bloodSugarHS = $scope.bloodSugarHS;
	nursingNoteEdits.bloodSugarPerformedBy = $scope.bloodSugarPerformedBy;
	nursingNoteEdits.bloodSugarSiteLocation = $scope.bloodSugarSiteLocation;
	nursingNoteEdits.bloodSugarSiteNote = $scope.bloodSugarSiteNote;

	nursingNoteEdits.diaManagement_diet = $scope.diaManagement_diet;
	nursingNoteEdits.diaManagement_exercise = $scope.diaManagement_exercise;
	nursingNoteEdits.diaManagement_oral = $scope.diaManagement_oral;
	nursingNoteEdits.diaManagement_insulin = $scope.diaManagement_insulin;
	nursingNoteEdits.diaManagement_na = $scope.diaManagement_na;
	nursingNoteEdits.diaManagement_patient = $scope.diaManagement_patient;
	nursingNoteEdits.diaManagement_caregiver = $scope.diaManagement_caregiver;
	nursingNoteEdits.diaManagement_SN = $scope.diaManagement_SN;

	nursingNoteEdits.SShyperglycemia_fatigue = $scope.SShyperglycemia_fatigue;
	nursingNoteEdits.SShyperglycemia_polydipsia = $scope.SShyperglycemia_polydipsia;
	nursingNoteEdits.SShyperglycemia_polyphagia = $scope.SShyperglycemia_polyphagia;
	nursingNoteEdits.SShyperglycemia_vision = $scope.SShyperglycemia_vision;
	nursingNoteEdits.SShyperglycemia_polyuria = $scope.SShyperglycemia_polyuria;
	nursingNoteEdits.SShyperglycemia_other = $scope.SShyperglycemia_other;
	nursingNoteEdits.SShyperglycemia_note = $scope.SShyperglycemia_note;

	nursingNoteEdits.SShypoglycemia_anxious = $scope.SShypoglycemia_anxious;
	nursingNoteEdits.SShypoglycemia_fatigue = $scope.SShypoglycemia_fatigue;
	nursingNoteEdits.SShypoglycemia_weakness  = $scope.SShypoglycemia_weakness;
	nursingNoteEdits.SShypoglycemia_dizziness = $scope.SShypoglycemia_dizziness;
	nursingNoteEdits.SShypoglycemia_perspiration  = $scope.SShypoglycemia_perspiration;
	nursingNoteEdits.SShypoglycemia_other  = $scope.SShypoglycemia_other;
	nursingNoteEdits.SShypoglycemia_note = $scope.SShypoglycemia_note;

	nursingNoteEdits.insulinType1 = $scope.insulinType1;
	nursingNoteEdits.insulinDose1 = $scope.insulinDose1;
	nursingNoteEdits.insulinSite1 = $scope.insulinSite1;
	nursingNoteEdits.insulinRoute1 = $scope.insulinRoute1;
	nursingNoteEdits.insulinType2 = $scope.insulinType2;
	nursingNoteEdits.insulinDose2 = $scope.insulinDose2;
	nursingNoteEdits.insulinSite2 = $scope.insulinSite2;
	nursingNoteEdits.insulinRoute2 = $scope.insulinRoute2;

	nursingNoteEdits.verbalize = $scope.verbalize;
	nursingNoteEdits.responseVerbal = $scope.responseVerbal;
	nursingNoteEdits.returnSlider = $scope.returnSlider;
	nursingNoteEdits.responseReturn = $scope.responseReturn;
	nursingNoteEdits.tolerate = $scope.tolerate;
	nursingNoteEdits.infusion = $scope.infusion;
	nursingNoteEdits.responseOther = $scope.responseOther;
	nursingNoteEdits.responseTeachingOther = $scope.responseTeachingOther;
	nursingNoteEdits.appetiteCheckSlider = $scope.appetiteCheckSlider;

	nursingNoteEdits.COC_reason = $scope.COC_reason;
	nursingNoteEdits.COC_MD = $scope.COC_MD;
	nursingNoteEdits.COC_SN = $scope.COC_SN;
	nursingNoteEdits.COC_LPN = $scope.COC_LPN;
	nursingNoteEdits.COC_PT = $scope.COC_PT;
	nursingNoteEdits.COC_OT = $scope.COC_OT;
	nursingNoteEdits.COC_ST = $scope.COC_ST;
	nursingNoteEdits.COC_HHA = $scope.COC_HHA;

	nursingNoteEdits.POC_note = $scope.POC_note;
	nursingNoteEdits.POC_order = $scope.POC_order;
	nursingNoteEdits.POC_review = $scope.POC_review;
	nursingNoteEdits.POC_understanding = $scope.POC_understanding;
	nursingNoteEdits.discharge_note = $scope.discharge_note;
	nursingNoteEdits.count_meal = $scope.count_meal;
	nursingNoteEdits.count_eaten = $scope.count_eaten;
	nursingNoteEdits.count_fluid = $scope.count_fluid;
	nursingNoteEdits.count_voids = $scope.count_voids;
	nursingNoteEdits.count_voidAMT = $scope.count_voidAMT;
	nursingNoteEdits.count_size = $scope.count_size;
	nursingNoteEdits.count_FR = $scope.count_FR;


    if ($scope.addPain)
    {
    	nursingNoteEdits.chestpain_duration = $scope.count_duration;
    	nursingNoteEdits.chestpain_prn = $scope.count_prn;
	}


    console.log(nursingNoteEdits);
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['skilledNursingNote'].edit,
      method: 'POST',
      // params: CONSTANTS.api.actions['oasisC2_2_20_1'].edit,
      data: $httpParamSerializerJQLike(nursingNoteEdits),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function(msg){
      console.log(msg)
      if ($scope.submitQA===true) { //if submitting to qa
        $scope.submitToQA();
      } else {
        if ($scope.form.ACTION==='QAEDIT') {
          $state.go('qa');
        } else {
          $state.go('records', {}, { reload: true })
        }
      }
    }, function(data) {
      console.log(data);
    });
    // if ($scope.form.ACTION==='QAEDIT') {
    //   $state.go('qa');
    // } else {
    //  // $state.go('scheduling');
    // }
  }


   $scope.getPatientMedicationList = function() {
     dataService.get('patientMedication', {PATID: $scope.patient.KEYID}).then(function(data) {
       $scope.activeMedicationList = data.data;
     });
   };

     $scope.getDoctorName = function() {
 		  var physcianid = {NPI: $scope.M0018_PHYSICIAN_ID};
  		  dataService.get('doctor', physcianid).then(function(data){
           $scope.doctorIn = data.data[0];
       });
   };

   $scope.getPatientMedicationList();

     $scope.newPatientMedication = function(event) {
       event.preventDefault();
       var modalInstance = $uibModal.open({
           controller: 'NewPatientMedicationController',
           ariaLabelledBy: 'modal-title',
           ariaDescribedBy: 'modal-body',
           templateUrl: './views/modals/newPatientMedication/newPatientMedication.html',
           size: 'lg',
           resolve: {
             getPatientMedicationList: function() {
               return $scope.getPatientMedicationList;
             }
           }
           // resolve: {
           //   // insurance: insurance
           // }
         });
   };


   // $scope.validateM0140=function () {
   //   if ()
   // }

      $scope.showPatientMedication = function(medication, event) {
         event.preventDefault();
         var modalInstance = $uibModal.open({
             controller: 'ViewPatientMedicationController',
             ariaLabelledBy: 'modal-title',
             ariaDescribedBy: 'modal-body',
             templateUrl: './views/modals/viewPatientMedication/viewPatientMedication.html',
             size: 'lg',
             resolve: {
               patientMedication: medication,
               getPatientMedicationList: function() {
                 return $scope.getPatientMedicationList;
               }
             }
         });
         // modalInstance.result.then(function () {
         //   $scope.getVisitTypeList();
         //   console.log($scope.activeVisitTypeList)
         // })
     }


  $scope.submitToQA=function () {
    //check to see if employee signature and date is complete
    if ($scope.signature && $scope.signature_date) { //if signature and date filled out, change form status to 1
      var form = {
        KEYID: $scope.form.FORMID,
        STATUS: 1,
      };
      dataService.edit('form', form).then(function (response) {
        console.log(response);
        $state.go('records', {}, { reload: true })
      });
    } else {
      alert('Please sign the form before submitting to QA.'); //if no signature/date, then alert
    }
  };

});
