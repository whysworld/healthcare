app.controller('WoundController', function ($scope, $http, $httpParamSerializerJQLike, $filter, formService, dataService, patientService, $anchorScroll, $timeout, CONSTANTS, $state) {
  $scope.PREVIOUS = function () {
    window.history.back();
  };
  $scope.form = {};
  // $scope.hhacareplan = {};
  $scope.editform = 'no';
  $scope.inputs = [];
  $scope.addfield = function () {
    $scope.inputs.push({})
  }
  var getFormValue = formService.get();
  if (getFormValue.STATUS == 0) {
    $scope.formId = getFormValue.FORMID;
  }
  $scope.saveWoundcareSheet = function (form) {

    var formLen = $scope.inputs.length; //23
    var wholeCount = formLen * 23;
    if (formLen != 0) {
      var arrForm = 0;

      angular.forEach($scope.inputs, function (value, key) {
        var df = $scope.modalobjCount(value);
        arrForm = arrForm + df;
      });

    }
    //return false;
    if (form.$valid && !$scope.warning && (wholeCount == arrForm)) {


      var newRouteSheet = {
        wound_first: $scope.inputs,
        treatment_performed: $scope.treatment_performed,
        narrative: $scope.narrative,
        edit_form: $scope.editform,
        currentTableId: $scope.currenttableId,
        FORMID: $scope.formId
      };
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['woundSheet'].add,
        method: 'POST',
        data: $httpParamSerializerJQLike(newRouteSheet),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }

      }).success(function (response) {

        if (response.status === 'success') {
          $anchorScroll();
          $scope.successTextAlert = response.msg;
          $scope.showSuccessAlert = true;
          $timeout(function () {
            $state.go(response.goto);
          }, 2000);
        }

      });
    } else {
      $scope.errorMsg = true;
      $anchorScroll();
      return false;
    }
  }
  $scope.modalobjCount = function (obj) {
    var arr = [];
    angular.forEach(obj, function (value, key) {
      arr.push(value);
    });
    return arr.length;
  }
  if (getFormValue.STATUS == 1) {

    $scope.form.STATUS = 1;

    $scope.treatment_performed = getFormValue.woundData.treatment_performed;
    $scope.narrative = getFormValue.woundData.narrative;
    $scope.woundcaredata = getFormValue.woundCareData;
    $scope.woundcaredata.forEach(function (detail) {
      var igDate = $filter("date")(detail.setdate, 'yyyy/MM/dd');
      $scope.form.SETDATE = new Date(igDate);
      $scope.inputs.push(detail);
    });



  }
  $scope.isQAMode = function () {
    if (getFormValue.ACTION === 'QAREVIEW') {
      $scope.formId = getFormValue.FORMID;
      alert(getFormValue.formPrimaryId + '--' + getFormValue.FORMID);
      $scope.currenttableId = getFormValue.formPrimaryId;
      $scope.approvedMsg = false; //form is not locked, allow editing
      $scope.enableQAEditBtn = true;
      $scope.enableQALockBtn = true;
      $scope.enableQARejectBtn = true;
      $scope.submitQAbtn = false;
      $scope.disableEdit = true;
      $scope.enableInput = false;
      $scope.showEmpSignatureImage = true;
      $scope.showEmpSignaturePad = false;
      $scope.showPatSignatureImage = true;
      $scope.showPatSignaturePad = false;

    } else {
      if ((getFormValue.STATUS === 1) || (getFormValue.STATUS === 3)) {
        $scope.disableEdit = true;
        $scope.enableInput = false;
        $scope.submitQAbtn = false;
        $scope.showEmpSignatureImage = true;
        $scope.showEmpSignaturePad = false;
        $scope.showPatSignatureImage = true;
        $scope.showPatSignaturePad = false;
      }
    }
  }
  //qa editing
  $scope.edit = function () {
    $scope.disableEdit = false;
    $scope.enableInput = true;
    $scope.enableQALockBtn = false;
    $scope.enableQAEditBtn = false;
    $scope.enableQARejectBtn = false;
    $scope.enableCancelBtn = true;
    $scope.enableSaveBtn = true;
    $scope.editform = "yes";

  };

  //cancel qa editing
  $scope.cancel = function () {
    $scope.disableEdit = true;
    $scope.enableInput = false;
    $scope.enableQALockBtn = true;
    $scope.enableQAEditBtn = true;
    $scope.enableQARejectBtn = true;
    $scope.enableCancelBtn = false;
    $scope.enableSaveBtn = false;
    $scope.getRouteSheet(); //get original route sheet info
    $scope.editform = "no";
  };
  $scope.reject = function () {

    var form = {
      KEYID: getFormValue.FORMID,
      STATUS: 4,

    };

    dataService.edit('form', form).then(function (response) {

      $state.go('qa');
    });
  };
  //qa approves form
  $scope.approve = function () {

    dataService.edit('form', {
      KEYID: getFormValue.FORMID,
      STATUS: 3
    }).then(function (response) {
      console.log('editing form', response);
      $state.go('qa');
    });
  }
  $scope.isQAMode();

});
