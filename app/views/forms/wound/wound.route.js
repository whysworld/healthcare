app.config(function ($stateProvider) {
  $stateProvider.state('wound', {
    url: '/wound',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/wound/wound.html',
        controller: 'WoundController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  })
})
