app.config(function($stateProvider) {
  $stateProvider.state('routeSheet', {
    url: '/routeSheet',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/routeSheet/routeSheet.html',
        controller: 'RouteSheetController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
