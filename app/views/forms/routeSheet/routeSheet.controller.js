app.controller('RouteSheetController', function($scope, $state, $http, CONSTANTS, $httpParamSerializerJQLike, $filter, dataService, formService, patientService) {

  $scope.visitTypes = [];
  $scope.employees = [];

  $scope.PREVIOUS = function() {
    window.history.back();
  };

  $scope.disableInput=function () {
    if ( ($scope.form.STATUS===1) || ($scope.form.STATUS===3) ) {
      $scope.disableEdit=true;
      $scope.enableInput=false;
      $scope.submitQAbtn=false;
    } else {
      $scope.disableEdit=false;
      $scope.enableInput=true;
      $scope.submitQAbtn=true
    }
  };

  //show btns to be seen during qa review
  $scope.isQAMode=function () {
    if ($scope.form.ACTION === 'QAREVIEW') {
      console.log('enabling qa mode');
      $scope.approvedMsg=false; //form is not locked, allow editing
      $scope.enableQAEditBtn=true;
      $scope.enableQALockBtn=true;
      $scope.enableQARejectBtn=true;
      $scope.submitQAbtn=false;
      $scope.disableEdit=true;
      $scope.enableInput=false;
      $scope.showEmpSignatureImage=true;
      $scope.showEmpSignaturePad=false;
      $scope.showPatSignatureImage=true;
      $scope.showPatSignaturePad=false;
    } else {
      if ( ($scope.form.STATUS===1) || ($scope.form.STATUS===3) ) {
        $scope.disableEdit=true;
        $scope.enableInput=false;
        $scope.submitQAbtn=false;
        $scope.showEmpSignatureImage=true;
        $scope.showEmpSignaturePad=false;
        $scope.showPatSignatureImage=true;
        $scope.showPatSignaturePad=false;
      } else {
        $scope.disableEdit=false;
        $scope.enableInput=true;
        // $scope.submitQAbtn=true
        $scope.approvedMsg=false; //form is not locked, allow editing
        $scope.enableQAEditBtn=false;
        $scope.enableQALockBtn=false;
        $scope.enableQARejectBtn=false;
        $scope.enableCancelBtn=false;
        $scope.enableSaveBtn=false;
        $scope.submitQAbtn=true;
        if ($scope.form.STATUS===4) {
          $scope.showEmpSignatureImage=true;
          $scope.showEmpSignaturePad=false;
          $scope.showPatSignatureImage=true;
          $scope.showPatSignaturePad=false;
        } else if ($scope.form.STATUS===0) {
          $scope.showEmpSignatureImage=false;
          $scope.showEmpSignaturePad=true;
          $scope.showPatSignatureImage=false;
          $scope.showPatSignaturePad=true;
        }
      }

    }
  };

  //qa editing
  $scope.edit = function(){
    $scope.disableEdit = false;
    $scope.enableInput = true;
    $scope.enableQALockBtn=false;
    $scope.enableQAEditBtn=false;
    $scope.enableQARejectBtn=false;
    $scope.enableCancelBtn=true;
    $scope.enableSaveBtn=true;
  };

  //cancel qa editing
  $scope.cancel=function () {
    $scope.disableEdit = true;
    $scope.enableInput = false;
    $scope.enableQALockBtn = true;
    $scope.enableQAEditBtn=true;
    $scope.enableQARejectBtn=true;
    $scope.enableCancelBtn=false;
    $scope.enableSaveBtn=false;
    $scope.getRouteSheet(); //get original route sheet info
  };

  //qa approves form
  $scope.approve=function () {
    //reset current routesheet
    dataService.get('routeSheet', {KEYID:$scope.form.OBJKEYID}).then(function (data) {
      var rs=data.data[0];
      console.log('route sheet', rs);
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['routeSheet'].edit,
        method: 'POST',
        data: $httpParamSerializerJQLike({KEYID:rs.KEYID, LOCKED:1}),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).then(function(response){
        console.log('editing routesheet', response);
        if(response.data.status==='success') {
          $scope.approvedMsg=true;
          $scope.enableQAEditBtn=false;
          $scope.enableQALockBtn=false;

          //edit form status to 3=approved;
          dataService.edit('form', {KEYID:rs.FORMID, STATUS:3}).then(function (response) {
            console.log('editing form', response);
            $state.go('qa');
          });
        }
      }, function(data) {
          console.log(data);
        }
      );
    });
  };

  //qa rejects form
  $scope.reject=function () {
    //change form status to in progress, and attach note?
    var form={
      KEYID: $scope.form.FORMID,
      STATUS: 4,
      // DETAIL1: 'Form has been rejected by QA'
    };
    // console.log('form);
    dataService.edit('form', form).then(function (response) {
      console.log('editing form', response);
      $state.go('qa');
    });
  };

  //get visit types on page load
  $scope.getVisitTypes = function() {
    dataService.getAll('visitType').then(function (data) {
      $scope.visitTypes = data.data;
      $scope.visitTypes.forEach(function (visitType) {
        if(visitType.KEYID===$scope.form.VISITTYPE) {
          $scope.visitType=visitType;
          // console.log($scope.visitType)
        }
      });
    });
  };

  //get employees list on page load
  $scope.getEmployees = function() {
    dataService.getAll('employee').then(function (data) {
        $scope.employees = data.data;
    });
  };

  //get original route sheet info on cancel
  $scope.getRouteSheet=function () {
    $scope.form=formService.get();
    $scope.submitpayroll = $scope.form.PAYROLL_SUBMIT;
    $scope.billable = $scope.form.BILLABLE;
    $scope.VISITTYPE = $scope.form.VISITTYPE;
    $scope.EMPKEYID = $scope.form.EMPLOYEE;
    $scope.MILEAGE = $scope.form.MILEAGE;
    $scope.SURCHARGE = $scope.form.SURCHARGE;
    $scope.ADDITIONAL = $scope.form.ADDITIONAL;
    $scope.HOURS=$scope.form.HOURS;
    $scope.sigrequired_pat = $scope.form.SIGREQUIRED_PAT;
    $scope.patSignature = $scope.form.SIGNATURE_PAT;
    $scope.sigrequired_emp = $scope.form.SIGREQUIRED_EMP;
    $scope.empSignature = $scope.form.SIGNATURE_EMP;

    $scope.formatFormFields();

    // $scope.patDate = new Date($filter("date")($scope.form.SIGDATE_PAT, 'yyyy/MM/dd'));
    // $scope.empDate = new Date($filter("date")($scope.form.SIGDATE_EMP, 'yyyy/MM/dd'));
  };

  //save form edits
  $scope.editRouteSheet=function () {
    if ($scope.TIMEIN !==undefined) {
      //Format TIMEIN and TIMEOUT
      var hourIN = $scope.TIMEIN.getHours();
      var hourOUT = $scope.TIMEOUT.getHours();
      var minOUT = $scope.TIMEOUT.getMinutes();
      var minIN = $scope.TIMEIN.getMinutes();

      if (minIN < 10) {
        minIN = minIN.toString();
        minIN = '0' + minIN;
      } else {
        minIN = minIN.toString();
      }
      if (minOUT<10){
        minOUT = minOUT.toString();
        minOUT='0'+ minOUT;
      } else {
        minOUT = minOUT.toString();
      }
      if (hourOUT<10){
        hourOUT = hourOUT.toString();
        hourOUT='0'+ hourOUT;
      } else {
        hourOUT = hourOUT.toString();
      }
      if (hourIN<10){
        hourIN = hourIN.toString();
        hourIN='0'+ hourIN;
      } else {
        hourIN = hourIN.toString();
      }

      $scope.TIMEIN = hourIN + minIN + '00';
      $scope.TIMEOUT = hourOUT + minOUT + '00';
    } else {
      $scope.TIMEIN = null;
      $scope.TIMEOUT = null;
    }

    var editRouteSheet = {
        KEYID: $scope.form.OBJKEYID,
        FORMID: $scope.form.FORMID,
        PAYROLL_SUBMIT: $scope.submitpayroll,
        BILLABLE: $scope.billable,
        VISITDATE: $filter("date")($scope.VISITDATE, 'yyyy/MM/dd'),
        TIMEIN: $scope.TIMEIN,
        TIMEOUT: $scope.TIMEOUT,
        HOURS: $scope.HOURS,
        VISITTYPE: $scope.VISITTYPE,
        EMPLOYEE: $scope.EMPKEYID,
        MILEAGE: $scope.MILEAGE,
        SURCHARGE: $scope.SURCHARGE,
        ADDITIONAL: $scope.ADDITIONAL,
        SIGREQUIRED_PAT: $scope.sigrequired_pat,
        SIGNATURE_PAT: $scope.patSignature,
        SIGDATE_PAT: $filter("date")($scope.patDate, 'yyyy/MM/dd'),
        SIGREQUIRED_EMP: $scope.sigrequired_emp,
        SIGNATURE_EMP: $scope.empSignature,
        SIGDATE_EMP: $filter("date")($scope.empDate, 'yyyy/MM/dd')
    };
    console.log("route sheet changes", editRouteSheet);
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['routeSheet'].edit,
      method: 'POST',
      data: $httpParamSerializerJQLike(editRouteSheet),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function(response){
      console.log('editing route sheet', response);
      if(response.data.status==='success') {

        //edit visit to update the timein and timeout
        var visitTimesUpdate={
          KEYID:$scope.form.VISITID,
          TIMEIN: $scope.TIMEIN,
          TIMEOUT: $scope.TIMEOUT,
          HOURS: $scope.HOURS,
        };
        dataService.edit('visit', visitTimesUpdate).then(function (response) {
          console.log('editiing visit', response);
        });

        //get route sheet and set as form
        dataService.get('routeSheet', {KEYID:$scope.form.OBJKEYID}).then(function (data) {
          var updatedForm=data.data[0];
          updatedForm.OBJKEYID=data.data[0].KEYID;
          updatedForm.FORMDATE=$filter("date")(data.data[0].VISITDATE, 'yyyy/MM/dd');

          formService.set(updatedForm);
          $scope.form=formService.get();

          console.log($scope.form)
          $scope.getRouteSheet();
          $scope.disableEdit = true;
          $scope.enableInput = false;
          $scope.enableQALockBtn = true;
          $scope.enableQAEditBtn=true;
          $scope.enableQARejectBtn=true;
          $scope.enableCancelBtn=false;
          $scope.enableSaveBtn=false;
        });

        //edit visit date, visit type, and employee?

      }
    });
  };
    // $scope.getRouteSheet = function() {
    //     dataService.get('routeSheet', {FORMID: $scope.form.FORMID}).then(function(data) {
    //         if (data.data.length > 0) {
    //             var rs = data.data[0];
    //             $scope.submitpayroll = rs.PAYROLL_SUBMIT;
    //             $scope.billable = rs.BILLABLE;
    //             $scope.selected = new Date($filter("date")(rs.VISITDATE, 'yyyy/MM/dd'));
    //             $scope.VISITTYPE = rs.VISITTYPE;
    //             $scope.TIMEIN = rs.TIMEIN;
    //             $scope.TIMEOUT = rs.TIMEOUT;
    //             $scope.EMPKEYID = rs.EMPLOYEE;
    //             $scope.MILEAGE = rs.MILEAGE;
    //             $scope.SURCHARGE = rs.SURCHARGE;
    //             $scope.ADDITIONAL = rs.ADDITIONAL;
    //             $scope.sigrequired_pat = rs.SIGREQUIRED_PAT;
    //             $scope.patSignature = rs.SIGNATURE_PAT;
    //             $scope.patDate = new Date($filter("date")(rs.SIGDATE_PAT, 'yyyy/MM/dd'));
    //             $scope.sigrequired_emp = rs.SIGREQUIRED_EMP;
    //             $scope.empSignature = rs.SIGNATURE_EMP;
    //             $scope.empDate = new Date($filter("date")(rs.SIGDATE_EMP, 'yyyy/MM/dd'));
    //             // dataService.get('visitType', {KEYID: rs.VISITTYPE}).then(function(visittypedata) {
    //             //     console.log(visittypedata);
    //             //     $scope.VISITTYPE = visittypedata.data[0];
    //             // });
    //         }
    //     });
    // };
    //

  //format fields to display on frontend
  $scope.formatFormFields=function () {
    var visitdate=$filter("date")($scope.form.FORMDATE, 'yyyy/MM/dd');
    $scope.form.FORMDATE=new Date(visitdate);
    //format timein and timeout if not new form
    if ( ($scope.form.TIMEIN) || ($scope.form.TIMEOUT) ) {
      console.log('formatting dates and time for front end');
    // if ( ($scope.form.TIMEIN !== null) || ($scope.form.TIMEIN !== undefined)) {
      //format TIMEIN and TIMEOUT to display
      var hourIN = $scope.form.TIMEIN.slice(0,2);
      var minIN = $scope.form.TIMEIN.slice(3,5);
      var hourOUT = $scope.form.TIMEOUT.slice(0,2);
      var minOUT = $scope.form.TIMEOUT.slice(3,5);
      // $scope.HOURS = visit.HOURS;
      $scope.TIMEIN = new Date().setHours(hourIN,minIN);
      $scope.TIMEIN = new Date($scope.TIMEIN);
      $scope.TIMEOUT = new Date().setHours(hourOUT, minOUT);
      $scope.TIMEOUT = new Date($scope.TIMEOUT);

      //format form date
      var signEmpDate=$filter("date")($scope.form.SIGDATE_EMP, 'yyyy/MM/dd');
      $scope.form.SIGDATE_EMP=new Date(signEmpDate);
      var signPatDate=$filter("date")($scope.form.SIGDATE_PAT, 'yyyy/MM/dd');
      $scope.form.SIGDATE_PAT=new Date(signPatDate);

    }
    //assign payroll_submit checkboxes
    if ($scope.form.PAYROLL_SUBMIT!==undefined) {
      $scope.submitpayroll = $scope.form.PAYROLL_SUBMIT;
    } else {
      $scope.submitpayroll=1;
    }

    //assign billable checkboxes
    if ($scope.form.BILLABLE!==undefined) {
      $scope.billable = $scope.form.BILLABLE;
    } else {
      $scope.billable=1;
    }

  };

  $scope.getTime=function() {
    $scope.warning = false;
    if ($scope.defaultHrs===1) { //if default hrs are used
      // console.log(parseInt($scope.visitType.DFLTHRS))
      // console.log($scope.TIMEIN.getHours())
      var dflthrs=parseInt($scope.visitType.DFLTHRS);
      var timein = new Date($scope.TIMEIN);
      $scope.TIMEOUT = timein.setHours(timein.getHours()+dflthrs);
      $scope.TIMEOUT = new Date($scope.TIMEOUT);
      $scope.HOURS=dflthrs;
      // console.log($scope.TIMEOUT);
      // console.log($scope.TIMEIN);
    } else {
      $scope.changed();
    }
  };

  //calculate hours on time change
  $scope.changed=function () {
    $scope.warning = false;
      //if TIMEOUT is after TIMEIN
      console.log($scope.TIMEOUT);
      console.log($scope.TIMEIN);
      // console.log(moment($scope.TIMEIN).isBefore(moment($scope.TIMEOUT)))

      if (moment($scope.TIMEIN).isBefore(moment($scope.TIMEOUT))) {
        var hourDiff = $scope.TIMEOUT.getHours() - $scope.TIMEIN.getHours();
        var minDiff = $scope.TIMEOUT.getMinutes() - $scope.TIMEIN.getMinutes();
        //Round hour by half hour increments
        if ( (hourDiff===0) && (minDiff>0) ) {
          $scope.HOURS = 1;
        } else if ( (hourDiff>0) && (minDiff>29) ) {
          $scope.HOURS = hourDiff + 1;
        } else if ( (hourDiff>0) && (minDiff<30) ) {
          $scope.HOURS = hourDiff;
        }
        $scope.warning = false;
      } else {
        $scope.warning = true;
      }
  };


  //saving new route sheet
  $scope.saveRouteSheet = function(form) {
      console.log("Route Sheet form");
      console.log(form.$valid);

      if (form.$valid && !$scope.warning) {
        if (!confirm("No changes are allowed once submitted to QA, are you sure you want to submit?")) { //no don't submit to qa
          console.log('no, do not submit to qa');
          event.preventDefault();
        } else { //yes submit to qa
          if ($scope.TIMEIN !==undefined) {
            //Format TIMEIN and TIMEOUT
            var hourIN = $scope.TIMEIN.getHours();
            var hourOUT = $scope.TIMEOUT.getHours();
            var minOUT = $scope.TIMEOUT.getMinutes();
            var minIN = $scope.TIMEIN.getMinutes();

            if (minIN < 10) {
              minIN = minIN.toString();
              minIN = '0' + minIN;
            } else {
              minIN = minIN.toString();
            }
            if (minOUT<10){
              minOUT = minOUT.toString();
              minOUT='0'+ minOUT;
            } else {
              minOUT = minOUT.toString();
            }
            if (hourOUT<10){
              hourOUT = hourOUT.toString();
              hourOUT='0'+ hourOUT;
            } else {
              hourOUT = hourOUT.toString();
            }
            if (hourIN<10){
              hourIN = hourIN.toString();
              hourIN='0'+ hourIN;
            } else {
              hourIN = hourIN.toString();
            }

            $scope.TIMEIN = hourIN + minIN + '00';
            $scope.TIMEOUT = hourOUT + minOUT + '00';
          } else {
            $scope.TIMEIN = null;
            $scope.TIMEOUT = null;
          }

          var newRouteSheet = {
              FORMID: $scope.form.FORMID,
              PAYROLL_SUBMIT: $scope.submitpayroll,
              BILLABLE: $scope.billable,
              VISITDATE: $filter("date")($scope.VISITDATE, 'yyyy/MM/dd'),
              TIMEIN: $scope.TIMEIN,
              TIMEOUT: $scope.TIMEOUT,
              HOURS: $scope.HOURS,
              VISITTYPE: $scope.VISITTYPE,
              EMPLOYEE: $scope.EMPKEYID,
              MILEAGE: $scope.MILEAGE,
              SURCHARGE: $scope.SURCHARGE,
              ADDITIONAL: $scope.ADDITIONAL,
              SIGREQUIRED_PAT: $scope.sigrequired_pat,
              SIGNATURE_PAT: $scope.acceptPat().dataUrl,
              SIGDATE_PAT: $filter("date")($scope.patDate, 'yyyy/MM/dd'),
              SIGREQUIRED_EMP: $scope.sigrequired_emp,
              SIGNATURE_EMP: $scope.acceptEmp().dataUrl,
              SIGDATE_EMP: $filter("date")($scope.empDate, 'yyyy/MM/dd')
          };

          console.log("route sheet", newRouteSheet);
          $http({
            url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['routeSheet'].add,
            method: 'POST',
            data: $httpParamSerializerJQLike(newRouteSheet),
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response){
                console.log('saving routesheet', response);
                if (response.data.status==='success') {
                  //edit form to contain objectkeyid and status
                  var form = {
                    KEYID: $scope.form.FORMID,
                    OBJKEYID: response.data.keyid,
                    STATUS: 1, //submit to qa
                  };
                  // edit the form to contain the object keyid and status
                  dataService.edit('form', form).then(function (response) {
                    console.log('editing form', response);

                    //edit visit to update the timein and timeout
                    var visitTimesUpdate={
                      KEYID:$scope.form.VISITID,
                      TIMEIN: $scope.TIMEIN,
                      TIMEOUT: $scope.TIMEOUT,
                      HOURS: $scope.HOURS,
                    }
                    dataService.edit('visit', visitTimesUpdate).then(function (response) {
                      console.log('editiing visit', response);
                      $state.go('records');
                    });
                  });
                }
            }, function(data) {
              console.log(data);
          });
        }
      } else {
          console.log("invalid form");
      }
  };

  $scope.removeEmpSignature=function () {
    // $scope.removeOldEmpSignature=true;
    $scope.showEmpSignaturePad=true;
    $scope.showEmpSignatureImage=false;
  };
  $scope.removePatSignature=function () {
    // $scope.removeOldPatSignature=true;
    $scope.showPatSignaturePad=true;
    $scope.showPatSignatureImage=false;
  };

  $scope.form = formService.get();
  console.log('got form', formService.get());
  $scope.getEmployees();
  $scope.getVisitTypes();
  $scope.formatFormFields();

  // console.log("got form: ");
  // console.log($scope.form);

  // $scope.getRouteSheet();

  // $scope.getRouteSheet();
  $scope.patient = patientService.get();
  // $scope.disableInput();
  $scope.isQAMode();

  // $scope.getRouteSheet();




});
