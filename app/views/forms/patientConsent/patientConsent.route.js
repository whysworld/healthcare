app.config(function($stateProvider) {
  $stateProvider.state('patientConsent', {
    url: '/patientConsent',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/patientConsent/patientConsent.html',
        controller: 'PatientConsentController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
