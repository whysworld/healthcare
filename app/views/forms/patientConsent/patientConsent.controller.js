app.controller('PatientConsentController', function($scope, $state, $filter, patientService, agencyService, formService, dataService, $http, CONSTANTS, $httpParamSerializerJQLike) {

  $scope.patient=patientService.get();
  console.log('$scope.patient', $scope.patient);

  $scope.agency=agencyService.get();
  console.log('$scope.agency', $scope.agency);

  $scope.form=formService.get();
  console.log('$scope.form', $scope.form);

  $scope.hotline = {
  	name : 'Nevada State Health Division, Bureau of Licensure and Certification',
  	phone : '1 (800) 225-3414',
  	address : '4220 S. Maryland Parkway, Ste. 810, Las Vegas, NV 89119'
  };

  //check form status
  $scope.checkFormStatus=function () {
    if ($scope.form.STATUS===0) { //form is incomplete, so new form
      console.log('starting new form');
      $scope.showSignaturePad=true;
    } else { //form is existing, get form obj and populate form
      dataService.get('patientConsent', {FORMID: $scope.form.FORMID}).then(function (data) {
        var formObj=data.data[0];
        $scope.formObj=data.data[0];
        console.log('getting existing form', formObj);
        //populate form w/ exisitng form data
        $scope.NOINFORMATIONRELEASEABOUT=formObj.patientConsent.noInformationReleaseAbout;
        $scope.NOINFORMATIONRELEASETO=formObj.patientConsent.noInformationReleaseTo;
        $scope.providedServicesNursing=formObj.patientConsent.providedServicesNursing;
        $scope.providedServicesCHHA=formObj.patientConsent.providedServicesCHHA;
        $scope.providedServicesPT=formObj.patientConsent.providedServicesPT;
        $scope.providedServicesOT=formObj.patientConsent.providedServicesOT;
        $scope.providedServicesST=formObj.patientConsent.providedServicesST;
        $scope.providedServicesDietician=formObj.patientConsent.providedServicesDietician;
        $scope.providedServicesMSW=formObj.patientConsent.providedServicesMSW;

        $scope.OTHERADVANCEDIRECTIVES=formObj.patientConsent.otherAdvanceDirectives;
        $scope.DONOTKNOWADVANCEDIRECTIVES=formObj.patientConsent.doNotKnowAdvanceDirectives;
        // $scope.SIGNATURE=formObj.patientConsent.signature;
        $scope.ADDITIONALSIGNATURE=formObj.patientConsent.additionalSignature;

        //if radio button values are null, then assign 0 to display 'no' on front end
        // if (!formObj.patientConsent.completedAdvanceDirectives){
        //   $scope.COMPLETEDADVANCEDIRECTIVES=0;
        // } else {
          $scope.COMPLETEDADVANCEDIRECTIVES=formObj.patientConsent.completedAdvanceDirectives;
        // }
        // if (!formObj.patientConsent.durablePowerOfAttorney) {
        //   $scope.DURABLEPOWEROFATTORNEY=0;
        // } else {
          $scope.DURABLEPOWEROFATTORNEY=formObj.patientConsent.durablePowerOfAttorney;
        // }
        // if (!formObj.patientConsent.livingWill) {
        //   $scope.LIVINGWILL=0;
        // } else {
          $scope.LIVINGWILL=formObj.patientConsent.livingWill;
        // }
        // if (!formObj.patientConsent.doNotResuscitate) {
        //   $scope.DONOTRESUSCITATE=0;
        // } else {
          $scope.DONOTRESUSCITATE=formObj.patientConsent.doNotResuscitate;
        // }
        // if (!formObj.patientConsent.photograph) {
        //   $scope.PHOTOGRAPH=0;
        // } else {
          $scope.PHOTOGRAPH=formObj.patientConsent.photograph;
        // }

        //only populate date if exists
        if (formObj.patientConsent.signatureDate) {
          $scope.SIGNATUREDATE=new Date($filter('date')(formObj.patientConsent.signatureDate, 'yyyy/MM/dd'));
        }
        if (formObj.patientConsent.additionalSignatureDate) {
          $scope.ADDITIONAL_SIGNATUREDATE=new Date($filter('date')(formObj.patientConsent.additionalSignatureDate, 'yyyy/MM/dd'));
        }
        if (formObj.patientConsent.signature) {
          $scope.SIGNATURE=formObj.patientConsent.signature;
          $scope.showSignaturePad=false;
          $scope.showSignatureImage=true;
        } else {
          $scope.showSignatureImage=false;
          $scope.showSignaturePad=true;
        }
      });
    }
  };

  $scope.getPatientConsentObj=function () {
    var patientConsentObj={
      patientConsent: {
        formId : $scope.form.FORMID,
        lang : 'EN',
        noInformationReleaseAbout: $scope.NOINFORMATIONRELEASEABOUT,
        noInformationReleaseTo: $scope.NOINFORMATIONRELEASETO,
        completedAdvanceDirectives : $scope.COMPLETEDADVANCEDIRECTIVES,
        durablePowerOfAttorney : $scope.DURABLEPOWEROFATTORNEY,
        livingWill : $scope.LIVINGWILL,
        doNotResuscitate : $scope.DONOTRESUSCITATE,
        otherAdvanceDirectives : $scope.OTHERADVANCEDIRECTIVES,
        doNotKnowAdvanceDirectives : $scope.DONOTKNOWADVANCEDIRECTIVES,
        photograph : $scope.PHOTOGRAPH,
        // signature : $scope.accept().dataUrl,
        signatureDate : $filter('date')($scope.SIGNATUREDATE, 'yyyy/MM/dd'),
        // additionalSignature : $scope.ADDITIONALSIGNATURE,
        // additionalSignatureDate : $filter('date')($scope.ADDITIONAL_SIGNATUREDATE, 'yyyy/MM/dd'),
        providedServicesNursing: $scope.providedServicesNursing,
      	providedServicesCHHA: $scope.providedServicesCHHA,
      	providedServicesPT: $scope.providedServicesPT,
      	providedServicesOT: $scope.providedServicesOT,
      	providedServicesST: $scope.providedServicesST,
      	providedServicesDietician: $scope.providedServicesDietician,
      	providedServicesMSW: $scope.providedServicesMSW,
      },
      patient: {
        FNAME: $scope.patient.FNAME,
        LNAME: $scope.patient.LNAME,
        KEYID: $scope.patient.KEYID,
      },
      hotline: {
        name: $scope.hotline.name,
        phone: $scope.hotline.phone,
        address: $scope.hotline.address,
      },
      agency: {
        NAME: $scope.agency.NAME,
        SHORTNAME: $scope.agency.SHORTNAME,
        anticipatedChargesSN: $scope.agency.AGENCYCHARGESSN,
        anticipatedChargesMSW: $scope.agency.AGENCYCHARGESMSW,
        anticipatedChargesCHHA: $scope.agency.AGENCYCHARGESCHHA,
        openDaysAndHours: $scope.agency.OPENDAYSANDHOURS,
        administrator: $scope.agency.ADMINISTRATOR,
        address1: $scope.agency.ADDRESS1,
        city: $scope.agency.CITY,
        state: $scope.agency.STATE,
        phone: $scope.agency.PHONE,
        fax: $scope.agency.FAX,
      }
    };
    return patientConsentObj;
  };

  //save patient consent form
  $scope.savePatientConsent=function () {
    var newPatientConsent=$scope.getPatientConsentObj();
    if ($scope.form.STATUS===0) { //if status is 0, form is incomplete, save form
      newPatientConsent.patientConsent.signature=$scope.accept().dataUrl;
      console.log('saving new patient consent obj', newPatientConsent);
      dataService.add('patientConsent', newPatientConsent).then(function (response) {
        console.log('add new patient consent record', response);
        if (response.status==='success') {
          //edit form status to 1 - 'in progress', add OBJKEYID
          dataService.edit('form', {KEYID: $scope.form.FORMID, STATUS: 2, OBJKEYID: response.keyid}).then(function (response) {
            console.log('edit form status', response);
            if (response.status==='success') {
              $state.go('records');
            }
          });
        }
      });
    } else { //edit form
      var editPatientConsent=newPatientConsent;
      editPatientConsent.patientConsent.id=$scope.formObj.patientConsent.id;
      //if signature has already been saved
      if ($scope.SIGNATURE) {
        if ($scope.removeOldSignature===true) { //if signature has been removed
          if ($scope.accept().dataUrl) { //save new siganture
            editPatientConsent.patientConsent.signature=$scope.accept().dataUrl;
          } else { //if there is no new signature, save null;
            editPatientConsent.patientConsent.signature="";
          }
        } else { //if signature hasn't been removed, use saved signature
          editPatientConsent.patientConsent.signature = $scope.SIGNATURE;
        }
      } else { //if signature hasn't been saved
        editPatientConsent.patientConsent.signature=$scope.accept().dataUrl;
      }
      console.log('edit patient consent obj', editPatientConsent);
      dataService.edit('patientConsent', editPatientConsent).then(function (response) {
        console.log('editing patient consent record', response);
        if (response.status==='success') {
          if ($scope.form.ACTION==='QAEDIT') {
            $state.go('qa');
          } else {
            $state.go('records');
          }
        }
      });
    }
  };


  //submit patient consent form
  $scope.submitPatientConsent=function(form) {
    $scope.submitForm=true;
    console.log(form)
    //check to see if inputs are complete
    if (form.$invalid) {
      // if (form.COMPLETEDADVANCEDIRECTIVES.$invalid || form.services.$invalid || form.DONOTRESUSCITATE.$invalid || form.DURABLEPOWEROFATTORNEY.$invalid || form.LIVINGWILL.$invalid || form.PHOTOGRAPH.$invalid) {
      //   console.log('missing input');
      //   $scope.showMoreConsent=true;
      //   $scope.areFieldsComplete=false;
      // } else {
      //   console.log('safd')
      // }
        $scope.showMoreConsent=true;
        $scope.areFieldsComplete=false;
        console.log('fields are incomplete')
    } else {
      console.log('fields are complete')
      $scope.areFieldsComplete=true;
    }

    //check to see if signature is complete
    //if signature has already been saved
    if ($scope.SIGNATURE) {
      if ($scope.removeOldSignature===true) { //if signature has been removed
        console.log('signature removed');
        if ($scope.accept().isEmpty===true) {
          $scope.isSignatureMissing=true;
          console.log('missing signature');
        } else {
          if ($scope.accept().dataUrl) {
            $scope.isSignatureMissing=false;
            console.log('not missing signature');
          } else {
            $scope.isSignatureMissing=true;
            console.log('missing signature');
          }
        }
      } else { //if signature hasn't been removed, use saved signature
        $scope.isSignatureMissing=false;
        console.log('signature not removed, use exisitng signature');
      }
    } else { //if signature has not been saved
      if ($scope.accept().dataUrl) { //if signed
        $scope.isSignatureMissing=false;
        console.log('not missing signature');
      } else { //if not signed
        $scope.isSignatureMissing=true;
        console.log('missing signature');
      }
    }

    //if inputs are complete and signature is complete
    if ($scope.areFieldsComplete===true && $scope.isSignatureMissing===false ) {
      console.log('submit form');
      var submitPatientConsentObj=$scope.getPatientConsentObj();
      //if signature has already been saved
      if ($scope.SIGNATURE) {
        if ($scope.removeOldSignature===true) { //if signature has been removed, save new signature
          submitPatientConsentObj.patientConsent.signature=$scope.accept().dataUrl;
        } else { //if signature hasn't been removed, use saved signature
          submitPatientConsentObj.patientConsent.signature = $scope.SIGNATURE;
        }
      } else { //if signature hasn't been saved
        submitPatientConsentObj.patientConsent.signature=$scope.accept().dataUrl;
      }
      console.log('submit patientConsent', submitPatientConsentObj);
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['patientConsent'].submit,
        method: 'POST',
        data: $httpParamSerializerJQLike(submitPatientConsentObj),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).then(function(response){
          console.log('submitting patient consent', response);
          if (response.data.status==='success') {
            //edit form status to 1 - 'in progress', add OBJKEYID
            dataService.edit('form', {KEYID: $scope.form.FORMID, OBJKEYID: response.data.keyid}).then(function (response) {
              console.log('edit form status', response);
              if (response.status==='success') {
                $state.go('records');
              }
            });
          }
        }, function(data) {
          console.log(data);
      });
    }
  };

  //if 'do not know advanced directive' checkbox is checked, clear all directive radio btns
  $scope.directive=function () {
    if ($scope.DONOTKNOWADVANCEDIRECTIVES===1) {
      $scope.LIVINGWILL=null;
      $scope.DONOTRESUSCITATE=null;
      $scope.DURABLEPOWEROFATTORNEY=null;
      $scope.COMPLETEDADVANCEDIRECTIVES=null;
    }
  };

  $scope.removeSignature=function () {
    $scope.removeOldSignature=true;
    $scope.showSignaturePad=true;
    $scope.showSignatureImage=false;
  };

  $scope.checkFormStatus();
});
