app.config(function($stateProvider) {
  $stateProvider.state('ABN-ES', {
    url: '/ABN-ES',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/ABN-ES/ABN-ES.html',
        controller: 'ABNESController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
