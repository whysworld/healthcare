app.controller('ABNESController', function($scope, $stateParams, $uibModal, patientService) {

  $scope.patient=patientService.get();

  $scope.save = function(form) {
    console.log(form.$submitted);
    console.log(form.$valid);

    if(form.$valid) {
      var abnForm = {
        D: $scope.D,
        D_description: $scope.D_description,
        E_reason: $scope.E_reason,
        F_cost: $scope.F_cost,
        G_option: $scope.G_option,
        H_info: $scope.H_info,
        I_signature: $scope.I_signature,
        J_date: $scope.J_date,
      };
      console.log("form submitted", abnForm);
    }
    else {
      console.log('Not Valid');
    }
  };
});
