app.config(function($stateProvider) {
  $stateProvider.state('ABN', {
    url: '/ABN',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/ABN/ABN.html',
        controller: 'ABNController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
