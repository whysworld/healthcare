app.controller('ViewPlanOfCareController', function($scope, $filter,  $state, dataService, $http, CONSTANTS, $httpParamSerializerJQLike, Blob, FileSaver, formService, patientService, agencyService) {

  //back button goes back to previous route
  $scope.PREVIOUS = function() {
    window.history.back();
  };

  //get patient info
  $scope.patient=patientService.get();
  $scope.patient.DOB=$filter('date')($scope.patient.DOB, 'MM-dd-yyyy');
  console.log('$scope.patient', $scope.patient);

  //get form info
  $scope.form = formService.get();
  console.log('$scope.form', $scope.form);

  //get agency info
  $scope.agency=agencyService.get();
  console.log('$scope.agency', $scope.agency);

	//get admit info to populate form
	dataService.get('admission', {KEYID:$scope.patient.LSTADMIT}).then(function (data) {
		$scope.admit=data.data[0];
		console.log('$scope.admit', $scope.admit);
		$scope.admit.SOCDATE=$filter('date')($scope.admit.SOCDATE, 'MM-dd-yyyy');

		// get physician's information, you can find same information on patientProfile page
		dataService.get('doctor', {'KEYID': $scope.admit.DOCTOR}).then(function(data) {
			$scope.DOCTOR = data.data[0];
		});
	});

  //get patient medication
  dataService.get('patientMedication', {PATID: $scope.patient.KEYID}).then(function (data) {
    $scope.medications=data.data;
  });

  //get oasis info
  dataService.get('form', {VISITID: $scope.form.VISITID}).then(function (data) {
    var allVisitForms=data.data;
    console.log(allVisitForms)
    var i=allVisitForms.findIndex(c => (c.FORMTYPE === 2) || (c.FORMTYPE === 3) || (c.FORMTYPE === 4))

    if (allVisitForms[i] !== undefined && allVisitForms[i].OBJKEYID!==null) {
      dataService.get('oasisC2_2_20_1', {'KEYID': allVisitForms[i].OBJKEYID}).then(function(data){
        console.log('$scope.oasis', data.data[0]);
        $scope.oasis=data.data[0];
        $scope.oasis.M0030_START_CARE_DT=$filter('date')(data.data[0].M0030_START_CARE_DT, 'MM-dd-yyyy')
        getCodeDesc($scope.oasis)

        //get certification date
        if (allVisitForms[i].FORMTYPE===2) { //if it's a soc visit
          console.log('start of care visit');
          dataService.get('episode', {KEYID:allVisitForms[i].EPIKEYID}).then(function (data) {
            var FROMDATE = $filter('date')(data.data[0].FROMDATE, 'MM-dd-yyyy');
            var THRUDATE = $filter('date')(data.data[0].THRUDATE, 'MM-dd-yyyy');
            $scope.CERTPERIOD = FROMDATE + ' - ' + THRUDATE;
          });
        } else if (allVisitForms[i].FORMTYPE===4) { //if it's a recert visit
          console.log('recertification visit');
          dataService.get('episode', {KEYID:allVisitForms[i].EPIKEYID}).then(function (data) {
            var endOfEpisode = moment(data.data[0].THRUDATE);
            var newEpisodeStartDate = endOfEpisode.clone().add(1, 'days');
            var newEpisodeEndDate = endOfEpisode.clone().add(60, 'days');
            newEpisodeStartDate=$filter("date")(new Date(newEpisodeStartDate), 'MM-dd-yyyy');
            newEpisodeEndDate=$filter("date")(new Date(newEpisodeEndDate), 'MM-dd-yyyy');
            $scope.CERTPERIOD = newEpisodeStartDate + ' - ' + newEpisodeEndDate;
          });
        }
      });
    }
    // for (var i = 0; i < allVisitForms.length; i++) {
    //   if ( (allVisitForms[i].FORMTYPE===2) || (allVisitForms[i].FORMTYPE===3) || (allVisitForms[i].FORMTYPE===4) ) { //oasis form was added to the visit
    //     // console.log(allVisitForms[i])
    //     if (allVisitForms[i].OBJKEYID!==null) {
    //       dataService.get('oasisC2_2_20_1', {'KEYID': allVisitForms[i].OBJKEYID}).then(function(data){
    //         console.log('$scope.oasis', data.data[0]);
    //         $scope.oasis=data.data[0];
    //         $scope.oasis.M0030_START_CARE_DT=$filter('date')(data.data[0].M0030_START_CARE_DT, 'MM-dd-yyyy')
    //         //get certification date
    //         dataService.get('form', {VISITID: $scope.form.VISITID}).then(function (data) {
    //           // console.log(data.data)
    //           dataService.get('episode', {KEYID:data.data[0].EPIKEYID}).then(function (data) {
    //             var lastEpisode=data.data[data.data.length-1];
    //             console.log('soc visit', lastEpisode)
    //             lastEpisode.FROMDATE = $filter('date')(lastEpisode.FROMDATE, 'MM-dd-yyyy');
    //             lastEpisode.THRUDATE = $filter('date')(lastEpisode.THRUDATE, 'MM-dd-yyyy');
    //             $scope.CERTPERIOD = lastEpisode.FROMDATE + ' - ' + lastEpisode.THRUDATE
    //           })
    //         });
    //         //get oasis for same visit
    //         // dataService.get('form', {VISITID: $scope.form.VISITID}).then(function (data) {
    //         //   console.log(data.data)
    //         //   var formsForVisitArr=data.data;
    //         //   formsForVisitArr.forEach(function (form) {
    //         //     if (form.FORMTYPE===2) { //soc
    //         //       //visit is soc visit
    //         //       //get episode period
    //         //       dataService.get('episode', {KEYID:form.EPIKEYID}).then(function (data) {
    //         //         var lastEpisode=data.data[data.data.lenght-1];
    //         //         console.log('soc visit', lastEpisode)
    //         //         // episode.FROMDATE = $filter('date')(new Date(episode.FROMDATE), 'MM-dd-yyyy');
    //         //         // episode.THRUDATE = $filter('date')(new Date(episode.THRUDATE), 'MM-dd-yyyy');
    //         //         // $scope.CERTPERIOD = episode.FROMDATE + ' - ' + episode.THRUDATE
    //         //       })
    //         //     } else if (form.FORMTYPE===4)  {//recert
    //         //       //visit is recert visit
    //         //     }
    //         //   })
    //         // })
    //         //if oasis for same visit is soc
    //         //cert period is visit episode
    //         //if oasis for same visit is recert
    //         //cert period is visit episode+1
    //
    //         getCodeDesc($scope.oasis)
    //       });
    //     }
    //   }
    //   break;
    // }
  });

$scope.saveToPDF = function() {

  // Ivan added
	if ( $scope.oasis ) {
		var data = {
			KEYID: $scope.form.OBJKEYID,
			ClaimNo: $scope.oasis.M0020_PAT_ID,
			StartOfCareDate: $scope.admit.SOCDATE,
			CertPeriod: $scope.CERTPERIOD,
			MRecordNo: $scope.patient.INSID,
			ProviderNo: $scope.agency.PROVIDERNUM,
			PatientName: $scope.patient.FNAME + " " + $scope.patient.LNAME,
			PatientAddress: "" + ($scope.patient.ADDRESS===undefined?"": $scope.patient.ADDRESS+",") + ($scope.patient.CITY===undefined?"": $scope.patient.CITY+",") + ($scope.patient.STATE===undefined?"": $scope.patient.STATE+",") + ($scope.patient.ZIP===undefined?"":$scope.patient.ZIP) ,
			PatientPhone: $scope.patient.PHONE1,
			ProviderName: $scope.agency.NAME,
			ProviderAddress: "" + ($scope.agency.ADDRESS===undefined?"": $scope.agency.ADDRESS+",") + ($scope.agency.CITY===undefined?"": $scope.agency.CITY+",") + ($scope.agency.STATE===undefined?"": $scope.agency.STATE+",") + ($scope.agency.ZIP===undefined?"":$scope.agency.ZIP),
			ProviderNPI: $scope.agency.NPI,
			ProviderPhone: $scope.agency.PHONE,
      ProviderFax: $scope.agency.FAX,
      DoctorName: $scope.DOCTOR.FNAME + " " + $scope.DOCTOR.LNAME,
			DoctorAddress: $scope.DOCTOR.ADDRESS,
			DoctorNPI: $scope.DOCTOR.NPI,
			DoctorPhone: $scope.DOCTOR.PHONE,
			DoctorFax: $scope.DOCTOR.FAX,
			DoctorEmail: $scope.DOCTOR.EMAIL,
			DateOfBirth: $scope.patient.DOB,
			Sex: $scope.patient.GENDER,
			Medications: $scope.medications,
			ICD11: $scope.oasis.M1021_PRIMARY_DIAG_ICD,
			Principal: $scope.M1021,
			Date11: $scope.oasis.M0030_START_CARE_DT,
			ICD13: {
				M1023_OTH_DIAG1_ICD: $scope.oasis.M1023_OTH_DIAG1_ICD,
				M1023_OTH_DIAG2_ICD: $scope.oasis.M1023_OTH_DIAG2_ICD,
				M1023_OTH_DIAG3_ICD: $scope.oasis.M1023_OTH_DIAG3_ICD,
				M1023_OTH_DIAG4_ICD: $scope.oasis.M1023_OTH_DIAG4_ICD,
				M1023_OTH_DIAG5_ICD: $scope.oasis.M1023_OTH_DIAG5_ICD,

				M1025_OPT_DIAG_ICD_A3: $scope.oasis.M1025_OPT_DIAG_ICD_A3,
				M1025_OPT_DIAG_ICD_A4: $scope.oasis.M1025_OPT_DIAG_ICD_A4,
				M1025_OPT_DIAG_ICD_B3: $scope.oasis.M1025_OPT_DIAG_ICD_B3,
				M1025_OPT_DIAG_ICD_B4: $scope.oasis.M1025_OPT_DIAG_ICD_B4,
				M1025_OPT_DIAG_ICD_C3: $scope.oasis.M1025_OPT_DIAG_ICD_C3,
				M1025_OPT_DIAG_ICD_C4: $scope.oasis.M1025_OPT_DIAG_ICD_C4,
				M1025_OPT_DIAG_ICD_D3: $scope.oasis.M1025_OPT_DIAG_ICD_D3,
				M1025_OPT_DIAG_ICD_D4: $scope.oasis.M1025_OPT_DIAG_ICD_D4,
				M1025_OPT_DIAG_ICD_E3: $scope.oasis.M1025_OPT_DIAG_ICD_E3,
				M1025_OPT_DIAG_ICD_E4: $scope.oasis.M1025_OPT_DIAG_ICD_E4,
				M1025_OPT_DIAG_ICD_F3: $scope.oasis.M1025_OPT_DIAG_ICD_F3,
				M1025_OPT_DIAG_ICD_F4: $scope.oasis.M1025_OPT_DIAG_ICD_F4
			},
			PertinentDiagnosis: {
				M1023_1: $scope.oasis.M1023_1,
				M1023_2: $scope.oasis.M1023_2,
				M1023_3: $scope.oasis.M1023_3,
				M1023_4: $scope.oasis.M1023_4,
				M1023_5: $scope.oasis.M1023_5,

				M1025A_3: $scope.oasis.M1025A_3,
				M1025A_4: $scope.oasis.M1025A_4,
				M1025B_3: $scope.oasis.M1025B_3,
				M1025B_4: $scope.oasis.M1025B_4,
				M1025C_3: $scope.oasis.M1025C_3,
				M1025C_4: $scope.oasis.M1025C_4,
				M1025D_3: $scope.oasis.M1025D_3,
				M1025D_4: $scope.oasis.M1025D_4,
				M1025E_3: $scope.oasis.M1025E_3,
				M1025E_4: $scope.oasis.M1025E_4,
				M1025F_3: $scope.oasis.M1025F_3,
				M1025F_4: $scope.oasis.M1025F_4
			}
		}
	} else {
		var data = {
			KEYID: $scope.form.OBJKEYID,
			ClaimNo: "",
			StartOfCareDate: $scope.admit.SOCDATE,
			CertPeriod: $scope.CERTPERIOD,
			MRecordNo: $scope.patient.INSID,
			ProviderNo: $scope.agency.PROVIDERNUM,
			PatientName: $scope.patient.FNAME + " " + $scope.patient.LNAME,
      PatientAddress: "" + ($scope.patient.ADDRESS===undefined?"": $scope.patient.ADDRESS+",") + ($scope.patient.CITY===undefined?"": $scope.patient.CITY+",") + ($scope.patient.STATE===undefined?"": $scope.patient.STATE+",") + ($scope.patient.ZIP===undefined?"":$scope.patient.ZIP) ,
			PatientPhone: $scope.patient.PHONE1,
			ProviderName: $scope.agency.NAME,
     	ProviderAddress: "" + ($scope.agency.ADDRESS===undefined?"": $scope.agency.ADDRESS+",") + ($scope.agency.CITY===undefined?"": $scope.agency.CITY+",") + ($scope.agency.STATE===undefined?"": $scope.agency.STATE+",") + ($scope.agency.ZIP===undefined?"":$scope.agency.ZIP),
      ProviderNPI: $scope.agency.NPI,
			ProviderPhone: $scope.agency.PHONE,
			ProviderFax: $scope.agency.FAX,
			DoctorName: $scope.DOCTOR.FNAME + " " + $scope.DOCTOR.LNAME,
			DoctorAddress: $scope.DOCTOR.ADDRESS,
			DoctorNPI: $scope.DOCTOR.NPI,
			DoctorPhone: $scope.DOCTOR.PHONE,
			DoctorFax: $scope.DOCTOR.FAX,
			DoctorEmail: $scope.DOCTOR.EMAIL,
			DateOfBirth: $scope.patient.DOB,
			Sex: $scope.patient.GENDER,
			Medications: $scope.medications,
		}
	}

  $http({
    url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['planOfCare'].getPDF,
    method: 'GET',
    params: data,
    paramSerializer: '$httpParamSerializerJQLike',
    responseType: 'arraybuffer'
  }).then(function(data) {
    console.log('data', data);
    console.log('headers', data.headers());

    var contentType = data.headers()['content-type'];
    var disposition = data.headers()['content-disposition'];
    console.log( 'content-type', contentType );
    console.log('disposition', disposition);

    /*
    if (disposition && disposition.indexOf('attachment') !== -1) {
      var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
      var matches = filenameRegex.exec(disposition);
      if (matches != null && matches[1]) { 
        filename = matches[1].replace(/['"]/g, '');
      } else {
        filename = "unknown";
      }
    }
    */

    filename = "CMS_485_" + $scope.patient.INSID + "_" + $scope.CERTPERIOD;
    console.log('filename', filename);
    console.log('contentType', contentType)
    console.log('data.data', data.data);
    
    var dataFile = new Blob([data.data], { type: contentType });

    console.log('dataFile', dataFile);

    FileSaver.saveAs(dataFile, filename);
  });
};

  //get the description for the icd codes
  function getCodeDesc(oasis) {
    var INPArr=[
      oasis.M1011_14_DAY_INP1_ICD,
      oasis.M1011_14_DAY_INP2_ICD,
      oasis.M1011_14_DAY_INP3_ICD,
      oasis.M1011_14_DAY_INP4_ICD,
      oasis.M1011_14_DAY_INP5_ICD,
      oasis.M1011_14_DAY_INP6_ICD
    ];
    function getM1011desc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1011_' + (i+1)]=codeDesc;
      });
    }
    for (var i = 0; i < INPArr.length; i++) {
      if (INPArr[i]!==undefined && INPArr[i]!=="" && INPArr[i]!==null) {
        var code=INPArr[i].replace('.', '');
        getM1011desc(i)
      }
    }
    var chgregArr=[
      oasis.M1017_CHGREG_ICD1,
      oasis.M1017_CHGREG_ICD2,
      oasis.M1017_CHGREG_ICD3,
      oasis.M1017_CHGREG_ICD4,
      oasis.M1017_CHGREG_ICD5,
      oasis.M1017_CHGREG_ICD6,
    ];
    function getM1017desc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1017_' + (i+1)]=codeDesc;
      });
    }
    for (var i = 0; i < chgregArr.length; i++) {
      if (chgregArr[i]!==undefined && chgregArr[i]!=="" && chgregArr[i]!==null) {
        var code=chgregArr[i].replace('.', '');
        getM1017desc(i)
      }
    }
    var m1021Arr=[
      oasis.M1021_PRIMARY_DIAG_ICD,
    ];
    function getM1021desc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1021']=codeDesc;
      });
    }
    for (var i = 0; i < m1021Arr.length; i++) {
      if (m1021Arr[i]!==undefined && m1021Arr[i]!=="" && m1021Arr[i]!==null) {
        var code=m1021Arr[i].replace('.', '');
        getM1021desc(i)
      }
    }
    var m1023Arr=[
      oasis.M1023_OTH_DIAG1_ICD,
      oasis.M1023_OTH_DIAG2_ICD,
      oasis.M1023_OTH_DIAG3_ICD,
      oasis.M1023_OTH_DIAG4_ICD,
      oasis.M1023_OTH_DIAG5_ICD,
    ];
    function getM1023desc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1023_' + (i+1)]=codeDesc;
      });
    }
    for (var i = 0; i < m1023Arr.length; i++) {
      if (m1023Arr[i]!==undefined && m1023Arr[i]!=="" && m1023Arr[i]!==null) {
        var code=m1023Arr[i].replace('.', '');
        getM1023desc(i)
      }
    }
    var m1025aArr=[
      oasis.M1025_OPT_DIAG_ICD_A3,
      oasis.M1025_OPT_DIAG_ICD_A4,
    ];
    function getM1025adesc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1025A_' + (i+3)]=codeDesc;
      });
    }
    for (var i = 0; i < m1025aArr.length; i++) {
      if (m1025aArr[i]!==undefined && m1025aArr[i]!=="" && m1025aArr[i]!==null) {
        var code=m1025aArr[i].replace('.', '');
        getM1025adesc(i)
      }
    }
    var m1025bArr=[
      oasis.M1025_OPT_DIAG_ICD_B3,
      oasis.M1025_OPT_DIAG_ICD_B4,
    ];
    function getM1025Bdesc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1025B_' + (i+3)]=codeDesc;
      });
    }
    for (var i = 0; i < m1025aArr.length; i++) {
      if (m1025bArr[i]!==undefined && m1025bArr[i]!=="" && m1025bArr[i]!==null) {
        var code=m1025bArr[i].replace('.', '');
        getM1025Bdesc(i)
      }
    }
    var m1025cArr=[
      oasis.M1025_OPT_DIAG_ICD_C3,
      oasis.M1025_OPT_DIAG_ICD_C4,
    ];
    function getM1025Cdesc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1025C_' + (i+3)]=codeDesc;
      });
    }
    for (var i = 0; i < m1025aArr.length; i++) {
      if (m1025cArr[i]!==undefined && m1025cArr[i]!=="" && m1025cArr[i]!==null) {
        var code=m1025cArr[i].replace('.', '');
        getM1025Cdesc(i)
      }
    }
    var m1025dArr=[
      oasis.M1025_OPT_DIAG_ICD_D3,
      oasis.M1025_OPT_DIAG_ICD_D4,
    ];
    function getM1025Ddesc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1025D_' + (i+3)]=codeDesc;
      });
    }
    for (var i = 0; i < m1025aArr.length; i++) {
      if (m1025dArr[i]!==undefined && m1025dArr[i]!=="" && m1025dArr[i]!==null) {
        var code=m1025dArr[i].replace('.', '');
        getM1025Ddesc(i)
      }
    }
    var m1025eArr=[
      oasis.M1025_OPT_DIAG_ICD_E3,
      oasis.M1025_OPT_DIAG_ICD_E4,
    ];
    function getM1025Edesc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1025E_' + (i+3)]=codeDesc;
      });
    }
    for (var i = 0; i < m1025aArr.length; i++) {
      if (m1025eArr[i]!==undefined && m1025eArr[i]!=="" && m1025eArr[i]!==null) {
        var code=m1025eArr[i].replace('.', '');
        getM1025Edesc(i)
      }
    }
    var m1025fArr=[
      oasis.M1025_OPT_DIAG_ICD_F3,
      oasis.M1025_OPT_DIAG_ICD_F4,
    ];
    function getM1025Fdesc(i) {
      dataService.get('ICD10Code', {CODE:code }).then(function (data) {
        var codeDesc=data.data[0].DESCRIPTION;
        $scope['M1025F_' + (i+3)]=codeDesc;
      });
    }
    for (var i = 0; i < m1025aArr.length; i++) {
      if (m1025fArr[i]!==undefined && m1025fArr[i]!=="" && m1025fArr[i]!==null) {
        var code=m1025fArr[i].replace('.', '');
        getM1025Fdesc(i)
      }
    }
  }

  //get POC and populate form with fields
  $scope.getPOC=function () {
    dataService.get('planOfCare', {KEYID:$scope.form.OBJKEYID}).then(function (data) {
      console.log('planofcare', data.data);
      var POC=data.data;
      $scope.Supplies=POC.SUPPLIES.Supplies;
      $scope.Trays=POC.SUPPLIES.Trays;
      $scope.Irrigation=POC.SUPPLIES.Irrigation;
      $scope.Gloves=POC.SUPPLIES.Gloves;
      $scope.Walker=POC.SUPPLIES.Walker;
      $scope.Commode=POC.SUPPLIES.Commode;
      $scope.Tape=POC.SUPPLIES.Tape;
      $scope.Syringes=POC.SUPPLIES.Syringes;
      $scope.Dressings=POC.SUPPLIES.Dressings;
      $scope.WC=POC.SUPPLIES.WC;
      $scope.NS=POC.SUPPLIES.NS;
      $scope.Ostomy=POC.SUPPLIES.Ostomy;
      $scope.O2=POC.SUPPLIES.O2;
      $scope.Cane=POC.SUPPLIES.Cane;
      $scope.Hoyerlift=POC.SUPPLIES.Hoyerlift;
      $scope.Glucometer=POC.SUPPLIES.Glucometer;
      $scope.Sharps=POC.SUPPLIES.Sharps;
      $scope.Shower=POC.SUPPLIES.Shower;
      $scope.Tube=POC.SUPPLIES.Tube;
      $scope.Pump=POC.SUPPLIES.Pump;
      $scope.Hospital=POC.SUPPLIES.Hospital;
      $scope.Other1=POC.SUPPLIES.Other1;
      $scope.Seizure=POC.SAFETY.Seizure;
      $scope.Wheelchair=POC.SAFETY.Wheelchair;
      $scope.Precautions=POC.SAFETY.Precautions;
      $scope.Anticoagulant=POC.SAFETY.Anticoagulant;
      $scope.Hoyer=POC.SAFETY.Hoyer;
      $scope.Lifeline=POC.SAFETY.Lifeline;
      $scope.Disposal=POC.SAFETY.Disposal;
      $scope.Fall=POC.SAFETY.Fall;
      $scope.Infection=POC.SAFETY.Infection;
      $scope.SMA=POC.SAFETY.SMA;
      $scope.Scatter=POC.SAFETY.Scatter;
      $scope.Pathways=POC.SAFETY.Pathways;
      $scope.TWA=POC.SAFETY.TWA;
      $scope.Railsup=POC.SAFETY.Railsup;
      $scope.rail1=POC.SAFETY.rail1;
      $scope.rail2=POC.SAFETY.rail2;
      $scope.pds=POC.SAFETY.pds;
      $scope.Emergency=POC.SAFETY.Emergency;
      $scope.Other2=POC.SAFETY.Other2;
      $scope.Diet=POC.NUTRITIONAL.Diet;
      $scope.Fluid=POC.NUTRITIONAL.Fluid;
      $scope.gts=POC.NUTRITIONAL.gts;
      $scope.ff=POC.NUTRITIONAL.ff;
      $scope.Medication=POC.NUTRITIONAL.Medication;
      $scope.Food=POC.NUTRITIONAL.Food;
      $scope.Amputation=POC.FUNCTIONAL.Amputation;
      $scope.Bowel=POC.FUNCTIONAL.Bowel;
      $scope.Contracture=POC.FUNCTIONAL.Contracture;
      $scope.Hearing=POC.FUNCTIONAL.Hearing;
      $scope.Paralysis=POC.FUNCTIONAL.Paralysis;
      $scope.Endurance=POC.FUNCTIONAL.Endurance;
      $scope.Ambulation=POC.FUNCTIONAL.Ambulation;
      $scope.Speech=POC.FUNCTIONAL.Speech;
      $scope.Dyspnea=POC.FUNCTIONAL.Dyspnea;
      $scope.Blind=POC.FUNCTIONAL.Blind;
      $scope.Otherfun=POC.FUNCTIONAL.Otherfun;
      $scope.Bedrest=POC.ACTIVITIES.Bedrest;
      $scope.brp=POC.ACTIVITIES.brp;
      $scope.uat=POC.ACTIVITIES.uat;
      $scope.bt=POC.ACTIVITIES.bt;
      $scope.ep=POC.ACTIVITIES.ep;
      $scope.pwb=POC.ACTIVITIES.pwb;
      $scope.iah=POC.ACTIVITIES.iah;
      $scope.Actwalker=POC.ACTIVITIES.Actwalker;
      $scope.Crutches=POC.ACTIVITIES.Crutches;
      $scope.Caneact=POC.ACTIVITIES.Caneact;
      $scope.act_wc=POC.ACTIVITIES.act_wc;
      $scope.restrictions=POC.ACTIVITIES.restrictions;
      $scope.Other3=POC.ACTIVITIES.Other3;
      $scope.Oriented=POC.MENTAL.Oriented;
      $scope.Comatose=POC.MENTAL.Comatose;
      $scope.Forgetful=POC.MENTAL.Forgetful;
      $scope.Depressed=POC.MENTAL.Depressed;
      $scope.Disoriented=POC.MENTAL.Disoriented;
      $scope.Agitated=POC.MENTAL.Agitated;
      $scope.Other4=POC.MENTAL.Other4;
      $scope.Prognosis=POC.ORDERS.race;
      $scope.rehab=POC.ORDERS.rehab;
      $scope.mdsn=POC.ORDERS.mdsn;
      $scope.txtpatientzipcode=POC.ORDERS.txtpatientzipcode;
      $scope.ept=POC.ORDERS.ept;
      $scope.ote=POC.ORDERS.ote;
      $scope.ste=POC.ORDERS.ste;
      $scope.msw=POC.ORDERS.msw;
      $scope.pns=POC.ORDERS.pns;
      $scope.psd=POC.ORDERS.psd;
      $scope.md=POC.SKILLED.md;
      $scope.sbp=POC.SKILLED.sbp;
      $scope.lt=POC.SKILLED.lt;
      $scope.dbp=POC.SKILLED.dbp;
      $scope.olt=POC.SKILLED.olt;
      $scope.pgt=POC.SKILLED.pgt;
      $scope.gtr=POC.SKILLED.gtr;
      $scope.lrs=POC.SKILLED.lrs;
      $scope.rgt=POC.SKILLED.rgt;
      $scope.grt=POC.SKILLED.grt;
      $scope.ls=POC.SKILLED.ls;
      $scope.tem=POC.SKILLED.tem;
      $scope.tgt=POC.SKILLED.tgt;
      $scope.eig=POC.SKILLED.eig;
      $scope.ggt=POC.SKILLED.ggt;
      $scope.lbsi=POC.SKILLED.lbsi;
      $scope.bsf=POC.SKILLED.bsf;
      $scope.mbs=POC.SKILLED.mbs;
      $scope.bsg=POC.SKILLED.bsg;
      $scope.orlt=POC.SKILLED.orlt;
      $scope.ypp=POC.SKILLED.ypp;
      $scope.tbp=POC.SKILLED.tbp;
      $scope.pls=POC.SKILLED.pls;
      $scope.ght=POC.SKILLED.ght;
      $scope.sn=POC.SKILLED.sn;
      $scope.ians=POC.SKILLED.ians;
      $scope.oxy=POC.SKILLED.oxy;
      $scope.svn=POC.SKILLED.svn;
      $scope.wcs=POC.SKILLED.wcs;
      $scope.tsf=POC.SKILLED.tsf;
      $scope.eth=POC.SKILLED.eth;
      $scope.hha=POC.SKILLED.hha;
      $scope.vs=POC.SKILLED.vs;
      $scope.tpc=POC.SKILLED.tpc;
      $scope.pcw=POC.SKILLED.pcw;
      $scope.mbp=POC.SKILLED.mbp;
      $scope.plse=POC.SKILLED.plse;
      $scope.tsn=POC.SKILLED.tsn;
      $scope.pc=POC.SKILLED.pc;
      $scope.ven=POC.LAB.ven;
      $scope.idt=POC.LAB.idt;
      $scope.ntt=POC.LAB.ntt;
      $scope.snw=POC.IV.snw;
      $scope.dos=POC.IV.dos;
      $scope.rtm=POC.IV.rtm;
      $scope.hickman=POC.IV.hickman;
      $scope.ggc=POC.IV.ggc;
      $scope.picc=POC.IV.picc;
      $scope.perl=POC.IV.perl;
      $scope.ptc=POC.IV.ptc;
      $scope.frq=POC.IV.frq;
      $scope.lot=POC.IV.lot;
      $scope.alf=POC.IV.alf;
      $scope.flw=POC.IV.flw;
      $scope.fln=POC.IV.fln;
      $scope.adm=POC.IV.adm;
      $scope.ini=POC.IV.ini;
      $scope.medi=POC.IV.medi;
      $scope.cco=POC.IV.cco;
      $scope.flusw=POC.IV.flusw;
      $scope.linew=POC.IV.linew;
      $scope.foll=POC.IV.foll;
      $scope.hep=POC.IV.hep;
      $scope.cpic=POC.IV.cpic;
      $scope.cht=POC.IV.cht;
      $scope.dpt=POC.IV.dpt;
      $scope.drq=POC.IV.drq;
      $scope.fry=POC.IV.fry;
      $scope.ipc=POC.IV.ipc;
      $scope.inc=POC.IV.inc;
      $scope.piu=POC.IV.piu;
      $scope.infec=POC.IV.infec;
      $scope.was=POC.IV.was;
      $scope.mat=POC.IV.mat;
      $scope.app=POC.IV.app;
      $scope.othr=POC.IV.othr;
      $scope.otr=POC.IV.otr;
      $scope.tpi=POC.IV.tpi;
      $scope.temo=POC.IV.temo;
      $scope.tpcr=POC.IV.tpcr;
      $scope.equ=POC.IV.equ;
      $scope.othh=POC.IV.othh;
      $scope.otre=POC.IV.otre;
      $scope.tsnw=POC.PAIN.tsnw;
      $scope.willm=POC.PAIN.willm;
      $scope.medic=POC.PAIN.medic;
      $scope.snin=POC.PAIN.snin;
      $scope.inth=POC.PAIN.inth;
      $scope.inst=POC.PAIN.inst;
      $scope.otreh=POC.PAIN.otreh;
      $scope.otrh=POC.PAIN.otrh;
      $scope.moni=POC.PAIN.moni;
      $scope.tor=POC.PAIN.tor;
      $scope.tore=POC.PAIN.tore;
      $scope.tpp=POC.PAIN.tpp;
      $scope.cont=POC.PAIN.cont;
      $scope.bet=POC.PAIN.bet;
      $scope.wit=POC.PAIN.wit;
      $scope.pati=POC.PAIN.pati;
      $scope.witn=POC.PAIN.witn;
      $scope.careg=POC.PAIN.careg;
      $scope.freq=POC.PAIN.freq;
      $scope.pai=POC.PAIN.pai;
      $scope.twil=POC.PAIN.twil;
      $scope.str=POC.PAIN.str;
      $scope.wo=POC.PAIN.wo;
      $scope.wou=POC.PAIN.wou;
      $scope.cle=POC.PAIN.cle;
      $scope.dr=POC.PAIN.dr;
      $scope.wth=POC.PAIN.wth;
      $scope.cov=POC.PAIN.cov;
      $scope.sec=POC.PAIN.sec;
      $scope.wu=POC.PAIN.wu;
      $scope.won=POC.PAIN.won;
      $scope.cla=POC.PAIN.cla;
      $scope.patd=POC.PAIN.patd;
      $scope.pac=POC.PAIN.pac;
      $scope.cove=POC.PAIN.cove;
      $scope.secu=POC.PAIN.secu;
      $scope.wn=POC.PAIN.wn;
      $scope.wod=POC.PAIN.wod;
      $scope.cln=POC.PAIN.cln;
      $scope.yap=POC.PAIN.yap;
      $scope.kw=POC.PAIN.kw;
      $scope.ver=POC.PAIN.ver;
      $scope.secr=POC.PAIN.secr;
      $scope.wd=POC.PAIN.wd;
      $scope.wun=POC.PAIN.wun;
      $scope.cls=POC.PAIN.cls;
      $scope.ylp=POC.PAIN.ylp;
      $scope.ack=POC.PAIN.ack;
      $scope.ith=POC.PAIN.ith;
      $scope.ser=POC.PAIN.ser;
      $scope.mea=POC.PAIN.mea;
      $scope.ase=POC.PAIN.ase;
      $scope.noti=POC.PAIN.noti;
      $scope.basl=POC.PAIN.basl;
      $scope.tru=POC.PAIN.tru;
      $scope.ent=POC.PAIN.ent;
      $scope.reg=POC.PAIN.reg;
      $scope.uct=POC.PAIN.uct;
      $scope.dec=POC.PAIN.dec;
      $scope.siz=POC.PAIN.siz;
      $scope.cmw=POC.PAIN.cmw;
      $scope.comp=POC.PAIN.comp;
      $scope.heal=POC.PAIN.heal;
      $scope.fr=POC.PAIN.fr;
      $scope.bsl=POC.PAIN.bsl;
      $scope.ovr=POC.PAIN.ovr;
      $scope.verb=POC.PAIN.verb;
      $scope.verw=POC.PAIN.verw;
      $scope.wthi=POC.PAIN.wthi;
      $scope.dem=POC.PAIN.dem;
      $scope.abl=POC.PAIN.abl;
      $scope.rela=POC.MUSCULOSKELETAL.rela;
      $scope.instr=POC.MUSCULOSKELETAL.instr;
      $scope.sat=POC.MUSCULOSKELETAL.sat;
      $scope.Asses=POC.MUSCULOSKELETAL.Asses;
      $scope.pasn=POC.MUSCULOSKELETAL.pasn;
      $scope.uso=POC.MUSCULOSKELETAL.uso;
      $scope.car=POC.MUSCULOSKELETAL.car;
      $scope.gac=POC.MUSCULOSKELETAL.gac;
      $scope.ety=POC.MUSCULOSKELETAL.ety;
      $scope.ble=POC.MUSCULOSKELETAL.ble;
      $scope.thin=POC.MUSCULOSKELETAL.thin;
      $scope.balo=POC.MUSCULOSKELETAL.balo;
      $scope.mdw=POC.MUSCULOSKELETAL.mdw;
      $scope.stra=POC.MUSCULOSKELETAL.stra;
      $scope.vic=POC.MUSCULOSKELETAL.vic;
      $scope.cto=POC.MUSCULOSKELETAL.cto;
      $scope.fet=POC.MUSCULOSKELETAL.fet;
      $scope.atr=POC.RENAL.atr;
      $scope.mess=POC.RENAL.mess;
      $scope.eat=POC.RENAL.eat;
      $scope.cted=POC.RENAL.cted;
      $scope.snwl=POC.RENAL.snwl;
      $scope.raig=POC.RENAL.raig;
      $scope.het=POC.RENAL.het;
      $scope.fole=POC.RENAL.fole;
      $scope.folc=POC.RENAL.folc;
      $scope.ench=POC.RENAL.ench;
      $scope.loon=POC.RENAL.loon;
      $scope.patie=POC.RENAL.patie;
      $scope.giv=POC.RENAL.giv;
      $scope.lmo=POC.RENAL.lmo;
      $scope.very=POC.RENAL.very;
      $scope.regis=POC.RENAL.regis;
      $scope.let=POC.RENAL.let;
      $scope.ual=POC.RENAL.ual;
      $scope.ify=POC.RENAL.ify;
      $scope.equa=POC.RENAL.equa;
      $scope.hyd=POC.RENAL.hyd;
      $scope.sxi=POC.RENAL.sxi;
      $scope.ien=POC.RENAL.ien;
      $scope.ski=POC.RENAL.ski;
      $scope.ete=POC.RENAL.ete;
      $scope.bagg=POC.RENAL.bagg;
      $scope.ets=POC.RENAL.ets;
      $scope.truc=POC.RENAL.truc;
      $scope.uti=POC.RENAL.uti;
      $scope.lipl=POC.RENAL.lipl;
      $scope.rit=POC.RENAL.rit;
      $scope.odo=POC.RENAL.odo;
      $scope.patc=POC.RENAL.patc;
      $scope.now=POC.RENAL.now;
      $scope.pcg=POC.RENAL.pcg;
      $scope.bali=POC.RENAL.bali;
      $scope.fetc=POC.RENAL.fetc;
      $scope.fem=POC.RENAL.fem;
      $scope.mai=POC.RENAL.mai;
      $scope.fec=POC.RENAL.fec;
      $scope.tyr=POC.RENAL.tyr;
      $scope.forc=POC.RENAL.forc;
      $scope.ated=POC.CARDIOVASCULAR.ated;
      $scope.ess=POC.CARDIOVASCULAR.ess;
      $scope.pats=POC.CARDIOVASCULAR.pats;
      $scope.gic=POC.CARDIOVASCULAR.gic;
      $scope.gard=POC.CARDIOVASCULAR.gard;
      $scope.era=POC.CARDIOVASCULAR.era;
      $scope.edm=POC.CARDIOVASCULAR.edm;
      $scope.cha=POC.CARDIOVASCULAR.cha;
      $scope.chn=POC.CARDIOVASCULAR.chn;
      $scope.fib=POC.CARDIOVASCULAR.fib;
      $scope.ail=POC.CARDIOVASCULAR.ail;
      $scope.mdo=POC.CARDIOVASCULAR.mdo;
      $scope.ole=POC.CARDIOVASCULAR.ole;
      $scope.dix=POC.CARDIOVASCULAR.dix;
      $scope.pos=POC.CARDIOVASCULAR.pos;
      $scope.itr=POC.CARDIOVASCULAR.itr;
      $scope.mpl=POC.CARDIOVASCULAR.mpl;
      $scope.ars=POC.CARDIOVASCULAR.ars;
      $scope.equs=POC.CARDIOVASCULAR.equs;
      $scope.clu=POC.CARDIOVASCULAR.clu;
      $scope.tpa=POC.CARDIOVASCULAR.tpa;
      $scope.dosg=POC.CARDIOVASCULAR.dosg;
      $scope.medc=POC.CARDIOVASCULAR.medc;
      $scope.verp=POC.CARDIOVASCULAR.verp;
      $scope.imi=POC.CARDIOVASCULAR.imi;
      $scope.chp=POC.CARDIOVASCULAR.chp;
      $scope.afe=POC.CARDIOVASCULAR.afe;
      $scope.patp=POC.CARDIOVASCULAR.patp;
      $scope.patws=POC.CARDIOVASCULAR.patws;
      $scope.bls=POC.CARDIOVASCULAR.bls;
      $scope.pab=POC.CARDIOVASCULAR.pab;
      $scope.patw=POC.CARDIOVASCULAR.patw;
      $scope.demd=POC.CARDIOVASCULAR.demd;
      $scope.resw=POC.CARDIOVASCULAR.resw;
      $scope.alig=POC.CARDIOVASCULAR.alig;
      $scope.adh=POC.CARDIOVASCULAR.adh;
      $scope.thi=POC.CARDIOVASCULAR.thi;
      $scope.trp=POC.CARDIOVASCULAR.trp;
      $scope.demo=POC.CARDIOVASCULAR.demo;
      $scope.ida=POC.NEUROLOGICAL.ida;
      $scope.hgr=POC.NEUROLOGICAL.hgr;
      $scope.gris=POC.NEUROLOGICAL.gris;
      $scope.pup=POC.NEUROLOGICAL.pup;
      $scope.wea=POC.NEUROLOGICAL.wea;
      $scope.iou=POC.NEUROLOGICAL.iou;
      $scope.hal=POC.NEUROLOGICAL.hal;
      $scope.dirc=POC.NEUROLOGICAL.dirc;
      $scope.patg=POC.NEUROLOGICAL.patg;
      $scope.cgp=POC.NEUROLOGICAL.cgp;
      $scope.ffe=POC.NEUROLOGICAL.ffe;
      $scope.regt=POC.NEUROLOGICAL.regt;
      $scope.regi=POC.NEUROLOGICAL.regi;
      $scope.ses=POC.NEUROLOGICAL.ses;
      $scope.mana=POC.NEUROLOGICAL.mana;
      $scope.age=POC.NEUROLOGICAL.age;
      $scope.req=POC.NEUROLOGICAL.req;
      $scope.eff=POC.NEUROLOGICAL.eff;
      $scope.relt=POC.NEUROLOGICAL.relt;
      $scope.ons=POC.NEUROLOGICAL.ons;
      $scope.icat=POC.NEUROLOGICAL.icat;
      $scope.ict=POC.NEUROLOGICAL.ict;
      $scope.carg=POC.NEUROLOGICAL.carg;
      $scope.bals=POC.NEUROLOGICAL.bals;
      $scope.sei=POC.NEUROLOGICAL.sei;
      $scope.spa=POC.NEUROLOGICAL.spa;
      $scope.con=POC.NEUROLOGICAL.con;
      $scope.frqs=POC.NEUROLOGICAL.frqs;
      $scope.emo=POC.NEUROLOGICAL.emo;
      $scope.ica=POC.NEUROLOGICAL.ica;
      $scope.onsa=POC.NEUROLOGICAL.onsa;
      $scope.kdo=POC.NEUROLOGICAL.kdo;
      $scope.rba=POC.NEUROLOGICAL.rba;
      $scope.mdwa=POC.NEUROLOGICAL.mdwa;
      $scope.neu=POC.NEUROLOGICAL.neu;
      $scope.sxs=POC.NEUROLOGICAL.sxs;
      $scope.end=POC.ENDOCRINE.end;
      $scope.isn=POC.ENDOCRINE.isn;
      $scope.tin=POC.ENDOCRINE.tin;
      $scope.rat=POC.ENDOCRINE.rat;
      $scope.chg=POC.ENDOCRINE.chg;
      $scope.gar=POC.ENDOCRINE.gar;
      $scope.repl=POC.ENDOCRINE.repl;
      $scope.hta=POC.ENDOCRINE.hta;
      $scope.sst=POC.ENDOCRINE.sst;
      $scope.admi=POC.ENDOCRINE.admi;
      $scope.tsr=POC.ENDOCRINE.tsr;
      $scope.rot=POC.ENDOCRINE.rot;
      $scope.ind=POC.ENDOCRINE.ind;
      $scope.dia=POC.ENDOCRINE.dia;
      $scope.ret=POC.ENDOCRINE.ret;
      $scope.abg=POC.ENDOCRINE.abg;
      $scope.prs=POC.ENDOCRINE.prs;
      $scope.sug=POC.ENDOCRINE.sug;
      $scope.lan=POC.ENDOCRINE.lan;
      $scope.cgl=POC.ENDOCRINE.cgl;
      $scope.acc=POC.ENDOCRINE.acc;
      $scope.wpa=POC.ENDOCRINE.wpa;
      $scope.alis=POC.ENDOCRINE.alis;
      $scope.perg=POC.ENDOCRINE.perg;
      $scope.ves=POC.ENDOCRINE.ves;
      $scope.det=POC.ENDOCRINE.det;
      $scope.wee=POC.ENDOCRINE.wee;
      $scope.ont=POC.ENDOCRINE.ont;
      $scope.com=POC.ENDOCRINE.com;
      $scope.suw=POC.ENDOCRINE.suw;
      $scope.ents=POC.ENDOCRINE.ents;
      $scope.goos=POC.ENDOCRINE.goos;
      $scope.dis=POC.ENDOCRINE.dis;
      $scope.bes=POC.ENDOCRINE.bes;
      $scope.ysi=POC.ENDOCRINE.ysi;
      $scope.clus=POC.ENDOCRINE.clus;
      $scope.int=POC.ENDOCRINE.int;
      $scope.ela=POC.RESPIRATORY.ela;
      $scope.nst=POC.RESPIRATORY.nst;
      $scope.res=POC.RESPIRATORY.res;
      $scope.ruc=POC.RESPIRATORY.ruc;
      $scope.brs=POC.RESPIRATORY.brs;
      $scope.est=POC.RESPIRATORY.est;
      $scope.patl=POC.RESPIRATORY.patl;
      $scope.trm=POC.RESPIRATORY.trm;
      $scope.yel=POC.RESPIRATORY.yel;
      $scope.hec=POC.RESPIRATORY.hec;
      $scope.mok=POC.RESPIRATORY.mok;
      $scope.fla=POC.RESPIRATORY.fla;
      $scope.merg=POC.RESPIRATORY.merg;
      $scope.ner=POC.RESPIRATORY.ner;
      $scope.oni=POC.RESPIRATORY.oni;
      $scope.cas=POC.RESPIRATORY.cas;
      $scope.cti=POC.RESPIRATORY.cti;
      $scope.redg=POC.RESPIRATORY.redg;
      $scope.quf=POC.RESPIRATORY.quf;
      $scope.ids=POC.RESPIRATORY.ids;
      $scope.cta=POC.RESPIRATORY.cta;
      $scope.bal=POC.RESPIRATORY.bal;
      $scope.mdwh=POC.RESPIRATORY.mdwh;
      $scope.ems=POC.RESPIRATORY.ems;
      $scope.qui=POC.RESPIRATORY.qui;
      $scope.rats=POC.RESPIRATORY.rats;
      $scope.ratsw=POC.RESPIRATORY.ratsw;
      $scope.dto=POC.RESPIRATORY.dto;
      $scope.liz=POC.RESPIRATORY.liz;
      $scope.rwp=POC.RESPIRATORY.rwp;
      $scope.egi=POC.RESPIRATORY.egi;
      $scope.aliz=POC.RESPIRATORY.aliz;
      $scope.adp=POC.GASTROINTESTINAL.adp;
      $scope.ccz=POC.GASTROINTESTINAL.ccz;
      $scope.nuk=POC.GASTROINTESTINAL.nuk;
      $scope.nau=POC.GASTROINTESTINAL.nau;
      $scope.pym=POC.GASTROINTESTINAL.pym;
      $scope.vom=POC.GASTROINTESTINAL.vom;
      $scope.disa=POC.GASTROINTESTINAL.disa;
      $scope.thw=POC.GASTROINTESTINAL.thw;
      $scope.prl=POC.GASTROINTESTINAL.prl;
      $scope.rev=POC.GASTROINTESTINAL.rev;
      $scope.olz=POC.GASTROINTESTINAL.olz;
      $scope.ibr=POC.GASTROINTESTINAL.ibr;
      $scope.buu=POC.GASTROINTESTINAL.buu;
      $scope.ages=POC.GASTROINTESTINAL.ages;
      $scope.usf=POC.GASTROINTESTINAL.usf;
      $scope.occ=POC.GASTROINTESTINAL.occ;
      $scope.occl=POC.GASTROINTESTINAL.occl;
      $scope.noq=POC.GASTROINTESTINAL.noq;
      $scope.mkd=POC.GASTROINTESTINAL.mkd;
      $scope.abd=POC.GASTROINTESTINAL.abd;
      $scope.paij=POC.GASTROINTESTINAL.paij;
      $scope.gik=POC.GASTROINTESTINAL.gik;
      $scope.siu=POC.GASTROINTESTINAL.siu;
      $scope.tio=POC.GASTROINTESTINAL.tio;
      $scope.fucl=POC.GASTROINTESTINAL.fucl;
      $scope.wli=POC.GASTROINTESTINAL.wli;
      $scope.tur=POC.GASTROINTESTINAL.tur;
      $scope.yla=POC.GASTROINTESTINAL.yla;
      $scope.tho=POC.GASTROINTESTINAL.tho;
      $scope.ithe=POC.GASTROINTESTINAL.ithe;
      $scope.mon=POC.GASTROINTESTINAL.mon;
      $scope.vem=POC.GASTROINTESTINAL.vem;
      $scope.ayd=POC.GASTROINTESTINAL.ayd;
      $scope.prd=POC.GASTROINTESTINAL.prd;
      $scope.mini=POC.GASTROINTESTINAL.mini;
      $scope.icl=POC.GASTROINTESTINAL.icl;
      $scope.inl=POC.GASTROINTESTINAL.inl;
      $scope.ipa=POC.GASTROINTESTINAL.ipa;
      $scope.tra=POC.GASTROINTESTINAL.tra;
      $scope.sel=POC.DISCHARGE.sel;
      $scope.givs=POC.DISCHARGE.givs;
      $scope.mesa=POC.DISCHARGE.mesa;
      $scope.sef=POC.DISCHARGE.sef;
      $scope.zop=POC.DISCHARGE.zop;
      $scope.zoy=POC.DISCHARGE.zoy;

      //build ORDERS
      var ordersObj={
        md: {
          'md_sbp': $scope.sbp,
          'md_lt': $scope.lt,
          'md_dbp': $scope.dbp,
          'md_olt': $scope.olt
        },
        pgt: {
          'pgt_gtr': $scope.gtr,
          'pgt_les': $scope.les,
        },
        rgt: {
          'rgt_grt': $scope.grt,
          'rgt_ls': $scope.ls,
        },
        tem: {
          'tem_tgt': $scope.tgt,
        },
        eig: {
          'eig_ggt': $scope.ggt,
          'eig_lbsi': $scope.lbsi,
        },
        bsf: {
          'bsf_mbs': $scope.mbs,
          'bsf_bsg': $scope.bsg,
          'bsf_orlt': $scope.orlt,
        },
        ypp: {
          'ypp_tbp': $scope.tbp,
          'ypp_pls': $scope.pls,
          'ypp_ght': $scope.ght,
        },
        sn: {
          'sn_wwm': $scope.wwm,
          'sn_ians': $scope.ians,
          'sn_oxy': $scope.oxy,
          'sn_svn': $scope.svn,
          'sn_wcs': $scope.wcs,
          'sn_tsf': $scope.tsf,
          'sn_eth': $scope.eth,
          'sn_hha': $scope.hha,
        },
        pcw: {
          'pcw_mbp': $scope.mbp,
          'pcw_plse': $scope.plse,
          'pcw_tsn': $scope.tsn,
        },
        flw: {
          'flw_fln': $scope.fln,
        },
        adm: {
          'adm_ini': $scope.ini,
          'adm_medi': $scope.medi,
          'adm_cco': $scope.cco,
        },
        flusw: {
          'flusw_linew': $scope.linew,
          'flusw_foll': $scope.foll,
          'flusw_hep': $scope.hep
        },
        cpic: {
          'cpic_cht': $scope.cht
        },
        dpt: {
          'dpt_drq': $scope.drq,
          'dpt_fry': $scope.fry,
        },
        ipc: {
          'ipc_inc': $scope.ipc,
        },
        infec: {
          'infec_was': $scope.was,
          'infec_mat': $scope. mat,
          'infec_app': $scope.app,
        },
        othr: {
          'othr_otr': $scope.otr,
        },
        tpi: {
          'tpi_temo': $scope.temo,
        },
        tpcr: {
          'tpcr_equ': $scope.equ,
        },
        othh: {
          'othh_otre': $scope.otre
        },
        willm: {
          'willm_medic': $scope.medic,
        },
        otreh: {
          'otreh_otrh': $scope.otrh
        },
        tor: {
          'tor_tore': $scope.tore
        },
        tpp: {
          'tpp_cont': $scope.cont,
          'tpp_bet': $scope.bet,
          'tpp_wit': $scope.wit,
        },
        pati: {
          'pati_witn': $scope.witn,
        },
        careg: {
          'careg_freq': $scope.freq,
          'careg_pai': $scope.pai,
        },
        twil: {
          'twil_str': $scope.str,
        },
        wo: {
          'wo_wou': $scope.wou,
          'wo_cle': $scope.cle,
          'wo_dr': $scope.dr,
          'wo_wth': $scope.wth,
          'wo_cov': $scope.cov,
          'wo_sec': $scope.sec,
        },
        wu: {
          'wu_won': $scope.won,
          'wu_cla': $scope.cla,
          'wu_patd': $scope.patd,
          'wu_pac': $scope.pac,
          'wu_cove': $scope.cove,
          'wu_secu': $scope.secu,
        },
        wn: {
          'wn_wod': $scope.wod,
          'wn_cln': $scope.cln,
          'wn_yap': $scope.yap,
          'wn_kw': $scope.kw,
          'wn_ver': $scope.ver,
          'wn_secr': $scope.secr,
        },
        wd: {
          'wd_wun': $scope.wun,
          'wd_cls': $scope.cls,
          'wd_ylp': $scope.ylp,
          'wd_ack': $scope.ack,
          'wd_ith': $scope.ith,
          'wd_ser': $scope.ser,
        },
        noti: {
          'noti_basl': $scope.basl,
        },
        reg: {
          'reg_uct': $scope.uct,
        },
        dec: {
          'dec_siz': $scope.size,
          'dec_cmw': $scope.cmw,
        },
        comp: {
          'comp_heal': $scope.heal,
        },
        fr: {
          'fr_bsl': $scope.bsl,
        },
        ovr: {
          'ovr_verb': $scope.verb
        },
        verw: {
          'verw_wthi': $scope.wthi
        },
        dem: {
          'dem_abl': $scope.abl
        },
        //MUSCULOSKELETAL
        instr: {
          'instr_sat': $scope.sat,
        },
        pasn: {
          'pasn_uso': $scope.uso,
        },
        gac: {
          'gac_ety': $scope.ety,
        },
        ble: {
          'ble_thin': $scope.thin,
        },
        balo: {
          'balo_mdw': $scope.mdw,
        },
        stra: {
          'stra_vic': $scope.vic
        },
        cto: {
          'cto_fet': $scope.fet
        },
        //RENAL
        mess: {
          'mess_eat': $scope.eat,
          'mess_cted': $scope.cted,
        },
        snwl: {
          'snwl_raig': $scope.raig,
          'snwl_het': $scope.het,
        },
        fole:{
          'fole_folc': $scope.folc,
          'fole_ench': $scope.ench,
          'fole_loon': $scope.loon,
        },
        patie: {
          'patie_giv': $scope.giv,
          'patie_lmo': $scope.lmo,
          'patie_very': $scope.very,
        },
        regis: {
          'regis_let': $scope.let,
        },
        ual: {
          'ual_ify': $scope.ify,
        },
        equa: {
          'equa_hyd': $scope.hyd,
          'equa_sxi': $scope.sxi,
        },
        ien: {
          'ien_ski': $scope.sky,
        },
        ete: {
          'ete_bagg': $scope.bagg,
        },
        ets: {
          'ets_truc': $scope.truc,
        },
        uti: {
          'uti_lipl': $scope.lipl,
        },
        rit: {
          'rit_odo': $scope.odo
        },
        patc: {
          'patc_now': $scope.now,
        },
        pcg: {
          'pcg_bali': $scope.bali,
          'pcg_fetc': $scope.fetc,
        },
        mai: {
          'mai_fec': $scope.fec,
        },
        tyr: {
          'tyr_forc': $scope.forc,
        },
        //CARDIOVASCULAR
        gic: {
          'gic_gard': $scope.gard,
        },
        era: {
          'era_edm': $scope.edm,
        },
        cha: {
          'cha_chn': $scope.chn,
        },
        fib: {
          'fib_ail': $scope.ail
        },
        mdo: {
          'mdo_ole': $scope.ole
        },
        equs: {
          'equs_clu': $scope.clu
        },
        tpa: {
          'tpa_dosg': $scope.dosg,
          'tpa_medc': $scope.medc,
        },
        verp: {
          'verp_imi': $scope.imi
        },
        chp: {
          'chp_afe': $scope.afe,
        },
        patp: {
          'patp_patws': $scope.patws
        },
        pab:{
          'pab_patw': $scope.patw
        },
        demd: {
          'demd_resw': $scope.resw
        },
        alig: {
          'alig_adh': $scope.adh,
          'alig_thi': $scope.thi,
        },
        trp: {
          'trp_demo': $scope.demo
        },
        //NEUROLOGICAL
        hgr: {
          'hgr_gris': $scope.gris,
          'hgr_pup': $scope.pup,
          'hgr_wea': $scope.wea,
          'hgr_iou': $scope.iou,
          'hgr_hal': $scope.hal,
          'hgr_dirc': $scope.dirc,
        },
        cgp: {
          'cgp_ffe': $scope.ffe,
        },
        mana: {
          'mana_age': $scope.age,
          'mana_req': $scope.req
        },
        relt: {
          'relt_ons': $scope.ons
        },
        icat: {
          'icat_ict': $scope.ict
        },
        carg: {
          'carg_bals': $scope.bals,
          'carg_sei': $scope.sei
        },
        spa: {
          'spa_con': $scope.con,
          'spa_frqs': $scope.frqs
        },
        emo: {
          'emo_qua': $scope.qua,
          'emo_ica': $scope.ica
        },
        onsa: {
          'onsa_kdo': $scope.kdo
        },
        rba: {
          'rba_mdwa': $scope.mdwa
        },
        neu: {
          'neu_sxs': $scope.sxs
        },
        //ENDOCRINE
        chg: {
          'chg_gar': $scope.gar,
        },
        repl: {
          'repl_hta': $scope.hta,
          'repl_sst': $scope.sst
        },
        tsr: {
          'tsr_rot': $scope.rot
        },
        sug: {
          'sug_lan': $scope.lan
        },
        cgl: {
          'cgl_wpa': $scope.wpa
        },
        wpa: {
          'wpa_alis': $scope.ali,
          'wpa_perg': $scope.perg
        },
        det: {
          'det_wee': $scope.wee
        },
        ont: {
          'ont_com': $scope.com,
          'ont_suw': $scope.suw,
          'ont_ents': $scope.ents
        },
        dis: {
          'dis_bes': $scope.bis,
          'dis_ysi': $scope.ysi,
        },
        clus: {
          'clus_int': $scope.int
        },
        //RESPIRATORY
        nst: {
          'nst_res': $scope.res,
        },
        ruc: {
          'ruc_brs': $scope.brs,
          'ruc_est': $scope.est,
          'ruc_patl': $scope.patl,
          'ruc_trm': $scope.trm,
          'ruc_yel': $scope.yel,
        },
        hec: {
          'hec_mok': $scope.mok,
          'hec_fla': $scope.fla
        },
        cti: {
          'cti_redg': $scope.redg,
        },
        quf: {
          'quf_ids': $scope.ids
        },
        cta: {
          'cta_bal': $scope.bal,
          'cta_mdwh': $scope.mdwh
        },
        ems: {
          'ems_qui': $scope.qui
        },
        rats: {
          'rats_ratsw': $scope.ratsw
        },
        liz: {
          'liz_rwp': $scope.rwp
        },
        egi: {
          'egi_aliz': $scope.aliz
        },
        //GASTROINTESTINAL
        adp: {
          'adp_ccz': $scope.ccz
        },
        nau: {
          'nau_pym': $scope.pym,
          'nau_vom': $scope.vom,
          'nau_disa': $scope.disa,
          'nau_thw': $scope.thw,
        },
        prl: {
          'prl_rev': $scope.rev,
          'prl_olz': $scope.olz,
          'prl_ibr': $scope.ibr,
          'prl_buu': $scope.buu,
          'prl_ages': $scope.ages,
          'prl_usf': $scope.usf,
        },
        occ: {
          'occ_occl': $scope.occl,
          'occ_noq': $scope.noq
        },
        mkd: {
          'mkd_abd': $scope.abd
        },
        paij: {
          'paij_gik': $scope.gik
        },
        tio: {
          'tio_fucl': $scope.fucl
        },
        wli: {
          'wli_tur': $scope.tur,
          'wli_yla': $scope.yla,
          'wli_tho': $scope.tho,
          'wli_ithe': $scope.ithe,
        },
        mon:{
          'mon_vem': $scope.vem,
          'mon_ayd': $scope.ayd
        },
        prd: {
          'prd_mini': $scope.mini
        },
        icl: {
          'icl_inl': $scope.inl
        },
        ipa: {
          'ipa_tra': $scope.tra
        },
      };

      //get all the keys in ordersObj
      var keys = Object.keys(ordersObj);
      for (var i = 0; i < keys.length; i++) {
        var key=keys[i];
        //get all the fields
        var fields=ordersObj[key];
        var fieldIDs = Object.keys(fields);

        if ($scope[key]!== null && $scope[key]!== '') {
          var input=$scope[key]
          var output=[];
          for (var j = 0; j < fieldIDs.length; j++) {
            // console.log(input)
            // console.log(fieldIDs[j]);
            var n = input.indexOf(fieldIDs[j]);

            var res = input.slice(0,n);
            output.push(res);
            input=input.replace(res, '');

            var fieldLength=fieldIDs[j].length;
            var fieldID=input.slice(0, fieldLength);
            output.push(fieldID);

            input=input.replace(fieldID, '');

          }
          output.push(input);

          for (var k = 0; k < fieldIDs.length; k++) {
            var index=output.indexOf(fieldIDs[k]);
            output[index]=fields[fieldIDs[k]];
          }

          var newKey=output.join(' ');

          // console.log(newKey);

          $scope[key]=newKey;
        }
      }
    });
  };

    // $http({
    //   url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['planOfCare'].get,
    //   method: 'GET',
    //   data: $httpParamSerializerJQLike(newPOC),
    //   headers: {
    //     'Content-Type': 'application/x-www-form-urlencoded'
    //   }
    // }).then(function(response){
    //   console.log(response)
    // }, function(data) {
    //   console.log(data)
    // });

    // $scope.POC={
    //     // FORMID: $scope.form.FORMID,
    //     SUPPLIES: {
    //       Supplies: 'Lab supplies',
    //       // Trays: 'Trays'
    //     }
    // };
    // console.log('$scope.POC', $scope.POC);
  // };

  //qa approves form
  $scope.approve=function () {
    //edit form status to 3=approved;
    dataService.edit('form', {KEYID: $scope.form.FORMID, STATUS:3}).then(function (response) {
      console.log('editing form', response);
      $state.go('qa');
    });
  };

  //qa rejects form
  $scope.reject=function () {
    //change form status to in progress, and attach note?
    var form={
      KEYID: $scope.form.FORMID,
      STATUS: 4,
    };
    dataService.edit('form', form).then(function (response) {
      console.log('editing form', response);
      $state.go('qa');
    });
  };

  $scope.getPOC();
})
