app.config(function($stateProvider) {
  $stateProvider.state('viewPlanOfCare', {
    url: '/viewPlanOfCare',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/viewPlanOfCare/viewPlanOfCare.html',
        controller: 'ViewPlanOfCareController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
