app.config(function($stateProvider) {
  $stateProvider.state('planOfCare', {
    url: '/planOfCare',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/planOfCare/planOfCare.html',
        controller: 'PlanOfCareController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
