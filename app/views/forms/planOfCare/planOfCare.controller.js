app.controller('PlanOfCareController', function($scope, $state, $http, CONSTANTS, $httpParamSerializerJQLike, Blob, FileSaver, formService, dataService, patientService) {

  $scope.form = formService.get();
  console.log('$scope.form', $scope.form);

  $scope.patient=patientService.get();
  console.log('$scope.patient', $scope.patient);

  $scope.PREVIOUS = function() {
    window.history.back();
  };

  //check to see if oasis has been completed/exists for visit
  $scope.isOasisComplete=function () {
    dataService.get('form', {VISITID: $scope.form.VISITID}).then(function (data) {
      var allVisitForms=data.data;

      var index=allVisitForms.findIndex(c => (c.FORMTYPE === 2) || (c.FORMTYPE === 3) || (c.FORMTYPE === 4))
      if (index !== -1) {
        if (allVisitForms[index].STATUS===3) {
          $scope.noOasis=false;
          console.log('oasis form for the visit has been completed');
        } else {
          console.log('oasis form for the visit has not been completed');
           $scope.noOasis=true;
        }
      } else {
          console.log('oasis form was not added to visit');
          $scope.noOasis=true;
      }
    });
  };

  //get POC
  $scope.getPOC=function () {
    if ($scope.form.OBJKEYID!==null) { //if objectkeyid exists,
      console.log('continuing exisiting poc');
      //if there is a POC, populate form using POC data
      dataService.get('planOfCare', {KEYID:$scope.form.OBJKEYID}).then(function (data) {
        console.log(data.data);
        var POC=data.data;
        $scope.Supplies=POC.SUPPLIES.Supplies;
        $scope.Trays=POC.SUPPLIES.Trays;
        $scope.Irrigation=POC.SUPPLIES.Irrigation;
        $scope.Gloves=POC.SUPPLIES.Gloves;
        $scope.Walker=POC.SUPPLIES.Walker;
        $scope.Commode=POC.SUPPLIES.Commode;
        $scope.Tape=POC.SUPPLIES.Tape;
        $scope.Syringes=POC.SUPPLIES.Syringes;
        $scope.Dressings=POC.SUPPLIES.Dressings;
        $scope.WC=POC.SUPPLIES.WC;
        $scope.NS=POC.SUPPLIES.NS;
        $scope.Ostomy=POC.SUPPLIES.Ostomy;
        $scope.O2=POC.SUPPLIES.O2;
        $scope.Cane=POC.SUPPLIES.Cane;
        $scope.Hoyerlift=POC.SUPPLIES.Hoyerlift;
        $scope.Glucometer=POC.SUPPLIES.Glucometer;
        $scope.Sharps=POC.SUPPLIES.Sharps;
        $scope.Shower=POC.SUPPLIES.Shower;
        $scope.Tube=POC.SUPPLIES.Tube;
        $scope.Pump=POC.SUPPLIES.Pump;
        $scope.Hospital=POC.SUPPLIES.Hospital;
        $scope.Other1=POC.SUPPLIES.Other1;
        $scope.Seizure=POC.SAFETY.Seizure;
        $scope.Wheelchair=POC.SAFETY.Wheelchair;
        $scope.Precautions=POC.SAFETY.Precautions;
        $scope.Anticoagulant=POC.SAFETY.Anticoagulant;
        $scope.Hoyer=POC.SAFETY.Hoyer;
        $scope.Lifeline=POC.SAFETY.Lifeline;
        $scope.Disposal=POC.SAFETY.Disposal;
        $scope.Fall=POC.SAFETY.Fall;
        $scope.Infection=POC.SAFETY.Infection;
        $scope.SMA=POC.SAFETY.SMA;
        $scope.Scatter=POC.SAFETY.Scatter;
        $scope.Pathways=POC.SAFETY.Pathways;
        $scope.TWA=POC.SAFETY.TWA;
        $scope.Railsup=POC.SAFETY.Railsup;
        $scope.rail1=POC.SAFETY.rail1;
        $scope.rail2=POC.SAFETY.rail2;
        $scope.pds=POC.SAFETY.pds;
        $scope.Emergency=POC.SAFETY.Emergency;
        $scope.Other2=POC.SAFETY.Other2;
        $scope.Diet=POC.NUTRITIONAL.Diet;
        $scope.Fluid=POC.NUTRITIONAL.Fluid;
        $scope.gts=POC.NUTRITIONAL.gts;
        $scope.ff=POC.NUTRITIONAL.ff;
        $scope.Medication=POC.NUTRITIONAL.Medication;
        $scope.Food=POC.NUTRITIONAL.Food;
        $scope.Amputation=POC.FUNCTIONAL.Amputation;
        $scope.Bowel=POC.FUNCTIONAL.Bowel;
        $scope.Contracture=POC.FUNCTIONAL.Contracture;
        $scope.Hearing=POC.FUNCTIONAL.Hearing;
        $scope.Paralysis=POC.FUNCTIONAL.Paralysis;
        $scope.Endurance=POC.FUNCTIONAL.Endurance;
        $scope.Ambulation=POC.FUNCTIONAL.Ambulation;
        $scope.Speech=POC.FUNCTIONAL.Speech;
        $scope.Dyspnea=POC.FUNCTIONAL.Dyspnea;
        $scope.Blind=POC.FUNCTIONAL.Blind;
        $scope.Otherfun=POC.FUNCTIONAL.Otherfun;
        $scope.Bedrest=POC.ACTIVITIES.Bedrest;
        $scope.brp=POC.ACTIVITIES.brp;
        $scope.uat=POC.ACTIVITIES.uat;
        $scope.bt=POC.ACTIVITIES.bt;
        $scope.ep=POC.ACTIVITIES.ep;
        $scope.pwb=POC.ACTIVITIES.pwb;
        $scope.iah=POC.ACTIVITIES.iah;
        $scope.Actwalker=POC.ACTIVITIES.Actwalker;
        $scope.Crutches=POC.ACTIVITIES.Crutches;
        $scope.Caneact=POC.ACTIVITIES.Caneact;
        $scope.act_wc=POC.ACTIVITIES.act_wc;
        $scope.restrictions=POC.ACTIVITIES.restrictions;
        $scope.Other3=POC.ACTIVITIES.Other3;
        $scope.Oriented=POC.MENTAL.Oriented;
        $scope.Comatose=POC.MENTAL.Comatose;
        $scope.Forgetful=POC.MENTAL.Forgetful;
        $scope.Depressed=POC.MENTAL.Depressed;
        $scope.Disoriented=POC.MENTAL.Disoriented;
        $scope.Agitated=POC.MENTAL.Agitated;
        $scope.Other4=POC.MENTAL.Other4;
        $scope.Prognosis=POC.ORDERS.race;
        $scope.rehab=POC.ORDERS.rehab;
        $scope.mdsn=POC.ORDERS.mdsn;
        $scope.txtpatientzipcode=POC.ORDERS.txtpatientzipcode;
        $scope.ept=POC.ORDERS.ept;
        $scope.ote=POC.ORDERS.ote;
        $scope.ste=POC.ORDERS.ste;
        $scope.msw=POC.ORDERS.msw;
        $scope.pns=POC.ORDERS.pns;
        $scope.psd=POC.ORDERS.psd;
        $scope.md=POC.SKILLED.md;
        $scope.sbp=POC.SKILLED.sbp;
        $scope.lt=POC.SKILLED.lt;
        $scope.dbp=POC.SKILLED.dbp;
        $scope.olt=POC.SKILLED.olt;
        $scope.pgt=POC.SKILLED.pgt;
        $scope.gtr=POC.SKILLED.gtr;
        $scope.lrs=POC.SKILLED.lrs;
        $scope.rgt=POC.SKILLED.rgt;
        $scope.grt=POC.SKILLED.grt;
        $scope.ls=POC.SKILLED.ls;
        $scope.tem=POC.SKILLED.tem;
        $scope.tgt=POC.SKILLED.tgt;
        $scope.eig=POC.SKILLED.eig;
        $scope.ggt=POC.SKILLED.ggt;
        $scope.lbsi=POC.SKILLED.lbsi;
        $scope.bsf=POC.SKILLED.bsf;
        $scope.mbs=POC.SKILLED.mbs;
        $scope.bsg=POC.SKILLED.bsg;
        $scope.orlt=POC.SKILLED.orlt;
        $scope.ypp=POC.SKILLED.ypp;
        $scope.tbp=POC.SKILLED.tbp;
        $scope.pls=POC.SKILLED.pls;
        $scope.ght=POC.SKILLED.ght;
        $scope.sn=POC.SKILLED.sn;
        $scope.ians=POC.SKILLED.ians;
        $scope.oxy=POC.SKILLED.oxy;
        $scope.svn=POC.SKILLED.svn;
        $scope.wcs=POC.SKILLED.wcs;
        $scope.tsf=POC.SKILLED.tsf;
        $scope.eth=POC.SKILLED.eth;
        $scope.hha=POC.SKILLED.hha;
        $scope.vs=POC.SKILLED.vs;
        $scope.tpc=POC.SKILLED.tpc;
        $scope.pcw=POC.SKILLED.pcw;
        $scope.mbp=POC.SKILLED.mbp;
        $scope.plse=POC.SKILLED.plse;
        $scope.tsn=POC.SKILLED.tsn;
        $scope.pc=POC.SKILLED.pc;
        $scope.sectionSkilled = POC.SKILLED.isApplicable;
        $scope.ven=POC.LAB.ven;
        $scope.idt=POC.LAB.idt;
        $scope.ntt=POC.LAB.ntt;
        $scope.sectionLab = POC.LAB.isApplicable;
        $scope.snw=POC.IV.snw;
        $scope.dos=POC.IV.dos;
        $scope.rtm=POC.IV.rtm;
        $scope.hickman=POC.IV.hickman;
        $scope.ggc=POC.IV.ggc;
        $scope.picc=POC.IV.picc;
        $scope.perl=POC.IV.perl;
        $scope.ptc=POC.IV.ptc;
        $scope.frq=POC.IV.frq;
        $scope.lot=POC.IV.lot;
        $scope.alf=POC.IV.alf;
        $scope.flw=POC.IV.flw;
        $scope.fln=POC.IV.fln;
        $scope.adm=POC.IV.adm;
        $scope.ini=POC.IV.ini;
        $scope.medi=POC.IV.medi;
        $scope.cco=POC.IV.cco;
        $scope.flusw=POC.IV.flusw;
        $scope.linew=POC.IV.linew;
        $scope.foll=POC.IV.foll;
        $scope.hep=POC.IV.hep;
        $scope.cpic=POC.IV.cpic;
        $scope.cht=POC.IV.cht;
        $scope.dpt=POC.IV.dpt;
        $scope.drq=POC.IV.drq;
        $scope.fry=POC.IV.fry;
        $scope.ipc=POC.IV.ipc;
        $scope.inc=POC.IV.inc;
        $scope.piu=POC.IV.piu;
        $scope.infec=POC.IV.infec;
        $scope.was=POC.IV.was;
        $scope.mat=POC.IV.mat;
        $scope.app=POC.IV.app;
        $scope.othr=POC.IV.othr;
        $scope.otr=POC.IV.otr;
        $scope.tpi=POC.IV.tpi;
        $scope.temo=POC.IV.temo;
        $scope.tpcr=POC.IV.tpcr;
        $scope.equ=POC.IV.equ;
        $scope.othh=POC.IV.othh;
        $scope.otre=POC.IV.otre;
        $scope.sectionIV = POC.IV.isApplicable;
        $scope.tsnw=POC.PAIN.tsnw;
        $scope.willm=POC.PAIN.willm;
        $scope.medic=POC.PAIN.medic;
        $scope.snin=POC.PAIN.snin;
        $scope.inth=POC.PAIN.inth;
        $scope.inst=POC.PAIN.inst;
        $scope.otreh=POC.PAIN.otreh;
        $scope.otrh=POC.PAIN.otrh;
        $scope.moni=POC.PAIN.moni;
        $scope.tor=POC.PAIN.tor;
        $scope.tore=POC.PAIN.tore;
        $scope.tpp=POC.PAIN.tpp;
        $scope.cont=POC.PAIN.cont;
        $scope.bet=POC.PAIN.bet;
        $scope.wit=POC.PAIN.wit;
        $scope.pati=POC.PAIN.pati;
        $scope.witn=POC.PAIN.witn;
        $scope.careg=POC.PAIN.careg;
        $scope.freq=POC.PAIN.freq;
        $scope.pai=POC.PAIN.pai;
        $scope.twil=POC.PAIN.twil;
        $scope.str=POC.PAIN.str;
        $scope.wo=POC.PAIN.wo;
        $scope.wou=POC.PAIN.wou;
        $scope.cle=POC.PAIN.cle;
        $scope.dr=POC.PAIN.dr;
        $scope.wth=POC.PAIN.wth;
        $scope.cov=POC.PAIN.cov;
        $scope.sec=POC.PAIN.sec;
        $scope.wu=POC.PAIN.wu;
        $scope.won=POC.PAIN.won;
        $scope.cla=POC.PAIN.cla;
        $scope.patd=POC.PAIN.patd;
        $scope.pac=POC.PAIN.pac;
        $scope.cove=POC.PAIN.cove;
        $scope.secu=POC.PAIN.secu;
        $scope.wn=POC.PAIN.wn;
        $scope.wod=POC.PAIN.wod;
        $scope.cln=POC.PAIN.cln;
        $scope.yap=POC.PAIN.yap;
        $scope.kw=POC.PAIN.kw;
        $scope.ver=POC.PAIN.ver;
        $scope.secr=POC.PAIN.secr;
        $scope.wd=POC.PAIN.wd;
        $scope.wun=POC.PAIN.wun;
        $scope.cls=POC.PAIN.cls;
        $scope.ylp=POC.PAIN.ylp;
        $scope.ack=POC.PAIN.ack;
        $scope.ith=POC.PAIN.ith;
        $scope.ser=POC.PAIN.ser;
        $scope.mea=POC.PAIN.mea;
        $scope.ase=POC.PAIN.ase;
        $scope.noti=POC.PAIN.noti;
        $scope.basl=POC.PAIN.basl;
        $scope.tru=POC.PAIN.tru;
        $scope.ent=POC.PAIN.ent;
        $scope.reg=POC.PAIN.reg;
        $scope.uct=POC.PAIN.uct;
        $scope.dec=POC.PAIN.dec;
        $scope.siz=POC.PAIN.siz;
        $scope.cmw=POC.PAIN.cmw;
        $scope.comp=POC.PAIN.comp;
        $scope.heal=POC.PAIN.heal;
        $scope.fr=POC.PAIN.fr;
        $scope.bsl=POC.PAIN.bsl;
        $scope.ovr=POC.PAIN.ovr;
        $scope.verb=POC.PAIN.verb;
        $scope.verw=POC.PAIN.verw;
        $scope.wthi=POC.PAIN.wthi;
        $scope.dem=POC.PAIN.dem;
        $scope.abl=POC.PAIN.abl;
        $scope.sectionWound = POC.PAIN.isApplicableW;
        $scope.sectionPain = POC.PAIN.isApplicableP;
        $scope.rela=POC.MUSCULOSKELETAL.rela;
        $scope.instr=POC.MUSCULOSKELETAL.instr;
        $scope.sat=POC.MUSCULOSKELETAL.sat;
        $scope.Asses=POC.MUSCULOSKELETAL.Asses;
        $scope.pasn=POC.MUSCULOSKELETAL.pasn;
        $scope.uso=POC.MUSCULOSKELETAL.uso;
        $scope.car=POC.MUSCULOSKELETAL.car;
        $scope.gac=POC.MUSCULOSKELETAL.gac;
        $scope.ety=POC.MUSCULOSKELETAL.ety;
        $scope.ble=POC.MUSCULOSKELETAL.ble;
        $scope.thin=POC.MUSCULOSKELETAL.thin;
        $scope.balo=POC.MUSCULOSKELETAL.balo;
        $scope.mdw=POC.MUSCULOSKELETAL.mdw;
        $scope.stra=POC.MUSCULOSKELETAL.stra;
        $scope.vic=POC.MUSCULOSKELETAL.vic;
        $scope.cto=POC.MUSCULOSKELETAL.cto;
        $scope.fet=POC.MUSCULOSKELETAL.fet;
        $scope.sectionMuscul = POC.MUSCULOSKELETAL.isApplicable;
        $scope.atr=POC.RENAL.atr;
        $scope.mess=POC.RENAL.mess;
        $scope.eat=POC.RENAL.eat;
        $scope.cted=POC.RENAL.cted;
        $scope.snwl=POC.RENAL.snwl;
        $scope.raig=POC.RENAL.raig;
        $scope.het=POC.RENAL.het;
        $scope.fole=POC.RENAL.fole;
        $scope.folc=POC.RENAL.folc;
        $scope.ench=POC.RENAL.ench;
        $scope.loon=POC.RENAL.loon;
        $scope.patie=POC.RENAL.patie;
        $scope.giv=POC.RENAL.giv;
        $scope.lmo=POC.RENAL.lmo;
        $scope.very=POC.RENAL.very;
        $scope.regis=POC.RENAL.regis;
        $scope.let=POC.RENAL.let;
        $scope.ual=POC.RENAL.ual;
        $scope.ify=POC.RENAL.ify;
        $scope.equa=POC.RENAL.equa;
        $scope.hyd=POC.RENAL.hyd;
        $scope.sxi=POC.RENAL.sxi;
        $scope.ien=POC.RENAL.ien;
        $scope.ski=POC.RENAL.ski;
        $scope.ete=POC.RENAL.ete;
        $scope.bagg=POC.RENAL.bagg;
        $scope.ets=POC.RENAL.ets;
        $scope.truc=POC.RENAL.truc;
        $scope.uti=POC.RENAL.uti;
        $scope.lipl=POC.RENAL.lipl;
        $scope.rit=POC.RENAL.rit;
        $scope.odo=POC.RENAL.odo;
        $scope.patc=POC.RENAL.patc;
        $scope.now=POC.RENAL.now;
        $scope.pcg=POC.RENAL.pcg;
        $scope.bali=POC.RENAL.bali;
        $scope.fetc=POC.RENAL.fetc;
        $scope.fem=POC.RENAL.fem;
        $scope.mai=POC.RENAL.mai;
        $scope.fec=POC.RENAL.fec;
        $scope.tyr=POC.RENAL.tyr;
        $scope.forc=POC.RENAL.forc;
        $scope.sectionRenal = POC.RENAL.isApplicable;
        $scope.ated=POC.CARDIOVASCULAR.ated;
        $scope.ess=POC.CARDIOVASCULAR.ess;
        $scope.pats=POC.CARDIOVASCULAR.pats;
        $scope.gic=POC.CARDIOVASCULAR.gic;
        $scope.gard=POC.CARDIOVASCULAR.gard;
        $scope.era=POC.CARDIOVASCULAR.era;
        $scope.edm=POC.CARDIOVASCULAR.edm;
        $scope.cha=POC.CARDIOVASCULAR.cha;
        $scope.chn=POC.CARDIOVASCULAR.chn;
        $scope.fib=POC.CARDIOVASCULAR.fib;
        $scope.ail=POC.CARDIOVASCULAR.ail;
        $scope.mdo=POC.CARDIOVASCULAR.mdo;
        $scope.ole=POC.CARDIOVASCULAR.ole;
        $scope.dix=POC.CARDIOVASCULAR.dix;
        $scope.pos=POC.CARDIOVASCULAR.pos;
        $scope.itr=POC.CARDIOVASCULAR.itr;
        $scope.mpl=POC.CARDIOVASCULAR.mpl;
        $scope.ars=POC.CARDIOVASCULAR.ars;
        $scope.equs=POC.CARDIOVASCULAR.equs;
        $scope.clu=POC.CARDIOVASCULAR.clu;
        $scope.tpa=POC.CARDIOVASCULAR.tpa;
        $scope.dosg=POC.CARDIOVASCULAR.dosg;
        $scope.medc=POC.CARDIOVASCULAR.medc;
        $scope.verp=POC.CARDIOVASCULAR.verp;
        $scope.imi=POC.CARDIOVASCULAR.imi;
        $scope.chp=POC.CARDIOVASCULAR.chp;
        $scope.afe=POC.CARDIOVASCULAR.afe;
        $scope.patp=POC.CARDIOVASCULAR.patp;
        $scope.patws=POC.CARDIOVASCULAR.patws;
        $scope.bls=POC.CARDIOVASCULAR.bls;
        $scope.pab=POC.CARDIOVASCULAR.pab;
        $scope.patw=POC.CARDIOVASCULAR.patw;
        $scope.demd=POC.CARDIOVASCULAR.demd;
        $scope.resw=POC.CARDIOVASCULAR.resw;
        $scope.alig=POC.CARDIOVASCULAR.alig;
        $scope.adh=POC.CARDIOVASCULAR.adh;
        $scope.thi=POC.CARDIOVASCULAR.thi;
        $scope.trp=POC.CARDIOVASCULAR.trp;
        $scope.demo=POC.CARDIOVASCULAR.demo;
        $scope.sectionCardio = POC.CARDIOVASCULAR.isApplicable;
        $scope.ida=POC.NEUROLOGICAL.ida;
        $scope.hgr=POC.NEUROLOGICAL.hgr;
        $scope.gris=POC.NEUROLOGICAL.gris;
        $scope.pup=POC.NEUROLOGICAL.pup;
        $scope.wea=POC.NEUROLOGICAL.wea;
        $scope.iou=POC.NEUROLOGICAL.iou;
        $scope.hal=POC.NEUROLOGICAL.hal;
        $scope.dirc=POC.NEUROLOGICAL.dirc;
        $scope.patg=POC.NEUROLOGICAL.patg;
        $scope.cgp=POC.NEUROLOGICAL.cgp;
        $scope.ffe=POC.NEUROLOGICAL.ffe;
        $scope.regt=POC.NEUROLOGICAL.regt;
        $scope.regi=POC.NEUROLOGICAL.regi;
        $scope.ses=POC.NEUROLOGICAL.ses;
        $scope.mana=POC.NEUROLOGICAL.mana;
        $scope.age=POC.NEUROLOGICAL.age;
        $scope.req=POC.NEUROLOGICAL.req;
        $scope.eff=POC.NEUROLOGICAL.eff;
        $scope.relt=POC.NEUROLOGICAL.relt;
        $scope.ons=POC.NEUROLOGICAL.ons;
        $scope.icat=POC.NEUROLOGICAL.icat;
        $scope.ict=POC.NEUROLOGICAL.ict;
        $scope.carg=POC.NEUROLOGICAL.carg;
        $scope.bals=POC.NEUROLOGICAL.bals;
        $scope.sei=POC.NEUROLOGICAL.sei;
        $scope.spa=POC.NEUROLOGICAL.spa;
        $scope.con=POC.NEUROLOGICAL.con;
        $scope.frqs=POC.NEUROLOGICAL.frqs;
        $scope.emo=POC.NEUROLOGICAL.emo;
        $scope.ica=POC.NEUROLOGICAL.ica;
        $scope.onsa=POC.NEUROLOGICAL.onsa;
        $scope.kdo=POC.NEUROLOGICAL.kdo;
        $scope.rba=POC.NEUROLOGICAL.rba;
        $scope.mdwa=POC.NEUROLOGICAL.mdwa;
        $scope.neu=POC.NEUROLOGICAL.neu;
        $scope.sxs=POC.NEUROLOGICAL.sxs;
        $scope.sectionNeuro = POC.NEUROLOGICAL.isApplicable;
        $scope.end=POC.ENDOCRINE.end;
        $scope.isn=POC.ENDOCRINE.isn;
        $scope.tin=POC.ENDOCRINE.tin;
        $scope.rat=POC.ENDOCRINE.rat;
        $scope.chg=POC.ENDOCRINE.chg;
        $scope.gar=POC.ENDOCRINE.gar;
        $scope.repl=POC.ENDOCRINE.repl;
        $scope.hta=POC.ENDOCRINE.hta;
        $scope.sst=POC.ENDOCRINE.sst;
        $scope.admi=POC.ENDOCRINE.admi;
        $scope.tsr=POC.ENDOCRINE.tsr;
        $scope.rot=POC.ENDOCRINE.rot;
        $scope.ind=POC.ENDOCRINE.ind;
        $scope.dia=POC.ENDOCRINE.dia;
        $scope.ret=POC.ENDOCRINE.ret;
        $scope.abg=POC.ENDOCRINE.abg;
        $scope.prs=POC.ENDOCRINE.prs;
        $scope.sug=POC.ENDOCRINE.sug;
        $scope.lan=POC.ENDOCRINE.lan;
        $scope.cgl=POC.ENDOCRINE.cgl;
        $scope.acc=POC.ENDOCRINE.acc;
        $scope.wpa=POC.ENDOCRINE.wpa;
        $scope.alis=POC.ENDOCRINE.alis;
        $scope.perg=POC.ENDOCRINE.perg;
        $scope.ves=POC.ENDOCRINE.ves;
        $scope.det=POC.ENDOCRINE.det;
        $scope.wee=POC.ENDOCRINE.wee;
        $scope.ont=POC.ENDOCRINE.ont;
        $scope.com=POC.ENDOCRINE.com;
        $scope.suw=POC.ENDOCRINE.suw;
        $scope.ents=POC.ENDOCRINE.ents;
        $scope.goos=POC.ENDOCRINE.goos;
        $scope.dis=POC.ENDOCRINE.dis;
        $scope.bes=POC.ENDOCRINE.bes;
        $scope.ysi=POC.ENDOCRINE.ysi;
        $scope.clus=POC.ENDOCRINE.clus;
        $scope.int=POC.ENDOCRINE.int;
        $scope.sectionEndo = POC.ENDOCRINE.isApplicable;
        $scope.ela=POC.RESPIRATORY.ela;
        $scope.nst=POC.RESPIRATORY.nst;
        $scope.res=POC.RESPIRATORY.res;
        $scope.ruc=POC.RESPIRATORY.ruc;
        $scope.brs=POC.RESPIRATORY.brs;
        $scope.est=POC.RESPIRATORY.est;
        $scope.patl=POC.RESPIRATORY.patl;
        $scope.trm=POC.RESPIRATORY.trm;
        $scope.yel=POC.RESPIRATORY.yel;
        $scope.hec=POC.RESPIRATORY.hec;
        $scope.mok=POC.RESPIRATORY.mok;
        $scope.fla=POC.RESPIRATORY.fla;
        $scope.merg=POC.RESPIRATORY.merg;
        $scope.ner=POC.RESPIRATORY.ner;
        $scope.oni=POC.RESPIRATORY.oni;
        $scope.cas=POC.RESPIRATORY.cas;
        $scope.cti=POC.RESPIRATORY.cti;
        $scope.redg=POC.RESPIRATORY.redg;
        $scope.quf=POC.RESPIRATORY.quf;
        $scope.ids=POC.RESPIRATORY.ids;
        $scope.cta=POC.RESPIRATORY.cta;
        $scope.bal=POC.RESPIRATORY.bal;
        $scope.mdwh=POC.RESPIRATORY.mdwh;
        $scope.ems=POC.RESPIRATORY.ems;
        $scope.qui=POC.RESPIRATORY.qui;
        $scope.rats=POC.RESPIRATORY.rats;
        $scope.ratsw=POC.RESPIRATORY.ratsw;
        $scope.dto=POC.RESPIRATORY.dto;
        $scope.liz=POC.RESPIRATORY.liz;
        $scope.rwp=POC.RESPIRATORY.rwp;
        $scope.eqi=POC.RESPIRATORY.eqi;
        $scope.aliz=POC.RESPIRATORY.aliz;
        $scope.sectionRes = POC.RESPIRATORY.isApplicable;
        $scope.adp=POC.GASTROINTESTINAL.adp;
        $scope.ccz=POC.GASTROINTESTINAL.ccz;
        $scope.nuk=POC.GASTROINTESTINAL.nuk;
        $scope.nau=POC.GASTROINTESTINAL.nau;
        $scope.pym=POC.GASTROINTESTINAL.pym;
        $scope.vom=POC.GASTROINTESTINAL.vom;
        $scope.disa=POC.GASTROINTESTINAL.disa;
        $scope.thw=POC.GASTROINTESTINAL.thw;
        $scope.prl=POC.GASTROINTESTINAL.prl;
        $scope.rev=POC.GASTROINTESTINAL.rev;
        $scope.olz=POC.GASTROINTESTINAL.olz;
        $scope.ibr=POC.GASTROINTESTINAL.ibr;
        $scope.buu=POC.GASTROINTESTINAL.buu;
        $scope.ages=POC.GASTROINTESTINAL.ages;
        $scope.usf=POC.GASTROINTESTINAL.usf;
        $scope.occ=POC.GASTROINTESTINAL.occ;
        $scope.occl=POC.GASTROINTESTINAL.occl;
        $scope.noq=POC.GASTROINTESTINAL.noq;
        $scope.mkd=POC.GASTROINTESTINAL.mkd;
        $scope.abd=POC.GASTROINTESTINAL.abd;
        $scope.paij=POC.GASTROINTESTINAL.paij;
        $scope.gik=POC.GASTROINTESTINAL.gik;
        $scope.siu=POC.GASTROINTESTINAL.siu;
        $scope.tio=POC.GASTROINTESTINAL.tio;
        $scope.fucl=POC.GASTROINTESTINAL.fucl;
        $scope.wli=POC.GASTROINTESTINAL.wli;
        $scope.tur=POC.GASTROINTESTINAL.tur;
        $scope.yla=POC.GASTROINTESTINAL.yla;
        $scope.tho=POC.GASTROINTESTINAL.tho;
        $scope.ithe=POC.GASTROINTESTINAL.ithe;
        $scope.mon=POC.GASTROINTESTINAL.mon;
        $scope.vem=POC.GASTROINTESTINAL.vem;
        $scope.ayd=POC.GASTROINTESTINAL.ayd;
        $scope.prd=POC.GASTROINTESTINAL.prd;
        $scope.mini=POC.GASTROINTESTINAL.mini;
        $scope.icl=POC.GASTROINTESTINAL.icl;
        $scope.inl=POC.GASTROINTESTINAL.inl;
        $scope.ipa=POC.GASTROINTESTINAL.ipa;
        $scope.tra=POC.GASTROINTESTINAL.tra;
        $scope.sectionGastro = POC.GASTROINTESTINAL.isApplicable;
        $scope.sel=POC.DISCHARGE.sel;
        $scope.givs=POC.DISCHARGE.givs;
        $scope.mesa=POC.DISCHARGE.mesa;
        $scope.sef=POC.DISCHARGE.sef;
        $scope.zop=POC.DISCHARGE.zop;
        $scope.zoy=POC.DISCHARGE.zoy;
      });
    } else {
      console.log('starting new poc');
    }

  };

  $scope.savePOC = function() {
    //check to see if form has objectkeyid
    dataService.get('form', {KEYID:$scope.form.FORMID}).then(function (data) {
      var form=data.data[0];
      if (form.OBJKEYID===null) {  //  if form.objkeyid is null, then record has not been created
        createPOC(); //create record
      } else {
        editPOC(); //if form.objkeyid exists, then record exists, edit record
      }
    });
  };


  function getPOCobject () {
      var POCobject = {
        FORMID: $scope.form.FORMID,
        SUPPLIES: {
          Supplies: $scope.Supplies,
          Trays: $scope.Trays,
          Irrigation: $scope.Irrigation,
          Gloves: $scope.Gloves,
          Walker: $scope.Walker,
          Commode: $scope.Commode,
          Tape: $scope.Tape,
          Syringes: $scope.Syringes,
          Dressings: $scope.Dressings,
          WC: $scope.WC,
          NS: $scope.NS,
          Ostomy: $scope.Ostomy,
          O2: $scope.O2,
          Cane: $scope.Cane,
          Hoyerlift: $scope.Hoyerlift,
          Glucometer: $scope.Glucometer,
          Sharps: $scope.Sharps,
          Shower: $scope.Shower,
          Tube: $scope.Tube,
          Pump: $scope.Pump,
          Hospital: $scope.Hospital,
          Other1: $scope.Other1,
        },
        SAFETY: {
          Seizure: $scope.Seizure,
          Wheelchair: $scope.Wheelchair,
          Precautions: $scope.Precautions,
          Anticoagulant: $scope.Anticoagulant,
          Hoyer: $scope.Hoyer,
          Lifeline: $scope.Lifeline,
          Disposal: $scope.Disposal,
          Fall: $scope.Fall,
          Infection: $scope.Infection,
          SMA: $scope.SMA,
          Scatter: $scope.Scatter,
          Pathways: $scope.Pathways,
          TWA: $scope.TWA,
          Railsup: $scope.Railsup,
          rail1: $scope.rail1,
          rail2: $scope.rail2,
          pds: $scope.pds,
          Emergency: $scope.Emergency,
          Other2: $scope.Other2,
        },
        NUTRITIONAL : {
          Diet: $scope.Diet,
          Fluid: $scope.Fluid,
          gts: $scope.gts,
          ff: $scope.ff,
          Medication: $scope.Medication,
          Food: $scope.Food,
        },
        FUNCTIONAL : {
          Amputation: $scope.Amputation,
          Bowel: $scope.Bowel,
          Contracture: $scope.Contracture,
          Hearing: $scope.Hearing,
          Paralysis: $scope.Paralysis,
          Endurance: $scope.Endurance,
          Ambulation: $scope.Ambulation,
          Speech: $scope.Speech,
          Dyspnea: $scope.Dyspnea,
          Blind: $scope.Blind,
          Otherfun: $scope.Otherfun
        },
        ACTIVITIES : {
          Bedrest: $scope.Bedrest,
          brp: $scope.brp,
          uat: $scope.uat,
          bt: $scope.bt,
          ep: $scope.ep,
          pwb: $scope.pwb,
          iah: $scope.iah,
          Actwalker: $scope.Actwalker,
          Crutches: $scope.Crutches,
          Caneact: $scope.Caneact,
          act_wc: $scope.act_wc,
          restrictions: $scope.restrictions,
          Other3: $scope.Other3,
        },
        MENTAL : {
          Oriented: $scope.Oriented,
          Comatose: $scope.Comatose,
          Forgetful: $scope.Forgetful,
          Depressed: $scope.Depressed,
          Disoriented: $scope.Disoriented,
          Agitated: $scope.Agitated,
          Other4: $scope.Other4,
        },
        ORDERS : {
          race : $scope.Prognosis,
          rehab: $scope.rehab,
          mdsn: $scope.mdsn,
          txtpatientzipcode: $scope.txtpatientzipcode,
          ept: $scope.ept,
          ote: $scope.ote,
          ste: $scope.ste,
          msw: $scope.msw,
          pns: $scope.pns,
          psd: $scope.psd,
        },
        SKILLED: {
          md: $scope.md,
          sbp: $scope.sbp,
          lt: $scope.lt,
          dbp: $scope.dbp,
          olt: $scope.olt,
          pgt: $scope.pgt,
          gtr: $scope.gtr,
          lrs: $scope.lrs,
          rgt: $scope.rgt,
          grt: $scope.grt,
          ls: $scope.ls,
          tem: $scope.tem,
          tgt: $scope.tgt,
          eig: $scope.eig,
          ggt: $scope.ggt,
          lbsi: $scope.lbsi,
          bsf: $scope.bsf,
          mbs: $scope.mbs,
          bsg: $scope.bsg,
          orlt: $scope.orlt,
          ypp: $scope.ypp,
          tbp: $scope.tbp,
          pls: $scope.pls,
          ght: $scope.ght,
          sn: $scope.sn,
          ians: $scope.ians,
          oxy: $scope.oxy,
          svn: $scope.svn,
          wcs: $scope.wcs,
          tsf: $scope.tsf,
          eth: $scope.eth,
          hha: $scope.hha,
          vs: $scope.vs,
          tpc: $scope.tpc,
          pcw: $scope.pcw,
          mbp: $scope.mbp,
          plse: $scope.plse,
          tsn: $scope.tsn,
          pc: $scope.pc,
          isApplicable: $scope.sectionSkilled
        },
        LAB: {
          ven: $scope.ven,
          idt: $scope.idt,
          ntt: $scope.ntt,
          isApplicable: $scope.sectionLab
        },
        IV : {
          snw: $scope.snw,
          dos: $scope.dos,
          rtm: $scope.rtm,
          hickman: $scope.hickman,
          ggc: $scope.ggc,
          picc: $scope.picc,
          perl: $scope.perl,
          ptc: $scope.ptc,
          frq: $scope.frq,
          lot: $scope.lot,
          alf: $scope.alf,
          flw: $scope.flw,
          fln: $scope.fln,
          adm: $scope.adm,
          ini: $scope.ini,
          medi: $scope.medi,
          cco: $scope.cco,
          flusw: $scope.flusw,
          linew: $scope.linew,
          foll: $scope.foll,
          hep: $scope.hep,
          cpic: $scope.cpic,
          cht: $scope.cht,
          dpt: $scope.dpt,
          drq: $scope.drq,
          fry: $scope.fry,
          ipc: $scope.ipc,
          inc: $scope.inc,
          piu: $scope.piu,
          infec: $scope.infec,
          was: $scope.was,
          mat: $scope.mat,
          app: $scope.app,
          othr: $scope.othr,
          otr: $scope.otr,
          tpi: $scope.tpi,
          temo: $scope.temo,
          tpcr: $scope.tpcr,
          equ: $scope.equ,
          othh: $scope.othh,
          otre: $scope.otre,
          isApplicable: $scope.sectionIV
        },
        PAIN : {
          tsnw: $scope.tsnw,
          willm: $scope.willm,
          medic: $scope.medic,
          snin: $scope.snin,
          inth: $scope.inth,
          inst: $scope.inst,
          otreh: $scope.otreh,
          otrh: $scope.otrh,
          moni: $scope.moni,
          tor: $scope.tor,
          tore: $scope.tore,
          tpp: $scope.tpp,
          cont: $scope.cont,
          bet: $scope.bet,
          wit: $scope.wit,
          pati: $scope.pati,
          witn: $scope.witn,
          careg: $scope.careg,
          freq: $scope.freq,
          pai: $scope.pai,
          twil: $scope.twil,
          str: $scope.str,
          wo: $scope.wo,
          wou: $scope.wou,
          cle: $scope.cle,
          dr: $scope.dr,
          wth: $scope.wth,
          cov: $scope.cov,
          sec: $scope.sec,
          wu: $scope.wu,
          won: $scope.won,
          cla: $scope.cla,
          patd: $scope.patd,
          pac: $scope.pac,
          cove: $scope.cove,
          secu: $scope.secu,
          wn: $scope.wn,
          wod: $scope.wod,
          cln: $scope.cln,
          yap: $scope.yap,
          kw: $scope.kw,
          ver: $scope.ver,
          secr: $scope.secr,
          wd: $scope.wd,
          wun: $scope.wun,
          cls: $scope.cls,
          ylp: $scope.ylp,
          ack: $scope.ack,
          ith: $scope.ith,
          ser: $scope.ser,
          mea: $scope.mea,
          ase: $scope.ase,
          noti: $scope.noti,
          basl: $scope.basl,
          tru: $scope.tru,
          ent: $scope.ent,
          reg: $scope.reg,
          uct: $scope.uct,
          dec: $scope.dec,
          siz: $scope.siz,
          cmw: $scope.cmw,
          comp: $scope.comp,
          heal: $scope.heal,
          fr: $scope.fr,
          bsl: $scope.bsl,
          ovr: $scope.ovr,
          verb: $scope.verb,
          verw: $scope.verw,
          wthi: $scope.wthi,
          dem: $scope.dem,
          abl: $scope.abl,
          isApplicableW: $scope.sectionWound,
          isApplicableP: $scope.sectionPain
        },
        MUSCULOSKELETAL : {
          rela : $scope.rela,
          instr: $scope.instr,
          sat: $scope.sat,
          Asses: $scope.Asses,
          pasn: $scope.pasn,
          uso: $scope.uso,
          car: $scope.car,
          gac: $scope.gac,
          ety: $scope.ety,
          ble: $scope.ble,
          thin: $scope.thin,
          balo: $scope.balo,
          mdw: $scope.mdw,
          stra: $scope.stra,
          vic: $scope.vic,
          cto: $scope.cto,
          fet: $scope.fet,
          isApplicable: $scope.sectionMuscul
        },
        RENAL : {
          atr: $scope.atr,
          mess: $scope.mess,
          eat: $scope.eat,
          cted: $scope.cted,
          snwl: $scope.snwl,
          raig: $scope.raig,
          het: $scope.het,
          fole: $scope.fole,
          folc: $scope.folc,
          ench: $scope.ench,
          loon: $scope.loon,
          patie: $scope.patie,
          giv: $scope.giv,
          lmo: $scope.lmo,
          very: $scope.very,
          regis: $scope.regis,
          let: $scope.let,
          ual: $scope.ual,
          ify: $scope.ify,
          equa: $scope.equa,
          hyd: $scope.hyd,
          sxi: $scope.sxi,
          ien: $scope.ien,
          ski: $scope.ski,
          ete: $scope.ete,
          bagg: $scope.bagg,
          ets: $scope.ets,
          truc: $scope.truc,
          uti: $scope.uti,
          lipl: $scope.lipl,
          rit: $scope.rit,
          odo: $scope.odo,
          patc: $scope.patc,
          now: $scope.now,
          pcg: $scope.pcg,
          bali: $scope.bali,
          fetc: $scope.fetc,
          fem: $scope.fem,
          mai: $scope.mai,
          fec: $scope.fec,
          tyr: $scope.tyr,
          forc: $scope.forc,
          isApplicable: $scope.sectionRenal
        },
        CARDIOVASCULAR : {
          ated: $scope.ated,
          ess: $scope.ess,
          pats: $scope.pats,
          gic: $scope.gic,
          gard: $scope.gard,
          era: $scope.era,
          edm: $scope.edm,
          cha: $scope.cha,
          chn: $scope.chn,
          fib: $scope.fib,
          ail: $scope.ail,
          mdo: $scope.mdo,
          ole: $scope.ole,
          dix: $scope.dix,
          pos: $scope.pos,
          itr: $scope.itr,
          mpl: $scope.mpl,
          ars: $scope.ars,
          equs: $scope.equs,
          clu: $scope.clu,
          tpa: $scope.tpa,
          dosg: $scope.dosg,
          medc: $scope.medc,
          verp: $scope.verp,
          imi: $scope.imi,
          chp: $scope.chp,
          afe: $scope.afe,
          patp: $scope.patp,
          patws: $scope.patws,
          bls: $scope.bls,
          pab: $scope.pab,
          patw: $scope.patw,
          demd: $scope.demd,
          resw: $scope.resw,
          alig: $scope.alig,
          adh: $scope.adh,
          thi: $scope.thi,
          trp: $scope.trp,
          demo: $scope.demo,
          isApplicable: $scope.sectionCardio
        },
        NEUROLOGICAL : {
          ida: $scope.ida,
          hgr: $scope.hgr,
          gris: $scope.gris,
          pup: $scope.pup,
          wea: $scope.wea,
          iou: $scope.iou,
          hal: $scope.hal,
          dirc: $scope.dirc,
          patg: $scope.patg,
          cgp: $scope.cgp,
          ffe: $scope.ffe,
          regt: $scope.regt,
          regi: $scope.regi,
          ses: $scope.ses,
          mana: $scope.mana,
          age: $scope.age,
          req: $scope.req,
          eff: $scope.eff,
          relt: $scope.relt,
          ons: $scope.ons,
          icat: $scope.icat,
          ict: $scope.ict,
          carg: $scope.carg,
          bals: $scope.bals,
          sei: $scope.sei,
          spa: $scope.spa,
          con: $scope.con,
          frqs: $scope.frqs,
          emo: $scope.emo,
          ica: $scope.ica,
          onsa: $scope.onsa,
          kdo: $scope.kdo,
          rba: $scope.rba,
          mdwa: $scope.mdwa,
          neu: $scope.neu,
          sxs: $scope.sxs,
          isApplicable: $scope.sectionNeuro
        },
        ENDOCRINE : {
          end: $scope.end,
          isn: $scope.isn,
          tin: $scope.tin,
          rat: $scope.rat,
          chg: $scope.chg,
          gar: $scope.gar,
          repl: $scope.repl,
          hta: $scope.hta,
          sst: $scope.sst,
          admi: $scope.admi,
          tsr: $scope.tsr,
          rot: $scope.rot,
          ind: $scope.ind,
          dia: $scope.dia,
          ret: $scope.ret,
          abg: $scope.abg,
          prs: $scope.prs,
          sug: $scope.sug,
          lan: $scope.lan,
          cgl: $scope.cgl,
          acc: $scope.acc,
          wpa: $scope.wpa,
          alis: $scope.alis,
          perg: $scope.perg,
          ves: $scope.ves,
          det: $scope.det,
          wee: $scope.wee,
          ont: $scope.ont,
          com: $scope.com,
          suw: $scope.suw,
          ents: $scope.ents,
          goos: $scope.goos,
          dis: $scope.dis,
          bes: $scope.bes,
          ysi: $scope.ysi,
          clus: $scope.clus,
          int: $scope.int,
          isApplicable: $scope.sectionEndo
        },
        RESPIRATORY : {
          ela: $scope.ela,
          nst: $scope.nst,
          res: $scope.res,
          ruc: $scope.ruc,
          brs: $scope.brs,
          est: $scope.est,
          patl: $scope.patl,
          trm: $scope.trm,
          yel: $scope.yel,
          hec: $scope.hec,
          mok: $scope.mok,
          fla: $scope.fla,
          merg: $scope.merg,
          ner: $scope.ner,
          oni: $scope.oni,
          cas: $scope.cas,
          cti: $scope.cti,
          redg: $scope.redg,
          quf: $scope.quf,
          ids: $scope.ids,
          cta: $scope.cta,
          bal: $scope.bal,
          mdwh: $scope.mdwh,
          ems: $scope.ems,
          qui: $scope.qui,
          rats: $scope.rats,
          ratsw: $scope.ratsw,
          dto: $scope.dto,
          liz: $scope.liz,
          rwp: $scope.rwp,
          eqi: $scope.eqi,
          aliz: $scope.aliz,
          isApplicable: $scope.sectionRes
        },
        GASTROINTESTINAL : {
          adp: $scope.adp,
          ccz: $scope.ccz,
          nuk: $scope.nuk,
          nau: $scope.nau,
          pym: $scope.pym,
          vom: $scope.vom,
          disa: $scope.disa,
          thw: $scope.thw,
          prl: $scope.prl,
          rev: $scope.rev,
          olz: $scope.olz,
          ibr: $scope.ibr,
          buu: $scope.buu,
          ages: $scope.ages,
          usf: $scope.usf,
          occ: $scope.occ,
          occl: $scope.occl,
          noq: $scope.noq,
          mkd: $scope.mkd,
          abd: $scope.abd,
          paij: $scope.paij,
          gik: $scope.gik,
          siu: $scope.siu,
          tio: $scope.tio,
          fucl: $scope.fucl,
          wli: $scope.wli,
          tur: $scope.tur,
          yla: $scope.yla,
          tho: $scope.tho,
          ithe: $scope.ithe,
          mon: $scope.mon,
          vem: $scope.vem,
          ayd: $scope.ayd,
          prd: $scope.prd,
          mini: $scope.mini,
          icl: $scope.icl,
          inl: $scope.inl,
          ipa: $scope.ipa,
          tra: $scope.tra,
          isApplicable: $scope.sectionGastro
        },
        DISCHARGE : {
          sel: $scope.sel,
          givs: $scope.givs,
          mesa:$scope.mesa,
          sef:$scope.sef,
          zop:$scope.zop,
          zoy:$scope.zoy,
        }
      };
      return POCobject;
    }

  function createPOC() {
    var newPOC = getPOCobject();
    console.log('newPOC', newPOC);
    //save POC
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['planOfCare'].add,
      method: 'POST',
      data: $httpParamSerializerJQLike(newPOC),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function(response){
      console.log('saving newPOC', response);
      if(response.data.status==='success') {
        //edit form to contain objectkeyid and status
        var form = {
          KEYID: $scope.form.FORMID,
          OBJKEYID: response.data.keyid,
          STATUS: 2,
        };

        // Ivan added : save form data
        $scope.form.OBJKEYID = response.data.keyid;
        formService.set( $scope.form )

        //if submitting to QA, change status to 1 for complete
        if ($scope.submitQA===true) {
          form.STATUS=1;
        }
        dataService.edit('form', form).then(function (response) {
          console.log('edit form', response);
          $state.go('viewPlanOfCare')
        });
      }
    }, function(data) {
      console.log(data);
    });
  }


  $scope.print = function() {
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['planOfCare'].getPDF,
      method: 'GET',
      params: {KEYID: $scope.form.OBJKEYID},
      paramSerializer: '$httpParamSerializerJQLike',
      responseType: 'arraybuffer'
    }).then(function(data) {
      console.log('data', data);
      console.log('headers', data.headers());

      var contentType = data.headers()['content-type'];
      var disposition = data.headers()['content-disposition'];
      console.log('disposition', disposition);

      if (disposition && disposition.indexOf('attachment') !== -1) {
        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        var matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) { 
          filename = matches[1].replace(/['"]/g, '');
        } else {
          filename = "unknown";
        }
      }

      console.log('filename', filename);
      console.log('contentType', contentType)
      console.log('data.data', data.data);
      
      var dataFile = new Blob([data.data], { type: contentType });

      console.log('dataFile', dataFile);

      FileSaver.saveAs(dataFile, filename);
    });
  };

  function editPOC() {
    var editPOCchanges = getPOCobject();
    editPOCchanges.KEYID=$scope.form.OBJKEYID;
    console.log("editStartOfCare", editPOCchanges);
    //edit POC
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['planOfCare'].edit,
      method: 'POST',
      data: $httpParamSerializerJQLike(editPOCchanges),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function(response){
      console.log('editing POC', response);
      if(response.data.status==='success') {
        //if submitting to QA, change status to 1 for complete
        if ($scope.submitQA===true) {
          var form={
            KEYID: $scope.form.FORMID,
            OBJKEYID: response.data.keyid,
            STATUS: 1
          }
          dataService.edit('form', form).then(function (response) {
            console.log('edit form', response);
          });
        }
        $state.go('viewPlanOfCare')
      }
    }, function(data) {
      console.log(data);
    });
  }

  $scope.submitToQA=function () {
    if (!confirm("No changes are allowed once submitted to QA, are you sure you want to submit?")) {
      console.log('no, do not submit to qa');
      $scope.submitQA=false;
      event.preventDefault();
    } else { //yes submit to qa
      $scope.submitQA=true;
      $scope.savePOC();
    }
  };

  //
  //build ORDERS
  var ordersObj={
    md: {
      'md_sbp': $scope.sbp,
      'md_lt': $scope.lt,
      'md_dbp': $scope.dbp,
      'md_olt': $scope.olt
    },
    pgt: {
      'pgt_gtr': $scope.gtr,
      'pgt_les': $scope.les,
    },
    rgt: {
      'rgt_grt': $scope.grt,
      'rgt_ls': $scope.ls,
    },
    tem: {
      'tem_tgt': $scope.tgt,
    },
    eig: {
      'eig_ggt': $scope.ggt,
      'eig_lbsi': $scope.lbsi,
    },
    bsf: {
      'bsf_mbs': $scope.mbs,
      'bsf_bsg': $scope.bsg,
      'bsf_orlt': $scope.orlt,
    },
    ypp: {
      'ypp_tbp': $scope.tbp,
      'ypp_pls': $scope.pls,
      'ypp_ght': $scope.ght,
    },
    sn: {
      'sn_wwm': $scope.wwm,
      'sn_ians': $scope.ians,
      'sn_oxy': $scope.oxy,
      'sn_svn': $scope.svn,
      'sn_wcs': $scope.wcs,
      'sn_tsf': $scope.tsf,
      'sn_eth': $scope.eth,
      'sn_hha': $scope.hha,
    },
    pcw: {
      'pcw_mbp': $scope.mbp,
      'pcw_plse': $scope.plse,
      'pcw_tsn': $scope.tsn,
    },
    flw: {
      'flw_fln': $scope.fln,
    },
    adm: {
      'adm_ini': $scope.ini,
      'adm_medi': $scope.medi,
      'adm_cco': $scope.cco,
    },
    flusw: {
      'flusw_linew': $scope.linew,
      'flusw_foll': $scope.foll,
      'flusw_hep': $scope.hep
    },
    cpic: {
      'cpic_cht': $scope.cht
    },
    dpt: {
      'dpt_drq': $scope.drq,
      'dpt_fry': $scope.fry,
    },
    ipc: {
      'ipc_inc': $scope.ipc,
    },
    infec: {
      'infec_was': $scope.was,
      'infec_mat': $scope. mat,
      'infec_app': $scope.app,
    },
    othr: {
      'othr_otr': $scope.otr,
    },
    tpi: {
      'tpi_temo': $scope.temo,
    },
    tpcr: {
      'tpcr_equ': $scope.equ,
    },
    othh: {
      'othh_otre': $scope.otre
    },
    willm: {
      'willm_medic': $scope.medic,
    },
    otreh: {
      'otreh_otrh': $scope.otrh
    },
    tor: {
      'tor_tore': $scope.tore
    },
    tpp: {
      'tpp_cont': $scope.cont,
      'tpp_bet': $scope.bet,
      'tpp_wit': $scope.wit,
    },
    pati: {
      'pati_witn': $scope.witn,
    },
    careg: {
      'careg_freq': $scope.freq,
      'careg_pai': $scope.pai,
    },
    twil: {
      'twil_str': $scope.str,
    },
    wo: {
      'wo_wou': $scope.wou,
      'wo_cle': $scope.cle,
      'wo_dr': $scope.dr,
      'wo_wth': $scope.wth,
      'wo_cov': $scope.cov,
      'wo_sec': $scope.sec,
    },
    wu: {
      'wu_won': $scope.won,
      'wu_cla': $scope.cla,
      'wu_patd': $scope.patd,
      'wu_pac': $scope.pac,
      'wu_cove': $scope.cove,
      'wu_secu': $scope.secu,
    },
    wn: {
      'wn_wod': $scope.wod,
      'wn_cln': $scope.cln,
      'wn_yap': $scope.yap,
      'wn_kw': $scope.kw,
      'wn_ver': $scope.ver,
      'wn_secr': $scope.secr,
    },
    wd: {
      'wd_wun': $scope.wun,
      'wd_cls': $scope.cls,
      'wd_ylp': $scope.ylp,
      'wd_ack': $scope.ack,
      'wd_ith': $scope.ith,
      'wd_ser': $scope.ser,
    },
    noti: {
      'noti_basl': $scope.basl,
    },
    reg: {
      'reg_uct': $scope.uct,
    },
    dec: {
      'dec_siz': $scope.size,
      'dec_cmw': $scope.cmw,
    },
    comp: {
      'comp_heal': $scope.heal,
    },
    fr: {
      'fr_bsl': $scope.bsl,
    },
    ovr: {
      'ovr_verb': $scope.verb
    },
    verw: {
      'verw_wthi': $scope.wthi
    },
    dem: {
      'dem_abl': $scope.abl
    },
    //MUSCULOSKELETAL
    instr: {
      'instr_sat': $scope.sat,
    },
    pasn: {
      'pasn_uso': $scope.uso,
    },
    gac: {
      'gac_ety': $scope.ety,
    },
    ble: {
      'ble_thin': $scope.thin,
    },
    balo: {
      'balo_mdw': $scope.mdw,
    },
    stra: {
      'stra_vic': $scope.vic
    },
    cto: {
      'cto_fet': $scope.fet
    },
    //RENAL
    mess: {
      'mess_eat': $scope.eat,
      'mess_cted': $scope.cted,
    },
    snwl: {
      'snwl_raig': $scope.raig,
      'snwl_het': $scope.het,
    },
    fole:{
      'fole_folc': $scope.folc,
      'fole_ench': $scope.ench,
      'fole_loon': $scope.loon,
    },
    patie: {
      'patie_giv': $scope.giv,
      'patie_lmo': $scope.lmo,
      'patie_very': $scope.very,
    },
    regis: {
      'regis_let': $scope.let,
    },
    ual: {
      'ual_ify': $scope.ify,
    },
    equa: {
      'equa_hyd': $scope.hyd,
      'equa_sxi': $scope.sxi,
    },
    ien: {
      'ien_ski': $scope.sky,
    },
    ete: {
      'ete_bagg': $scope.bagg,
    },
    ets: {
      'ets_truc': $scope.truc,
    },
    uti: {
      'uti_lipl': $scope.lipl,
    },
    rit: {
      'rit_odo': $scope.odo
    },
    patc: {
      'patc_now': $scope.now,
    },
    pcg: {
      'pcg_bali': $scope.bali,
      'pcg_fetc': $scope.fetc,
    },
    mai: {
      'mai_fec': $scope.fec,
    },
    tyr: {
      'tyr_forc': $scope.forc,
    },
    //CARDIOVASCULAR
    gic: {
      'gic_gard': $scope.gard,
    },
    era: {
      'era_edm': $scope.edm,
    },
    cha: {
      'cha_chn': $scope.chn,
    },
    fib: {
      'fib_ail': $scope.ail
    },
    mdo: {
      'mdo_ole': $scope.ole
    },
    equs: {
      'equs_clu': $scope.clu
    },
    tpa: {
      'tpa_dosg': $scope.dosg,
      'tpa_medc': $scope.medc,
    },
    verp: {
      'verp_imi': $scope.imi
    },
    chp: {
      'chp_afe': $scope.afe,
    },
    patp: {
      'patp_patws': $scope.patws
    },
    pab:{
      'pab_patw': $scope.patw
    },
    demd: {
      'demd_resw': $scope.resw
    },
    alig: {
      'alig_adh': $scope.adh,
      'alig_thi': $scope.thi,
    },
    trp: {
      'trp_demo': $scope.demo
    },
    //NEUROLOGICAL
    hgr: {
      'hgr_gris': $scope.gris,
      'hgr_pup': $scope.pup,
      'hgr_wea': $scope.wea,
      'hgr_iou': $scope.iou,
      'hgr_hal': $scope.hal,
      'hgr_dirc': $scope.dirc,
    },
    cgp: {
      'cgp_ffe': $scope.ffe,
    },
    mana: {
      'mana_age': $scope.age,
      'mana_req': $scope.req
    },
    relt: {
      'relt_ons': $scope.ons
    },
    icat: {
      'icat_ict': $scope.ict
    },
    carg: {
      'carg_bals': $scope.bals,
      'carg_sei': $scope.sei
    },
    spa: {
      'spa_con': $scope.con,
      'spa_frqs': $scope.frqs
    },
    emo: {
      'emo_qua': $scope.qua,
      'emo_ica': $scope.ica
    },
    onsa: {
      'onsa_kdo': $scope.kdo
    },
    rba: {
      'rba_mdwa': $scope.mdwa
    },
    neu: {
      'neu_sxs': $scope.sxs
    },
    //ENDOCRINE
    chg: {
      'chg_gar': $scope.gar,
    },
    repl: {
      'repl_hta': $scope.hta,
      'repl_sst': $scope.sst
    },
    tsr: {
      'tsr_rot': $scope.rot
    },
    sug: {
      'sug_lan': $scope.lan
    },
    cgl: {
      'cgl_wpa': $scope.wpa
    },
    wpa: {
      'wpa_alis': $scope.ali,
      'wpa_perg': $scope.perg
    },
    det: {
      'det_wee': $scope.wee
    },
    ont: {
      'ont_com': $scope.com,
      'ont_suw': $scope.suw,
      'ont_ents': $scope.ents
    },
    dis: {
      'dis_bes': $scope.bis,
      'dis_ysi': $scope.ysi,
    },
    clus: {
      'clus_int': $scope.int
    },
    //RESPIRATORY
    nst: {
      'nst_res': $scope.res,
    },
    ruc: {
      'ruc_brs': $scope.brs,
      'ruc_est': $scope.est,
      'ruc_patl': $scope.patl,
      'ruc_trm': $scope.trm,
      'ruc_yel': $scope.yel,
    },
    hec: {
      'hec_mok': $scope.mok,
      'hec_fla': $scope.fla
    },
    cti: {
      'cti_redg': $scope.redg,
    },
    quf: {
      'quf_ids': $scope.ids
    },
    cta: {
      'cta_bal': $scope.bal,
      'cta_mdwh': $scope.mdwh
    },
    ems: {
      'ems_qui': $scope.qui
    },
    rats: {
      'rats_ratsw': $scope.ratsw
    },
    liz: {
      'liz_rwp': $scope.rwp
    },
    egi: {
      'egi_aliz': $scope.aliz
    },
    //GASTROINTESTINAL
    adp: {
      'adp_ccz': $scope.ccz
    },
    nau: {
      'nau_pym': $scope.pym,
      'nau_vom': $scope.vom,
      'nau_disa': $scope.disa,
      'nau_thw': $scope.thw,
    },
    prl: {
      'prl_rev': $scope.rev,
      'prl_olz': $scope.olz,
      'prl_ibr': $scope.ibr,
      'prl_buu': $scope.buu,
      'prl_ages': $scope.ages,
      'prl_usf': $scope.usf,
    },
    occ: {
      'occ_occl': $scope.occl,
      'occ_noq': $scope.noq
    },
    mkd: {
      'mkd_abd': $scope.abd
    },
    paij: {
      'paij_gik': $scope.gik
    },
    tio: {
      'tio_fucl': $scope.fucl
    },
    wli: {
      'wli_tur': $scope.tur,
      'wli_yla': $scope.yla,
      'wli_tho': $scope.tho,
      'wli_ithe': $scope.ithe,
    },
    mon:{
      'mon_vem': $scope.vem,
      'mon_ayd': $scope.ayd
    },
    prd: {
      'prd_mini': $scope.mini
    },
    icl: {
      'icl_inl': $scope.inl
    },
    ipa: {
      'ipa_tra': $scope.tra
    },
  };


  $scope.checkbox=function (key) {
    if($scope[key]==='') {
      var fieldsObj=ordersObj[key];
      var inputArr=Object.keys(fieldsObj);
      for (var i = 0; i < inputArr.length; i++) {
        var n=inputArr[i].indexOf('_');
        var input=inputArr[i].slice(n+1,inputArr[i].length);
        $scope[input]='';
      }
    }
  };

  var sectionsObj = {
    sectionSkilled: [ 'md', 'pgt', 'rgt', 'tem', 'eig', 'bsf', 'ypp', 'sn', 'vs', 'tpc', 'pcw', 'pc' ],
    sectionLab: [ 'ven', 'idt', 'ntt' ],
    sectionIV: [ 'snw', 'dos', 'rtm', 'hickman', 'ggc', 'picc', 'perl', 'ptc', 'frq', 'lot', 'alf', 'flw', 'adm', 'flusw', 'cpic', 'dpt', 'ipc', 'piu', 'infec', 'othr', 'tpi', 'tpcr', 'othh' ],
    sectionPain: [ 'tsnw', 'willm', 'snin', 'inth', 'inst', 'otreh', 'moni', 'tor', 'tpp', 'pati', 'careg', 'twil' ],
    sectionWound: [ 'wo', 'wu', 'wn', 'wd', 'mea', 'ase', 'noti', 'tru', 'ent', 'reg', 'dec', 'comp', 'fr', 'ovr', 'verw', 'dem' ],
    sectionMuscul: [ 'rela', 'instr', 'Asses', 'pasn', 'car', 'gac', 'ble', 'balo', 'stra', 'cto' ],
    sectionRenal: [ 'atr', 'mess', 'snwl', 'fole', 'patie', 'regis', 'ual', 'equa', 'ien', 'ete', 'ets', 'uti', 'rit', 'patc', 'pcg', 'rem', 'mai', 'tyr' ],
    sectionCardio: [ 'ated', 'ess', 'pats', 'gic', 'era', 'cha', 'fib', 'mdo', 'dix', 'pos', 'itr', 'mpl', 'ars', 'equs', 'tpa', 'verp', 'chp', 'patp', 'bls', 'pab', 'demd', 'alig', 'trp' ],
    sectionNeuro: [ 'ida', 'hgr', 'patg', 'cgp', 'regt', 'regi', 'ses', 'mana', 'eff', 'relt', 'icat', 'carg', 'spa', 'emo', 'onsa', 'rba', 'neu' ],
    sectionEndo: [ 'end', 'isn', 'tin', 'rat', 'chg', 'repl', 'admi', 'tsr', 'ind', 'dia', 'ret', 'abg', 'prs', 'sug', 'cgl', 'wpa', 'ves', 'det', 'ont', 'goos', 'dis', 'clus' ],
    sectionRes: [ 'ela', 'nst', 'ruc', 'hec', 'merg', 'ner', 'oni', 'cas', 'cti', 'quf', 'cta', 'ems', 'rats', 'dto', 'liz', 'egi' ],
    sectionGastro: [ 'adp', 'nuk', 'nau', 'prl', 'occ', 'mkd', 'paij', 'siu', 'tio', 'wli', 'mon', 'prd', 'icl', 'ipa' ],
  }

  $scope.enableSystemSection = function ( key ) {
    for ( let i=0; i<sectionsObj[ key ].length; i++ ) {
      $scope[ sectionsObj[ key ][ i ] ] = "";

      if ( ordersObj[ sectionsObj[ key ][ i ] ] ) {
        $scope.checkbox( sectionsObj[ key ][ i ] );
      }
    }
  }

  $scope.getPOC();
  $scope.isOasisComplete();

});
