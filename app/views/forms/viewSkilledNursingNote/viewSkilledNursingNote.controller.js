app.controller('ViewSkilledNursingNoteController', function($scope, $filter, $state, dataService, patientService, formService) {

  $scope.patient = patientService.get();
  console.log('patient', $scope.patient);

  $scope.form=formService.get();
  console.log('form', $scope.form);

  $scope.currentForm = 'Vitals';

  $scope.nextForm = function(current, destination) {
    if ($scope.currentForm != destination) {
      $scope.currentForm = destination;
      $('.' + current ).fadeOut('fast', function() {
        $('.' + destination ).fadeIn('fast');
      });
    }
  };

  $scope.activeForm = function(form) {
    if ($scope.currentForm == form) {
      return true;
    }
  };

  dataService.get('skilledNursingNote', {'FORMID': $scope.form.FORMID}).then(function(data){
    var note = data.data[0];
    console.log(note);

    var note=data.data[0];

    $scope.signature=note.signature;
    $scope.signature_date=new Date($filter('date')(note.signature_date, 'yyyy/MM/dd'));

    if (note.completeTemp===1) {
      $scope.addTemp=true;
      $scope.check_temp=true;
      $scope.completeTemp=note.completeTemp;
      $scope.count_temp=note.count_temp;
      $scope.temp_type=note.temp_type;
      $scope.tempNotebox=note.tempNotebox;
      if(note.completeTemp===1) {
        $scope.alert=true;
      }
    }
    if (note.completeVitPulse===1) {
      $scope.addVitPulse=true;
      $scope.check_vitPulse=true;
      $scope.completeVitPulse=note.completeVitPulse;
      $scope.count_pulse=note.count_pulse;
      $scope.vitPulse_type=note.vitPulse_type;
      $scope.vitPulse_unit=note.vitPulse_unit;
      $scope.pulseNotebox=note.pulseNotebox;
    }
    if (note.completeResp===1) {
      $scope.addResp=true;
      $scope.check_resp=true;
      $scope.completeResp=note.completeResp;
      $scope.count_resp=note.count_resp;
      $scope.resp_type=note.resp_type;
    }
    if (note.completeBP===1) {
      $scope.addBP=true;
      $scope.check_BP=true;
      $scope.completeBP=note.completeBP;
      if (note.sitting===1) {
        $scope.sitting=note.sitting;
        $scope.count_systolic_1=note.count_systolic_1;
        $scope.count_diastolic_1=note.count_diastolic_1;
        $scope.BP_type_1=note.BP_type_1;
      }
      if (note.standing===1) {
        $scope.standing=note.standing;
        $scope.count_systolic_2=note.count_systolic_2;
        $scope.count_diastolic_2=note.count_diastolic_2;
        $scope.BP_type_2=note.BP_type_2;
      }
      if (note.lying===1) {
        $scope.lying=note.lying;
        $scope.count_systolic_3=note.count_systolic_3;
        $scope.count_diastolic_3=note.count_diastolic_3;
        $scope.BP_type_3=note.BP_type_3;
      }
    }
    if (note.completeWeight===1) {
      $scope.addWeight=true;
      $scope.check_weight=true;
      $scope.completeWeight=note.completeWeight;
      $scope.count_weight=note.count_weight;
    }
    if (note.completeSugar===1) {
      $scope.addSugar=true;
      $scope.check_sugar=true;
      $scope.completeSugar=note.completeSugar;
      $scope.count_sugar=note.count_sugar;
      $scope.BP_type=note.BP_type;
    }
    if (note.completeINR===1){
      $scope.addINR=true;
      $scope.check_INR=true;
      $scope.completeINR=note.completeINR;
      $scope.count_INR=note.count_INR;
    }
    if (note.completePainLoc===1) {
      $scope.addPainLoc=true;
      $scope.check_painLoc=true;
      $scope.completePainLoc=note.completePainLoc;
      $scope.pain_location=note.pain_location;
    }
    if (note.completePainCause===1) {
      $scope.addPainCause=true;
      $scope.check_painCause=true;
      $scope.completePainCause=note.completePainCause;
      $scope.pain_cause=note.pain_cause;
    }
    if (note.completePainLevel===1) {
      $scope.addPainLevel=true;
      $scope.check_painLevel=true;
      $scope.completePainLevel=note.completePainLevel;
      $scope.painScaleType=note.painScaleType;
      $scope.intensity=parseInt(note.intensity);
      $scope.interventionNumeric=note.interventionNumeric;
      $scope.notifyNumeric=note.notifyNumeric;
    }
    if (note.completePainDescription===1) {
      $scope.addPainLevel=true;
      $scope.check_painDescription=true;
      $scope.completePainDescription=note.completePainDescription;
      $scope.description=note.description;
      $scope.painObservations=note.painObservations;
    }

  });


  $scope.edit=function () {
    $scope.form.ACTION='QAEDIT';
    formService.set($scope.form);
    $state.go('skilledNursingNote');
  };

  $scope.approve=function () {
    //edit form status to 3=approved;
    dataService.edit('form', {KEYID:$scope.form.FORMID, STATUS:3}).then(function (response) {
      console.log(response);
      $state.go('qa');
    });
  };

  $scope.reject=function () {
    //change form status to in progress, and attach note?
    var form={
      KEYID: $scope.form.FORMID,
      STATUS: 4,
    };
    dataService.edit('form', form).then(function (response) {
      console.log(response);
      $state.go('qa');
    });
  };

});
