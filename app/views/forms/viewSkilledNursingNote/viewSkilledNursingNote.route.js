app.config(function($stateProvider) {
  $stateProvider.state('viewSkilledNursingNote', {
    url: '/viewSkilledNursingNote',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/viewSkilledNursingNote/viewSkilledNursingNote.html',
        controller: 'ViewSkilledNursingNoteController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
