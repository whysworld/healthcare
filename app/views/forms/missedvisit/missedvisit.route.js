app.config(function ($stateProvider) {
  $stateProvider.state('missedvisit', {
    url: '/missedvisit',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/missedvisit/missedvisit.html',
        controller: 'missedVisitController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  })
})
