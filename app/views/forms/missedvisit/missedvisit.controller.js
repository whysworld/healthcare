app.controller('missedVisitController', function ($scope, $http, $httpParamSerializerJQLike, $filter, formService, dataService, patientService, $anchorScroll, $timeout, CONSTANTS, $state) {
  $scope.PREVIOUS = function () {
    window.history.back();
  };
  $scope.imgvalidate = false;
  $scope.imgvalidateerror = 1;
  $scope.showPatSignatureImage = false;
  $scope.showPatSignaturePad = true;
  $scope.visit = {};
  $scope.editform = 'no';
  var getFormValue = formService.get(); // Getting values from services
  $scope.getDetails = function () {

    if (getFormValue.STATUS == 0) {
      var formData = {
        formId: getFormValue.FORMID
      }
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['missedvisitSheet'].getAll,
        method: 'POST',
        data: $httpParamSerializerJQLike(formData),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }

      }).success(function (data) {

        $scope.visit.patient_name = data.PATKEYID;
        $scope.visit.patient_name_id = data.fn + ' ' + data.ln;

        $scope.visit.FORMID = data.formId;

      });
    }
  };
  $scope.getDetails();

  /* Save form*/
  $scope.clearPat1 = function () {
    $scope.imgvalidate = true;
    $scope.imgvalidateerror = 0;

    console.log('=-' + $scope.imgvalidateerror);
  }
  $scope.saveMissedSheet = function (form) {
    var radiobut = $scope.visit.physician_office_notified;
    $scope.validationmsg = false;
    if (radiobut != '' && typeof (radiobut) != 'undefined') {
      $scope.validationmsg = false;
    } else {
      $scope.validationmsg = true;
    }
    if ($scope.editform == 'no') {
      $scope.im = $scope.acceptPat().dataUrl
    } else if ($scope.editform == 'yes') {
      $scope.im = $scope.empSignature;
    }
    if (typeof $scope.im == 'undefined' || $scope.im == '') {
      $scope.imgvalidate = true;
      $scope.imgvalidateerror = 0;
    } else {

      if ($scope.editform == 'no' && $scope.acceptPat().dataUrl == 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjgAAADcCAQAAADXNhPAAAACIklEQVR42u3UIQEAAAzDsM+/6UsYG0okFDQHMBIJAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcCQADAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDkQAwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAegeayZAN3dLgwnAAAAAElFTkSuQmCC') {

        $scope.imgvalidate = true;
        $scope.imgvalidateerror = 0;

      } else {
        $scope.imgvalidate = false;
        $scope.imgvalidateerror = 1;
      }
    }



    if (form.$valid && $scope.imgvalidateerror) { //if (form.$valid  && $scope.validationmsg != true && imgvalidateerror)
      if (!confirm("No changes are allowed once submitted to Missed Visit, are you sure you want to submit?")) { //no don't submit to qa
        console.log('no, do not submit to qa');
        event.preventDefault();
      } else {
        $scope.visit.date_of_visit = $filter("date")($scope.visit.date_of_visit, 'yyyy-MM-dd');
        $scope.visit.episode_date = $filter("date")($scope.visit.episode_date, 'yyyy-MM-dd');
        $scope.visit.date_md_notified = $filter("date")($scope.visit.date_md_notified, 'yyyy-MM-dd');
        $scope.visit.date = $filter("date")($scope.visit.date, 'yyyy-MM-dd');
        var newRouteSheet = {
          visitdata: $scope.visit,
          sign_phy: $scope.im,
          edit_form: $scope.editform,
          currentTableId: $scope.currenttableId
        };
        console.log('---' + $scope.visit.date_of_visit);
        $http({
          url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['missedvisitSheet'].add,
          method: 'POST',
          data: $httpParamSerializerJQLike(newRouteSheet),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          }

        }).success(function (response) {
          console.log('saving routesheet', response);
          if (response.status === 'success') {
            $anchorScroll();
            $scope.successTextAlert = response.msg;
            $scope.showSuccessAlert = true;
            $timeout(function () {
              $state.go(response.goto);
            }, 2000);
          }
        }, function (data) {
          console.log(data);
        });
      }
    } else {

    }
  }
  /* End Form*/
  $scope.form = {};
  $scope.form1 = {};
  if (getFormValue.STATUS == 1 || (getFormValue.STATUS === 3)) {

    $scope.currenttableId = getFormValue.formPrimaryId;
    $scope.form1.clician_signature_path = getFormValue.clician_signature_path;
    $scope.empSignature = $scope.form1.clician_signature_path;
    var visitdate = $filter("date")(getFormValue.VISITDATE, 'yyyy/MM/dd');
    $scope.form.VISITDATE = new Date(visitdate);

    var igDate = $filter("date")(getFormValue.EPISODEDATE, 'yyyy/MM/dd');
    $scope.form.EPISODEDATE = new Date(igDate);

    var visitdate = $filter("date")(getFormValue.MDDATE, 'yyyy/MM/dd');
    $scope.form.MDDATE = new Date(visitdate);
    var igDate = $filter("date")(getFormValue.SIGDATE, 'yyyy/MM/dd');
    $scope.form.SIGDATE = new Date(igDate);
    $scope.visit = getFormValue;
    $scope.visit.patient_name = getFormValue.PATKEYID;
    $scope.visit.patient_name_id = getFormValue.FNAME + ' ' + getFormValue.LNAME;
    $scope.visit.formId = getFormValue.FORMID;

  }
  $scope.isQAModel = function () {
    if (getFormValue.ACTION === 'QAREVIEW') {

      $scope.approvedMsg = false; //form is not locked, allow editing
      $scope.enableQAEditBtn = true;
      $scope.enableQALockBtn = true;
      $scope.enableQARejectBtn = true;
      $scope.submitQAbtn = false;
      $scope.disableEdit = true;
      $scope.enableInput = false;
      $scope.showEmpSignatureImage = true;
      $scope.showEmpSignaturePad = false;
      $scope.showPatSignatureImage = true;
      $scope.showPatSignaturePad = false;
      var visitdate = $filter("date")(getFormValue.date_of_visit, 'yyyy/MM/dd');
      $scope.form.VISITDATE = new Date(visitdate);
      var igDate = $filter("date")(getFormValue.episode_date, 'yyyy/MM/dd');
      $scope.form.EPISODEDATE = new Date(igDate);
      var visitdate = $filter("date")(getFormValue.date_md_notified, 'yyyy/MM/dd');
      $scope.form.MDDATE = new Date(visitdate);
      var igDate = $filter("date")(getFormValue.date, 'yyyy/MM/dd');
      $scope.form.SIGDATE = new Date(igDate);

    } else {
      if ((getFormValue.STATUS === 1) || (getFormValue.STATUS === 3)) {

        $scope.disableEdit = true;
        $scope.enableInput = false;
        $scope.submitQAbtn = false;
        $scope.showEmpSignatureImage = true;
        $scope.showEmpSignaturePad = false;
        $scope.showPatSignatureImage = true;
        $scope.showPatSignaturePad = false;
      }
    }
  }
  $scope.edit = function () {
    $scope.disableEdit = false;
    $scope.enableInput = true;
    $scope.enableQALockBtn = false;
    $scope.enableQAEditBtn = false;
    $scope.enableQARejectBtn = false;
    $scope.enableCancelBtn = true;
    $scope.enableSaveBtn = true;
    $scope.editform = "yes";

  };
  $scope.cancel = function () {
    $scope.disableEdit = true;
    $scope.enableInput = false;
    $scope.enableQALockBtn = true;
    $scope.enableQAEditBtn = true;
    $scope.enableQARejectBtn = true;
    $scope.enableCancelBtn = false;
    $scope.enableSaveBtn = false;

    $scope.editform = "no";
  };
  $scope.reject = function () {

    var form = {
      KEYID: getFormValue.FORMID,
      STATUS: 4,

    };

    dataService.edit('form', form).then(function (response) {

      $state.go('qa');
    });
  };
  //qa approves form
  $scope.approve = function () {

    dataService.edit('form', {
      KEYID: getFormValue.FORMID,
      STATUS: 3
    }).then(function (response) {
      console.log('editing form', response);
      $state.go('qa');
    });
  }
  $scope.isQAModel();
  $scope.removeEmpSignature = function () {

    $scope.showPatSignaturePad = true;
    $scope.showEmpSignatureImage = false;
  };

});
