app.controller('ViewPatientConsentController', function($scope, $state, $filter, formService, dataService, patientService) {

  $scope.form=formService.get();
  console.log('$scope.form', $scope.form);

  $scope.patient=patientService.get();
  console.log('$scope.patient', $scope.patient);

  $scope.populateForm=function () {
    $scope.NOINFORMATIONRELEASEABOUT=$scope.form.patientConsent.noInformationReleaseAbout;
    $scope.NOINFORMATIONRELEASETO=$scope.form.patientConsent.noInformationReleaseTo;
    $scope.providedServicesNursing=$scope.form.patientConsent.providedServicesNursing;
    $scope.providedServicesCHHA=$scope.form.patientConsent.providedServicesCHHA;
    $scope.providedServicesPT=$scope.form.patientConsent.providedServicesPT;
    $scope.providedServicesOT=$scope.form.patientConsent.providedServicesOT;
    $scope.providedServicesST=$scope.form.patientConsent.providedServicesST;
    $scope.providedServicesDietician=$scope.form.patientConsent.providedServicesDietician;
    $scope.providedServicesMSW=$scope.form.patientConsent.providedServicesMSW;

    $scope.OTHERADVANCEDIRECTIVES=$scope.form.patientConsent.otherAdvanceDirectives;
    $scope.DONOTKNOWADVANCEDIRECTIVES=$scope.form.patientConsent.doNotKnowAdvanceDirectives;
    $scope.SIGNATURE=$scope.form.patientConsent.signature;
    $scope.ADDITIONALSIGNATURE=$scope.form.patientConsent.additionalSignature;
    $scope.COMPLETEDADVANCEDIRECTIVES=$scope.form.patientConsent.completedAdvanceDirectives;
    $scope.DURABLEPOWEROFATTORNEY=$scope.form.patientConsent.durablePowerOfAttorney;
    $scope.LIVINGWILL=$scope.form.patientConsent.livingWill;
    $scope.DONOTRESUSCITATE=$scope.form.patientConsent.doNotResuscitate;
    $scope.PHOTOGRAPH=$scope.form.patientConsent.photograph;

    //only populate date if exists
    if ($scope.form.patientConsent.signatureDate) {
      $scope.SIGNATUREDATE=new Date($filter('date')($scope.form.patientConsent.signatureDate, 'yyyy/MM/dd'));
    }
    if ($scope.form.patientConsent.additionalSignatureDate) {
      $scope.ADDITIONAL_SIGNATUREDATE=new Date($filter('date')($scope.form.patientConsent.additionalSignatureDate, 'yyyy/MM/dd'));
    }

    $scope.hotline=$scope.form.hotline;
    $scope.agency=$scope.form.agency;
  };

  $scope.edit=function () {
    // $scope.form.ACTION='QAEDIT';
    var form = {
      FORMID: $scope.form.FORMID,
      OBJKEYID: $scope.form.OBJKEYID,
      STATUS: $scope.form.STATUS,
      VISITID: $scope.form.VISITID,
      ACTION: 'QAEDIT',
    };
    formService.set(form);
    $state.go('patientConsent');
  };

  $scope.approve=function () {
    //edit form status to 3=approved;
    dataService.edit('form', {KEYID:$scope.form.FORMID, STATUS:3}).then(function (response) {
      console.log(response);
      $state.go('qa');
    });
  };

  $scope.reject=function () {
    //change form status to in progress, and attach note?
    var form={
      KEYID: $scope.form.FORMID,
      STATUS: 4,
    };
    dataService.edit('form', form).then(function (response) {
      console.log(response);
      $state.go('qa');
    });
  };



  $scope.populateForm();
});
