app.config(function($stateProvider) {
  $stateProvider.state('viewPatientConsent', {
    url: '/viewPatientConsent',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/viewPatientConsent/viewPatientConsent.html',
        controller: 'ViewPatientConsentController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
