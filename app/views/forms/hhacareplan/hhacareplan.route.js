app.config(function ($stateProvider) {
  $stateProvider.state('hhacareplan', {
    url: '/hhacareplan',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/hhacareplan/hhacareplan.html',
        controller: 'HHACareController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  })
})
