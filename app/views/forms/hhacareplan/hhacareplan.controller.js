app.controller('HHACareController', function ($scope, $http, $httpParamSerializerJQLike, $filter, formService, dataService, patientService, $anchorScroll, $timeout, CONSTANTS, $state) {
  $scope.PREVIOUS = function () {
    window.history.back();
  };
  $scope.showPatSignatureImage = false;
  $scope.showPatSignaturePad = true;
  $scope.form = {};
  $scope.form1 = {};
  $scope.hhacareplan = {};
  var getFormValue = formService.get(); // Getting values from services
  $scope.imgvalidate = false;
  $scope.imgvalidateerror = 1;
  $scope.getDetails = function () {

    if (getFormValue.STATUS == 0) {
      var formData = {
        formId: getFormValue.FORMID
      }
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['missedvisitSheet'].getAll,
        method: 'POST',
        data: $httpParamSerializerJQLike(formData),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }

      }).success(function (data) {

        $scope.hhacareplan.patient_name = data.PATKEYID;
        $scope.hhacareplan.patient_name_id = data.fn + ' ' + data.ln;
        //$scope.patient_dob = data.dob;
        $scope.hhacareplan.FORMID = data.formId;

      });
    }
  };
  $scope.getDetails();

  $scope.editform = 'no';
  $scope.saveHHACareplanSheet = function (form) {
    if ($scope.editform == 'no') {
      $scope.si = $scope.acceptPat().dataUrl;
    } else if ($scope.editform == 'yes') {
      $scope.si = $scope.empSignature;
    }
    if (typeof $scope.si == 'undefined' || $scope.si == '') {
      $scope.imgvalidate = true;
      $scope.imgvalidateerror = 0;

    } else {
      if ($scope.editform == 'no' && $scope.acceptPat().dataUrl == 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjgAAADcCAQAAADXNhPAAAACIklEQVR42u3UIQEAAAzDsM+/6UsYG0okFDQHMBIJAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcCQADAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDkQAwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAegeayZAN3dLgwnAAAAAElFTkSuQmCC') {

        $scope.imgvalidate = true;
        $scope.imgvalidateerror = 0;

      } else {

        $scope.imgvalidate = false;
        $scope.imgvalidateerror = 1;
      }

    }
    if (form.$valid && $scope.imgvalidateerror) { //&& !$scope.warning

      $scope.hhacareplan.visit_date = $filter("date")($scope.hhacareplan.visit_date, 'yyyy/MM/dd');
      $scope.hhacareplan.signature_date = $filter("date")($scope.hhacareplan.signature_date, 'yyyy/MM/dd');

      var newRouteSheet = {
        visitdata: $scope.hhacareplan,
        sign_phy: $scope.si,
        edit_form: $scope.editform,
        currentTableId: $scope.currenttableId

      };
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['hhacareSheet'].add,
        method: 'POST',
        data: $httpParamSerializerJQLike(newRouteSheet),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }

      }).success(function (response) {
        if (response.status === 'success') {
          $anchorScroll();
          $scope.successTextAlert = response.msg;
          $scope.showSuccessAlert = true;
          $timeout(function () {
            $state.go(response.goto);
          }, 2000);
        }
      });
    }
  };
  if (getFormValue.STATUS == 1) {



    $scope.form1.path = getFormValue.path;
    $scope.empSignature = $scope.form1.path;
    $scope.form.STATUS = 1;
    var visitdate = $filter("date")(getFormValue.signature_date, 'yyyy/MM/dd');
    $scope.form.SIGDATE = new Date(visitdate);
    var igDate = $filter("date")(getFormValue.visit_date, 'yyyy/MM/dd');
    $scope.form.VISITDATE = new Date(igDate);
    $scope.hhacareplan = getFormValue;
    $scope.hhacareplan.home_health_aide = getFormValue.home_health_aide;
    $scope.hhacareplan.patient_oriented_careplan = getFormValue.patient_oriented_careplan;
    /* Checkbox tick*/

    var saftyJson = JSON.parse(getFormValue.safety_precautions);
    $scope.hhacareplan.anticoagulant_precautions = saftyJson.anticoagulant_precautions;
    $scope.hhacareplan.emergency = saftyJson.emergency;
    $scope.hhacareplan.fall_precautions = saftyJson.fall_precautions;
    $scope.hhacareplan.pathway_clear = saftyJson.pathway_clear;
    $scope.hhacareplan.side_rails = saftyJson.side_rails;
    $scope.hhacareplan.neutropenic_precautions = saftyJson.neutropenic_precautions;
    $scope.hhacareplan.o2_precautions = saftyJson.o2_precautions;
    $scope.hhacareplan.proper_meals = saftyJson.proper_meals;
    $scope.hhacareplan.adls = saftyJson.adls;
    $scope.hhacareplan.seizure = saftyJson.seizure;
    $scope.hhacareplan.sharps_safety = saftyJson.sharps_safety;
    $scope.hhacareplan.slow_position_change = saftyJson.slow_position_change;
    $scope.hhacareplan.std_precautions = saftyJson.std_precautions;
    $scope.hhacareplan.support_transfer = saftyJson.support_transfer;
    $scope.hhacareplan.assistive_devices = saftyJson.assistive_devices;
    var functionalJson = JSON.parse(getFormValue.functional_data);
    $scope.hhacareplan.amputation = functionalJson.amputation;
    $scope.hhacareplan.bowel_incontinence = functionalJson.bowel_incontinence;
    $scope.hhacareplan.contracture = functionalJson.contracture;
    $scope.hhacareplan.hearing = functionalJson.hearing;
    $scope.hhacareplan.paralysis = functionalJson.paralysis;
    $scope.hhacareplan.endurance = functionalJson.endurance;
    $scope.hhacareplan.ambulation = functionalJson.ambulation;
    $scope.hhacareplan.speech = functionalJson.speech;
    $scope.hhacareplan.blind = functionalJson.blind;
    $scope.hhacareplan.dyspnea_exertion = functionalJson.dyspnea_exertion;
    var activeJson = JSON.parse(getFormValue.activities_data);
    $scope.hhacareplan.complete_bed_rest = activeJson.complete_bed_rest;
    $scope.hhacareplan.bed_rest_brp = activeJson.bed_rest_brp;
    $scope.hhacareplan.up_as_tolerated = activeJson.up_as_tolerated;
    $scope.hhacareplan.transfer_bed_chair = activeJson.transfer_bed_chair;
    $scope.hhacareplan.exercise_prescribed = activeJson.exercise_prescribed;
    $scope.hhacareplan.partial_weight_bearing = activeJson.partial_weight_bearing;
    $scope.hhacareplan.independent_at_home = activeJson.independent_at_home;
    $scope.hhacareplan.crutches = activeJson.crutches;
    $scope.hhacareplan.cane = activeJson.cane;
    $scope.hhacareplan.wheelchair = activeJson.wheelchair;
    $scope.hhacareplan.walker = activeJson.walker;
    var planJson = JSON.parse(getFormValue.plan_data);
    console.log(planJson);
    console.log('=-=-=-=-=-=-=' + planJson.temperature);
    $scope.hhacareplan.temperature = planJson.temperature;
    $scope.hhacareplan.blood_pressure = planJson.blood_pressure;
    $scope.hhacareplan.heart_rate = planJson.heart_rate;
    $scope.hhacareplan.respirations = planJson.respirations;
    $scope.hhacareplan.weight = planJson.weight;
    $scope.hhacareplan.assist_bed = planJson.assist_bed;
    $scope.hhacareplan.assist_bsc = planJson.assist_bsc;
    $scope.hhacareplan.incontinence_care = planJson.incontinence_care;
    $scope.hhacareplan.drainage_bag = planJson.drainage_bag;
    $scope.hhacareplan.bowel_movement = planJson.bowel_movement;
    $scope.hhacareplan.catheter_care = planJson.catheter_care;
    $scope.hhacareplan.bed_bath = planJson.bed_bath;
    $scope.hhacareplan.chair_bath = planJson.chair_bath;
    $scope.hhacareplan.tub_bath = planJson.tub_bath;
    $scope.hhacareplan.shower = planJson.shower;
    $scope.hhacareplan.shower_chair = planJson.shower_chair;
    $scope.hhacareplan.shampoo_hair = planJson.shampoo_hair;
    $scope.hhacareplan.hair_care = planJson.hair_care;
    $scope.hhacareplan.oral_care = planJson.oral_care;
    $scope.hhacareplan.skin_care = planJson.skin_care;
    $scope.hhacareplan.pericare = planJson.pericare;
    $scope.hhacareplan.nail_care = planJson.nail_care;
    $scope.hhacareplan.shave = planJson.shave;
    $scope.hhacareplan.assist_dressing = planJson.assist_dressing;
    $scope.hhacareplan.medication_reminder = planJson.medication_reminder;
    $scope.hhacareplan.dangle_bed = planJson.dangle_bed;
    $scope.hhacareplan.turn_position = planJson.turn_position;
    $scope.hhacareplan.assist_transfer = planJson.assist_transfer;
    $scope.hhacareplan.range_motion = planJson.range_motion;
    $scope.hhacareplan.assist_ambulation = planJson.assist_ambulation;
    $scope.hhacareplan.equipment_care = planJson.equipment_care;
    $scope.hhacareplan.make_bed = planJson.make_bed;
    $scope.hhacareplan.change_linen = planJson.change_linen;
    $scope.hhacareplan.light_housekeeping = planJson.light_housekeeping;
    $scope.hhacareplan.meal = planJson.meal;
    $scope.hhacareplan.assist_feeding = planJson.assist_feeding;
    //$scope.hhacareplan.temperature = planJson.temperature;



    /* End*/
    $scope.hhacareplan.activites_other_comments = getFormValue.activities_comments;
    $scope.hhacareplan.functional_others_comments = getFormValue.functional_comments;
    $scope.hhacareplan.other_comments = getFormValue.safety_comments;
    $scope.hhacareplan.patient_name = getFormValue.PATKEYID;
    $scope.hhacareplan.patient_name_id = getFormValue.FNAME + ' ' + getFormValue.LNAME;
    $scope.hhacareplan.formId = getFormValue.FORMID;

  }
  $scope.isQAMode = function () {
    if (getFormValue.ACTION === 'QAREVIEW') {
      $scope.currenttableId = getFormValue.formPrimaryId;
      $scope.approvedMsg = false; //form is not locked, allow editing
      $scope.enableQAEditBtn = true;
      $scope.enableQALockBtn = true;
      $scope.enableQARejectBtn = true;
      $scope.submitQAbtn = false;
      $scope.disableEdit = true;
      $scope.enableInput = false;
      $scope.showEmpSignatureImage = true;
      $scope.showEmpSignaturePad = false;
      $scope.showPatSignatureImage = true;
      $scope.showPatSignaturePad = false;
      var visitdate = $filter("date")(getFormValue.signature_date, 'yyyy/MM/dd');
      $scope.form.SIDATE = new Date(visitdate);
      var igDate = $filter("date")(getFormValue.visit_date, 'yyyy/MM/dd');
      $scope.form.VIDATE = new Date(igDate);
    } else {
      if ((getFormValue.STATUS === 1) || (getFormValue.STATUS === 3)) {
        $scope.disableEdit = true;
        $scope.enableInput = false;
        $scope.submitQAbtn = false;
        $scope.showEmpSignatureImage = true;
        $scope.showEmpSignaturePad = false;
        $scope.showPatSignatureImage = true;
        $scope.showPatSignaturePad = false;
      }
    }
  }
  //qa editing
  $scope.edit = function () {
    $scope.disableEdit = false;
    $scope.enableInput = true;
    $scope.enableQALockBtn = false;
    $scope.enableQAEditBtn = false;
    $scope.enableQARejectBtn = false;
    $scope.enableCancelBtn = true;
    $scope.enableSaveBtn = true;
    $scope.editform = "yes";

  };
  $scope.removeEmpSignature = function () {
    $scope.showPatSignaturePad = true;
    $scope.showEmpSignatureImage = false;
  };
  //cancel qa editing
  $scope.cancel = function () {
    $scope.disableEdit = true;
    $scope.enableInput = false;
    $scope.enableQALockBtn = true;
    $scope.enableQAEditBtn = true;
    $scope.enableQARejectBtn = true;
    $scope.enableCancelBtn = false;
    $scope.enableSaveBtn = false;
    $scope.getRouteSheet(); //get original route sheet info
    $scope.editform = "no";
  };
  $scope.reject = function () {

    var form = {
      KEYID: getFormValue.FORMID,
      STATUS: 4,

    };

    dataService.edit('form', form).then(function (response) {

      $state.go('qa');
    });
  };
  //qa approves form
  $scope.approve = function () {

    dataService.edit('form', {
      KEYID: getFormValue.FORMID,
      STATUS: 3
    }).then(function (response) {
      console.log('editing form', response);
      $state.go('qa');
    });
  }
  $scope.isQAMode();

});
