app.controller('HHAController', function ($scope, $http, $httpParamSerializerJQLike, $filter, formService, dataService, patientService, $anchorScroll, $timeout, CONSTANTS, $state) {
  $scope.PREVIOUS = function () {
    window.history.back();
  };
  $scope.imgvalidate = false;
  $scope.imgvalidateerror = 1;
  $scope.showPatSignatureImage = false;
  $scope.showPatSignaturePad = true;
  $scope.form = {};
  $scope.form1 = {};
  $scope.hha = {};
  var getFormValue = formService.get(); // Getting values from services
  $scope.getDetails = function () {

    if (getFormValue.STATUS == 0) {
      var formData = {
        formId: getFormValue.FORMID
      }
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['missedvisitSheet'].getAll,
        method: 'POST',
        data: $httpParamSerializerJQLike(formData),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }

      }).success(function (data) {

        $scope.hha.patient_name = data.PATKEYID;
        $scope.hha.patient_name_id = data.fn + ' ' + data.ln;

        $scope.hha.FORMID = data.formId;

      });
    }
  };
  $scope.getDetails();

  $scope.editform = 'no';
  $scope.saveHHASheet = function (form) {
    var radiosize = 0;


    if ($scope.editform == 'no') {
      $scope.si = $scope.acceptPat().dataUrl;
    } else if ($scope.editform == 'yes') {
      $scope.si = $scope.empSignature;
    }
    if (typeof $scope.si == 'undefined' || $scope.si == '') {
      $scope.imgvalidate = true;
      $scope.imgvalidateerror = 0;

    } else {
      if ($scope.editform == 'no' && $scope.acceptPat().dataUrl == 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjgAAADcCAQAAADXNhPAAAACIklEQVR42u3UIQEAAAzDsM+/6UsYG0okFDQHMBIJAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcCQADAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDkQAwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAegeayZAN3dLgwnAAAAAElFTkSuQmCC') {

        $scope.imgvalidate = true;
        $scope.imgvalidateerror = 0;

      } else {

        $scope.imgvalidate = false;
        $scope.imgvalidateerror = 1;
      }

    }
    if (form.$valid && $scope.imgvalidateerror) { //&& !$scope.warning && radiosize == 7
      $scope.hha.visit_date = $filter("date")($scope.hha.visit_date, 'yyyy/MM/dd');
      $scope.hha.signature_date = $filter("date")($scope.hha.signature_date, 'yyyy/MM/dd');
      var newRouteSheet = {
        visitdata: $scope.hha,
        sign_phy: $scope.si,
        edit_form: $scope.editform,
        currentTableId: $scope.currenttableId

      };
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['hhaSheet'].add,
        method: 'POST',
        data: $httpParamSerializerJQLike(newRouteSheet),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }

      }).success(function (response) {
        if (response.status === 'success') {
          $anchorScroll();
          $scope.successTextAlert = response.msg;
          $scope.showSuccessAlert = true;
          $timeout(function () {
            $state.go(response.goto);
          }, 2000);
        }
      });
    } else {
      if (radiosize != 7) {
        $scope.radioErr = true;
      }
      $anchorScroll();
      return false;
    }
  };
  if (getFormValue.STATUS == 1) {



    $scope.form1.signature_path = getFormValue.signature_path;
    $scope.empSignature = $scope.form1.signature_path;
    $scope.form.STATUS = 1;
    var visitdate = $filter("date")(getFormValue.signature_date, 'yyyy/MM/dd');
    $scope.form.SIDATE = new Date(visitdate);
    var igDate = $filter("date")(getFormValue.visit_date, 'yyyy/MM/dd');
    $scope.form.VIDATE = new Date(igDate);
    $scope.hha = getFormValue;
    $scope.hha.patient_name = getFormValue.PATKEYID;
    $scope.hha.patient_name_id = getFormValue.FNAME + ' ' + getFormValue.LNAME;
    $scope.hha.formId = getFormValue.FORMID;

  }
  $scope.isQAMode = function () {
    if (getFormValue.ACTION === 'QAREVIEW') {
      $scope.currenttableId = getFormValue.formPrimaryId;
      $scope.approvedMsg = false; //form is not locked, allow editing
      $scope.enableQAEditBtn = true;
      $scope.enableQALockBtn = true;
      $scope.enableQARejectBtn = true;
      $scope.submitQAbtn = false;
      $scope.disableEdit = true;
      $scope.enableInput = false;
      $scope.showEmpSignatureImage = true;
      $scope.showEmpSignaturePad = false;
      $scope.showPatSignatureImage = true;
      $scope.showPatSignaturePad = false;
      var visitdate = $filter("date")(getFormValue.signature_date, 'yyyy/MM/dd');
      $scope.form.SIDATE = new Date(visitdate);
      var igDate = $filter("date")(getFormValue.visit_date, 'yyyy/MM/dd');
      $scope.form.VIDATE = new Date(igDate);
    } else {
      if ((getFormValue.STATUS === 1) || (getFormValue.STATUS === 3)) {
        $scope.disableEdit = true;
        $scope.enableInput = false;
        $scope.submitQAbtn = false;
        $scope.showEmpSignatureImage = true;
        $scope.showEmpSignaturePad = false;
        $scope.showPatSignatureImage = true;
        $scope.showPatSignaturePad = false;
      }
    }
  }
  //qa editing
  $scope.edit = function () {
    $scope.disableEdit = false;
    $scope.enableInput = true;
    $scope.enableQALockBtn = false;
    $scope.enableQAEditBtn = false;
    $scope.enableQARejectBtn = false;
    $scope.enableCancelBtn = true;
    $scope.enableSaveBtn = true;
    $scope.editform = "yes";

  };

  //cancel qa editing
  $scope.cancel = function () {
    $scope.disableEdit = true;
    $scope.enableInput = false;
    $scope.enableQALockBtn = true;
    $scope.enableQAEditBtn = true;
    $scope.enableQARejectBtn = true;
    $scope.enableCancelBtn = false;
    $scope.enableSaveBtn = false;
    $scope.getRouteSheet(); //get original route sheet info
    $scope.editform = "no";
  };
  $scope.removeEmpSignature = function () {
    $scope.showPatSignaturePad = true;
    $scope.showEmpSignatureImage = false;
  };
  $scope.reject = function () {

    var form = {
      KEYID: getFormValue.FORMID,
      STATUS: 4,

    };

    dataService.edit('form', form).then(function (response) {

      $state.go('qa');
    });
  };
  //qa approves form
  $scope.approve = function () {

    dataService.edit('form', {
      KEYID: getFormValue.FORMID,
      STATUS: 3
    }).then(function (response) {
      console.log('editing form', response);
      $state.go('qa');
    });
  }
  $scope.isQAMode();

});
