app.config(function ($stateProvider) {
  $stateProvider.state('hha', {
    url: '/hha',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/hha/hha.html',
        controller: 'HHAController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  })
})
