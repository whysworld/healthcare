app.controller('ResumptionOfCareController', function($scope, $filter, $state, $http,$uibModal,  CONSTANTS, $httpParamSerializerJQLike, patientService, dataService, formService, agencyService) {

  $scope.patient = patientService.get();

  $scope.form = formService.get();
  console.log($scope.form);

  $scope.currentForm = 'General';

  $scope.nextForm = function(current, destination) {
    if ($scope.currentForm != destination) {
      $scope.currentForm = destination;
      $('.' + current ).fadeOut('fast', function() {
        $('.' + destination ).fadeIn('fast');
      });
    }
  };

  $scope.activeForm = function(form) {
    if ($scope.currentForm == form) {
      return true;
    }
  };

  $scope.submitSection = function(form, destination) {
    console.log(form.$submitted);
    console.log(form.$valid);
    if(form.$valid) {
      $('#' + $scope.currentForm + ' i').css({color: 'green'});
      var section = $scope.currentForm + 'Complete';

      $scope.nextForm($scope.currentForm, destination);
      console.log(section);
      $scope[section] = true;
      console.log($scope[section]);
    }
    else {
      $('#' + $scope.currentForm + ' i').css({color: 'lightgrey'});
      console.log('Not Valid');
    }
  };

  //populating field per visit date
  $scope.changeVisitDate=false;

  //alerting msg if M0906 date is changed and different from form date
  $scope.checkDate=function () {
    if ($filter("date")($scope.M0032_ROC_DT, 'yyyy/MM/dd') !==  $filter("date")($scope.form.VISITDATE, 'yyyy/MM/dd')) {
      alert("Note: Changing M0032 will automatically change date of visit.");
      $scope.changeVisitDate=true;
    } else {
      $scope.changeVisitDate=false;
    }
  };

  //get form obj if exists
  dataService.get('oasisC2_2_20_1', {FORMID: $scope.form.FORMID}).then(function(data){
    console.log(data);
    if(data.data.length>0){
      var oasis = data.data[0];

      var INPArr=[
        oasis.M1011_14_DAY_INP1_ICD,
        oasis.M1011_14_DAY_INP2_ICD,
        oasis.M1011_14_DAY_INP3_ICD,
        oasis.M1011_14_DAY_INP4_ICD,
        oasis.M1011_14_DAY_INP5_ICD,
        oasis.M1011_14_DAY_INP6_ICD
      ];
      function getM1011desc(i) {
        dataService.get('ICD10Code', {CODE:code }).then(function (data) {
          var codeDesc=data.data[0].DESCRIPTION;
          $scope['M1011_' + (i+1)]=codeDesc;
        });
      }
      for (var i = 0; i < INPArr.length; i++) {
        if (INPArr[i]!==undefined && INPArr[i]!=="" && INPArr[i]!==null) {
          var code=INPArr[i].replace('.', '');
          getM1011desc(i)
        }
      }


      var chgregArr=[
        oasis.M1017_CHGREG_ICD1,
        oasis.M1017_CHGREG_ICD2,
        oasis.M1017_CHGREG_ICD3,
        oasis.M1017_CHGREG_ICD4,
        oasis.M1017_CHGREG_ICD5,
        oasis.M1017_CHGREG_ICD6,
      ];
      function getM1017desc(i) {
        dataService.get('ICD10Code', {CODE:code }).then(function (data) {
          var codeDesc=data.data[0].DESCRIPTION;
          $scope['M1017_' + (i+1)]=codeDesc;
        });
      }
      for (var i = 0; i < chgregArr.length; i++) {
        if (chgregArr[i]!==undefined && chgregArr[i]!=="" && chgregArr[i]!==null) {
          var code=chgregArr[i].replace('.', '');
          getM1017desc(i)
        }
      }
      var m1021Arr=[
        oasis.M1021_PRIMARY_DIAG_ICD,
      ];
      function getM1021desc(i) {
        dataService.get('ICD10Code', {CODE:code }).then(function (data) {
          var codeDesc=data.data[0].DESCRIPTION;
          $scope['M1021']=codeDesc;
        });
      }
      for (var i = 0; i < m1021Arr.length; i++) {
        if (m1021Arr[i]!==undefined && m1021Arr[i]!=="" && m1021Arr[i]!==null) {
          var code=m1021Arr[i].replace('.', '');
          getM1021desc(i)
        }
      }
      var m1023Arr=[
        oasis.M1023_OTH_DIAG1_ICD,
        oasis.M1023_OTH_DIAG2_ICD,
        oasis.M1023_OTH_DIAG3_ICD,
        oasis.M1023_OTH_DIAG4_ICD,
        oasis.M1023_OTH_DIAG5_ICD,
      ];
      function getM1023desc(i) {
        dataService.get('ICD10Code', {CODE:code }).then(function (data) {
          var codeDesc=data.data[0].DESCRIPTION;
          $scope['M1023_' + (i+1)]=codeDesc;
        });
      }
      for (var i = 0; i < m1023Arr.length; i++) {
        if (m1023Arr[i]!==undefined && m1023Arr[i]!=="" && m1023Arr[i]!==null) {
          var code=m1023Arr[i].replace('.', '');
          getM1023desc(i)
        }
      }
      var m1025aArr=[
        oasis.M1025_OPT_DIAG_ICD_A3,
        oasis.M1025_OPT_DIAG_ICD_A4,
      ];
      function getM1025adesc(i) {
        dataService.get('ICD10Code', {CODE:code }).then(function (data) {
          var codeDesc=data.data[0].DESCRIPTION;
          $scope['M1025A_' + (i+3)]=codeDesc;
        });
      }
      for (var i = 0; i < m1025aArr.length; i++) {
        if (m1025aArr[i]!==undefined && m1025aArr[i]!=="" && m1025aArr[i]!==null) {
          var code=m1025aArr[i].replace('.', '');
          getM1025adesc(i)
        }
      }
      var m1025bArr=[
        oasis.M1025_OPT_DIAG_ICD_B3,
        oasis.M1025_OPT_DIAG_ICD_B4,
      ];
      function getM1025Bdesc(i) {
        dataService.get('ICD10Code', {CODE:code }).then(function (data) {
          var codeDesc=data.data[0].DESCRIPTION;
          $scope['M1025B_' + (i+3)]=codeDesc;
        });
      }
      for (var i = 0; i < m1025aArr.length; i++) {
        if (m1025bArr[i]!==undefined && m1025bArr[i]!=="" && m1025bArr[i]!==null) {
          var code=m1025bArr[i].replace('.', '');
          getM1025Bdesc(i)
        }
      }
      var m1025cArr=[
        oasis.M1025_OPT_DIAG_ICD_C3,
        oasis.M1025_OPT_DIAG_ICD_C4,
      ];
      function getM1025Cdesc(i) {
        dataService.get('ICD10Code', {CODE:code }).then(function (data) {
          var codeDesc=data.data[0].DESCRIPTION;
          $scope['M1025C_' + (i+3)]=codeDesc;
        });
      }
      for (var i = 0; i < m1025aArr.length; i++) {
        if (m1025cArr[i]!==undefined && m1025cArr[i]!=="" && m1025cArr[i]!==null) {
          var code=m1025cArr[i].replace('.', '');
          getM1025Cdesc(i)
        }
      }
      var m1025dArr=[
        oasis.M1025_OPT_DIAG_ICD_D3,
        oasis.M1025_OPT_DIAG_ICD_D4,
      ];
      function getM1025Ddesc(i) {
        dataService.get('ICD10Code', {CODE:code }).then(function (data) {
          var codeDesc=data.data[0].DESCRIPTION;
          $scope['M1025D_' + (i+3)]=codeDesc;
        });
      }
      for (var i = 0; i < m1025aArr.length; i++) {
        if (m1025dArr[i]!==undefined && m1025dArr[i]!=="" && m1025dArr[i]!==null) {
          var code=m1025dArr[i].replace('.', '');
          getM1025Ddesc(i)
        }
      }
      var m1025eArr=[
        oasis.M1025_OPT_DIAG_ICD_E3,
        oasis.M1025_OPT_DIAG_ICD_E4,
      ];
      function getM1025Edesc(i) {
        dataService.get('ICD10Code', {CODE:code }).then(function (data) {
          var codeDesc=data.data[0].DESCRIPTION;
          $scope['M1025E_' + (i+3)]=codeDesc;
        });
      }
      for (var i = 0; i < m1025aArr.length; i++) {
        if (m1025eArr[i]!==undefined && m1025eArr[i]!=="" && m1025eArr[i]!==null) {
          var code=m1025eArr[i].replace('.', '');
          getM1025Edesc(i)
        }
      }
      var m1025fArr=[
        oasis.M1025_OPT_DIAG_ICD_F3,
        oasis.M1025_OPT_DIAG_ICD_F4,
      ];
      function getM1025Fdesc(i) {
        dataService.get('ICD10Code', {CODE:code }).then(function (data) {
          var codeDesc=data.data[0].DESCRIPTION;
          $scope['M1025F_' + (i+3)]=codeDesc;
        });
      }
      for (var i = 0; i < m1025aArr.length; i++) {
        if (m1025fArr[i]!==undefined && m1025fArr[i]!=="" && m1025fArr[i]!==null) {
          var code=m1025fArr[i].replace('.', '');
          getM1025Fdesc(i)
        }
      }
      console.log(oasis);
      //******** GENERAL ********//
      $scope.M0010_CCN=oasis.M0010_CCN;
      $scope.M0014_BRANCH_STATE=oasis.M0014_BRANCH_STATE;
      $scope.M0016_BRANCH_ID=oasis.M0016_BRANCH_ID;
      $scope.M0018_PHYSICIAN_ID=oasis.M0018_PHYSICIAN_ID;
      $scope.M0018_PHYSICIAN_UK=oasis.M0018_PHYSICIAN_UK;
      $scope.M0020_PAT_ID=oasis.M0020_PAT_ID;
      $scope.M0030_START_CARE_DT=new Date($filter("date")(oasis.M0030_START_CARE_DT, 'yyyy/MM/dd'));
      $scope.M0032_ROC_DT=new Date($filter("date")(oasis.M0032_ROC_DT, 'yyyy/MM/dd'));
      $scope.M0032_ROC_DT_NA=oasis.M0032_ROC_DT_NA;
      $scope.M0040_PAT_FNAME=oasis.M0040_PAT_FNAME;
      $scope.M0040_PAT_MI=oasis.M0040_PAT_MI;
      $scope.M0040_PAT_LNAME=oasis.M0040_PAT_LNAME;
      $scope.M0040_PAT_SUFFIX=oasis.M0040_PAT_SUFFIX;
      $scope.M0050_PAT_ST=oasis.M0050_PAT_ST;
      $scope.M0060_PAT_ZIP=oasis.M0060_PAT_ZIP;
      $scope.M0063_MEDICARE_NUM=oasis.M0063_MEDICARE_NUM;
      $scope.M0063_MEDICARE_NA=oasis.M0063_MEDICARE_NA;
      $scope.M0064_SSN=oasis.M0064_SSN;
      $scope.M0064_SSN_UK=oasis.M0064_SSN_UK;
      $scope.M0065_MEDICAID_NUM=oasis.M0065_MEDICAID_NUM;
      $scope.M0065_MEDICAID_NA=oasis.M0065_MEDICAID_NA;
      $scope.M0066_PAT_BIRTH_DT=new Date($filter("date")(oasis.M0066_PAT_BIRTH_DT, 'yyyy/MM/dd'));
      $scope.M0069_PAT_GENDER=oasis.M0069_PAT_GENDER;
      $scope.M0090_INFO_COMPLETED_DT=new Date($filter("date")(oasis.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd'));
      $scope.M0140_ETHNIC_AI_AN=oasis.M0140_ETHNIC_AI_AN;
      $scope.M0140_ETHNIC_ASIAN=oasis.M0140_ETHNIC_ASIAN;
      $scope.M0140_ETHNIC_BLACK=oasis.M0140_ETHNIC_BLACK;
      $scope.M0140_ETHNIC_HISP=oasis.M0140_ETHNIC_HISP;
      $scope.M0140_ETHNIC_NH_PI=oasis.M0140_ETHNIC_NH_PI;
      $scope.M0140_ETHNIC_WHITE=oasis.M0140_ETHNIC_WHITE;
      $scope.M0150_CPAY_NONE=oasis.M0150_CPAY_NONE;
      $scope.M0150_CPAY_MCARE_FFS=oasis.M0150_CPAY_MCARE_FFS;
      $scope.M0150_CPAY_MCARE_HMO=oasis.M0150_CPAY_MCARE_HMO;
      $scope.M0150_CPAY_MCAID_FFS=oasis.M0150_CPAY_MCAID_FFS;
      $scope.M0150_CPAY_MCAID_HMO=oasis.M0150_CPAY_MCAID_HMO;
      $scope.M0150_CPAY_WRKCOMP=oasis.M0150_CPAY_WRKCOMP;
      $scope.M0150_CPAY_TITLEPGMS=oasis.M0150_CPAY_TITLEPGMS;
      $scope.M0150_CPAY_OTH_GOVT=oasis.M0150_CPAY_OTH_GOVT;
      $scope.M0150_CPAY_PRIV_INS=oasis.M0150_CPAY_PRIV_INS;
      $scope.M0150_CPAY_PRIV_HMO=oasis.M0150_CPAY_PRIV_HMO;
      $scope.M0150_CPAY_SELFPAY=oasis.M0150_CPAY_SELFPAY;
      $scope.M0150_CPAY_OTHER=oasis.M0150_CPAY_OTHER;
      $scope.M0150_CPAY_UK=oasis.M0150_CPAY_UK;
      //clinical record
      $scope.M0080_ASSESSOR_DISCIPLINE=oasis.M0080_ASSESSOR_DISCIPLINE;
      $scope.M0090_INFO_COMPLETED_DT=oasis.M0090_INFO_COMPLETED_DT;
      $scope.M0100_ASSMT_REASON=oasis.M0100_ASSMT_REASON;
      $scope.M0102_PHYSN_ORDRD_SOCROC_DT=oasis.M0102_PHYSN_ORDRD_SOCROC_DT;
      $scope.M0102_PHYSN_ORDRD_SOCROC_DT_NA=oasis.M0102_PHYSN_ORDRD_SOCROC_DT_NA;
      $scope.M0104_PHYSN_RFRL_DT=oasis.M0104_PHYSN_RFRL_DT;
      $scope.M0110_EPISODE_TIMING=oasis.M0110_EPISODE_TIMING;
      //patient history
      $scope.M1000_DC_LTC_14_DA=oasis.M1000_DC_LTC_14_DA;
      $scope.M1000_DC_SNF_14_DA=oasis.M1000_DC_SNF_14_DA;
      $scope.M1000_DC_IPPS_14_DA=oasis.M1000_DC_IPPS_14_DA;
      $scope.M1000_DC_LTCH_14_DA=oasis.M1000_DC_LTCH_14_DA;
      $scope.M1000_DC_IRF_14_DA=oasis.M1000_DC_IRF_14_DA;
      $scope.M1000_DC_PSYCH_14_DA=oasis.M1000_DC_PSYCH_14_DA;
      $scope.M1000_DC_OTH_14_DA=oasis.M1000_DC_OTH_14_DA;
      $scope.M1000_DC_NONE_14_DA=oasis.M1000_DC_NONE_14_DA;
      $scope.M1005_INP_DISCHARGE_DT=oasis.M1005_INP_DISCHARGE_DT;
      $scope.M1005_INP_DSCHG_UNKNOWN=oasis.M1005_INP_DSCHG_UNKNOWN;
      $scope.M1011_14_DAY_INP1_ICD=oasis.M1011_14_DAY_INP1_ICD;
      $scope.M1011_14_DAY_INP2_ICD=oasis.M1011_14_DAY_INP2_ICD;
      $scope.M1011_14_DAY_INP3_ICD=oasis.M1011_14_DAY_INP3_ICD;
      $scope.M1011_14_DAY_INP4_ICD=oasis.M1011_14_DAY_INP4_ICD;
      $scope.M1011_14_DAY_INP5_ICD=oasis.M1011_14_DAY_INP5_ICD;
      $scope.M1011_14_DAY_INP6_ICD=oasis.M1011_14_DAY_INP6_ICD;
      $scope.M1011_14_DAY_INP_NA=oasis.M1011_14_DAY_INP_NA;
      $scope.M1017_CHGREG_ICD1=oasis.M1017_CHGREG_ICD1;
      $scope.M1017_CHGREG_ICD2=oasis.M1017_CHGREG_ICD2;
      $scope.M1017_CHGREG_ICD3=oasis.M1017_CHGREG_ICD3;
      $scope.M1017_CHGREG_ICD5=oasis.M1017_CHGREG_ICD5;
      $scope.M1017_CHGREG_ICD6=oasis.M1017_CHGREG_ICD6;
      $scope.M1017_CHGREG_ICD_NA=oasis.M1017_CHGREG_ICD_NA;
      $scope.M1018_PRIOR_UR_INCON=oasis.M1018_PRIOR_UR_INCON;
      $scope.M1018_PRIOR_CATH=oasis.M1018_PRIOR_CATH;
      $scope.M1018_PRIOR_INTRACT_PAIN=oasis.M1018_PRIOR_INTRACT_PAIN ;
      $scope.M1018_PRIOR_IMPR_DECSN=oasis.M1018_PRIOR_IMPR_DECSN;
      $scope.M1018_PRIOR_DISRUPTIVE=oasis.M1018_PRIOR_DISRUPTIVE;
      $scope.M1018_PRIOR_MEM_LOSS=oasis.M1018_PRIOR_MEM_LOSS;
      $scope.M1018_PRIOR_NONE=oasis.M1018_PRIOR_NONE;
      $scope.M1018_PRIOR_NOCHG_14D=oasis.M1018_PRIOR_NOCHG_14D;
      $scope.M1018_PRIOR_UNKNOWN=oasis.M1018_PRIOR_UNKNOWN;
      $scope.M1021_PRIMARY_DIAG_ICD=oasis.M1021_PRIMARY_DIAG_ICD;
      $scope.M1021_PRIMARY_DIAG_SEVERITY=oasis.M1021_PRIMARY_DIAG_SEVERITY;
      $scope.M1023_OTH_DIAG1_ICD=oasis.M1023_OTH_DIAG1_ICD;
      $scope.M1023_OTH_DIAG1_SEVERITY=oasis.M1023_OTH_DIAG1_SEVERITY;
      $scope.M1023_OTH_DIAG2_ICD=oasis.M1023_OTH_DIAG2_ICD;
      $scope.M1023_OTH_DIAG2_SEVERITY=oasis.M1023_OTH_DIAG2_SEVERITY;
      $scope.M1023_OTH_DIAG3_ICD=oasis.M1023_OTH_DIAG3_ICD;
      $scope.M1023_OTH_DIAG3_SEVERITY=oasis.M1023_OTH_DIAG3_SEVERITY;
      $scope.M1023_OTH_DIAG4_ICD=oasis.M1023_OTH_DIAG4_ICD;
      $scope.M1023_OTH_DIAG4_SEVERITY=oasis.M1023_OTH_DIAG4_SEVERITY;
      $scope.M1023_OTH_DIAG5_ICD=oasis.M1023_OTH_DIAG5_ICD;
      $scope.M1023_OTH_DIAG5_SEVERITY=oasis.M1023_OTH_DIAG5_SEVERITY;
      $scope.M1025_OPT_DIAG_ICD_A3=oasis.M1025_OPT_DIAG_ICD_A3;
      $scope.M1025_OPT_DIAG_ICD_A4=oasis.M1025_OPT_DIAG_ICD_A4;
      $scope.M1025_OPT_DIAG_ICD_B3=oasis.M1025_OPT_DIAG_ICD_B3;
      $scope.M1025_OPT_DIAG_ICD_B4=oasis.M1025_OPT_DIAG_ICD_B4;
      $scope.M1025_OPT_DIAG_ICD_C3=oasis.M1025_OPT_DIAG_ICD_C3;
      $scope.M1025_OPT_DIAG_ICD_C4=oasis.M1025_OPT_DIAG_ICD_C4;
      $scope.M1025_OPT_DIAG_ICD_D3=oasis.M1025_OPT_DIAG_ICD_D3;
      $scope.M1025_OPT_DIAG_ICD_D4=oasis.M1025_OPT_DIAG_ICD_D4;
      $scope.M1025_OPT_DIAG_ICD_E3=oasis.M1025_OPT_DIAG_ICD_E3;
      $scope.M1025_OPT_DIAG_ICD_E4=oasis.M1025_OPT_DIAG_ICD_E4;
      $scope.M1025_OPT_DIAG_ICD_F3=oasis.M1025_OPT_DIAG_ICD_F3;
      $scope.M1025_OPT_DIAG_ICD_F4=oasis.M1025_OPT_DIAG_ICD_F4;
      $scope.M1028_ACTV_DIAG_PVD_PAD=oasis.M1028_ACTV_DIAG_PVD_PAD;
      $scope.M1028_ACTV_DIAG_DM=oasis.M1028_ACTV_DIAG_DM;
      $scope.M1030_THH_IV_INFUSION=oasis.M1030_THH_IV_INFUSION;
      $scope.M1030_THH_PAR_NUTRITION=oasis.M1030_THH_PAR_NUTRITION;
      $scope.M1030_THH_ENT_NUTRITION=oasis.M1030_THH_ENT_NUTRITION;
      $scope.M1030_THH_NONE_ABOVE=oasis.M1030_THH_NONE_ABOVE;
      $scope.M1033_HOSP_RISK_HSTRY_FALLS=oasis.M1033_HOSP_RISK_HSTRY_FALLS;
      $scope.M1033_HOSP_RISK_WEIGHT_LOSS=oasis.M1033_HOSP_RISK_WEIGHT_LOSS;
      $scope.M1033_HOSP_RISK_MLTPL_HOSPZTN=oasis.M1033_HOSP_RISK_MLTPL_HOSPZTN;
      $scope.M1033_HOSP_RISK_MLTPL_ED_VISIT=oasis.M1033_HOSP_RISK_MLTPL_ED_VISIT;
      $scope.M1033_HOSP_RISK_MNTL_BHV_DCLN=oasis.M1033_HOSP_RISK_MNTL_BHV_DCLN;
      $scope.M1033_HOSP_RISK_COMPLIANCE=oasis.M1033_HOSP_RISK_COMPLIANCE;
      $scope.M1033_HOSP_RISK_5PLUS_MDCTN=oasis.M1033_HOSP_RISK_5PLUS_MDCTN;
      $scope.M1033_HOSP_RISK_CRNT_EXHSTN=oasis.M1033_HOSP_RISK_CRNT_EXHSTN;
      $scope.M1033_HOSP_RISK_OTHR_RISK=oasis.M1033_HOSP_RISK_OTHR_RISK;
      $scope.M1033_HOSP_RISK_NONE_ABOVE=oasis.M1033_HOSP_RISK_NONE_ABOVE;
      $scope.M1034_PTNT_OVRAL_STUS=oasis.M1034_PTNT_OVRAL_STUS;
      $scope.M1036_RSK_SMOKING=oasis.M1036_RSK_SMOKING;
      $scope.M1036_RSK_OBESITY=oasis.M1036_RSK_OBESITY;
      $scope.M1036_RSK_ALCOHOLISM=oasis.M1036_RSK_ALCOHOLISM;
      $scope.M1036_RSK_DRUGS=oasis.M1036_RSK_DRUGS;
      $scope.M1036_RSK_NONE=oasis.M1036_RSK_NONE;
      $scope.M1036_RSK_UNKNOWN=oasis.M1036_RSK_UNKNOWN;
      $scope.M1060_HEIGHT_A=oasis.M1060_HEIGHT_A;
      $scope.M1060_WEIGHT_B=oasis.M1060_WEIGHT_B;
      //living situation
      $scope.M1100_PTNT_LVG_STUTN=oasis.M1100_PTNT_LVG_STUTN;
      //sensory
      $scope.M1200_VISION=oasis.M1200_VISION;
      $scope.M1210_HEARG_ABLTY=oasis.M1210_HEARG_ABLTY;
      $scope.M1220_UNDRSTG_VERBAL_CNTNT=oasis.M1220_UNDRSTG_VERBAL_CNTNT;
      $scope.M1230_SPEECH=oasis.M1230_SPEECH;
      $scope.M1240_FRML_PAIN_ASMT=oasis.M1240_FRML_PAIN_ASMT;
      $scope.M1242_PAIN_FREQ_ACTVTY_MVMT=oasis.M1242_PAIN_FREQ_ACTVTY_MVMT;
      //integumentary
      $scope.M1300_PRSR_ULCR_RISK_ASMT=oasis.M1300_PRSR_ULCR_RISK_ASMT;
      $scope.M1302_RISK_OF_PRSR_ULCR=oasis.M1302_RISK_OF_PRSR_ULCR;
      $scope.M1306_UNHLD_STG2_PRSR_ULCR=oasis.M1306_UNHLD_STG2_PRSR_ULCR;
      $scope.M1311_NBR_PRSULC_STG2_A1=oasis.M1311_NBR_PRSULC_STG2_A1;
      $scope.M1311_NBR_ULC_SOCROC_STG2_A2=oasis.M1311_NBR_ULC_SOCROC_STG2_A2;
      $scope.M1311_NBR_PRSULC_STG3_B1=oasis.M1311_NBR_PRSULC_STG3_B1;
      $scope.M1311_NBR_ULC_SOCROC_STG3_B2=oasis.M1311_NBR_ULC_SOCROC_STG3_B2;
      $scope.M1311_NBR_PRSULC_STG4_C1=oasis.M1311_NBR_PRSULC_STG4_C1;
      $scope.M1311_NBR_ULC_SOCROC_STG4_C2=oasis.M1311_NBR_ULC_SOCROC_STG4_C2;
      $scope.M1311_NSTG_DRSG_D1=oasis.M1311_NSTG_DRSG_D1;
      $scope.M1311_NSTG_DRSG_SOCROC_D2=oasis.M1311_NSTG_DRSG_SOCROC_D2;
      $scope.M1311_NSTG_CVRG_E1=oasis.M1311_NSTG_CVRG_E1;
      $scope.M1311_NSTG_CVRG_SOCROC_E2=oasis.M1311_NSTG_CVRG_SOCROC_E2;
      $scope.M1311_NSTG_DEEP_TSUE_F1=oasis.M1311_NSTG_DEEP_TSUE_F1;
      $scope.M1311_NSTG_DEEP_TSUE_SOCROC_F2=oasis.M1311_NSTG_DEEP_TSUE_SOCROC_F2;
      $scope.M1320_STUS_PRBLM_PRSR_ULCR=oasis.M1320_STUS_PRBLM_PRSR_ULCR;
      $scope.M1322_NBR_PRSULC_STG1=oasis.M1322_NBR_PRSULC_STG1;
      $scope.M1324_STG_PRBLM_ULCER=oasis.M1324_STG_PRBLM_ULCER;
      $scope.M1330_STAS_ULCR_PRSNT=oasis.M1330_STAS_ULCR_PRSNT;
      $scope.M1332_NBR_STAS_ULCR=oasis.M1332_NBR_STAS_ULCR;
      $scope.M1334_STUS_PRBLM_STAS_ULCR=oasis.M1334_STUS_PRBLM_STAS_ULCR;
      $scope.M1340_SRGCL_WND_PRSNT=oasis.M1340_SRGCL_WND_PRSNT;
      $scope.M1342_STUS_PRBLM_SRGCL_WND=oasis.M1342_STUS_PRBLM_SRGCL_WND;
      $scope.M1350_LESION_OPEN_WND=oasis.M1350_LESION_OPEN_WND;
      //respiratory
      $scope.M1400_WHEN_DYSPNEIC=oasis.M1400_WHEN_DYSPNEIC;
      $scope.M1410_RESPTX_OXYGEN=oasis.M1410_RESPTX_OXYGEN;
      $scope.M1410_RESPTX_VENTILATOR=oasis.M1410_RESPTX_VENTILATOR;
      $scope.M1410_RESPTX_AIRPRESS=oasis.M1410_RESPTX_AIRPRESS;
      $scope.M1410_RESPTX_NONE=oasis.M1410_RESPTX_NONE;
      //elimination
      $scope.M1600_UTI=oasis.M1600_UTI;
      $scope.M1610_UR_INCONT=oasis.M1610_UR_INCONT;
      $scope.M1615_INCNTNT_TIMING=oasis.M1615_INCNTNT_TIMING;
      $scope.M1620_BWL_INCONT=oasis.M1620_BWL_INCONT;
      $scope.M1630_OSTOMY=oasis.M1630_OSTOMY;
      //behavioural
      $scope.M1700_COG_FUNCTION=oasis.M1700_COG_FUNCTION;
      $scope.M1710_WHEN_CONFUSED=oasis.M1710_WHEN_CONFUSED;
      $scope.M1720_WHEN_ANXIOUS=oasis.M1720_WHEN_ANXIOUS;
      $scope.M1730_STDZ_DPRSN_SCRNG=oasis.M1730_STDZ_DPRSN_SCRNG;
      $scope.M1730_PHQ2_LACK_INTRST=oasis.M1730_PHQ2_LACK_INTRST;
      $scope.M1730_PHQ2_DPRSN=oasis.M1730_PHQ2_DPRSN;
      $scope.M1740_BD_MEM_DEFICIT=oasis.M1740_BD_MEM_DEFICIT ;
      $scope.M1740_BD_IMP_DECISN=oasis.M1740_BD_IMP_DECISN;
      $scope.M1740_BD_VERBAL=oasis.M1740_BD_VERBAL;
      $scope.M1740_BD_PHYSICAL=oasis.M1740_BD_PHYSICAL;
      $scope.M1740_BD_SOC_INAPPRO=oasis.M1740_BD_SOC_INAPPRO;
      $scope.M1740_BD_DELUSIONS=oasis.M1740_BD_DELUSIONS;
      $scope.M1740_BD_NONE=oasis.M1740_BD_NONE;
      $scope.M1745_BEH_PROB_FREQ=oasis.M1745_BEH_PROB_FREQ;
      $scope.M1750_REC_PSYCH_NURS=oasis.M1750_REC_PSYCH_NURS;
      //ADL
      $scope.M1800_CRNT_GROOMING=oasis.M1800_CRNT_GROOMING;
      $scope.M1810_CRNT_DRESS_UPPER=oasis.M1810_CRNT_DRESS_UPPER;
      $scope.M1820_CRNT_DRESS_LOWER=oasis.M1820_CRNT_DRESS_LOWER;
      $scope.M1830_CRNT_BATHG=oasis.M1830_CRNT_BATHG;
      $scope.M1840_CRNT_TOILTG=oasis.M1840_CRNT_TOILTG;
      $scope.M1845_CRNT_TOILTG_HYGN=oasis.M1845_CRNT_TOILTG_HYGN ;
      $scope.M1850_CRNT_TRNSFRNG=oasis.M1850_CRNT_TRNSFRNG;
      $scope.M1860_CRNT_AMBLTN=oasis.M1860_CRNT_AMBLTN;
      $scope.M1870_CRNT_FEEDING=oasis.M1870_CRNT_FEEDING;
      $scope.M1880_CRNT_PREP_LT_MEALS=oasis.M1880_CRNT_PREP_LT_MEALS;
      $scope.M1890_CRNT_PHONE_USE=oasis.M1890_CRNT_PHONE_USE;
      $scope.M1900_PRIOR_ADLIADL_SELF=oasis.M1900_PRIOR_ADLIADL_SELF;
      $scope.M1900_PRIOR_ADLIADL_AMBLTN=oasis.M1900_PRIOR_ADLIADL_AMBLTN;
      $scope.M1900_PRIOR_ADLIADL_TRNSFR=oasis.M1900_PRIOR_ADLIADL_TRNSFR;
      $scope.M1900_PRIOR_ADLIADL_HSEHOLD=oasis.M1900_PRIOR_ADLIADL_HSEHOLD;
      $scope.M1910_MLT_FCTR_FALL_RISK_ASMT=oasis.M1910_MLT_FCTR_FALL_RISK_ASMT;
      //medication
      $scope.M2001_DRUG_RGMN_RVW=oasis.M2001_DRUG_RGMN_RVW;
      $scope.M2003_MDCTN_FLWP=oasis.M2003_MDCTN_FLWP;
      $scope.M2010_HIGH_RISK_DRUG_EDCTN=oasis.M2010_HIGH_RISK_DRUG_EDCTN;
      $scope.M2020_CRNT_MGMT_ORAL_MDCTN=oasis.M2020_CRNT_MGMT_ORAL_MDCTN;
      $scope.M2030_CRNT_MGMT_INJCTN_MDCTN=oasis.M2030_CRNT_MGMT_INJCTN_MDCTN;
      $scope.M2040_PRIOR_MGMT_ORAL_MDCTN=oasis.M2040_PRIOR_MGMT_ORAL_MDCTN;
      $scope.M2040_PRIOR_MGMT_INJCTN_MDCTN=oasis.M2040_PRIOR_MGMT_INJCTN_MDCTN;
      //careManagement
      $scope.M2102_CARE_TYPE_SRC_ADL=oasis.M2102_CARE_TYPE_SRC_ADL;
      $scope.M2102_CARE_TYPE_SRC_ADVCY=oasis.M2102_CARE_TYPE_SRC_ADVCY;
      $scope.M2102_CARE_TYPE_SRC_EQUIP=oasis.M2102_CARE_TYPE_SRC_EQUIP;
      $scope.M2102_CARE_TYPE_SRC_IADL=oasis.M2102_CARE_TYPE_SRC_IADL;
      $scope.M2102_CARE_TYPE_SRC_MDCTN=oasis.M2102_CARE_TYPE_SRC_MDCTN;
      $scope.M2102_CARE_TYPE_SRC_PRCDR=oasis.M2102_CARE_TYPE_SRC_PRCDR;
      $scope.M2102_CARE_TYPE_SRC_SPRVSN=oasis.M2102_CARE_TYPE_SRC_SPRVSN;
      $scope.M2110_ADL_IADL_ASTNC_FREQ=oasis.M2110_ADL_IADL_ASTNC_FREQ;
      //therapy
      $scope.M2200_THER_NEED_NBR=oasis.M2200_THER_NEED_NBR;
      $scope.M2200_THER_NEED_NA=oasis.M2200_THER_NEED_NA;
      $scope.M2250_PLAN_SMRY_PTNT_SPECF=oasis.M2250_PLAN_SMRY_PTNT_SPECF;
      $scope.M2250_PLAN_SMRY_DBTS_FT_CARE=oasis.M2250_PLAN_SMRY_DBTS_FT_CARE;
      $scope.M2250_PLAN_SMRY_FALL_PRVNT=oasis.M2250_PLAN_SMRY_FALL_PRVNT;
      $scope.M2250_PLAN_SMRY_DPRSN_INTRVTN=oasis.M2250_PLAN_SMRY_DPRSN_INTRVTN;
      $scope.M2250_PLAN_SMRY_PAIN_INTRVTN=oasis.M2250_PLAN_SMRY_PAIN_INTRVTN;
      $scope.M2250_PLAN_SMRY_PRSULC_PRVNT=oasis.M2250_PLAN_SMRY_PRSULC_PRVNT;
      $scope.M2250_PLAN_SMRY_PRSULC_TRTMT=oasis.M2250_PLAN_SMRY_PRSULC_TRTMT;
      //functional
      $scope.GG0170C_MOBILITY_SOCROC_PERF=oasis.GG0170C_MOBILITY_SOCROC_PERF;
      $scope.GG0170C_MOBILITY_DSCHG_GOAL=oasis.GG0170C_MOBILITY_DSCHG_GOAL;

      if (oasis.M0032_ROC_DT !== null) {
        $scope.M0032_ROC_DT = new Date($filter("date")(oasis.M0032_ROC_DT, 'yyyy/MM/dd'))
      }

      if (oasis.M0090_INFO_COMPLETED_DT !== null) {
        $scope.M0090_INFO_COMPLETED_DT = new Date($filter("date")(oasis.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd'))
      }

      if (oasis.M0102_PHYSN_ORDRD_SOCROC_DT !== null) {
        $scope.M0102_PHYSN_ORDRD_SOCROC_DT = new Date($filter("date")(oasis.M0102_PHYSN_ORDRD_SOCROC_DT, 'yyyy/MM/dd'))
      }

      if (oasis.M0104_PHYSN_RFRL_DT !== null) {
        $scope.M0104_PHYSN_RFRL_DT = new Date($filter("date")(oasis.M0104_PHYSN_RFRL_DT, 'yyyy/MM/dd'))
      }

      if (oasis.M1005_INP_DISCHARGE_DT !== null) {
        $scope.M1005_INP_DISCHARGE_DT = new Date($filter("date")(oasis.M1005_INP_DISCHARGE_DT, 'yyyy/MM/dd'))
      }
    } else { //if there is no oasis, populate form using patient info
      console.log('populating form using patient info')
      $scope.form.OBJKEYID=null;
      $scope.M0020_PAT_ID=$scope.patient.PATIENTID;
      $scope.M0040_PAT_FNAME=$scope.patient.FNAME;
      $scope.M0040_PAT_MI=$scope.patient.MINITIAL;
      $scope.M0040_PAT_LNAME=$scope.patient.LNAME;
      $scope.M0050_PAT_ST=$scope.patient.STATE;
      $scope.M0060_PAT_ZIP=$scope.patient.ZIP;
      $scope.M0064_SSN=$scope.patient.SSN;
      //converting patient DOB
      $scope.patient.DOB = $filter("date")($scope.patient.DOB, 'yyyy/MM/dd');
      $scope.DOB = new Date($scope.patient.DOB);
      //converting patient gender fields
      if($scope.patient.GENDER ==='M') {
        $scope.M0069_PAT_GENDER = '1';
      } else if ($scope.patient.GENDER === 'F') {
         $scope.M0069_PAT_GENDER = '2';
      }
      //converting ethnicity
      switch ($scope.patient.M0140) {
        case "1":
          $scope.M0140_ETHNIC_AI_AN='1';
          break;
        case "2":
          $scope.M0140_ETHNIC_ASIAN='1';
          break;
        case "3":
          $scope.M0140_ETHNIC_BLACK='1';
          break;
        case "4":
          $scope.M0140_ETHNIC_HISP='1';
          break;
        case "5":
          $scope.M0140_ETHNIC_NH_PI='1';
          break;
        case "6":
          $scope.M0140_ETHNIC_WHITE='1';
          break;
        default:
          $scope.M0140_ETHNIC_AI_AN='0';
          $scope.M0140_ETHNIC_ASIAN='0';
          $scope.M0140_ETHNIC_BLACK='0';
          $scope.M0140_ETHNIC_HISP='0';
          $scope.M0140_ETHNIC_NH_PI='0';
          $scope.M0140_ETHNIC_WHITE='0';
      }
      //getting agency info
      $scope.agency=agencyService.get();
      console.log('agency config', $scope.agency);
      $scope.M0014_BRANCH_STATE=$scope.agency.STATE;
      $scope.M0016_BRANCH_ID=$scope.agency.BRANCH;
      $scope.M0010_CCN=$scope.agency.PROVIDERNUM;
      //check if insurance is medicare
      if ($scope.patient.INSCO===1) {
        $scope.M0150_CPAY_MCARE_FFS='1'
        $scope.M0063_MEDICARE_NUM=$scope.patient.INSID;
        $scope.M0063_MEDICARE_NA='0'
      } else {
        $scope.M0063_MEDICARE_NA='1'
        $scope.M0150_CPAY_MCARE_FFS='0'
      }
      //check if insurance is medicaid
      if ($scope.patient.INSCO===7) {
        $scope.M0150_CPAY_MCAID_FFS='1'
        $scope.M0065_MEDICAID_NUM=$scope.patient.INSID;
        $scope.M0065_MEDICAID_NA='0'
      } else {
        $scope.M0065_MEDICAID_NA='1'
        $scope.M0150_CPAY_MCAID_FFS='0'
      }
      //assigning  ROC complete date
      $scope.M0032_ROC_DT = new Date($filter("date")($scope.form.VISITDATE, 'yyyy/MM/dd'));
      $scope.M0090_INFO_COMPLETED_DT = new Date($filter("date")($scope.form.VISITDATE, 'yyyy/MM/dd'));
      //assign assessment reason
      $scope.M0100_ASSMT_REASON='03';
      //assign soc date
      dataService.get('admission', {'KEYID':$scope.patient.LSTADMIT}).then(function(data){
        var admit = data.data[0];
        //get soc date and populate
        var soc = $filter("date")(admit.SOCDATE, 'yyyy/MM/dd');
        $scope.M0030_START_CARE_DT = new Date(soc);
        //get NPI and insurance info
        $scope.lastPatAdmit = data.data[0];
        console.log('form admit', $scope.lastPatAdmit)
        // findInsurCo();
        findDoctor();
      });
    }
  });

  $scope.submitToQA=function () {
    if (!confirm("No changes are allowed once submitted to QA, are you sure you want to submit?")) { //no don't submit to qa
      console.log('no, do not submit to qa');
      event.preventDefault();
      $scope.submitQA=false;
    } else { //yes submit to qa
      $scope.submitQA=true;
      $scope.saveAndContinueLater();
    }
  };

  $scope.saveAndContinueLater = function() {
    $scope.dataSaved=false;
    if ( ($scope.M0032_ROC_DT===null) || ($scope.M0032_ROC_DT===undefined) ){
      alert('Resumption of Care Date Cannot be Empty')
    } else {
      // check to see if form has objkeyid
      dataService.get('form', {KEYID:$scope.form.FORMID}).then(function (data) {
        var form = data.data[0];
        console.log(form);
        //if objkeyid is null, then save form, else edit form
        if (form.OBJKEYID === null) {
          $scope.createResumptionOfCare();
          $scope.dataLoading = true;
        } else {
          console.log('save edited oasis');
          editResumptionOfCare(form.OBJKEYID);
          $scope.dataLoading = true;
        }
      });
    };
  }

  $scope.createResumptionOfCare = function() {
    var newResumptionOfCare = getOasisObject()
    console.log("newResumptionOfCare", newResumptionOfCare);
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['oasisC2_2_20_1'].add,
      method: 'POST',
      data: $httpParamSerializerJQLike(newResumptionOfCare),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function(response){
        console.log(response);
        var formkeyid = parseInt(response.data['form keyid']);
        //edit patient info
        editPatient();
        //edit form to contain objectkeyid
        var form = {
          KEYID: formkeyid,
          OBJKEYID: response.data.keyid,
          STATUS: 2,
          ASSIGNEDTO:$scope.form.ASSIGNEDTO
        };
        //if submitting to QA, change status to 1 for complete
        if ($scope.submitQA===true) {
          form.STATUS=1;
        }
        if ($scope.changeVisitDate) {
          form.FORMDATE=$filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')
        }
        dataService.edit('form', form).then(function (response) {
          console.log(response);
        });

        //check to see if date is within last 5 days of episode, if it is,
        //then create new epi, if not, continue episode
        var rocDate=moment($filter("date")($scope.M0032_ROC_DT, 'yyyy/MM/dd'));
        dataService.get('form', {KEYID: $scope.form.FORMID}).then(function (data) {
          var form = data.data[0];
          console.log(form)
          dataService.get('episode', {KEYID:form.EPIKEYID}).then(function (data) {
            $scope.episode = data.data[0];
            $scope.endOfEpisode = moment($scope.episode.THRUDATE);
            $scope.lastFiveDays = moment($scope.endOfEpisode.clone().subtract(5, 'days'));
            if ( rocDate.isBetween($scope.lastFiveDays, $scope.endOfEpisode, null, '[)') ) {
              //create new episode
              createNewEpisode();
            } else {
              console.log('episode continues')
            }
          });
        });
        //edit current visit to PERFORMED=1 and edit visit date if changed
        dataService.get('form', {KEYID: $scope.form.FORMID}).then(function (data) {
          var form = data.data[0];
          console.log(form);
          var editVisit= {
            KEYID: form.VISITID,
            PERFORMED: 1
          }
          if ($scope.changeVisitDate) {
            editVisit.VISITDATE=$filter("date")($scope.M0032_ROC_DT, 'yyyy/MM/dd')
          }
          // console.log(visitid);
          dataService.edit('visit', editVisit).then(function (response) {
            console.log(response);
          });
        });

        //edit patient status
        dataService.edit('patient', {'KEYID': $scope.patient.KEYID, 'PATSTATUS': 'A'}).then(function (response) {
          console.log(response);
          //reset patient
          dataService.get('patient', {KEYID: $scope.patient.KEYID}).then(function (data) {
            patientService.set(data.data[0]);
            $state.go('patientProfile');
          });
        });
      }, function(data) {
        console.log(data);
    });
  };

  function createNewEpisode() {
    console.log('creating new episode');
    //create new episode
    //Get currentAdmitKEYID
    dataService.get('admission', {'PATKEYID':$scope.patient.KEYID}).then(function(data){
      var admitArr = data.data;
      var currentAdmit = admitArr.slice(-1).pop();
      $scope.currentAdmitKEYID = currentAdmit.KEYID;

      var newEpisodeStartDate = $scope.endOfEpisode.clone().add(1, 'days');
      var newEpisodeEndDate = $scope.endOfEpisode.clone().add(60, 'days');

      var newEpisode = {
        PATKEYID: $scope.patient.KEYID,
        ADMITKEYID: $scope.currentAdmitKEYID,
        FROMDATE: $filter("date")(new Date(newEpisodeStartDate), 'yyyy/MM/dd'),
        THRUDATE: $filter("date")(new Date(newEpisodeEndDate), 'yyyy/MM/dd'),
        INITIAL: 1,
        DISCHARGED: 0,
        DCDATE: null,
        EPISODENO: $scope.episode.EPISODENO + 1,
      };
      console.log('newEpisode', newEpisode);
      dataService.add('episode', newEpisode).then(function(response) {
        console.log(response);
        $state.go('patientProfile');
      });
    });
  }

  function editResumptionOfCare(objkeyid) {
    var editResumptionOfCareOasis=getOasisObject();
    editResumptionOfCareOasis.KEYID=objkeyid;

    console.log('editResumptionOfCareOasis', editResumptionOfCareOasis)
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['oasisC2_2_20_1'].edit,
      method: 'POST',
      // params: CONSTANTS.api.actions['oasisC2_2_20_1'].edit,
      data: $httpParamSerializerJQLike(editResumptionOfCareOasis),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function(msg){
      console.log(msg)
      //edit patient
      editPatient()
      //edit visit date if changed
      if ($scope.changeVisitDate) {
        dataService.get('form', {KEYID: $scope.form.FORMID}).then(function (data) {
          var form = data.data[0];
          console.log(form);
          var editVisit= {
            KEYID: form.VISITID,
            VISITDATE: $filter("date")($scope.M0032_ROC_DT, 'yyyy/MM/dd')
          };
          dataService.edit('visit', editVisit).then(function (response) {
            console.log(response);
          });
        });
      }

      //if submitting to QA, change status to 1 for complete,
      if ($scope.submitQA===true) {
        var form = {
          KEYID: $scope.form.FORMID,
          STATUS: 1,
          ASSIGNEDTO:$scope.form.ASSIGNEDTO
        };
        if ($scope.changeVisitDate) {
          form.FORMDATE=$filter("date")($scope.M0032_ROC_DT, 'yyyy/MM/dd')
        }
        dataService.edit('form', form).then(function (response) {
          console.log(response);
          $state.go('scheduling');
        });
      }
    });
  }



  function getOasisObject() {
    var oasisObject = {
      FORMID: $scope.form.FORMID,
      FORMTYPE: 3,
      ADMITKEYID: $scope.form.ADMITKEYID,
      VISITID: $scope.form.VISITID,
      ASMT_SYS_CD: 'OASIS',
      ITM_SBST_CD: '03',
      ITM_SET_VRSN_CD: 'C2-012017',
      SPEC_VRSN_CD: '2.20',
      CORRECTION_NUM: '00',
      STATE_CD: 'NV',
      HHA_AGENCY_ID: 'FAKE',
      SFW_ID: '111111111',
      SFW_NAME: 'KRYPTONITE',
      SFW_EMAIL_ADR: 'TEST@TEST.COM',
      SFW_PROD_NAME: 'KRYPTONITE',
      SFW_PROD_VRSN_CD: '1',
      NATL_PRVDR_ID: '1111111111',
      TRANS_TYPE_CD: 1,
      //******** GENERAL ********//
      M0010_CCN: $scope.M0010_CCN,
      M0014_BRANCH_STATE: $scope.M0014_BRANCH_STATE,
      M0016_BRANCH_ID: $scope.M0016_BRANCH_ID,
      M0018_PHYSICIAN_ID: $scope.M0018_PHYSICIAN_ID,
      M0018_PHYSICIAN_UK: $scope.M0018_PHYSICIAN_UK,
      M0020_PAT_ID: $scope.M0020_PAT_ID,
      M0030_START_CARE_DT: $filter("date")($scope.M0030_START_CARE_DT, 'yyyy-MM-dd'),
      M0032_ROC_DT: $filter("date")($scope.M0032_ROC_DT, 'yyyy-MM-dd'),
      M0032_ROC_DT_NA: $scope.M0032_ROC_DT_NA,
      M0040_PAT_FNAME: $scope.M0040_PAT_FNAME,
      M0040_PAT_MI: $scope.M0040_PAT_MI,
      M0040_PAT_LNAME: $scope.M0040_PAT_LNAME,
      M0040_PAT_SUFFIX: $scope.M0040_PAT_SUFFIX,
      M0050_PAT_ST: $scope.M0050_PAT_ST,
      M0060_PAT_ZIP: $scope.M0060_PAT_ZIP,
      M0063_MEDICARE_NUM: $scope.M0063_MEDICARE_NUM,
      M0063_MEDICARE_NA: $scope.M0063_MEDICARE_NA,
      M0064_SSN: $scope.M0064_SSN,
      M0064_SSN_UK: $scope.M0064_SSN_UK,
      M0065_MEDICAID_NUM: $scope.M0065_MEDICAID_NUM,
      M0065_MEDICAID_NA: $scope.M0065_MEDICAID_NA,
      M0066_PAT_BIRTH_DT: $filter("date")($scope.DOB, 'yyyy-MM-dd'),
      M0069_PAT_GENDER: $scope.M0069_PAT_GENDER,
      M0140_ETHNIC_AI_AN: $scope.M0140_ETHNIC_AI_AN,
      M0140_ETHNIC_ASIAN: $scope.M0140_ETHNIC_ASIAN,
      M0140_ETHNIC_BLACK: $scope.M0140_ETHNIC_BLACK,
      M0140_ETHNIC_HISP: $scope.M0140_ETHNIC_HISP,
      M0140_ETHNIC_NH_PI: $scope.M0140_ETHNIC_NH_PI,
      M0140_ETHNIC_WHITE: $scope.M0140_ETHNIC_WHITE,
      M0150_CPAY_NONE: $scope.M0150_CPAY_NONE,
      M0150_CPAY_MCARE_FFS: $scope.M0150_CPAY_MCARE_FFS,
      M0150_CPAY_MCARE_HMO: $scope.M0150_CPAY_MCARE_HMO,
      M0150_CPAY_MCAID_FFS: $scope.M0150_CPAY_MCAID_FFS,
      M0150_CPAY_MCAID_HMO: $scope.M0150_CPAY_MCAID_HMO,
      M0150_CPAY_WRKCOMP: $scope.M0150_CPAY_WRKCOMP,
      M0150_CPAY_TITLEPGMS: $scope.M0150_CPAY_TITLEPGMS,
      M0150_CPAY_OTH_GOVT: $scope.M0150_CPAY_OTH_GOVT,
      M0150_CPAY_PRIV_INS: $scope.M0150_CPAY_PRIV_INS,
      M0150_CPAY_PRIV_HMO: $scope.M0150_CPAY_PRIV_HMO,
      M0150_CPAY_SELFPAY: $scope.M0150_CPAY_SELFPAY,
      M0150_CPAY_OTHER: $scope.M0150_CPAY_OTHER,
      M0150_CPAY_UK: $scope.M0150_CPAY_UK,
      //clinical record
      M0080_ASSESSOR_DISCIPLINE: $scope.M0080_ASSESSOR_DISCIPLINE,
      M0090_INFO_COMPLETED_DT: $filter("date")($scope.M0032_ROC_DT, 'yyyy-MM-dd'),
      M0100_ASSMT_REASON: '03',
      M0102_PHYSN_ORDRD_SOCROC_DT: $filter("date")($scope.M0102_PHYSN_ORDRD_SOCROC_DT, 'yyyy-MM-dd'),
      M0102_PHYSN_ORDRD_SOCROC_DT_NA: $scope.M0102_PHYSN_ORDRD_SOCROC_DT_NA,
      M0104_PHYSN_RFRL_DT: $filter("date")($scope.M0104_PHYSN_RFRL_DT, 'yyyy-MM-dd'),
      M0110_EPISODE_TIMING: $scope.M0110_EPISODE_TIMING,
      //patient history
      M1000_DC_LTC_14_DA: $scope.M1000_DC_LTC_14_DA,
      M1000_DC_SNF_14_DA: $scope.M1000_DC_SNF_14_DA,
      M1000_DC_IPPS_14_DA: $scope.M1000_DC_IPPS_14_DA,
      M1000_DC_LTCH_14_DA: $scope.M1000_DC_LTCH_14_DA,
      M1000_DC_IRF_14_DA: $scope.M1000_DC_IRF_14_DA,
      M1000_DC_PSYCH_14_DA: $scope.M1000_DC_PSYCH_14_DA,
      M1000_DC_OTH_14_DA: $scope.M1000_DC_OTH_14_DA,
      M1000_DC_NONE_14_DA: $scope.M1000_DC_NONE_14_DA,
      M1005_INP_DISCHARGE_DT: $filter("date")($scope.M1005_INP_DISCHARGE_DT, 'yyyy-MM-dd'),
      M1005_INP_DSCHG_UNKNOWN: $scope.M1005_INP_DSCHG_UNKNOWN,
      M1011_14_DAY_INP1_ICD: $scope.M1011_14_DAY_INP1_ICD,
      M1011_14_DAY_INP2_ICD: $scope.M1011_14_DAY_INP2_ICD,
      M1011_14_DAY_INP3_ICD: $scope.M1011_14_DAY_INP3_ICD,
      M1011_14_DAY_INP4_ICD: $scope.M1011_14_DAY_INP4_ICD,
      M1011_14_DAY_INP5_ICD: $scope.M1011_14_DAY_INP5_ICD,
      M1011_14_DAY_INP6_ICD: $scope.M1011_14_DAY_INP6_ICD,
      M1011_14_DAY_INP_NA: $scope.M1011_14_DAY_INP_NA,
      M1017_CHGREG_ICD1: $scope.M1017_CHGREG_ICD1,
      M1017_CHGREG_ICD2: $scope.M1017_CHGREG_ICD2,
      M1017_CHGREG_ICD3: $scope.M1017_CHGREG_ICD3,
      M1017_CHGREG_ICD4: $scope.M1017_CHGREG_ICD4,
      M1017_CHGREG_ICD5: $scope.M1017_CHGREG_ICD5,
      M1017_CHGREG_ICD6: $scope.M1017_CHGREG_ICD6,
      M1017_CHGREG_ICD_NA: $scope.M1017_CHGREG_ICD_NA,
      M1018_PRIOR_UR_INCON: $scope.M1018_PRIOR_UR_INCON,
      M1018_PRIOR_CATH: $scope.M1018_PRIOR_CATH,
      M1018_PRIOR_INTRACT_PAIN: $scope.M1018_PRIOR_INTRACT_PAIN ,
      M1018_PRIOR_IMPR_DECSN: $scope.M1018_PRIOR_IMPR_DECSN,
      M1018_PRIOR_DISRUPTIVE: $scope.M1018_PRIOR_DISRUPTIVE,
      M1018_PRIOR_MEM_LOSS: $scope.M1018_PRIOR_MEM_LOSS,
      M1018_PRIOR_NONE: $scope.M1018_PRIOR_NONE,
      M1018_PRIOR_NOCHG_14D: $scope.M1018_PRIOR_NOCHG_14D,
      M1018_PRIOR_UNKNOWN: $scope.M1018_PRIOR_UNKNOWN,
      M1021_PRIMARY_DIAG_ICD: $scope.M1021_PRIMARY_DIAG_ICD,
      M1021_PRIMARY_DIAG_SEVERITY: $scope.M1021_PRIMARY_DIAG_SEVERITY,
      M1023_OTH_DIAG1_ICD: $scope.M1023_OTH_DIAG1_ICD,
      M1023_OTH_DIAG1_SEVERITY: $scope.M1023_OTH_DIAG1_SEVERITY,
      M1023_OTH_DIAG2_ICD: $scope.M1023_OTH_DIAG2_ICD,
      M1023_OTH_DIAG2_SEVERITY: $scope.M1023_OTH_DIAG2_SEVERITY,
      M1023_OTH_DIAG3_ICD: $scope.M1023_OTH_DIAG3_ICD,
      M1023_OTH_DIAG3_SEVERITY: $scope.M1023_OTH_DIAG3_SEVERITY,
      M1023_OTH_DIAG4_ICD: $scope.M1023_OTH_DIAG4_ICD,
      M1023_OTH_DIAG4_SEVERITY: $scope.M1023_OTH_DIAG4_SEVERITY,
      M1023_OTH_DIAG5_ICD: $scope.M1023_OTH_DIAG5_ICD,
      M1023_OTH_DIAG5_SEVERITY: $scope.M1023_OTH_DIAG5_SEVERITY,
      M1025_OPT_DIAG_ICD_A3: $scope.M1025_OPT_DIAG_ICD_A3,
      M1025_OPT_DIAG_ICD_A4: $scope.M1025_OPT_DIAG_ICD_A4,
      M1025_OPT_DIAG_ICD_B3: $scope.M1025_OPT_DIAG_ICD_B3,
      M1025_OPT_DIAG_ICD_B4: $scope.M1025_OPT_DIAG_ICD_B4,
      M1025_OPT_DIAG_ICD_C3: $scope.M1025_OPT_DIAG_ICD_C3,
      M1025_OPT_DIAG_ICD_C4: $scope.M1025_OPT_DIAG_ICD_C4,
      M1025_OPT_DIAG_ICD_D3: $scope.M1025_OPT_DIAG_ICD_D3,
      M1025_OPT_DIAG_ICD_D4: $scope.M1025_OPT_DIAG_ICD_D4,
      M1025_OPT_DIAG_ICD_E3: $scope.M1025_OPT_DIAG_ICD_E3,
      M1025_OPT_DIAG_ICD_E4: $scope.M1025_OPT_DIAG_ICD_E4,
      M1025_OPT_DIAG_ICD_F3: $scope.M1025_OPT_DIAG_ICD_F3,
      M1025_OPT_DIAG_ICD_F4: $scope.M1025_OPT_DIAG_ICD_F4,
      M1028_ACTV_DIAG_PVD_PAD: $scope.M1028_ACTV_DIAG_PVD_PAD,
      M1028_ACTV_DIAG_DM: $scope.M1028_ACTV_DIAG_DM,
      M1030_THH_IV_INFUSION: $scope.M1030_THH_IV_INFUSION,
      M1030_THH_PAR_NUTRITION: $scope.M1030_THH_PAR_NUTRITION,
      M1030_THH_ENT_NUTRITION: $scope.M1030_THH_ENT_NUTRITION,
      M1030_THH_NONE_ABOVE: $scope.M1030_THH_NONE_ABOVE,
      M1033_HOSP_RISK_HSTRY_FALLS: $scope.M1033_HOSP_RISK_HSTRY_FALLS,
      M1033_HOSP_RISK_WEIGHT_LOSS: $scope.M1033_HOSP_RISK_WEIGHT_LOSS,
      M1033_HOSP_RISK_MLTPL_HOSPZTN: $scope.M1033_HOSP_RISK_MLTPL_HOSPZTN,
      M1033_HOSP_RISK_MLTPL_ED_VISIT: $scope.M1033_HOSP_RISK_MLTPL_ED_VISIT,
      M1033_HOSP_RISK_MNTL_BHV_DCLN: $scope.M1033_HOSP_RISK_MNTL_BHV_DCLN,
      M1033_HOSP_RISK_COMPLIANCE: $scope.M1033_HOSP_RISK_COMPLIANCE,
      M1033_HOSP_RISK_5PLUS_MDCTN: $scope.M1033_HOSP_RISK_5PLUS_MDCTN,
      M1033_HOSP_RISK_CRNT_EXHSTN: $scope.M1033_HOSP_RISK_CRNT_EXHSTN,
      M1033_HOSP_RISK_OTHR_RISK: $scope.M1033_HOSP_RISK_OTHR_RISK,
      M1033_HOSP_RISK_NONE_ABOVE: $scope.M1033_HOSP_RISK_NONE_ABOVE,
      M1034_PTNT_OVRAL_STUS: $scope.M1034_PTNT_OVRAL_STUS,
      M1036_RSK_SMOKING: $scope.M1036_RSK_SMOKING,
      M1036_RSK_OBESITY: $scope.M1036_RSK_OBESITY,
      M1036_RSK_ALCOHOLISM: $scope.M1036_RSK_ALCOHOLISM,
      M1036_RSK_DRUGS: $scope.M1036_RSK_DRUGS,
      M1036_RSK_NONE: $scope.M1036_RSK_NONE,
      M1036_RSK_UNKNOWN: $scope.M1036_RSK_UNKNOWN,
      M1060_HEIGHT_A: $scope.M1060_HEIGHT_A,
      M1060_WEIGHT_B: $scope.M1060_WEIGHT_B,
      //living situation
      M1100_PTNT_LVG_STUTN: $scope.M1100_PTNT_LVG_STUTN,
      //sensory
      M1200_VISION: $scope.M1200_VISION,
      M1210_HEARG_ABLTY: $scope.M1210_HEARG_ABLTY,
      M1220_UNDRSTG_VERBAL_CNTNT: $scope.M1220_UNDRSTG_VERBAL_CNTNT,
      M1230_SPEECH: $scope.M1230_SPEECH,
      M1240_FRML_PAIN_ASMT: $scope.M1240_FRML_PAIN_ASMT,
      M1242_PAIN_FREQ_ACTVTY_MVMT: $scope.M1242_PAIN_FREQ_ACTVTY_MVMT,
      //integumentary
      M1300_PRSR_ULCR_RISK_ASMT: $scope.M1300_PRSR_ULCR_RISK_ASMT,
      M1302_RISK_OF_PRSR_ULCR: $scope.M1302_RISK_OF_PRSR_ULCR,
      M1306_UNHLD_STG2_PRSR_ULCR: $scope.M1306_UNHLD_STG2_PRSR_ULCR,
      M1311_NBR_PRSULC_STG2_A1: $scope.M1311_NBR_PRSULC_STG2_A1,
      M1311_NBR_ULC_SOCROC_STG2_A2: $scope.M1311_NBR_ULC_SOCROC_STG2_A2,
      M1311_NBR_PRSULC_STG3_B1: $scope.M1311_NBR_PRSULC_STG3_B1,
      M1311_NBR_ULC_SOCROC_STG3_B2: $scope.M1311_NBR_ULC_SOCROC_STG3_B2,
      M1311_NBR_PRSULC_STG4_C1: $scope.M1311_NBR_PRSULC_STG4_C1,
      M1311_NBR_ULC_SOCROC_STG4_C2: $scope.M1311_NBR_ULC_SOCROC_STG4_C2,
      M1311_NSTG_DRSG_D1: $scope.M1311_NSTG_DRSG_D1,
      M1311_NSTG_DRSG_SOCROC_D2: $scope.M1311_NSTG_DRSG_SOCROC_D2,
      M1311_NSTG_CVRG_E1: $scope.M1311_NSTG_CVRG_E1,
      M1311_NSTG_CVRG_SOCROC_E2: $scope.M1311_NSTG_CVRG_SOCROC_E2,
      M1311_NSTG_DEEP_TSUE_F1: $scope.M1311_NSTG_DEEP_TSUE_F1,
      M1311_NSTG_DEEP_TSUE_SOCROC_F2: $scope.M1311_NSTG_DEEP_TSUE_SOCROC_F2,
      M1320_STUS_PRBLM_PRSR_ULCR: $scope.M1320_STUS_PRBLM_PRSR_ULCR,
      M1322_NBR_PRSULC_STG1: $scope.M1322_NBR_PRSULC_STG1,
      M1324_STG_PRBLM_ULCER: $scope.M1324_STG_PRBLM_ULCER,
      M1330_STAS_ULCR_PRSNT: $scope.M1330_STAS_ULCR_PRSNT,
      M1332_NBR_STAS_ULCR: $scope.M1332_NBR_STAS_ULCR,
      M1334_STUS_PRBLM_STAS_ULCR: $scope.M1334_STUS_PRBLM_STAS_ULCR,
      M1340_SRGCL_WND_PRSNT: $scope.M1340_SRGCL_WND_PRSNT,
      M1342_STUS_PRBLM_SRGCL_WND: $scope.M1342_STUS_PRBLM_SRGCL_WND,
      M1350_LESION_OPEN_WND: $scope.M1350_LESION_OPEN_WND,
      //respiratory
      M1400_WHEN_DYSPNEIC: $scope.M1400_WHEN_DYSPNEIC,
      M1410_RESPTX_OXYGEN: $scope.M1410_RESPTX_OXYGEN,
      M1410_RESPTX_VENTILATOR: $scope.M1410_RESPTX_VENTILATOR,
      M1410_RESPTX_AIRPRESS: $scope.M1410_RESPTX_AIRPRESS,
      M1410_RESPTX_NONE: $scope.M1410_RESPTX_NONE,
      //elimination
      M1600_UTI: $scope.M1600_UTI,
      M1610_UR_INCONT: $scope.M1610_UR_INCONT,
      M1615_INCNTNT_TIMING: $scope.M1615_INCNTNT_TIMING,
      M1620_BWL_INCONT: $scope.M1620_BWL_INCONT,
      M1630_OSTOMY: $scope.M1630_OSTOMY,
      //behavioural
      M1700_COG_FUNCTION: $scope.M1700_COG_FUNCTION,
      M1710_WHEN_CONFUSED: $scope.M1710_WHEN_CONFUSED,
      M1720_WHEN_ANXIOUS: $scope.M1720_WHEN_ANXIOUS,
      M1730_STDZ_DPRSN_SCRNG: $scope.M1730_STDZ_DPRSN_SCRNG,
      M1730_PHQ2_LACK_INTRST: $scope.M1730_PHQ2_LACK_INTRST,
      M1730_PHQ2_DPRSN: $scope.M1730_PHQ2_DPRSN,
      M1740_BD_MEM_DEFICIT: $scope.M1740_BD_MEM_DEFICIT ,
      M1740_BD_IMP_DECISN: $scope.M1740_BD_IMP_DECISN,
      M1740_BD_VERBAL: $scope.M1740_BD_VERBAL,
      M1740_BD_PHYSICAL: $scope.M1740_BD_PHYSICAL,
      M1740_BD_SOC_INAPPRO: $scope.M1740_BD_SOC_INAPPRO,
      M1740_BD_DELUSIONS: $scope.M1740_BD_DELUSIONS,
      M1740_BD_NONE: $scope.M1740_BD_NONE,
      M1745_BEH_PROB_FREQ: $scope.M1745_BEH_PROB_FREQ,
      M1750_REC_PSYCH_NURS: $scope.M1750_REC_PSYCH_NURS,
      //ADL
      M1800_CRNT_GROOMING: $scope.M1800_CRNT_GROOMING,
      M1810_CRNT_DRESS_UPPER: $scope.M1810_CRNT_DRESS_UPPER,
      M1820_CRNT_DRESS_LOWER: $scope.M1820_CRNT_DRESS_LOWER,
      M1830_CRNT_BATHG: $scope.M1830_CRNT_BATHG,
      M1840_CRNT_TOILTG: $scope.M1840_CRNT_TOILTG,
      M1845_CRNT_TOILTG_HYGN: $scope.M1845_CRNT_TOILTG_HYGN ,
      M1850_CRNT_TRNSFRNG: $scope.M1850_CRNT_TRNSFRNG,
      M1860_CRNT_AMBLTN: $scope.M1860_CRNT_AMBLTN,
      M1870_CRNT_FEEDING: $scope.M1870_CRNT_FEEDING,
      M1880_CRNT_PREP_LT_MEALS: $scope.M1880_CRNT_PREP_LT_MEALS,
      M1890_CRNT_PHONE_USE: $scope.M1890_CRNT_PHONE_USE,
      M1900_PRIOR_ADLIADL_SELF: $scope.M1900_PRIOR_ADLIADL_SELF,
      M1900_PRIOR_ADLIADL_AMBLTN: $scope.M1900_PRIOR_ADLIADL_AMBLTN,
      M1900_PRIOR_ADLIADL_TRNSFR: $scope.M1900_PRIOR_ADLIADL_TRNSFR,
      M1900_PRIOR_ADLIADL_HSEHOLD: $scope.M1900_PRIOR_ADLIADL_HSEHOLD,
      M1910_MLT_FCTR_FALL_RISK_ASMT: $scope.M1910_MLT_FCTR_FALL_RISK_ASMT,
      //medication
      M2001_DRUG_RGMN_RVW: $scope.M2001_DRUG_RGMN_RVW,
      M2003_MDCTN_FLWP: $scope.M2003_MDCTN_FLWP,
      // M2005_MDCTN_INTRVTN: $scope.M2005_MDCTN_INTRVTN,
      M2010_HIGH_RISK_DRUG_EDCTN: $scope.M2010_HIGH_RISK_DRUG_EDCTN,
      // M2016_DRUG_EDCTN_INTRVTN: $scope.M2016_DRUG_EDCTN_INTRVTN,
      M2020_CRNT_MGMT_ORAL_MDCTN: $scope.M2020_CRNT_MGMT_ORAL_MDCTN,
      M2030_CRNT_MGMT_INJCTN_MDCTN: $scope.M2030_CRNT_MGMT_INJCTN_MDCTN,
      M2040_PRIOR_MGMT_ORAL_MDCTN: $scope.M2040_PRIOR_MGMT_ORAL_MDCTN,
      M2040_PRIOR_MGMT_INJCTN_MDCTN: $scope.M2040_PRIOR_MGMT_INJCTN_MDCTN,
      //careManagement
      M2102_CARE_TYPE_SRC_ADL: $scope.M2102_CARE_TYPE_SRC_ADL,
      M2102_CARE_TYPE_SRC_ADVCY: $scope.M2102_CARE_TYPE_SRC_ADVCY,
      M2102_CARE_TYPE_SRC_EQUIP: $scope.M2102_CARE_TYPE_SRC_EQUIP,
      M2102_CARE_TYPE_SRC_IADL: $scope.M2102_CARE_TYPE_SRC_IADL,
      M2102_CARE_TYPE_SRC_MDCTN: $scope.M2102_CARE_TYPE_SRC_MDCTN,
      M2102_CARE_TYPE_SRC_PRCDR: $scope.M2102_CARE_TYPE_SRC_PRCDR,
      M2102_CARE_TYPE_SRC_SPRVSN: $scope.M2102_CARE_TYPE_SRC_SPRVSN,
      M2110_ADL_IADL_ASTNC_FREQ: $scope.M2110_ADL_IADL_ASTNC_FREQ,
      //therapy
      M2200_THER_NEED_NBR: $scope.M2200_THER_NEED_NBR,
      M2200_THER_NEED_NA: $scope.M2200_THER_NEED_NA,
      M2250_PLAN_SMRY_PTNT_SPECF: $scope.M2250_PLAN_SMRY_PTNT_SPECF,
      M2250_PLAN_SMRY_DBTS_FT_CARE: $scope.M2250_PLAN_SMRY_DBTS_FT_CARE,
      M2250_PLAN_SMRY_FALL_PRVNT: $scope.M2250_PLAN_SMRY_FALL_PRVNT,
      M2250_PLAN_SMRY_DPRSN_INTRVTN: $scope.M2250_PLAN_SMRY_DPRSN_INTRVTN,
      M2250_PLAN_SMRY_PAIN_INTRVTN: $scope.M2250_PLAN_SMRY_PAIN_INTRVTN,
      M2250_PLAN_SMRY_PRSULC_PRVNT: $scope.M2250_PLAN_SMRY_PRSULC_PRVNT,
      M2250_PLAN_SMRY_PRSULC_TRTMT: $scope.M2250_PLAN_SMRY_PRSULC_TRTMT,
      //functional
      GG0170C_MOBILITY_SOCROC_PERF: $scope.GG0170C_MOBILITY_SOCROC_PERF,
      GG0170C_MOBILITY_DSCHG_GOAL: $scope.GG0170C_MOBILITY_DSCHG_GOAL,
    }
    return oasisObject
  }

  function editPatient() {
    var patientChanges={
      KEYID:$scope.patient.KEYID,
      FNAME: $scope.patient.FNAME,
      LNAME: $scope.patient.LNAME,
      MINITIAL: $scope.patient.MINITIAL,
      // GENDER: $scope.patient.GENDER,
      DOB: $filter("date")($scope.DOB, 'yyyy/MM/dd'),
      SSN: $scope.M0064_SSN,
      ZIP: $scope.M0060_PAT_ZIP,
      STATE: $scope.M0050_PAT_ST
    };
    if ($scope.M0069_PAT_GENDER==='1') {
      patientChanges.GENDER='M';
    } else if ($scope.M0069_PAT_GENDER==='2') {
      patientChanges.GENDER='F';
    }

    if ($scope.M0065_MEDICAID_NUM!==null){
      patientChanges.INSCO=7; //hardcord medicaid KEYID
      patientChanges.INSID=$scope.M0065_MEDICAID_NUM;
    }
    if ($scope.M0063_MEDICARE_NUM!==null){
      patientChanges.INSCO=1; //hardcord medicaid KEYID
      patientChanges.INSID=$scope.M0063_MEDICARE_NUM;
    }
    console.log('patientChanges', patientChanges);
    dataService.edit('patient', patientChanges).then(function (response) {
      console.log('editing patient', response);
      if (response.status==='success'){
        dataService.get('patient', {KEYID:$scope.patient.KEYID}).then(function (data) {
          console.log('updated patient', data.data[0]);
          patientService.set(data.data[0]);
          //if submitting to qa, than redirect to scheduling page
          if ($scope.submitQA===true) {
            $state.go('scheduling', {}, { reload: true });
          } else { //if not, show saved msg and stay on page
            $scope.dataLoading=false;
            // console.log('dataloading=false');
            $scope.dataSaved=true;
            // console.log('dataSaved-true');
          }
        });
      }
    });
  }

  $scope.scrubber = function() {
    $scope.loadingErrorMsg=false;
    $('.error-box').animate({
      'margin-right': '0px'
    }, 200);
    $('.error-box').css({position: 'fixed', top: '120px', right: '0px'});
    console.log("sends form and received errors");

    var oasis=getOasisObject();
    var INPArr=[
      oasis.M1011_14_DAY_INP1_ICD,
      oasis.M1011_14_DAY_INP2_ICD,
      oasis.M1011_14_DAY_INP3_ICD,
      oasis.M1011_14_DAY_INP4_ICD,
      oasis.M1011_14_DAY_INP5_ICD,
      oasis.M1011_14_DAY_INP6_ICD
    ];
    var chgregArr=[
      oasis.M1017_CHGREG_ICD1,
      oasis.M1017_CHGREG_ICD2,
      oasis.M1017_CHGREG_ICD3,
      oasis.M1017_CHGREG_ICD4,
      oasis.M1017_CHGREG_ICD5,
      oasis.M1017_CHGREG_ICD6,
    ];
    var m1021Arr=[
      oasis.M1021_PRIMARY_DIAG_ICD,
    ];
    var m1023Arr=[
      oasis.M1023_OTH_DIAG1_ICD,
      oasis.M1023_OTH_DIAG2_ICD,
      oasis.M1023_OTH_DIAG3_ICD,
      oasis.M1023_OTH_DIAG4_ICD,
      oasis.M1023_OTH_DIAG5_ICD,
    ];
    var m1025aArr=[
      oasis.M1025_OPT_DIAG_ICD_A3,
      oasis.M1025_OPT_DIAG_ICD_A4,
    ];
    var m1025bArr=[
      oasis.M1025_OPT_DIAG_ICD_B3,
      oasis.M1025_OPT_DIAG_ICD_B4,
    ];
    var m1025cArr=[
      oasis.M1025_OPT_DIAG_ICD_C3,
      oasis.M1025_OPT_DIAG_ICD_C4,
    ];
    var m1025dArr=[
      oasis.M1025_OPT_DIAG_ICD_D3,
      oasis.M1025_OPT_DIAG_ICD_D4,
    ];
    var m1025eArr=[
      oasis.M1025_OPT_DIAG_ICD_E3,
      oasis.M1025_OPT_DIAG_ICD_E4,
    ];
    var m1025fArr=[
      oasis.M1025_OPT_DIAG_ICD_F3,
      oasis.M1025_OPT_DIAG_ICD_F4,
    ];

    //format M1011 INP codes for scrubber
    console.log('INPArr',INPArr);
    for (var i = 0; i < INPArr.length; i++) {
      if (INPArr[i]==="") {
        oasis['M1011_14_DAY_INP' + (i+1) + '_ICD']= INPArr[i];
      } else if (INPArr[i]!==undefined && INPArr[i]!==null) {
        if (INPArr[i].length<8) {
          var addCount = 8 - INPArr[i].length;
          for (var j = 0; j < addCount; j++) {
            INPArr[i]+='^';
          }
          oasis['M1011_14_DAY_INP' + (i+1) + '_ICD']= INPArr[i];
        }
      }
    }
    //format M1017 INP codes for scrubber
    console.log('chgregArr',chgregArr);
    for (var l = 0; l < chgregArr.length; l++) {
      if (chgregArr[l]==="") {
        oasis['M1017_CHGREG_ICD' + (l+1)]= chgregArr[l];
      } else if (chgregArr[l]!==undefined && chgregArr[l]!==null) {
        if (chgregArr[l].length<8) {
          var count = 8 - chgregArr[l].length;
          for (var n = 0; n < count; n++) {
            chgregArr[l]+='^';
          }
          oasis['M1017_CHGREG_ICD' + (l+1)]= chgregArr[l];
        }
      }
    }
    //format M1021 INP codes for scrubber
    console.log('m1021Arr',m1021Arr);
    for (var l = 0; l < m1021Arr.length; l++) {
      if (m1021Arr[l]==="") {
        oasis.M1021_PRIMARY_DIAG_ICD= m1021Arr[l];
      } else if (m1021Arr[l]!==undefined && m1021Arr[l]!==null) {
        if (m1021Arr[l].length<8) {
          var count = 8 - m1021Arr[l].length;
          for (var n = 0; n < count; n++) {
            m1021Arr[l]+='^';
          }
          oasis.M1021_PRIMARY_DIAG_ICD= m1021Arr[l];
        }
      }
    }
    //format M1023 INP codes for scrubber
    console.log('m1023Arr', m1023Arr);
    for (var l = 0; l < m1023Arr.length; l++) {
      if (m1023Arr[l]==="") {
        oasis['M1023_OTH_DIAG' + (l+1) + '_ICD']= m1023Arr[l];
      } else if (m1023Arr[l]!==undefined && m1023Arr[l]!==null) {
        if (m1023Arr[l].length<8) {
          var count = 8 - m1023Arr[l].length;
          for (var n = 0; n < count; n++) {
            m1023Arr[l]+='^';
          }
          oasis['M1023_OTH_DIAG' + (l+1) + '_ICD']= m1023Arr[l];
        }
      }
    }
    //format M1025a INP codes for scrubber
    console.log('m1025aArr', m1025aArr);
    for (var l = 0; l < m1025aArr.length; l++) {
      if (m1025aArr[l]==="") {
        oasis['M1025_OPT_DIAG_ICD_A' + (l+3)]= m1025aArr[l];
      } else if (m1025aArr[l]!==undefined && m1025aArr[l]!==null) {
        if (m1025aArr[l].length<8) {
          var count = 8 - m1025aArr[l].length;
          for (var n = 0; n < count; n++) {
            m1025aArr[l]+='^';
          }
          oasis['M1025_OPT_DIAG_ICD_A' + (l+3)]= m1025aArr[l];
        }
      }
    }
    //format M1025b INP codes for scrubber
    console.log('m1025bArr', m1025bArr);
    for (var l = 0; l < m1025bArr.length; l++) {
      if (m1025bArr[l]==="") {
        oasis['M1025_OPT_DIAG_ICD_B' + (l+3)]= m1025bArr[l];
      } else if (m1025bArr[l]!==undefined && m1025bArr[l]!==null) {
        if (m1025bArr[l].length<8) {
          var count = 8 - m1025bArr[l].length;
          for (var n = 0; n < count; n++) {
            m1025bArr[l]+='^';
          }
          oasis['M1025_OPT_DIAG_ICD_B' + (l+3)]= m1025bArr[l];
        }
      }
    }
    //format M1025c INP codes for scrubber
    console.log('m1025cArr', m1025cArr);
    for (var l = 0; l < m1025cArr.length; l++) {
      if (m1025cArr[l]==="") {
        oasis['M1025_OPT_DIAG_ICD_C' + (l+3)]= m1025cArr[l];
      } else if (m1025cArr[l]!==undefined && m1025cArr[l]!==null) {
        if (m1025cArr[l].length<8) {
          var count = 8 - m1025cArr[l].length;
          for (var n = 0; n < count; n++) {
            m1025cArr[l]+='^';
          }
          oasis['M1025_OPT_DIAG_ICD_C' + (l+3)]= m1025cArr[l];
        }
      }
    }
    //format M1025d INP codes for scrubber
    console.log('m1025dArr', m1025dArr);
    for (var l = 0; l < m1025dArr.length; l++) {
      if (m1025dArr[l]==="") {
        oasis['M1025_OPT_DIAG_ICD_D' + (l+3)]= m1025dArr[l];
      } else if (m1025dArr[l]!==undefined && m1025dArr[l]!==null) {
        if (m1025dArr[l].length<8) {
          var count = 8 - m1025dArr[l].length;
          for (var n = 0; n < count; n++) {
            m1025dArr[l]+='^';
          }
          oasis['M1025_OPT_DIAG_ICD_D' + (l+3)]= m1025dArr[l];
        }
      }
    }
    //format M1025e INP codes for scrubber
    console.log('m1025eArr', m1025eArr);
    for (var l = 0; l < m1025eArr.length; l++) {
      if (m1025eArr[l]==="") {
        oasis['M1025_OPT_DIAG_ICD_E' + (l+3)]= m1025eArr[l];
      } else if (m1025eArr[l]!==undefined && m1025eArr[l]!==null) {
        if (m1025eArr[l].length<8) {
          var count = 8 - m1025eArr[l].length;
          for (var n = 0; n < count; n++) {
            m1025eArr[l]+='^';
          }
          oasis['M1025_OPT_DIAG_ICD_E' + (l+3)]= m1025eArr[l];
        }
      }
    }
    //format M1025f INP codes for scrubber
    console.log('m1025fArr', m1025fArr);
    for (var l = 0; l < m1025fArr.length; l++) {
      if (m1025fArr[l]==="") {
        oasis['M1025_OPT_DIAG_ICD_F' + (l+3)]= m1025fArr[l];
      } else if (m1025fArr[l]!==undefined && m1025fArr[l]!==null) {
        if (m1025fArr[l].length<8) {
          var count = 8 - m1025fArr[l].length;
          for (var n = 0; n < count; n++) {
            m1025fArr[l]+='^';
          }
          oasis['M1025_OPT_DIAG_ICD_F' + (l+3)]= m1025fArr[l];
        }
      }
    }
    console.log('scrubbing oasis', oasis);
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['oasisC2_2_20_1'].scrub,
      method: 'GET',
      params: oasis,
      paramSerializer: '$httpParamSerializerJQLike',
    }).then(function(response){
      console.log(response);
      if ($scope.errors!==undefined){
        //clear old errors
        $scope.errors.forEach(function (error) {
          var field=error.Field1Name;
          var element = document.getElementById(field); //get element of error
          var $section = $(element).closest('section'); //find out which section the element is in
          var sectionName=$section.attr('class'); //find name of section

          if (document.getElementById(field)!==null) {
            document.getElementById(field).className = document.getElementById(field).className.replace(new RegExp('(?:^|s)' + 'scrubber' + '(?!S)'), '');
          }
        });
      }
      if (response.data.status==='Success'){
        $scope.loadingErrorMsg=true;

        var scrubObj=JSON.parse(response.data.msg);
        $scope.errors=scrubObj.ErrorResult.Errors;

        //sorting errors
        $scope.errors.sort(function(a,b) {
          var dateA = a.Field1Name.toLowerCase();
          var dateB = b.Field1Name.toLowerCase();

          if(dateA < dateB)
            return -1;
          if(dateA > dateB)
            return 1;
          return 0;
        });
        console.log('$scope.errors', $scope.errors)
        //scrubber errors to show as red
        $scope.errors.forEach(function (error) {
          var field=error.Field1Name;
          var element = document.getElementById(field); //get element of error
          var $section = $(element).closest('section'); //find out which section the element is in
          var sectionName=$section.attr('class'); //find name of section

          if (document.getElementById(field)!==null) {
            document.getElementById(field).className = "scrubber";
          }
        });
      }
    }, function(data) {
      console.log(data);
    });
  };

  $scope.getError=function (error) {
    event.stopPropagation();
    console.log('error', error);
    var field=error.Field1Name;
    var fieldMoleNumber=field.substr(0,5);
    var element = document.getElementById(field); //get element of error

    var $section = $(element).closest('section'); //find out which section the element is in
    var sectionName=$section.attr('class'); //find name of section

    //go to section where element is in
    var current=$scope.currentForm;
    if ($scope.currentForm !== sectionName) {
      $scope.currentForm=sectionName;

      $('.' + current ).fadeOut('fast', function() {
        $('.' + sectionName ).fadeIn('fast');



              //find parent element based on class name
              var focusElement = document.getElementsByClassName(fieldMoleNumber);
              focusElement=focusElement[0];
              console.log(focusElement);
              focusElement.scrollIntoView(true); //scroll to that element
              window.scrollBy(0, -60); //offset by hearder
                  });
    } else {
      var focusElement = document.getElementsByClassName(fieldMoleNumber);
      focusElement=focusElement[0];
      console.log(focusElement);
      focusElement.scrollIntoView(true);
      window.scrollBy(0, -60);
    }
  };



  // function findInsurCo(){
  // //find insurance 1
  //   if ($scope.lastPatAdmit.INS1CO !== null) {
  //     dataService.get('insurance', {'KEYID': $scope.lastPatAdmit.INS1CO}).then(function(ins1co){
  //       $scope.ins1co = ins1co.data[0].NAME.toLowerCase();
  //       $scope.ins1id = $scope.lastPatAdmit.INS1ID;
  //       console.log("insurance1=",$scope.ins1co);
  //
  //       if ($scope.ins1co==='medicare') {//if it's medicare, then populate medicare number
  //         $scope.M0063_MEDICARE_NUM=$scope.lastPatAdmit.INS1ID;
  //         $scope.M0150_CPAY_MCARE_FFS='1'
  //         // $scope.M0063_MEDICARE_NA=0;
  //         console.log('medicare num=', $scope.M0063_MEDICARE_NUM);
  //       } else if ($scope.ins1co==='medicaid') { //if it's medicaid, then populate medicaid number
  //         $scope.M0065_MEDICAID_NUM = $scope.lastPatAdmit.INS1ID;
  //         $scope.M0150_CPAY_MCAID_FFS='1'
  //         // $scope.M0065_MEDICAID_NA=0;
  //         console.log('medicaid num=', $scope.M0065_MEDICAID_NUM);
  //       }
  //       //check if there is a second insurance
  //       if ($scope.lastPatAdmit.INS2CO !== null) {
  //         dataService.get('insurance', {'KEYID': $scope.lastPatAdmit.INS2CO}).then(function(ins2co){
  //           $scope.ins2co = ins2co.data[0].NAME.toLowerCase();
  //           $scope.ins2id = $scope.lastPatAdmit.INS2ID;
  //
  //           console.log("insurance2=", $scope.ins2co);
  //           if ($scope.ins2co==='medicare') {//if it's medicare, then populate medicare number
  //             $scope.M0063_MEDICARE_NUM=$scope.lastPatAdmit.INS2ID;
  //             // $scope.M0063_MEDICARE_NA=0;
  //             console.log('medicare num=', $scope.M0063_MEDICARE_NUM);
  //           } else if ($scope.ins2co==='medicaid') { //if it's medicaid, then populate medicaid number
  //             $scope.M0065_MEDICAID_NUM = $scope.lastPatAdmit.INS2ID;
  //             // $scope.M0065_MEDICAID_NA=0;
  //             console.log('medicaid num=', $scope.M0065_MEDICAID_NUM);
  //           }
  //
  //           console.log('medicare',$scope.M0063_MEDICARE_NUM);
  //           console.log('medicaid', $scope.M0065_MEDICAID_NUM);
  //
  //           if ( ($scope.M0063_MEDICARE_NUM==='') || ($scope.M0063_MEDICARE_NUM===null) || ($scope.M0063_MEDICARE_NUM===undefined)) {
  //             $scope.M0063_MEDICARE_NA='1';
  //             $scope.M0063_MEDICARE_NUM="";
  //           }
  //           if ( ($scope.M0065_MEDICAID_NUM==='') || ($scope.M0065_MEDICAID_NUM===null) || ($scope.M0065_MEDICAID_NUM===undefined) ) {
  //             $scope.M0065_MEDICAID_NA='1';
  //             $scope.M0065_MEDICAID_NUM=''
  //           }
  //         });
  //       } else { //if there isn't a second insurance
  //         console.log('medicare',$scope.M0063_MEDICARE_NUM);
  //         console.log('medicaid', $scope.M0065_MEDICAID_NUM);
  //
  //         if ($scope.M0063_MEDICARE_NUM===null || ($scope.M0063_MEDICARE_NUM===undefined) || ($scope.M0063_MEDICARE_NUM==='')) {
  //           $scope.M0063_MEDICARE_NA='1';
  //           $scope.M0063_MEDICARE_NUM="";
  //         }
  //         if ( ($scope.M0065_MEDICAID_NUM===null) || ($scope.M0065_MEDICAID_NUM===undefined) || ($scope.M0065_MEDICAID_NUM==='') ) {
  //           $scope.M0065_MEDICAID_NA='1';
  //           $scope.M0065_MEDICAID_NUM=''
  //         }
  //       }
  //     });
  //   }
  // }

  /* set empty field */
  $scope.fieldObjs = {
    M0150: [
      'M0150_CPAY_NONE',
      'M0150_CPAY_MCARE_FFS',
      'M0150_CPAY_MCARE_HMO',
      'M0150_CPAY_MCAID_FFS',
      'M0150_CPAY_MCAID_HMO',
      'M0150_CPAY_WRKCOMP',
      'M0150_CPAY_TITLEPGMS',
      'M0150_CPAY_OTH_GOVT',
      'M0150_CPAY_PRIV_INS',  
      'M0150_CPAY_PRIV_HMO',
      'M0150_CPAY_SELFPAY',
      'M0150_CPAY_OTHER',
    ],
    M1000: [
        'M1000_DC_LTC_14_DA',
        'M1000_DC_SNF_14_DA',
        'M1000_DC_IPPS_14_DA',
        'M1000_DC_LTCH_14_DA',
        'M1000_DC_IRF_14_DA',
        'M1000_DC_PSYCH_14_DA',
        'M1000_DC_OTH_14_DA',
      ],
    M1030: [
        'M1030_THH_IV_INFUSION',
        'M1030_THH_PAR_NUTRITION',
        'M1030_THH_ENT_NUTRITION',
      ],
    M1033: [
        'M1033_HOSP_RISK_HSTRY_FALLS',
        'M1033_HOSP_RISK_WEIGHT_LOSS',
        'M1033_HOSP_RISK_MLTPL_HOSPZTN',
        'M1033_HOSP_RISK_MLTPL_ED_VISIT',
        'M1033_HOSP_RISK_MNTL_BHV_DCLN',
        'M1033_HOSP_RISK_COMPLIANCE',
        'M1033_HOSP_RISK_5PLUS_MDCTN',
        'M1033_HOSP_RISK_CRNT_EXHSTN',
        'M1033_HOSP_RISK_OTHR_RISK',
      ],
    M1036: [
        'M1036_RSK_SMOKING',
        'M1036_RSK_OBESITY',
        'M1036_RSK_ALCOHOLISM',
        'M1036_RSK_DRUGS',
      ],
    M1410: [
        'M1410_RESPTX_OXYGEN',
        'M1410_RESPTX_VENTILATOR',
        'M1410_RESPTX_AIRPRESS'
      ],
    M1740: [
      'M1740_BD_MEM_DEFICIT',
      'M1740_BD_IMP_DECISN',
      'M1740_BD_VERBAL',
      'M1740_BD_PHYSICAL',
      'M1740_BD_SOC_INAPPRO',
      'M1740_BD_DELUSIONS',
    ],
    M1018: [
      '',
    ],
  };

  $scope.setEmptyValue = function( field, val ) {
    for ( let i=0; i<$scope.fieldObjs[ field ].length; i++) {
      $scope[ $scope.fieldObjs[ field ][ i ] ] = val;
    }
  }

  $scope.setEmptyValueM1018 = function( mode ) {
    let arr = ['M1018_PRIOR_UR_INCON','M1018_PRIOR_CATH','M1018_PRIOR_INTRACT_PAIN',
    'M1018_PRIOR_IMPR_DECSN','M1018_PRIOR_DISRUPTIVE','M1018_PRIOR_MEM_LOSS','M1018_PRIOR_NONE','M1018_PRIOR_NOCHG_14D'];

    if ( mode === 1 ) {
      for ( let i=0; i<7; i++ ) {
        $scope[ arr[i] ] = '0';
      }
    } else {
      for ( let i=0; i<8; i++ ) {
        $scope[ arr[i] ] = '0';
      }
    }
  }

  function findDoctor(){
    if ($scope.lastPatAdmit.DOCTOR!==null) {
      dataService.get('doctor', {'KEYID':$scope.lastPatAdmit.DOCTOR}).then(function(data){
        $scope.doctor = data.data[0];
        var doctor=data.data[0];
        $scope.M0018_PHYSICIAN_ID=doctor.NPI;
      });
    } else {
      $scope.M0018_PHYSICIAN_ID=null
    }
  }

  $scope.getICDCodes=function(val) {
    console.log('getting icd codes:searching...', val);
    return dataService.search('ICD10Code', {CODE:val, DESCRIPTION:val}).then(function (data) {
      $scope.codes=$filter('limitTo')(data.data, 15);
      console.log('$scope.codes',$scope.codes);
      return $scope.codes.map(function(code) {
        return code;
      });
    });
  };

  $scope.findDIAGDESC = function(field, descField) {
    var codes = $scope.codes;
    for (i=0; i<codes.length; i++) {
      if ($scope[field] === codes[i].CODE) {
        $scope[descField] = codes[i].DESCRIPTION;
        $scope[field] = [codes[i].CODE.slice(0, 3), '.', codes[i].CODE.slice(3,codes[i].CODE.length)].join('');
        break;
      }
    }
  };

  $scope.checkEmptyField=function (field, severity) {
    console.log(field, severity)
    if ($scope[field]==="") {
      console.log(field)
      console.log(severity)
      $scope[severity]=""
    }
  }

  $scope.clear=function (field) {
    $scope[field]=null
  }




   $scope.getPatientMedicationList = function() {
     dataService.get('patientMedication', {PATID: $scope.patient.KEYID}).then(function(data) {
       $scope.activeMedicationList = data.data;
     });
   };

     $scope.getDoctorName = function() {
 		  var physcianid = {NPI: $scope.M0018_PHYSICIAN_ID};
  		  dataService.get('doctor', physcianid).then(function(data){
           $scope.doctorIn = data.data[0];
       });
   };

  $scope.getPatientMedicationList();

  $scope.newPatientMedication = function(event) {
    $scope.newMedication = {};
    arrNewFlag = [];
    $('#addSidenav').width( 550 );
  };

   $scope.showPatientMedication = function(medication, event) {
      $scope.patient = patientService.get();

      $scope.medication = {
        KEYID : medication.KEYID,
        ADMIN_BY : medication.ADMIN_BY,
        CODE : medication.CODE,
        DOSE : medication.DOSE,
        UNIT : medication.UNIT,
        FORM : medication.FORM,
        TAKEN : medication.TAKEN,
        FREQUENCY : medication.FREQUENCY,
        NAME : medication.NAME,
        PHARMACY : medication.PHARMACY,
        PURPOSE : medication.PURPOSE,
        SIDE_EFFECTS : medication.SIDE_EFFECTS,
      };

      if (medication.DATE_STARTED != null)
      $scope.medication.DATE_STARTED = new Date($filter("date")(medication.DATE_STARTED, 'yyyy/MM/dd'));

      if (medication.DATE_DISC != null)
        $scope.medication.DATE_DISC = new Date($filter("date")(medication.DATE_DISC, 'yyyy/MM/dd'));

      $('#editSidenav').width( 550 );
  }

  // Right sidebar for add medication
  $scope.closeNav = function() {
    $('#addSidenav').width( 0 );
    $('#editSidenav').width( 0 );
  }

  $scope.getMedications=function(val) {
    console.log('getting medications:searching...', val);
    return dataService.search('Medication', {CODE:val, DESCRIPTION:val}).then(function (data) {
      $scope.codes=$filter('limitTo')(data.data, 15);
      console.log('$scope.codes',$scope.codes);
      return $scope.codes.map(function(code) {
        return code;
      });
    });
  };

  $scope.addPatientMedication = function(isInvalid) {
    if (isInvalid === true) {
      console.log("Form not valid");
    } else {
      // If the medication is valid, save it.

      var newMedication = {
        ADMIN_BY : $scope.newMedication.ADMIN_BY1,
        CODE : $scope.newMedication.CODE1,
        DATE_DISC : $filter("date")($scope.newMedication.DATE_DISC1, 'yyyy-MM-dd'),
        DATE_STARTED : $filter("date")($scope.newMedication.DATE_STARTED1, 'yyyy-MM-dd'),
        DOSE : $scope.newMedication.DOSE1,
        UNIT : $scope.newMedication.UNIT1,
        FORM : $scope.newMedication.FORM1,
        TAKEN : $scope.newMedication.TAKEN1,
        FREQUENCY : $scope.newMedication.FREQUENCY1,
        NAME : $scope.newMedication.NAME1,
        PATID : $scope.patient.KEYID,
        PHARMACY : $scope.newMedication.PHARMACY1,
        PURPOSE : $scope.newMedication.PURPOSE1,
        SIDE_EFFECTS : $scope.newMedication.SIDE_EFFECTS1,
        IS_SIGNED : 0,
        SIGNER_TITLE : "",
        SIGNER_FNAME : "",
        SIGNER_MI : "",
        SIGNER_LNAME : ""
      };

      dataService.add('patientMedication', newMedication).then(function(response) {
        console.log(response);
        $scope.getPatientMedicationList();
        $scope.closeNav();
      });
    }
  }

  $scope.editPatientMedication = function(isInvalid) {
    if (isInvalid === true) {
      console.log("Form not valid");
    } else {
      // If the medication is valid, save it.

      var editedMedication = {
        KEYID : $scope.medication.KEYID,
        ADMIN_BY : $scope.medication.ADMIN_BY,
        CODE : $scope.medication.CODE,
        DATE_STARTED : $filter("date")($scope.medication.DATE_STARTED, 'yyyy-MM-dd'),
        DATE_DISC : $filter("date")($scope.medication.DATE_DISC, 'yyyy-MM-dd'),
        DOSE : $scope.medication.DOSE,
        UNIT : $scope.medication.UNIT,
        FORM : $scope.medication.FORM,
        TAKEN : $scope.medication.TAKEN,
        FREQUENCY : $scope.medication.FREQUENCY,
        NAME : $scope.medication.NAME,
        PATID : $scope.patient.KEYID,
        PHARMACY : $scope.medication.PHARMACY,
        PURPOSE : $scope.medication.PURPOSE,
        SIDE_EFFECTS : $scope.medication.SIDE_EFFECTS,
        IS_SIGNED : 0,
        SIGNER_TITLE : "",
        SIGNER_FNAME : "",
        SIGNER_MI : "",
        SIGNER_LNAME : ""
      };

      dataService.edit('patientMedication', editedMedication).then(function(response) {
        console.log(response);
        $scope.getPatientMedicationList();
        $scope.closeNav();
      });
    }
  }

  $scope.deletePatientMedication = function() {
    dataService.delete("patientMedication", {KEYID: $scope.medication.KEYID}).then(function(data) {
      console.log(data);
      $scope.getPatientMedicationList();
      $scope.closeNav();
    })
  }

  /** Set style for new medication when hover, focus, error on input */

  function setFormStyle( id, background_color, border_color, color, input_border_color ) {
    $("#" + id).css( 'background-color', background_color );
    $("#" + id).css( 'border-color', border_color );
    $("#" + id).css( 'color', color );
    $("[name='" + id + "']").css( 'border-color', input_border_color );
  }

  var arrNewID = ['PHARMACY1', 'ADMIN_BY1', 'DATE_STARTED1', 'DATE_DISC1', 'NAME1', 'CODE1', 'SIDE_EFFECTS1', 'DOSE1', 'UNIT1', 'FREQUENCY1', 'FORM1', 'TAKEN1', 'PURPOSE1'];
  var arrNewFlag = [];

  for ( let i=0; i<arrNewID.length; i++ ) {

    arrNewFlag.push(false);

    $("[name='" + arrNewID[i] + "']").hover( 
      function() {
        if ( $("[name='" + arrNewID[i] + "']").is(':focus') ) return;
        setFormStyle( arrNewID[i], '#D1CED3', '#D1CED3', '#555', '#8B8B90' );
      },
      function() {
        if ( $("[name='" + arrNewID[i] + "']").is(':focus') ) return;
        if ( ($scope.newMedication[ arrNewID[i] ] !== "" && $scope.newMedication[ arrNewID[i] ] !== undefined) || !arrNewFlag[i] ){
          setFormStyle( arrNewID[i], '#eee', '#ccc', '#555', '#ccc' );
        }
        if ( ($scope.newMedication[ arrNewID[i] ] === "" || $scope.newMedication[ arrNewID[i] ] === undefined ) && arrNewFlag[i] ) {
          if ( arrNewID[i] !== 'DATE_DISC1' ) {
            setFormStyle( arrNewID[i], 'red', 'red', 'white', 'red' );
          } else {
            setFormStyle( arrNewID[i], '#eee', '#ccc', '#555', '#ccc' );
          }
        }
      }
    );

    $("[name='" + arrNewID[i] + "']").focus( function() {
      arrNewFlag[i] = true;
      setFormStyle( arrNewID[i], '#70499B', '#5E2E88', 'white', '#5E2E88' );
    } );

    $("[name='" + arrNewID[i] + "']").blur( function() {
      if ( $scope.newMedication[ arrNewID[i] ] === "" || $scope.newMedication[ arrNewID[i] ] === undefined ) {
        if ( arrNewID[i] !== 'DATE_DISC1' ) {
          setFormStyle( arrNewID[i], 'red', 'red', 'white', 'red' );
        } else {
          setFormStyle( arrNewID[i], '#eee', '#ccc', '#555', '#ccc' );
        }
      } else {
        setFormStyle( arrNewID[i], '#eee', '#ccc', '#555', '#ccc' );
      }
    } );
  }
  /* */

  /** Set style for edit medication when hover, focus, error on input */

  var arrEditID = ['PHARMACY', 'ADMIN_BY', 'DATE_STARTED', 'DATE_DISC', 'NAME', 'CODE', 'SIDE_EFFECTS', 'DOSE', 'UNIT', 'FREQUENCY', 'FORM', 'TAKEN', 'PURPOSE'];
  var arrEditFlag = [];

  for ( let i=0; i<arrEditID.length; i++ ) {

    arrEditFlag.push(false);

    $("[name='" + arrEditID[i] + "']").hover( 
      function() {
        if ( $("[name='" + arrEditID[i] + "']").is(':focus') ) return;
        setFormStyle( arrEditID[i], '#D1CED3', '#D1CED3', '#555', '#8B8B90' );
      },
      function() {
        if ( $("[name='" + arrEditID[i] + "']").is(':focus') ) return;
        if ( ($scope.medication[ arrEditID[i] ] !== "" && $scope.medication[ arrEditID[i] ] !== undefined) || !arrEditFlag[i] ){
          setFormStyle( arrEditID[i], '#eee', '#ccc', '#555', '#ccc' );
        }
        if ( ($scope.medication[ arrEditID[i] ] === "" || $scope.medication[ arrEditID[i] ] === undefined ) && arrEditFlag[i] ) {
          if ( arrEditID[i] !== 'DATE_DISC' ) {
            setFormStyle( arrEditID[i], 'red', 'red', 'white', 'red' );
          } else {
            setFormStyle( arrEditID[i], '#eee', '#ccc', '#555', '#ccc' );
          }
        }
      }
    );

    $("[name='" + arrEditID[i] + "']").focus( function() {
      arrEditFlag[i] = true;
      setFormStyle( arrEditID[i], '#70499B', '#5E2E88', 'white', '#5E2E88' );
    } );

    $("[name='" + arrEditID[i] + "']").blur( function() {
      if (  $scope.medication[ arrEditID[i] ] === "" || $scope.medication[ arrEditID[i] ] === undefined ) {
        if ( arrEditID[i] !== 'DATE_DISC' ) {
          setFormStyle( arrEditID[i], 'red', 'red', 'white', 'red' );
        } else {
          setFormStyle( arrEditID[i], '#eee', '#ccc', '#555', '#ccc' );
        } 
      } else {
        setFormStyle( arrEditID[i], '#eee', '#ccc', '#555', '#ccc' );
      }
    } );
  }
  /* */

  $('#main_div').click( function(e) {
    if ($(e.target).parent('.add-medication').length)
      return;
    if ($(e.target).hasClass('row') || $(e.target).hasClass('col-md-1') || $(e.target).hasClass('medication-info-item') )
      return;
    $scope.closeNav();
  } )

 });
