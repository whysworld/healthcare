app.config(function($stateProvider) {
  $stateProvider.state('resumptionOfCare', {
    url: '/oasisC2/resumptionOfCare',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/oasisC2-ICD10/resumptionOfCare/resumptionOfCare.html',
        controller: 'ResumptionOfCareController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
