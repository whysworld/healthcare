app.config(function($stateProvider) {
  $stateProvider.state('recertificationAssessment', {
    url: '/oasisC2/recertificationAssessment',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/oasisC2-ICD10/recertificationAssessment/recertificationAssessment.html',
        controller: 'RecertificationAssessmentController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
