app.config(function($stateProvider) {
  $stateProvider.state('transferredNotDischarged', {
    url: '/oasisC2/transferredNotDischarged',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/oasisC2-ICD10/transferredNotDischarged/transferredNotDischarged.html',
        controller: 'TransferredNotDischargedController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
