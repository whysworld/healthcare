app.config(function($stateProvider) {
  $stateProvider.state('startOfCare', {
    url: '/oasisC2/startOfCare',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/oasisC2-ICD10/startOfCare/startOfCare.html',
        controller: 'StartOfCareController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
