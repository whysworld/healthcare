app.config(function($stateProvider) {
  $stateProvider.state('deathAtHome', {
    url: '/oasisC2/deathAtHome',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/oasisC2-ICD10/deathAtHome/deathAtHome.html',
        controller: 'DeathAtHomeController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
