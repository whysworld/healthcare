app.config(function($stateProvider) {
  $stateProvider.state('discharge', {
    url: '/oasisC2/discharge',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/oasisC2-ICD10/discharge/discharge.html',
        controller: 'DischargeController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
