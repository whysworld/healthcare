app.config(function($stateProvider) {
  $stateProvider.state('otherFollowUp', {
    url: '/oasisC2/otherFollowUp',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/oasisC2-ICD10/otherFollowUp/otherFollowUp.html',
        controller: 'OtherFollowUpController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
