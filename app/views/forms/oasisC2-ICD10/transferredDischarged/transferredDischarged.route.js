app.config(function($stateProvider) {
  $stateProvider.state('transferredDischarged', {
    url: '/oasisC2/transferredDischarged',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/oasisC2-ICD10/transferredDischarged/transferredDischarged.html',
        controller: 'TransferredDischargedController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
