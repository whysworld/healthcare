app.controller('TransferredDischargedController', function($scope, $filter, $state, $http, CONSTANTS, $httpParamSerializerJQLike, patientService, dataService, formService, agencyService) {

  $scope.patient = patientService.get();

  $scope.form = formService.get();

  $scope.currentForm = 'General';

  $scope.nextForm = function(current, destination) {
    if ($scope.currentForm != destination) {
      $scope.currentForm = destination;
      $('.' + current ).fadeOut('fast', function() {
        $('.' + destination ).fadeIn('fast');
      });
    }
  };

  $scope.activeForm = function(form) {
    if ($scope.currentForm == form) {
      return true;
    }
  };

  $scope.submitSection = function(form, destination) {
    console.log(form.$submitted);
    console.log(form.$valid);
    if(form.$valid) {
      $('#' + $scope.currentForm + ' i').css({color: 'green'});
      var section = $scope.currentForm + 'Complete';

      $scope.nextForm($scope.currentForm, destination);
      console.log(section);
      $scope[section] = true;
      console.log($scope[section]);
    }
    else {
      $('#' + $scope.currentForm + ' i').css({color: 'lightgrey'});
      console.log('Not Valid');
    }
  };


    //populating field per visit date
    $scope.changeVisitDate=false;

    //alerting msg if M0906 date is changed and different from form date
    $scope.checkDate=function () {
      if ($filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd') !==  $filter("date")($scope.form.VISITDATE, 'yyyy/MM/dd')) {
        alert("Note: Changing M0906 will automatically change date of visit.");
        $scope.changeVisitDate=true;
      } else {
        $scope.changeVisitDate=false;
      }
    };

  //get form obj if exists
  dataService.get('oasisC2_2_20_1', {FORMID: $scope.form.FORMID}).then(function(data){
    console.log(data);
    if(data.data.length>0){
      var oasis = data.data[0];

      //******** GENERAL ********//
      $scope.M0010_CCN=oasis.M0010_CCN;
      $scope.M0014_BRANCH_STATE=oasis.M0014_BRANCH_STATE;
      $scope.M0016_BRANCH_ID=oasis.M0016_BRANCH_ID;
      $scope.M0018_PHYSICIAN_ID=oasis.M0018_PHYSICIAN_ID;
      $scope.M0018_PHYSICIAN_UK=oasis.M0018_PHYSICIAN_UK;
      $scope.M0020_PAT_ID=oasis.M0020_PAT_ID;
      $scope.M0030_START_CARE_DT= new Date($filter("date")(oasis.M0030_START_CARE_DT, 'yyyy/MM/dd'));
      $scope.M0032_ROC_DT=new Date($filter("date")(oasis.M0032_ROC_DT, 'yyyy/MM/dd'));
      $scope.M0032_ROC_DT_NA=oasis.M0032_ROC_DT_NA;
      $scope.M0040_PAT_FNAME=oasis.M0040_PAT_FNAME;
      $scope.M0040_PAT_MI=oasis.M0040_PAT_MI;
      $scope.M0040_PAT_LNAME=oasis.M0040_PAT_LNAME;
      $scope.M0040_PAT_SUFFIX=oasis.M0040_PAT_SUFFIX;
      $scope.M0050_PAT_ST=oasis.M0050_PAT_ST;
      $scope.M0060_PAT_ZIP=oasis.M0060_PAT_ZIP;
      $scope.M0063_MEDICARE_NUM=oasis.M0063_MEDICARE_NUM;
      $scope.M0063_MEDICARE_NA=oasis.M0063_MEDICARE_NA;
      $scope.M0064_SSN=oasis.M0064_SSN;
      $scope.M0064_SSN_UK=oasis.M0064_SSN_UK;
      $scope.M0065_MEDICAID_NUM=oasis.M0065_MEDICAID_NUM;
      $scope.M0065_MEDICAID_NA=oasis.M0065_MEDICAID_NA;
      $scope.M0066_PAT_BIRTH_DT= new Date($filter("date")(oasis.M0066_PAT_BIRTH_DT, 'yyyy/MM/dd'));
      $scope.M0069_PAT_GENDER=oasis.M0069_PAT_GENDER;
      $scope.M0140_ETHNIC_AI_AN=oasis.M0140_ETHNIC_AI_AN;
      $scope.M0140_ETHNIC_ASIAN=oasis.M0140_ETHNIC_ASIAN;
      $scope.M0140_ETHNIC_BLACK=oasis.M0140_ETHNIC_BLACK;
      $scope.M0140_ETHNIC_HISP=oasis.M0140_ETHNIC_HISP;
      $scope.M0140_ETHNIC_NH_PI=oasis.M0140_ETHNIC_NH_PI;
      $scope.M0140_ETHNIC_WHITE=oasis.M0140_ETHNIC_WHITE;
      $scope.M0150_CPAY_NONE=oasis.M0150_CPAY_NONE;
      $scope.M0150_CPAY_MCARE_FFS=oasis.M0150_CPAY_MCARE_FFS;
      $scope.M0150_CPAY_MCARE_HMO=oasis.M0150_CPAY_MCARE_HMO;
      $scope.M0150_CPAY_MCAID_FFS=oasis.M0150_CPAY_MCAID_FFS;
      $scope.M0150_CPAY_MCAID_HMO=oasis.M0150_CPAY_MCAID_HMO;
      $scope.M0150_CPAY_WRKCOMP=oasis.M0150_CPAY_WRKCOMP;
      $scope.M0150_CPAY_TITLEPGMS=oasis.M0150_CPAY_TITLEPGMS;
      $scope.M0150_CPAY_OTH_GOVT=oasis.M0150_CPAY_OTH_GOVT;
      $scope.M0150_CPAY_PRIV_INS=oasis.M0150_CPAY_PRIV_INS;
      $scope.M0150_CPAY_PRIV_HMO=oasis.M0150_CPAY_PRIV_HMO;
      $scope.M0150_CPAY_SELFPAY=oasis.M0150_CPAY_SELFPAY;
      $scope.M0150_CPAY_OTHER=oasis.M0150_CPAY_OTHER;
      $scope.M0150_CPAY_UK=oasis.M0150_CPAY_UK;
      //clinical record
      $scope.M0080_ASSESSOR_DISCIPLINE=oasis.M0080_ASSESSOR_DISCIPLINE;
      // $scope.M0090_INFO_COMPLETED_DT=oasis.M0090_INFO_COMPLETED_DT;
      $scope.M0100_ASSMT_REASON=oasis.M0100_ASSMT_REASON;
      $scope.M1041_IN_INFLNZ_SEASON=oasis.M1041_IN_INFLNZ_SEASON;
      $scope.M1046_INFLNZ_RECD_CRNT_SEASON=oasis.M1046_INFLNZ_RECD_CRNT_SEASON;
      $scope.M1051_PVX_RCVD_AGNCY=oasis.M1051_PVX_RCVD_AGNCY;
      $scope.M1056_PVX_RSN_NOT_RCVD_AGNCY=oasis.M1056_PVX_RSN_NOT_RCVD_AGNCY;
      //cardiac
      $scope.M1501_SYMTM_HRT_FAILR_PTNTS=oasis.M1501_SYMTM_HRT_FAILR_PTNTS;
      $scope.M1511_HRT_FAILR_NO_ACTN=oasis.M1511_HRT_FAILR_NO_ACTN;
      $scope.M1511_HRT_FAILR_PHYSN_CNTCT=oasis.M1511_HRT_FAILR_PHYSN_CNTCT;
      $scope.M1511_HRT_FAILR_ER_TRTMT=oasis.M1511_HRT_FAILR_ER_TRTMT;
      $scope.M1511_HRT_FAILR_PHYSN_TRTMT=oasis.M1511_HRT_FAILR_PHYSN_TRTMT;
      $scope.M1511_HRT_FAILR_CLNCL_INTRVTN=oasis.M1511_HRT_FAILR_CLNCL_INTRVTN;
      $scope.M1511_HRT_FAILR_CARE_PLAN_CHG=oasis.M1511_HRT_FAILR_CARE_PLAN_CHG;
      //medication
      $scope.M2005_MDCTN_INTRVTN=oasis.M2005_MDCTN_INTRVTN;
      $scope.M2016_DRUG_EDCTN_INTRVTN=oasis.M2016_DRUG_EDCTN_INTRVTN;
      //emergent care
      $scope.M2301_EMER_USE_AFTR_LAST_ASMT=oasis.M2301_EMER_USE_AFTR_LAST_ASMT;
      $scope.M2310_ECR_MEDICATION=oasis.M2310_ECR_MEDICATION;
      $scope.M2310_ECR_INJRY_BY_FALL=oasis.M2310_ECR_INJRY_BY_FALL;
      $scope.M2310_ECR_RSPRTRY_INFCTN=oasis.M2310_ECR_RSPRTRY_INFCTN;
      $scope.M2310_ECR_RSPRTRY_OTHR=oasis.M2310_ECR_RSPRTRY_OTHR;
      $scope.M2310_ECR_HRT_FAILR=oasis.M2310_ECR_HRT_FAILR;
      $scope.M2310_ECR_CRDC_DSRTHM=oasis.M2310_ECR_CRDC_DSRTHM;
      $scope.M2310_ECR_MI_CHST_PAIN=oasis.M2310_ECR_MI_CHST_PAIN;
      $scope.M2310_ECR_OTHR_HRT_DEASE=oasis.M2310_ECR_OTHR_HRT_DEASE;
      $scope.M2310_ECR_STROKE_TIA=oasis.M2310_ECR_STROKE_TIA;
      $scope.M2310_ECR_HYPOGLYC=oasis.M2310_ECR_HYPOGLYC;
      $scope.M2310_ECR_GI_PRBLM=oasis.M2310_ECR_GI_PRBLM;
      $scope.M2310_ECR_DHYDRTN_MALNTR=oasis.M2310_ECR_DHYDRTN_MALNTR;
      $scope.M2310_ECR_UTI=oasis.M2310_ECR_UTI;
      $scope.M2310_ECR_CTHTR_CMPLCTN=oasis.M2310_ECR_CTHTR_CMPLCTN;
      $scope.M2310_ECR_WND_INFCTN_DTRORTN=oasis.M2310_ECR_WND_INFCTN_DTRORTN;
      $scope.M2310_ECR_UNCNTLD_PAIN=oasis.M2310_ECR_UNCNTLD_PAIN;
      $scope.M2310_ECR_MENTL_BHVRL_PRBLM=oasis.M2310_ECR_MENTL_BHVRL_PRBLM;
      $scope.M2310_ECR_DVT_PULMNRY=oasis.M2310_ECR_DVT_PULMNRY;
      $scope.M2310_ECR_OTHER=oasis.M2310_ECR_OTHER;
      $scope.M2310_ECR_UNKNOWN=oasis.M2310_ECR_UNKNOWN;
      $scope.M2401_INTRVTN_SMRY_DBTS_FT=oasis.M2401_INTRVTN_SMRY_DBTS_FT;
      $scope.M2401_INTRVTN_SMRY_FALL_PRVNT=oasis.M2401_INTRVTN_SMRY_FALL_PRVNT;
      $scope.M2401_INTRVTN_SMRY_DPRSN=oasis.M2401_INTRVTN_SMRY_DPRSN;
      $scope.M2401_INTRVTN_SMRY_PAIN_MNTR=oasis.M2401_INTRVTN_SMRY_PAIN_MNTR;
      $scope.M2401_INTRVTN_SMRY_PRSULC_PRVN=oasis.M2401_INTRVTN_SMRY_PRSULC_PRVN;
      $scope.M2401_INTRVTN_SMRY_PRSULC_WET=oasis.M2401_INTRVTN_SMRY_PRSULC_WET;
      $scope.M2410_INPAT_FACILITY=oasis.M2410_INPAT_FACILITY;
      $scope.M2430_HOSP_MED=oasis.M2430_HOSP_MED;
      $scope.M2430_HOSP_INJRY_BY_FALL=oasis.M2430_HOSP_INJRY_BY_FALL;
      $scope.M2430_HOSP_RSPRTRY_INFCTN=oasis.M2430_HOSP_RSPRTRY_INFCTN;
      $scope.M2430_HOSP_RSPRTRY_OTHR=oasis.M2430_HOSP_RSPRTRY_OTHR;
      $scope.M2430_HOSP_HRT_FAILR=oasis.M2430_HOSP_HRT_FAILR;
      $scope.M2430_HOSP_CRDC_DSRTHM=oasis.M2430_HOSP_CRDC_DSRTHM;
      $scope.M2430_HOSP_MI_CHST_PAIN=oasis.M2430_HOSP_MI_CHST_PAIN;
      $scope.M2430_HOSP_OTHR_HRT_DEASE=oasis.M2430_HOSP_OTHR_HRT_DEASE;
      $scope.M2430_HOSP_STROKE_TIA=oasis.M2430_HOSP_STROKE_TIA;
      $scope.M2430_HOSP_HYPOGLYC=oasis.M2430_HOSP_HYPOGLYC;
      $scope.M2430_HOSP_GI_PRBLM=oasis.M2430_HOSP_GI_PRBLM;
      $scope.M2430_HOSP_DHYDRTN_MALNTR=oasis.M2430_HOSP_DHYDRTN_MALNTR;
      $scope.M2430_HOSP_UR_TRACT=oasis.M2430_HOSP_UR_TRACT;
      $scope.M2430_HOSP_CTHTR_CMPLCTN=oasis.M2430_HOSP_CTHTR_CMPLCTN;
      $scope.M2430_HOSP_WND_INFCTN=oasis.M2430_HOSP_WND_INFCTN;
      $scope.M2430_HOSP_PAIN=oasis.M2430_HOSP_PAIN;
      $scope.M2430_HOSP_MENTL_BHVRL_PRBLM=oasis.M2430_HOSP_MENTL_BHVRL_PRBLM;
      $scope.M2430_HOSP_DVT_PULMNRY=oasis.M2430_HOSP_DVT_PULMNRY;
      $scope.M2430_HOSP_SCHLD_TRTMT=oasis.M2430_HOSP_SCHLD_TRTMT;
      $scope.M2430_HOSP_OTHER=oasis.M2430_HOSP_OTHER;
      $scope.M2430_HOSP_UK=oasis.M2430_HOSP_UK;
      $scope.M0903_LAST_HOME_VISIT=oasis.M0903_LAST_HOME_VISIT;
      $scope.M0906_DC_TRAN_DTH_DT=oasis.M0906_DC_TRAN_DTH_DT;

      if (oasis.M0090_INFO_COMPLETED_DT !== null) {
        $scope.M0090_INFO_COMPLETED_DT = new Date($filter("date")(oasis.M0090_INFO_COMPLETED_DT, 'yyyy/MM/dd'))
      }
      if (oasis.M0903_LAST_HOME_VISIT !== null) {
        $scope.M0903_LAST_HOME_VISIT = new Date($filter("date")(oasis.M0903_LAST_HOME_VISIT, 'yyyy/MM/dd'))
      }
      if (oasis.M0906_DC_TRAN_DTH_DT !== null) {
        $scope.M0906_DC_TRAN_DTH_DT = new Date($filter("date")(oasis.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd'))
      }
      if (oasis.M0030_START_CARE_DT===null){
        $scope.M0030_START_CARE_DT="";
      } else {
        $scope.M0030_START_CARE_DT=new Date($filter("date")(oasis.M0030_START_CARE_DT, 'yyyy/MM/dd'));
      }
      if (oasis.M0032_ROC_DT===null){
        $scope.M0032_ROC_DT="";
      } else {
        $scope.M0032_ROC_DT=new Date($filter("date")(oasis.M0032_ROC_DT, 'yyyy/MM/dd'));
      }
      if (oasis.M0066_PAT_BIRTH_DT===null){
        $scope.M0066_PAT_BIRTH_DT="";
      } else {
        $scope.M0066_PAT_BIRTH_DT=new Date($filter("date")(oasis.M0066_PAT_BIRTH_DT, 'yyyy/MM/dd'));
      }

    } else { //if there is no oasis, populate form using patient info
      console.log('populating form using patient info')
      $scope.form.OBJKEYID=null;
      $scope.M0020_PAT_ID=$scope.patient.PATIENTID;
      $scope.M0040_PAT_FNAME=$scope.patient.FNAME;
      $scope.M0040_PAT_MI=$scope.patient.MINITIAL;
      $scope.M0040_PAT_LNAME=$scope.patient.LNAME;
      $scope.M0050_PAT_ST=$scope.patient.STATE;
      $scope.M0060_PAT_ZIP=$scope.patient.ZIP;
      $scope.M0064_SSN=$scope.patient.SSN;
      //converting patient DOB
      $scope.patient.DOB = $filter("date")($scope.patient.DOB, 'yyyy/MM/dd');
      $scope.DOB = new Date($scope.patient.DOB);
      //converting patient gender fields
      if($scope.patient.GENDER ==='M') {
        $scope.M0069_PAT_GENDER = '1';
      } else if ($scope.patient.GENDER === 'F') {
         $scope.M0069_PAT_GENDER = '2';
      }
      //converting ethnicity
      switch ($scope.patient.M0140) {
        case "1":
          $scope.M0140_ETHNIC_AI_AN='1';
          break;
        case "2":
          $scope.M0140_ETHNIC_ASIAN='1';
          break;
        case "3":
          $scope.M0140_ETHNIC_BLACK='1';
          break;
        case "4":
          $scope.M0140_ETHNIC_HISP='1';
          break;
        case "5":
          $scope.M0140_ETHNIC_NH_PI='1';
          break;
        case "6":
          $scope.M0140_ETHNIC_WHITE='1';
          break;
        default:
          $scope.M0140_ETHNIC_AI_AN='0';
          $scope.M0140_ETHNIC_ASIAN='0';
          $scope.M0140_ETHNIC_BLACK='0';
          $scope.M0140_ETHNIC_HISP='0';
          $scope.M0140_ETHNIC_NH_PI='0';
          $scope.M0140_ETHNIC_WHITE='0';
      }
      //getting agency info
      $scope.agency=agencyService.get();
      console.log('agency config', $scope.agency);
      $scope.M0014_BRANCH_STATE=$scope.agency.STATE;
      $scope.M0016_BRANCH_ID=$scope.agency.BRANCH;
      $scope.M0010_CCN=$scope.agency.PROVIDERNUM;
      //check if insurance is medicare
      if ($scope.patient.INSCO===1) {
        $scope.M0150_CPAY_MCARE_FFS='1'
        $scope.M0063_MEDICARE_NUM=$scope.patient.INSID;
        $scope.M0063_MEDICARE_NA='0'
      } else {
        $scope.M0063_MEDICARE_NA='1'
        $scope.M0150_CPAY_MCARE_FFS='0'
      }
      //check if insurance is medicaid
      if ($scope.patient.INSCO===7) {
        $scope.M0150_CPAY_MCAID_FFS='1'
        $scope.M0065_MEDICAID_NUM=$scope.patient.INSID;
        $scope.M0065_MEDICAID_NA='0'
      } else {
        $scope.M0065_MEDICAID_NA='1'
        $scope.M0150_CPAY_MCAID_FFS='0'
      }
      //assigning  info complete date
      $scope.M0090_INFO_COMPLETED_DT = new Date($filter("date")($scope.form.VISITDATE, 'yyyy/MM/dd'));
      //assign assessment reason
      $scope.M0100_ASSMT_REASON='07';
      //assign M0906_DC_TRAN_DTH_DT
      $scope.M0906_DC_TRAN_DTH_DT = new Date($filter("date")($scope.form.VISITDATE, 'yyyy/MM/dd'));
      //assign soc date
      dataService.get('admission', {'KEYID':$scope.patient.LSTADMIT}).then(function(data){
        var admit = data.data[0];
        //get soc date and populate
        var soc = $filter("date")(admit.SOCDATE, 'yyyy/MM/dd');
        $scope.M0030_START_CARE_DT = new Date(soc);
        //get NPI and insurance info
        $scope.lastPatAdmit = data.data[0];
        console.log('form admit', $scope.lastPatAdmit)
        // findInsurCo();
        findDoctor();
      });
    }
  });

  $scope.submitToQA=function () {
    if (!confirm("No changes are allowed once submitted to QA, are you sure you want to submit?")) { //no don't submit to qa
      console.log('no, do not submit to qa');
      event.preventDefault();
      $scope.submitQA=false;
    } else { //yes submit to qa
      $scope.submitQA=true;
      $scope.saveAndContinueLater();
    }
  };

  $scope.saveAndContinueLater = function() {
    $scope.dataSaved=false;
    if (($scope.M0906_DC_TRAN_DTH_DT===null) || ($scope.M0906_DC_TRAN_DTH_DT===undefined)) {
      alert('M0906 Field must be entered.')
    } else {
      //check to see if form has objkeyid
      dataService.get('form', {KEYID:$scope.form.FORMID}).then(function (data) {
        var form = data.data[0];
        console.log(form);
        //if objkeyid is null, then save form, else edit form
        if (form.OBJKEYID === null) {
          $scope.createTransferredDischarge();
          $scope.dataLoading = true;
        } else {
          console.log('save edited oasis');
          editTransferredDischarge(form.OBJKEYID);
          $scope.dataLoading = true;
        }
      });
    };
  }

  function getOasisObject() {
    var oasisObject = {
      FORMID: $scope.form.FORMID,
      FORMTYPE: 7,
      ADMITKEYID: $scope.form.ADMITKEYID,
      VISITID: $scope.form.VISITID,
      ASMT_SYS_CD: 'OASIS',
      ITM_SBST_CD: '07',
      ITM_SET_VRSN_CD: 'C2-012017',
      SPEC_VRSN_CD: '2.20',
      CORRECTION_NUM: '00',
      STATE_CD: 'NV',
      HHA_AGENCY_ID: 'FAKE',
      SFW_ID: '111111111',
      SFW_NAME: 'KRYPTONITE',
      SFW_EMAIL_ADR: 'TEST@TEST.COM',
      SFW_PROD_NAME: 'KRYPTONITE',
      SFW_PROD_VRSN_CD: '1',
      NATL_PRVDR_ID: '1111111111',
      TRANS_TYPE_CD: 1,
      //******** GENERAL ********//
      M0010_CCN: $scope.M0010_CCN,
      M0014_BRANCH_STATE: $scope.M0014_BRANCH_STATE,
      M0016_BRANCH_ID: $scope.M0016_BRANCH_ID,
      M0018_PHYSICIAN_ID: $scope.M0018_PHYSICIAN_ID,
      M0018_PHYSICIAN_UK: $scope.M0018_PHYSICIAN_UK,
      M0020_PAT_ID: $scope.M0020_PAT_ID,
      M0030_START_CARE_DT: $filter("date")($scope.M0030_START_CARE_DT, 'yyyy-MM-dd'),
      M0032_ROC_DT: $filter("date")($scope.M0032_ROC_DT, 'yyyy-MM-dd'),
      M0032_ROC_DT_NA: $scope.M0032_ROC_DT_NA,
      M0040_PAT_FNAME: $scope.M0040_PAT_FNAME,
      M0040_PAT_MI: $scope.M0040_PAT_MI,
      M0040_PAT_LNAME: $scope.M0040_PAT_LNAME,
      M0040_PAT_SUFFIX: $scope.M0040_PAT_SUFFIX,
      M0050_PAT_ST: $scope.M0050_PAT_ST,
      M0060_PAT_ZIP: $scope.M0060_PAT_ZIP,
      M0063_MEDICARE_NUM: $scope.M0063_MEDICARE_NUM,
      M0063_MEDICARE_NA: $scope.M0063_MEDICARE_NA,
      M0064_SSN: $scope.M0064_SSN,
      M0064_SSN_UK: $scope.M0064_SSN_UK,
      M0065_MEDICAID_NUM: $scope.M0065_MEDICAID_NUM,
      M0065_MEDICAID_NA: $scope.M0065_MEDICAID_NA,
      M0066_PAT_BIRTH_DT: $filter("date")($scope.DOB, 'yyyy-MM-dd'),
      M0069_PAT_GENDER: $scope.M0069_PAT_GENDER,
      M0140_ETHNIC_AI_AN: $scope.M0140_ETHNIC_AI_AN,
      M0140_ETHNIC_ASIAN: $scope.M0140_ETHNIC_ASIAN,
      M0140_ETHNIC_BLACK: $scope.M0140_ETHNIC_BLACK,
      M0140_ETHNIC_HISP: $scope.M0140_ETHNIC_HISP,
      M0140_ETHNIC_NH_PI: $scope.M0140_ETHNIC_NH_PI,
      M0140_ETHNIC_WHITE: $scope.M0140_ETHNIC_WHITE,
      M0150_CPAY_NONE: $scope.M0150_CPAY_NONE,
      M0150_CPAY_MCARE_FFS: $scope.M0150_CPAY_MCARE_FFS,
      M0150_CPAY_MCARE_HMO: $scope.M0150_CPAY_MCARE_HMO,
      M0150_CPAY_MCAID_FFS: $scope.M0150_CPAY_MCAID_FFS,
      M0150_CPAY_MCAID_HMO: $scope.M0150_CPAY_MCAID_HMO,
      M0150_CPAY_WRKCOMP: $scope.M0150_CPAY_WRKCOMP,
      M0150_CPAY_TITLEPGMS: $scope.M0150_CPAY_TITLEPGMS,
      M0150_CPAY_OTH_GOVT: $scope.M0150_CPAY_OTH_GOVT,
      M0150_CPAY_PRIV_INS: $scope.M0150_CPAY_PRIV_INS,
      M0150_CPAY_PRIV_HMO: $scope.M0150_CPAY_PRIV_HMO,
      M0150_CPAY_SELFPAY: $scope.M0150_CPAY_SELFPAY,
      M0150_CPAY_OTHER: $scope.M0150_CPAY_OTHER,
      M0150_CPAY_UK: $scope.M0150_CPAY_UK,
      //clinical record
      M0080_ASSESSOR_DISCIPLINE: $scope.M0080_ASSESSOR_DISCIPLINE,
      M0090_INFO_COMPLETED_DT: $filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy-MM-dd'),
      M0100_ASSMT_REASON: '07',
      //patient history
      M1041_IN_INFLNZ_SEASON: $scope.M1041_IN_INFLNZ_SEASON,
      M1046_INFLNZ_RECD_CRNT_SEASON: $scope.M1046_INFLNZ_RECD_CRNT_SEASON,
      M1051_PVX_RCVD_AGNCY: $scope.M1051_PVX_RCVD_AGNCY,
      M1056_PVX_RSN_NOT_RCVD_AGNCY: $scope.M1056_PVX_RSN_NOT_RCVD_AGNCY,
      //cardiac
      M1501_SYMTM_HRT_FAILR_PTNTS: $scope.M1501_SYMTM_HRT_FAILR_PTNTS,
      M1511_HRT_FAILR_NO_ACTN: $scope.M1511_HRT_FAILR_NO_ACTN,
      M1511_HRT_FAILR_PHYSN_CNTCT: $scope.M1511_HRT_FAILR_PHYSN_CNTCT,
      M1511_HRT_FAILR_ER_TRTMT: $scope.M1511_HRT_FAILR_ER_TRTMT,
      M1511_HRT_FAILR_PHYSN_TRTMT: $scope.M1511_HRT_FAILR_PHYSN_TRTMT,
      M1511_HRT_FAILR_CLNCL_INTRVTN: $scope.M1511_HRT_FAILR_CLNCL_INTRVTN,
      M1511_HRT_FAILR_CARE_PLAN_CHG: $scope.M1511_HRT_FAILR_CARE_PLAN_CHG,
      //medication
      M2005_MDCTN_INTRVTN: $scope.M2005_MDCTN_INTRVTN,
      M2016_DRUG_EDCTN_INTRVTN: $scope.M2016_DRUG_EDCTN_INTRVTN,
      //emergent care
      M2301_EMER_USE_AFTR_LAST_ASMT: $scope.M2301_EMER_USE_AFTR_LAST_ASMT,
      M2310_ECR_MEDICATION: $scope.M2310_ECR_MEDICATION,
      M2310_ECR_INJRY_BY_FALL: $scope.M2310_ECR_INJRY_BY_FALL,
      M2310_ECR_RSPRTRY_INFCTN: $scope.M2310_ECR_RSPRTRY_INFCTN,
      M2310_ECR_RSPRTRY_OTHR: $scope.M2310_ECR_RSPRTRY_OTHR,
      M2310_ECR_HRT_FAILR: $scope.M2310_ECR_HRT_FAILR,
      M2310_ECR_CRDC_DSRTHM: $scope.M2310_ECR_CRDC_DSRTHM,
      M2310_ECR_MI_CHST_PAIN: $scope.M2310_ECR_MI_CHST_PAIN,
      M2310_ECR_OTHR_HRT_DEASE: $scope.M2310_ECR_OTHR_HRT_DEASE,
      M2310_ECR_STROKE_TIA: $scope.M2310_ECR_STROKE_TIA,
      M2310_ECR_HYPOGLYC: $scope.M2310_ECR_HYPOGLYC,
      M2310_ECR_GI_PRBLM: $scope.M2310_ECR_GI_PRBLM,
      M2310_ECR_DHYDRTN_MALNTR: $scope.M2310_ECR_DHYDRTN_MALNTR,
      M2310_ECR_UTI: $scope.M2310_ECR_UTI,
      M2310_ECR_CTHTR_CMPLCTN: $scope.M2310_ECR_CTHTR_CMPLCTN,
      M2310_ECR_WND_INFCTN_DTRORTN: $scope.M2310_ECR_WND_INFCTN_DTRORTN,
      M2310_ECR_UNCNTLD_PAIN: $scope.M2310_ECR_UNCNTLD_PAIN,
      M2310_ECR_MENTL_BHVRL_PRBLM: $scope.M2310_ECR_MENTL_BHVRL_PRBLM,
      M2310_ECR_DVT_PULMNRY: $scope.M2310_ECR_DVT_PULMNRY,
      M2310_ECR_OTHER: $scope.M2310_ECR_OTHER,
      M2310_ECR_UNKNOWN: $scope.M2310_ECR_UNKNOWN,
      M2401_INTRVTN_SMRY_DBTS_FT: $scope.M2401_INTRVTN_SMRY_DBTS_FT,
      M2401_INTRVTN_SMRY_FALL_PRVNT: $scope.M2401_INTRVTN_SMRY_FALL_PRVNT,
      M2401_INTRVTN_SMRY_DPRSN: $scope.M2401_INTRVTN_SMRY_DPRSN,
      M2401_INTRVTN_SMRY_PAIN_MNTR: $scope.M2401_INTRVTN_SMRY_PAIN_MNTR,
      M2401_INTRVTN_SMRY_PRSULC_PRVN: $scope.M2401_INTRVTN_SMRY_PRSULC_PRVN,
      M2401_INTRVTN_SMRY_PRSULC_WET: $scope.M2401_INTRVTN_SMRY_PRSULC_WET,
      M2410_INPAT_FACILITY: $scope.M2410_INPAT_FACILITY,
      M2430_HOSP_MED:$scope.M2430_HOSP_MED,
      M2430_HOSP_INJRY_BY_FALL: $scope.M2430_HOSP_INJRY_BY_FALL,
      M2430_HOSP_RSPRTRY_INFCTN: $scope.M2430_HOSP_RSPRTRY_INFCTN,
      M2430_HOSP_RSPRTRY_OTHR: $scope.M2430_HOSP_RSPRTRY_OTHR,
      M2430_HOSP_HRT_FAILR: $scope.M2430_HOSP_HRT_FAILR,
      M2430_HOSP_CRDC_DSRTHM: $scope.M2430_HOSP_CRDC_DSRTHM,
      M2430_HOSP_MI_CHST_PAIN: $scope.M2430_HOSP_MI_CHST_PAIN,
      M2430_HOSP_OTHR_HRT_DEASE: $scope.M2430_HOSP_OTHR_HRT_DEASE,
      M2430_HOSP_STROKE_TIA: $scope.M2430_HOSP_STROKE_TIA,
      M2430_HOSP_HYPOGLYC: $scope.M2430_HOSP_HYPOGLYC,
      M2430_HOSP_GI_PRBLM: $scope.M2430_HOSP_GI_PRBLM,
      M2430_HOSP_DHYDRTN_MALNTR: $scope.M2430_HOSP_DHYDRTN_MALNTR,
      M2430_HOSP_UR_TRACT: $scope.M2430_HOSP_UR_TRACT,
      M2430_HOSP_CTHTR_CMPLCTN: $scope.M2430_HOSP_CTHTR_CMPLCTN,
      M2430_HOSP_WND_INFCTN: $scope.M2430_HOSP_WND_INFCTN,
      M2430_HOSP_PAIN: $scope.M2430_HOSP_PAIN,
      M2430_HOSP_MENTL_BHVRL_PRBLM: $scope.M2430_HOSP_MENTL_BHVRL_PRBLM,
      M2430_HOSP_DVT_PULMNRY: $scope.M2430_HOSP_DVT_PULMNRY,
      M2430_HOSP_SCHLD_TRTMT: $scope.M2430_HOSP_SCHLD_TRTMT,
      M2430_HOSP_OTHER: $scope.M2430_HOSP_OTHER,
      M2430_HOSP_UK: $scope.M2430_HOSP_UK,
      M0903_LAST_HOME_VISIT: $filter("date")($scope.M0903_LAST_HOME_VISIT, 'yyyy-MM-dd'),
      M0906_DC_TRAN_DTH_DT: $filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy-MM-dd')
    };
    return oasisObject
  }

  $scope.createTransferredDischarge = function() {
    var newTransferredDischarge=getOasisObject();
    console.log("newTransferredDischarge", newTransferredDischarge);
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['oasisC2_2_20_1'].add,
      method: 'POST',
      data: $httpParamSerializerJQLike(newTransferredDischarge),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function(response){
          console.log(response);
          // console.log(response.data['form keyid']);
          var formkeyid = parseInt(response.data['form keyid']);
          //edit Patient
          editPatient();
          //edit form to contain objectkeyid
          var form = {
            KEYID: formkeyid,
            OBJKEYID: response.data.keyid,
            STATUS: 2,
            ASSIGNEDTO:$scope.form.ASSIGNEDTO
          };
          //if submitting to QA, change status to 1 for complete
          if ($scope.submitQA===true) {
            form.STATUS=1;
          }
          if ($scope.changeVisitDate) {
            form.FORMDATE=$filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')
          }
          dataService.edit('form', form).then(function (response) {
            console.log(response);
          });
          //edit admit to update patient status, and include discharge date
          //get currentAdmit
          editAdmit();


          //edit current visit to PERFORMED=1 and edit visit date if changed
          dataService.get('form', {KEYID: $scope.form.FORMID}).then(function (data) {
            var form = data.data[0];
            console.log(form);
            var editVisit= {
              KEYID: form.VISITID,
              PERFORMED: 1
            }

            if ($scope.changeVisitDate) {
              editVisit.VISITDATE=$filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')
            }

            // console.log(visitid);
            dataService.edit('visit', editVisit).then(function (response) {
              console.log(response);
            });
          });

          //edit episode to include DC date
          //Get current episode
          editEpisode();


          //edit patient status
          dataService.edit('patient', {'KEYID': $scope.patient.KEYID, 'PATSTATUS': 'D'}).then(function (response) {
            console.log(response);
            //reset patient
            dataService.get('patient', {KEYID: $scope.patient.KEYID}).then(function (data) {
              patientService.set(data.data[0]);
              $state.go('patientProfile')
            });
          });

      }, function(data) {
        console.log(data);
    });
  };


  function editTransferredDischarge(objkeyid) {
    //converting all empty strings to null so oasis will save edits
    if ($scope.M0032_ROC_DT==="" || $scope.M0032_ROC_DT===undefined){
      $scope.M0032_ROC_DT=null;
    } else {
      $scope.M0032_ROC_DT=$filter("date")($scope.M0032_ROC_DT, 'yyyy/MM/dd');
    }
    var editTransferredDischargeOasis = getOasisObject();
    editTransferredDischargeOasis.KEYID=objkeyid;

    console.log("edit Discharge", editTransferredDischargeOasis);
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['oasisC2_2_20_1'].edit,
      method: 'POST',
      // params: CONSTANTS.api.actions['oasisC2_2_20_1'].edit,
      data: $httpParamSerializerJQLike(editTransferredDischargeOasis),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function(msg){
      console.log(msg)
      //edit patient
      editPatient();
      //edit visit date if changed
      if ($scope.changeVisitDate) {
        dataService.get('form', {KEYID: $scope.form.FORMID}).then(function (data) {
          var form = data.data[0];
          console.log(form);
          var editVisit= {
            KEYID: form.VISITID,
            VISITDATE: $filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')
          };
          dataService.edit('visit', editVisit).then(function (response) {
            console.log(response);
          });
        });
      }

      //if submitting to QA, change status to 1 for complete,
      if ($scope.submitQA===true) {
        var form = {
          KEYID: $scope.form.FORMID,
          STATUS: 1,
          ASSIGNEDTO:$scope.form.ASSIGNEDTO
        };
        if ($scope.changeVisitDate) {
          form.FORMDATE=$filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd')
        }
        dataService.edit('form', form).then(function (response) {
          console.log(response);
        });
      }

      });
      editAdmit(); //edit Admit to update discharge date
      editEpisode(); //edit episode to update discharge date
      $state.go('scheduling');
  }


  function editAdmit() {
    dataService.get('admission', {'PATKEYID':$scope.patient.KEYID}).then(function(data){
      var admitArr = data.data;
      var currentAdmit = admitArr.slice(-1).pop();
      $scope.currentAdmitKEYID = currentAdmit.KEYID;

      //update admit object
      var updateAdmit = {
        KEYID: $scope.currentAdmitKEYID,
        PATSTATUS: 'D',
        DCDATE: $filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd'),
      };
      console.log(updateAdmit);
      dataService.edit('admission', updateAdmit).then(function(response) {
        console.log(response);
      });
    });
  }

  function editEpisode() {
    dataService.get('episode', {'PATKEYID':$scope.patient.KEYID}).then(function(data){
      var episodeArr = data.data;
      var currentEpisode = episodeArr.slice(-1).pop();
      console.log('current episode', currentEpisode);

      //update episode object
      var updateEpisode = {
        KEYID: currentEpisode.KEYID,
        DISCHARGED: 1,
        DCDATE: $filter("date")($scope.M0906_DC_TRAN_DTH_DT, 'yyyy/MM/dd'),
      };
      console.log(updateEpisode);
      dataService.edit('episode', updateEpisode).then(function(response) {
        console.log(response);
      });
    });
  }


  function editPatient() {
    var patientChanges={
      KEYID:$scope.patient.KEYID,
      FNAME: $scope.patient.FNAME,
      LNAME: $scope.patient.LNAME,
      MINITIAL: $scope.patient.MINITIAL,
      // GENDER: $scope.patient.GENDER,
      DOB: $filter("date")($scope.DOB, 'yyyy/MM/dd'),
      SSN: $scope.M0064_SSN,
      ZIP: $scope.M0060_PAT_ZIP,
      STATE: $scope.M0050_PAT_ST
    };
    if ($scope.M0069_PAT_GENDER==='1') {
      patientChanges.GENDER='M';
    } else if ($scope.M0069_PAT_GENDER==='2') {
      patientChanges.GENDER='F';
    }

    if ($scope.M0065_MEDICAID_NUM!==null){
      patientChanges.INSCO=7; //hardcord medicaid KEYID
      patientChanges.INSID=$scope.M0065_MEDICAID_NUM;
    }
    if ($scope.M0063_MEDICARE_NUM!==null){
      patientChanges.INSCO=1; //hardcord medicaid KEYID
      patientChanges.INSID=$scope.M0063_MEDICARE_NUM;
    }
    console.log('patientChanges', patientChanges);
    dataService.edit('patient', patientChanges).then(function (response) {
      console.log('editing patient', response);
      if (response.status==='success'){
        dataService.get('patient', {KEYID:$scope.patient.KEYID}).then(function (data) {
          console.log('updated patient', data.data[0]);
          patientService.set(data.data[0]);
          //if submitting to qa, than redirect to scheduling page
          if ($scope.submitQA===true) {
            $state.go('scheduling', {}, { reload: true });
          } else { //if not, show saved msg and stay on page
            $scope.dataLoading=false;
            // console.log('dataloading=false');
            $scope.dataSaved=true;
            // console.log('dataSaved-true');
          }
        });
      }
    });
  }

  $scope.scrubber = function() {
    $scope.loadingErrorMsg=false;
    $('.error-box').animate({
      'margin-right': '0px'
    }, 200);
    $('.error-box').css({position: 'fixed', top: '120px', right: '0px'});
    console.log("sends form and received errors");

    var oasis=getOasisObject();
    console.log('scrubbing oasis', oasis);

    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['oasisC2_2_20_1'].scrub,
      method: 'GET',
      params: oasis,
      paramSerializer: '$httpParamSerializerJQLike',
    }).then(function(response){
      console.log(response);
      if ($scope.errors!==undefined){
        //clear old errors
        $scope.errors.forEach(function (error) {
          var field=error.Field1Name;
          var element = document.getElementById(field); //get element of error
          var $section = $(element).closest('section'); //find out which section the element is in
          var sectionName=$section.attr('class'); //find name of section

          if (document.getElementById(field)!==null) {
            document.getElementById(field).className = document.getElementById(field).className.replace(new RegExp('(?:^|s)' + 'scrubber' + '(?!S)'), '');
          }
        });
    }
    if (response.data.status==='Success'){
      $scope.loadingErrorMsg=true;

      var scrubObj=JSON.parse(response.data.msg);
      $scope.errors=scrubObj.ErrorResult.Errors;

      //sorting errors
      $scope.errors.sort(function(a,b) {
        var dateA = a.Field1Name.toLowerCase();
        var dateB = b.Field1Name.toLowerCase();

        if(dateA < dateB)
          return -1;
        if(dateA > dateB)
          return 1;
        return 0;
      });
      console.log($scope.errors)

      //scrubber errors to show as red
      $scope.errors.forEach(function (error) {
        var field=error.Field1Name;
        var element = document.getElementById(field); //get element of error
        var $section = $(element).closest('section'); //find out which section the element is in
        var sectionName=$section.attr('class'); //find name of section

        if (document.getElementById(field)!==null) {
          document.getElementById(field).className = "scrubber";
        }
      });
    }
  }, function(data) {
    console.log(data);
  });
};

  $scope.getError=function (error) {
    event.stopPropagation();
    console.log('error', error);
    var field=error.Field1Name;
    var fieldMoleNumber=field.substr(0,5);
    var element = document.getElementById(field); //get element of error

    var $section = $(element).closest('section'); //find out which section the element is in
    var sectionName=$section.attr('class'); //find name of section

    //go to section where element is in
    var current=$scope.currentForm;
    if ($scope.currentForm !== sectionName) {
      $scope.currentForm=sectionName;

      $('.' + current ).fadeOut('fast', function() {
        $('.' + sectionName ).fadeIn('fast');

        //find parent element based on class name
        var focusElement = document.getElementsByClassName(fieldMoleNumber);
        console.log(focusElement)
        focusElement=focusElement[0];
        console.log(focusElement);
        focusElement.scrollIntoView(true); //scroll to that element
        window.scrollBy(0, -60); //offset by hearder
            });
    } else {
      var focusElement = document.getElementsByClassName(fieldMoleNumber);
      focusElement=focusElement[0];
      console.log(focusElement);
      focusElement.scrollIntoView(true);
      window.scrollBy(0, -60);
    }
  };

  // function findInsurCo(){
  //   //find insurance 1
  //   if ($scope.lastPatAdmit.INS1CO !== null) {
  //     dataService.get('insurance', {'KEYID': $scope.lastPatAdmit.INS1CO}).then(function(ins1co){
  //       $scope.ins1co = ins1co.data[0].NAME.toLowerCase();
  //       $scope.ins1id = $scope.lastPatAdmit.INS1ID;
  //       console.log("insurance1=",$scope.ins1co);
  //
  //       if ($scope.ins1co==='medicare') {//if it's medicare, then populate medicare number
  //         $scope.M0063_MEDICARE_NUM=$scope.lastPatAdmit.INS1ID;
  //         $scope.M0150_CPAY_MCARE_FFS='1'
  //         // $scope.M0063_MEDICARE_NA=0;
  //         console.log('medicare num=', $scope.M0063_MEDICARE_NUM);
  //       } else if ($scope.ins1co==='medicaid') { //if it's medicaid, then populate medicaid number
  //         $scope.M0065_MEDICAID_NUM = $scope.lastPatAdmit.INS1ID;
  //         $scope.M0150_CPAY_MCAID_FFS='1'
  //         // $scope.M0065_MEDICAID_NA=0;
  //         console.log('medicaid num=', $scope.M0065_MEDICAID_NUM);
  //       }
  //       //check if there is a second insurance
  //       if ($scope.lastPatAdmit.INS2CO !== null) {
  //         dataService.get('insurance', {'KEYID': $scope.lastPatAdmit.INS2CO}).then(function(ins2co){
  //           $scope.ins2co = ins2co.data[0].NAME.toLowerCase();
  //           $scope.ins2id = $scope.lastPatAdmit.INS2ID;
  //
  //           console.log("insurance2=", $scope.ins2co);
  //           if ($scope.ins2co==='medicare') {//if it's medicare, then populate medicare number
  //             $scope.M0063_MEDICARE_NUM=$scope.lastPatAdmit.INS2ID;
  //             // $scope.M0063_MEDICARE_NA=0;
  //             console.log('medicare num=', $scope.M0063_MEDICARE_NUM);
  //           } else if ($scope.ins2co==='medicaid') { //if it's medicaid, then populate medicaid number
  //             $scope.M0065_MEDICAID_NUM = $scope.lastPatAdmit.INS2ID;
  //             // $scope.M0065_MEDICAID_NA=0;
  //             console.log('medicaid num=', $scope.M0065_MEDICAID_NUM);
  //           }
  //
  //           console.log('medicare',$scope.M0063_MEDICARE_NUM);
  //           console.log('medicaid', $scope.M0065_MEDICAID_NUM);
  //
  //           if ( ($scope.M0063_MEDICARE_NUM==='') || ($scope.M0063_MEDICARE_NUM===null) || ($scope.M0063_MEDICARE_NUM===undefined)) {
  //             $scope.M0063_MEDICARE_NA='1';
  //             $scope.M0063_MEDICARE_NUM="";
  //           }
  //           if ( ($scope.M0065_MEDICAID_NUM==='') || ($scope.M0065_MEDICAID_NUM===null) || ($scope.M0065_MEDICAID_NUM===undefined) ) {
  //             $scope.M0065_MEDICAID_NA='1';
  //             $scope.M0065_MEDICAID_NUM=''
  //           }
  //         });
  //       } else { //if there isn't a second insurance
  //         console.log('medicare',$scope.M0063_MEDICARE_NUM);
  //         console.log('medicaid', $scope.M0065_MEDICAID_NUM);
  //
  //         if ($scope.M0063_MEDICARE_NUM===null || ($scope.M0063_MEDICARE_NUM===undefined) || ($scope.M0063_MEDICARE_NUM==='')) {
  //           $scope.M0063_MEDICARE_NA='1';
  //           $scope.M0063_MEDICARE_NUM="";
  //         }
  //         if ( ($scope.M0065_MEDICAID_NUM===null) || ($scope.M0065_MEDICAID_NUM===undefined) || ($scope.M0065_MEDICAID_NUM==='') ) {
  //           $scope.M0065_MEDICAID_NA='1';
  //           $scope.M0065_MEDICAID_NUM=''
  //         }
  //       }
  //
  //     });
  //   }
  // }

  $scope.checkMedicare=function () {
    if($scope.M0063_MEDICARE_NA===1){
      $scope.M0063_MEDICARE_NUM='';
    }
  };

  $scope.checkMedicaid=function () {
    if($scope.M0065_MEDICAID_NA===1){
      $scope.M0065_MEDICAID_NUM='';
    }
  };

  function findDoctor(){
    if ($scope.lastPatAdmit.DOCTOR!==null) {
      dataService.get('doctor', {'KEYID':$scope.lastPatAdmit.DOCTOR}).then(function(data){
        $scope.doctor = data.data[0];
        var doctor=data.data[0];
        $scope.M0018_PHYSICIAN_ID=doctor.NPI;
      });
    } else {
      $scope.M0018_PHYSICIAN_ID=null
    }
  }

  $scope.clear=function (field) {
    $scope[field]=null
  }

  /* set empty field */
  $scope.fieldObjs = {
    M0150: [
      'M0150_CPAY_NONE',
      'M0150_CPAY_MCARE_FFS',
      'M0150_CPAY_MCARE_HMO',
      'M0150_CPAY_MCAID_FFS',
      'M0150_CPAY_MCAID_HMO',
      'M0150_CPAY_WRKCOMP',
      'M0150_CPAY_TITLEPGMS',
      'M0150_CPAY_OTH_GOVT',
      'M0150_CPAY_PRIV_INS',  
      'M0150_CPAY_PRIV_HMO',
      'M0150_CPAY_SELFPAY',
      'M0150_CPAY_OTHER',
    ],
    M2310: [
        'M2310_ECR_MEDICATION',
        'M2310_ECR_INJRY_BY_FALL',
        'M2310_ECR_RSPRTRY_INFCTN',
        'M2310_ECR_RSPRTRY_OTHR',
        'M2310_ECR_HRT_FAILR',
        'M2310_ECR_CRDC_DSRTHM',
        'M2310_ECR_MI_CHST_PAIN',
        'M2310_ECR_OTHR_HRT_DEASE',
        'M2310_ECR_STROKE_TIA',
        'M2310_ECR_HYPOGLYC',
        'M2310_ECR_GI_PRBLM',
        'M2310_ECR_DHYDRTN_MALNTR',
        'M2310_ECR_UTI',
        'M2310_ECR_CTHTR_CMPLCTN',
        'M2310_ECR_WND_INFCTN_DTRORTN',
        'M2310_ECR_UNCNTLD_PAIN',
        'M2310_ECR_MENTL_BHVRL_PRBLM',
        'M2310_ECR_DVT_PULMNRY',
        'M2310_ECR_OTHER',
      ],
    M2430: [
        'M2430_HOSP_MED',
        'M2430_HOSP_INJRY_BY_FALL',
        'M2430_HOSP_RSPRTRY_INFCTN',
        'M2430_HOSP_RSPRTRY_OTHR',
        'M2430_HOSP_HRT_FAILR',
        'M2430_HOSP_CRDC_DSRTHM',
        'M2430_HOSP_MI_CHST_PAIN',
        'M2430_HOSP_OTHR_HRT_DEASE',
        'M2430_HOSP_STROKE_TIA',
        'M2430_HOSP_HYPOGLYC',
        'M2430_HOSP_GI_PRBLM',
        'M2430_HOSP_DHYDRTN_MALNTR',
        'M2430_HOSP_UR_TRACT',
        'M2430_HOSP_CTHTR_CMPLCTN',
        'M2430_HOSP_WND_INFCTN',
        'M2430_HOSP_PAIN',
        'M2430_HOSP_MENTL_BHVRL_PRBLM',
        'M2430_HOSP_DVT_PULMNRY',
        'M2430_HOSP_SCHLD_TRTMT',
        'M2430_HOSP_OTHER',
      ]
  };

  $scope.setEmptyValue = function( field, val ) {
    for ( let i=0; i<$scope.fieldObjs[ field ].length; i++) {
      $scope[ $scope.fieldObjs[ field ][ i ] ] = val;
    }
  }

});
