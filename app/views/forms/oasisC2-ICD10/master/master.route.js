app.config(function($stateProvider) {
  $stateProvider.state('master', {
    url: '/oasisC2/master',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/oasisC2-ICD10/master/master.html',
        controller: 'MasterController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
