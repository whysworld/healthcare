app.config(function ($stateProvider) {
  $stateProvider.state('ptvisit', {
    url: '/ptvisit',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/ptvisit/ptvisit.html',
        controller: 'PTVisitController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  })
})
