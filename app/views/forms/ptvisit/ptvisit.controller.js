app.controller('PTVisitController', function ($scope, $http, $httpParamSerializerJQLike, $filter, formService, dataService, patientService, $anchorScroll, $timeout, CONSTANTS, $state) {
  $scope.balData = [{
      id: 1,
      label: 'N',
    },
    {
      id: 2,
      label: 'G',
    }, {
      id: 3,
      label: 'F+',
    },
    {
      id: 4,
      label: 'F',
    },
    {
      id: 5,
      label: 'F-',
    },
    {
      id: 6,
      label: 'P+',
    },
    {
      id: 7,
      label: 'P',
    }, {
      id: 8,
      label: 'P-',
    },
    {
      id: 9,
      label: 'U',
    }
  ];
  $scope.items = [{
    id: 1,
    label: 'Independent',

  }, {
    id: 2,
    label: 'Supervision',

  }, {
    id: 3,
    label: 'Verbal Cue',

  }, {
    id: 4,
    label: 'Contact Guard Assist',

  }, {
    id: 5,
    label: 'Min A 25%  Assist',

  }, {
    id: 6,
    label: 'Mod A 50% Assist',

  }, {
    id: 7,
    label: 'Max A 75% Assist',

  }, {
    id: 8,
    label: 'Tot A 100% Assist',

  }, {
    id: 9,
    label: ' Not Tested',

  }, {
    id: 10,
    label: 'Modified Independent',

  }];
  $scope.assDevice = [{
      id: 1,
      label: 'Rolling Walker',
    }, {
      id: 2,
      label: 'Standard Walker',
    },
    {
      id: 3,
      label: 'Platform Walker',
    }, {
      id: 4,
      label: '4 wheeled walter',
    }, {
      id: 5,
      label: 'wide based quad cane',
    }, {
      id: 6,
      label: 'short based quad cane',
    }, {
      id: 7,
      label: 'standard cane',
    }, {
      id: 8,
      label: 'crutches',
    }, {
      id: 9,
      label: 'none',
    }, {
      id: 10,
      label: 'other'
    }
  ];
  $scope.PREVIOUS = function () {
    window.history.back();
  };
  $scope.showPatSignatureImage = false;
  $scope.showPatSignaturePad = true;
  $scope.editform = 'no';
  $scope.ptvisit = {};
  $scope.form = {};
  $scope.form1 = {};
  var getFormValue = formService.get();
  $scope.getDetails = function () {
    if (getFormValue.STATUS == 0) {
      var formData = {
        formId: getFormValue.FORMID
      }
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['missedvisitSheet'].getAll,
        method: 'POST',
        data: $httpParamSerializerJQLike(formData),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }

      }).success(function (data) {
        $scope.ptvisit.basicinfo.patient_name = data.PATKEYID;
        $scope.ptvisit.basicinfo.patient_name_id = data.fn + ' ' + data.ln;
        $scope.ptvisit.basicinfo.FORMID = data.formId;
      });
    }
  };
  $scope.getDetails();
  if (getFormValue.STATUS == 0) {
    $scope.formId = getFormValue.FORMID;
  }
  $scope.imgvalidate = false;
  $scope.imgvalidateerror = 1;

  $scope.savePTvisitSheet = function (form) {
    if ($scope.editform == 'no') {
      $scope.si = $scope.acceptPat().dataUrl;
    } else if ($scope.editform == 'yes') {
      $scope.si = $scope.empSignature;
    }
    //        alert($scope.ptvisit.supervisory.supervisory_lpta_present+"="+$scope.ptvisit.subjective.subjective_na);
    //        return false;
    if (typeof $scope.si == 'undefined' || $scope.si == '') {
      $scope.imgvalidate = true;
      $scope.imgvalidateerror = 0;

    } else {
      if ($scope.editform == 'no' && $scope.acceptPat().dataUrl == 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjgAAADcCAQAAADXNhPAAAACIklEQVR42u3UIQEAAAzDsM+/6UsYG0okFDQHMBIJAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcCQADAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDkQAwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAegeayZAN3dLgwnAAAAAElFTkSuQmCC') {

        $scope.imgvalidate = true;
        $scope.imgvalidateerror = 0;

      } else {

        $scope.imgvalidate = false;
        $scope.imgvalidateerror = 1;
      }

    }
    if (form.$valid && $scope.imgvalidateerror) {
      $scope.ptvisit.basicinfo.visit_date = $filter("date")($scope.ptvisit.basicinfo.visit_date, 'yyyy/MM/dd');
      $scope.ptvisit.basicinfo.episode_date = $filter("date")($scope.ptvisit.basicinfo.episode_date, 'yyyy/MM/dd');
      $scope.ptvisit.basicinfo.epito_date = $filter("date")($scope.ptvisit.basicinfo.epito_date, 'yyyy/MM/dd');
      $scope.ptvisit.sign.signature_date = $filter("date")($scope.ptvisit.sign.signature_date, 'yyyy/MM/dd');
      var newRouteSheet = {
        pt_data: $scope.ptvisit,
        edit_form: $scope.editform,
        currentTableId: $scope.currenttableId,
        FORMID: $scope.formId,
        sign_phy: $scope.si
      };
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['ptvisitSheet'].add,
        method: 'POST',
        data: $httpParamSerializerJQLike(newRouteSheet),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }

      }).success(function (response) {
        if (response.status === 'success') {
          $anchorScroll();
          $scope.successTextAlert = response.msg;
          $scope.showSuccessAlert = true;
          $timeout(function () {
            $state.go(response.goto);
          }, 2000);
        }

      });
    } else {
      return false;
    }
  }
  if (getFormValue.STATUS == 1 || getFormValue.STATUS == 3) {

    $scope.form.STATUS = 1;
    $scope.form1.path = getFormValue.sign_phy;
    $scope.empSignature = $scope.form1.path;

    $scope.ptvisit = getFormValue;
    $scope.ptvisit.assess = getFormValue.assess;
    $scope.ptvisit.basicinfo = getFormValue.basicinfo;
    var visitdate = $filter("date")(getFormValue.basicinfo.visit_date, 'yyyy/MM/dd');
    $scope.form.VIDATE = new Date(visitdate);
    var visitdate = $filter("date")(getFormValue.basicinfo.episode_date, 'yyyy/MM/dd');
    $scope.form.EP = new Date(visitdate);
    var visitdate = $filter("date")(getFormValue.basicinfo.epito_date, 'yyyy/MM/dd');
    $scope.form.EPT = new Date(visitdate);

    $scope.ptvisit.bedm = getFormValue.bedm;
    $scope.ptvisit.bedt = getFormValue.bedt;
    $scope.ptvisit.functional = getFormValue.functional;
    $scope.ptvisit.gait = getFormValue.gait;
    $scope.ptvisit.homebound = getFormValue.homebound;

    $scope.ptvisit.narrative = getFormValue.narrative;
    $scope.ptvisit.objective = getFormValue.objective;
    $scope.ptvisit.pain = getFormValue.pain;
    $scope.ptvisit.plan = getFormValue.plan;
    $scope.ptvisit.progress = getFormValue.progress;
    $scope.ptvisit.sign = getFormValue.sign;
    var visitdate = $filter("date")(getFormValue.sign.signature_date, 'yyyy/MM/dd');
    $scope.form.SIDATE = new Date(visitdate);
    $scope.ptvisit.skill = getFormValue.skill;
    $scope.ptvisit.subjective = getFormValue.subjective;
    $scope.ptvisit.supervisory = getFormValue.supervisory;
    $scope.ptvisit.teaching = getFormValue.teaching;
    $scope.ptvisit.vital = getFormValue.vital;

    $scope.ptvisit.basicinfo.formId = getFormValue.FORMID;
    $scope.ptvisit.basicinfo.patient_name = getFormValue.patient_id
    $scope.ptvisit.basicinfo.patient_name_id = getFormValue.FNAME + ' ' + getFormValue.LNAME;
    $scope.formId = getFormValue.FORMID;
    $scope.currenttableId = getFormValue.currentFormId

    $scope.ptvisit.bedm.bedm_roll_from = $scope.items[getFormValue.bedm.bedm_roll_from];
    $scope.ptvisit.bedm.bedm_sit_from = $scope.items[getFormValue.bedm.bedm_sit_from];
    $scope.ptvisit.bedm.bedm_scooting_from = $scope.items[getFormValue.bedm.bedm_scooting_from];
    $scope.ptvisit.bedm.bedm_stand_from = $scope.items[getFormValue.bedm.bedm_stand_from];

    $scope.ptvisit.bedt.bedt_assist = $scope.assDevice[getFormValue.bedt.bedt_assist];
    $scope.ptvisit.bedt.bedt_chair = $scope.items[getFormValue.bedt.bedt_chair];
    $scope.ptvisit.bedt.bedt_chair_toilet = $scope.items[getFormValue.bedt.bedt_chair_toilet];
    $scope.ptvisit.bedt.bedt_chair_car = $scope.items[getFormValue.bedt.bedt_chair_car];

    $scope.ptvisit.bedt.bedt_sit_static_bal = $scope.balData[getFormValue.bedt.bedt_sit_static_bal];
    $scope.ptvisit.bedt.bedt_sit_dyn_bal = $scope.balData[getFormValue.bedt.bedt_sit_dyn_bal];
    $scope.ptvisit.bedt.bedt_std_static_bal = $scope.balData[getFormValue.bedt.bedt_std_static_bal];
    $scope.ptvisit.bedt.bedt_std_dyn_bal = $scope.balData[getFormValue.bedt.bedt_std_dyn_bal];

    $scope.ptvisit.gait.gait_assistdev_1 = $scope.assDevice[getFormValue.gait.gait_assistdev_1];
    $scope.ptvisit.gait.gait_assist_1 = $scope.items[getFormValue.gait.gait_assist_1];
    $scope.ptvisit.gait.gait_assistdev_2 = $scope.assDevice[getFormValue.gait.gait_assistdev_2];
    $scope.ptvisit.gait.gait_assist_2 = $scope.items[getFormValue.gait.gait_assist_2];


  }
  $scope.isQAMode = function () {
    if (getFormValue.ACTION === 'QAREVIEW') {
      $scope.currenttableId = getFormValue.formPrimaryId;
      $scope.approvedMsg = false;
      $scope.enableQAEditBtn = true;
      $scope.enableQALockBtn = true;
      $scope.enableQARejectBtn = true;
      $scope.submitQAbtn = false;
      $scope.disableEdit = true;
      $scope.enableInput = false;
      $scope.showEmpSignatureImage = true;
      $scope.showEmpSignaturePad = false;
      $scope.showPatSignatureImage = true;
      $scope.showPatSignaturePad = false;

    } else {
      if ((getFormValue.STATUS === 1) || (getFormValue.STATUS === 3)) {
        $scope.disableEdit = true;
        $scope.enableInput = false;
        $scope.submitQAbtn = false;
        $scope.showEmpSignatureImage = true;
        $scope.showEmpSignaturePad = false;
        $scope.showPatSignatureImage = true;
        $scope.showPatSignaturePad = false;
      }
    }
  }
  //qa editing
  $scope.edit = function () {
    $scope.disableEdit = false;
    $scope.enableInput = true;
    $scope.enableQALockBtn = false;
    $scope.enableQAEditBtn = false;
    $scope.enableQARejectBtn = false;
    $scope.enableCancelBtn = true;
    $scope.enableSaveBtn = true;
    $scope.editform = "yes";

  };
  $scope.removeEmpSignature = function () {
    $scope.showPatSignaturePad = true;
    $scope.showEmpSignatureImage = false;
  };
  //cancel qa editing
  $scope.cancel = function () {
    $scope.disableEdit = true;
    $scope.enableInput = false;
    $scope.enableQALockBtn = true;
    $scope.enableQAEditBtn = true;
    $scope.enableQARejectBtn = true;
    $scope.enableCancelBtn = false;
    $scope.enableSaveBtn = false;
    $scope.getRouteSheet(); //get original route sheet info
    $scope.editform = "no";
  };
  $scope.reject = function () {
    var form = {
      KEYID: getFormValue.FORMID,
      STATUS: 4,
    };
    dataService.edit('form', form).then(function (response) {
      $state.go('qa');
    });
  };
  //qa approves form
  $scope.approve = function () {
    dataService.edit('form', {
      KEYID: getFormValue.FORMID,
      STATUS: 3
    }).then(function (response) {
      $state.go('qa');
    });
  }
  $scope.isQAMode();

});
