app.config(function ($stateProvider) {
  $stateProvider.state('ptdischarge', {
    url: '/ptdischarge',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/ptdischarge/ptdischarge.html',
        controller: 'PTDischargeController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  })
})
