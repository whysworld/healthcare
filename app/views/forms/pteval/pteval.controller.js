app.controller('PTEvalController', function ($scope, $http, $httpParamSerializerJQLike, $filter, formService, dataService, patientService, $anchorScroll, $timeout, CONSTANTS, $state) {
  $scope.wbData = [{
      id: 1,
      label: 'Full weight bearing',
    },
    {
      id: 2,
      label: 'Weight bearing as tolerated',
    },
    {
      id: 3,
      label: 'partial weight bearing',
    },
    {
      id: 4,
      label: 'toe touch weight bearing',
    },
    {
      id: 5,
      label: 'touch down weight bearing',
    },
    {
      id: 6,
      label: 'non-weight bearing',
    }
  ];
  $scope.balData = [{
      id: 1,
      label: 'N',
    },
    {
      id: 2,
      label: 'G',
    }, {
      id: 3,
      label: 'F+',
    },
    {
      id: 4,
      label: 'F',
    },
    {
      id: 5,
      label: 'F-',
    },
    {
      id: 6,
      label: 'P+',
    },
    {
      id: 7,
      label: 'P',
    }, {
      id: 8,
      label: 'P-',
    },
    {
      id: 9,
      label: 'U',
    }
  ];
  $scope.assDevice = [{
      id: 1,
      label: 'Rolling Walker',
    }, {
      id: 2,
      label: 'Standard Walker',
    },
    {
      id: 3,
      label: 'Platform Walker',
    }, {
      id: 4,
      label: '4 wheeled walter',
    }, {
      id: 5,
      label: 'wide based quad cane',
    }, {
      id: 6,
      label: 'short based quad cane',
    }, {
      id: 7,
      label: 'standard cane',
    }, {
      id: 8,
      label: 'crutches',
    }, {
      id: 9,
      label: 'none',
    }, {
      id: 10,
      label: 'other'
    }
  ];
  $scope.items = [{
    id: 1,
    label: 'Independent',

  }, {
    id: 2,
    label: 'Supervision',

  }, {
    id: 3,
    label: 'Verbal Cue',

  }, {
    id: 4,
    label: 'Contact Guard Assist',

  }, {
    id: 5,
    label: 'Min A 25%  Assist',

  }, {
    id: 6,
    label: 'Mod A 50% Assist',

  }, {
    id: 7,
    label: 'Max A 75% Assist',

  }, {
    id: 8,
    label: 'Tot A 100% Assist',

  }, {
    id: 9,
    label: ' Not Tested',

  }, {
    id: 10,
    label: 'Modified Independent',

  }];
  $scope.editform = 'no';
  $scope.form = {};
  $scope.form1 = {};
  $scope.pteval = {};
  $scope.imgvalidate = false;
  $scope.imgvalidateerror = 1;
  $scope.showPatSignatureImage = false;
  $scope.showPatSignaturePad = true;
  $scope.pteval.bed_a_rollright = $scope.items[5];
  var getFormValue = formService.get();
  $scope.getDetails = function () {
    if (getFormValue.STATUS == 0) {
      var formData = {
        formId: getFormValue.FORMID
      }
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['missedvisitSheet'].getAll,
        method: 'POST',
        data: $httpParamSerializerJQLike(formData),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }

      }).success(function (data) {
        $scope.pteval.patient_id = data.PATKEYID;
        $scope.pteval.patient_name_id = data.fn + ' ' + data.ln;
        $scope.pteval.FORMID = data.formId;
      });
    }
  };
  $scope.getDetails();
  $scope.savePTEvalSheet = function (form) {
    if ($scope.editform == 'no') {
      $scope.si = $scope.acceptPat().dataUrl;
    } else if ($scope.editform == 'yes') {
      $scope.si = $scope.empSignature;
    }
    if (typeof $scope.si == 'undefined' || $scope.si == '') {
      $scope.imgvalidate = true;
      $scope.imgvalidateerror = 0;

    } else {
      if ($scope.editform == 'no' && $scope.acceptPat().dataUrl == 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjgAAADcCAQAAADXNhPAAAACIklEQVR42u3UIQEAAAzDsM+/6UsYG0okFDQHMBIJAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcCQADAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDkQAwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAegeayZAN3dLgwnAAAAAElFTkSuQmCC') {

        $scope.imgvalidate = true;
        $scope.imgvalidateerror = 0;

      } else {

        $scope.imgvalidate = false;
        $scope.imgvalidateerror = 1;
      }

    }

    if (form.$valid && $scope.imgvalidateerror) {
      $scope.pteval.visit_date = $filter("date")($scope.pteval.visit_date, 'yyyy/MM/dd');
      $scope.pteval.episode_date = $filter("date")($scope.pteval.episode_date, 'yyyy/MM/dd');
      $scope.pteval.epito_date = $filter("date")($scope.pteval.epito_date, 'yyyy/MM/dd');
      $scope.pteval.signature_date = $filter("date")($scope.pteval.signature_date, 'yyyy/MM/dd');
      var newRouteSheet = {
        pt_data: $scope.pteval,
        edit_form: $scope.editform,
        currentTableId: $scope.currenttableId,
        FORMID: $scope.formId,
        sign_phy: $scope.si
      };
      $http({
        url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['ptevalSheet'].add,
        method: 'POST',
        data: $httpParamSerializerJQLike(newRouteSheet),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }

      }).success(function (response) {
        if (response.status === 'success') {
          $anchorScroll();
          $scope.successTextAlert = response.msg;
          $scope.showSuccessAlert = true;
          $timeout(function () {
            $state.go(response.goto);
          }, 2000);
        }

      });
    } else {
      return false;
    }
  };
  if (getFormValue.STATUS == 1 || getFormValue.STATUS == 3) {

    $scope.form.STATUS = 1;

    $scope.form1.path = getFormValue.signature_path;
    $scope.empSignature = $scope.form1.path;

    $scope.pteval = getFormValue;

    var visitdate = $filter("date")(getFormValue.visit_date, 'yyyy/MM/dd');
    $scope.form.VIDATE = new Date(visitdate);
    var visitdate = $filter("date")(getFormValue.epito_date, 'yyyy/MM/dd');
    $scope.form.EP = new Date(visitdate);
    var visitdate = $filter("date")(getFormValue.episode_date, 'yyyy/MM/dd');
    $scope.form.EPT = new Date(visitdate);
    var visitdate = $filter("date")(getFormValue.med_onset_date, 'yyyy/MM/dd');
    $scope.form.MO = new Date(visitdate);
    var visitdate = $filter("date")(getFormValue.med_onset_date1, 'yyyy/MM/dd');
    $scope.form.MOD = new Date(visitdate);
    var visitdate = $filter("date")(getFormValue.signature_date, 'yyyy/MM/dd');
    $scope.form.SIDATE = new Date(visitdate);
    $scope.pteval.count_temp = parseInt(getFormValue.count_temp);
    $scope.pteval.standing_sbp = parseInt(getFormValue.standing_sbp);
    $scope.pteval.count_sbp = parseInt(getFormValue.count_sbp);
    $scope.pteval.lying_sbp = parseInt(getFormValue.lying_sbp);
    $scope.pteval.count_dbp = parseInt(getFormValue.count_dbp);
    $scope.pteval.standing_dbp = parseInt(getFormValue.standing_dbp);
    $scope.pteval.lying_dbp = parseInt(getFormValue.lying_dbp);
    $scope.pteval.count_hr = parseInt(getFormValue.count_hr);
    $scope.pteval.standing_hr = parseInt(getFormValue.standing_hr);
    $scope.pteval.lying_hr = parseInt(getFormValue.lying_hr);
    $scope.pteval.count_resp = parseInt(getFormValue.count_resp);
    $scope.pteval.count_weight = parseInt(getFormValue.count_weight);
    $scope.pteval.count_sugar = parseInt(getFormValue.count_sugar);
    $scope.pteval.count_so2 = parseInt(getFormValue.count_so2);
    $scope.pteval.count_sbp_std_systolic = parseInt(getFormValue.count_so2);
    $scope.pteval.count_sbp_std_diastolic = parseInt(getFormValue.count_sbp_std_diastolic);
    $scope.pteval.count_sbp_ly_systolic = parseInt(getFormValue.count_sbp_ly_systolic);
    $scope.pteval.count_sbp_ly_diastolic = parseInt(getFormValue.count_sbp_ly_diastolic);
    $scope.pteval.count_dbp_std_systolic = parseInt(getFormValue.count_dbp_std_systolic);
    $scope.pteval.count_dbp_std_diastolic = parseInt(getFormValue.count_dbp_std_diastolic);
    $scope.pteval.count_dbp_ly_systolic = parseInt(getFormValue.count_dbp_ly_systolic);
    $scope.pteval.count_dbp_ly_diastolic = parseInt(getFormValue.count_dbp_ly_diastolic);
    $scope.pteval.count_hr_std_systolic = parseInt(getFormValue.count_hr_std_systolic);
    $scope.pteval.count_hr_std_diastolic = parseInt(getFormValue.count_hr_std_diastolic);
    $scope.pteval.count_hr_ly_systolic = parseInt(getFormValue.count_hr_ly_systolic);
    $scope.pteval.count_hr_ly_diastolic = parseInt(getFormValue.count_hr_ly_diastolic);

    $scope.pteval.bed_a_rollright = $scope.items[getFormValue.bed_a_rollright];
    $scope.pteval.bed_ad_rollright = $scope.items[getFormValue.bed_ad_rollright];
    $scope.pteval.bed_a_rollleft = $scope.items[getFormValue.bed_a_rollleft];
    $scope.pteval.bed_ad_rollleft = $scope.items[getFormValue.bed_ad_rollleft];
    $scope.pteval.bed_a_ss = $scope.items[getFormValue.bed_a_ss];
    $scope.pteval.bed_ad_ss = $scope.items[getFormValue.bed_ad_ss];
    $scope.pteval.bed_a_sus = $scope.items[getFormValue.bed_a_sus];
    $scope.pteval.bed_ad_sus = $scope.items[getFormValue.bed_ad_sus];


    $scope.pteval.ga_level = $scope.items[getFormValue.ga_level];
    $scope.pteval.ga_unlevel = $scope.items[getFormValue.ga_unlevel];
    $scope.pteval.ga_step = $scope.items[getFormValue.ga_step];

    $scope.pteval.ga_device = $scope.assDevice[getFormValue.ga_device];


    $scope.pteval.trns_a_bed = $scope.items[getFormValue.trns_a_bed];
    $scope.pteval.trns_a_chair = $scope.items[getFormValue.trns_a_chair];
    $scope.pteval.trns_a_wc = $scope.items[getFormValue.trns_a_wc];
    $scope.pteval.trns_a_bsc = $scope.items[getFormValue.trns_a_bsc];
    $scope.pteval.trns_a_van = $scope.items[getFormValue.trns_a_van];
    $scope.pteval.trns_a_tub = $scope.items[getFormValue.trns_a_tub];

    $scope.pteval.trns_ad_bed = $scope.assDevice[getFormValue.trns_ad_bed];
    $scope.pteval.trns_ad_chair = $scope.assDevice[getFormValue.trns_ad_chair];
    $scope.pteval.trns_ad_wc = $scope.assDevice[getFormValue.trns_ad_wc];
    $scope.pteval.trns_ad_bsc = $scope.assDevice[getFormValue.trns_ad_bsc];
    $scope.pteval.trns_ad_van = $scope.assDevice[getFormValue.trns_ad_van];
    $scope.pteval.trns_ad_tub = $scope.assDevice[getFormValue.trns_ad_tub];

    $scope.pteval.trns_sit_std = $scope.balData[getFormValue.trns_sit_std];
    $scope.pteval.trns_std_std = $scope.balData[getFormValue.trns_std_std];
    $scope.pteval.trns_sit_dyn = $scope.balData[getFormValue.trns_sit_dyn];
    $scope.pteval.trns_std_dyn = $scope.balData[getFormValue.trns_std_dyn];


    $scope.pteval.wb_sts = $scope.wbData[getFormValue.wb_sts];


    $scope.pteval.wc_level = $scope.items[getFormValue.wc_level];
    $scope.pteval.wc_uneven = $scope.items[getFormValue.wc_uneven];
    $scope.pteval.wc_man = $scope.items[getFormValue.wc_man];
    $scope.pteval.wc_adl = $scope.items[getFormValue.wc_adl];


    $scope.pteval.patient_id = getFormValue.patient_id;
    $scope.pteval.patient_name_id = getFormValue.FNAME + ' ' + getFormValue.LNAME;
    $scope.formId = getFormValue.FORMID;
    $scope.currenttableId = getFormValue.currentFormId;



  }
  $scope.isQAMode = function () {
    if (getFormValue.ACTION === 'QAREVIEW') {
      $scope.currenttableId = getFormValue.formPrimaryId;
      $scope.approvedMsg = false;
      $scope.enableQAEditBtn = true;
      $scope.enableQALockBtn = true;
      $scope.enableQARejectBtn = true;
      $scope.submitQAbtn = false;
      $scope.disableEdit = true;
      $scope.enableInput = false;
      $scope.showEmpSignatureImage = true;
      $scope.showEmpSignaturePad = false;
      $scope.showPatSignatureImage = true;
      $scope.showPatSignaturePad = false;

    } else {
      if ((getFormValue.STATUS === 1) || (getFormValue.STATUS === 3)) {
        $scope.disableEdit = true;
        $scope.enableInput = false;
        $scope.submitQAbtn = false;
        $scope.showEmpSignatureImage = true;
        $scope.showEmpSignaturePad = false;
        $scope.showPatSignatureImage = true;
        $scope.showPatSignaturePad = false;
      }
    }
  }
  //qa editing
  $scope.edit = function () {
    $scope.disableEdit = false;
    $scope.enableInput = true;
    $scope.enableQALockBtn = false;
    $scope.enableQAEditBtn = false;
    $scope.enableQARejectBtn = false;
    $scope.enableCancelBtn = true;
    $scope.enableSaveBtn = true;
    $scope.editform = "yes";

  };
  $scope.removeEmpSignature = function () {
    $scope.showPatSignaturePad = true;
    $scope.showEmpSignatureImage = false;
  };
  //cancel qa editing
  $scope.cancel = function () {
    $scope.disableEdit = true;
    $scope.enableInput = false;
    $scope.enableQALockBtn = true;
    $scope.enableQAEditBtn = true;
    $scope.enableQARejectBtn = true;
    $scope.enableCancelBtn = false;
    $scope.enableSaveBtn = false;
    $scope.getRouteSheet(); //get original route sheet info
    $scope.editform = "no";
  };
  $scope.reject = function () {
    var form = {
      KEYID: getFormValue.FORMID,
      STATUS: 4,
    };
    dataService.edit('form', form).then(function (response) {
      $state.go('qa');
    });
  };
  //qa approves form
  $scope.approve = function () {
    dataService.edit('form', {
      KEYID: getFormValue.FORMID,
      STATUS: 3
    }).then(function (response) {
      $state.go('qa');
    });
  }
  $scope.isQAMode();
  $scope.PREVIOUS = function () {
    window.history.back();
  };
});
