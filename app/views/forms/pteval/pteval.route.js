app.config(function ($stateProvider) {
  $stateProvider.state('pteval', {
    url: '/pteval',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/forms/pteval/pteval.html',
        controller: 'PTEvalController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  })
})
