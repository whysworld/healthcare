app.controller('ClaimEstimateController', function($scope, $uibModal, patientService, dataService) {

  $scope.patient=patientService.get();

  //sample starter data
  $scope.claims = [
    {
      ADDRESS: '123 Happy Lane',
      CITY: 'Las Vegas',
      STATE: 'CA',
      ZIP: '22192',
      GENDER: 'M',
      DOB: '01/02/1960',
      CLAIMTYPE: "RAP",
      INSURANCECO: "Medicare",
      CLAIMNO:'02912',
      CLAIMDATE: '02/23/2017',
      EPINUM: 1,
      MSG: "Error: Signed Certification not Received.",
      EPISODES: [
        {
          ADMITKEYID: 1,
          EPISODENO:1,
          FROMDATE: "01/12/2017",
          THURDATE:"02/23/2017"
        },
        {
          ADMITKEYID: 1,
          EPISODENO: 2,
          FROMDATE: "02/24/2017",
          THURDATE:"03/01/2017"
        }
      ],
      ADMITNUM: 1,
      FNAME: "John",
      LNAME: "Smith",
      PATID: 'PAT01',
      STATUS: "Ready",
      TOTALCHARGES: "$9,651"
    },
    {
      CLAIMTYPE: "FC",
      INSURANCECO: "Insurance ABC",
      CLAIMNO:'12716',
      CLAIMDATE: '08/11/2016',
      EPINUM: 2,
      ADMITNUM: 1,
      MSG: "Warning: RAP has not been Paid.",
      EPISODES: [
        {
          ADMITKEYID: 1,
          EPISODENO:1,
          FROMDATE: "06/29/2016",
          THURDATE:"08/11/2016"
        },
        {
          ADMITKEYID: 1,
          EPISODENO: 2,
          FROMDATE: "08/12/2016",
          THURDATE:"09/17/2016"
        }
      ],
      FNAME: "Bily",
      LNAME: "Jones",
      PATID: 'PAT12',
      STATUS: "Sent",
      TOTALCHARGES: "$2,312"
    },
    {
      CLAIMTYPE: "FC",
      INSURANCECO: "Insurance1",
      CLAIMNO:'87291',
      CLAIMDATE: '10/21/2016',
      EPINUM: 1,
      ADMITNUM: 1,
      MSG: "Error: Signed Certification not Received.",
      EPISODES: [
        {
          ADMITKEYID: 1,
          EPISODENO:1,
          FROMDATE: "08/06/2016",
          THURDATE:"10/21/2016"
        },
        {
          ADMITKEYID: 1,
          EPISODENO: 2,
          FROMDATE: "10/22/2016",
          THURDATE:"11/19/2016"
        }
      ],
      FNAME: "Jack",
      LNAME: "Johnson",
      PATID: 'PAT192',
      STATUS: "Paid",
      TOTALCHARGES: "$7,286"
    }
  ];


  $scope.activeClaimsList = [{}];
  var claims = $scope.claims

  var allClaims = [];
  var claimsRAP = [];
  var claimsFC = [];

  function sortClaims(claims) {
    allClaims = [];
    claimsRAP = [];
    claimsFC = [];
    allClaims = claims;
    $scope.activeClaimsList = allClaims;
    console.log($scope.activeClaimsList);
    for (var i = 0; i < claims.length; i++) {
      console.log(claims[i])
      switch(claims[i].CLAIMTYPE) {
        case "FC":
          claimsFC.push(claims[i]);
          break;
        case "RAP":
          claimsRAP.push(claims[i]);
          break;
      }
    }
  }

  $scope.claimType = 'ALL';


  $scope.updateClaimList = function(claimType) {
    switch(claimType) {
      case "ALL":
        $scope.activeClaimsList = allClaims;
        break;
      case "RAP":
        $scope.activeClaimsList = claimsRAP;
        break;
      case "FC":
        $scope.activeClaimsList = claimsFC;
        break;
    }
  };

  sortClaims(claims);

  //open modal to display all patients to add new billing
  $scope.newBilling = function() {
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'NewBillingController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/newBilling/newBilling.html',
        size: 'lg',
      });
    modalInstance.result.then(function (data) {
      //after claim generated, display updated patient list sorted by claimtype
      // var patients = data.patients;
      for (var i = 0; i < data.length; i++) {
        $scope.claims.push(data[i]);
      }
      sortClaims($scope.claims)
    });
  };

  $scope.showForm=function(claim) {
    console.log(claim)
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'UB04Controller',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/UB-04/UB-04.html',
        size: 'lg',
        resolve: {
          claim: claim,
        }
      });
  }

  //select All checkbox
  $scope.checkAllReady = function () {
    console.log($scope.selectedAllReady);
    if ($scope.selectedAllReady) {
        $scope.selectedAllReady = true;
        $scope.showGenerateClaim=true;

    } else {
        $scope.selectedAllReady = false;
        $scope.showGenerateClaim = false;

    }

    angular.forEach($scope.activeClaimsList, function (claim) {
      //only 'Ready' claims are allowed to be selected
      if (claim.STATUS === 'Ready') {
        claim.selectedReady = $scope.selectedAllReady;
      }
    });
  };

  //show 'Generate' Button
  $scope.generateClaimBtn=function () {
    // $scope.activeClaimsList = $scope.billingsFinal.concat($scope.billingsRAP);
    for (var i = 0; i < $scope.activeClaimsList.length; i++) {
      if ($scope.activeClaimsList[i].selectedReady === true) {
        $scope.showGenerateClaim=true;
        break;
      } else {
        $scope.showGenerateClaim=false;
      }
    }
  };

  //open modal to display all patients to add new billing
  $scope.reviewClaim = function() {
    //get all selected claims
    var billingsReadyArr = [];
    angular.forEach($scope.activeClaimsList, function (claim) {
      if (claim.selectedReady === true) {
        billingsReadyArr.push(claim);
      }
    });
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'ReviewClaimController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/reviewClaim/reviewClaim.html',
        size: 'lg',
        resolve: {
          billingsReadyArr: function() {
            return billingsReadyArr;
          }
        }
      });
  };

});
