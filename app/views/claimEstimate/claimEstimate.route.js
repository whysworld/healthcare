app.config(function($stateProvider) {
  $stateProvider.state('claimEstimate', {
    url: '/claimEstimate',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/claimEstimate/claimEstimate.html',
        controller: 'ClaimEstimateController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
