app.controller('PatientBillingController', function($scope, patientService, dataService) {

  $scope.TOTALHOURS = 0;
  $scope.TOTALCOST = 0;

  $scope.getTotals = function() {
    console.log('getting totals');
    console.log('filtered visits', $scope.filteredVisits);
    $scope.TOTALHOURS = 0;
    $scope.TOTALCOST = 0;
    for (var k = 0; k < $scope.filteredVisits.length; k++) {
      if ($scope.filteredVisits[k].HOURS !== null) {
        $scope.TOTALHOURS += parseInt($scope.filteredVisits[k].HOURS);
      }
      if ($scope.filteredVisits[k].COST !== null) {
        $scope.TOTALCOST += parseInt($scope.filteredVisits[k].COST);
      }
    }
  }

  $scope.patient=patientService.get();

  dataService.get('admission', {PATKEYID:$scope.patient.KEYID}).then(function (data) {
    $scope.admissions = [{'ADMITNO' : 'All'}].concat(data.data);
    $scope.ADMIT = $scope.admissions[$scope.admissions.length - 1];
  });

  dataService.get('episode', {'ADMITKEYID': $scope.patient.LSTADMIT}).then(function(data) {
    if (data.data.length<1) {
      $scope.episodes = [{'EPISODENO' : 'All'}];
      $scope.EPISODE = $scope.episodes[0];
    } else {
      $scope.episodes = [{'EPISODENO' : 'All'}].concat(data.data);
      $scope.EPISODE = $scope.episodes[$scope.episodes.length - 1];
    }
  });

  // $scope.visits = [];
  dataService.get('patientBillingVisit', {PATKEYID: $scope.patient.KEYID}).then(function (data) {
    $scope.visits = data.data;
    $scope.filteredVisits = data.data;
    $scope.getTotals();


    // var visits = data.data;
    // for (var i = 0; i < visits.length; i++) {
    //   if(moment(visits[i].VISITDATE).isBefore(moment(new Date()))) {
    //     $scope.visits.push(visits[i]);
    //   }
    // }

    console.log('visits', $scope.visits);

  });

  // $scope.changeAdmit = function() {
  //   console.log('changeadmit', $scope.ADMIT);
  //   if ($scope.ADMIT.ADMITNO !== 'All') {
  //     dataService.get('episode', {'ADMITKEYID': $scope.ADMIT.KEYID}).then(function(data) {
  //       if (data.data.length<1) {
  //         $scope.episodes = [{'EPISODENO' : 'All'}];
  //         $scope.EPISODE = $scope.episodes[0];
  //       } else {
  //         $scope.episodes = [{'EPISODENO' : 'All'}].concat(data.data);
  //         $scope.EPISODE = $scope.episodes[$scope.episodes.length - 1];
  //       }
  //     });    
  //   } else {
  //     $scope.episodes = [{'EPISODENO' : 'All'}];
  //     $scope.EPISODE = $scope.episodes[0];
  //   }
  //   $scope.updateTotals();
  // }

  // $scope.changeEpisode = function() {
  //   if ($scope.EPISODE.EPISODENO !== 'All') {
  //     $scope.filter.EPISODENO = $scope.EPISODE.EPISODENO;
  //   } else {
  //     $scope.filter.EPISODENO = '';
  //   }
  //   $scope.updateTotals();
  // }


  // $scope.visits=[];
  // //get all visits for patient
  // dataService.get('visit', {PATKEYID: $scope.patient.KEYID}).then(function (data) {
  //   var visits = data.data;
  //   for (var i = 0; i < visits.length; i++) {
  //     if(moment(visits[i].VISITDATE).isBefore(moment(new Date()))) {
  //       $scope.visits.push(visits[i]);
  //     }
  //   }
  //   //get employee name
  //   dataService.getAll('employee').then(function(data) {
  //     var employees = data.data;
  //     $scope.visits.forEach(function (visit) {
  //       employees.forEach(function (employee) {
  //         if(visit.EMPKEYID === employee.KEYID) {
  //           visit.EMPLOYEE = employee.FNAME + ' ' + employee.LNAME;
  //         }
  //       });
  //     });
  //   });
  //   //find total hours
  //   for (var k = 0; k < $scope.visits.length; k++) {
  //     $scope.TOTALHOURS += parseInt($scope.visits[k].HOURS);
  //     console.log($scope.TOTALHOURS);
  //   }
  //   //find total quantity
  //   for (var j = 0; j < $scope.visits.length; j++) {
  //     if ($scope.visits[j].QUANTITY !== null) {
  //       $scope.TOTALQTY += parseInt($scope.visits[j].QUANTITY);
  //     }
  //   }
  //   //get visittype discipline and desc
  //   dataService.getAll('visitType').then(function (data) {
  //     var visitTypes = data.data;
  //     $scope.visits.forEach(function (visit) {
  //       visitTypes.forEach(function (visitType) {
  //         if (visit.VISITTYPE === visitType.KEYID) {
  //           visit.VISITTYPEDESC = visitType.DESC;
  //           visit.DISCIPLINE = visitType.DISCIPLINE;
  //         }
  //       });
  //     });
  //   });

  //   //get admission number
  //   $scope.visits.forEach(function (visit) {
  //     dataService.get('admission', {PATKEYID:$scope.patient.KEYID}).then(function(data) {
  //       $scope.admissions = data.data;

  //       $scope.admissions.forEach(function(admit) {
  //         if (visit.ADMITKEYID === admit.KEYID) {
  //           visit.ADMITNO = admit.ADMITNO;
  //         }
  //       });
  //     });
  //     dataService.get('episode', {PATKEYID:$scope.patient.KEYID}).then(function(data) {
  //       $scope.episodes = data.data;

  //       $scope.episodes.forEach(function(episode) {
  //         if (visit.EPIKEYID === episode.KEYID) {
  //           visit.EPISODENO = episode.EPISODENO;
  //         }
  //       });
  //     });
  //   });
  //   console.log($scope.visits);
  //   $scope.updatedVisitsList = $scope.visits;
  // });


  //fiter by admit
  $scope.changeAdmit = function() {
    $scope.filteredVisits = [];
    // console.log('change admit', $scope.ADMIT);
    //if admit is selected
    if ($scope.ADMIT.ADMITNO !== 'All') {
      var selectedAdmit = $scope.ADMIT;
      dataService.get('episode', {'ADMITKEYID': selectedAdmit.KEYID}).then(function(data) {
        if (data.data.length < 1) {
          $scope.episodes = [{'EPISODENO' : 'All'}];
          $scope.EPISODE = $scope.episodes[0];
        } else {
          $scope.episodes = [{'EPISODENO' : 'All'}].concat(data.data);
          $scope.EPISODE = $scope.episodes[$scope.episodes.length - 1];
        };

        if ($scope.EPISODE.EPISODENO === 'All') { //if episode is 'all'
          $scope.visits.forEach(function(visit) {
            if (visit.ADMITNO === selectedAdmit.ADMITNO) {
              $scope.filteredVisits.push(visit);
            };
          });
          if($scope.filteredVisits.length<1){
            $scope.errorMsg = true;
          } else {
            $scope.errorMsg = false;
          }
        } else { //if episode is selected
          var selectedEpisode = $scope.EPISODE;
          $scope.visits.forEach(function(visit) {
            if ( (visit.EPISODENO === selectedEpisode.EPISODENO) && (visit.ADMITNO === selectedAdmit.ADMITNO) ) {
              $scope.filteredVisits.push(visit);
            }
          });
          if($scope.filteredVisits.length<1){
            $scope.errorMsg = true;
            // console.log('5')
          } else {
            $scope.errorMsg = false;
          }
        }
        $scope.getTotals();
      });
    } else { //else show all
      $scope.filteredVisits = $scope.visits;

      $scope.episodes = [{'EPISODENO' : 'All'}];
      $scope.EPISODE = $scope.episodes[0];

      if ($scope.filteredVisits.length < 1) {
        $scope.errorMsg = true;
      } else {
        $scope.errorMsg = false;
      }
      $scope.getTotals();
    }
  };

  $scope.changeEpisode=function() {
    $scope.filteredVisits = [];
    //if episode is selected
    if ($scope.EPISODE.EPISODENO !== 'All') {
      var selectedEpisode = $scope.EPISODE;

      if ($scope.ADMIT.ADMITNO ==='All') { //if admit is 'all'
        $scope.visits.forEach(function(visit) {
          if (visit.EPISODENO === selectedEpisode.EPISODENO) {
            $scope.filteredVisits.push(visit);
          }
        });
        if ($scope.filteredVisits.length < 1){
          $scope.errorMsg = true;
        } else {
          $scope.errorMsg = false;
        }
      } else { //if admit is selected
        var selectedAdmit = $scope.ADMIT;
        $scope.visits.forEach(function(visit) {
          if ( (visit.EPISODENO === selectedEpisode.EPISODENO) && (visit.ADMITNO === selectedAdmit.ADMITNO) ) {
            $scope.filteredVisits.push(visit);
          }
        });
        if ($scope.filteredVisits.length < 1){
          $scope.errorMsg = true;
        } else {
          $scope.errorMsg = false;
        }
      }
    } else if ($scope.ADMIT.ADMITNO !=='All') { //if admit is selected
        var selectedAdmit = $scope.ADMIT;
        $scope.visits.forEach(function(visit) {
          if (visit.ADMITNO === selectedAdmit.ADMITNO) {
            $scope.filteredVisits.push(visit);
          }
        });
        if ($scope.filteredVisits.length < 1){
          $scope.errorMsg = true;
          // console.log('1')
        } else {
          $scope.errorMsg = false;
        }
    } else { //else show all;
      $scope.filteredVisits = $scope.visits;
      $scope.errorMsg = false;
    }
    $scope.getTotals();
  };


  // //fiter by admit
  // $scope.changeAdmit=function() {
  //   $scope.filteredVisits = [];
  //   //if admit is selected
  //   if ($scope.ADMIT.ADMITNO !== 'All') {
  //     var selectedAdmit = $scope.ADMIT;

  //     if ($scope.EPISODE.EPISODENO === 'All') { 
  //       $scope.visits.forEach(function(visit) {
  //         if (visit.ADMITKEYID === selectedAdmit.KEYID) {
  //           $scope.filteredVisits.push(visit);
  //         }
  //       });
  //       if ($scope.filteredVisits.length < 1) {
  //         error.log('a');
  //         $scope.errorMsg = true;
  //       } else {
  //         $scope.errorMsg = false;
  //       }
  //     } else { 
  //       var selectedEpisode = $scope.EPISODE;
  //       console.log('selectedEpisode', selectedEpisode);
  //       console.log('selectedAdmit', selectedAdmit);
  //       console.log('visits', $scope.visits);
  //       $scope.visits.forEach(function(visit) {
  //         if ( (visit.EPISODENO === selectedEpisode.EPISODENO) && (visit.ADMITNO === selectedAdmit.ADMITNO) ) {
  //           $scope.filteredVisits.push(visit);
  //         }
  //       });
  //       if ($scope.filteredVisits.length<1){
  //         $scope.errorMsg = true;
  //       } else {
  //         $scope.errorMsg = false;
  //       }
  //     }
  //   } else if ($scope.EPISODE.EPISODENO !== 'All') { //if episode is selected
  //       var selectedEpisode = $scope.EPISODE;
  //       $scope.visits.forEach(function(visit) {
  //         if (visit.EPIKEYID === selectedEpisode.KEYID) {
  //           $scope.filteredVisits.push(visit);
  //         }
  //       });
  //       if ($scope.filteredVisits.length < 1) {
  //         $scope.errorMsg = true;
  //       } else {
  //         $scope.errorMsg = false;
  //       }
  //   } else { //else show all
  //     $scope.filteredVisits = $scope.visits;
  //     $scope.errorMsg = false;
  //   }
  // };

  // $scope.changeEpisode=function() {
  //   $scope.updatedVisitsList = [];
  //   //if episode is selected
  //   if ($scope.EPISODE !== 'all') {
  //     var selectedEpisode = JSON.parse($scope.EPISODE);

  //     if ($scope.ADMIT ==='all') { //if admit is 'all'
  //       $scope.visits.forEach(function(visit) {
  //         if (visit.EPIKEYID === selectedEpisode.KEYID) {
  //           $scope.updatedVisitsList.push(visit);
  //         }
  //       });
  //       if($scope.updatedVisitsList.length<1){
  //         $scope.errorMsg = true;
  //       }else {
  //         $scope.errorMsg = false;
  //       }
  //     } else { //if admit is selected
  //       var selectedAdmit = JSON.parse($scope.ADMIT);
  //       $scope.visits.forEach(function(visit) {
  //         if ( (visit.EPIKEYID === selectedEpisode.KEYID) && (visit.ADMITKEYID === selectedAdmit.KEYID) ) {
  //           $scope.updatedVisitsList.push(visit);
  //         }
  //       });
  //       if($scope.updatedVisitsList.length<1){
  //         $scope.errorMsg = true;
  //       } else {
  //         $scope.errorMsg = false;
  //       }
  //     }
  //   } else if ($scope.ADMIT !=='all') { //if admit is selected
  //       var selectedAdmit = JSON.parse($scope.ADMIT);
  //       $scope.visits.forEach(function(visit) {
  //         if (visit.ADMITKEYID === selectedAdmit.KEYID) {
  //           $scope.updatedVisitsList.push(visit);
  //         }
  //       });
  //       if($scope.updatedVisitsList.length<1){
  //         $scope.errorMsg = true;
  //       }else {
  //         $scope.errorMsg = false;
  //       }
  //   } else { //else show all;
  //     $scope.updatedVisitsList = $scope.visits;
  //     $scope.errorMsg = false;
  //   }
  // };


});
