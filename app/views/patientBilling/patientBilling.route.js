app.config(function($stateProvider) {
  $stateProvider.state('patientBilling', {
    url: '/patientBilling',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/patientBilling/patientBilling.html',
        controller: 'PatientBillingController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
