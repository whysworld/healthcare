app.directive('headerNav', function(navService, $state, auth, agencyService) {
  return {
    scope: {
      destination: '@',
      patientnav: '@',
      patientnavopen: '@',
      empnav: '@',
      empnavopen: '@',
      billingnav: '@',
      settingsnav: '@',
      userprofile:'@',
      oasisnav: '@',
      patientcreatenav: '@',
      skillednav: '@',
    },
    templateUrl: 'views/directives/headerNav/views/headerNav.html',
    link: function(scope, attrs) {
      scope.user = auth.getUser();
      scope.agency=agencyService.get();

      if (scope.oasisnav==='true') {
        scope.rightMenuOpen=true;
        scope.secondarynav='.oasisForm-nav';
      }

      if (scope.skillednav==='true') {
        scope.rightMenuOpen=true;
        scope.secondarynav='.skilled-nurse-nav';
      }

      if (scope.patientcreatenav==='true') {
        scope.rightMenuOpen=true;
        scope.secondarynav='.form-nav';
      }

      //on log in and set iconMenu to true;
      if (navService.get()===null) {
        navService.set(true);
        scope.iconMenu=true;
        //if iconMenu false
      } else if (navService.get()===false){
        if (scope.patientnavopen==='true') {
          $('.header-nav-menu').css({left: '0'});
          scope.rightMenuOpen=true;
          scope.secondarynav='.right-menu';
          $('#left-nav-menu').css({left: '-210px'});
          $(scope.secondarynav).css({left: '0px'});
          $(scope.destination).css({'margin-left': '210px'});
        } else if (scope.empnavopen==='true') {
          scope.rightMenuOpen=true;
          scope.secondarynav='.employee-right-menu';
          $('#left-nav-menu').css({left: '-210px'});
          $(scope.secondarynav).css({left: '0px'});
          $(scope.destination).css({'margin-left': '210px'});
          //billing nav true
        } else if (scope.billingnav==='true') {
          scope.rightMenuOpen=true;
          scope.secondarynav='.billings-right-menu';
          $('#left-nav-menu').css({left: '-210px'});
          $(scope.secondarynav).css({left: '0px'});
          $(scope.destination).css({'margin-left': '210px'});
          //settings nav true
        } else if (scope.settingsnav==='true') {
          scope.rightMenuOpen=true;
          scope.secondarynav='.settings-right-menu';
          $('#left-nav-menu').css({left: '-210px'});
          $(scope.secondarynav).css({left: '0px'});
          $(scope.destination).css({'margin-left': '210px'});
          //user profile true
        } else if (scope.userprofile==='true') {
          $('#left-nav-menu').css({left: '-210px'});
          $(scope.destination).css({'margin-left': '0px'});
        }
        //if iconMenu true
      } else if (navService.get()===true) {
        //if patientnavopen true
        if (scope.patientnavopen==='true') {

          scope.rightMenuOpen=true;
          scope.secondarynav='.right-menu';
          $(scope.secondarynav).css({left: '160px'});
          //empnavopen true
        } else if (scope.empnavopen==='true') {
          scope.rightMenuOpen=true;
          scope.secondarynav='.employee-right-menu';
          $(scope.secondarynav).css({left: '160px'});
          $(scope.destination).css({'margin-left': '370px'});
          //billing nav true
        } else if (scope.billingnav==='true') {
          scope.rightMenuOpen=true;
          scope.secondarynav='.billings-right-menu';
          $(scope.secondarynav).css({left: '160px'});
          $(scope.destination).css({'margin-left': '370px'});
          //settings nav true
        } else if (scope.settingsnav==='true') {
          scope.rightMenuOpen=true;
          scope.secondarynav='.settings-right-menu';
          $(scope.secondarynav).css({left: '160px'});
          $(scope.destination).css({'margin-left': '370px'});
          //userprofile true
        } else if (scope.userprofile==='true') {
          $(scope.destination).css({'margin-left': '160px'});
        }
      }

      //watch for changes in emp nav
      scope.$watch('empnav', function(oldvalue, newvalue) {
        if (oldvalue!==newvalue) {
          scope.iconMenu=navService.get();
          scope.rightMenuOpen=true;
          scope.secondarynav='.employee-right-menu';
          _expandnav(scope);
        }
      });

      //watch for changes in patient nav
      scope.$watch('patientnav', function(oldvalue, newvalue) {
        if (oldvalue!==newvalue) {
          scope.iconMenu=navService.get();
          scope.rightMenuOpen=true;
          scope.secondarynav='.right-menu';
          _expandnav(scope);
        }
      });

      //open and close hamburger icon nav
      scope.showIconMenu=function () {
        scope.iconMenu=navService.get();
        scope.iconMenu=!scope.iconMenu;
        // console.log(scope.iconMenu)
        navService.set(scope.iconMenu);

        console.log('right menu', scope.rightMenuOpen)


        if (scope.iconMenu===false) {
          //close icon menu
          $('#left-nav-menu').animate({
            left: '-160'
          }, 200);
          $('.header-nav-menu').animate({
            left: '0'
          }, 200);

          //if right menu not open
          if (scope.rightMenuOpen==='false' || scope.rightMenuOpen===undefined) {
            $(scope.destination).animate({
              'margin-left': '0'
            }, 200);
            //if right menu open
          } else if (scope.rightMenuOpen===true){
            $(scope.destination).animate({
              'margin-left': '210'
            }, 200);
            $(scope.secondarynav).animate({
              left: '0'
            }, 200);
          }
        } else {
          //open icon menu
          $('#left-nav-menu').animate({
            left: '0'
          }, 200);
          $('.header-nav-menu').animate({
            left: '160'
          }, 200);

          //if right menu not open
          if (scope.rightMenuOpen==='false' || scope.rightMenuOpen===undefined) {
            $('#left-nav-menu').animate({
              left: '0'
            }, 200);
            $(scope.destination).animate({
              'margin-left': '160'
            }, 200);
            //if right menu open
          } else if (scope.rightMenuOpen===true) {
            $('#left-nav-menu').animate({
              left: '0'
            }, 200);
            $(scope.secondarynav).animate({
              left: '160'
            }, 200);
            $(scope.destination).animate({
              'margin-left': '370'
            }, 200);
          }
        }
      };

      scope.logout=function () {
        auth.logout();
      	$state.go('landingPage');
      };
    }
  };

  function _expandnav(scope) {
    //if icon bar is open
    if (scope.iconMenu===true) {
    //close right menu
    $(scope.secondarynav).animate({
        left: '-50'
      }, 200);
      $(scope.destination).animate({
        'margin-left': '160'
      }, 200);
      //open right menu
    $(scope.secondarynav).animate({
        left: '160'
      }, 200);
      $(scope.destination).animate({
        'margin-left': '370'
      }, 200);
      //if icon bar is closed
    } else if (scope.iconMenu===false){
      //close right menu
      $(scope.secondarynav).animate({
          left: '-210'
        }, 200);
        $(scope.destination).animate({
          'margin-left': '0'
        }, 200);
        //open right menu
      $(scope.secondarynav).animate({
          left: '0'
        }, 200);
        $(scope.destination).animate({
          'margin-left': '210'
        }, 200);
    }
  }
});
