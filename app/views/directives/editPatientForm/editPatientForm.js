app.directive('demographic', function() {
  return {
    templateUrl: 'views/directives/editPatientForm/views/demographic.html'
  };
});
app.directive('contact', function() {
  return {
    templateUrl: 'views/directives/editPatientForm/views/contact.html'
  };
});
app.directive('insurance', function() {
  return {
    templateUrl: 'views/directives/editPatientForm/views/insurance.html'
  };
});
app.directive('insurance2', function() {
  return {
    templateUrl: 'views/directives/editPatientForm/views/insurance2.html'
  };
});
app.directive('notes', function() {
  return {
    templateUrl: 'views/directives/editPatientForm/views/notes.html'
  };
});
app.directive('referral', function() {
  return {
    templateUrl: 'views/directives/editPatientForm/views/referral.html'
  };
});
app.directive('physician', function() {
  return {
    templateUrl: 'views/directives/editPatientForm/views/physician.html'
  };
});
app.directive('diagnosis', function() {
  return {
    templateUrl: 'views/directives/editPatientForm/views/diagnosis.html'
  };
});
app.directive('history', function() {
  return {
    templateUrl: 'views/directives/editPatientForm/views/history.html'
  };
});
app.directive('other', function() {
  return {
    templateUrl: 'views/directives/editPatientForm/views/other.html'
  };
});
