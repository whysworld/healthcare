app.directive("calendar", function() {
    return {
        restrict: "E",
        // scope: {
        //   // selected: "=",
        //   episode: '=',
        // },
        templateUrl: 'views/directives/calendar/views/calendar.html',
        link: function(scope, attrs) {


          // _buildCalendar(scope)

            scope.$watch('showVisits', function(oldvalue, newvalue) {
              console.log(oldvalue)
              console.log(newvalue)
                // if(scope.cake === !scope.cake) {
                  // console.log(oldvalue.length)
                  // console.log(newvalue.length)
                  // console.log(oldvalue)
                  // console.log(newvalue)
                  _buildCalendar(scope)
                // }
            });
            scope.$watch('episode', function(oldvalue, newvalue) {
                // if(scope.cake === !scope.cake) {
                  // console.log(oldvalue.length)
                  // console.log(newvalue.length)
                  // console.log(oldvalue)
                  // console.log(newvalue)
                  _buildCalendar(scope)
                // }
            });


        }
    };

    function _buildCalendar(scope){
      // console.log("building calendar")
      scope.selected = moment()

      ///testing
      // scope.selected = moment("09/12/2016", "DD/MM/YYYY");
      // scope.selected = _removeTime(scope.startEpi || moment());
      // scope.startEpi = moment(scope.episode.startDate, "DD/MM/YYYY");
      // scope.startEpi = _removeTime(scope.startEpi || moment());


      // scope.month = scope.startEpi.clone();
      scope.month=scope.selected.clone()

      // var start = scope.startEpi.clone();
      // var start=scope.selected.clone()
      scope.start = scope.selected.clone()
      // console.log("start", start.date())

      // start.date(1);
      // start.subtract(31, 'days')
      // _removeTime(start.day(0));

      _centerCalendar(scope, scope.start)

      _buildMonth(scope, scope.start, scope.month);

      scope.select = function(day) {
          // console.log(day)
          scope.selected = day.date;
          // displayVisits(scope, day);
          // console.log(day.date)

      };

      scope.next = function() {
          var next = scope.month.clone();
          _removeTime(next.month(next.month()+1).date(1));
          scope.month.month(scope.month.month()+1);
          _buildMonth(scope, next, scope.month);
      };

      scope.previous = function() {
          var previous = scope.month.clone();
          _removeTime(previous.month(previous.month()-1).date(1));
          scope.month.month(scope.month.month()-1);
          _buildMonth(scope, previous, scope.month);
      };
    }

    function _centerCalendar(scope, start) {
      console.log('building calendar:')
      //center calendar around submitted SOC DATE
      console.log(scope.lastAdmit)
      if (scope.lastAdmit.SOCDATE !== null) {
        // console.log(scope.lastAdmit.SOCDATE)
        var epistart = moment(scope.episode.FROMDATE);
        // console.log(epistart)
        // scope.start=taco.date(1);
        // scope.start=_removeTime(taco.day(0));
        // epistart = epistart.date(1)
        scope.start = _removeTime(epistart.day(0));
        console.log("center calendar around submitted SOC Date", scope.start)
      } else if (scope.SOCVISITSCHEDULED !== null) { //center calendar to show episode based on scheduled SOC date
        // console.log(scope.SOCVISITSCHEDULED)
        // var socstart = moment(scope.SOCVISITSCHEDULED,"DD/MM/YYYY");
        //  socstart = socstart.subtract(28, 'days');
        //  scope.start = _removeTime(socstart.day(0));
        var epistart = moment(scope.SOCVISITSCHEDULED);
        scope.start = _removeTime(epistart.day(0));
        scope.taco = true;

         console.log("center calendar around scheduled SOC visit date",scope.start)
      } else { //center calendar around today's date
        // console.log("dfss", scope.SOCVISITSCHEDULED)
        var blankstart = start;
        blankstart = blankstart.subtract(28, 'days');
        scope.start = _removeTime(blankstart.day(0));
        console.log("center calendar around todays Date", scope.start)
      }

      // if (scope.SOCVISITSCHEDULED === null){
      //   // var start = scope.selected.clone()
      //   var start = start
      //   console.log(start)
      //
      //     scope.start = start.subtract(31, 'days')
      // } elseif (scope.SOCVISITSCHEDULED !=== null &&) {
      //   var start = moment(scope.SOCVISITSCHEDULED,"DD/MM/YYYY");
      //   // console.log(start)
      //   // start = _removeTime(start)
      //
      //    scope.start = start.subtract(31, 'days')
      //    console.log(start)
      //
      // }
    }
    // function displayVisits (scope, day) {
    //   var clickedDate = moment(day.date).format("DD/MM/YYYY");
    //   scope.visits = [];
    //   for (var i = 0; i < scope.episode.visits.length; i++) {
    //     if ( clickedDate === scope.episode.visits[i].VISITDATE ) {
    //       scope.visits.push(scope.episode.visits[i]);
    //     }
    //   }
    // }

    function _removeTime(date) {
        return date.day(0).hour(0).minute(0).second(0).millisecond(0);
    }

    function _buildMonth(scope, start, month) {
        scope.weeks = [];

        var done = false, date = start.clone(), monthIndex = date.month(), count = 0;
        // console.log("as",start.clone())

        while (!done) {
            scope.weeks.push({ days: _buildWeek(scope, date.clone(), month) });
            date.add(1, "w");
            done = count++ >8;
            //  done = count++ > 8 && monthIndex !== date.month()
            monthIndex = date.month();
            // console.log("as",date.clone())
        }
        // console.log("weeks")
    }

    function _buildWeek(scope, date, month) {
        var days = [];
        // console.log(scope.episode)
        if (scope.episode !== null) {
          var startOfEpisode = moment(scope.episode.FROMDATE);
          var endOfEpisode = moment(scope.episode.THRUDATE);

          var firstFiveDays = startOfEpisode.clone().add(5, 'days');
          var lastFiveDays = endOfEpisode.clone().subtract(5, 'days');
        }
        // console.log(firstFiveDays)

        if (scope.taco === true) {
          // console.log(scope.start)
          var startOfEpisode = moment(scope.SOCVISITSCHEDULED);
          // console.log(startOfEpisode)
          var endOfEpisode = startOfEpisode.clone().add(59, 'days')
          // console.log(endOfEpisode)


          var firstFiveDays = null;
          var lastFiveDays = null;
        }

        if(scope.patient.PATSTATUS==='T'){
          var patientTransferred=true
          var transferDate=moment(scope.patientTransferDate)
        }

        for (var i = 0; i < 7; i++) {
            days.push({
                name: date.format("dd").substring(0, 1),
                number: date.date(),
                // isCurrentMonth: date.month() === month.month(),
                isToday: date.isSame(new Date(), "day"),
                date: date,
                isEpisode: moment(date).isBetween(startOfEpisode, endOfEpisode, null, '[]'),
                isFirstFiveDays: moment(date).isBetween(startOfEpisode, firstFiveDays, null, '[)'),
                isLastFiveDays: moment(date).isBetween(lastFiveDays, endOfEpisode, null, '(]'),
                patientTransferred: patientTransferred && moment(date).isAfter(transferDate),
                visits: sortVisits()
            });
            date = date.clone();
            date.add(1, "d");
            // console.log("date1",days)
        }
        return days;

        function sortVisits() {
          var allVisits = scope.showVisits;
          // console.log(allVisits)
          var visits = [];

          for (var i = 0; i < allVisits.length; i++) {
            // console.log(date.format("MM-DD-YYYY"));
            // console.log(moment(allVisits[i].VISITDATE).format('MM-DD-YYYY'))
            // console.log(allVisits[4].VISITDATE)
            if ( date.format("MM-DD-YYYY") === moment(allVisits[i].VISITDATE).format('MM-DD-YYYY') ) {
              visits.push(allVisits[i]);
            }
          } return visits;
        }
    }
});
