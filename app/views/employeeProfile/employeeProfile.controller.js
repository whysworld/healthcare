app.controller('EmployeeProfileController', function($scope, employeeService, dataService) {

  // $scope.employee = $state.params.employee;

	$scope.printSection = function (divName) {

	    var printContents = document.getElementById(divName).innerHTML;
	    var originalContents = document.body.innerHTML;

	    if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
	        var popupWin = window.open('', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
	        popupWin.window.focus();
	        popupWin.document.write('<!DOCTYPE html><html><head>' +
	            '<link rel="stylesheet" type="text/css" href="style.css" />' +
	            '</head><body onload="window.print()"><div class="reward-body">' + printContents + '</div></html>');
	        popupWin.onbeforeunload = function (event) {
	            popupWin.close();
	            return '.\n';
	        };
	        popupWin.onabort = function (event) {
	            popupWin.document.close();
	            popupWin.close();
	        }
	    } else {
	        var popupWin = window.open('', '_blank', 'width=800,height=600');
	        popupWin.document.open();
	        popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
	        popupWin.document.close();
	    }
	    popupWin.document.close();

	    return true;
	}

  $scope.employee = employeeService.get();
  $scope.notEditable = true;

  $scope.editMode = function() {
    $scope.notEditable = false;
    $scope.editModeBtn = true;
  };

  $scope.cancel = function() {
    $scope.notEditable = true;
    $scope.editModeBtn = false;
  };

  dataService.getAll('userGroup').then(function(data) {
    $scope.userGroups = data.data;
  });

	convertEMPSTATUScode();
	function convertEMPSTATUScode() {
		switch ($scope.employee.STATUS) {
			case 'I':
	      $scope.empStatus = 'Inactive';
				break;
			case 'A':
	      $scope.empStatus = 'Active';
				break;
		}
	}

	convertEMPCONTRACTORcode();
	function convertEMPCONTRACTORcode() {
		switch ($scope.employee.CONTRACTOR) {
			case 1:
	      $scope.empContractor = 'Yes';
				break;
			case 0:
	      $scope.empContractor = 'No';
				break;
		}
	}

  $scope.saveEmployeeEdits = function () {
      $scope.notEditable = true;
      $scope.editModeBtn = false;
    // $scope.editMode = false;
    // $scope.isCheckboxDisabled = true;
    // console.log($scope.employee.GROUPKEYID.DESCRIPTION)
    var editedEmployee = {
      KEYID: $scope.employee.KEYID,
      LOGINNAME: $scope.LOGINNAME,
      PASSWORD: $scope.PASSWORD,
      FNAME: $scope.FNAME,
      LNAME: $scope.LNAME,
      EMAIL: $scope.EMAIL,
      ADDRESS: $scope.ADDRESS,
      CITY: $scope.CITY,
      STATE: $scope.STATE,
			ZIP: $scope.ZIP,
      PHONE: $scope.PHONE,
			CELL: $scope.CELL,
  		FAX: $scope.FAX,
      STATUS: $scope.STATUS,
      CONTRACTOR: $scope.CONTRACTOR,
      TITLE: $scope.TITLE,
      DISCIPLINE: $scope.DISCIPLINE,
      GROUPKEYID: $scope.GROUPKEYID
    };
		console.log(editedEmployee);
    dataService.edit('employee', editedEmployee).then(function(response) {
      console.log(response);
			dataService.get('employee', {KEYID:$scope.employee.KEYID}).then(function(data) {
				var updatedEmployee=data.data[0];
				employeeService.set(updatedEmployee);
				$scope.employee=employeeService.get();
				convertEMPSTATUScode();
				convertEMPCONTRACTORcode();
			});
    });

  };
});
