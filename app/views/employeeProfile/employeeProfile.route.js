app.config(function($stateProvider) {
  $stateProvider.state('employeeProfile', {
    url: '/employeeProfile',
    params: {
      employee: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/employeeProfile/employeeProfile.html',
        controller: 'EmployeeProfileController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
