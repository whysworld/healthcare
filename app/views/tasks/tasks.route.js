app.config(function($stateProvider) {
  $stateProvider.state('tasks', {
    url: '/tasks',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/tasks/tasks.html',
        controller: 'TasksController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
