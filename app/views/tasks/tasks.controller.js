app.controller('TasksController', function($scope, $uibModal) {

  $scope.newTask = function() {
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'NewTaskController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/newTask/newTask.html',
        size: 'lg',
        resolve: {
          // task: $scope.taskObject
        }
      });
  };

  $scope.replyTask = function() {
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'ReplyTaskController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/replyTask/replyTask.html',
        size: 'lg',
        resolve: {
          // task: $scope.taskObject
        }
      });
  };

  $scope.taskObject = [{
      assignTo: 'Jack',
      taskStatus: 'Assigned',
      taskDueDate: '09/02/2016',
      taskDescription: 'You can easily customise the date formats and i18n strings used throughout the calendar by using the',
      reply: '',
    },
    {
      assignTo: 'Bob',
      taskStatus: 'Assigned',
      taskDueDate: '03/09/2016',
      taskDescription: 'Testing',
      reply: 'I have a reply',
    },
    {
      assignTo: 'Bill',
      taskStatus: 'Pending',
      taskDueDate: '11/05/2016',
      taskDescription: 'more testing',
      reply: '',
    }
  ];
});
