app.config(function($stateProvider) {
  $stateProvider.state('referralSources', {
    url: '/referralSources',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/referralSources/referralSources.html',
        controller: 'ReferralSourcesController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
