app.controller('ReferralSourcesController', function($scope, $uibModal, dataService) {

  dataService.getAll('referralSource').then(function(data) {
    $scope.referralSources=data.data;
  });

  $scope.newReferral = function() {
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'NewReferralSourceController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/newReferralSource/newReferralSource.html',
        size: 'md',
        // resolve: {
        //   // insurance: insurance
        // }
      });
  };

  $scope.showReferralSourceDetail=function(referralSource) {
    console.log(referralSource)
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'ViewReferralSourcesController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/viewReferralSources/viewReferralSources.html',
        size: 'md',
        resolve: {
          referralSource: referralSource
        }
      });
  };
});
