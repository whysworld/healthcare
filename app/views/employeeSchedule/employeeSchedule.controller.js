app.controller('EmployeeScheduleController', function($scope, $filter, employeeService, dataService) {

  $scope.employee = employeeService.get();
  console.log($scope.employee)

  //find today's date
  var today = $filter("date")(new Date(), 'yyyy/MM/dd');
  //set today's date as default for calendar
  $scope.DATEFROM = new Date(today);
  $scope.DATETO = new Date(today);


  //on page load get all visits for today's date
  dataService.get('visit', {VISITDATE: today}).then(function(data) {
    var visits=[];
    var allVisits = data.data;
    // console.log(visits);
    //remove all visits that are not for employee
    allVisits.forEach(function (visit) {
      if (visit.EMPKEYID === $scope.employee.KEYID) {
        visits.push(visit);
      }
    });
    console.log(visits)
    //get all visitTypes
    dataService.getAll('visitType').then(function(data) {
      $scope.visitTypes=data.data;
      //get all patients
      dataService.getAll('patient').then(function (data) {
        $scope.patients = data.data;
        getVisitInfo(visits);
      });
    });
  });

  function getVisitInfo(visits) {
    //clear activeVisitList
    console.log(visits)
    $scope.activeVisitList=[];

    visits.forEach(function(visit) {
      $scope.visitTypes.forEach(function (visitType) {
        if(visit.VISITTYPE===visitType.KEYID){
          visit.VISITTYPENAME=visitType.DESC;
        }
      });
      $scope.patients.forEach(function(patient) {
        if (visit.PATKEYID===patient.KEYID){
          visit.PATIENTNAME=patient.FNAME + ' ' + patient.LNAME;
        }
      });
    });
    console.log(visits);
    $scope.activeVisitList=visits;
    //sort active visit list by date
    $scope.activeVisitList.sort(function(a,b) {
      var dateA = a.VISITDATE.toLowerCase();
      var dateB = b.VISITDATE.toLowerCase();

      if(dateA > dateB)
        return -1;
      if(dateA < dateB)
        return 1;
      return 0;
    });
  }

  $scope.search=function() {
    //get array of dates between search period
    var startDate = moment($scope.DATEFROM);
    var endDate = moment($scope.DATETO);

    if (endDate.diff(startDate, 'days') > 7) {
      $scope.errorMsg=true;
      $scope.noResults = false;
      var selectedVisitDates=[];
      $scope.activeVisitList=[]
    } else {
      $scope.errorMsg=false;
      var selectedVisitDates=[];

      while(startDate.isBefore(endDate) || startDate.isSame(endDate)) {
        selectedVisitDates.push(startDate.clone());
        startDate.add('days', 1);
      }

      var visitsList=[];
      //get visits for dates
      console.log(selectedVisitDates)
      selectedVisitDates.forEach(function(visitDate) {
        dataService.get('visit', {VISITDATE: visitDate.format("YYYY-MM-DD")}).then(function(data) {
          var visitsArr = [];
          data.data.forEach(function (visit) {
            if (visit.EMPKEYID === $scope.employee.KEYID) {
              visitsArr.push(visit);
            }
          });
          console.log(visitsArr)
          visitsArr.forEach(function (visit) {
            //if patient is selected, sort patient
            if ($scope.PATIENT !== 'all'){
              var patient = JSON.parse($scope.PATIENT);
               if (visit.PATKEYID === patient.KEYID) {
                 visitsList.push(visit);
               }
            } else { //else show all visits between dates
              visitsList.push(visit);
            }
          });
          if (visitsList.length<1) {
            $scope.noResults = true;
            getVisitInfo(visitsList);
          } else {
            $scope.noResults = false;
            getVisitInfo(visitsList);
            console.log(visitsList);
          }
        });
      });
    }
  };



});
