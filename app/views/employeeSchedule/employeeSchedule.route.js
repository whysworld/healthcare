app.config(function($stateProvider) {
  $stateProvider.state('employeeSchedule', {
    url: '/employeeSchedule',
    params: {
      employee: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/employeeSchedule/employeeSchedule.html',
        controller: 'EmployeeScheduleController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
