app.config(function($stateProvider) {
  $stateProvider.state('employeeDashboard', {
    url: '/employeeDashboard',
    params: {
      employee: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/employeeDashboard/employeeDashboard.html',
        controller: 'EmployeeDashboardController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
