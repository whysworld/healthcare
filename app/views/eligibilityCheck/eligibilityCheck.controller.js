app.controller('EligibilityCheckController', function ($scope, $filter, patientService, dataService, $http, CONSTANTS, $httpParamSerializerJQLike, $uibModal, $timeout, $anchorScroll, $location) {
  // get patient
  $scope.patient = patientService.get()
  $scope.showLoader = false
  $scope.showSuccessAlert = false
  $scope.currentPage = 0
  $scope.numofpages = 0

  // format patient dob
  $scope.patient.DOB = $filter('date')($scope.patient.DOB, 'MM-dd-yyyy')
  var ageyear = $filter('date')($scope.patient.DOB, 'MM-dd-yyyy')
  ageyear = ageyear.split('-')

  $scope.dateDiff = function (birthMonth, birthDay, birthYear) {
    var todayDate = new Date()
    var todayYear = todayDate.getFullYear()
    var todayMonth = todayDate.getMonth()
    var todayDay = todayDate.getDate()
    var age = todayYear - birthYear

    if (todayMonth < birthMonth - 1) {
      age--
    }

    if (birthMonth - 1 === todayMonth && todayDay < birthDay) {
      age--
    }

    return age
  }

  $scope.dobyear = $scope.dateDiff(ageyear[0], ageyear[1], ageyear[2])
  // convert language code to show language description
  $scope.convertLANGUAGEcode = function () {
    if ($scope.patient.LANGUAGE !== null) {
      dataService.get('language', {
        'KEYID': $scope.patient.LANGUAGE
      }).then(function (data) {
        $scope.LANGUAGEDESC = data.data[0].DESC
      })
    }
  }

  // convert M0140 code to show M0140 description
  $scope.convertM0140code = function () {
    switch ($scope.patient.M0140) {
      case '1':
        $scope.M0140DESC = 'American Indian or Alaska Native'
        break
      case '2':
        $scope.M0140DESC = 'Asian'
        break
      case '3':
        $scope.M0140DESC = 'Black or African-American'
        break
      case '4':
        $scope.M0140DESC = 'Hispanic or Latino'
        break
      case '5':
        $scope.M0140DESC = 'Native Hawaiian or Pacific Islander'
        break
      case '6':
        $scope.M0140DESC = 'White'
        break
    }
  }

  $scope.getData = function (page = 1) {
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['eligibilitycheck'].getAll,
      method: 'POST',
      data: $httpParamSerializerJQLike({
        'PATID': $scope.patient.KEYID,
        offset: page,
        limit: 10
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }

    }).success(function (data) {
      $scope.updatedFormsList = data.responsedata
      $scope.currentPage = data.currentpage
      if (data.pages) {
        $scope.openpages = true
      }

      $scope.totalpages = data.pages
      $scope.numofpages = data.pages
    })
  }

  $scope.range = function (n) {
    return new Array(n)
  }

  $scope.goTo = function (page) {
    $scope.getData(page)
  }
  $scope.goToPreNext = function (sec) {
    console.log('=-=' + $scope.currentPage)
    if (sec === 1) {
      if ($scope.currentPage === 1) {
        $scope.getData(1)
      } else {
        $scope.getData($scope.currentPage - 1)
      }
    } else {
      // if($scope.numofpages <= $scope.currentPage)
      $scope.getData(parseInt($scope.currentPage) + 1)
    }
  }

  $scope.getEligibilityDetails = function () {
    $scope.showLoader = true
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['eligibilitycheck'].get,
      method: 'POST',
      data: $httpParamSerializerJQLike({
        'PATID': $scope.patient.KEYID,
        'LNAME': $scope.patient.LNAME,
        'FNAME': $scope.patient.FNAME,
        'DOB': $scope.patient.DOB
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).success(function (data) {
      if (data.status_code === 200) {
        $scope.getData()
        $scope.showLoader = false
      } else {
        $scope.showSuccessAlert = true
        $scope.successTextAlert = '503 Service Unavailable'
        $scope.showLoader = false
        $timeout(function () {
          $scope.showSuccessAlert = false
        }, 4000)
      }
    })
  }

  $scope.newVisit = function (selected, event, pecId) {
    // $location.hash('completedetails');
    $anchorScroll('completedetails')
    //          event.preventDefault();
    //
    //          var modalInstance = $uibModal.open({
    //            controller: 'EligibilitydetailController',
    //            ariaLabelledBy: 'modal-title',
    //            ariaDescribedBy: 'modal-body',
    //            templateUrl: './views/modals/eligibilitycheck/detailseligibility.html',
    //            size: 'lg',
    //            resolve: {
    //              id: function () {
    // return pecId;
    // }
    //            }
    //          });
    $scope.pecid = pecId
    $scope.showdetail = true
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['eligibilitypopup'].get,
      method: 'POST',
      data: $httpParamSerializerJQLike({
        pecId: $scope.pecid
      }),

      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).success(function (res) {
      $scope.allEsData = res.es_data
      $scope.allDtpData = res.dtp_data
    })
  }
  // call functions on page load
  $scope.convertLANGUAGEcode()
  $scope.convertM0140code()
  $scope.getData()
})

// This was added by Omkar but I'm not sure it's needed, can't find where it's Cancelled
// app.controller('EligibilitydetailController', function ($scope, $httpParamSerializerJQLike, $http, CONSTANTS, $filter, $uibModalInstance, dataService, id) {
//   $scope.pecid = id
//   // console.log($scope.action);
//   $http({
//     url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['eligibilitypopup'].get,
//     method: 'POST',
//     data: $httpParamSerializerJQLike({
//       pecId: $scope.pecid
//     }),
//
//     headers: {
//       'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
//     }
//   }).success(function (res) {
//     //      $scope.allGsData = res.gs_data;
//     //      $scope.allBHTData = res.bht_data;
//     //      $scope.allHLData = res.hl_data;
//     //      $scope.allNM1Data = res.nm1_data;
//     //      $scope.allREFData = res.ref_data;
//     //      $scope.allSEData = res.se_data;
//     //      $scope.allGEData = res.ge_data;
//     $scope.allEsData = res.es_data
//     $scope.allDtpData = res.dtp_data
//   })
// })

// Previous version of eligibility controller
// app.controller('EligibilityCheckController', function($scope, $filter, patientService, dataService) {
//   //get patient
//   $scope.patient = patientService.get();
//
//   //format patient dob
//   $scope.patient.DOB=$filter('date')($scope.patient.DOB, 'MM-dd-yyyy');
//
//   //convert language code to show language description
//   $scope.convertLANGUAGEcode=function() {
//     if ($scope.patient.LANGUAGE!== null) {
//       dataService.get('language', {'KEYID': $scope.patient.LANGUAGE}).then(function(data) {
//         $scope.LANGUAGEDESC = data.data[0].DESC;
//       });
//     }
//   };
//
//   //convert M0140 code to show M0140 description
//   $scope.convertM0140code=function() {
//       switch ($scope.patient.M0140) {
//           case '1':
//         $scope.M0140DESC = 'American Indian or Alaska Native';
//               break;
//           case '2':
//         $scope.M0140DESC = 'Asian';
//               break;
//       case '3':
//         $scope.M0140DESC = 'Black or African-American';
//         break;
//       case '4':
//         $scope.M0140DESC = 'Hispanic or Latino';
//         break;
//       case '5':
//         $scope.M0140DESC = 'Native Hawaiian or Pacific Islander';
//         break;
//       case '6':
//         $scope.M0140DESC = 'White';
//         break;
//       }
//   };
//
//   //call functions on page load
//   $scope.convertLANGUAGEcode();
//   $scope.convertM0140code();
// });
