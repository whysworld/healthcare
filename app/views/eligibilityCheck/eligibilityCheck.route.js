app.config(function($stateProvider) {
  $stateProvider.state('eligibilityCheck', {
    url: '/eligibilityCheck',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/eligibilityCheck/eligibilityCheck.html',
        controller: 'EligibilityCheckController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
