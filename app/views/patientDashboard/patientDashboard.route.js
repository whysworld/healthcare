app.config(function($stateProvider) {
  $stateProvider.state('patientDashboard', {
    url: '/patientDashboard/',
    params: {
      patient: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/patientDashboard/patientDashboard.html',
        controller: 'PatientDashboardController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
