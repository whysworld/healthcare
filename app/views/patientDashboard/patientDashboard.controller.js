app.controller('PatientDashboardController', function($scope, $state, $stateParams, $uibModal, patientService, dataService) {
  // if($stateParams.patient) {
  //   $scope.patientName = $stateParams.patient.name;
  //   $scope.patientAddress = $stateParams.patient.address;
  //   $scope.patientPhone = $stateParams.patient.phone;
  // }

  //get patient
  $scope.patient = patientService.get();

  //get patient insurance company name
  $scope.getInsuranceCoName=function () {
    dataService.get('insurance', {'KEYID': $scope.patient.INSCO}).then(function(data) {
      $scope.patient.INSCONAME = data.data[0].NAME;
    });
  };

  //check to see if SCHEDULE SOC button should be displayed
  dataService.get('form', {ADMITKEYID:$scope.patient.LSTADMIT}).then(function(data) {
    var forms = data.data;
    //If there are no forms for current patient admit, then SOC has not been scheduled
    if (forms.length===0) {
      $scope.SOCexists = false;
    // If there are forms for the current patient admit
    } else {
      for (var i = 0; i < forms.length; i++) {
        if (forms[i].FORMTYPE===2) { //check to see if there is a SOC OAsis form , if there is, hide schedule SOC button
          $scope.SOCexists = true;
          break;
        } else {
          $scope.SOCexists = false; //if there isnt' a SOC oasis form, then show button to schedule SOC
        }
      }
    }
  });

  $scope.error = function (event){
    alert("SOC exists or has already been scheduled");
  };

  $scope.uploadModal = function() {
    var modalInstance = $uibModal.open({
        controller: 'UploadDocumentController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/uploadDocument.html',
        size: 'lg',
        resolve: {
          patient: 10
        }
      });
  };

  $scope.scheduleSOC = function(event) {
    event.preventDefault();
    var modalInstance = $uibModal.open({
      controller: 'ScheduleSOCController',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/scheduleSOC/scheduleSOC.html',
      // windowClass: 'modal fade in',
      // windowClass: 'display-visit-modal',
      size: 'lg',
      resolve: {
      }
    });

    modalInstance.result.then(function (result) {
      $state.go('scheduling');
    });
  };

  $scope.getInsuranceCoName();

});
  //
  // $scope.closeNav = function() {
  //   // delete $scope.activePatient.selected;
  //   $('.right-menu').animate({
  //     'margin-left': '-150'
  //   }, 200);
  //   $('#patient-dashboard').animate({
  //     'margin-left': '60'
  //   }, 200);
  // };
//
//   $scope.patient = {
//       KEYID: 1,
//       PATIENTID: "PAT02",
//       LNAME: "Austin",
//       FNAME: "Becky",
//       MINITIAL: "M",
//       GENDER: "F",
//       DOB: "1950-06-25",
//       SSN: "078-05-1120",
//       LANGUAGE: null,
//       RELIGION: null,
//       MARITAL: 1,
//       LSTADMIT: null,
//       HISTORY: "Some patient history will go here",
//       ALLERGIES: "Patient allergies will go here",
//       ADDRESS: "429 Senator Street",
//       LOCTYPE: "1",
//       CITY: "Ada",
//       STATE: "Maryland",
//       ZIP: "5658",
//       HASSECONDADDR: 1,
//       ADDRESS2: "729 Schweikerts Walk",
//       LOCTYPE2: "2",
//       CITY2: "Movico",
//       STATE2: "Arizona",
//       ZIP2: "6082",
//       PHONE1: "19564263390",
//       PHONE2: "18845822071",
//       CELLPHONE: "19804542352",
//       EMAIL: "beckyaustin@canopoly.com",
//       INSCO: null,
//       INSID: "1-800-633-4227",
//       PATSTATUS: "5",
//       STATUSSORT: "01",
//       REASON: "",
//       ARCHIVED: 0,
//       M0140: "6",
//       CAHPSEXCL: 1,
//       MEDICARE_ELIGIBILITY: 0,
//       NOTES: "Some modified notes regarding the patient"
//   };
// });
