app.config(function($stateProvider) {
  $stateProvider.state('medication', {
    url: '/medication',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/medication/medication.html',
        controller: 'MedicationController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
