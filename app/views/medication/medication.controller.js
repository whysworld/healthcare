app.controller('MedicationController', function($scope, $filter, $uibModal, Blob, $http, CONSTANTS, FileSaver, patientService, auth, agencyService, dataService) {
  
  $scope.today = new Date().toISOString();

  /* get user info */
  $scope.user = auth.getUser();
  $scope.sign_firstname = $scope.user.firstName;
  $scope.sign_lastname = $scope.user.lastName;
  $scope.sign_title = $scope.user.title;

  /* get agency info */
  $scope.agency=agencyService.get();
  console.log('$scope.agency', $scope.agency);

  /* get patient info */
  $scope.patient=patientService.get();
  $scope.patient.DOB=$filter('date')($scope.patient.DOB, 'MM-dd-yyyy');
  console.log('$scope.patient', $scope.patient);

  //get admit info to populate form
	dataService.get('admission', {KEYID:$scope.patient.LSTADMIT}).then(function (data) {
		$scope.admit=data.data[0];

		// get physician's information, you can find same information on patientProfile page
		dataService.get('doctor', {'KEYID': $scope.admit.DOCTOR}).then(function(data) {
			$scope.DOCTOR = data.data[0];
		});
  });

  $scope.getPatientMedicationList = function() {
    dataService.get('patientMedication', {PATID: $scope.patient.KEYID}).then(function(data) {
      $scope.activeMedicationList = data.data;
      $scope.sign_enabled = 0;

      if ( $scope.activeMedicationList.length ) {
        if ( $scope.activeMedicationList[0].IS_SIGNED === 1 ) {
          $scope.sign_terms = 'checked';
          $scope.sign_title = $scope.activeMedicationList[0].SIGNER_TITLE;
          $scope.sign_firstname = $scope.activeMedicationList[0].SIGNER_FNAME;
          $scope.sign_mi = $scope.activeMedicationList[0].SIGNER_MI;
          $scope.sign_lastname = $scope.activeMedicationList[0].SIGNER_LNAME;
          $scope.sign_enabled = 1;
        }
      }
    });
  };

  $scope.getPatientMedicationList();

  $scope.newPatientMedication = function(event) {
    /* Show modal for add medication

    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'NewPatientMedicationController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/newPatientMedication/newPatientMedication.html',
        size: 'lg',
        resolve: {
          getPatientMedicationList: function() {
            return $scope.getPatientMedicationList;
          }
        }
        // resolve: {
        //   // insurance: insurance
        // }
      });
      */
    $scope.newMedication = {};
    arrNewFlag = [];
    $('#addSidenav').width( 550 );
  };

  $scope.showPatientMedication = function(medication, event) {
    /* Show modal for edit medication

    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'ViewPatientMedicationController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/viewPatientMedication/viewPatientMedication.html',
        size: 'lg',
        resolve: {
          patientMedication: medication,
          getPatientMedicationList: function() {
            return $scope.getPatientMedicationList;
          }
        }
    });
    // modalInstance.result.then(function () {
    //   $scope.getVisitTypeList();
    //   console.log($scope.activeVisitTypeList)
    // })
    */
    $scope.patient = patientService.get();

    $scope.medication = {
      KEYID : medication.KEYID,
      ADMIN_BY : medication.ADMIN_BY,
      CODE : medication.CODE,
      DOSE : medication.DOSE,
      UNIT : medication.UNIT,
      FORM : medication.FORM,
      TAKEN : medication.TAKEN,
      FREQUENCY : medication.FREQUENCY,
      NAME : medication.NAME,
      PHARMACY : medication.PHARMACY,
      PURPOSE : medication.PURPOSE,
      SIDE_EFFECTS : medication.SIDE_EFFECTS,
    };

    if (medication.DATE_STARTED != null)
 	   $scope.medication.DATE_STARTED = new Date($filter("date")(medication.DATE_STARTED, 'yyyy/MM/dd'));

    if (medication.DATE_DISC != null)
      $scope.medication.DATE_DISC = new Date($filter("date")(medication.DATE_DISC, 'yyyy/MM/dd'));

    $('#editSidenav').width( 550 );
  }

  // Right sidebar for add medication
  $scope.closeNav = function() {
    $('#addSidenav').width( 0 );
    $('#editSidenav').width( 0 );
  }

  $scope.getMedications=function(val) {
    console.log('getting medications:searching...', val);
    return dataService.search('Medication', {CODE:val, DESCRIPTION:val}).then(function (data) {
      $scope.codes=$filter('limitTo')(data.data, 15);
      console.log('$scope.codes',$scope.codes);
      return $scope.codes.map(function(code) {
        return code;
      });
    });
  };

  $scope.addPatientMedication = function(isInvalid) {
    if (isInvalid === true) {
      console.log("Form not valid");
    } else {
      // If the medication is valid, save it.

      var newMedication = {
        ADMIN_BY : $scope.newMedication.ADMIN_BY1,
        CODE : $scope.newMedication.CODE1,
        DATE_DISC : $filter("date")($scope.newMedication.DATE_DISC1, 'yyyy-MM-dd'),
        DATE_STARTED : $filter("date")($scope.newMedication.DATE_STARTED1, 'yyyy-MM-dd'),
        DOSE : $scope.newMedication.DOSE1,
        UNIT : $scope.newMedication.UNIT1,
        FORM : $scope.newMedication.FORM1,
        TAKEN : $scope.newMedication.TAKEN1,
        FREQUENCY : $scope.newMedication.FREQUENCY1,
        NAME : $scope.newMedication.NAME1,
        PATID : $scope.patient.KEYID,
        PHARMACY : $scope.newMedication.PHARMACY1,
        PURPOSE : $scope.newMedication.PURPOSE1,
        SIDE_EFFECTS : $scope.newMedication.SIDE_EFFECTS1,
        IS_SIGNED : 0,
        SIGNER_TITLE : "",
        SIGNER_FNAME : "",
        SIGNER_MI : "",
        SIGNER_LNAME : ""
      };

      dataService.add('patientMedication', newMedication).then(function(response) {
        console.log(response);
        $scope.getPatientMedicationList();
        $scope.closeNav();
      });
    }
  }

  $scope.editPatientMedication = function(isInvalid) {
    if (isInvalid === true) {
      console.log("Form not valid");
    } else {
      // If the medication is valid, save it.

      var editedMedication = {
        KEYID : $scope.medication.KEYID,
        ADMIN_BY : $scope.medication.ADMIN_BY,
        CODE : $scope.medication.CODE,
        DATE_STARTED : $filter("date")($scope.medication.DATE_STARTED, 'yyyy-MM-dd'),
        DATE_DISC : $filter("date")($scope.medication.DATE_DISC, 'yyyy-MM-dd'),
        DOSE : $scope.medication.DOSE,
        UNIT : $scope.medication.UNIT,
		    FORM : $scope.medication.FORM,
        TAKEN : $scope.medication.TAKEN,
        FREQUENCY : $scope.medication.FREQUENCY,
        NAME : $scope.medication.NAME,
        PATID : $scope.patient.KEYID,
        PHARMACY : $scope.medication.PHARMACY,
        PURPOSE : $scope.medication.PURPOSE,
        SIDE_EFFECTS : $scope.medication.SIDE_EFFECTS,
        IS_SIGNED : 0,
        SIGNER_TITLE : "",
        SIGNER_FNAME : "",
        SIGNER_MI : "",
        SIGNER_LNAME : ""
      };

      dataService.edit('patientMedication', editedMedication).then(function(response) {
        console.log(response);
        $scope.getPatientMedicationList();
        $scope.closeNav();
      });
    }
  }

  $scope.deletePatientMedication = function() {
    dataService.delete("patientMedication", {KEYID: $scope.medication.KEYID}).then(function(data) {
      console.log(data);
      $scope.getPatientMedicationList();
      $scope.closeNav();
    })
  }

  $scope.signPatientMedication = function() {

    var data = {
      PATID : $scope.patient.KEYID,
      IS_SIGNED : 1,
      SIGNER_TITLE : $scope.sign_title,
      SIGNER_FNAME : $scope.sign_firstname,
      SIGNER_MI : $scope.sign_mi,
      SIGNER_LNAME : $scope.sign_lastname
    }

    dataService.edit('patientMedication', data).then(function(response) {
      console.log( 'successfully signed patient medication!' );
      $scope.getPatientMedicationList();
    });
  }

  /*
    Save Medication to PDF
  */
  $scope.saveAllToPDF = function() {

    var active_medications = [];
    var inactive_medications = [];

    /* get active medications */
    for ( var i=0; i<$scope.activeMedicationList.length; i++ ) {
      if ( $scope.activeMedicationList[i].DATE_DISC > $scope.today ) {

        let medication_obj = {
          DateStarted: $scope.activeMedicationList[i].DATE_STARTED,
          DateDisc: $scope.activeMedicationList[i].DATE_DISC,
          Medication: $scope.activeMedicationList[i].NAME,
          Code: $scope.activeMedicationList[i].CODE,
          DoseRoute: $scope.activeMedicationList[i].DOSE + $scope.activeMedicationList[i].UNIT,
          Frequency: $scope.activeMedicationList[i].FREQUENCY,
          Purpose: $scope.activeMedicationList[i].PURPOSE,
          PotentialSideEffect: $scope.activeMedicationList[i].SIDE_EFFECTS,
          AdministeredBy: $scope.activeMedicationList[i].ADMIN_BY
        }
        active_medications.push( medication_obj );
      }
      if ( $scope.activeMedicationList[i].DATE_DISC <= $scope.today ) {

        let medication_obj = {
          DateStarted: $scope.activeMedicationList[i].DATE_STARTED,
          DateDisc: $scope.activeMedicationList[i].DATE_DISC,
          Medication: $scope.activeMedicationList[i].NAME,
          Code: $scope.activeMedicationList[i].CODE,
          DoseRoute: $scope.activeMedicationList[i].DOSE + $scope.activeMedicationList[i].UNIT,
          Frequency: $scope.activeMedicationList[i].FREQUENCY,
          Purpose: $scope.activeMedicationList[i].PURPOSE,
          PotentialSideEffect: $scope.activeMedicationList[i].SIDE_EFFECTS,
          AdministeredBy: $scope.activeMedicationList[i].ADMIN_BY
        }
        inactive_medications.push( medication_obj );
      }
    }
    /* */

    var data = {
      Type: 'ALL',

      ProviderName: $scope.agency.NAME,
      ProviderAddress: $scope.agency.ADDRESS===undefined?"": $scope.agency.ADDRESS,
			ProviderCity: "" + ($scope.agency.CITY===undefined?"": $scope.agency.CITY+",") + ($scope.agency.STATE===undefined?"": $scope.agency.STATE+",") + ($scope.agency.ZIP===undefined?"":$scope.agency.ZIP),
			ProviderPhone: $scope.agency.PHONE,
      ProviderFax: $scope.agency.FAX,

      PatientName: $scope.patient.FNAME + ' ' + $scope.patient.LNAME,
      MRN: $scope.patient.INSID,
      Physician: $scope.DOCTOR.FNAME + " " + $scope.DOCTOR.LNAME,
      Allergies: "",
      Weight: "",
      Height: "",
      AdministeredBy: "",
      Pharmacy: "",
      PhysicianPhone: $scope.DOCTOR.PHONE,
      PhysicianFax: $scope.DOCTOR.FAX,

      ActiveMedications: active_medications,
      DiscontinuedMedications: inactive_medications
    }

    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['patientMedication'].getPDF,
      method: 'GET',
      params: data,
      paramSerializer: '$httpParamSerializerJQLike',
      responseType: 'arraybuffer'
    }).then(function(data) {
      console.log('data', data);
      console.log('headers', data.headers());
  
      var contentType = data.headers()['content-type'];
      var disposition = data.headers()['content-disposition'];
      console.log( 'content-type', contentType );
      console.log('disposition', disposition);
  
      /*
      if (disposition && disposition.indexOf('attachment') !== -1) {
        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        var matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) { 
          filename = matches[1].replace(/['"]/g, '');
        } else {
          filename = "unknown";
        }
      }
      */
  
      filename = 'PatientMedication';
      console.log('filename', filename);
      console.log('contentType', contentType)
      console.log('data.data', data.data);
      
      var dataFile = new Blob([data.data], { type: contentType });
  
      console.log('dataFile', dataFile);
  
      FileSaver.saveAs(dataFile, filename);
    });
  }

  $scope.saveActiveToPDF = function() {

    var active_medications = [];

    /* get active medications */
    for ( var i=0; i<$scope.activeMedicationList.length; i++ ) {
      if ( $scope.activeMedicationList[i].DATE_DISC > $scope.today ) {

        let medication_obj = {
          DateStarted: $scope.activeMedicationList[i].DATE_STARTED,
          DateDisc: $scope.activeMedicationList[i].DATE_DISC,
          Medication: $scope.activeMedicationList[i].NAME,
          Code: $scope.activeMedicationList[i].CODE,
          DoseRoute: $scope.activeMedicationList[i].DOSE + $scope.activeMedicationList[i].UNIT,
          Frequency: $scope.activeMedicationList[i].FREQUENCY,
          Purpose: $scope.activeMedicationList[i].PURPOSE,
          PotentialSideEffect: $scope.activeMedicationList[i].SIDE_EFFECTS,
          AdministeredBy: $scope.activeMedicationList[i].ADMIN_BY
        }
        active_medications.push( medication_obj );
      } 
    }
    /* */

    var data = {
      Type: 'ACTIVE',
      
      ProviderAddress: $scope.agency.ADDRESS===undefined?"": $scope.agency.ADDRESS,
      ProviderName: $scope.agency.NAME,
			ProviderCity: "" + ($scope.agency.CITY===undefined?"": $scope.agency.CITY+",") + ($scope.agency.STATE===undefined?"": $scope.agency.STATE+",") + ($scope.agency.ZIP===undefined?"":$scope.agency.ZIP),
			ProviderPhone: $scope.agency.PHONE,
      ProviderFax: $scope.agency.FAX,

      PatientName: $scope.patient.FNAME + ' ' + $scope.patient.LNAME,
      MRN: $scope.patient.INSID,
      Physician: $scope.DOCTOR.FNAME + " " + $scope.DOCTOR.LNAME,
      Allergies: "",
      Weight: "",
      Height: "",
      AdministeredBy: "",
      Pharmacy: "",
      PhysicianPhone: $scope.DOCTOR.PHONE,
      PhysicianFax: $scope.DOCTOR.FAX,

      ActiveMedications: active_medications,
      DiscontinuedMedications: []
    }

    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['patientMedication'].getPDF,
      method: 'GET',
      params: data,
      paramSerializer: '$httpParamSerializerJQLike',
      responseType: 'arraybuffer'
    }).then(function(data) {
      console.log('data', data);
      console.log('headers', data.headers());
  
      var contentType = data.headers()['content-type'];
      var disposition = data.headers()['content-disposition'];
      console.log( 'content-type', contentType );
      console.log('disposition', disposition);
  
      /*
      if (disposition && disposition.indexOf('attachment') !== -1) {
        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        var matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) { 
          filename = matches[1].replace(/['"]/g, '');
        } else {
          filename = "unknown";
        }
      }
      */
  
      filename = 'PatientMedication';
      console.log('filename', filename);
      console.log('contentType', contentType)
      console.log('data.data', data.data);
      
      var dataFile = new Blob([data.data], { type: contentType });
  
      console.log('dataFile', dataFile);
  
      FileSaver.saveAs(dataFile, filename);
    });
  }
  /* */

  $('#medication_main').click( function(e) {
    if ($(e.target).parent('.medication-actions').length)
      return;
    if ($(e.target).hasClass('medication-edit'))
      return;
    $scope.closeNav();
  } )

  /** Set style for new medication when hover, focus, error on input */

  function setFormStyle( id, background_color, border_color, color, input_border_color ) {
    $("#" + id).css( 'background-color', background_color );
    $("#" + id).css( 'border-color', border_color );
    $("#" + id).css( 'color', color );
    $("[name='" + id + "']").css( 'border-color', input_border_color );
  }

  var arrNewID = ['PHARMACY1', 'ADMIN_BY1', 'DATE_STARTED1', 'DATE_DISC1', 'NAME1', 'CODE1', 'SIDE_EFFECTS1', 'DOSE1', 'UNIT1', 'FREQUENCY1', 'FORM1', 'TAKEN1', 'PURPOSE1'];
  var arrNewFlag = [];

  for ( let i=0; i<arrNewID.length; i++ ) {

    arrNewFlag.push(false);

    $("[name='" + arrNewID[i] + "']").hover( 
      function() {
        if ( $("[name='" + arrNewID[i] + "']").is(':focus') ) return;
        setFormStyle( arrNewID[i], '#D1CED3', '#D1CED3', '#555', '#8B8B90' );
      },
      function() {
        if ( $("[name='" + arrNewID[i] + "']").is(':focus') ) return;
        if ( ($scope.newMedication[ arrNewID[i] ] !== "" && $scope.newMedication[ arrNewID[i] ] !== undefined) || !arrNewFlag[i] ){
          setFormStyle( arrNewID[i], '#eee', '#ccc', '#555', '#ccc' );
        }
        if ( ($scope.newMedication[ arrNewID[i] ] === "" || $scope.newMedication[ arrNewID[i] ] === undefined ) && arrNewFlag[i] ) {
          if ( arrNewID[i] !== 'DATE_DISC1' ) {
            setFormStyle( arrNewID[i], 'red', 'red', 'white', 'red' );
          } else {
            setFormStyle( arrNewID[i], '#eee', '#ccc', '#555', '#ccc' );
          }
        }
      }
    );
  
    $("[name='" + arrNewID[i] + "']").focus( function() {
      arrNewFlag[i] = true;
      setFormStyle( arrNewID[i], '#70499B', '#5E2E88', 'white', '#5E2E88' );
    } );
  
    $("[name='" + arrNewID[i] + "']").blur( function() {
      if ( $scope.newMedication[ arrNewID[i] ] === "" || $scope.newMedication[ arrNewID[i] ] === undefined ) {
        if ( arrNewID[i] !== 'DATE_DISC1' ) {
          setFormStyle( arrNewID[i], 'red', 'red', 'white', 'red' );
        } else {
          setFormStyle( arrNewID[i], '#eee', '#ccc', '#555', '#ccc' );
        }
      } else {
        setFormStyle( arrNewID[i], '#eee', '#ccc', '#555', '#ccc' );
      }
    } );
  }
  /* */

  /** Set style for edit medication when hover, focus, error on input */

  var arrEditID = ['PHARMACY', 'ADMIN_BY', 'DATE_STARTED', 'DATE_DISC', 'NAME', 'CODE', 'SIDE_EFFECTS', 'DOSE', 'UNIT', 'FREQUENCY', 'FORM', 'TAKEN', 'PURPOSE'];
  var arrEditFlag = [];

  for ( let i=0; i<arrEditID.length; i++ ) {

    arrEditFlag.push(false);

    $("[name='" + arrEditID[i] + "']").hover( 
      function() {
        if ( $("[name='" + arrEditID[i] + "']").is(':focus') ) return;
        setFormStyle( arrEditID[i], '#D1CED3', '#D1CED3', '#555', '#8B8B90' );
      },
      function() {
        if ( $("[name='" + arrEditID[i] + "']").is(':focus') ) return;
        if ( ($scope.medication[ arrEditID[i] ] !== "" && $scope.medication[ arrEditID[i] ] !== undefined) || !arrEditFlag[i] ){
          setFormStyle( arrEditID[i], '#eee', '#ccc', '#555', '#ccc' );
        }
        if ( ($scope.medication[ arrEditID[i] ] === "" || $scope.medication[ arrEditID[i] ] === undefined ) && arrEditFlag[i] ) {
          if ( arrEditID[i] !== 'DATE_DISC' ) {
            setFormStyle( arrEditID[i], 'red', 'red', 'white', 'red' );
          } else {
            setFormStyle( arrEditID[i], '#eee', '#ccc', '#555', '#ccc' );
          }
        }
      }
    );
  
    $("[name='" + arrEditID[i] + "']").focus( function() {
      arrEditFlag[i] = true;
      setFormStyle( arrEditID[i], '#70499B', '#5E2E88', 'white', '#5E2E88' );
    } );
  
    $("[name='" + arrEditID[i] + "']").blur( function() {
      if (  $scope.medication[ arrEditID[i] ] === "" || $scope.medication[ arrEditID[i] ] === undefined ) {
        if ( arrEditID[i] !== 'DATE_DISC' ) {
          setFormStyle( arrEditID[i], 'red', 'red', 'white', 'red' );
        } else {
          setFormStyle( arrEditID[i], '#eee', '#ccc', '#555', '#ccc' );
        } 
      } else {
        setFormStyle( arrEditID[i], '#eee', '#ccc', '#555', '#ccc' );
      }
    } );
  }
  /* */
});
