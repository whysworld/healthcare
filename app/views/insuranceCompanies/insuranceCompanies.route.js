app.config(function($stateProvider) {
  $stateProvider.state('insuranceCompanies', {
    url: '/insuranceCompanies',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/insuranceCompanies/insuranceCompanies.html',
        controller: 'InsuranceCompaniesController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
