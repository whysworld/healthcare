app.controller('InsuranceCompaniesController', function($scope, $uibModal,  dataService) {

  dataService.getAll('insurance').then(function(data) {
    console.log(data);
    // sortInsurance(data);
    $scope.insurances = data.data;
  });


  $scope.showInsuranceDetail = function(insurance) {
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'ViewInsuranceController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/viewInsurance/viewInsurance.html',
        size: 'lg',
        resolve: {
          insurance: insurance
        }
      });
      modalInstance.result.then(function () {
        dataService.getAll('insurance').then(function(data) {
          console.log(data);
          $scope.insurances = data.data;
        });
      });
  };

  $scope.newInsurance = function(event) {
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'NewInsuranceController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/newInsurance/newInsurance.html',
        size: 'lg',
        // resolve: {
        //   // insurance: insurance
        // }
      });
      modalInstance.result.then(function () {
        dataService.getAll('insurance').then(function(data) {
          $scope.insurances = data.data;
        });
      });
  };



  $scope.activeInsuranceList = [{}];


  var allInsurances = [];
  var insuranceType1 = [];
  var insuranceType2 = [];
  var insuranceType3 = [];

  function sortInsurance(insurances) {
    allInsurances = insurances;
    $scope.activeInsuranceList = allInsurances;
    console.log($scope.activeInsuranceList);
    for (var i = 0; i < insurances.length; i++) {
      switch(insurances[i].type) {
        case "insurances[i].type":
          // insuranceArr[i].type = 'insuranceType1';
          // insuranceType1.push(insuranceArr[i]);
          // insuranceType1 = insuranceArr[i];
          break;
        case "insuranceType2":
          // insuranceArr[i].type = 'insuranceType2';
          // insuranceType2.push(insuranceArr[i]);
          // insuranceType2 = insuranceArr[i];

          break;
        case "insuranceType3":
          // insuranceArr[i].type = 'insuranceType3';
          // insuranceType3.push(insuranceArr[i]);
          // insuranceType3 = insuranceArr[i];
          break;
      }
    }
  }

  // $scope.updateInsuranceList = function(insuranceType) {
  //   switch(insuranceType) {
  //     case "ALL":
  //       $scope.activeInsuranceList = allInsurances;
  //       break;
  //     case "INSURANCETYPE1":
  //       $scope.activeInsuranceList = insuranceType1;
  //       break;
  //     case "INSURANCETYPE2":
  //       $scope.activeInsuranceList = insuranceType2;
  //       break;
  //     case "INSURANCETYPE3":
  //       $scope.activeInsuranceList = insuranceType3;
  //       break;
  //   }
    // console.log(insuranceType3)
  // };



  //
  // var insuranceArr = [{
  //   type: "insuranceType1",
  //   name: "ABC Insured",
  //   address: "1221 Queen Street",
  //   city: "Sacremento",
  //   state: "CA",
  //   zip: "92112",
  //   phone: "1-916-232-0000"
  // },
  // {
  //   type: "insuranceType2",
  //   name: "100% Insured",
  //   address: "1221 Queen Street",
  //   city: "Sacremento",
  //   state: "CA",
  //   zip: "92112",
  //   phone: "1-916-232-0000"
  // },
  // {
  //   type: "insuranceType3",
  //   name: "Always Insured",
  //   address: "1221 Queen Street",
  //   city: "Sacremento",
  //   state: "CA",
  //   zip: "92112",
  //   phone: "1-916-232-0000"
  // }];
});
