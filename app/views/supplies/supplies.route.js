app.config(function($stateProvider) {
  $stateProvider.state('supplies', {
    url: '/supplies',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/supplies/supplies.html',
        controller: 'SuppliesController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
