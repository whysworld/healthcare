app.controller('SuppliesController', function($scope) {


  $scope.noEdit = true;

  $scope.editSupply = function($index) {
    $scope.index[$index] = true;
  }

  $scope.activeSupplyList = [{
    code: "1210",
    description: "Neddles 18gx1 1/2",
    type: "Non-Routine",
    revcode: "0270",
    hcpc: "A4215",
    billable: true,
    amount: "$0.12"
  },
  {
    code: "1210",
    description: "Neddles 18gx1 1/2",
    type: "Non-Routine",
    revcode: "0270",
    hcpc: "A4215",
    billable: true,
    amount: "$0.12"
  },
  {
    code: "1210",
    description: "Neddles 18gx1 1/2",
    type: "Non-Routine",
    revcode: "0270",
    hcpc: "A4215",
    billable: true,
    amount: "$0.12"
  },
  {
    code: "1210",
    description: "Neddles 18gx1 1/2",
    type: "Non-Routine",
    revcode: "0270",
    hcpc: "A4215",
    billable: true,
    amount: "$0.12"
  },
  {
    code: "1210",
    description: "Neddles 18gx1 1/2",
    type: "Non-Routine",
    revcode: "0270",
    hcpc: "A4215",
    billable: true,
    amount: "$0.12"
  },
  {
    code: "0110",
    description: "Normal Saline 1000 cc",
    type: "Non-Routine",
    revcode: "0270",
    hcpc: "A4215",
    billable: true,
    amount: "$4.92"
  }]
});
