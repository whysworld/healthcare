app.config(function($stateProvider) {
  $stateProvider.state('remittanceAdvice', {
    url: '/remittanceAdvice',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/remittanceAdvice/remittanceAdvice.html',
        controller: 'RemittanceAdviceController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
