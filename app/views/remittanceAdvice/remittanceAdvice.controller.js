app.controller('RemittanceAdviceController', function ($scope, CONSTANTS, $http, $httpParamSerializerJQLike) {
  $scope.listView = true
  $http({
    url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['remittanceadvice'].get,
    method: 'POST',
    // data: $httpParamSerializerJQLike({"PATID":$scope.patient.KEYID}),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    }

  }).success(function (data) {
    $scope.remittances = data
  })
  $scope.goToDetails = function (listId) {
    $scope.listView = false
    $scope.detailView = true
    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['remittanceadvice'].getAll,
      method: 'POST',
      data: $httpParamSerializerJQLike({
        'LISTID': listId
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }

    }).success(function (data) {
      $scope.detailRemittance = data
    })
  }
  $scope.goToBack = function (listId) {
    $scope.listView = true
    $scope.detailView = false
  }
})

// Old controller
// app.controller('RemittanceAdviceController', function($scope) {
//   //sample starter data
//   $scope.remittances = [
//     {
//       REMITTANCEID: "REMIT01390",
//       REMITTANCEDATE: "02/23/2017",
//       PAYMENTDATE:'02/25/2017',
//       PROVIDER_PAYMENT: '$2,291',
//       CLAIM_COUNT: 1,
//     },
//     {
//       REMITTANCEID: "EFT461129",
//       REMITTANCEDATE: "11/23/2016",
//       PAYMENTDATE:'11/25/2016',
//       PROVIDER_PAYMENT: '$1,495',
//       CLAIM_COUNT: 2,
//     },
//     {
//       REMITTANCEID: "EFT461129",
//       REMITTANCEDATE: "01/02/2017",
//       PAYMENTDATE:'$3,256',
//       PROVIDER_PAYMENT: '10/21/2016',
//       CLAIM_COUNT: 5,
//     }
//   ]
//
// })
