app.config(function($stateProvider) {
  $stateProvider.state('employeeFileUpload', {
    url: '/employeeFileUpload',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/employeeFileUpload/employeeFileUpload.html',
        controller: 'EmployeeFileUploadController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
