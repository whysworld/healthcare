app.controller('EmployeeFileUploadController', function($scope) {


  $scope.toggleAll = function() {
    // console.log($scope.isAllSelected)
     var toggleStatus = $scope.isAllSelected;
    //  console.log(toggleStatus)
     angular.forEach($scope.files, function(file) {
       file.selected = toggleStatus;
      //  console.log(file.selected)
      //  console.log(file)
     });
  };

  $scope.fileToggled = function(){
    $scope.isAllSelected = $scope.files.every(function(file){
      return file.selected;
    });
  };



  $scope.files = [{
    user: "User",
    name: "Test",
    uploadBy: "Ana",
    date: "10/02/2016",
    size: "10kb"
  },
  {
    user: "User2",
    name: "aFileName",
    uploadBy: "John",
    date: "11/12/2016",
    size: "121kb"
  },
  {
    user: "User3",
    name: "fileName2",
    uploadBy: "Dan",
    date: "08/17/2016",
    size: "78kb"
  },
  {
    user: "User4",
    name: "moreFiles",
    uploadBy: "Sarah",
    date: "09/09/2016",
    size: "16kb"
  }];

});
