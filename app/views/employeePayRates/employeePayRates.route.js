app.config(function($stateProvider) {
  $stateProvider.state('employeePayRates', {
    url: '/employeePayRates',
    params: {
      employee: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/employeePayRates/employeePayRates.html',
        controller: 'EmployeePayRatesController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
