app.controller('EmployeePayRatesController', function($scope, $uibModal, employeeService, dataService) {

  $scope.getEmployee=function () {
    $scope.employee = employeeService.get();
  };

  $scope.getVisitTypes=function () {
    dataService.getAll('visitType').then(function (data) {
      $scope.visitTypes = data.data;
      //get custom pay rate
      dataService.get('employeePayrate', {EMPLOYEE_ID: $scope.employee.KEYID}).then(function (data) {
        $scope.empPayrates=data.data;
          $scope.visitTypes.forEach(function (visitType) {
            $scope.empPayrates.forEach(function (payrate) {
            if (visitType.KEYID===payrate.VISITTYPE_ID) {
              visitType.customEmpPayrate=payrate.EMPPAY;
              visitType.PERVISIT=payrate.EMPPAY_PERVISIT;
            }
          });
        });
        console.log('$scope.visitTypes', $scope.visitTypes);
      });
    });
  };

  $scope.addEmployeePayRate=function () {
    event.preventDefault();
    var modalInstance = $uibModal.open({
      controller: 'NewEmployeePayRates',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/newEmployeePayRates/newEmployeePayRates.html',
      size: 'md',
      resolve: {
        employee: $scope.employee,
        // empPayrates: function() {
        //   return $scope.empPayrates;
        // },
        // visitTypes: function() {
        //   return $scope.visitTypes;
        // }
      }
    });
    modalInstance.result.then(function () {
      $scope.getVisitTypes();
    });
  };

  $scope.editEmployeePayRate=function (visitType) {
    event.preventDefault();
    var modalInstance = $uibModal.open({
      controller: 'ViewEmployeePayRates',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/viewEmployeePayRates/viewEmployeePayRates.html',
      size: 'md',
      resolve: {
        employee: $scope.employee,
        // empPayrates: function() {
        //   return $scope.empPayrates;
        // },
        visitType: visitType
      }
    });
    modalInstance.result.then(function () {
      $scope.getVisitTypes();
    });
  };

  $scope.deleteEmployeePayRate=function (visitType) {
    console.log('visitType', visitType);
    if (confirm("Are you sure you want to delete selected adjusted employee pay rate?")) {
      dataService.delete('employeePayrate', {EMPLOYEE_ID:$scope.employee.KEYID, VISITTYPE_ID:visitType.KEYID}).then(function (response) {
        console.log('deleting payrate', response);
        if (response.status==='success') {
          $scope.getVisitTypes();
        }
      });
    }
  };


  $scope.getEmployee();
  $scope.getVisitTypes();
});
