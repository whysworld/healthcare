app.config(function($stateProvider) {
  $stateProvider.state('orders', {
    url: '/orders',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/orders/orders.html',
        controller: 'OrdersController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
