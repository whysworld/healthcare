app.controller('OrdersController', function($scope, $filter, $uibModal, dataService, $window){

  $scope.countMoreThan30Days=0;
  $scope.countMoreThan20Days=0;

  //get orders list
  $scope.populateOrders=function () {
    dataService.getAll('orders').then(function (data) {
      $scope.orders=data.data;
      console.log('$scope.orders', data.data);
      //format date for front end
      $scope.orders.forEach(function (order) {
        if (order.SENT) {
          order.SENT=$filter('date')(new Date(order.SENT), 'MM-dd-yyyy');
        }
        if (order.RECEIVED) {
          order.RECEIVED=$filter('date')(new Date(order.RECEIVED), 'MM-dd-yyyy');
        }
        if (order.PHYSICIANSIGNDATE) {
          order.PHYSICIANSIGNDATE=$filter('date')(new Date(order.PHYSICIANSIGNDATE), 'MM-dd-yyyy');
        }
        if (order.ORDERSDATE) {
          var orderDate = moment(order.ORDERSDATE);
          var currentDate = Date.now();
          order.DAYSAGO = orderDate.diff(currentDate, 'days');
          if (order.DAYSAGO<-30) {
            $scope.countMoreThan30Days+=1;
            $scope.countMoreThan20Days+=1;
          } else if (order.DAYSAGO<-20) {
            $scope.countMoreThan20Days+=1;
          }

          order.ORDERSDATE=$filter('date')(new Date(order.ORDERSDATE), 'MM-dd-yyyy');
        }
      });
      console.log($scope.countMoreThan20Days);
      console.log($scope.countMoreThan30Days);
    });
  };

  //open edit order modal
  $scope.editOrder=function (event, order, print) {
    // console.log(order);
    console.log(print)
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'NewOrderDatesController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/newOrderDates/newOrderDates.html',
        size: 'md',
        resolve: {
          order: order,
          print: print,
        }
      });
    modalInstance.result.then(function () {
      //update orders list
      $scope.populateOrders();
    });
  };

  $scope.populateOrders();
});
