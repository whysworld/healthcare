app.controller('RecordsController', function ($scope, $filter, CONSTANTS, $uibModal, $state, $stateParams, patientService, dataService, formService) {
  $scope.patient = patientService.get()
  $scope.currentForm = 'Record'

  $scope.nextForm = function (current, destination) {
    if ($scope.currentForm !== destination) {
      $scope.currentForm = destination
      $('.' + current).fadeOut('fast', function () {
        $('.' + destination).fadeIn('fast')
      })
    }
  }

  $scope.activeForm = function (form) {
    if ($scope.currentForm === form) {
      return true
    }
  }

  $scope.getRecords = function () {
    dataService.get('listPatientRecords', {
      'PATKEYID': $scope.patient.KEYID
    }).then(function (data) {
      $scope.records = data.data
      $scope.forms = []
      $scope.updatedFormsList = []

      // get admissions for patient
      dataService.get('admission', {
        PATKEYID: $scope.patient.KEYID
      }).then(function (data) {
        $scope.admissions = data.data

        // generate forms list
        angular.forEach($scope.records, function (record) {
          if (record.EPIKEYID === null) {
            angular.forEach(record.FORMS, function (form) {
              angular.forEach($scope.admissions, function (admit) {
                if (record.ADMITKEYID === admit.KEYID) {
                  form.ADMITNO = admit.ADMITNO
                }
              })
              // create a forms array
              form.ADMITKEYID = record.ADMITKEYID
              form.ASSIGNEDTO = record.EMPKEYID
              form.ASSIGNED_FNAME = record.EMP_FNAME
              form.ASSIGNED_MINITIAL = record.EMP_MINITIAL
              form.ASSIGNED_LNAME = record.EMP_LNAME
              form.VISITDATE = $filter('date')(record.VISITDATE, 'MM-dd-yyyy')
              form.VISITID = record.KEYID
              form.EPIKEYID = record.EPIKEYID
              $scope.forms.push(form)
              form.HASXML = false
              if (form.STATUS === 1) {
                form.LINK = CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + 'getOasisC2_2_20_1_xmlFile&KEYID=' + form.OBJKEYID
                form.HASXML = true
              }
            })
          } else {
            angular.forEach($scope.episodes, function (episode) {
              if (record.EPIKEYID === episode.KEYID) {
                record.EPISODENO = episode.EPISODENO
              }
            })
            angular.forEach(record.FORMS, function (form) {
              angular.forEach($scope.admissions, function (admit) {
                if (record.ADMITKEYID === admit.KEYID) {
                  form.ADMITNO = admit.ADMITNO
                }
              })
              // create a forms array
              form.ASSIGNEDTO = record.EMPKEYID
              form.ADMITKEYID = record.ADMITKEYID
              form.ASSIGNED_FNAME = record.EMP_FNAME
              form.ASSIGNED_MINITIAL = record.EMP_MINITIAL
              form.ASSIGNED_LNAME = record.EMP_LNAME
              form.VISITDATE = $filter('date')(record.VISITDATE, 'MM-dd-yyyy')
              form.EPISODENO = record.EPISODENO
              form.EPIKEYID = record.EPIKEYID
              form.VISITID = record.KEYID
              $scope.forms.push(form)

              form.HASXML = false
              if (form.STATUS === 1) {
                form.LINK = CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + 'getOasisC2_2_20_1_xmlFile&KEYID=' + form.OBJKEYID
                form.HASXML = true
              }
            })
          }
        })
        // get current admit
        $scope.currentAdmit = $scope.admissions[$scope.admissions.length - 1]
        // assign current admit as default for dropdown
        $scope.ADMIT = JSON.stringify($scope.admissions[$scope.admissions.length - 1])
        // generate list of forms for current admit

        // get current episode
        dataService.get('episode', {
          'ADMITKEYID': $scope.currentAdmit.KEYID
        }).then(function (data) {
          $scope.episodeList = data.data
          if (data.data.length < 1) {
            $scope.EPISODE = 'all'
            // show all episodes
            $scope.forms.forEach(function (form) {
              if (form.ADMITKEYID === $scope.currentAdmit.KEYID) {
                $scope.updatedFormsList.push(form)
              }
            })
          } else {
            $scope.currentEpisode = data.data[data.data.length - 1]
            $scope.EPISODE = JSON.stringify($scope.currentEpisode)
            $scope.forms.forEach(function (form) {
              if (form.EPIKEYID === $scope.currentEpisode.KEYID && form.ADMITKEYID === $scope.currentAdmit.KEYID) {
                $scope.updatedFormsList.push(form)
              }
            })
          }

          // if there are no forms in the current list, show error msg
          if ($scope.updatedFormsList.length < 1) {
            $scope.errorMsg = true
          } else {
            $scope.errorMsg = false
          }
        })
      })
      $scope.sortByVISITDATE()
    })
  }

  $scope.setVisitNum = function (index) {
    $scope.activeVisit = index
  }

  $scope.isShowing = function (index) {
    return $scope.activeVisit === index
  }

  $scope.viewForm = function (form) {
    if (form.TABLENAME === 'oasis_c2_2_20_1') { // OASIS FROMS
      // If form status is not 1 (so not submitted to qa), or not 3 (so not approved)

      if ((form.STATUS === 0) || (form.STATUS === 2) || (form.STATUS === 4)) { // go to oasis forms
        $scope.formdata = {
          FORMID: form.FORMID,
          VISITDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
          ASSIGNEDTO: form.ASSIGNEDTO,
          ADMITKEYID: form.ADMITKEYID,
          VISITID: form.VISITID,
          EPIKEYID: form.EPIKEYID
        }

        formService.set($scope.formdata)

        switch (form.FORMTYPE) {
          case 1:
            // $state.go('master')
            alert('unable to view this form')
            break
          case 2:
            $state.go('startOfCare')
            break
          case 3:
            $state.go('resumptionOfCare')
            break
          case 4:
            $state.go('recertificationAssessment')
            break
          case 5:
            $state.go('otherFollowUp')
            break
          case 6:
            $state.go('transferredNotDischarged')
            break
          case 7:
            $state.go('transferredDischarged')
            break
          case 8:
            $state.go('deathAtHome')
            break
          case 9:
            $state.go('discharge')
            break
          default:
            alert('An error occured. Please contact the staff.')
        }
      } else { // view OASIS FORMS
        // If form is complete, open it
        dataService.get('oasisC2_2_20_1', {
          KEYID: form.OBJKEYID
        }).then(function (data) {
          formService.set(data.data[0])

          // $state.go('viewOasisStartOfCare')
          //
          switch (form.FORMTYPE) {
            case 1:
              // $state.go('master')
              alert('Unable to view this form')
              break
            case 2:
              $state.go('viewOasisStartOfCare')
              break
            case 3:
              $state.go('viewOasisResumptionOfCare')
              break
            case 4:
              $state.go('viewOasisRecertification')
              break
            case 5:
              $state.go('viewOasisOtherFollowUp')
              break
            case 6:
              $state.go('viewOasisTransferNoDischarge')
              break
            case 7:
              $state.go('viewOasisTransferAndDischarge')
              break
            case 8:
              $state.go('viewOasisDeathAtHome')
              break
            case 9:
              $state.go('viewOasisDischarge')
              break
              // case 16:
              //   $state.go('viewSkilledNursingNote')
              //   break
            default:
              alert('An error occured. Please contact the staff.')
          }
        })
      }
    } else if (form.TABLENAME === 'skillednursingnote') { // go to Skilled nursing note
      // If form status is incomplete, in progress, or rejected
      if ((form.STATUS === 0) || (form.STATUS === 2) || (form.STATUS === 4)) { // go to skillednursingnote forms
        $scope.formdata = {
          FORMID: form.FORMID,
          VISITDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
          ASSIGNEDTO: form.ASSIGNEDTO,
          EPIKEYID: form.EPIKEYID,
          ADMITKEYID: form.ADMITKEYID,
          VISITID: form.VISITID
        }

        formService.set($scope.formdata)

        switch (form.FORMTYPE) {
          case 1:
            alert('unable to view this form')
            break
          case 16:
            $state.go('skilledNursingNote')
            break
          default:
            alert('An error occured. Please contact the staff.')
        }
      } else { // view SKILLED NURSING note
        // If form is complete, open it
        dataService.get('skilledNursingNote', {
          FORMID: form.FORMID
        }).then(function (data) {
          data.data[0].STATUS = form.STATUS
          data.data[0].EPIKEYID = form.EPIKEYID
          data.data[0].ADMITKEYID = form.ADMITKEYID
          data.data[0].VISITID = form.VISITID
          formService.set(data.data[0])
          switch (form.FORMTYPE) {
            case 1:
              alert('Unable to view this form')
              break
            case 16:
              $state.go('viewSkilledNursingNote')
              break
            default:
              alert('An error occured. Please contact the staff.')
          }
        })
      }
    } else if (form.TABLENAME === 'planofcare') { // go to POC
      // check to see if objkeyid exists
      if (form.OBJKEYID === null) { // go to poc worksheet version
        $scope.formdata = {
          FORMID: form.FORMID,
          STATUS: form.STATUS,
          OBJKEYID: form.OBJKEYID,
          VISITID: form.VISITID
        }

        formService.set($scope.formdata)

        switch (form.FORMTYPE) {
          case 1:
            alert('unable to view this form')
            break
          case 15:
            $state.go('planOfCare')
            break
          default:
            alert('An error occured. Please contact the staff.')
        }
      } else { // poc objectkeyid exists, go to poc form version
        $scope.formdata = {
          FORMID: form.FORMID,
          STATUS: form.STATUS,
          OBJKEYID: form.OBJKEYID,
          VISITID: form.VISITID
        }

        formService.set($scope.formdata)
        switch (form.FORMTYPE) {
          case 1:
            alert('Unable to view this form')
            break
          case 15:
            $state.go('viewPlanOfCare')
            break
          default:
            alert('An error occured. Please contact the staff.')
        }
        // })
      }
    } else if (form.NAME === 'Patient Consent') {
      // if form status is incomplete, in progress, or rejected, go to form
      if (form.STATUS === 0 || form.STATUS === 2 || form.STATUS === 4) {
        $scope.formdata = {
          FORMID: form.FORMID,
          STATUS: form.STATUS,
          VISITID: form.VISITID
        }

        if (form.OBJKEYID) {
          $scope.formdata.OBJKEYID = form.OBJKEYID
        }

        formService.set($scope.formdata)

        switch (form.FORMTYPE) {
          case 1:
            alert('unable to view this form')
            break
          case 11:
            $state.go('patientConsent')
            break
          default:
            alert('An error occured. Please contact the staff.')
        }
      } else { // if not, then view form
        // If form is complete, open it
        dataService.get('patientConsent', {
          FORMID: form.FORMID
        }).then(function (data) {
          data.data[0].STATUS = form.STATUS
          formService.set(data.data[0])
          switch (form.FORMTYPE) {
            case 1:
              alert('Unable to view this form')
              break
            case 11:
              $state.go('viewPatientConsent')
              break
            default:
              alert('An error occured. Please contact the staff.')
          }
        })
      }
    } else if (form.NAME === 'Physician Order') {
      // if form status is incomplete, in progress, or rejected, go to form
      if (form.STATUS === 0 || form.STATUS === 2 || form.STATUS === 4) {
        var modalInstance = $uibModal.open({
          controller: 'NewOrderController',
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: './views/modals/newOrder/newOrder.html',
          size: 'md',
          // resolve: {
          //   getPatientMedicationList: function() {
          //     return $scope.getPatientMedicationList
          //   }
          // }
          resolve: {
            form: form,
            formAdded: true
          }
        })
        modalInstance.result.then(function () {
          $scope.getRecords()
        })
      } else { // view order
        dataService.get('orders', {
          KEYID: form.OBJKEYID
        }).then(function (data) {
          var orderObj = data.data[0]
          orderObj.STATUS = form.STATUS
          var modalInstance = $uibModal.open({
            controller: 'ViewOrderController',
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './views/modals/viewOrder/viewOrder.html',
            size: 'md',
            resolve: {
              form: orderObj,
              formAdded: true
            }
          })
          modalInstance.result.then(function () {
            $scope.getRecords()
          })
        })
      }
    } else if (form.TABLENAME === 'routesheet') { // go to route sheet
      if (form.STATUS === 0) { // form is incomplete, route sheet record not saved
        dataService.get('visit', {
          KEYID: form.VISITID
        }).then(function (data) {
          $scope.formdata = {
            FORMID: form.FORMID,
            FORMDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            VISITDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            ASSIGNEDTO: form.ASSIGNEDTO,
            VISITID: form.VISITID,
            VISITTYPE: data.data[0].VISITTYPE,
            STATUS: form.STATUS,
            TIMEIN: data.data[0].TIMEIN,
            TIMEOUT: data.data[0].TIMEOUT
          }

          formService.set($scope.formdata)
          $state.go('routeSheet')
        })
      } else { // route sheet record saved
        dataService.get('routeSheet', {
          FORMID: form.FORMID
        }).then(function (data) {
          $scope.formdata = data.data[0]

          $scope.formdata.FORMDATE = $filter('date')(form.VISITDATE, 'yyyy/MM/dd')
          $scope.formdata.STATUS = form.STATUS
          $scope.formdata.ASSIGNEDTO = $scope.formdata.EMPLOYEE
          formService.set($scope.formdata)

          $state.go('routeSheet')
        })
      }
    } else if (form.TABLENAME === 'face') { // go to route sheet
      if (form.STATUS === 0) { // form is incomplete, route sheet record not saved
        dataService.get('visit', {
          KEYID: form.VISITID
        }).then(function (data) {
          // return false
          var formData = {
            FORMID: form.FORMID,
            FORMDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            VISITDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            ASSIGNEDTO: form.ASSIGNEDTO,
            VISITID: form.VISITID,
            VISITTYPE: data.data[0].VISITTYPE,
            STATUS: form.STATUS,
            TIMEIN: data.data[0].TIMEIN,
            TIMEOUT: data.data[0].TIMEOUT
          }

          formService.set(formData)
          $state.go('face')
        })
      } else { // route sheet record saved
        dataService.get('faceSheet', {
          FORMID: form.FORMID
        }).then(function (data) {
          var formData = data.data

          formData.FORMDATE = $filter('date')(formData.date_encounter, 'yyyy/MM/dd')
          formData.SIGDATE = $filter('date')(formData.date, 'yyyy/MM/dd')
          formData.formPrimaryId = formData.currentFormId
          formService.set(formData)

          $state.go('face')
        })
      }
    } else if (form.TABLENAME === 'missedvisitform') { // go to route sheet
      if (form.STATUS === 0) { // form is incomplete, route sheet record not saved
        dataService.get('visit', {
          KEYID: form.VISITID
        }).then(function (data) {
          var formData = {
            FORMID: form.FORMID,
            FORMDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            VISITDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            ASSIGNEDTO: form.ASSIGNEDTO,
            VISITID: form.VISITID,
            VISITTYPE: data.data[0].VISITTYPE,
            STATUS: form.STATUS,
            TIMEIN: data.data[0].TIMEIN,
            TIMEOUT: data.data[0].TIMEOUT
          }

          formService.set(formData)
          $state.go('missedvisit')
        })
      } else { // route sheet record saved
        dataService.get('missedvisitSheet', {
          FORMID: form.FORMID
        }).then(function (data) {
          var formData = data.data

          formData.VISITDATE = $filter('date')(formData.date_of_visit, 'yyyy/MM/dd')
          formData.EPISODEDATE = $filter('date')(formData.episode_date, 'yyyy/MM/dd')
          formData.MDDATE = $filter('date')(formData.date_md_notified, 'yyyy/MM/dd')
          formData.SIGDATE = $filter('date')(formData.date, 'yyyy/MM/dd')
          formService.set(formData)

          $state.go('missedvisit')
        })
      }
    } else if (form.TABLENAME === 'lvnform') { // go to route sheet
      if (form.STATUS === 0) { // form is incomplete, route sheet record not saved
        dataService.get('visit', {
          KEYID: form.VISITID
        }).then(function (data) {
          var formData = {
            FORMID: form.FORMID,
            FORMDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            VISITDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            ASSIGNEDTO: form.ASSIGNEDTO,
            VISITID: form.VISITID,
            VISITTYPE: data.data[0].VISITTYPE,
            STATUS: form.STATUS,
            TIMEIN: data.data[0].TIMEIN,
            TIMEOUT: data.data[0].TIMEOUT
          }

          formService.set(formData)
          $state.go('lvn')
        })
      } else { // route sheet record saved
        // alert('dfd')
        dataService.get('lvnSheet', {
          FORMID: form.FORMID
        }).then(function (data) {
          var formData = data.data

          // formData.VISITDATE = $filter('date')(formData.date_of_visit, 'yyyy/MM/dd')
          //      formData.EPISODEDATE = $filter('date')(formData.episode_date, 'yyyy/MM/dd')
          //    formData.MDDATE = $filter('date')(formData.date_md_notified, 'yyyy/MM/dd')
          //  formData.SIGDATE = $filter('date')(formData.date, 'yyyy/MM/dd')
          formService.set(formData)

          $state.go('lvn')
        })
      }
    } else if (form.TABLENAME === 'hhaform') { // go to route sheet
      if (form.STATUS === 0) { // form is incomplete, route sheet record not saved
        dataService.get('visit', {
          KEYID: form.VISITID
        }).then(function (data) {
          var formData = {
            FORMID: form.FORMID,
            FORMDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            VISITDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            ASSIGNEDTO: form.ASSIGNEDTO,
            VISITID: form.VISITID,
            VISITTYPE: data.data[0].VISITTYPE,
            STATUS: form.STATUS,
            TIMEIN: data.data[0].TIMEIN,
            TIMEOUT: data.data[0].TIMEOUT
          }

          formService.set(formData)
          $state.go('hha')
        })
      } else { // route sheet record saved
        // alert('dfd')
        dataService.get('hhaSheet', {
          FORMID: form.FORMID
        }).then(function (data) {
          var formData = data.data

          // formData.VISITDATE = $filter('date')(formData.date_of_visit, 'yyyy/MM/dd')
          //      formData.EPISODEDATE = $filter('date')(formData.episode_date, 'yyyy/MM/dd')
          //    formData.MDDATE = $filter('date')(formData.date_md_notified, 'yyyy/MM/dd')
          //  formData.SIGDATE = $filter('date')(formData.date, 'yyyy/MM/dd')
          formService.set(formData)

          $state.go('hha')
        })
      }
    } else if (form.TABLENAME === 'hhacareplan') { // go to route sheet
      if (form.STATUS === 0) { // form is incomplete, route sheet record not saved
        dataService.get('visit', {
          KEYID: form.VISITID
        }).then(function (data) {
          var formData = {
            FORMID: form.FORMID,
            FORMDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            VISITDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            ASSIGNEDTO: form.ASSIGNEDTO,
            VISITID: form.VISITID,
            VISITTYPE: data.data[0].VISITTYPE,
            STATUS: form.STATUS,
            TIMEIN: data.data[0].TIMEIN,
            TIMEOUT: data.data[0].TIMEOUT
          }

          formService.set(formData)
          $state.go('hhacareplan')
        })
      } else { // route sheet record saved
        // alert('dfd')
        dataService.get('hhacareSheet', {
          FORMID: form.FORMID
        }).then(function (data) {
          var formData = data.data

          // formData.VISITDATE = $filter('date')(formData.date_of_visit, 'yyyy/MM/dd')
          //      formData.EPISODEDATE = $filter('date')(formData.episode_date, 'yyyy/MM/dd')
          //    formData.MDDATE = $filter('date')(formData.date_md_notified, 'yyyy/MM/dd')
          //  formData.SIGDATE = $filter('date')(formData.date, 'yyyy/MM/dd')
          formService.set(formData)

          $state.go('hhacareplan')
        })
      }
    } else if (form.TABLENAME === 'woundform') { // go to route sheet
      if (form.STATUS === 0) { // form is incomplete, route sheet record not saved
        dataService.get('visit', {
          KEYID: form.VISITID
        }).then(function (data) {
          var formData = {
            FORMID: form.FORMID,
            FORMDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            VISITDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            ASSIGNEDTO: form.ASSIGNEDTO,
            VISITID: form.VISITID,
            VISITTYPE: data.data[0].VISITTYPE,
            STATUS: form.STATUS,
            TIMEIN: data.data[0].TIMEIN,
            TIMEOUT: data.data[0].TIMEOUT
          }

          formService.set(formData)
          $state.go('wound')
        })
      } else { // route sheet record saved
        // alert('dfd')
        dataService.get('woundSheet', {
          FORMID: form.FORMID
        }).then(function (data) {
          var formData = data.data
          formData.STATUS = data.data.woundData.STATUS

          // formData.VISITDATE = $filter('date')(formData.date_of_visit, 'yyyy/MM/dd')
          //      formData.EPISODEDATE = $filter('date')(formData.episode_date, 'yyyy/MM/dd')
          //    formData.MDDATE = $filter('date')(formData.date_md_notified, 'yyyy/MM/dd')
          //  formData.SIGDATE = $filter('date')(formData.date, 'yyyy/MM/dd')
          formService.set(formData)

          $state.go('wound')
        })
      }
    } else if (form.TABLENAME === 'ptvisit') { // go to route sheet
      if (form.STATUS === 0) { // form is incomplete, route sheet record not saved
        dataService.get('visit', {
          KEYID: form.VISITID
        }).then(function (data) {
          var formData = {
            FORMID: form.FORMID,
            FORMDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            VISITDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            ASSIGNEDTO: form.ASSIGNEDTO,
            VISITID: form.VISITID,
            VISITTYPE: data.data[0].VISITTYPE,
            STATUS: form.STATUS,
            TIMEIN: data.data[0].TIMEIN,
            TIMEOUT: data.data[0].TIMEOUT
          }

          formService.set(formData)
          $state.go('ptvisit')
        })
      } else { // route sheet record saved
        // alert('dfd')
        dataService.get('ptvisitSheet', {
          FORMID: form.FORMID
        }).then(function (data) {
          var formData = data.data
          formData.STATUS = data.data.STATUS

          formService.set(formData)

          $state.go('ptvisit')
        })
      }
    } else if (form.TABLENAME === 'pteval') { // go to route sheet
      if (form.STATUS === 0) { // form is incomplete, route sheet record not saved
        dataService.get('visit', {
          KEYID: form.VISITID
        }).then(function (data) {
          var formData = {
            FORMID: form.FORMID,
            FORMDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            VISITDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            ASSIGNEDTO: form.ASSIGNEDTO,
            VISITID: form.VISITID,
            VISITTYPE: data.data[0].VISITTYPE,
            STATUS: form.STATUS,
            TIMEIN: data.data[0].TIMEIN,
            TIMEOUT: data.data[0].TIMEOUT
          }

          formService.set(formData)
          $state.go('pteval')
        })
      } else { // route sheet record saved
        // alert('dfd')
        dataService.get('ptevalSheet', {
          FORMID: form.FORMID
        }).then(function (data) {
          var formData = data.data
          formData.STATUS = data.data.STATUS

          formService.set(formData)

          $state.go('pteval')
        })
      }
    } else if (form.TABLENAME === 'ptdischarge') { // go to route sheet
      if (form.STATUS === 0) { // form is incomplete, route sheet record not saved
        dataService.get('visit', {
          KEYID: form.VISITID
        }).then(function (data) {
          var formData = {
            FORMID: form.FORMID,
            FORMDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            VISITDATE: $filter('date')(form.VISITDATE, 'yyyy/MM/dd'),
            ASSIGNEDTO: form.ASSIGNEDTO,
            VISITID: form.VISITID,
            VISITTYPE: data.data[0].VISITTYPE,
            STATUS: form.STATUS,
            TIMEIN: data.data[0].TIMEIN,
            TIMEOUT: data.data[0].TIMEOUT
          }

          formService.set(formData)
          $state.go('ptdischarge')
        })
      } else { // route sheet record saved
        // alert('dfd')
        dataService.get('ptdischargeSheet', {
          FORMID: form.FORMID
        }).then(function (data) {
          var formData = data.data
          formData.STATUS = data.data.STATUS

          formService.set(formData)

          $state.go('ptdischarge')
        })
      }
    } else { // all other forms
      console.log('all other forms')
    }
  }

  $scope.add = function () {
    event.preventDefault()
    var modalInstance = $uibModal.open({
      controller: 'NewFormController',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/newForm/newForm.html',
      size: 'md'
    })
    modalInstance.result.then(function (data) {
      // after forms have been selected, add forms to form list
      // $scope.forms = $scope.forms.concat(data)
      $scope.getRecords()
    })
  }

  $scope.sortByVISITDATE = function () {
    $scope.sortFORMNAME = true
    $scope.sortSTATUS = true
    $scope.sortVISITDATE = !$scope.sortVISITDATE

    if ($scope.sortVISITDATE === false) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.VISITDATE.toLowerCase()
        var dateB = b.VISITDATE.toLowerCase()

        if (dateA < dateB) {
          return -1
        }
        if (dateA > dateB) {
          return 1
        }
        return 0
      })
    } else if ($scope.sortVISITDATE === true) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.VISITDATE.toLowerCase()
        var dateB = b.VISITDATE.toLowerCase()

        if (dateA > dateB) {
          return -1
        }
        if (dateA < dateB) {
          return 1
        }
        return 0
      })
    }
  }

  $scope.sortByFORMNAME = function () {
    $scope.sortVISITDATE = true
    $scope.sortSTATUS = true
    $scope.sortFORMNAME = !$scope.sortFORMNAME

    if ($scope.sortFORMNAME === false) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.NAME.toLowerCase()
        var dateB = b.NAME.toLowerCase()

        if (dateA < dateB) {
          return -1
        }
        if (dateA > dateB) {
          return 1
        }
        return 0
      })
    } else if ($scope.sortFORMNAME === true) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.NAME.toLowerCase()
        var dateB = b.NAME.toLowerCase()

        if (dateA > dateB) {
          return -1
        }
        if (dateA < dateB) {
          return 1
        }
        return 0
      })
    }
  }

  $scope.sortBySTATUS = function () {
    $scope.sortVISITDATE = true
    $scope.sortFORMNAME = true
    $scope.sortSTATUS = !$scope.sortSTATUS

    if ($scope.sortSTATUS === false) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.STATUS.toLowerCase()
        var dateB = b.STATUS.toLowerCase()

        if (dateA < dateB) {
          return -1
        }
        if (dateA > dateB) {
          return 1
        }
        return 0
      })
    } else if ($scope.sortSTATUS === true) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.STATUS.toLowerCase()
        var dateB = b.STATUS.toLowerCase()

        if (dateA > dateB) {
          return -1
        }
        if (dateA < dateB) {
          return 1
        }
        return 0
      })
    }
  }

  // fiter by admit
  $scope.changeAdmit = function () {
    $scope.updatedFormsList = []

    // if admit is selected
    if ($scope.ADMIT !== 'all') {
      var selectedAdmit = JSON.parse($scope.ADMIT)
      dataService.get('episode', {
        'ADMITKEYID': selectedAdmit.KEYID
      }).then(function (data) {
        if (data.data.length < 1) {
          $scope.episodeList = null
        } else {
          $scope.episodeList = data.data
          $scope.EPISODE = 'all'
        }

        if ($scope.EPISODE === 'all') { // if episode is 'all'
          $scope.forms.forEach(function (form) {
            if (form.ADMITKEYID === selectedAdmit.KEYID) {
              $scope.updatedFormsList.push(form)
            }
          })
          if ($scope.updatedFormsList.length < 1) {
            $scope.errorMsg = true
          } else {
            $scope.errorMsg = false
          }
        } else { // if episode is selected
          var selectedEpisode = JSON.parse($scope.EPISODE)
          $scope.forms.forEach(function (form) {
            if ((form.EPIKEYID === selectedEpisode.KEYID) && (form.ADMITKEYID === selectedAdmit.KEYID)) {
              $scope.updatedFormsList.push(form)
            }
          })
          if ($scope.updatedFormsList.length < 1) {
            $scope.errorMsg = true
          } else {
            $scope.errorMsg = false
          }
        }
      })
    } else if ($scope.EPISODE !== 'all' && $scope.ADMIT !== 'all') { // if episode is selected
      var selectedEpisode = JSON.parse($scope.EPISODE)
      $scope.forms.forEach(function (form) {
        if (form.EPIKEYID === selectedEpisode.KEYID) {
          $scope.updatedFormsList.push(form)
        }
      })
      if ($scope.updatedFormsList.length < 1) {
        $scope.errorMsg = true
      } else {
        $scope.errorMsg = false
      }
    } else { // else show all
      $scope.updatedFormsList = $scope.forms
      $scope.episodeList = null
      $scope.EPISODE = 'all'
      if ($scope.updatedFormsList.length < 1) {
        $scope.errorMsg = true
      } else {
        $scope.errorMsg = false
      }
    }
  }

  $scope.changeEpisode = function () {
    $scope.updatedFormsList = []
    // if episode is selected
    if ($scope.EPISODE !== 'all') {
      var selectedEpisode = JSON.parse($scope.EPISODE)
      var selectedAdmit
      if ($scope.ADMIT === 'all') { // if admit is 'all'
        $scope.forms.forEach(function (form) {
          if (form.EPIKEYID === selectedEpisode.KEYID) {
            $scope.updatedFormsList.push(form)
          }
        })
        if ($scope.updatedFormsList.length < 1) {
          $scope.errorMsg = true
        } else {
          $scope.errorMsg = false
        }
      } else { // if admit is selected
        selectedAdmit = JSON.parse($scope.ADMIT)
        $scope.forms.forEach(function (form) {
          if ((form.EPIKEYID === selectedEpisode.KEYID) && (form.ADMITKEYID === selectedAdmit.KEYID)) {
            $scope.updatedFormsList.push(form)
          }
        })
        if ($scope.updatedFormsList.length < 1) {
          $scope.errorMsg = true
        } else {
          $scope.errorMsg = false
        }
      }
    } else if ($scope.ADMIT !== 'all') { // if admit is selected
      selectedAdmit = JSON.parse($scope.ADMIT)
      $scope.forms.forEach(function (form) {
        if (form.ADMITKEYID === selectedAdmit.KEYID) {
          $scope.updatedFormsList.push(form)
        }
      })
      if ($scope.updatedFormsList.length < 1) {
        $scope.errorMsg = true
      } else {
        $scope.errorMsg = false
      }
    } else { // else show all
      $scope.updatedFormsList = $scope.forms
      $scope.errorMsg = false
    }
  }

  dataService.getAll('visitType').then(function (data) {
    $scope.visitTypes = data.data
  })

  dataService.getAll('employee').then(function (data) {
    $scope.employees = data.data
  })
  dataService.get('visit', {
    'PATKEYID': $scope.patient.KEYID
  }).then(function (data) {
    $scope.visits = data.data
  })

  dataService.get('episode', {
    'PATKEYID': $scope.patient.KEYID
  }).then(function (data) {
    $scope.episodes = data.data
    // $scope.episodeList=data.data
  })

  $scope.getRecords()

  // sort by visit date
  $scope.sortVISITDATE = true

  // sort by form name
  $scope.sortFORMNAME = true

  // sort by status
  $scope.sortSTATUS = true
})
