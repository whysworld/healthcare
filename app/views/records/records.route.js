app.config(function($stateProvider) {
  $stateProvider.state('records', {
    url: '/records',
    params: {
      patient: null,
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/records/records.html',
        controller: 'RecordsController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
