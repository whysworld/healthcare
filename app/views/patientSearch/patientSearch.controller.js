app.controller('SearchPatientController', function($scope, $filter, patientService, patientInfo, $window, $state, dataService, $http, navService) {

  // Set the patientService to the currently selected patient
  $scope.setPatient = function (patient) {
    patientService.set(patient);
    $scope.patient = patient;
  };

  // Initialise the patients list
  $scope.activePatientList = [{}];

  // TODO: replace with patients list (No more cycling into referral sources)
  dataService.list('patient').then(function(data) {
    // All the patients available in the db
    var patients = data.data;

    // Date of birth as date
    patients.forEach(function(patient) {
      patient.DOB = $filter('date')(patient.DOB, 'MM-dd-yyyy');

      switch(patient.PATSTATUS) {
        case "A":
        patient.PATSTATUSDESC = 'Admitted';
        break;
        case "P":
        patient.PATSTATUSDESC = 'Pending';
        break;
        case "D":
        patient.PATSTATUSDESC = 'Discharged';
        break;
        case "N":
        patient.PATSTATUSDESC = 'Non-Admit';
        break;
        case "T":
        patient.PATSTATUSDESC = 'Transfer';
        break;
      }
    });

    sortPatients(patients);
    $scope.updatePatientList('ADMITTED');

    // dataService.getAll('referralSource').then(function (data) {
    //   var referralSources = data.data;
    //   dataService.getAll('doctor').then(function (data) {
    //     var doctors = data.data;
    //
    //     patients.forEach(function (patient) {
    //       dataService.get('admission', {KEYID: patient.LSTADMIT}).then(function(data) {
    //         var admission = data.data[0];
    //
    //         if (admission !== undefined) {
    //           patient.REFSOURCEKEY = admission.REFERRSRC;
    //           patient.REFMDKEYID = admission.REFERRPHYSICIAN;
    //           if (admission.REFERRDATE) {
    //             patient.REFDATE = $filter('date')(new Date(admission.REFERRDATE), 'MM-dd-yyyy');
    //           }
    //
    //           referralSources.forEach(function (refSource) {
    //             if (patient.REFSOURCEKEY === refSource.KEYID) {
    //               patient.REFSOURCE = refSource.NAME;
    //             }
    //           });
    //           doctors.forEach(function (doctor) {
    //             if (patient.REFMDKEYID === doctor.KEYID) {
    //               patient.REFMD = doctor.FNAME + ' ' + doctor.LNAME;
    //             }
    //           });
    //           //get status
    //         }
    //
    //       console.log("patients after referralSource cycle", JSON.stringify(patients[patients.length-10]));
    //       });
    //     });
    //     sortPatients(patients);
    //     $scope.updatePatientList('ADMITTED');
    //   });
    // });

  });

  var allPatients = [];
  var admittedPatients = [];
  var pendingPatients = [];
  var dischargedPatients = [];
  var nonAdmitPatients = [];
  var transferPatients = [];
  function sortPatients(patients) {
    allPatients = patients;
    $scope.activePatientList = allPatients;

    for (var i = 0; i < patients.length; i++) {
      switch(patients[i].PATSTATUS) {
        case "A":
        admittedPatients.push(patients[i]);
        break;
        case "P":
        pendingPatients.push(patients[i]);
        break;
        case "D":
        dischargedPatients.push(patients[i]);
        break;
        case "N":
        nonAdmitPatients.push(patients[i]);
        break;
        case "T":
        transferPatients.push(patients[i]);
        break;
      }
    }
  };

  $scope.patientType = 'ADMITTED';

  $scope.updatePatientList = function(patientType) {
    switch(patientType) {
      case "ALL":
      $scope.activePatientList = allPatients;
      break;
      case "ADMITTED":
      $scope.activePatientList = admittedPatients;
      break;
      case "PENDING":
      $scope.activePatientList = pendingPatients;
      break;
      case "DISCHARGED":
      $scope.activePatientList = dischargedPatients;
      break;
      case "NON-ADMIT":
      $scope.activePatientList = nonAdmitPatients;
      break;
      case "TRANSFER":
      $scope.activePatientList = transferPatients;
      break;
    }
  };

  $scope.showPatientDetail=function() {
    $scope.expandpatientnav =! $scope.expandpatientnav;
  };

  // $scope.rightMenuOpen = false;
  // $scope.showPatientDetail = function(patient, event) {
  //   if($scope.activePatient) {
  //     delete $scope.activePatient.selected;
  //     $('.right-menu').animate({
  //       left: '-210px'
  //     }, 200);
  //     $('#patient-search').animate({
  //       'margin-left': '180'
  //     }, 200);
  //   }
  //   $scope.rightMenuOpen = true;
  //   $scope.activePatient = patient;
  //   $scope.activePatient.selected = true;
  //   event.stopPropagation();
  //   $scope.selectedPatient = patient;
  //   $('.right-menu').animate({
  //     left: '180'
  //   }, 200);
  //   $('#patient-search').animate({
  //     'margin-left': '390'
  //   }, 200);
  // };
  // $scope.closePatientDetail = function() {
  //   delete $scope.activePatient.selected;
  //   $('.right-menu').animate({
  //     left: '-150px'
  //   }, 200);
  //   $('#patient-search').animate({
  //     'margin-left': '60'
  //   }, 200);
  // };

  $scope.noHighlight = function(event) {
    if($scope.rightMenuOpen) {
      delete $scope.activePatient.selected;
    }
    event.stopPropagation();
  };

  // $(window).click(function() {
  //   if(rightMenuOpen) {
  //     $scope.$apply(function() {
  //       $scope.closePatientDetail();
  //     });
  //   }
  // });

  $('.right-menu').click(function(event){
    event.stopPropagation();
  });

  $scope.pendingStatus = function(selectedPatient) {
    if (selectedPatient === 'PENDING') {
      return true;
    }
  };

  $scope.search = function(patient) {
    if (!$scope.searchText || (patient.FNAME && patient.FNAME.toLowerCase().indexOf($scope.searchText.toLowerCase()) != -1) || (patient.LNAME && patient.LNAME.toLowerCase().indexOf($scope.searchText.toLowerCase()) != -1) ){
      return true;
    } else {
      return false;
    }
  };

});
