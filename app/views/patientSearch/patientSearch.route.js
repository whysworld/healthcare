app.config(function($stateProvider) {
  $stateProvider.state('patientSearch', {
    url: '/patientSearch',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html',
      },
      'pageContent': {
        templateUrl: 'views/patientSearch/patientSearch.html',
        controller: 'SearchPatientController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
