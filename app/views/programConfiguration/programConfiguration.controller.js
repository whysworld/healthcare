app.controller('ProgramConfigurationController', function($scope, $state, dataService, agencyService) {

  $scope.noEdit = true;

  $scope.cancel=function () {
    $scope.getAgency();
  }

  $scope.getAgency = function() {
    dataService.get("agencyConfig", {}).then(function(data) {
      $scope.agency = data.data;
      console.log($scope.agency);

      $scope.NAME=$scope.agency.NAME;
      $scope.SHORTNAME=$scope.agency.SHORTNAME;
      $scope.ADDRESS1=$scope.agency.ADDRESS1;
      $scope.CITY=$scope.agency.CITY;
      $scope.STATE=$scope.agency.STATE;
      $scope.ZIP=$scope.agency.ZIP;
      $scope.PHONE=$scope.agency.PHONE;
      $scope.FAX=$scope.agency.FAX;
      $scope.EMAIL=$scope.agency.EMAIL;
      $scope.CONTACT=$scope.agency.CONTACT;
      $scope.NOTES=$scope.agency.NOTES;
      $scope.FEDTAXID=$scope.agency.FEDTAXID;
      $scope.PROVIDERNUM=$scope.agency.PROVIDERNUM;
      $scope.NPI=$scope.agency.NPI;
      $scope.STATEAGENCY=$scope.agency.STATEAGENCY;
      $scope.BRANCH=$scope.agency.BRANCH;
      $scope.AGENCYCHARGESSN=$scope.agency.AGENCYCHARGESSN;
      $scope.AGENCYCHARGESMSW=$scope.agency.AGENCYCHARGESMSW;
      $scope.AGENCYCHARGESCHHA=$scope.agency.AGENCYCHARGESCHHA;
      $scope.MILEAGERATE=$scope.agency.MILEAGERATE;
      $scope.OPENDAYSANDHOURS=$scope.agency.OPENDAYSANDHOURS;
      $scope.ADMINISTRATOR=$scope.agency.ADMINISTRATOR;
    });
  };

  $scope.getAgency();

  $scope.saveAgency = function() {
    var agency={
      KEYID: $scope.agency.KEYID,
      SHORTNAME: $scope.SHORTNAME,
      NAME: $scope.NAME,
      ADDRESS1: $scope.ADDRESS1,
      CITY: $scope.CITY,
      STATE: $scope.STATE,
      ZIP: $scope.ZIP,
      PHONE: $scope.PHONE,
      FAX: $scope.FAX,
      EMAIL: $scope.EMAIL,
      FEDTAXID: $scope.FEDTAXID,
      PROVIDERNUM: $scope.PROVIDERNUM,
      NPI: $scope.NPI,
      STATEAGENCY:$scope.STATEAGENCY,
      CONTACT: $scope.CONTACT,
      NOTES: $scope.NOTES,
      BRANCH: $scope.BRANCH,
      AGENCYCHARGESSN: $scope.AGENCYCHARGESSN,
      AGENCYCHARGESMSW: $scope.AGENCYCHARGESMSW,
      AGENCYCHARGESCHHA: $scope.AGENCYCHARGESCHHA,
      MILEAGERATE: $scope.MILEAGERATE,
      OPENDAYSANDHOURS: $scope.OPENDAYSANDHOURS,
      ADMINISTRATOR: $scope.ADMINISTRATOR,
    };
    console.log('agencyChanges', agency);
    dataService.edit('config', agency).then(function (response) {
      console.log('edit agency', response);
      if (response.status==='success'){
        $scope.enableEdit=true;
        agencyService.set(agency);
        $state.go('homeDashboard');
      }
    });
  };

  // $scope.agency = {
  //   name: "ABC Agency",
  //   address: "1223 Front Street",
  //   city: "San Francisco",
  //   state: "CA",
  //   zip: "92212",
  //   email: "info@agencyname1.com",
  //   phone: "1-415-222-3435",
  //   fax: "1-415-222-3436",
  //   notes: "Somes notes",
  //   federalTaxID: "111-1111",
  //   providerNum: "29384",
  //   NPI: "none",
  //   stateAgencyID: "0293",
  //   branchID: "2-33",
  //   contact: "Bobby"
  // };
});
