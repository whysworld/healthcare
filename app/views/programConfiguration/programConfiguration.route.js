app.config(function($stateProvider) {
  $stateProvider.state('programConfiguration', {
    url: '/programConfiguration',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/programConfiguration/programConfiguration.html',
        controller: 'ProgramConfigurationController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
