app.controller('PatientProfileController', function($scope,$state, $filter, $uibModal, patientService, dataService, agencyService) {



  $scope.printSection = function() {
    var referral = document.getElementById('referral').textContent;
    var demographic = document.getElementById('demographic').textContent;
    var contact = document.getElementById('contact').textContent;
    var insurance = document.getElementById('insurance').textContent;
    var physicians = document.getElementById('physicians').textContent;
    var diagnosis = document.getElementById('diagnosis').textContent;
    var episodes = document.getElementById('episodes').textContent;
    var notes = document.getElementById('notes').textContent;

    var docDefinition = {
      content: [
        {
          table: {
            widths: ['*', '*', '*'],
            body: [
              [{text: 'Maya Healthcare Inc.', style: 'header', colSpan: 3, alignment: 'center'}, {}, {}],
    					[
                {
                  text: $scope.agency.NAME + '\n' + $scope.agency.ADDRESS1 + ', ' + $scope.agency.CITY + ', ' + $scope.agency.STATE + ', ' + $scope.agency.ZIP + '\n' + 'Phone: ' + $scope.agency.PHONE,
                  style: 'agency',
                  colSpan: 2
                },
                {},
                {
                  // noWrap: true,
                  text: [
                    {text:'PATIENT PROFILE', style: 'formtitle'},
                    '\n',
                    {text:$scope.patient.FNAME + ' ' + $scope.patient.LNAME, alignment: 'center', fontSize:12},
                  ]
                }
              ],
    					[
                {
                  style: 'small' ,
                  text: [
                    {text: 'REFERRAL', style:'subsection'},
                    referral,
                    '\n',
                    {text: 'DEMOGRAHPIC', style:'subsection'},
                    demographic,
                    {text: 'CONTACT', style:'subsection'},
                    contact,
                  ]
                },
                {
                  style: 'small' ,
                  text: [
                    {text: 'INSURANCE', style:'subsection'},
                    insurance,
                    {text: 'PHYSICIANS', style:'subsection'},
                    physicians,
                    {text: 'DIAGONOSIS', style:'subsection'},
                    diagnosis,
                  ]
                },
                {
                  style: 'small' ,
                  text: [
                    {text: 'EPISODES', style:'subsection'},
                    episodes,
                    '\n',
                    {text: 'NOTES', style:'subsection'},
                    notes,
                  ]
                }
              ]
            ]
          }
        },
      ],
      footer: {
          columns: [
            {
              text: 'PRINTED:' + $scope.printDate + '\n' + 'Confidential Information\n Maya Healthcare, Inc. Copyright 2017',
              style: 'footer',
            },
          ]
      },
      styles: {
        header: {
          fontSize: 16,
          bold: true
        },
        subsection: {
          fontSize:10,
          bold: true
        },
        formtitle: {
          fontSize: 14,
          alignment: 'center',
          bold: true
        },
        agency: {
          fontSize:10,
        },
        small: {
          fontSize: 9
        },
        footer: {
          fontSize: 8,
          alignment: 'center'
        }
      }
    };
    pdfMake.createPdf(docDefinition).download('patientProfile'+$scope.patient.FNAME+$scope.patient.LNAME+'.pdf');
  };

  // $scope.nonEditMode = true;
  //
  // $scope.enableEditMode = function(section) {
  //   console.log(section);
  //   event.preventDefault();
  //   $scope.nonEditMode = false;
  //   $scope['edit' + section] = true;
  // };
  // $scope.disableEditMode = function(section) {
  //   $scope.nonEditMode = true;
  //   $scope['edit' + section] = false;
  // };


  $scope.patient = patientService.get();
  console.log('$scope.patient', $scope.patient);

  $scope.agency = agencyService.get();
  $scope.agency.PHONE=formatAgencyPhone($scope.agency.PHONE);
  console.log('$scope.agency', $scope.agency);

  //format agency phone for pdf printing
  function formatAgencyPhone(tel) {
    if (!tel) { return ''; }
    var value = tel.toString().trim().replace(/^\+/, '');
    if (value.match(/[^0-9]/)) {
        return tel;
    }
    var country, city, number;
    switch (value.length) {
        case 10: // +1PPP####### -> C (PPP) ###-####
            city = value.slice(0, 3);
            number = value.slice(3);
            break;
        default:
            return tel;
    }
    number = number.slice(0, 3) + '-' + number.slice(3);
    return ("(" + city + ") " + number).trim();
  }

  $scope.printDate=new Date();

  dataService.getAll('language').then(function(data) {
    $scope.languages = data.data;
    convertLANGUAGEcode();
  });
  dataService.getAll('religion').then(function(data) {
    $scope.religions = data.data;
    convertRELIGIONcode();
  });
  dataService.getAll('insurance').then(function(data) {
    $scope.insurances = data.data;
    convertINSCOcode();

  });
  dataService.getAll('employee').then(function(data) {
    $scope.employees = data.data;
  });
  dataService.getAll('doctor').then(function(data) {
    $scope.doctors = data.data;
  });
  dataService.getAll('referralSource').then(function(data) {
    $scope.referralSources = data.data;
  });
  dataService.getAll('ICD10Code').then(function(data) {
    $scope.ICDcodes = data.data;
  });

  dataService.get('form', {ADMITKEYID: $scope.patient.LSTADMIT}).then(function (data) {
    var forms=data.data
    if (forms.length>0) {
      $scope.transferFormArr=[]
      forms.forEach(function(form) {
        if (form.FORMTYPE===6) {
          $scope.transferFormArr.push(form);
        }
      })
      if ($scope.transferFormArr.length>0){
        $scope.patientTransferDate=$scope.transferFormArr[$scope.transferFormArr.length-1].FORMDATE;
        $scope.patientTransferDate= $filter("date")($scope.patientTransferDate, 'MM-dd-yyyy');
        console.log($scope.patientTransferDate);
      }
    }
  })


  dataService.get('admission', {'PATKEYID': $scope.patient.KEYID}).then(function(data) {
    $scope.admission=data.data[data.data.length-1];
    console.log('$scope.admission', $scope.admission);
    $scope.admissions=data.data;
    $scope.totalAdmits = data.data.length;
    convertREFRECEIVEDcode();
    convertREFERRSRCcode();
    convertREFERRPHYSICIANcode();
    convertADMITSRCEcode();
    convertACUITYcode();
    convertINS2COcode();
    if ($scope.admission.REFERRDATE!==null) {
      $scope.admission.REFERRDATE = new Date($scope.admission.REFERRDATE);
      $scope.admission.REFERRDATE = $filter("date")($scope.admission.REFERRDATE, 'MM-dd-yyyy');
    }
    $scope.admission.DIAGONSET = $filter("date")($scope.admission.DIAGONSET, 'MM-dd-yyyy');
    $scope.admission.DIAGONSET2 = $filter("date")($scope.admission.DIAGONSET2, 'MM-dd-yyyy');
    $scope.admission.DIAGONSET3 = $filter("date")($scope.admission.DIAGONSET3, 'MM-dd-yyyy');
    $scope.admission.DIAGONSET4 = $filter("date")($scope.admission.DIAGONSET4, 'MM-dd-yyyy');
    $scope.admission.DIAGONSET5 = $filter("date")($scope.admission.DIAGONSET5, 'MM-dd-yyyy');
    $scope.admission.DIAGONSET6 = $filter("date")($scope.admission.DIAGONSET6, 'MM-dd-yyyy');

    $scope.admission.DCDATE = $filter("date")($scope.admission.DCDATE, 'MM-dd-yyyy');

    dataService.get('episode',{'ADMITKEYID': $scope.admission.KEYID}).then(function(data){
      if(data.data.length>0) {
        $scope.episodes = data.data;
        $scope.totalEpi = $scope.episodes.length;
        var currentEpi = data.data.slice(-1).pop();
        currentEpi.FROMDATE= $filter("date")(currentEpi.FROMDATE, 'MM-dd-yyyy');
        currentEpi.THRUDATE= $filter("date")(currentEpi.THRUDATE, 'MM-dd-yyyy');
        currentEpi.THRUDATE= $filter("date")(currentEpi.THRUDATE, 'MM-dd-yyyy');
        $scope.currentEpi=currentEpi;
      } else {
        $scope.episodes = data.data;
      }
    });
  });


  $scope.editPatStatus = function(event) {
    //tweak depending on PATSTATUS and what options we want avaiable
    // console.log($scope.PATSTATUS)
    // if ($scope.PATSTATUS === 'Pending') {
    //   $scope.statusOptions = ['P', 'N']
    // } else if ($scope.PATSTATUS === 'Active') {
    //   $scope.statusOptions = ['D']
    // }
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'PatientStatusController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/patientStatus/patientStatus.html',
        size: 'md',
        resolve: {
          patient: $scope.patient,
          admission: $scope.admission,
        }
      });
    modalInstance.result.then(function (updatedPatient) {
      //check if PATSTATUS has changed
      if (updatedPatient !== undefined) {
        $scope.patient = updatedPatient;
        console.log($scope.patient)
        convertSTATUScode();
      }
    });
  };


  $scope.enableEditMode = function(section, event) {
    console.log(section);
    event.preventDefault();

    // $scope.nonEditMode = false;
    // $scope['edit' + section] = true;

    var modalInstance = $uibModal.open({
        controller: 'EditIntakeController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/intake/editIntake.html',
        size: 'lg',
        resolve: {
          section: function() {
            return section;
          },
          patient: $scope.patient,
          languages: function() {
            return $scope.languages;
          },
          religions: function() {
            return $scope.religions;
          },
          insurances: function() {
            return $scope.insurances;
          },
          employees: function() {
            return $scope.employees;
          },
					referralSources: function() {
						return $scope.referralSources;
					},
          doctors: function() {
            return $scope.doctors;
          },
          ICDcodes: function() {
            return $scope.ICDcodes;
          },
          admission: $scope.admission,
        }
      });

			modalInstance.result.then(function (opts) {
			  $scope.patient = opts.updatedPatient;
        $scope.patient.DOB = $filter("date")($scope.patient.DOB, 'MM-dd-yyyy');

        // console.log($scope.patient)
        $scope.admission = opts.updatedAdmission;
        $scope.admission.DIAGONSET = $filter("date")($scope.admission.DIAGONSET, 'MM-dd-yyyy');

        console.log(opts)
        convertMARITALcode();
        convertRELIGIONcode();
        convertLANGUAGEcode();
        convertINSCOcode();
        convertM0140code();
        convertLOCTYPEcode();
        convertINS2COcode();
        convertREFRECEIVEDcode();
        convertREFERRSRCcode();
        convertREFERRPHYSICIANcode();
        convertADMITSRCEcode();
        convertACUITYcode();
        convertLOCTYPE2code();
        dataService.getAll('doctor').then(function(data) {
          $scope.doctors = data.data;
        });


				// console.log("updatedPatient", data);
			});
  };
//

convertMARITALcode();
function convertMARITALcode() {
	switch ($scope.patient.MARITAL) {
		case 1:
      $scope.MARITALDESC = 'Unknown';
			break;
		case 2:
      $scope.MARITALDESC = 'Single';
			break;
    case 3:
      $scope.MARITALDESC = 'Married';
      break;
    case 4:
      $scope.MARITALDESC = 'Separated';
      break;
    case 5:
      $scope.MARITALDESC = 'Divorced';
      break;
    case 6:
      $scope.MARITALDESC = 'Widowed';
      break;
	}
}

convertSTATUScode();
function convertSTATUScode() {
  console.log($scope.patient)
	switch ($scope.patient.PATSTATUS) {
		case 'P':
      $scope.PATSTATUS = 'Pending';
			break;
		case 'A':
      $scope.PATSTATUS = 'Admitted';
			break;
    case 'N':
      $scope.PATSTATUS = 'Non-Admit';
      break;
    case 'D':
      $scope.PATSTATUS = 'Discharge';
      break;
    case 'T':
      $scope.PATSTATUS = 'Transferred';
      break;
	}
}

convertM0140code();
function convertM0140code() {
	switch ($scope.patient.M0140) {
		case '1':
      $scope.M0140DESC = 'American Indian or Alaska Native';
			break;
		case '2':
      $scope.M0140DESC = 'Asian';
			break;
    case '3':
      $scope.M0140DESC = 'Black or African-American';
      break;
    case '4':
      $scope.M0140DESC = 'Hispanic or Latino';
      break;
    case '5':
      $scope.M0140DESC = 'Native Hawaiian or Pacific Islander';
      break;
    case '6':
      $scope.M0140DESC = 'White';
      break;
	}
}

convertLOCTYPEcode();
function convertLOCTYPEcode() {
	switch ($scope.patient.LOCTYPE) {
		case 'H':
      $scope.LOCTYPEDESC = 'Home';
			break;
		case 'A':
      $scope.LOCTYPEDESC = 'Assisted Living';
			break;
    case 'O':
      $scope.LOCTYPEDESC = 'Other';
      break;
    }
  }

  convertLOCTYPE2code();
  function convertLOCTYPE2code() {
  	switch ($scope.patient.LOCTYPE2) {
  		case 'H':
        $scope.LOCTYPE2DESC = 'Home';
  			break;
  		case 'A':
        $scope.LOCTYPE2DESC = 'Apartment';
  			break;
      }
    }


  function convertRELIGIONcode() {
    if ($scope.patient.RELIGION!== null) {
      dataService.get('religion', {'KEYID': $scope.patient.RELIGION}).then(function(data) {
        // console.log(data);
        $scope.RELIGIONDESC = data.data[0].DESC;
      });
    }
  }

  function convertLANGUAGEcode() {
    if ($scope.patient.LANGUAGE!== null) {
      dataService.get('language', {'KEYID': $scope.patient.LANGUAGE}).then(function(data) {
        $scope.LANGUAGEDESC = data.data[0].DESC;
      });
    }
  }

  function convertINSCOcode(){
    dataService.get('insurance', {'KEYID': $scope.patient.INSCO}).then(function(data) {
      $scope.INSCO = data.data[0];
      // $scope.INSCONAME = insurance.NAME;
    });
  }

  function convertINS2COcode(){
    // console.log($scope.admission)
    dataService.get('insurance', {'KEYID': $scope.admission.INS2CO}).then(function(data) {
      $scope.INS2CO = data.data[0];
      // console.log($scope.INS2CO)
      // $scope.INSCONAME = insurance.NAME;
    });
  }

  function convertREFRECEIVEDcode() {
    if ($scope.admission.REFRECEIVED !== null) {
      dataService.get('employee', {'KEYID': $scope.admission.REFRECEIVED}).then(function(data) {
        $scope.REFRECEIVED = data.data[0];
        // $scope.REFRECEIVEDNAME = employee.FNAME + " " + employee.LNAME ;
      });
    }
  }

  function convertREFERRSRCcode(){
    dataService.get('referralSource', {'KEYID': $scope.admission.REFERRSRC}).then(function(data) {
      $scope.REFERRSRC = data.data[0];
      // $scope.REFERRSRCNAME = referralSource.NAME;
    });
  }

  function convertREFERRPHYSICIANcode(){
    dataService.get('doctor', {'KEYID': $scope.admission.REFERRPHYSICIAN}).then(function(data) {
      $scope.REFERRPHYSICIAN = data.data[0];
      // $scope.REFERRPHYSICIANNAME = physician.FNAME + " " + physician.LNAME;
    });
    dataService.get('doctor', {'KEYID': $scope.admission.DOCTOR}).then(function(data) {
      $scope.DOCTOR = data.data[0];
      console.log($scope.DOCTOR)
      // $scope.DOCTORNAME = doctor.FNAME + " " + doctor.LNAME;
    });
  }

  function convertADMITSRCEcode() {
  	switch ($scope.admission.ADMITSRCE) {
  		case '1':
        $scope.ADMITSRCEDESC = 'Non-HC Facility(Home)';
  			break;
  		case '2':
        $scope.ADMITSRCEDESC = 'Xfr from Clinic/Office';
  			break;
      case '4':
        $scope.ADMITSRCEDESC = 'Xfr from Hospital';
        break;
      case '5':
        $scope.ADMITSRCEDESC = 'Xfr from SNF';
        break;
      case '6':
        $scope.ADMITSRCEDESC = 'Xfr from Other Facility';
        break;
      case '8':
        $scope.ADMITSRCEDESC = 'Xfr from court/law facility';
        break;
      case '3':
        $scope.ADMITSRCEDESC = 'n/a (HMO ref)';
        break;
      case '7':
        $scope.ADMITSRCEDESC = 'n/a (emergency room)';
        break;
      case '9':
        $scope.ADMITSRCEDESC = 'n/a (info not available)';
        break;
      case 'B':
        $scope.ADMITSRCEDESC = 'n/a (xfr another HHA)';
        break;
      case 'C':
        $scope.ADMITSRCEDESC = 'n/a (Re-admit same HHA)';
        break;
  	}
  }

  function convertACUITYcode() {
    switch ($scope.admission.ACUITY) {
      case 1:
        $scope.ACUITYDESC = 'Low';
        break;
      case 2:
        $scope.ACUITYDESC = 'Medium';
        break;
      case 3:
        $scope.ACUITYDESC = 'High';
        break;
    }
  }


  $scope.patient.DOB = $filter("date")($scope.patient.DOB, 'MM-dd-yyyy');

  $scope.scheduleSOC = function(event) {
    // alert("werwe")
    event.preventDefault();
    var modalInstance = $uibModal.open({
      controller: 'ScheduleSOCController',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/scheduleSOC/scheduleSOC.html',
      // windowClass: 'modal fade in',
      // windowClass: 'display-visit-modal',
      size: 'md',
      resolve: {
        // visit: visit
      }
    });

    modalInstance.result.then(function (result) {
      $state.go('scheduling');
    });
  }


});
