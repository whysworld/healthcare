app.config(function($stateProvider) {
  $stateProvider.state('patientProfile', {
    url: '/patientProfile',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/patientProfile/patientProfile.html',
        controller: 'PatientProfileController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
