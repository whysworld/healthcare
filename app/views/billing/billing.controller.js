app.controller('BillingController', function($scope, $uibModal, patientService, dataService) {

    dataService.list('claim').then(function(data) {
        $scope.claims = data.data;

        sortClaims($scope.claims);
    });

    $scope.activeClaimsList = [{}];

    var allClaims = [];
    var claimsRAP = [];
    var claimsFC = [];

    function sortClaims(claims) {
        allClaims = [];
        claimsRAP = [];
        claimsFC = [];
        allClaims = claims;
        $scope.activeClaimsList = allClaims;
        for (var i = 0; i < claims.length; i++) {
            switch (claims[i].TOB) {
                case "328":
                    claimsFC.push(claims[i]);
                    break;
                case "322":
                    claimsRAP.push(claims[i]);
                    break;
            }
        }
    }

    $scope.claimType = 'ALL';

    $scope.updateClaimList = function(claimType) {
        switch (claimType) {
            case "ALL":
                $scope.activeClaimsList = allClaims;
                break;
            case "RAP":
                $scope.activeClaimsList = claimsRAP;
                break;
            case "FC":
                $scope.activeClaimsList = claimsFC;
                break;
        }
    };

    //open modal to display all patients to add new billing
    $scope.newBilling = function() {
        event.preventDefault();
        var modalInstance = $uibModal.open({controller: 'NewBillingController', ariaLabelledBy: 'modal-title', ariaDescribedBy: 'modal-body', templateUrl: './views/modals/newBilling/newBilling.html', size: 'lg'});
        modalInstance.result.then(function(data) {
            //after claim generated, display updated patient list sorted by claimtype
            // var patients = data.patients;
            for (var i = 0; i < data.length; i++) {
                $scope.claims.push(data[i]);
            }
            sortClaims($scope.claims)
        });
    };

    $scope.showForm = function(claim) {
        event.preventDefault();
        var modalInstance = $uibModal.open({
            controller: 'UB04Controller',
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './views/modals/UB-04/UB-04.html',
            size: 'lg',
            resolve: {
                claim: claim
            }
        });
    }

    //select All checkbox
    $scope.checkAllReady = function() {

        if ($scope.selectedAllReady) {
            $scope.selectedAllReady = true;
            $scope.showGenerateClaim = true;

        } else {
            $scope.selectedAllReady = false;
            $scope.showGenerateClaim = false;

        }

        angular.forEach($scope.activeClaimsList, function(claim) {
            //only 'Ready' claims are allowed to be selected
            if (claim.STATUS === 'Ready') {
                claim.selectedReady = $scope.selectedAllReady;
            }
        });
    };

    //show 'Generate' Button
    $scope.generateClaimBtn = function() {
        // $scope.activeClaimsList = $scope.billingsFinal.concat($scope.billingsRAP);
        for (var i = 0; i < $scope.activeClaimsList.length; i++) {
            if ($scope.activeClaimsList[i].selectedReady === true) {
                $scope.showGenerateClaim = true;
                break;
            } else {
                $scope.showGenerateClaim = false;
            }
        }
    };

    //open modal to display all patients to add new billing
    $scope.reviewClaim = function() {
        //get all selected claims
        var billingsReadyArr = [];
        angular.forEach($scope.activeClaimsList, function(claim) {
            if (claim.selectedReady === true) {
                billingsReadyArr.push(claim);
            }
        });
        event.preventDefault();
        var modalInstance = $uibModal.open({
            controller: 'ReviewClaimController',
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './views/modals/reviewClaim/reviewClaim.html',
            size: 'lg',
            resolve: {
                billingsReadyArr: function() {
                    return billingsReadyArr;
                }
            }
        });
    };

});
