app.config(function($stateProvider) {
  $stateProvider.state('billing', {
    url: '/billing',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/billing/billing.html',
        controller: 'BillingController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
