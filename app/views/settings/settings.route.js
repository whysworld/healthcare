app.config(function($stateProvider) {
  $stateProvider.state('settings', {
    url: '/settings',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/settings/settings.html',
        controller: 'SettingsController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
