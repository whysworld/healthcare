app.config(function($stateProvider) {
  $stateProvider.state('employeeReports', {
    url: '/employeeReports',
    params: {
      employee: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/employeeReports/employeeReports.html',
        controller: 'EmployeeReportsController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
