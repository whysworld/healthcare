app.controller('ScheduleDashboardController', function($scope, $filter, dataService) {

  //find today's date
  var today = $filter("date")(new Date(), 'yyyy/MM/dd');
  //set today's date as default for calendar
  $scope.DATEFROM = new Date(today);
  $scope.DATETO = new Date(today);

  //on page load get all visits for today's date
  dataService.get('visit', {VISITDATE: today}).then(function(data) {
    var visits = data.data;
    //get all visitTypes
    dataService.getAll('visitType').then(function(data) {
      $scope.visitTypes=data.data;
      //get all patients
      dataService.getAll('patient').then(function (data) {
        $scope.patients = data.data;
        $scope.patients.sort(function(a,b) {
          var dateA = a.LNAME.toLowerCase();
          var dateB = b.LNAME.toLowerCase();

          if(dateA < dateB)
            return -1;
          if(dateA > dateB)
            return 1;
          return 0;
        });
        //get all employees
        dataService.getAll('employee').then(function (data) {
          $scope.employees = data.data;
          $scope.employees.sort(function(a,b) {
            var empA = a.LNAME.toLowerCase();
            var empB = b.LNAME.toLowerCase();

            if(empA < empB)
              return -1;
            if(empA > empB)
              return 1;
            return 0;
          });
          getVisitInfo(visits);
        });
      });
    });
  });

  function getVisitInfo(visits) {
    //clear activeVisitList
    $scope.activeVisitList=[];

    visits.forEach(function(visit) {
      $scope.visitTypes.forEach(function (visitType) {
        if(visit.VISITTYPE===visitType.KEYID){
          visit.VISITTYPENAME=visitType.DESC;
        }
      });
      $scope.patients.forEach(function(patient) {
        if (visit.PATKEYID===patient.KEYID){
          visit.PATIENTNAME=patient.FNAME + ' ' + patient.LNAME;
        }
      });
      $scope.employees.forEach(function(employee) {
        if(visit.EMPKEYID===employee.KEYID) {
          visit.EMPLOYEENAME=employee.FNAME + ' ' + employee.LNAME;
        }
      });
    });
    console.log(visits);
    $scope.activeVisitList=visits;
    //sort active visit list by date
    $scope.activeVisitList.sort(function(a,b) {
      var dateA = a.VISITDATE.toLowerCase();
      var dateB = b.VISITDATE.toLowerCase();

      if(dateA > dateB)
        return -1;
      if(dateA < dateB)
        return 1;
      return 0;
    });
  }


  $scope.search=function() {
    //get array of dates between search period
    var startDate = moment($scope.DATEFROM);
    var today = moment(new Date())
    var endDate = moment($scope.DATETO);

    if (today.diff(startDate, 'days') > 180) {
      $scope.errorMsg=true;
      $scope.noResults = false;
      var selectedVisitDates=[];
      $scope.activeVisitList=[];
    } else {
      $scope.errorMsg=false;
      var selectedVisitDates=[];

      while(startDate.isBefore(endDate) || startDate.isSame(endDate)) {
        selectedVisitDates.push(startDate.clone());
        startDate.add('days', 1);
      }

      var visitsList=[];
      //get visits for dates
      selectedVisitDates.forEach(function(date) {
        dataService.get('visit', {VISITDATE: date.format("YYYY-MM-DD")}).then(function(data) {
          var visitsArr = data.data;
          visitsArr.forEach(function (visit) {
            //if employee and patient is selected, sort by employee and patient
            if ( ($scope.EMPLOYEE !== 'all') && ($scope.PATIENT !== 'all') ) {
              var patient = JSON.parse($scope.PATIENT);
              var employee = JSON.parse($scope.EMPLOYEE);
              if ( (visit.PATKEYID === patient.KEYID) && (visit.EMPKEYID === employee.KEYID) ) {
                visitsList.push(visit);
              }
            } else if ($scope.PATIENT !== 'all'){ //if only patient is selected, sort by patient
              var patient = JSON.parse($scope.PATIENT);
               if (visit.PATKEYID === patient.KEYID) {
                 visitsList.push(visit);
               }
            } else if ($scope.EMPLOYEE !== 'all') { //if only employee is selected, sort by employee
              var employee = JSON.parse($scope.EMPLOYEE);
              if (visit.EMPKEYID === employee.KEYID){
                visitsList.push(visit);
              }
            } else { //if nothing is selected, show all visits between dates
              visitsList.push(visit);
            }
          });
          if (visitsList.length<1) {
            $scope.noResults = true;
            getVisitInfo(visitsList);
          } else {
            $scope.noResults = false;
            getVisitInfo(visitsList);
            console.log(visitsList);
          }
        });
      });
    };
  }

});
