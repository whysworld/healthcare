app.config(function($stateProvider) {
  $stateProvider.state('scheduleDashboard', {
    url: '/scheduleDashboard',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/scheduleDashboard/scheduleDashboard.html',
        controller: 'ScheduleDashboardController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
