app.controller('EmployeeTasksController', function($scope, employeeService, dataService) {

  $scope.employee = employeeService.get();
  console.log('$scope.employee', $scope.employee);

  dataService.get('visit', {'EMPKEYID': $scope.employee.KEYID}).then(function(data){
    $scope.visits=data.data;
    console.log('$scope.visits', $scope.visits);
    $scope.visits.forEach(function(visit) {
      //get visitType desc for a visit
      dataService.get('visitType', {KEYID:visit.VISITTYPE}).then(function(data){
        // console.log(data.data[0]);
        var visitType = data.data[0];
        // console.log(visit);
        //adding visit type desc to visit object
        visit.VISITTYPEDESC = visitType.DESC;
      });
      //get patient for a visit
      dataService.get('patient', {KEYID:visit.PATKEYID}).then(function(data){
        // console.log(data.data[0]);
        var patient = data.data[0];
        // console.log(visit);
        //adding visit type desc to visit object
        visit.VISITPATIENT = patient.FNAME + ' ' + patient.LNAME;

        dataService.get('form', {VISITID: visit.KEYID}).then(function(data){
          console.log('forms for visit', data.data);
          var forms = data.data;
          visit.VISITFORMS = forms;
          visit.VISITFORMS.forEach(function (form) {
            dataService.get('formType', {KEYID:form.FORMTYPE}).then(function (data) {
              form.VISITFORM_DESC = data.data[0].NAME;
            });
          });
        });
          // dataService.get('listPatientRecords', {PATKEYID: patient.KEYID}).then(function(data) {
          //   console.log('visits for patient', data.data);
          //   var forms = data.data.FORMS;
          //   visit.VISITFORMS = forms;
          // });
      });

    });
  });

  // dataService.get('listPatientRecords', {PATKEYID: patient.KEYID}).then(function(data) {
  //   console.log('forms for patient', data.data);
  //   var forms = data.data;
  //   // visit.VISITFORMS = forms;
  // });


  $scope.setVisitNum = function (index){
    // console.log(index);
    $scope.activeVisit=index;
  };

  $scope.isShowing = function(index){
    return $scope.activeVisit === index;
  };




  // $scope.forms = [{
  //     EMPNAME: 'Jack Bauer',
  //     PNAME: 'Dan Smith',
  //     NAME: 'OASIS-C2 Transfer-disc v. 2.20.1',
  //     DESC: 'OASIS Transfer to inpatient facility - Patient dischange',
  //     DEADLINEDATE: '01/21/2017',
  //     RECVDATE: '01/02/2017',
  //     STATUS: 'Incomplete',
  //     TABLENAME: 'oasis_c2_2_20_1',
  //     FORMTYPE: 7,
  //     PAID: 1,
  //   },
  //   {
  //     EMPNAME: 'Jill Jackson',
  //     PNAME: 'Catherine Jones',
  //     NAME: 'OASIS-C2 Recert v. 2.20.1',
  //     DESC: 'OASIS Recertification subset',
  //     DEADLINEDATE: '01/19/2017',
  //     ASSIGNEDFROM: 'You can easily customise the date formats and i18n strings used throughout the calendar by using the',
  //     RECVDATE: '01/01/2017',
  //     STATUS: 'Complete',
  //     TABLENAME: 'oasis_c2_2_20_1',
  //     FORMTYPE: 4,
  //     PAID: 0,
  //   },
  //   {
  //     EMPNAME: 'Bob Smith',
  //     PNAME: 'Sam Green',
  //     NAME: 'OASIS-C2 SOC v. 2.20.1',
  //     DESC: 'OASIS Start of Care subset',
  //     DEADLINEDATE: '01/28/2017',
  //     RECVDATE: '01/09/2017',
  //     STATUS: 'Incomplete',
  //     TABLENAME: 'oasis_c2_2_20_1',
  //     FORMTYPE: 2,
  //     PAID: 1,
  //   },
  // ];
});
