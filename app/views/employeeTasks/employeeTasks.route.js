app.config(function($stateProvider) {
  $stateProvider.state('employeeTasks', {
    url: '/employeeTasks',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/employeeTasks/employeeTasks.html',
        controller: 'EmployeeTasksController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
