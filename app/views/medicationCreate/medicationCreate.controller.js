app.controller('MedicationCreateController', function($scope, $filter, $uibModal, dataService, patientService) {

	$scope.patient = patientService.get();

	$scope.createMedication = function(isValid) {

		if (isValid === false) {
			console.log("Form not valid");
		} else {
			// If the medication is valid, save it.
			// 
			var newMedication = {
				ADMINBY : $scope.newMedication.ADMINBY,
				CODE : $scope.newMedication.CODE,
				DATE_DISC : $filter("date")($scope.newMedication.DATE_DISC, 'yyyy-MM-dd'),
				DATE_STARTED : $filter("date")($scope.newMedication.DATE_STARTED, 'yyyy-MM-dd'),
				DOSE : $scope.newMedication.DOSE,
				FREQUENCY : $scope.newMedication.FREQUENCY,
				NAME : $scope.newMedication.NAME,
				PATID : $scope.patient.KEYID,
				PHARMACY : $scope.newMedication.PHARMACY,
				PURPOSE : $scope.newMedication.PURPOSE,
				SIDE_EFFECTS : $scope.newMedication.SIDE_EFFECTS,
			};

			console.log(newMedication);
			dataService.add('patientMedication', newMedication).then(function(response) {
			  console.log(response);
			});
		}
	}
});
