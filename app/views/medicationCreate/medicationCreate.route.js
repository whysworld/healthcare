app.config(function($stateProvider) {
  $stateProvider.state('medicationCreate', {
    url: '/medicationCreate',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/medicationCreate/medicationCreate.html',
        controller: 'MedicationCreateController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
