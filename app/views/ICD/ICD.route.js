app.config(function($stateProvider) {
  $stateProvider.state('ICD', {
    url: '/ICD',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/ICD/ICD.html',
        controller: 'ICDController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
