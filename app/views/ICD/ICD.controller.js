app.controller('ICDController', function($scope, dataService) {

	$scope.getICD10CodesList = function() {
		if (($scope.searchCode == null || $scope.searchCode == "") && ($scope.searchDescription == null || $scope.searchDescription == "")) {
			dataService.getAll('ICD10Code').then(function(data) {
				$scope.activeICDCodeList = data.data;
			});
		} else {
			var searchObject = {
				CODE: $scope.searchCode,
				DESCRIPTION: $scope.searchDescription
			}
			dataService.search('ICD10Code', searchObject).then(function(data) {
				$scope.activeICDCodeList = data.data;
			});
		}
	}

	$scope.getICD10CodesList();

});
