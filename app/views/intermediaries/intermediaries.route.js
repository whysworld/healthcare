app.config(function($stateProvider) {
  $stateProvider.state('intermediaries', {
    url: '/intermediaries',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/intermediaries/intermediaries.html',
        controller: 'IntermediariesController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
