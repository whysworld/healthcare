app.controller('IntermediariesController', function ($scope,
  $uibModal, dataService) {
  dataService.getAll('intermediary').then(function (data) {
    $scope.intermediariesObject = data.data
  })

  $scope.newIntermediary = function () {
    event.preventDefault()
    var modalInstance = $uibModal.open({
      controller: 'NewIntermediaryController',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/newIntermediary/newIntermediary.html',
      size: 'lg'
    })
    modalInstance.result.then(function () {
      dataService.getAll('intermediary').then(function (data) {
        $scope.intermediariesObject = data.data
      })
    })
  }

  $scope.showIntermediaryDetail = function (intermediary) {
    event.preventDefault()
    var modalInstance = $uibModal.open({
      controller: 'ViewIntermediaryController',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/viewIntermediary/viewIntermediary.html',
      size: 'lg',
      resolve: {
        intermediary: intermediary
      }
    })
    modalInstance.result.then(function () {
      // $scope.intermediariesObject = intermediaries
      dataService.getAll('intermediary').then(function (data) {
        $scope.intermediariesObject = data.data
      })
    })
  }
})
