app.config(function($stateProvider) {
  $stateProvider.state('qa', {
    url: '/qa',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/qa/qa.html',
        controller: 'QAController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
