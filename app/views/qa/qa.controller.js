app.controller('QAController', function ($scope, $state, $uibModal, dataService, patientService, formService) {
  $scope.getQAlist = function () {
    // getting list of forms with status 1
    dataService.get('listQAForms').then(function (data) {
      // $scope.forms=data.data
      console.log('listQAForms', data)
    })
  }

  $scope.viewForm = function (form) {
    dataService.get('patient', {
      KEYID: form.PATKEYID
    }).then(function (data) {
      patientService.set(data.data[0])
      $scope.patient = patientService.get()
    })
    console.log('view form', form)
    if (form.TABLENAME === 'oasis_c2_2_20_1') {
      dataService.get('oasisC2_2_20_1', {
        'KEYID': form.OBJKEYID
      }).then(function (data) {
        console.log(data.data[0])
        data.data[0].ACTION = 'QAREVIEW'
        data.data[0].ADMITKEYID = form.ADMITKEYID
        data.data[0].FORMID = form.KEYID
        data.data[0].OBJKEYID = form.OBJKEYID
        data.data[0].PATKEYID = form.PATKEYID
        data.data[0].EPIKEYID = form.EPIKEYID
        data.data[0].FORMDATE = form.FORMDATE
        data.data[0].VISITID = form.VISITID
        formService.set(data.data[0])
        switch (form.FORMTYPE) {
          case 1:
            // $state.go('master')
            alert('Unable to view this form')
            break
          case 2:
            $state.go('viewOasisStartOfCare')
            break
          case 3:
            $state.go('viewOasisResumptionOfCare')
            break
          case 4:
            $state.go('viewOasisRecertification')
            break
          case 5:
            $state.go('viewOasisOtherFollowUp')
            break
          case 6:
            $state.go('viewOasisTransferNoDischarge')
            break
          case 7:
            $state.go('viewOasisTransferAndDischarge')
            break
          case 8:
            $state.go('viewOasisDeathAtHome')
            break
          case 9:
            $state.go('viewOasisDischarge')
            break
          default:
            alert('An error occured. Please contact the staff.')
        }
      })
    } else if (form.TABLENAME === 'routesheet') {
      dataService.get('routeSheet', {
        'FORMID': form.KEYID
      }).then(function (data) {
        console.log(data.data[0])
        data.data[0].ACTION = 'QAREVIEW'
        data.data[0].ADMITKEYID = form.ADMITKEYID
        data.data[0].FORMID = form.KEYID
        data.data[0].OBJKEYID = form.OBJKEYID
        data.data[0].PATKEYID = form.PATKEYID
        data.data[0].EPIKEYID = form.EPIKEYID
        data.data[0].FORMDATE = form.FORMDATE
        data.data[0].VISITID = form.VISITID
        data.data[0].ASSIGNEDTO = form.ASSIGNEDTO
        data.data[0].STATUS = form.STATUS
        formService.set(data.data[0])
        $state.go('routeSheet')
      })
    } else if (form.TABLENAME === 'face') {
      dataService.get('faceSheet', {
        'FORMID': form.KEYID
      }).then(function (data) {
        console.log('-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-')
        console.log(data.data)
        data.data.ACTION = 'QAREVIEW'
        // data.data[0].ADMITKEYID=form.ADMITKEYID
        data.data.FORMID = form.KEYID
        data.data.OBJKEYID = form.OBJKEYID
        data.data.PATKEYID = form.PATKEYID
        data.data.EPIKEYID = form.EPIKEYID
        // data.daa[0].FORMDATE=form.FORMDATE
        data.data.VISITID = form.VISITID
        data.data.ASSIGNEDTO = form.ASSIGNEDTO
        data.data.STATUS = form.STATUS
        data.data.formPrimaryId = data.data.currentFormId
        // data.data.FORMDATE = $filter('date')(data.data.date_encounter, 'yyyy/MM/dd')
        // data.data.SIGDATE = $filter('date')(data.data.date, 'yyyy/MM/dd')
        formService.set(data.data)
        $state.go('face')
      })
    } else if (form.TABLENAME === 'missedvisitform') {
      dataService.get('missedvisitSheet', {
        'FORMID': form.KEYID
      }).then(function (data) {
        console.log('-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-')
        console.log(data.data)
        data.data.ACTION = 'QAREVIEW'
        // data.data[0].ADMITKEYID=form.ADMITKEYID
        data.data.FORMID = form.KEYID
        data.data.OBJKEYID = form.OBJKEYID
        data.data.PATKEYID = form.PATKEYID
        data.data.EPIKEYID = form.EPIKEYID
        // data.daa[0].FORMDATE=form.FORMDATE
        data.data.VISITID = form.VISITID
        data.data.ASSIGNEDTO = form.ASSIGNEDTO
        data.data.STATUS = form.STATUS
        data.data.formPrimaryId = data.data.currentFormId
        // data.data.FORMDATE = $filter('date')(data.data.date_encounter, 'yyyy/MM/dd')
        // data.data.SIGDATE = $filter('date')(data.data.date, 'yyyy/MM/dd')
        formService.set(data.data)
        $state.go('missedvisit')
      })
    } else if (form.TABLENAME === 'lvnform') {
      dataService.get('lvnSheet', {
        'FORMID': form.KEYID
      }).then(function (data) {
        data.data.ACTION = 'QAREVIEW'

        data.data.FORMID = form.KEYID
        data.data.OBJKEYID = form.OBJKEYID
        data.data.PATKEYID = form.PATKEYID
        data.data.EPIKEYID = form.EPIKEYID

        data.data.VISITID = form.VISITID
        data.data.ASSIGNEDTO = form.ASSIGNEDTO
        data.data.STATUS = form.STATUS
        data.data.formPrimaryId = data.data.currentFormId

        formService.set(data.data)
        $state.go('lvn')
      })
    } else if (form.TABLENAME === 'hhaform') {
      dataService.get('hhaSheet', {
        'FORMID': form.KEYID
      }).then(function (data) {
        data.data.ACTION = 'QAREVIEW'

        data.data.FORMID = form.KEYID
        data.data.OBJKEYID = form.OBJKEYID
        data.data.PATKEYID = form.PATKEYID
        data.data.EPIKEYID = form.EPIKEYID

        data.data.VISITID = form.VISITID
        data.data.ASSIGNEDTO = form.ASSIGNEDTO
        data.data.STATUS = form.STATUS
        data.data.formPrimaryId = data.data.currentFormId

        formService.set(data.data)
        $state.go('hha')
      })
    } else if (form.TABLENAME === 'hhacareplan') {
      dataService.get('hhacareSheet', {
        'FORMID': form.KEYID
      }).then(function (data) {
        data.data.ACTION = 'QAREVIEW'

        data.data.FORMID = form.KEYID
        data.data.OBJKEYID = form.OBJKEYID
        data.data.PATKEYID = form.PATKEYID
        data.data.EPIKEYID = form.EPIKEYID

        data.data.VISITID = form.VISITID
        data.data.ASSIGNEDTO = form.ASSIGNEDTO
        data.data.STATUS = form.STATUS
        data.data.formPrimaryId = data.data.currentFormId

        formService.set(data.data)
        $state.go('hhacareplan')
      })
    } else if (form.TABLENAME === 'woundform') {
      dataService.get('woundSheet', {
        'FORMID': form.KEYID
      }).then(function (data) {
        data.data.ACTION = 'QAREVIEW'

        data.data.FORMID = form.KEYID
        data.data.OBJKEYID = form.OBJKEYID
        data.data.PATKEYID = form.PATKEYID
        data.data.EPIKEYID = form.EPIKEYID

        data.data.VISITID = form.VISITID
        data.data.ASSIGNEDTO = form.ASSIGNEDTO
        data.data.STATUS = form.STATUS
        data.data.formPrimaryId = data.data.woundData.currentFormId

        formService.set(data.data)
        $state.go('wound')
      })
    } else if (form.TABLENAME === 'ptvisit') {
      dataService.get('ptvisitSheet', {
        'FORMID': form.KEYID
      }).then(function (data) {
        data.data.ACTION = 'QAREVIEW'

        data.data.FORMID = form.KEYID
        data.data.OBJKEYID = form.OBJKEYID
        data.data.PATKEYID = form.PATKEYID
        data.data.EPIKEYID = form.EPIKEYID

        data.data.VISITID = form.VISITID
        data.data.ASSIGNEDTO = form.ASSIGNEDTO
        data.data.STATUS = form.STATUS
        data.data.formPrimaryId = data.data.currentFormId

        formService.set(data.data)
        $state.go('ptvisit')
      })
    } else if (form.TABLENAME === 'pteval') {
      dataService.get('ptevalSheet', {
        'FORMID': form.KEYID
      }).then(function (data) {
        data.data.ACTION = 'QAREVIEW'

        data.data.FORMID = form.KEYID
        data.data.OBJKEYID = form.OBJKEYID
        data.data.PATKEYID = form.PATKEYID
        data.data.EPIKEYID = form.EPIKEYID

        data.data.VISITID = form.VISITID
        data.data.ASSIGNEDTO = form.ASSIGNEDTO
        data.data.STATUS = form.STATUS
        data.data.formPrimaryId = data.data.currentFormId

        formService.set(data.data)
        $state.go('pteval')
      })
    } else if (form.TABLENAME === 'ptdischarge') {
      dataService.get('ptdischargeSheet', {
        'FORMID': form.KEYID
      }).then(function (data) {
        data.data.ACTION = 'QAREVIEW'

        data.data.FORMID = form.KEYID
        data.data.OBJKEYID = form.OBJKEYID
        data.data.PATKEYID = form.PATKEYID
        data.data.EPIKEYID = form.EPIKEYID

        data.data.VISITID = form.VISITID
        data.data.ASSIGNEDTO = form.ASSIGNEDTO
        data.data.STATUS = form.STATUS
        data.data.formPrimaryId = data.data.currentFormId

        formService.set(data.data)
        $state.go('ptdischarge')
      })
    } else if (form.TABLENAME === 'planofcare') {
      var formData = {
        FORMID: form.KEYID,
        OBJKEYID: form.OBJKEYID,
        STATUS: form.STATUS,
        ACTION: 'qaEdit'
      }
      formService.set(formData)
      $state.go('viewPlanOfCare')
    } else if (form.TABLENAME === 'skillednursingnote') {
      dataService.get('skilledNursingNote', {
        FORMID: form.KEYID
      }).then(function (data) {
        console.log(data)
        data.data[0].ACTION = 'qaMode'
        data.data[0].STATUS = form.STATUS
        formService.set(data.data[0])
        $state.go('viewSkilledNursingNote')
      })
    } else if (form.FORMNAME === 'Patient Consent') {
      dataService.get('patientConsent', {
        FORMID: form.KEYID
      }).then(function (data) {
        data.data[0].ACTION = 'qaMode'
        data.data[0].STATUS = form.STATUS
        data.data[0].FORMID = form.KEYID
        data.data[0].OBJKEYID = form.OBJKEYID
        data.data[0].VISITID = form.VISITID
        formService.set(data.data[0])
        $state.go('viewPatientConsent')
      })
    } else if (form.FORMNAME === 'Physician Order') {
      console.log(form)

      dataService.get('orders', {
        KEYID: form.OBJKEYID
      }).then(function (data) {
        var orderObj = data.data[0]
        orderObj.STATUS = form.STATUS
        orderObj.ACTION = 'qaMode'
        var modalInstance = $uibModal.open({
          controller: 'ViewOrderController',
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: './views/modals/viewOrder/viewOrder.html',
          size: 'md',
          resolve: {
            form: orderObj,
            formAdded: true
          }
        })
        modalInstance.result.then(function () {
          $scope.getForms()
        })
      })
    } else {
      alert('error')
    }
  }

  // sort by form name
  $scope.sortFORMNAME = true
  $scope.sortByFORMNAME = function () {
    $scope.sortDEADLINEDATE = true
    $scope.sortPNAME = true
    $scope.sortEMPNAME = true
    $scope.sortDESC = true
    $scope.sortSTATUS = true
    $scope.sortPAID = true
    $scope.sortFORMNAME = !$scope.sortFORMNAME
    console.log($scope.sortFORMNAME)
    if ($scope.sortFORMNAME === false) {
      $scope.forms.sort(function (a, b) {
        var nameA = a.FORMNAME.toLowerCase()
        var nameB = b.FORMNAME.toLowerCase()

        if (nameA < nameB) {
          return -1
        }
        if (nameA > nameB) {
          return 1
        }
        return 0
      })
    } else if ($scope.sortFORMNAME === true) {
      $scope.forms.sort(function (a, b) {
        var nameA = a.FORMNAME.toLowerCase()
        var nameB = b.FORMNAME.toLowerCase()

        if (nameA > nameB) {
          return -1
        }
        if (nameA < nameB) {
          return 1
        }
        return 0
      })
    }
  }

  // sort by deadline date
  $scope.sortDEADLINEDATE = true
  $scope.sortByDEADLINEDATE = function () {
    $scope.sortFORMNAME = true
    $scope.sortPNAME = true
    $scope.sortEMPNAME = true
    $scope.sortDESC = true
    $scope.sortSTATUS = true
    $scope.sortPAID = true
    $scope.sortDEADLINEDATE = !$scope.sortDEADLINEDATE
    console.log($scope.sortDEADLINEDATE)
    if ($scope.sortDEADLINEDATE === false) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.DEADLINEDATE.toLowerCase()
        var dateB = b.DEADLINEDATE.toLowerCase()

        if (dateA < dateB) {
          return -1
        }
        if (dateA > dateB) {
          return 1
        }
        return 0
      })
    } else if ($scope.sortDEADLINEDATE === true) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.DEADLINEDATE.toLowerCase()
        var dateB = b.DEADLINEDATE.toLowerCase()

        if (dateA > dateB) {
          return -1
        }
        if (dateA < dateB) {
          return 1
        }
        return 0
      })
    }
  }

  // sort by patient name
  $scope.sortPNAME = true
  $scope.sortByPNAME = function () {
    $scope.sortFORMNAME = true
    $scope.sortDEADLINEDATE = true
    $scope.sortEMPNAME = true
    $scope.sortDESC = true
    $scope.sortSTATUS = true
    $scope.sortPAID = true
    $scope.sortPNAME = !$scope.sortPNAME
    console.log($scope.sortPNAME)
    if ($scope.sortPNAME === false) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.PNAME.toLowerCase()
        var dateB = b.PNAME.toLowerCase()

        if (dateA < dateB) {
          return -1
        }
        if (dateA > dateB) {
          return 1
        }
        return 0
      })
    } else if ($scope.sortPNAME === true) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.PNAME.toLowerCase()
        var dateB = b.PNAME.toLowerCase()

        if (dateA > dateB) {
          return -1
        }
        if (dateA < dateB) {
          return 1
        }
        return 0
      })
    }
  }

  // sort by form description
  $scope.sortDESC = true
  $scope.sortByDESC = function () {
    $scope.sortFORMNAME = true
    $scope.sortDEADLINEDATE = true
    $scope.sortPNAME = true
    $scope.sortEMPNAME = true
    $scope.sortSTATUS = true
    $scope.sortPAID = true
    $scope.sortDESC = !$scope.sortDESC
    console.log($scope.sortDESC)
    if ($scope.sortDESC === false) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.DESC.toLowerCase()
        var dateB = b.DESC.toLowerCase()

        if (dateA < dateB) {
          return -1
        }
        if (dateA > dateB) {
          return 1
        }
        return 0
      })
    } else if ($scope.sortDESC === true) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.DESC.toLowerCase()
        var dateB = b.DESC.toLowerCase()

        if (dateA > dateB) {
          return -1
        }
        if (dateA < dateB) {
          return 1
        }
        return 0
      })
    }
  }

  // sort by employee name
  $scope.sortEMPNAME = true
  $scope.sortByEMPNAME = function () {
    $scope.sortFORMNAME = true
    $scope.sortDEADLINEDATE = true
    $scope.sortPNAME = true
    $scope.sortDESC = true
    $scope.sortSTATUS = true
    $scope.sortPAID = true
    $scope.sortEMPNAME = !$scope.sortEMPNAME
    console.log($scope.sortEMPNAME)
    if ($scope.sortEMPNAME === false) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.EMPNAME.toLowerCase()
        var dateB = b.EMPNAME.toLowerCase()

        if (dateA < dateB) {
          return -1
        }
        if (dateA > dateB) {
          return 1
        }
        return 0
      })
    } else if ($scope.sortEMPNAME === true) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.EMPNAME.toLowerCase()
        var dateB = b.EMPNAME.toLowerCase()

        if (dateA > dateB) {
          return -1
        }
        if (dateA < dateB) {
          return 1
        }
        return 0
      })
    }
  }

  // sort by status
  $scope.sortSTATUS = true
  $scope.sortBySTATUS = function () {
    $scope.sortFORMNAME = true
    $scope.sortDEADLINEDATE = true
    $scope.sortPNAME = true
    $scope.sortDESC = true
    $scope.sortEMPNAME = true
    $scope.sortPAID = true
    $scope.sortSTATUS = !$scope.sortSTATUS
    console.log($scope.sortSTATUS)
    if ($scope.sortSTATUS === false) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.STATUS.toLowerCase()
        var dateB = b.STATUS.toLowerCase()

        if (dateA < dateB) {
          return -1
        }
        if (dateA > dateB) {
          return 1
        }
        return 0
      })
    } else if ($scope.sortSTATUS === true) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.STATUS.toLowerCase()
        var dateB = b.STATUS.toLowerCase()

        if (dateA > dateB) {
          return -1
        }
        if (dateA < dateB) {
          return 1
        }
        return 0
      })
    }
  }

  // sort by PAID
  $scope.sortPAID = true
  $scope.sortByPAID = function () {
    $scope.sortFORMNAME = true
    $scope.sortDEADLINEDATE = true
    $scope.sortPNAME = true
    $scope.sortDESC = true
    $scope.sortEMPNAME = true
    $scope.sortSTATUS = true
    $scope.sortPAID = !$scope.sortPAID
    console.log($scope.sortPAID)
    if ($scope.sortPAID === false) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.PAID
        var dateB = b.PAID

        if (dateA < dateB) {
          return -1
        }
        if (dateA > dateB) {
          return 1
        }
        return 0
      })
    } else if ($scope.sortPAID === true) {
      $scope.forms.sort(function (a, b) {
        var dateA = a.PAID
        var dateB = b.PAID

        if (dateA > dateB) {
          return -1
        }
        if (dateA < dateB) {
          return 1
        }
        return 0
      })
    }
  }

  $scope.getForms = function () {
    dataService.get('form', {
      STATUS: 1
    }).then(function (data) {
      console.log(data.data)
      $scope.forms = data.data
      $scope.forms.forEach(function (form) {
        dataService.get('patient', {
          KEYID: form.PATKEYID
        }).then(function (data) {
          var patient = data.data[0]
          form.PNAME = patient.FNAME + ' ' + patient.LNAME
        })
        if (form.ASSIGNEDTO !== null) {
          dataService.get('employee', {
            KEYID: form.ASSIGNEDTO
          }).then(function (data) {
            var employee = data.data[0]
            // console.log(employee)
            form.EMPNAME = employee.FNAME + ' ' + employee.LNAME
          })
        }
        dataService.get('formType', {
          KEYID: form.FORMTYPE
        }).then(function (data) {
          var formType = data.data[0]
          form.FORMNAME = formType.NAME
          form.DESC = formType.DESCRIPTION
          form.TABLENAME = formType.TABLENAME
        })
      })
      // console.log($scope.forms)
    })
  }

  // $scope.forms = [{
  //     EMPNAME: 'Jack Bauer',
  //     PNAME: 'Dan Smith',
  //     NAME: 'OASIS-C2 Transfer-disc v. 2.20.1',
  //     DESC: 'OASIS Transfer to inpatient facility - Patient dischange',
  //     DEADLINEDATE: '01/21/2017',
  //     RECVDATE: '01/02/2017',
  //     STATUS: 'Incomplete',
  //     TABLENAME: 'oasis_c2_2_20_1',
  //     FORMTYPE: 7,
  //     PAID: 1,
  //   },
  //   {
  //     EMPNAME: 'Jill Jackson',
  //     PNAME: 'Catherine Jones',
  //     NAME: 'OASIS-C2 Recert v. 2.20.1',
  //     DESC: 'OASIS Recertification subset',
  //     DEADLINEDATE: '01/19/2017',
  //     ASSIGNEDFROM: 'You can easily customise the date formats and i18n strings used throughout the calendar by using the',
  //     RECVDATE: '01/01/2017',
  //     STATUS: 'Complete',
  //     TABLENAME: 'oasis_c2_2_20_1',
  //     FORMTYPE: 4,
  //     PAID: 0,
  //   },
  //   {
  //     EMPNAME: 'Bob Smith',
  //     PNAME: 'Sam Green',
  //     NAME: 'OASIS-C2 SOC v. 2.20.1',
  //     DESC: 'OASIS Start of Care subset',
  //     DEADLINEDATE: '01/28/2017',
  //     RECVDATE: '01/09/2017',
  //     STATUS: 'Incomplete',
  //     TABLENAME: 'oasis_c2_2_20_1',
  //     FORMTYPE: 2,
  //     PAID: 1,
  //   },
  // ]

  $scope.getQAlist()
  $scope.getForms()
})
