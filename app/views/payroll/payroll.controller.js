app.controller('PayrollController', function ($scope, $filter, dataService) {
  // get all Employee
  dataService.getAll('employee').then(function (data) {
    $scope.employees = data.data
  })

  // get all visitTypes
  dataService.getAll('visitType').then(function (data) {
    $scope.visitTypes = data.data
  })

  $scope.payroll = true

  // select employee dropdown
  $scope.selectEMP = function (employee) {
    if (employee === 'all') {
      $scope.employeeInfo = false
      $scope.total = false
      $scope.header = false
      $scope.noResults = false
      $scope.dateErrorMsg = false
      $scope.print = false
      // $scope.payroll=true
    } else {
      $scope.allEmployees = false
      // $scope.payroll=true
      $scope.total = false
      $scope.header = false
      $scope.noResults = false
      $scope.dateErrorMsg = false
      $scope.print = false
      $scope.employeeInfo = true

      $scope.employee = JSON.parse(employee)
    }

    // // if selected View All Employee
    // if(employee === 'all') {
    //   $scope.employeeInfo=false
    //   // get visits for all employees
    //   $scope.employees.forEach(function(employee) {
    //     dataService.get('visit', {'EMPKEYID': employee.KEYID}).then(function(data){
    //       console.log(data.data)
    //       employee.VISITSARR = data.data
    //       // get visitType info
    //       employee.VISITSARR.forEach(function(visit) {
    //         // get visitType desc for a visit
    //         for (var i = 0; i < $scope.visitTypes.length; i++) {
    //           if (visit.VISITTYPE === $scope.visitTypes[i].KEYID) {
    //               visit.VISITTYPEDESC = $scope.visitTypes[i].DESC
    //               visit.VISITTYPEDISC = $scope.visitTypes[i].DISCIPLINE
    //           }
    //         }
    //         // get patient for a visit
    //         dataService.get('patient', {KEYID:visit.PATKEYID}).then(function(data){
    //           var patient = data.data[0]
    //           // adding visit type desc to visit object
    //           visit.VISITPATIENT = patient.LNAME + ', ' + patient.FNAME
    //         })
    //         // get TOTAL
    //         visit.VISITTOTAL = (visit.HOURS) * (visit.UNITCHARGE)
    //       })
    //     })
    //   })
    // } else {
    //   $scope.employee = JSON.parse(employee)
    //   $scope.employeeInfo=true
    //   dataService.get('visit', {'EMPKEYID': $scope.employee.KEYID}).then(function(data){
    //     $scope.visits = data.data
    //     $scope.selectedVisits=[]
    //     // get visitType info
    //     $scope.visits.forEach(function(visit) {
    //       // get visitType desc for a visit
    //       for (var i = 0; i < $scope.visitTypes.length; i++) {
    //         if (visit.VISITTYPE === $scope.visitTypes[i].KEYID) {
    //             visit.VISITTYPEDESC = $scope.visitTypes[i].DESC
    //             visit.VISITTYPEDISC = $scope.visitTypes[i].DISCIPLINE
    //         }
    //       }
    //       // get patient for a visit
    //       dataService.get('patient', {KEYID:visit.PATKEYID}).then(function(data){
    //         var patient = data.data[0]
    //         // adding visit type desc to visit object
    //         visit.VISITPATIENT = patient.LNAME + ', ' + patient.FNAME
    //       })
    //
    //       // get TOTAL
    //       visit.VISITTOTAL = (visit.HOURS) * (visit.UNITCHARGE)
    //     })
    //   })
    // }
  }

  $scope.getVisits = function () {
    $scope.EMPPAYROLL = new Date()
    $scope.noResults = false
    $scope.dateErrorMsg = false
    // check to see if end date is entered
    if ($scope.ENDDATE === undefined) {
      $scope.dateErrorMsg = true // show error msg if not
    } else { // if end date is entered
      var endDate = $filter('date')($scope.ENDDATE, 'yyyy-MM-dd')
      // if all employees is selected
      if ($scope.EMPLOYEE === 'all') {
        $scope.print = true
        $scope.allEmployees = true
        $scope.header = false
        $scope.employees.forEach(function (employee) {
          var allEmpHoursTotal = 0
          var allEmpPayTotal = 0
          dataService.get('listPayroll', {
            EMPLOYEE: employee.KEYID,
            PAY_PERIOD_ENDS: endDate
          }).then(function (data) {
            employee.VISITS = data.data.response
            employee.VISITS.forEach(function (visit) {
              // format visit date
              visit.date = $filter('date')(visit.date, 'MM-dd-yyyy')
              // format total and unit charge
              if (visit.payPerVisit === 1) {
                visit.VISITTOTAL = visit.defaultEmployeePay
                visit.UNITCHARGE = 'N/A'
              } else {
                visit.UNITCHARGE = visit.defaultEmployeePay
                visit.VISITTOTAL = parseInt(visit.defaultEmployeePay) + parseInt(visit.Hours)
              }
              // format status
              if ((visit.numSubmittedQAForms + visit.numCompletedForms) < visit.numForms) { // Incomplete if at least one form has status 0
                visit.STATUS = 'Incomplete'
              } else if (visit.numCompletedForms === visit.numForms) { // complete if all the forms have status 2
                visit.STATUS = 'Complete'
              } else if (visit.numSubmittedQAForms !== null) { // "Complete, X of Y waiting for QA" if there are X forms in QA
                visit.STATUS = visit.numSubmittedQAForms + ' pending QA'
              }
              // find total payroll hrs and pay
              if (visit.Hours !== null) {
                allEmpHoursTotal += parseInt(visit.Hours)
              }
              allEmpPayTotal += parseInt(visit.VISITTOTAL)
            })
            employee.HOURSEMPTOTAL = allEmpHoursTotal
            employee.PAYEMPTOTAL = allEmpPayTotal
          })
          console.log('$scope.employees', $scope.employees)

          //  var i = employee.VISITSARR.length
          //  while(i--) {
          //    var date = moment(employee.VISITSARR[i].VISITDATE)
          //    if(date.isBetween(startDate, endDate, null, '[]')) {
          //    } else {
          //      employee.VISITSARR.splice(i, 1)
          //    }
          //  }
          //   dataService.get('visit', {'EMPKEYID': employee.KEYID}).then(function(data){
          //     console.log(data.data)
          //     employee.VISITSARR = data.data
          //
          //     // get visitType info
          //     employee.VISITSARR.forEach(function(visit) {
          //       // get visitType desc for a visit
          //       for (var i = 0; i < $scope.visitTypes.length; i++) {
          //         if (visit.VISITTYPE === $scope.visitTypes[i].KEYID) {
          //             visit.VISITTYPEDESC = $scope.visitTypes[i].DESC
          //             visit.VISITTYPEDISC = $scope.visitTypes[i].DISCIPLINE
          //         }
          //       }
          //       // get patient for a visit
          //       dataService.get('patient', {KEYID:visit.PATKEYID}).then(function(data){
          //         var patient = data.data[0]
          //         // adding visit type desc to visit object
          //         visit.VISITPATIENT = patient.LNAME + ', ' + patient.FNAME
          //       })
          //       // get TOTAL
          //       visit.VISITTOTAL = (visit.HOURS) * (visit.UNITCHARGE)
          //     })
          //
          //     var allEmpSelVisits = []
          //     employee.VISITSARR.forEach(function(visit) {
          //       var date = moment(visit.VISITDATE)
          //
          //       // TODO check to make sure visit is between date or unpaid visits before startDate
          //       if(date.isBetween(startDate, endDate, null, '[]')) {
          //         allEmpSelVisits.push(visit)
          //       }
          //     })
          //     employee.SELECTEDVISITS = allEmpSelVisits
          //     // if there are no visits between selected period
          //     if(employee.SELECTEDVISITS.length < 1) {
          //       // $scope.noResults=true
          //       // $scope.print=false
          //         // $scope.header = false
          //         console.log('no results')
          //     } else {
          //       // if there are visits, find total hours
          //       // $scope.print=true
          //       // $scope.header = true
          //       var HOURSEMPTOTAL = 0
          //       var PAYEMPTOTAL = 0
          //       for (var i = 0; i < employee.SELECTEDVISITS.length; i++) {
          //         HOURSEMPTOTAL += parseInt(employee.SELECTEDVISITS[i].HOURS)
          //         PAYEMPTOTAL += parseInt(employee.SELECTEDVISITS[i].VISITTOTAL)
          //       }
          //       employee.HOURSEMPTOTAL = HOURSEMPTOTAL
          //       employee.PAYEMPTOTAL = PAYEMPTOTAL
          //     }
          //   })
        })
      } else { // for single employee
        $scope.allEmployees = false
        $scope.showCalendar = false
        // $scope.employee.VISITS=[]

        dataService.get('listPayroll', {
          EMPLOYEE: $scope.employee.KEYID,
          PAY_PERIOD_ENDS: endDate
        }).then(function (data) {
          // if there are no visits for selected pay period
          if (data.data.response.length === 0) {
            $scope.noResults = true
            $scope.total = false
            $scope.print = false
            $scope.header = false
          } else { // if there are visits for selected pay period
            $scope.print = true
            $scope.total = true
            $scope.header = true
            $scope.HOURSTOTAL = 0
            $scope.PAYTOTAL = 0

            $scope.employee.VISITS = data.data.response
            $scope.employee.VISITS.forEach(function (visit) {
              // format visit date
              visit.date = $filter('date')(visit.date, 'MM-dd-yyyy')
              // format total and unit charge
              if (visit.payPerVisit === 1) {
                visit.VISITTOTAL = visit.defaultEmployeePay
                visit.UNITCHARGE = 'N/A'
              } else {
                visit.UNITCHARGE = visit.defaultEmployeePay
                visit.VISITTOTAL = parseInt(visit.defaultEmployeePay) + parseInt(visit.Hours)
              }
              // format status
              if ((visit.numSubmittedQAForms + visit.numCompletedForms) < visit.numForms) { // Incomplete if at least one form has status 0
                visit.STATUS = 'Incomplete'
              } else if (visit.numCompletedForms === visit.numForms) { // complete if all the forms have status 2
                visit.STATUS = 'Complete'
              } else if (visit.numSubmittedQAForms !== null) { // "Complete, X of Y waiting for QA" if there are X forms in QA
                visit.STATUS = visit.numSubmittedQAForms + ' pending QA'
              }
              // find total payroll hrs and pay
              if (visit.Hours !== null) {
                $scope.HOURSTOTAL += parseInt(visit.Hours)
              }
              $scope.PAYTOTAL += parseInt(visit.VISITTOTAL)
            })
            console.log('$scope.employee', $scope.employee)
          }
        })
      }
    }
  }

  // $scope.payEmpPayroll=function (employee) {
  //   console.log('pay employee', employee)
  //   if ($scope.submitVisits.length<1) {
  //     alert('Please select visits to be paid.')
  //   } else {
  //     $scope.selectedEmployee=employee
  //     $scope.showCalendar=true
  //   }
  // }

  $scope.submitEmpPayroll = function (empPayrollDate, employee) {
    $scope.submitVisits = []

    employee.VISITS.forEach(function (visit) {
      if (visit.selected) {
        $scope.submitVisits.push(visit.visitID)
      }
    })
    console.log($scope.submitVisits)
    console.log(empPayrollDate)
    if (($scope.submitVisits.length < 1) || (empPayrollDate === undefined)) {
      alert('Please select visits to be paid and/or enter paycheck date.')
    } else {
      // confirm msg
      if (confirm('Are you sure you want to submit the selected visits?')) {
        var newPayroll = {
          EMPLOYEE: employee.KEYID,
          CREATION_DATE: $filter('date')(new Date(), 'yyyy-MM-dd'),
          PAYCHECK_DATE: $filter('date')(empPayrollDate, 'yyyy-MM-dd'),
          VISITS: $scope.submitVisits
        }
        console.log('new payroll', newPayroll)
        dataService.add('payroll', newPayroll).then(function (response) {
          console.log(response)
          if (response.status === 'error') {
            alert('Error: selected visits have already been paid.')
          }
        })
      }
    }
  }

  // assigning payroll date to selected visits
  $scope.assignPayrollDate = function () {
    if ($scope.EMPLOYEE === 'all') { // for all employees
      $scope.employees.forEach(function (employee) {
        employee.VISITS.forEach(function (visit) {
          if (visit.selected) {
            if ($scope.EMPPAYROLL !== undefined) {
              visit.paycheckDate = $filter('date')($scope.EMPPAYROLL, 'MM-dd-yyyy')
              visit.assign = true
            } else {
              visit.assign = false
            }
          } else {
            visit.assign = false
          }
        })
      })
    } else { // for specific employee
      $scope.employee.VISITS.forEach(function (visit) {
        if (visit.selected) {
          if ($scope.EMPPAYROLL !== undefined) {
            visit.paycheckDate = $filter('date')($scope.EMPPAYROLL, 'MM-dd-yyyy')
            visit.assign = true
          } else {
            visit.assign = false
          }
        } else {
          visit.assign = false
        }
      })
    }
  }

  $scope.printSection = function (divName) {
    var printContents = document.getElementById(divName).innerHTML
    // var originalContents = document.body.innerHTML
    var popupWin

    if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
      popupWin = window.open('', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no')

      popupWin.window.focus()

      popupWin.document.write('<!DOCTYPE html><html><head>' +
        '<link rel="stylesheet" type="text/css" href="style.css" />' +
        '</head><body onload="window.print()"><div class="reward-body">' +
        printContents +
        '</div></html>'
      )

      popupWin.onbeforeunload = function (event) {
        popupWin.close()
        return '.\n'
      }

      popupWin.onabort = function (event) {
        popupWin.document.close()
        popupWin.close()
      }
    } else {
      popupWin = window.open('', '_blank', 'width=800,height=600')
      popupWin.document.open()
      popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>')
      popupWin.document.close()
    }
    popupWin.document.close()

    return true
  }

  // select All checkbox
  $scope.checkAll = function () {
    console.log($scope.selectedAll)
    if ($scope.selectedAll) {
      $scope.selectedAll = true
    } else {
      $scope.selectedAll = false
    }
    angular.forEach($scope.employee.VISITS, function (visit) {
      if (visit.STATUS !== 'Incomplete') {
        visit.selected = $scope.selectedAll
      }
    })
  }

  // select All checkbox
  $scope.checkEmpAll = function (index) {
    if ($scope.employees[index].selectedEmpAll) {
      $scope.employees[index].selectedEmpAll = true
    } else {
      $scope.employees[index].selectedEmpAll = false
    }
    angular.forEach($scope.employees[index].SELECTEDVISITS, function (visit) {
      visit.selected = $scope.employees[index].selectedEmpAll
    })
  }

  // // finding current payroll period
  // function getCurrentPayPeriod() {
  //   var now = moment()
  //   $scope.payrollPeriods.forEach(function(period) {
  //     var startPeriod = moment(period.STARTDATE, 'MM-DD-YYYY')
  //     var endPeriod = moment(period.ENDDATE,'MM-DD-YYYY')
  //     if(now.isBetween(startPeriod, endPeriod, 'day', '[]')) {
  //       $scope.STARTDATE = new Date($filter("date")(period.STARTDATE, 'yyyy-MM-dd'))
  //       $scope.ENDDATE = new Date($filter("date")(period.ENDDATE, 'yyyy-MM-dd'))
  //     }
  //   })
  // }
  //
  // // sample data
  // $scope.payrollPeriods=[{
  //   KEYID:1,
  //   STARTDATE:"01-01-2017",
  //   ENDDATE:"01-15-2017"
  // },
  // {
  //   KEYID:2,
  //   STARTDATE:"01-16-2017",
  //   ENDDATE:"01-31-2017"
  // },
  // {
  //   KEYID:3,
  //   STARTDATE:"02-01-2017",
  //   ENDDATE:"02-15-2017"
  // }]
  //
  // $scope.payroll=true
  // getCurrentPayPeriod()
})
