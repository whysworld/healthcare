app.config(function($stateProvider) {
  $stateProvider.state('payroll', {
    url: '/payroll',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/payroll/payroll.html',
        controller: 'PayrollController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
