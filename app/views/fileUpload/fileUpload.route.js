app.config(function($stateProvider) {
  $stateProvider.state('fileUpload', {
    url: '/fileUpload',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/fileUpload/fileUpload.html',
        controller: 'FileUploadController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
