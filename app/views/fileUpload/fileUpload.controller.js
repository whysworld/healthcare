app.controller('FileUploadController', function($scope, $uibModal, dataService, patientService, $http, CONSTANTS, $httpParamSerializerJQLike, Blob, FileSaver) {

  $scope.patient = patientService.get();

  $scope.toggleAll = function() {
    // console.log($scope.isAllSelected)
     var toggleStatus = $scope.isAllSelected;
    //  console.log(toggleStatus)
     angular.forEach($scope.files, function(file) {
       file.selected = toggleStatus;
      //  console.log(file.selected)
      //  console.log(file)
     });
  };

  $scope.fileToggled = function(){
    $scope.isAllSelected = $scope.files.every(function(file){
      return file.selected;
    });
  };


  // $scope.files = [{
  //   user: "User",
  //   name: "Test",
  //   uploadBy: "Ana",
  //   date: "10/02/2016",
  //   size: "10kb"
  // },
  // {
  //   user: "User2",
  //   name: "aFileName",
  //   uploadBy: "John",
  //   date: "11/12/2016",
  //   size: "121kb"
  // },
  // {
  //   user: "User3",
  //   name: "fileName2",
  //   uploadBy: "Dan",
  //   date: "08/17/2016",
  //   size: "78kb"
  // },
  // {
  //   user: "User4",
  //   name: "moreFiles",
  //   uploadBy: "Sarah",
  //   date: "09/09/2016",
  //   size: "16kb"
  // }];
  
  $scope.files = [{}];
  $scope.loadAll = function() {
    dataService.get('patientUpload', {'PATID' : patientService.get().KEYID}).then(function(data) {
      $scope.files = data.data;
      console.log('files', $scope.files);
      angular.forEach($scope.files, function(file) {
        file.LINK = CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + "getPatientUploadFile&KEYID=" + file.KEYID;
        // console.log("link: " + file.LINK);
      });
    });
  }
  $scope.loadAll();

  $scope.newUpload = function() {
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'PatientUploadController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/patientUpload/patientUpload.html',
        size: 'lg',
        resolve: {
          selected: $scope.fileToggled(),
          parent: $scope
        }
      });
  };

  $scope.discardFiles = function() {
    angular.forEach($scope.files, function(file) {
      if (file.selected) {
        dataService.delete('patientUpload', {'KEYID': file.KEYID}).then(function(data) {
          $scope.loadAll();
        });
      }
    });
  };

  $scope.download = function(file) {
    console.log("Downloading file", file);

    $http({
      url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['patientUploadFile'].get,
      method: 'GET',
      params: {KEYID: file.KEYID},
      paramSerializer: '$httpParamSerializerJQLike',
      responseType: 'arraybuffer'
    }).then(function(data) {
      console.log('data', data);
      console.log('headers', data.headers());

      var contentType = data.headers()['content-type'];
      var disposition = data.headers()['content-disposition'];
      console.log('disposition', disposition);

      if (disposition && disposition.indexOf('attachment') !== -1) {
        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        var matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) { 
          filename = matches[1].replace(/['"]/g, '');
        } else {
          filename = "unknown";
        }
      }

      console.log('filename', filename);
      console.log('contentType', contentType)
      console.log('data.data', data.data);
      
      var dataFile = new Blob([data.data], { type: contentType });

      console.log('dataFile', dataFile);

      FileSaver.saveAs(dataFile, filename);
    });

    // dataService.get('patientUploadFile', {KEYID: file.KEYID})
  };

});
