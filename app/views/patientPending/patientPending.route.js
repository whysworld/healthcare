app.config(function($stateProvider) {
  $stateProvider.state('patientPending', {
    url: '/patientPending',
    params: {
      patient: null,
      complete: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/patientPending/patientPending.html',
        controller: 'PatientPendingController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
