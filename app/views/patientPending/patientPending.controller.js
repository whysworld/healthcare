app.controller('PatientPendingController', function($scope, $uibModal, patientService, dataService, $stateParams) {


  $scope.currentPatient = patientService.getPendingPatient();

  $scope.patientIntakeFormComplete = $stateParams.complete;

  dataService.getAll('language').then(function(data) {
    $scope.languages = data.data;
  });
  dataService.getAll('religion').then(function(data) {
    $scope.religions = data.data;
  });
  dataService.getAll('insurance').then(function(data) {
    $scope.insurances = data.data;
  });
  dataService.getAll('employee').then(function(data) {
    $scope.employees = data.data;
  });
  dataService.getAll('doctor').then(function(data) {
    $scope.doctors = data.data;
  });
  // $scope.currentPatient = {name: 'Jessica'};

  $scope.patientReview = function() {
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'PatientReviewController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/patientReview/patientReview.html',
        size: 'lg',
        resolve: {
          patient: $scope.currentPatient,
          languages: function() {
            return $scope.languages;
          },
          religions: function() {
            return $scope.religions;
          },
          insurances: function() {
            return $scope.insurances;
          },
          employees: function() {
            return $scope.employees;
          },
          doctors: function() {
            return $scope.doctors;
          }
        }
      });
  };

  $scope.createPatient = function() {
    var patient = patientService.getPendingPatient();
    if ($scope.patientIntakeFormComplete === null) {
      alert("Please complete Step 1 and Step 2 before submitting");
    }
    else {
      $scope.completeProcessComplete=true;
      var newPatient = {
        FNAME: patient.FNAME,
        DOB: patient.DOB,
        MINITIAL: patient.MINITIAL,
        GENDER: patient.GENDER,
        LNAME: patient.LNAME,
        SSN: patient.SSN,
        MARITAL: patient.MARITAL,
        LANGUAGE: patient.LANGUAGE,
        M0140: patient.M0140,
        RELIGION: patient.RELIGION,
        // responsiblePerson: null,
        ACUITY: patient.ACUITY,
        NOTES: patient.NOTES,

        LOCTYPE: patient.LOCTYPE,
        //county: patient.county,
        ADDRESS: patient.ADDRESS,
        CITY: patient.CITY,
        STATE: patient.STATE,
        ZIP: patient.ZIP,
        //country: patient.country,
        PHONE1: patient.PHONE1,
        CELLPHONE: patient.CELLPHONE,
        EMAIL: patient.EMAIL,
        //addressNotes: patient.addressNotes,
        LOCTYPE2: patient.LOCTYPE2,
        //secondaryCounty: patient.secondaryCounty,
        ADDRESS2: patient.ADDRESS2,
        CITY2: patient.CITY2,
        STATE2: patient.STATE2,
        ZIP2: patient.ZIP2,
        //secondaryCountry = patient.secondaryCountry,
        PHONE2: patient.PHONE2,
        //secondaryCell = patient.secondaryCell,
        //secondaryEmail = patient.secondaryEmail,
        //secondaryAddressNotes = patient.secondaryAddressNotes,


        //ICD??
      };
      dataService.add('patient', newPatient).then(function(response) {
        console.log(response);
        createAdmit(response, patient);
      });
    }
  };

  function createAdmit(response, patient) {
    var newAdmit = {
      PATKEYID: response.keyid,
      ADDRESS: patient.ADDRESS,
      CITY: patient.CITY,
      STATE: patient.STATE,
      ZIP: patient.ZIP,
      PHONE1: patient.PHONE1,
      PHONE2: patient.PHONE2,
      LOCTYPE: patient.LOCTYPE,

      INS1CO: patient.INS1CO,
      INS1PLAN: patient.INS1PLAN,
      INS1ID: patient.INS1ID,
      INS1GROUP: patient.INS1GROUP,
      INS1AUTHCODE: patient.INS1AUTHCODE,
      INS1NOTES: patient.INS1NOTES,
      INS2CO: patient.INS2CO,
      INS2ID: patient.INS2ID,
      INS2GROUP: patient.INS2GROUP,
      INS2AUTHCODE: patient.INS2AUTHCODE,
      INS2NOTES: patient.INS2NOTES,

      REFRECEIVED: patient.REFRECEIVED,
      REFERRSRC: patient.REFERRSRC,
      REFERRDATE: patient.REFERRDATE,
      //referralSourceInd: patient.referralSourceInd,
      REFERRPHYSICIAN: patient.REFERRPHYSICIAN,
      ADMITSRCE: patient.ADMITSRCE,
      // caretype:??
      REFERRNOTES: patient.REFERRNOTES,

      //npi?
      //F2F?
      DOCTOR: patient.DOCTOR,
      DOCTOR2: patient.DOCTOR2,
      DOCTOR3: patient.DOCTOR3,
      DOCTOR4: patient.DOCTOR4,
      DOCTOR5: patient.DOCTOR5,
      PHARMACY: patient.PHARMACY,
      DME: patient.DME,
      CASEMNGR: patient.CASEMNGR,
      DISCRN: patient.DISCRN,
      DISCLPN: patient.DISCLPN,
      DISCHA: patient.DISCHA,
      DISCPT: patient.DISCPT,
      DISCMS: patient.DISCMS,
      DISCOT: patient.DISCOT,
      DISCST: patient.DISCST,
      DISCRD: patient.DISCRD,
      MKTSOURCE: patient.MKTSOURCE,

      //icd?
      DIAGCODE: patient.DIAGCODE,
      SURGCODE: patient.SURGCODE,
    };
    dataService.add('admission', newAdmit).then(function(response) {
      console.log(response);
    });
  }
});
