app.controller('LanguagesController', function($scope, dataService) {

  dataService.getAll('language').then(function (data) {
    $scope.languages = data.data;
  });

  $scope.add = function () {
    dataService.add('language', {'DESC':$scope.DESC}).then(function (response) {
      console.log(response);
      dataService.getAll('language').then(function (data) {
        $scope.languages = data.data;
        $scope.DESC='';
      });
    });
  };

  $scope.remove = function(language) {
    console.log(language);
    dataService.delete('language', {'KEYID': language.KEYID}).then(function(response) {
      console.log(response);
      dataService.getAll('language').then(function (data) {
        $scope.languages = data.data;
      });
    });
  };

  $scope.edit = function(language, index) {
    console.log(language, index)
    $scope.activeLanguage = index;
  };

  $scope.save=function(language) {
    console.log(language);
    dataService.edit('language', language).then(function(response) {
      console.log(response);
      $scope.activeLanguage = false;
      dataService.getAll('language').then(function (data) {
        $scope.languages = data.data;
      });
    });
  };

  $scope.cancel=function() {
    $scope.activeLanguage = false;
  };
});
