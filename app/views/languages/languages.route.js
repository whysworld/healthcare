app.config(function($stateProvider) {
  $stateProvider.state('languages', {
    url: '/languages',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/languages/languages.html',
        controller: 'LanguagesController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
