app.config(function($stateProvider) {
  $stateProvider.state('landingPage', {
    url: '/',
    views: {
      // 'primaryNavigation': {
      //   templateUrl: 'views/navigation/navigation.html'
      // },
      'pageContent': {
        templateUrl: 'views/landingPage/landingPage.html',
        controller: 'LandingPageController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
