app.controller('LandingPageController', function($scope, $location, auth, $http, CONSTANTS, $httpParamSerializerJQLike, md5) {

	$scope.incorrectLogin = false
	$scope.notice = false;
	
	$scope.login = function() {

		// Hide the incorrect login data message, if any
		$scope.incorrectLogin = false;

		// Try to log in using the provided username and password
		// If they are correct, redirect the user to the welcome page
		$http({
			url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + 'login',
			method: 'POST',
			data: $httpParamSerializerJQLike(
				{
					'u' : $scope.username,
					'p' : md5.createHash($scope.password || '')
				}
			),
			headers: {
			  'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).then(function(response) {
			console.log('Saving token');
			console.log('token data returned', response.data);

			auth.saveAccessToken(response.data['access_jwt']);
			auth.saveRefreshToken(response.data['refresh_tkn'])

			$scope.incorrectLogin = false;
			console.log('Moving to welcome page');
			$location.path('welcomePage');
	    }, function(response) {
	    	console.log('Unable to login', response);
	    	console.log('Not moving to welcome page');
	    	$scope.incorrectLogin = true;
	    	$scope.password = '';
	    });
	}
});
