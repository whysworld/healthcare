app.config(function($stateProvider) {
  $stateProvider.state('employeeSearch', {
    url: '/employeeSearch',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/employeeSearch/employeeSearch.html',
        controller: 'EmployeeSearchController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
