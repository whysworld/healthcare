app.controller('EmployeeSearchController', function($scope, employeeInfo, dataService, $state, employeeService) {
  // employeeInfo.then(function(data) {
  //   $scope.employees = data;
  //   $scope.employeeType = 'ALL';
  // });

  // //
  // dataService.getAll('employee').then(function(data) {
  //   $scope.employees = data;
  //   $scope.employeeType = 'ALL';
  //   console.log(data)
  // });

  // dataService.getAll('employee').then(function(data) {
  //   console.log(data);
  // });
  // dataService.get('employee', {FNAME: 'Bob'}).then(function(data) {
  //   console.log(data);
  // });


  $scope.setEmployee = function (employee) {
    employeeService.set(employee);
    $scope.employee = employee;
  };

  $scope.activeEmployeeList = [{}];
  dataService.getAll('employee').then(function(data) {
    sortEmployees(data.data);
  });

  var allEmployees = [];
  var SNEmployees = [];
  var HAEmployees = [];
  var PTEmployees = [];
  var OTEmployees = [];
  var STEmployees = [];
  var MSWEmployees = [];
  var OTHEREmployees = [];

  function sortEmployees(employees) {
    allEmployees = employees;
    $scope.activeEmployeeList = allEmployees;
    // console.log($scope.activeEmployeeList);
    for (var i = 0; i < employees.length; i++) {
      // console.log(employees[i].DISCIPLINE);
      switch(employees[i].DISCIPLINE) {
        case "SN":
          // employees[i].DISCIPLINE = 'Admitted';
          SNEmployees.push(employees[i]);
          break;
        case "HA":
          // employees[i].DISCIPLINE = 'Pending';
          HAEmployees.push(employees[i]);
          break;
        case "PT":
          PTEmployees.push(employees[i]);
          break;
        case "OT":
          OTEmployees.push(employees[i]);
          break;
        case "ST":
          STEmployees.push(employees[i]);
          break;
        case "MSW":
          MSWEmployees.push(employees[i]);
          break;
        default:
          OTHEREmployees.push(employees[i]);
      }

      //get status
      switch(employees[i].STATUS) {
        case "A":
          employees[i].STATUSDESC = 'Active';
          break;
        case "I":
          employees[i].STATUSDESC = 'Inactive';
          break;
        }
    }
  }

  // $scope.employeeDashboard = function(event, employee) {
  //   event.stopPropagation();
  //   $state.go('employeeDashboard', {myParam: {employee: employee}});
  // };
  // $scope.employeeDashboard = function(event, employee) {
  //   event.stopPropagation();
  //   $state.go('employeeDashboard', {myParam: {employee: selectedEmployee}});
  // };
  // $scope.employeeProfile = function(event, employee) {
  //   event.stopPropagation();
  //   $state.go('employeeProfile', {myParam: {employee: selectedEmployee}});
  // };
  $scope.employeeType = 'ALL';

  $scope.updateEmployeeList = function(employeeType) {
    switch(employeeType) {
      case "ALL":
        $scope.activeEmployeeList = allEmployees;
        break;
      case "SN":
        $scope.activeEmployeeList = SNEmployees;
        break;
      case "HA":
        $scope.activeEmployeeList = HAEmployees;
        break;
      case "PT":
        $scope.activeEmployeeList = PTEmployees;
        break;
      case "OT":
        $scope.activeEmployeeList = OTEmployees;
        break;
      case "ST":
        $scope.activeEmployeeList = STEmployees;
        break;
      case "MSW":
        $scope.activeEmployeeList = MSWEmployees;
        break;
      case "OTHER":
        $scope.activeEmployeeList = OTHEREmployees;
        break;
    }
  };

  $scope.showEmployeeDetail=function() {
    $scope.expandempnav=!$scope.expandempnav;
    console.log($scope.expandempnav);
  };

  // var rightMenuOpen = false;
  // $scope.showEmployeeDetail = function(employee) {
  //   if($scope.activeEmployee) {
  //     delete $scope.activeEmployee.selected;
  //     $('.employee-right-menu').animate({
  //       left: '-150px'
  //     }, 200);
  //     $('#employee-search').animate({
  //       'margin-left': '60'
  //     }, 200);
  //   }
  //   rightMenuOpen = true;
  //   $scope.employee = employee
  //   $scope.activeEmployee = employee;
  //   $scope.activeEmployee.selected = true;
  //   event.stopPropagation();
  //   $scope.selectedEmployee = employee;
  //   $('.employee-right-menu').animate({
  //     left: '60'
  //   }, 200);
  //   $('#employee-search').animate({
  //     'margin-left': '270'
  //   }, 200);
  // };

  $scope.closeEmployeeDetail = function() {
    delete $scope.activeEmployee.selected;
    $('.employee-right-menu').animate({
      left: '-150px'
    }, 200);
    $('#employee-search').animate({
      'margin-left': '60'
    }, 200);
  };
  $scope.noHighlight = function() {
    if(rightMenuOpen) {
      delete $scope.activeEmployee.selected;
    }
    event.stopPropagation();
  };
  // $(window).click(function() {
  //   if(rightMenuOpen) {
  //     $scope.$apply(function() {
  //       $scope.closePatientDetail();
  //     });
  //   }
  // });
  $('.employee-right-menu').click(function(){
      event.stopPropagation();
  });
});
