app.controller('EmployeeCreateController', function($scope, $state, $uibModal, dataService) {

  // dataService.add('employee').then(function(data) {
  //   console.log(data);
  // });


  $scope.createEmployee = function(newEmployee, isValid) {
    console.log(newEmployee);
    if (isValid === false){
      console.log("Form not Valid");
    } else {
      // if ($scope.CONTRACTOR === "true"){
      //   $scope.CONTRACTOR = true;
      // } else {
      //   $scope.CONTRACTOR = false;
      // }
      var addEmployee = {
        LOGINNAME: $scope.LOGINNAME,
        PASSWORD: $scope.PASSWORD,
        FNAME: $scope.FNAME,
        LNAME: $scope.LNAME,
        EMAIL: $scope.EMAIL,
        ADDRESS: $scope.ADDRESS,
        CITY: $scope.CITY,
        CELL: $scope.CELL,
        FAX: $scope.FAX,
        STATE: $scope.STATE,
        ZIP: $scope.ZIP,
        PHONE: $scope.PHONE,
        STATUS: $scope.STATUS,
        CONTRACTOR: $scope.CONTRACTOR,
        TITLE: $scope.TITLE,
        DISCIPLINE: $scope.DISCIPLINE,
        GROUPKEYID: $scope.GROUPKEYID.KEYID
      };
      console.log("newEmployee",addEmployee);
      dataService.add('employee', addEmployee).then(function(response) {
        console.log(response);
        console.log(parseInt(response.keyid))
        if (response.status==='success') {
          dataService.edit('employee', {KEYID: response.keyid, EMPLOYEEID: parseInt(response.keyid)}).then(function (response) {
            console.log(response)
            $state.go('employeeSearch', {}, { reload: true });
          })
        }
      }, function(data) {
        console.log(data);
      });
    }
  };

  $scope.close = function() {
    $scope.group = '';
  };

  $scope.viewGroups = function() {
    // console.log(userGroups)
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'ViewGroupsController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/viewGroups/viewGroups.html',
        size: 'lg',
        resolve: {
          userGroups: function() {
            return $scope.userGroups;
          }
        },
    });
    modalInstance.result.then(function () {
      console.log('Modal success');
      dataService.getAll('userGroup').then(function(data) {
        console.log('updated userGroups', data.data);
        $scope.userGroups = data.data;
      });
    });
  };


  dataService.getAll('userGroup').then(function(data) {
    $scope.userGroups = data.data;
  });

});
