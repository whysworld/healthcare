app.config(function($stateProvider) {
  $stateProvider.state('employeeCreate', {
    url: '/employeeCreate',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/employeeCreate/employeeCreate.html',
        controller: 'EmployeeCreateController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
