app.config(function($stateProvider) {
  $stateProvider.state('communication', {
    url: '/communication',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/communication/communication.html',
        controller: 'CommunicationController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
