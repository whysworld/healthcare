app.config(function($stateProvider) {
  $stateProvider.state('welcomePage', {
    url: '/welcomePage',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/welcomePage/welcomePage.html',
        controller: 'WelcomePageController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
