app.controller('WelcomePageController', function($scope, $state, dataService, agencyService, auth, dataService) {

  $scope.user = auth.getUser();
  // console.log('user', $scope.user);

  // TODO: show some loader
  dataService.get('pageWelcomePage').then(function(data) {
    var pageContent = data.data;
    console.log("pageContent", pageContent);

    if ('agencies' in pageContent) {
      $scope.agencies = pageContent.agencies;
      // console.log('agencies', $scope.agencies);
    };

    if ('employee' in pageContent) {
      $scope.user = pageContent.employee;
      // console.log('employee', $scope.user);
    }
    // TODO: stop the loader here
  });

  //get agency
  // dataService.get('agencyConfig').then(function(data){
  //   console.log(data);
  //   $scope.agency=data.data;
  // });

  //set agency
  $scope.setAgency = function(agency) {
    // TODO: getAgencyConfig will return an array of configurations for the available agencies
    // we will use agency.id to select only one agency in agencyService
    // so we should filter data.data before calling agencyService.set()
    dataService.get('agencyConfig').then(function(data) {
      agencyService.set(data.data);
      $state.go('patientSearch')
    });
  };

});
