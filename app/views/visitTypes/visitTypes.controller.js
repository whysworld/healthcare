app.controller('VisitTypesController', function($scope, $uibModal, dataService) {

  $scope.showVisitTypeDetail = function(visitType, event) {
    console.log(visitType)
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'ViewVisitTypeController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/viewVisitType/viewVisitType.html',
        size: 'lg',
        resolve: {
          visitType: visitType,
          getVisitTypeList: function() {
            return $scope.getVisitTypeList;
          }
        }
    });
    modalInstance.result.then(function () {
      $scope.getVisitTypeList();
      console.log($scope.activeVisitTypeList)
    })

  };

  $scope.newVisitType = function(event) {
    event.preventDefault();
    var modalInstance = $uibModal.open({
        controller: 'NewVisitTypeController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/newVisitType/newVisitType.html',
        size: 'lg',
        resolve: {
          getVisitTypeList: function() {
            return $scope.getVisitTypeList;
          }
        }
        // resolve: {
        //   // insurance: insurance
        // }
      });
  };

  $scope.activeVisitTypeList = [{}];

  $scope.getVisitTypeList = function() {
    dataService.list('visitType').then(function(data) {
    // dataService.getAll('visitType').then(function(data) {
      // $scope.visitTypes = data.data;

      //removing all the inactive visitTypes
      $scope.inactiveVisitTypeList=[];
      data.data.forEach(function (visitType) {
        if (visitType.STATUS === 'Inactive'){
          $scope.inactiveVisitTypeList.push(visitType);
          var index = data.data.indexOf(visitType);
          if (index>-1){
            data.data.splice(index, 1);
          }
        }
      });
      console.log(data.data);
      sortVisitType(data.data);
    });
  };

  $scope.inactiveList=false;
  //show inactive list of visittypes
  $scope.viewInactive=function() {
    $scope.inactiveList=!$scope.inactiveList;
  }

  $scope.getVisitTypeList();

  var allVisitTypes = [];
  var SNVisit = [];
  var PTVisit = [];
  var OTVisit = [];
  var STVisit = [];
  var MSVisit = [];
  var HAVisit = [];

  function sortVisitType(visitTypes) {
    allVisitTypes = visitTypes;
    $scope.activeVisitTypeList = allVisitTypes;
    for (var i = 0; i < visitTypes.length; i++) {
      // console.log(visitTypes[i].DESC)
      switch(visitTypes[i].DISCIPLINE) {
        case "SN":
          // visitTypes[i].PATSTATUS = 'Admitted';
          SNVisit.push(visitTypes[i]);
          break;
        case "PT":
          // visitTypes[i].PATSTATUS = 'Pending';
          PTVisit.push(visitTypes[i]);
          break;
        case "OT":
          // visitTypes[i].PATSTATUS = 'Pending';
          OTVisit.push(visitTypes[i]);
          break;
        case "ST":
          // visitTypes[i].PATSTATUS = 'Pending';
          STVisit.push(visitTypes[i]);
          break;
        case "MS":
          // visitTypes[i].PATSTATUS = 'Pending';
          MSVisit.push(visitTypes[i]);
          break;
        case "HA":
          // visitTypes[i].PATSTATUS = 'Pending';
          HAVisit.push(visitTypes[i]);
          break;
      }
    }
  };

  $scope.visitType = 'ALL';

  $scope.updateVisitTypeList = function(visitType) {
    switch(visitType) {
      case "ALL":
        $scope.activeVisitTypeList = allVisitTypes;
        break;
      case "SNVisit":
        $scope.activeVisitTypeList = SNVisit;
        break;
      case "PTVisit":
        $scope.activeVisitTypeList = PTVisit;
        break;
      case "OTVisit":
        $scope.activeVisitTypeList = OTVisit;
        break;
      case "STVisit":
        $scope.activeVisitTypeList = STVisit;
        break;
      case "MSVisit":
        $scope.activeVisitTypeList = MSVisit;
        break;
      case "HAVisit":
        $scope.activeVisitTypeList = HAVisit;
        break;
    }
  };
});
