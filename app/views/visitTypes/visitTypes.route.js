app.config(function($stateProvider) {
  $stateProvider.state('visitTypes', {
    url: '/visitTypes',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/visitTypes/visitTypes.html',
        controller: 'VisitTypesController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
