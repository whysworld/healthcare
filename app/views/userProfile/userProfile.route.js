app.config(function($stateProvider) {
  $stateProvider.state('userProfile', {
    url: '/userProfile',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/userProfile/userProfile.html',
        controller: 'UserProfileController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
