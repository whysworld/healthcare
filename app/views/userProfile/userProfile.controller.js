app.controller('UserProfileController', function($scope, auth, dataService) {

  $scope.populateForm=function () {
    dataService.get('employee', {KEYID: auth.getUser().id}).then(function (data) {
      console.log(data.data[0]);
      $scope.employee=data.data[0];
      var employeeObj=data.data[0];
      $scope.LOGINNAME=employeeObj.LOGINNAME;
      $scope.PASSWORD=employeeObj.PASSWORD;

      $scope.FNAME=employeeObj.FNAME;
      $scope.LNAME=employeeObj.LNAME;
      $scope.EMAIL=employeeObj.EMAIL;
      $scope.ADDRESS=employeeObj.ADDRESS;
      $scope.CITY=employeeObj.CITY;
      $scope.STATE=employeeObj.STATE;
      $scope.ZIP=employeeObj.ZIP;
      $scope.FAX=employeeObj.FAX;
      $scope.PHONE=employeeObj.PHONE;
      $scope.CELL=employeeObj.CELL;
      $scope.GROUPKEYID=employeeObj.GROUPKEYID;

      convertEMPSTATUScode();
      convertEMPCONTRACTORcode();

      dataService.getAll('userGroup').then(function(data) {
        $scope.userGroups = data.data;
      });
    });
  };


  $scope.notEditable = true;

  $scope.editMode = function() {
    $scope.notEditable = false;
    $scope.editModeBtn = true;
  };

  $scope.cancel = function() {
    $scope.notEditable = true;
    $scope.editModeBtn = false;
  };



	function convertEMPSTATUScode() {
		switch ($scope.employee.STATUS) {
			case 'I':
	      $scope.empStatus = 'Inactive';
				break;
			case 'A':
	      $scope.empStatus = 'Active';
				break;
		}
	}

	function convertEMPCONTRACTORcode() {
		switch ($scope.employee.CONTRACTOR) {
			case 1:
	      $scope.empContractor = 'Yes';
				break;
			case 0:
	      $scope.empContractor = 'No';
				break;
		}
	}

  $scope.saveEmployeeEdits = function () {
      $scope.notEditable = true;
      $scope.editModeBtn = false;
    // $scope.editMode = false;
    // $scope.isCheckboxDisabled = true;
    // console.log($scope.employee.GROUPKEYID.DESCRIPTION)
    var editedEmployee = {
      KEYID: $scope.employee.KEYID,
      LOGINNAME: $scope.LOGINNAME,
      PASSWORD: $scope.PASSWORD,
      FNAME: $scope.FNAME,
      LNAME: $scope.LNAME,
      EMAIL: $scope.EMAIL,
      ADDRESS: $scope.ADDRESS,
      CITY: $scope.CITY,
      STATE: $scope.STATE,
			ZIP: $scope.ZIP,
      PHONE: $scope.PHONE,
			CELL: $scope.CELL,
  		FAX: $scope.FAX,
      STATUS: $scope.STATUS,
      CONTRACTOR: $scope.CONTRACTOR,
      TITLE: $scope.TITLE,
      DISCIPLINE: $scope.DISCIPLINE,
      GROUPKEYID: $scope.GROUPKEYID
    };
		console.log(editedEmployee);
    dataService.edit('employee', editedEmployee).then(function(response) {
      console.log(response);
			dataService.get('employee', {KEYID:$scope.employee.KEYID}).then(function(data) {
				var updatedEmployee=data.data[0];
				employeeService.set(updatedEmployee);
				$scope.employee=employeeService.get();
				convertEMPSTATUScode();
				convertEMPCONTRACTORcode();
			});
    });

  };

  $scope.populateForm();
})
