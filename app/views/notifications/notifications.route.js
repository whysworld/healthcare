app.config(function($stateProvider) {
  $stateProvider.state('notifications', {
    url: '/notifications',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/notifications/notifications.html',
        controller: 'NotificationsController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
