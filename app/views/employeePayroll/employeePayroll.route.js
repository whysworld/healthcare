app.config(function($stateProvider) {
  $stateProvider.state('employeePayroll', {
    url: '/employeePayroll',
    params: {
      employee: null
    },
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/employeePayroll/employeePayroll.html',
        controller: 'EmployeePayrollController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
