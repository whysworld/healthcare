app.controller('EmployeePayrollController', function($scope, $filter, employeeService, dataService) {

  $scope.employee = employeeService.get();

  $scope.getAllPayroll=function () {
    dataService.get('payroll', {EMPLOYEE:$scope.employee.KEYID}).then(function (data) {
      $scope.payrolls=data.data;
      console.log('$scope.payrolls', data.data);

      $scope.payrolls.forEach(function (payroll) {
        var HOURSEMPTOTAL = 0;
        var PAYEMPTOTAL = 0;

        payroll.Visits.forEach(function (visit) {
          //formatting date
          visit.Date= $filter('date')(visit.Date, "MM-dd-yyyy");
          //calulating visit total hours
          if (visit.DefaultEmployeePayPerVisit===1) {
            visit.VISITTOTAL=visit.DefaultEmployeePay;
            visit.PAYCHECK_DATE=payroll.PAYCHECK_DATE;
            // visit.UNITCHARGE='N/A';
          }
          //caluclating payroll total hours
          // HOURSEMPTOTAL += parseInt(visit.HOURS);
          PAYEMPTOTAL += parseInt(visit.VISITTOTAL);
        });
        // $scope.payroll.HOURSEMPTOTAL = HOURSEMPTOTAL;
        payroll.PAYEMPTOTAL = PAYEMPTOTAL;
        payroll.PAYCHECK_DATE= $filter('date')(payroll.PAYCHECK_DATE, "MM-dd-yyyy");

      });
    });
  };


  $scope.selectPayroll = function(payroll) {
    console.log(payroll);
    //if selected View All Payrolls
    if(payroll === null) {
      $scope.payrollDetails=false;
      $scope.payrollAllDetails=true;
      // $scope.getAllPayroll();
    } else {
      $scope.payrollAllDetails=false;
      $scope.payrollDetails=true;

      $scope.payroll=payroll;


      //
      // $scope.payroll.visits.forEach(function(visit) {
      //   //get visitType desc for a visit
      //   for (var i = 0; i < $scope.visitTypes.length; i++) {
      //     if (visit.VISITTYPE === $scope.visitTypes[i].KEYID) {
      //         visit.VISITTYPEDESC = $scope.visitTypes[i].DESC;
      //         visit.VISITTYPEDISC = $scope.visitTypes[i].DISCIPLINE;
      //     }
      //   }
      //   //get patient for a visit
      //   dataService.get('patient', {KEYID:visit.PATKEYID}).then(function(data){
      //     var patient = data.data[0];
      //     //adding visit type desc to visit object
      //     visit.VISITPATIENT = patient.LNAME + ', ' + patient.FNAME;
      //   });
      //   //get TOTAL
      //   visit.VISITTOTAL = (visit.HOURS) * (visit.UNITCHARGE);
      // });

      // var HOURSEMPTOTAL = 0;
      // var PAYEMPTOTAL = 0;
      // for (var i = 0; i < $scope.payroll.Visits.length; i++) {
      //   HOURSEMPTOTAL += parseInt($scope.payroll.Visits[i].HOURS);
      //   PAYEMPTOTAL += parseInt($scope.payroll.Visits[i].VISITTOTAL);
      // }
      // $scope.payroll.HOURSEMPTOTAL = HOURSEMPTOTAL;
      // $scope.payroll.PAYEMPTOTAL = PAYEMPTOTAL;
    }
  };

  // //sorting all visits for employee into payroll periods
  // dataService.get('visit', {'EMPKEYID': $scope.employee.KEYID}).then(function(data){
  //   var visitsArr = data.data;
  //
  //   $scope.payrolls.forEach(function(payroll) {
  //       var start = moment(payroll.STARTDATE);
  //       var end = moment(payroll.ENDDATE);
  //       var periodVisits = [];
  //
  //       visitsArr.forEach(function(visit) {
  //         var visitDate= moment(visit.VISITDATE);
  //         if (visitDate.isBetween(start, end, null, '[]')) {
  //           periodVisits.push(visit);
  //         }
  //       });
  //       payroll.VISITS = periodVisits;
  //
  //       var HOURSEMPTOTAL = 0;
  //       var PAYEMPTOTAL = 0;
  //       for (var i = 0; i < payroll.VISITS.length; i++) {
  //         HOURSEMPTOTAL += parseInt(payroll.VISITS[i].HOURS);
  //         PAYEMPTOTAL += parseInt(payroll.VISITS[i].VISITTOTAL);
  //       }
  //       payroll.HOURSEMPTOTAL = HOURSEMPTOTAL;
  //       payroll.PAYEMPTOTAL = PAYEMPTOTAL;
  //   });
  //   getCurrentPayPeriod();
  // });
  //
  // //sample data
  // $scope.payrolls = [
  //   {
  //     KEYID: 1,
  //     VISITS: [],
  //     PAID: 1,
  //     STARTDATE:"01-01-2017",
  //     ENDDATE:"01-15-2017"
  //   },
  //   {
  //     KEYID: 2,
  //     VISITS: [],
  //     PAID: 0,
  //     STARTDATE:"01-16-2017",
  //     ENDDATE:"01-31-2017"
  //   },
  //   {
  //     KEYID: 3,
  //     VISITS: [],
  //     PAID: 0,
  //     STARTDATE:"02-01-2017",
  //     ENDDATE:"02-15-2017"
  //   }
  // ];
  //
  // //finding current payroll period
  // function getCurrentPayPeriod() {
  //   var now = moment();
  //   // $scope.payrolls.forEach(function(period) {
  //   //   var startPeriod = moment(period.STARTDATE, 'MM-DD-YYYY');
  //   //   var endPeriod = moment(period.ENDDATE,'MM-DD-YYYY');
  //   //   if(now.isBetween(startPeriod, endPeriod, 'day', '[]')) {
  //   //     $scope.STARTDATE = new Date($filter("date")(period.STARTDATE, 'yyyy-MM-dd'));
  //   //     $scope.ENDDATE = new Date($filter("date")(period.ENDDATE, 'yyyy-MM-dd'));
  //   //   }
  //   // });
  //
  //   //get all visitTypes
  //   dataService.getAll('visitType').then(function(data) {
  //     $scope.visitTypes = data.data;
  //     $scope.payrolls.forEach(function(payroll) {
  //       //get visits for all payrolls
  //       payroll.VISITS.forEach(function(visit) {
  //         //get visitType desc for a visit
  //         for (var i = 0; i < $scope.visitTypes.length; i++) {
  //           if (visit.VISITTYPE === $scope.visitTypes[i].KEYID) {
  //               visit.VISITTYPEDESC = $scope.visitTypes[i].DESC;
  //               visit.VISITTYPEDISC = $scope.visitTypes[i].DISCIPLINE;
  //           }
  //         }
  //         //get patient for a visit
  //         dataService.get('patient', {KEYID:visit.PATKEYID}).then(function(data){
  //           var patient = data.data[0];
  //           //adding visit type desc to visit object
  //           visit.VISITPATIENT = patient.LNAME + ', ' + patient.FNAME;
  //         });
  //         //get TOTAL
  //         visit.VISITTOTAL = (visit.HOURS) * (visit.UNITCHARGE);
  //       });
  //     });
  //   });
  // }
  //
  $scope.printSection = function (divName) {
    var printContents = document.getElementById(divName).innerHTML;
    console.log(printContents)
    // var originalContents = document.body.innerHTML;

    if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
        var popupWin = window.open('', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWin.window.focus();
        popupWin.document.write('<!DOCTYPE html><html><head>' +
            '<link rel="stylesheet" type="text/css" href="style.css" />' +
            '</head><body onload="window.print()"><div class="reward-body">' + printContents + '</div></html>');
        popupWin.onbeforeunload = function (event) {
            popupWin.close();
            return '.\n';
        };
        popupWin.onabort = function (event) {
            popupWin.document.close();
            popupWin.close();
        }
    } else {
        var popupWin = window.open('', '_blank', 'width=800,height=600');
        popupWin.document.open();
        popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
        popupWin.document.close();
    }
    popupWin.document.close();

    return true;
  };

  $scope.payrollAllDetails=true;
  $scope.getAllPayroll();
});
