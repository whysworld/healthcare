app.config(function($stateProvider) {
  $stateProvider.state('patientCreate', {
    url: '/patientCreate',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/patientCreate/patientCreate.html',
        controller: 'PatientCreateController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
