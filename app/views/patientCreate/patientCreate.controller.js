app.controller('PatientCreateController', function($scope, $filter, $state, $window, $uibModal, dataService, patientService) {

  $scope.allDoctors = [];
  $scope.filteredDoctors = $scope.npiSearchText ? $scope.allDoctors.filter($scope.createFilterFor($scope.npiSearchText)) : $scope.allDoctors;

  $scope.getDoctors = function() {
    dataService.getAll('doctor').then(function(data) {
      $scope.allDoctors = data.data.map(function(doctor) {
        doctor.value = doctor.NPI + " " + doctor.FNAME.toLowerCase() + " " + doctor.LNAME.toLowerCase() + " " + doctor.CITY.toLowerCase();
        return doctor;
      });
    });
  };

  $scope.hideInfo=function () {
    //check to make sure INS2CO not same as INS1CO
    if ($scope.INSCO===$scope.INS2CO){
      $scope.isDuplicatePrimary=true;
      $scope.INSCO=null;
    } else {
      $scope.isDuplicatePrimary=false;
      //if selected insurance is Medicare, hide auth code, plan, group
      if($scope.INSCO===1){
        $scope.hideInsuranceInfo=true;
      } else {
        $scope.hideInsuranceInfo=false;
      }
    }
  };

  $scope.hide2Info=function () {
    //check to make sure INS2CO not same as INS1CO
    if ($scope.INS2CO===$scope.INSCO){
      $scope.isDuplicate=true;
      $scope.INS2CO=null;
    } else {
      $scope.isDuplicate=false;
      //if selected insurance is Medicare, hide auth code, plan, group
      if($scope.INS2CO===1){
        $scope.hideInsurance2Info=true;
      } else {
        $scope.hideInsurance2Info=false;
      }
    }
  };

  //clear secondary insurance fields if add secondary insurance checkbox is unchecked
  $scope.isSecondaryInsuranceChecked=function () {
    if($scope.secondaryInsuranceCheckbox===false) {
      $scope.INS2CO=null;
      $scope.INS2ID=null;
      $scope.INS2PLAN=null;
      $scope.INS2GROUP=null;
      $scope.INS2AUTHCODE=null;
      $scope.INS2NOTES=null;
    }
  };

  $scope.getDoctors();

  $scope.npiSearchTextChange = function() {
    // console.log("Query: " + query);
    // $scope.getDoctors();
    $scope.filteredDoctors = $scope.npiSearchText ? $scope.allDoctors.filter($scope.createFilterFor($scope.npiSearchText)) : $scope.allDoctors;
  };

  $scope.selectedDoctorChange = function(doctor) {
    console.log("selected doctor changed:");
    console.log(doctor);

    $scope.physicianFNAME = doctor.FNAME;
    $scope.physicianMINITIAL = doctor.MINITIAL;
    $scope.physicianLNAME = doctor.LNAME;
    $scope.physicianAddress = doctor.ADDRESS;
    $scope.physicianCity = doctor.CITY;
    $scope.physicianPhone = doctor.PHONE;
    $scope.physicianState = doctor.STATE;
    $scope.physicianFax = doctor.FAX;
    $scope.physicianZip = doctor.ZIP;
    $scope.physicianEmail = doctor.EMAIL;
    $scope.physicianNotes = doctor.NOTES;
    $scope.physicianF2F = doctor.F2F;
    $scope.physicianPOC= doctor.POC;
  }

  $scope.selectPhysician = function(doctor) {
    console.log("Selected physician");
    console.log(doctor);

    $scope.npiSearchText = "";
    $scope.selectedDoctor = doctor;
  }

  $scope.createFilterFor = function(query) {
    var lowercaseQuery = angular.lowercase(query);

    return function filterFn(item) {
      return (item.value.indexOf(lowercaseQuery) > -1);
    };
  }

  $scope.addDoctor = function(event, addRefPhysician) {
    console.log("addDoctor");
    console.log(addRefPhysician)
    event.preventDefault();
    var modalInstance = $uibModal.open({
      controller: 'SearchNppesDoctorController',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/searchNppesDoctor/searchNppesDoctor.html',
      size: 'lg',
      scope: $scope,
      resolve: {
        addRefPhysician: addRefPhysician
      }
    });
    modalInstance.result.then(function (doctor) {
      console.log(doctor);
      //if adding referral physician
      if (addRefPhysician===true) {
        //populate list of doctors
        dataService.getAll('doctor').then(function(data) {
          $scope.doctors=data.data;
          //find the newly added doctor in the db and the keyid
          doctor.KEYID = $scope.doctors.find(x = x.NPI===doctor.NPI.toString()).KEYID;
          //assign the doctor to as the selectd REFERRPHYSICIAN
          $scope.REFERRPHYSICIAN=doctor.KEYID;
          //sort the doctors dropdown list
          $scope.doctors.sort(function(a, b){
            if(a.FNAME < b.FNAME) return -1;
            if(a.FNAME > b.FNAME) return 1;
            return 0;
          });
        });
      } else { //if adding physician
      // var md=doctor
        $scope.getDoctors();
        // md.NPI=doctor.NPI.toString();
        // $scope.selectedDoctor=doctor;
        // console.log($scope.selectedDoctor)
        $scope.selectedDoctorChange(doctor);
        $scope.npiSearchTextChange();
        // $scope.npiSearchTextChange()
        // filterPhysiciansbasedonthesearchtext();
      }
    }, function() {
        // This is called when the modal is dismissed using the dismiss() method (like when clicking the X button?)
      //  doWeWantToDoSomethingHere(?);
    });
  };

  $scope.disableEditPhysician = true;

  $scope.editPhysician = function() {
    $scope.disableEditPhysician=!$scope.disableEditPhysician;
  };

  $scope.savePhysicianChanges = function() {
    $scope.disableEditPhysician=!$scope.disableEditPhysician;
    console.log($scope.selectedDoctor);
    var physicianChanges = {
      KEYID: $scope.selectedDoctor.KEYID,
      NPI: $scope.selectedDoctor.NPI,
      LNAME: $scope.physicianLNAME,
      FNAME: $scope.physicianFNAME,
      MINITIAL: $scope.physicianMINITIAL,
      ADDRESS: $scope.physicianAddress,
      CITY: $scope.physicianCity,
      STATE: $scope.physicianState,
      ZIP: $scope.physicianZip,
      EMAIL: $scope.physicianEmail,
      PHONE: $scope.physicianPhone,
      FAX: $scope.physicianFax,
      F2F: $scope.physicianF2F,
      POC: $scope.physicianPOC,
      NOTES: $scope.physicianNotes,
    };
    // console.log(physicianChanges)
    dataService.edit('doctor', physicianChanges).then(function(response) {
      console.log(response);
    });
  };


  $scope.getICDCodes=function(val) {
    console.log('getting icd codes');
    return dataService.search('ICD10Code', {CODE:val, DESCRIPTION:val}).then(function (data) {
      $scope.codes=$filter('limitTo')(data.data, 15);
      console.log('$scope.codes',$scope.codes);
      return $scope.codes.map(function(code) {
        return code;
      });
    });
  };

  $scope.findDIAGDESC = function(field, descField) {
    var codes = $scope.codes;
    for (i=0; i<codes.length; i++) {
      if ($scope[field] === codes[i].CODE) {
        $scope[descField] = codes[i].DESCRIPTION;
        break;
      }
    }
  };



  $scope.currentForm = 'Demographic';


  $scope.nextForm = function(current, destination) {
    // if ($scope.demographicSectionComplete===true) {
    //   if ($scope.currentForm != destination) {
    //     $scope.currentForm = destination;
    //     $('.' + current ).fadeOut('fast', function() {
    //       $('.' + destination ).fadeIn('fast');
    //     });
    //   }
    // } else if (destination!=='Demographic'){
    //   alert('Please complete Demographic section first.')
    // }

    //testing
    $scope.currentForm = destination;
       $('.' + current ).fadeOut('fast', function() {
         $('.' + destination ).fadeIn('fast');
       });
  };

  $scope.activeForm = function(form) {
    if ($scope.currentForm == form) {
      return true;
    }
  };

  $scope.submitSection = function(form, destination) {
    console.log(form.$submitted);
    console.log(form.$valid);
    if(form.$valid) {
      savePatient(destination)

    }
    else {
      $('#' + $scope.currentForm + ' i').css({color: 'lightgrey'});
      console.log('Not Valid');
    }
  };

$scope.physicians = [];

$scope.addPhysician = function() {
  $scope.physicians.push({});
};
$scope.removePhysician = function(index) {
  $scope.physicians.splice(index, 1);
};


$scope.additionalAddress = function(secondaryAddress) {
  $scope.secondaryAddress = secondaryAddress;
  if(secondaryAddress === 'Yes') {
    $scope.showSecondayAddress = true;
  }
  else {
    $scope.showSecondayAddress = false;
  }
};

$scope.specialists = [];
$scope.addSpecialist = function() {
  $scope.specialists.push({});
}
$scope.removeSpecialist = function(index) {
  $scope.specialists.splice(index, 1);
};
$scope.populateNames = function() {
  document.getElementById('managerName').disabled=false;
};

$scope.referralStatus = 'Pending';
$scope.REFERRDATE = new Date();

// function saveSection(form, currentForm){
//   // create patient
//   var newPatient = {
//     //******** DEMOGRPHAICS ********//
//     FNAME: $scope.FNAME,
//     DOB: $scope.DOB,
//     MINITIAL: $scope.MINITIAL,
//     GENDER: $scope.GENDER,
//     LNAME: $scope.LNAME,
//     SSN: $scope.SSN,
//     MARITAL: $scope.MARITAL,
//     LANGUAGE: $scope.LANGUAGE,
//     M0140: $scope.M0140,
//     RELIGION: $scope.RELIGION,
//     NOTES: $scope.NOTES,
//     PATSTATUS: "P",
//     LSTADMIT: 1,
//     //******** CONTACT INFO ********//
//     LOCTYPE: $scope.LOCTYPE,
//     ADDRESS: $scope.ADDRESS,
//     CITY: $scope.CITY,
//     STATE: $scope.STATE,
//     ZIP: $scope.ZIP,
//     PHONE1: $scope.PHONE1,
//     CELLPHONE: $scope.CELLPHONE,
//     EMAIL: $scope.EMAIL,
//     ADDRESS_NOTES: $scope.ADDRESS_NOTES,
//     LOCTYPE2: $scope.LOCTYPE2,
//     ADDRESS2: $scope.ADDRESS2,
//     CITY2: $scope.CITY2,
//     STATE2: $scope.STATE2,
//     ZIP2: $scope.ZIP2,
//     PHONE2: $scope.PHONE2,
//     //******** INSURANCE ********//
//     INSCO: $scope.INSCO,
//     INSID: $scope.INSID,
//     //******** HISTORY ********//
//     HISTORY: $scope.HISTORY,
//     //******** ALLERGIES ********//
//     ALLERGIES: $scope.ALLERGIES
//   };
//   console.log('newPatient', newPatient);
//   if (currentForm ==='Demographic') {
//     dataService.add('patient', newPatient).then(function(response) {
//       console.log(response);
//       // createAdmit(response);
//       // addDoctorInfo();
//       // dataService.get('patient', {'KEYID': response.keyid}).then(function(patient){
//       //   var patient = patient.data[0];
//       //   patientService.set(patient);
//       //   $scope.patient = patient;
//       //   console.log("saved patient", patient);
//       // });
//     });
//   } else {
//     //continue editing patient info
//     alert('s')
//
//   }
//
// }

function savePatient(destination) {

  var newPatient = {
    //******** DEMOGRPHAICS ********//
    FNAME: $scope.FNAME,
    DOB: $filter("date")($scope.DOB, 'yyyy-MM-dd'),
    MINITIAL: $scope.MINITIAL,
    GENDER: $scope.GENDER,
    LNAME: $scope.LNAME,
    SSN: $scope.SSN,
    MARITAL: $scope.MARITAL,
    LANGUAGE: $scope.LANGUAGE,
    M0140: $scope.M0140,
    RELIGION: $scope.RELIGION,

    NOTES: $scope.NOTES,
    PATSTATUS: "P",
    //******** CONTACT INFO ********//
    LOCTYPE: $scope.LOCTYPE,
    //county: $scope.county,
    ADDRESS: $scope.ADDRESS,
    CITY: $scope.CITY,
    STATE: $scope.STATE,
    ZIP: $scope.ZIP,
    //country: $scope.country,
    PHONE1: $scope.PHONE1,
    CELLPHONE: $scope.CELLPHONE,
    EMAIL: $scope.EMAIL,
    ADDRESS_NOTES: $scope.ADDRESS_NOTES,
    // LOCTYPE2: $scope.LOCTYPE2,
    // ADDRESS2: $scope.ADDRESS2,
    // CITY2: $scope.CITY2,
    // STATE2: $scope.STATE2,
    // ZIP2: $scope.ZIP2,
    //secondaryCountry = $scope.secondaryCountry,
    // PHONE2: $scope.PHONE2,
    //secondaryCell = $scope.secondaryCell,
    //secondaryEmail = $scope.secondaryEmail,
    //secondaryAddressNotes = $scope.secondaryAddressNotes,

    //******** INSURANCE ********//
    INSCO: $scope.INSCO,
    // INS1PLAN: $scope.INS1PLAN,
    INSID: $scope.INSID,
    // INS1GROUP: $scope.INS1GROUP,
    // INS1AUTHCODE: $scope.INS1AUTHCODE,
    // INS1NOTES: $scope.INS1NOTES,
    // INS2CO: $scope.INS2CO,
    // INS2ID: $scope.INS2ID,
    // INS2GROUP: $scope.INS2GROUP,
    // INS2AUTHCODE: $scope.INS2AUTHCODE,
    // INS2NOTES: $scope.INS2NOTES,

    //******** HISTORY ********//
    HISTORY: $scope.HISTORY,

    //******** ALLERGIES ********//
    ALLERGIES: $scope.ALLERGIES,

  };

  if ($scope.secondaryAddressCheckbox) {
    console.log('asfsa')
    newPatient.LOCTYPE2=$scope.LOCTYPE2;
    newPatient.ADDRESS2=$scope.ADDRESS2;
    newPatient.CITY2=$scope.CITY2;
    newPatient.STATE2=$scope.STATE2;
    newPatient.ZIP2=$scope.ZIP2;
    newPatient.PHONE2=$scope.PHONE2;
  } else {
    newPatient.LOCTYPE2="";
    newPatient.ADDRESS2="";
    newPatient.CITY2="";
    newPatient.STATE2="";
    newPatient.ZIP2="";
    newPatient.PHONE2="";
  }
  console.log('newPatient', newPatient);

  if ($scope.currentForm === 'Demographic') {
    dataService.get('patient', {SSN:$scope.SSN}).then(function (data) {
      console.log(data);
      if (data.data.length===0) {
        console.log('patient does not exist');
        dataService.add('patient', newPatient).then(function(response) {
          console.log(response);
          if(response.status==='success') {
            dataService.edit('patient', {KEYID:response.keyid, PATIENTID: response.keyid}).then(function (response) {
              $scope.demographicSectionComplete=true;
              $('#' + $scope.currentForm + ' i').css({color: 'green'});
              var section = $scope.currentForm + 'Complete';

              $scope.nextForm($scope.currentForm, destination);
              $scope[section] = true;
            });
          }
          createAdmit(response);
        });
      } else {
        alert('Patient already exists.');
      }
    });
  } else {
    console.log('editing existing patient');
    $scope.demographicSectionComplete=true;
    $('#' + $scope.currentForm + ' i').css({color: 'green'});
    var section = $scope.currentForm + 'Complete';

    $scope.nextForm($scope.currentForm, destination);
    $scope[section] = true;

    patient = patientService.get();
    newPatient.KEYID = patient.KEYID;
    console.log(newPatient)
    dataService.edit('patient', newPatient).then(function(response) {
      console.log(response);
      editAdmit(patient);
      if ($scope.selectedDoctor !== null) {
        addDoctorInfo();
      }
    });
  }
}

function addDoctorInfo() {
  dataService.edit('doctor', {
      'KEYID': $scope.selectedDoctor.KEYID,
      'F2F': $scope.physicianF2F,
      'POC': $scope.physicianPOC,
      'NOTES': $scope.physicianNotes,
    }).then(function(response){
      console.log(response);
    });
}



  function createAdmit(response) {
    var newAdmit = {
      PATKEYID: response.keyid,
      PATSTATUS: "P",
      ADMITNO: 1,
      ACUITY: $scope.ACUITY,
    };

    if ($scope.primaryCheckbox) {
      newAdmit.PCGNAME=$scope.PCGNAME;
      newAdmit.PCGNOTES=$scope.PCGNOTES;
      newAdmit.PCGPHONE=$scope.PCGPHONE;
    } else {
      newAdmit.PCGNAME="";
      newAdmit.PCGNOTES="";
      newAdmit.PCGPHONE="";
    }

    if ($scope.emergencyCheckbox) {
      newAdmit.ECNAME=$scope.ECNAME;
      newAdmit.ECNOTES=$scope.ECNOTES;
      newAdmit.ECPHONE=$scope.ECPHONE;
    } else {
      newAdmit.ECNAME="";
      newAdmit.ECNOTES="";
      newAdmit.ECPHONE="";
    }

    console.log('newAdmitObj', newAdmit);
    //adding new admit
    dataService.add('admission', newAdmit).then(function(response) {
      console.log(response);
      //once new admit added, editing patient to include LSTADMIT field
      dataService.edit('patient', {'KEYID': newAdmit.PATKEYID, 'LSTADMIT': response.keyid}).then(function(response){
        console.log(response);
        //resetting patient
        dataService.get('patient', {'KEYID': newAdmit.PATKEYID}).then(function(patient){
          patient = patient.data[0];
          patientService.set(patient);
          $scope.patient = patient;
          console.log("saved patient", patient);
        });
      });
    });
  }

  function editAdmit(patient) {
    var editAdmit = {
      KEYID: patient.LSTADMIT,
      PATKEYID: patient.KEYID,
      PATSTATUS: "P",
      LOCTYPE: $scope.LOCTYPE,
      ADMITNO: 1,

      //county: $scope.county,
      ADDRESS: $scope.ADDRESS,
      CITY: $scope.CITY,
      STATE: $scope.STATE,
      ZIP: $scope.ZIP,

      //primary care giver
      // PCGNAME: $scope.PCGNAME,
      // PCGPHONE: $scope.PCGPHONE,
      // PCGNOTES: $scope.PCGNOTES,
      //
      // //emergency contact
      // ECNAME: $scope.ECNAME,
      // ECPHONE: $scope.ECPHONE,
      // ECNOTES: $scope.ECNOTES,
      //country: $scope.country,
      PHONE1: $scope.PHONE1,
      INS1CO: $scope.INSCO,
      INS1ID: $scope.INSID,
      INS1PLAN: $scope.INS1PLAN,
      INS1GROUP: $scope.INS1GROUP,
      INS1AUTHCODE: $scope.INS1AUTHCODE,
      INS1NOTES: $scope.INS1NOTES,
      // INS2CO: $scope.INS2CO,
      // INS2ID: $scope.INS2ID,
      // INS2GROUP: $scope.INS2GROUP,
      // INS2AUTHCODE: $scope.INS2AUTHCODE,
      // INS2PLAN: $scope.INS2PLAN,
      // INS2NOTES: $scope.INS2NOTES,
      //******** REFERRAL ********//
      REFRECEIVED: $scope.REFRECEIVED,
      REFERRSRC: $scope.REFERRSRC,
      REFERRDATE: $filter("date")($scope.REFERRDATE, 'yyyy-MM-dd'),
      //referralSourceInd: $scope.referralSourceInd,
      REFERRPHYSICIAN: $scope.REFERRPHYSICIAN,
      ADMITSRCE: $scope.ADMITSRCE,
      // caretype:??
      REFERRNOTES: $scope.REFERRNOTES,
      SERVICE_SN: $scope.SERVICE_SN,
      SERVICE_PT: $scope.SERVICE_PT,
      SERVICE_OT: $scope.SERVICE_OT,
      SERVICE_ST: $scope.SERVICE_ST,
      SERVICE_MS: $scope.SERVICE_MS,

      //******** PHYSICIAN ********//
      // DOCTOR: $scope.selectedDoctor.KEYID,
      DOCTOR2: null,
      DOCTOR3: null,
      DOCTOR4: null,
      DOCTOR5: null,
      PHARMACY: null,
      DME: null,
      CASEMNGR: null,
      DISCRN: null,
      DISCLPN: null,
      DISCHA: null,
      DISCPT: null,
      DISCMS: null,
      DISCOT: null,
      DISCST: null,
      DISCRD: null,
      MKTSOURCE: null,

      //******** DIAGNOSIS ********//
      //ICD?
      DIAGCODE: $scope.DIAGCODE,
      DIAGDESC: $scope.DIAGDESC,
      DIAGONSET: $filter("date")($scope.DIAGONSET, 'yyyy-MM-dd'),
      DIAGCODE2: "",
      DIAGDESC2: "",
      DIAGONSET2: "",
      DIAGCODE3: "",
      DIAGDESC3: "",
      DIAGONSET3: "",
      DIAGCODE4: "",
      DIAGONSET4: "",
      DIAGDESC4: "",
      DIAGCODE5: "",
      DIAGONSET5: "",
      DIAGDESC5: "",
      DIAGCODE6: "",
      DIAGONSET6: "",
      DIAGDESC6: "",

      // SURGCODE: $scope.SURGCODE,
      // SURGDESC: $scope.SURGDESC,
      // SURGONSET: $scope.SURGONSET,

      //******** HISTORY ********//
      HISTORY: $scope.HISTORY,

      //******** ALLERGIES ********//
      ALLERGIES: $scope.ALLERGIES,
    };

    if ($scope.selectedDoctor !== null) {
        editAdmit.DOCTOR = $scope.selectedDoctor.KEYID;
    }

    if ($scope.primaryCheckbox) {
      editAdmit.PCGNAME=$scope.PCGNAME;
      editAdmit.PCGNOTES=$scope.PCGNOTES;
      editAdmit.PCGPHONE=$scope.PCGPHONE;
    } else {
      editAdmit.PCGNAME="";
      editAdmit.PCGNOTES="";
      editAdmit.PCGPHONE="";
    }

    if ($scope.emergencyCheckbox) {
      editAdmit.ECNAME=$scope.ECNAME;
      editAdmit.ECNOTES=$scope.ECNOTES;
      editAdmit.ECPHONE=$scope.ECPHONE;
    } else {
      editAdmit.ECNAME="";
      editAdmit.ECNOTES="";
      editAdmit.ECPHONE="";
    }

    // if ($scope.secondaryInsuranceCheckbox) {
    //   editAdmit.INS2CO=$scope.INS2CO;
    //   editAdmit.INS2ID=$scope.INS2ID;
    //   editAdmit.INS2PLAN=$scope.INS2PLAN;
    //   editAdmit.INS2GROUP=$scope.INS2GROUP;
    //   editAdmit.INS2AUTHCODE=$scope.INS2AUTHCODE;
    //   editAdmit.INS2NOTES=$scope.INS2NOTES;
    // } else {
    //   editAdmit.INS2CO="";
    //   editAdmit.INS2ID="";
    //   editAdmit.INS2PLAN="";
    //   editAdmit.INS2GROUP="";
    //   editAdmit.INS2AUTHCODE="";
    //   editAdmit.INS2NOTES="";
    // }

    //get icdarray
    $scope.addICDArray.forEach(function (icdObj) {
      Object.keys($scope.ICDCODES).forEach(function(key) {
        console.log(key)
        if (icdObj.code===key) {
          console.log($scope.ICDCODES[key]);
          icdObj.codeNum=$scope.ICDCODES[key];
        }
        if (icdObj.desc===key) {
          console.log($scope.ICDCODES[key]);
          icdObj.descDesc=$scope.ICDCODES[key];
        }
        if (icdObj.date===key) {
          console.log($scope.ICDCODES[key]);
          icdObj.dateDesc=$scope.ICDCODES[key];
        }
      });
    });
    console.log($scope.addICDArray);

    if ($scope.addICDArray) {
      for (var i = 0; i < $scope.addICDArray.length; i++) {
        var code='DIAGCODE'+(i+2);
        var desc='DIAGDESC'+(i+2);
        var date='DIAGONSET'+(i+2);
        editAdmit[code]=$scope.addICDArray[i].codeNum;
        editAdmit[desc]=$scope.addICDArray[i].descDesc;
        editAdmit[date]=$filter("date")($scope.addICDArray[0].dateDesc, 'yyyy-MM-dd');
      }
    }

    console.log('editAdmit', editAdmit);
    dataService.edit('admission', editAdmit).then(function(response) {
      console.log(response);
    });
  }

  dataService.getAll('employee').then(function(data) {
    $scope.employees = data.data;
  });


  dataService.getAll('doctor').then(function(data) {
    $scope.doctors=data.data;
    $scope.doctors.sort(function(a, b){
      if(a.FNAME < b.FNAME) return -1;
      if(a.FNAME > b.FNAME) return 1;
      return 0;
    });
  });
  dataService.getAll('language').then(function(data) {
    $scope.languages = data.data;
  });
  dataService.getAll('religion').then(function(data) {
    $scope.religions = data.data;
  });
  dataService.getAll('insurance').then(function(data) {
    $scope.insurances = data.data;
  });
  dataService.getAll('referralSource').then(function(data) {
    $scope.referralSources = data.data;
  });


  //patient must be 18 yrs or older and less than 140 years
  var dob18=moment(new Date).subtract(18, 'years');
  var dob140=moment(new Date).subtract(140, 'years');

  $scope.inlineOptionsDOB = {
    maxDate: new Date($filter("date")(dob18, 'yyyy/MM/dd')),
    minDate: new Date($filter("date")(dob140, 'yyyy/MM/dd')),
    initDate: new Date($filter("date")(dob18, 'yyyy/MM/dd')),
  };

  //close 'x' icon
  $scope.close = function () {
    $state.go('patientDashboard');
  };


  $scope.$on('$stateChangeStart', function(event) {
        var answer = confirm("Are you sure you want to leave this page? Any unsaved changes will be lost.")
        if (!answer) {
            event.preventDefault();
        }
    });


  //get icd desc for all other added icd codes
  $scope.updateICD = function(icd) {
    // console.log($scope.ICDCODES)
    // console.log(icd)
    var ICDcodes = $scope.codes;
    // console.log(ICDcodes)

    Object.keys($scope.ICDCODES).forEach(function(key) {
      for (i=0; i<ICDcodes.length; i++) {
        if ($scope.ICDCODES[key] == ICDcodes[i].CODE) {
          // console.log($scope.ICDCODES[key])
          // console.log(ICDcodes[i].DESCRIPTION)
          var desc='DESC'+ icd.index.toString();
          $scope.ICDCODES[desc]=ICDcodes[i].DESCRIPTION;
          // $scope.add
          // console.log($scope.ICDCODES)
        }
      }
    });
  };

  //adding additional icd codes
  var index=1;
  $scope.addICDArray=[]
  $scope.ICDCODES={}
  $scope.addICD=function() {
    console.log('adding icd')
    index+=1;
    // var showICD='addICD' + $scope.counterArray[index];
    // $scope[showICD]=true;
    //
    // $scope.counterArray.splice(index, 1);
    // console.log($scope.counterArray);
    var icdObj={
      // update: "ICD"+index.toString(),
      code:"CODE"+index,
      // loading: "ICD"+index.toString()+"Codes",
      // noResults: "Results"+index.toString(),
      date: "DATE" + index,
      // // openDate: "openedICDDATE"+index.toString(),
      desc: "DESC"+index.toString(),
      index:index.toString()
      // code:
    }

    $scope.addICDArray.push(icdObj);
    console.log('adding $scope.addICDArray', $scope.addICDArray)
  };

  //deleting added icd code
  $scope.deleteICD=function(icd) {
    console.log('deleting', icd)
    // console.log($scope.addICDArray);
    var i=$scope.addICDArray.indexOf(icd);
    // console.log(i)
    $scope.addICDArray.splice(i, 1);
    console.log($scope.addICDArray);
  }

});
