app.controller('NewVisitController', function($scope, $filter, $uibModalInstance, selected, dataService, patient, patientService, episode, action) {

  // $scope.visits = visits
  $scope.patient = patient;
  $scope.episode = episode;
  console.log('episode:', $scope.episode);

  $scope.action=action;
  console.log(action);
  //set visit date if selected
  if (selected !== null){
    $scope.selected = new Date($filter("date")(selected, 'yyyy-MM-dd'));
  } else {
    $scope.selected = null;
    if ($scope.patient.PATSTATUS==='T') {
      dataService.get('form', {ADMITKEYID:$scope.episode.ADMITKEYID, EPIKEYID:$scope.episode.KEYID, FORMTYPE:6}).then(function (data) {
        var formdate = data.data[0].FORMDATE;
        $scope.inlineOptions = {
          minDate: new Date($filter("date")(formdate, 'yyyy/MM/dd')),
          maxDate: new Date($filter("date")($scope.episode.THRUDATE, 'yyyy/MM/dd'))
        };
      });
    } else {
      $scope.inlineOptions = {
        minDate: new Date($filter("date")($scope.episode.FROMDATE, 'yyyy/MM/dd')),
        maxDate: new Date($filter("date")($scope.episode.THRUDATE, 'yyyy/MM/dd'))
      };
    }
  }

  //if patient is trying to discharge, allow date selection
  if(action === 'D') {
    $scope.lockDate = false;
    // $scope.showDCreason = true;
  } else if (action === 'T') {
    $scope.lockDate = false;
  } else if (action ==='A' && $scope.patient.PATSTATUS==='T'){
      $scope.lockDate = false;
  } else {
    $scope.lockDate=true;
    // $scope.showDCreason = false;
  }

  getEpisode();
  function getEpisode() {
    if ($scope.episode === undefined) {
      $scope.epikeyid = null;
    } else {
      $scope.epikeyid = $scope.episode.KEYID;
    }
  }

  getLastFiveDays();
  function getLastFiveDays() {
    if ($scope.episode !== undefined) {
      $scope.endOfEpisode = moment($scope.episode.THRUDATE);
      $scope.lastFiveDays = moment($scope.endOfEpisode.clone().subtract(5, 'days'));
    }
  }

  console.log("Selected:", $scope.selected);

  // Get all employees details
  dataService.getAll('employee').then(function(data) {
    $scope.employees = data.data;
  });



  // Get all the visits for patient's lastadmit
  dataService.get('visit', {'ADMITKEYID': $scope.patient.LSTADMIT}).then(function(data){
    var visits = data.data;
    // Get all the active visit types
    dataService.get('visitType', {'ACTIVE': 1}).then(function(data){
      var visitTypes = data.data;
      $scope.visitTypes = visitTypes;
      // // If there is no scheduled visit for the patient
      // if (visits.length === 0) {
      //   $scope.visitTypes = visitTypes;
      // // If there are visits for the patient
      // } else {
      //   for (var i = 0; i < visits.length; i++) {
      //     // If this is a start of care visit (VISITTYPE === 1) already scheduled, don't show SOC visit types
      //     if (visits[i].VISITTYPE === 1) {
      //       var index = visitTypes.findIndex(x => x.KEYID===1);
      //       visitTypes.splice(index, 1);
      //       $scope.visitTypes = visitTypes;
      //     } else {
      //       $scope.visitTypes = visitTypes;
      //     }
      //     // console.log($scope.visitTypes);
      //   } console.log($scope.visitTypes);
      // }

      //if patient is not trying to discharge,
      // if (action !== 'D') {
      //   // if selected date is not within last 5 days of episode
      //   if (selected.isBetween($scope.lastFiveDays, $scope.endOfEpisode, null, '[)') !== true) {
      //     //remove recertification visittype
      //     for (var j = 0; j < $scope.visitTypes.length; j++) {
      //       if ($scope.visitTypes[j].KEYID === 84) {
      //         var index = $scope.visitTypes.indexOf($scope.visitTypes[j]);
      //         $scope.visitTypes.splice(index, 1);
      //       }
      //     }
      //   }
      // } else {//if patient trying to discharge, set visittype to discharge, and populate forms
      //   var index = visitTypes.findIndex(x => x.DESC==='Discharge');
      //   $scope.VISITTYPE = visitTypes[index];
      //   $scope.lockVisitType = true;
      //   $scope.updateVisitTypeDetails()
      // }
    });
  });

  //hiding SOC form in dropdown
  // $scope.hideSOC = function (form) {
  //   return form.NAME !== 'OASIS-C2 SOC v. 2.20.1';
  // };



  dataService.get('form', {ADMITKEYID:$scope.patient.LSTADMIT}).then(function(data) {
    var forms = data.data;
    console.log('forms', forms)

    // Get all the available form types
    dataService.getAll('formType').then(function(data){
      var formTypes = data.data;
      //find SOC form
      var index = formTypes.findIndex(x => x.KEYID===2);
      $scope.socForm = formTypes[index];
      //find discharge form
      var j = formTypes.findIndex(x =>x.KEYID===9);
      $scope.dischargeForm=formTypes[j];
      //find transfer no discharge form
      var k = formTypes.findIndex(x =>x.KEYID===6);
      $scope.transferForm=formTypes[k];
      //find ROC form
      var l = formTypes.findIndex(x =>x.KEYID===3);
      $scope.rocForm=formTypes[l];


      //If there are no forms for the patient, then show all formTypes, select SOC form
      if (forms.length===0) {
        var index = formTypes.findIndex(x => x.KEYID===2);
        formTypes.splice(index, 1);
        $scope.formTypes = formTypes;
        $scope.scheduleSOC=true;
        $scope.selectedForms.push($scope.socForm);
        // If there are forms for the patient
      } else  {
        for (var i = 0; i < forms.length; i++) {
          if (forms[i].FORMTYPE===2) { //check to see if there is a SOC OAsis form , if there is, remove it form formTypes
            var index = formTypes.findIndex(x => x.KEYID===2);
               formTypes.splice(index, 1);
               $scope.formTypes = formTypes;
          } else {
            $scope.formTypes = formTypes; //if there isnt' a SOC oasis form, then show all formTypes
          }
        }
      }

      if (action ==='D'){
        $scope.selectedForms.push($scope.dischargeForm);
        var index = $scope.formTypes.findIndex(x => x.KEYID===9);
        $scope.formTypes.splice(index, 1);
        // $scope.formTypes = formTypes;
      }

      if (action ==='T'){
        $scope.selectedForms.push($scope.transferForm);
        var index = $scope.formTypes.findIndex(x => x.KEYID===6);
        $scope.formTypes.splice(index, 1);
        // $scope.formTypes = formTypes;
      }

      //if patient status is not Transfer, then remove ROC oasis forms
      if ($scope.patient.PATSTATUS !== 'T') {
        var index = formTypes.findIndex(x => x.KEYID===3);
        formTypes.splice(index, 1);
        $scope.formTypes = formTypes;
      } else if ($scope.patient.PATSTATUS === 'T' && action ==='A') {
        $scope.selectedForms.push($scope.rocForm);

        var index = formTypes.findIndex(x => x.KEYID===3);
        formTypes.splice(index, 1);
        $scope.formTypes = formTypes;
      } else if ($scope.patient.PATSTATUS === 'T' && action ==='D') {
        var index = formTypes.findIndex(x => x.KEYID===3);
        formTypes.splice(index, 1);
        $scope.formTypes = formTypes;
      }

      if (action !== 'D' && selected !== null) {
        console.log('check if selected date is w/in last 5 days')
        // if selected date is not within last 5 days of episode
        if (selected.isBetween($scope.lastFiveDays, $scope.endOfEpisode, null, '(]') !== true) {
          console.log('selected date is not within last 5 days, recert form not allowed')
          //remove recertification form
          for (var j = 0; j < $scope.formTypes.length; j++) {
            if ($scope.formTypes[j].KEYID === 4) {
              var index = $scope.formTypes.indexOf($scope.formTypes[j]);
              $scope.formTypes.splice(index, 1);
            }
          }
        } else {
          console.log('recertification form allowed');
          //check to see if recertification visit already exists
          var formsForCurrentEpisode=[];
          forms.forEach(function (form) {
            if (form.EPIKEYID===$scope.episode.KEYID) {
              formsForCurrentEpisode.push(form);
            }
          });
          console.log('formsForCurrentEpisode', formsForCurrentEpisode);
          for (var i = 0; i < formsForCurrentEpisode.length; i++) {
            if (formsForCurrentEpisode[i].FORMTYPE===4) { //check to see if there is a recertification OAsis form , if there is, remove it form formTypes
              console.log('recert form already exists for current episode');
              var index = formTypes.findIndex(x => x.KEYID===4);
                 formTypes.splice(index, 1);
                 $scope.formTypes = formTypes;
            } else {
              console.log('allow recert form');
              $scope.formTypes = formTypes; //if there isnt' a recertification oasis form, then show all formTypes
            }
          }
        }
      }
      // } else {
      //   $scope.formTypes = formTypes;
      //   //if patient trying to discharge, formType 'discharge' is available
      //   // var index = formTypes.findIndex(x => x.KEYID===9);
      //   // $scope.formTypes = [];
      //   // $scope.formTypes.push(formTypes[index]);
      //   // $scope.updateVisitTypeDetails()
      // }


    });
  })



  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.TIMEIN=null;
  $scope.TIMEOUT=null;

  //Autopopulate TIMEOUT based on TIMEIN + default hours
 $scope.changedTIMEIN = function(){
   console.log($scope.TIMEOUT)
   console.log($scope.TIMEIN)

   if ($scope.TIMEIN !== null) {
     var timein = new Date($scope.TIMEIN);
     $scope.TIMEOUT = timein.setHours(timein.getHours()+$scope.HOURS);
     $scope.TIMEOUT = new Date($scope.TIMEOUT);
   }
 };

 //calculate HOURS based on TIMEOUT
 $scope.changed = function() {
   console.log($scope.TIMEOUT)
   console.log($scope.TIMEIN)

   //if TIMEOUT is after TIMEIN
   if ($scope.TIMEOUT.getHours()> $scope.TIMEIN.getHours()) {
     var hourDiff = $scope.TIMEOUT.getHours() - $scope.TIMEIN.getHours();
     var minDiff = $scope.TIMEOUT.getMinutes() - $scope.TIMEIN.getMinutes();
     //Round hour by half hour increments
     if ( (hourDiff===0) && (minDiff>0) ) {
       $scope.HOURS = 1;
     } else if ( (hourDiff>0) && (minDiff>29) ) {
       $scope.HOURS = hourDiff + 1;
     } else if ( (hourDiff>0) && (minDiff<30) ) {
       $scope.HOURS = hourDiff;
     }
   } else {
     $scope.HOURS = null;
   }
 };

  $scope.newVisit = function(newForm) {
    console.log(newForm);
    $scope.timeError=false;

    if (newForm.TIMEOUT.$invalid || newForm.TIMEIN.$invalid) {
      $scope.timeError=true;
      // alert('invalid')
    } else if ($scope.TIMEOUT && $scope.TIMEIN) {
      // alert('filled-works')
      //Format TIMEIN and TIMEOUT
      var hourIN = $scope.TIMEIN.getHours();
      var hourOUT = $scope.TIMEOUT.getHours();
      var minOUT = $scope.TIMEOUT.getMinutes();
      var minIN = $scope.TIMEIN.getMinutes();

      if (minIN < 10) {
        minIN = minIN.toString();
        minIN = '0' + minIN;
      } else {
        minIN = minIN.toString();
      }
      if (minOUT<10){
        minOUT = minOUT.toString();
        minOUT='0'+ minOUT;
      } else {
        minOUT = minOUT.toString();
      }
      if (hourOUT<10){
        hourOUT = hourOUT.toString();
        hourOUT='0'+ hourOUT;
      } else {
        hourOUT = hourOUT.toString();
      }
      if (hourIN<10){
        hourIN = hourIN.toString();
        hourIN='0'+ hourIN;
      } else {
        hourIN = hourIN.toString();
      }

      $scope.TIMEIN = hourIN + minIN + '00';
      $scope.TIMEOUT = hourOUT + minOUT + '00';


      //get UNITCHARGE
      for (var i = 0; i < $scope.visitTypes.length; i++) {
        if ($scope.visitTypes[i].KEYID === $scope.VISITTYPE.KEYID) {
          $scope.UNITCHARGE = $scope.visitTypes[i].DFLTEMPPAY;
        }
      }
    } else if (!$scope.TIMEOUT && !$scope.TIMEIN) {
      // alert('empty-works')
      $scope.TIMEIN = null;
      $scope.TIMEOUT = null;
    } else {
      // alert('error')
      $scope.timeError=true;
    }


    if (newForm.EMPKEYID.$valid && newForm.VISITTYPE.$valid && !$scope.timeError) {
      var createVisit = {
        PATKEYID: $scope.patient.KEYID,
        // FIXME: LSTADMIT is just the last admit, we may
        // want to add a visit for a previous admit
        ADMITKEYID: $scope.patient.LSTADMIT,
        EPIKEYID: $scope.epikeyid,
        EMPKEYID: $scope.EMPKEYID.KEYID,
        VISITTYPE: $scope.VISITTYPE.KEYID,
        DISCIPLINE: $scope.DISCIPLINE,
        VISITDATE: $filter("date")($scope.selected, 'yyyy-MM-dd'), //new Date(selected),
        TIMEIN: $scope.TIMEIN,
        TIMEOUT: $scope.TIMEOUT,
        HOURS: $scope.HOURS,
        MILEAGE: $scope.MILEAGE,
        NOTES: $scope.NOTES,
        QUANTITY: $scope.HOURS,
        UNITCHARGE: $scope.UNITCHARGE
      };
      console.log(createVisit);

      dataService.add('visit', createVisit).then(function(response) {
        console.log(response);


        //Add forms for all selectedForms
        $scope.selectedForms.forEach(function(selectedFormType) {
          var form = {
            PATKEYID : $scope.patient.KEYID,
            // FIXME: LSTADMIT is just the last admit, we may
            // want to add a visit for a previous admit
            ADMITKEYID : $scope.patient.LSTADMIT,
            EPIKEYID : $scope.epikeyid,
            VISITID : response.keyid,
            FORMTYPE : selectedFormType.KEYID,
            FORMDATE : $filter("date")($scope.selected, 'yyyy-MM-dd'),
            ASSIGNEDTO : $scope.EMPKEYID.KEYID,
            STATUS : 0
          };
          console.log(form);
          dataService.add('form', form).then(function(msg) {
            console.log(msg);
          });
          //If SOC visit scheduled, edit Admit PATSTATUS from 'P' to 'A'
          if (selectedFormType.KEYID === 2) {
            console.log("Asdfsa");
            editAdmit();
          }
        });
        $uibModalInstance.close();


      });
    }
  };

  //Get currentAdmitKEYID
  dataService.get('admission', {'PATKEYID':$scope.patient.KEYID}).then(function(data){
    var admitArr = data.data;
    var currentAdmit = admitArr.slice(-1).pop();
    $scope.currentAdmitKEYID = currentAdmit.KEYID;
  });

  //edit Admit and Patient STATUS from 'P' to 'A'
  function editAdmit() {
    var testAdmit = {
      KEYID: $scope.currentAdmitKEYID,
      PATSTATUS: 'A',
      SOCDATE: $scope.M0030_START_CARE_DT
    };
    dataService.edit('admission', testAdmit).then(function(response){
        console.log(response);
    });
    //edit patient table to udpate PATSTATUS from 'P' to 'A'
    dataService.edit('patient', {'KEYID': $scope.patient.KEYID, 'PATSTATUS': 'A'}).then(function(response){
      console.log(response);
      //reset patient
      dataService.get('patient', {'KEYID': patientService.get().KEYID}).then(function(data) {
        var updatedPatient = data.data[0];
        patientService.set(updatedPatient);
      });
    });
  }

  $scope.updateVisitTypeDetails = function() {
    console.log("visit type to update");
    console.log($scope.VISITTYPE);

    $scope.getForms($scope.VISITTYPE.KEYID);
    $scope.HOURS = parseFloat($scope.VISITTYPE.DFLTHRS);
    $scope.changedTIMEIN();
  };

  $scope.selectedForms = [];

  $scope.addForm = function(selectForm) {
    var selectedForms = $scope.selectedForms;
    //If form is not already selected
    if (selectedForms.indexOf(selectForm) === -1 ) {
      $scope.selectedForms.push(selectForm);
    }
    else {
      alert("This form has already be selected");
    }
    console.log($scope.selectedForms);
  };

  $scope.removeForm = function(form) {
    var index = $scope.selectedForms.indexOf(form);
    $scope.selectedForms.splice(index, 1);
    console.log($scope.selectedForms);
  };

  //Get list of formType corresponding to selected visitType
  $scope.getForms = function(visitTypeID) {
    dataService.get('visitTypeForm', {'VISITTYPE_ID': visitTypeID}).then(function(data){
      console.log("visit type forms:", data.data);
      var visitTypeFormArr= data.data
      var index;
      for (var i = 0; i < visitTypeFormArr.length; i++) {
        //remove all forms that are FORMTYPE_ID ===2 and not VISITTYPE_ID ===1 <--not Start of Care
        if ((visitTypeFormArr[i].FORMTYPE_ID === 2) && (visitTypeFormArr[i].VISITTYPE_ID !==1)) {
          index = visitTypeFormArr.indexOf(visitTypeFormArr[i]);
          visitTypeFormArr.splice(index, 1);
        }
        // console.log(visitTypeFormArr);
      };

      $scope.selectedForms = [];

      if ($scope.scheduleSOC) {
        $scope.selectedForms.push($scope.socForm);
      }

      if (action==='D'){
        $scope.selectedForms.push($scope.dischargeForm);
      }

      if (action==='T'){
        $scope.selectedForms.push($scope.transferForm);
      }

      if (action==='A' && $scope.patient.PATSTATUS==='T'){
        $scope.selectedForms.push($scope.rocForm);
      }

      for (var i = 0; i < visitTypeFormArr.length; i++) {
        for (var j = 0; j < $scope.formTypes.length; j++) {
          if (data.data[i].FORMTYPE_ID == $scope.formTypes[j].KEYID) {
            console.log("adding form type: " + $scope.formTypes[j].KEYID);
            $scope.selectedForms.push($scope.formTypes[j]);
          }
        }
      }
        console.log('selectedForms', $scope.selectedForms)
    });
  };
});
