app.controller('PatientReviewController', function($scope, $uibModalInstance, patient, languages, religions, patientService, insurances, employees, doctors) {

  $scope.patient = patient;
  $scope.languages = languages;
  $scope.religions = religions;
  $scope.insurances = insurances;
  $scope.employees = employees;
  $scope.doctors = doctors;
  $scope.nonEditMode = true;

  $scope.enableEditMode = function(section) {
    console.log(section);
    event.preventDefault();
    $scope.nonEditMode = false;
    $scope['edit' + section] = true;
  };
  $scope.disableEditMode = function(section) {
    $scope.nonEditMode = true;
    $scope['edit' + section] = false;
  };

  $scope.saveChanges = function() {
    var patientChanges = {
      FNAME: $scope.FNAME,
      DOB: $scope.DOB,
      MINITIAL: $scope.MINITIAL,
      GENDER: $scope.GENDER,
      LNAME: $scope.LNAME,
      SSN: $scope.SSN,
      MARITAL: $scope.MARITAL,
      LANGUAGE: $scope.LANGUAGE,
      M0140: $scope.M0140,
      RELIGION: $scope.RELIGION,
      // responsiblePerson: null,
      ACUITY: $scope.ACUITY,
      NOTES: $scope.NOTES,

      LOCTYPE: $scope.LOCTYPE,
      //county: $scope.county,
      ADDRESS: $scope.ADDRESS,
      CITY: $scope.CITY,
      STATE: $scope.STATE,
      ZIP: $scope.ZIP,
      //country: $scope.country,
      PHONE1: $scope.PHONE1,
      CELLPHONE: $scope.CELLPHONE,
      EMAIL: $scope.EMAIL,
      //addressNotes: $scope.addressNotes,
      LOCTYPE2: $scope.LOCTYPE2,
      //secondaryCounty: $scope.secondaryCounty,
      ADDRESS2: $scope.ADDRESS2,
      CITY2: $scope.CITY2,
      STATE2: $scope.STATE2,
      ZIP2: $scope.ZIP2,
      //secondaryCountry = $scope.secondaryCountry,
      PHONE2: $scope.PHONE2,
      //secondaryCell = $scope.secondaryCell,
      //secondaryEmail = $scope.secondaryEmail,
      //secondaryAddressNotes = $scope.secondaryAddressNotes,


      //ICD??

      INS1CO: $scope.INS1CO,
      INS1PLAN: $scope.INS1PLAN,
      INS1ID: $scope.INS1ID,
      INS1GROUP: $scope.INS1GROUP,
      INS1AUTHCODE: $scope.INS1AUTHCODE,
      INS1NOTES: $scope.INS1NOTES,
      INS2CO: $scope.INS2CO,
      INS2ID: $scope.INS2ID,
      INS2GROUP: $scope.INS2GROUP,
      INS2AUTHCODE: $scope.INS2AUTHCODE,
      INS2NOTES: $scope.INS2NOTES,

      REFRECEIVED: $scope.REFRECEIVED,
      REFERRSRC: $scope.REFERRSRC,
      REFERRDATE: $scope.REFERRDATE,
      //referralSourceInd: $scope.referralSourceInd,
      REFERRPHYSICIAN: $scope.REFERRPHYSICIAN,
      ADMITSRCE: $scope.ADMITSRCE,
      // caretype:??
      REFERRNOTES: $scope.REFERRNOTES,

      //npi?
      //F2F
      DOCTOR: $scope.DOCTOR,
      DOCTOR2: $scope.DOCTOR2,
      DOCTOR3: $scope.DOCTOR3,
      DOCTOR4: $scope.DOCTOR4,
      DOCTOR5: $scope.DOCTOR5,
      PHARMACY: $scope.PHARMACY,
      DME: $scope.DME,
      CASEMNGR: $scope.CASEMNGR,
      DISCRN: $scope.DISCRN,
      DISCLPN: $scope.DISCLPN,
      DISCHA: $scope.DISCHA,
      DISCPT: $scope.DISCPT,
      DISCMS: $scope.DISCMS,
      DISCOT: $scope.DISCOT,
      DISCST: $scope.DISCST,
      DISCRD: $scope.DISCRD,
      MKTSOURCE: $scope.MKTSOURCE,

      //icd?
      DIAGCODE: $scope.DIAGCODE,
      SURGCODE: $scope.SURGCODE,
    };
    // console.log(patientChanges);
    patientService.setPendingPatient(patientChanges);
    console.log("patientChanges", patientChanges);
  };

  $scope.close = function() {
    $uibModalInstance.close();
  };

//SAVE FOR TESTING//
  // $scope.patient = {
  //   FNAME: 'Bob',
  //   DOB: '2012-01-02',
  //   MINITIAL: 'K',
  //   GENDER: 'M',
  //   LNAME: 'Smith',
  //   SSN: '111111111',
  //   MARITAL: '1',
  //   LANGUAGE: 1,
  //   M0140: '6',
  //   RELIGION: 1,
  //   // responsiblePerson: null,
  //   ACUITY: 1,
  //   NOTES: 'Notes',
  //
  //   LOCTYPE: 'H',
  //   //county: county,
  //   ADDRESS: '111 Front Street',
  //   CITY: 'San Francisco',
  //   STATE: 'NV',
  //   ZIP: '22321',
  //   //country: country,
  //   PHONE1: '+192833333',
  //   CELLPHONE: '+19282222222',
  //   EMAIL: 'bob@smith.com',
  //   //addressNotes: addressNotes,
  //   LOCTYPE2: 'A',
  //   //secondaryCounty: secondaryCounty,
  //   ADDRESS2: '123 King Street',
  //   CITY2: 'Sacramento',
  //   STATE2: 'NV',
  //   ZIP2: '98123',
  //   //secondaryCountry = secondaryCountry,
  //   PHONE2: '+19162227777',
  //   //secondaryCell = secondaryCell,
  //   //secondaryEmail = secondaryEmail,
  //   //secondaryAddressNotes = secondaryAddressNotes,
  //
  //
  //   //ICD??
  //
  //   INS1CO: 1,
  //   INS1PLAN: 'Medicare',
  //   INS1ID: 'MedID',
  //   INS1GROUP: 'MedGroup',
  //   INS1AUTHCODE: 'MedAuthCode',
  //   INS1NOTES: 'Notes for Medicare',
  //   INS2CO: 1,
  //   INS2ID: 'MedID2',
  //   INS2GROUP: 'MedGroup2',
  //   INS2AUTHCODE: 'MedAuthCode2',
  //   INS2NOTES: 'MedNotes2',
  //
  //   REFRECEIVED: 1,
  //   REFERRSRC: 2,
  //   REFERRDATE: '2012-02-01',
  //   //referralSourceInd: referralSourceInd,
  //   REFERRPHYSICIAN: 1,
  //   ADMITSRCE: 2,
  //   // caretype:??
  //   REFERRNOTES: 'Referral Notes',
  //
  //   //npi?
  //   //F2F
  //   DOCTOR: 1,
  //   DOCTORLNAME: 'Doyle',
  //   DOCTORADDRESS: '111 Lundy Lane',
  //   DOCTORCITY: 'San Francisco',
  //   DOCTORPHONE: '1111111',
  //   DOCTORSTATE: 'NV',
  //   DOCTORFAX: '22222222',
  //   DOCTORZIP: '29382',
  //   DOCTOREMAIL: 'dr@gmail.com',
  //
  //   DOCTOR2: null,
  //   DOCTOR3: null,
  //   DOCTOR4: null,
  //   DOCTOR5: null,
  //   PHARMACY: null,
  //   DME: null,
  //   CASEMNGR: null,
  //   DISCRN: null,
  //   DISCLPN: null,
  //   DISCHA: null,
  //   DISCPT: null,
  //   DISCMS: null,
  //   DISCOT: null,
  //   DISCST: null,
  //   DISCRD: null,
  //   MKTSOURCE: null,
  //
  //   //icd?
  //   DIAGCODE: null,
  //   SURGCODE: null,
  // }

});
