app.controller('ViewReferralSourcesController', function($scope, $state, $uibModalInstance, dataService, referralSource) {

  $scope.close = function() {
    $uibModalInstance.close();
  };

  console.log('$scope.referralSource', referralSource);

  $scope.NAME=referralSource.NAME;
  $scope.TYPE=referralSource.TYPE;
  $scope.CATEGORY=referralSource.CATEGORY;
  $scope.EXT=referralSource.EXTKEYID;

  $scope.saveReferralChanges=function(newReferralSourceForm) {
    if (newReferralSourceForm.$valid) {
      var referralChanges={
        KEYID: referralSource.KEYID,
        NAME: $scope.NAME,
        TYPE: $scope.TYPE,
        CATEGORY: $scope.CATEGORY,
        EXTKEYID: $scope.EXT
      };
      console.log('referralChanges', referralChanges);
      dataService.edit('referralSource', referralChanges).then(function(response) {
        console.log('edit referral', response);
        if (response.status==='success') {
          $state.go('referralSources', {}, { reload: true });
          $uibModalInstance.close();
        } else {
          alert('Error in saving changes.');
        }
      });
    }
  };

  $scope.delete=function () {
    if (confirm('Are you sure you want to delete Referral Source?')) {
      dataService.delete('referralSource', {KEYID:referralSource.KEYID}).then(function (response) {
        console.log('deleting referral source', response);
        if (response.status==='success') {
          $state.go('referralSources', {}, { reload: true });
          $uibModalInstance.close();
        } else {
          alert('Error in deleting referral source.');
        }
      });
    }
  };


});
