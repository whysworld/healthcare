app.controller('NewVisitServiceController', function($scope, $filter, $uibModalInstance, CONSTANTS, dataService, getVisitServices) {

  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.disciplineList = CONSTANTS.disciplines;

  $scope.addVisitService = function() {
    console.log("Adding visit service");

    var newVisitService = {
      DISCIPLINE : $scope.DISCIPLINE,
      SVCCODE : $scope.SVCCODE,
      DESC : $scope.DESC,
      HCPC : $scope.HCPC,
      BILLABLE : $scope.BILLABLE,
      DISCDFLT : $scope.DISCDFLT
    };

    dataService.add("visitService", newVisitService).then(function(msg) {
      console.log("Visit service added. Message:");
      console.log(msg);

      getVisitServices();

      $scope.close();
    });
  };

});
