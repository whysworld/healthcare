app.controller('NewBillingController', function($scope, $uibModalInstance, dataService) {

  $scope.close = function() {
    $uibModalInstance.close();
  };

  // sample data
  dataService.getAll('patient').then(function(data) {
    $scope.patients = data.data;
    $scope.patients.forEach(function(patient) {
      //get Episodes for each patient
      dataService.get('episode', {'PATKEYID': patient.KEYID}).then(function(data) {
        var episodes = data.data;
        patient.EPISODES=[];
        for (var j = 0; j < episodes.length; j++) {
          if (episodes[j].PATKEYID === patient.KEYID) {
            patient.EPISODES.push(episodes[j]);
          }
        }
      });
      //randomaly assign claim type - sample data
      if (patient.ADDRESS.length % 2) {
        patient.CLAIMTYPE = "RAP";
      } else {
        patient.CLAIMTYPE = "FC";
      }
      //randomly assign detail - sample data
      if (patient.FNAME.length %2) {
        patient.DETAIL = "Ready";
        patient.MSG = "Error: Signed Certification not Received.";
      } else if (patient.FNAME.length %3) {
        patient.DETAIL = "Warning";
        patient.MSG = "Warning: RAP has not been Paid.";
      } else {
        patient.DETAIL = "Error";
        patient.MSG = "Error: Signed Certification not Received.";
      }

      patient.TOTALCHARGES = Math.floor(Math.random() * (1000 - 100 + 1)) + 100;
    });
    console.log($scope.patients);
  });

  //select All checkbox
  $scope.checkAll = function () {
    console.log($scope.selectedAll);
    if ($scope.selectedAll) {
        $scope.selectedAll = true;
        $scope.showGenerate=true;

    } else {
        $scope.selectedAll = false;
        $scope.showGenerate = false;

    }
    angular.forEach($scope.patients, function (patient) {
      //only 'Ready' claims are allowed to be selected
      if (patient.DETAIL === 'Ready') {
        patient.selected = $scope.selectedAll;
        patient.STATUS = 'Ready';
      }
    });
  };

  //show 'Generate' Button
  $scope.generateBtn=function () {
    for (var i = 0; i < $scope.patients.length; i++) {
      if ($scope.patients[i].selected === true) {
        $scope.showGenerate=true;
        break;
      } else {
        $scope.showGenerate=false;
      }
    }
  };

  //click Generate and add STATUS of claim to 'Ready'
  $scope.generate = function() {
    $scope.updatedClaimsList = [];
    for (var i = 0; i < $scope.patients.length; i++) {
      if ($scope.patients[i].selected === true) {
        console.log($scope.patients[i])
        $scope.patients[i].STATUS='Ready';
      }

      if($scope.patients[i].selected===true){
        $scope.updatedClaimsList.push($scope.patients[i]);
      }
    }
    $uibModalInstance.close($scope.updatedClaimsList);
  };
});
