app.controller('NewFormController', function($scope, $filter, $uibModalInstance, dataService, patientService) {

  $scope.patient=patientService.get();

  $scope.currentVisits=[];

  dataService.get('listPatientRecords', {'PATKEYID': $scope.patient.KEYID}).then(function(data){
    var records=data.data;
    console.log('records', records)

    dataService.getAll('visitType').then(function(data) {
      console.log('visittypes', data.data);
      var visitTypes = data.data;

      records.forEach(function(record) {
        // if (record.EPIKEYID===$scope.currentEpisode.KEYID) {
          visitTypes.forEach(function (visitType) {
            if (record.VISITTYPE===visitType.KEYID) {
              record.VISITTYPEDESC=visitType.DESC + ' - ' + visitType.DISCIPLINE;
            }
          });
          record.VISITDATE=$filter("date")(record.VISITDATE, 'MM-dd-yyyy');
          $scope.currentVisits.push(record);
        // }
      });
      console.log('current visits', $scope.currentVisits);
    });
  });

  // dataService.get('episode', {PATKEYID:$scope.patient.KEYID}).then(function(data) {
  //   console.log('current epi', data.data[data.data.length-1]);
  //   $scope.currentEpisode=data.data[data.data.length-1];
  //
  //   dataService.get('visit', {PATKEYID: $scope.patient.KEYID}).then(function (data) {
  //     var visits=data.data;
  //
  //     dataService.getAll('visitType').then(function(data) {
  //       console.log('visittypes', data.data);
  //       var visitTypes = data.data;
  //
  //       visits.forEach(function(visit) {
  //         if (visit.EPIKEYID===$scope.currentEpisode.KEYID) {
  //           visitTypes.forEach(function (visitType) {
  //             if (visit.VISITTYPE===visitType.KEYID) {
  //               visit.VISITTYPEDESC=visitType.DESC + ' - ' + visitType.DISCIPLINE;
  //             }
  //           });
  //           $scope.currentVisits.push(visit);
  //         }
  //       });
  //       console.log('current visits', $scope.currentVisits);
  //     });
  //   });
  // });

  $scope.disable=true;

  $scope.changeVisit=function (visit) {
    $scope.selectedForms = [];
    $scope.selectForm='';
    $scope.disable=false;
    $scope.selectedVisit=visit;
    console.log('$scope.selectedVisit', $scope.selectedVisit);
  };


  //get all formTypes
  dataService.getAll('formType').then(function (data) {
    $scope.formTypes = data.data;

    $scope.selectedForms = [];
    //addForm to selected Form array
    $scope.addForm = function(selectForm) {
      var selectedForms = $scope.selectedForms;
      console.log('selected form', selectForm);
      //check to see if visit already contains the selectd form
      var isAlreadyAdded=false;
      for (var i = 0; i < $scope.selectedVisit.FORMS.length; i++) {
        if ($scope.selectedVisit.FORMS[i].FORMTYPE === selectForm.KEYID) {
          isAlreadyAdded = true;
          break;
        }
      }
      //if visit doesn't contain form
      if (!isAlreadyAdded) {
        //If form is not already selected
        if (selectedForms.indexOf(selectForm) === -1 ) {
          //add form to seelcted Form array
          $scope.selectedForms.push(selectForm);
        }
        else {
          alert("This form has already been selected.");
          $scope.selectForm='';
        }
      } else {
        alert("The selected visit already contains this form.");
        $scope.selectForm='';
      }
      console.log('form added, $scope.selectedForms', $scope.selectedForms);
    };

    $scope.removeForm = function(form) {
      var index = $scope.selectedForms.indexOf(form);
      $scope.selectedForms.splice(index, 1);
      console.log('form removed, $scope.selectedForms', $scope.selectedForms);
    };
  });

  //get all employess
  dataService.getAll('employee').then(function (data) {
    $scope.employees = data.data;
  });



  $scope.add = function() {
    $scope.selectedForms.forEach(function(selectedFormType) {
      // console.log(selectedFormType);
      // console.log($scope.VISIT)
      // console.log($scope.VISIT.VISITDATE);
      var visitDate=new Date($scope.VISIT.VISITDATE);
      visitDate=$filter("date")(visitDate, 'yyyy-MM-dd');
      // console.log(visitDate)
      // form.ASSIGNED_FNAME = $scope.EMP.FNAME;
      // form.ASSIGNED_LNAME = $scope.EMP.LNAME;
      // form.VISITDATE = $filter("date")($scope.VISITDATE, 'yyyy-MM-dd');

      //Add forms for all selectedForms
      var form = {
        PATKEYID : $scope.patient.KEYID,
        // FIXME: LSTADMIT is just the last admit, we may
        // want to add a visit for a previous admit
        ADMITKEYID : $scope.VISIT.ADMITKEYID,
        EPIKEYID : $scope.VISIT.EPIKEYID,
        VISITID : $scope.VISIT.KEYID,
        FORMTYPE : selectedFormType.KEYID,
        FORMDATE : visitDate,
        ASSIGNEDTO : $scope.VISIT.EMPKEYID,
        STATUS : 0
      };
      console.log('adding form', form);
      dataService.add('form', form).then(function(msg) {
        console.log('form added', msg);
        $uibModalInstance.close($scope.selectedForms);

      });
    });
  };

  $scope.close=function () {
    $uibModalInstance.close()
  }
});
