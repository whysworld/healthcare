app.controller('NewOrderDatesController', function($scope, $filter, $uibModalInstance, dataService, order, print, agencyService, patientService){

  console.log('edit order', order);
  $scope.order=order;

  //get details
  $scope.getDetails=function () {
    dataService.get('orders', {KEYID: order.KEYID}).then(function (data) {
      $scope.orderDetails=data.data[0];
      console.log('order details', $scope.orderDetails);
      //get patient details
      dataService.get('patient', {KEYID: data.data[0].PATKEYID}).then(function (data) {
        $scope.patient=data.data[0];
        $scope.patient.PHONE1=formatAgencyPhone($scope.patient.PHONE1);
        $scope.patient.DOB=$filter('date')(new Date($scope.patient.DOB), 'MM-dd-yyyy');
        if (!$scope.patient.ALLERGIES) {
          $scope.patient.ALLERGIES='N/A';
        }
        console.log('patient details', $scope.patient);
      });

      //get physician details
      dataService.get('doctor', {KEYID: data.data[0].DOCTORID}).then(function (data) {
        $scope.doctor=data.data[0];
        console.log('doctor details', $scope.doctor);
      });

      //get episode Information
      dataService.get('episode', {KEYID: data.data[0].EPIKEYID}).then(function (data) {
        $scope.episode=data.data[0];
        $scope.episode.FROMDATE=$filter('date')(new Date($scope.episode.FROMDATE), 'MM-dd-yyyy');
        $scope.episode.THRUDATE=$filter('date')(new Date($scope.episode.THRUDATE), 'MM-dd-yyyy');
        console.log('episode details', $scope.episode);
      });

      //get admit information
      dataService.get('admission', {KEYID: data.data[0].ADMITKEYID}).then(function (data) {
        $scope.admit=data.data[0];
        if (!$scope.admit.DIAGCODE) {
          $scope.admit.DIAGCODE='N/A';
          $scope.admit.DIAGDESC='N/A';
        }
        if (!$scope.admit.DIAGCODE2) {
          $scope.admit.DIAGCODE2='N/A';
          $scope.admit.DIAGDESC2='N/A';
        }
        if (!$scope.admit.DIAGCODE3) {
          $scope.admit.DIAGCODE3='N/A';
          $scope.admit.DIAGDESC3='N/A';
        }
        console.log('admit details', $scope.admit);
      });
    });
  }


  //populate dates
  $scope.populateDates=function () {
    //populate SENT date if exist
    if (order.SENT) {
      $scope.SENT=new Date($filter('date')(order.SENT, 'MM-dd-yyyy'));
    }
    //populate RECEIVED date if exist
    if (order.RECEIVED) {
      $scope.RECEIVED=new Date($filter('date')(order.RECEIVED, 'MM-dd-yyyy'));
    }
    //populate PHYSICIANSIGNDATE date if exist
    if (order.PHYSICIANSIGNDATE) {
      $scope.PHYSICIANSIGNDATE=new Date($filter('date')(order.PHYSICIANSIGNDATE, 'MM-dd-yyyy'));
    }
  };

  $scope.agency=agencyService.get();
  $scope.agency.PHONE=formatAgencyPhone($scope.agency.PHONE);
  console.log('$scope.agency', $scope.agency);

  //format agency phone for pdf printing
  function formatAgencyPhone(tel) {
    if (!tel) { return ''; }
    var value = tel.toString().trim().replace(/^\+/, '');
    if (value.match(/[^0-9]/)) {
        return tel;
    }
    var country, city, number;
    switch (value.length) {
        case 10: // +1PPP####### -> C (PPP) ###-####
            city = value.slice(0, 3);
            number = value.slice(3);
            break;
        default:
            return tel;
    }
    number = number.slice(0, 3) + '-' + number.slice(3);
    return ("(" + city + ") " + number).trim();
  }

  //print ORDER
  $scope.printOrder=function () {
    // var printContents = document.getElementById('printContents').textContent;

    var docDefinition = {
      content: [
        {
          table: {
            widths: ['*', '*', '*'],
            body: [
              [{text: 'Maya Healthcare Inc.', style: 'header', colSpan: 3, alignment: 'center'}, {}, {}],
    					[
                {
                  text: $scope.agency.NAME + '\n' + $scope.agency.ADDRESS1 + ', ' + $scope.agency.CITY + ', ' + $scope.agency.STATE + ', ' + $scope.agency.ZIP + '\n' + 'Phone: ' + $scope.agency.PHONE,
                  style: 'agency',
                  colSpan: 2
                },
                {},
                {
                  // noWrap: true,
                  text: [
                    {text:'PHYSICIAN ORDER', style: 'formtitle'},
                  ]
                }
              ],
              [
                {
                  style: 'agency',
                  text: [
                    {
                      text: 'Patient: ' + $scope.patient.FNAME +  $scope.patient.FNAME + '\n',
                      bold:true
                    },
                    {text: $scope.patient.ADDRESS + ',\n' + $scope.patient.CITY + ', ' + $scope.patient.STATE + ', ' + $scope.patient.ZIP + '\nPhone: ' + $scope.patient.PHONE1 + '\n'},
                    {text: 'DOB: ' + $scope.patient.DOB + '\nMRN: ' + $scope.patient.PATIENTID},
                  ],
                },
                {
                  colSpan: 2,
                  style: 'agency',
                  text: [
                    {
                      text: 'Physician: ' + $scope.doctor.FNAME +  $scope.doctor.LNAME + '\n',
                      bold:true
                    },
                    {text:$scope.doctor.ADDRESS + ',\n' + $scope.doctor.CITY + ', ' + $scope.doctor.STATE + ', ' + $scope.doctor.ZIP + '\nPhone: ' + $scope.doctor.PHONE + '\nNPI: ' + $scope.doctor.NPI},
                  ]
                },
                {},
              ],
              [
                {
                  style: 'small',
                  colSpan: 3,
                  text: [
                    {
                      text: 'Allergies:\n',
                      style:'subsection'
                    },
                    $scope.patient.ALLERGIES,
                  ]
                }
              ],
              [
                {
                  style: 'small',
                  colSpan: 3,
                  text: [
                    {
                      text: 'Diagnosis:\n',
                      style:'subsection'
                    },
                    {
                      text: 'ICD-10: ',
                      style:'subsection'
                    },
                    $scope.admit.DIAGCODE + ' - ' + $scope.admit.DIAGDESC + '\n',
                    {
                      text: 'ICD-10: ',
                      style:'subsection'
                    },
                    $scope.admit.DIAGCODE2 + ' - ' + $scope.admit.DIAGDESC2 + '\n',
                    {
                      text: 'ICD-10: ',
                      style:'subsection'
                    },
                    $scope.admit.DIAGCODE3 + ' - ' + $scope.admit.DIAGDESC3 + '\n',
                  ]
                }
              ],
    					[
                {
                  style: 'small',
                  colSpan: 3,
                  text: [
                    {
                      text: 'Order Date: ',
                      style:'subsection'
                    },
                    $scope.order.ORDERSDATE + ' ',
                    {
                      text: 'Order #: ',
                      style:'subsection'
                    },
                    $scope.order.ORDERNUMBER + '\n',
                    {
                      text: 'Episode Associated: ',
                      style:'subsection'
                    },
                    $scope.episode.FROMDATE + ' - ' + $scope.episode.THRUDATE,
                  ]
                }
              ],
              [
                {
                  style: 'small',
                  colSpan: 3,
                  text: [
                    {
                      text: 'Subject: ',
                      style:'subsection'
                    },
                    $scope.orderDetails.SUBJECT + '\n',
                    {
                      text: 'Description: ',
                      style:'subsection'
                    },
                    $scope.orderDetails.DESCRIPTION
                  ]
                }
              ],
              [
                {
                  style: 'small',
                  text: [
                    {
                      text: 'Communication: Orders verified -  ',
                      style:'subsection'
                    },
                    'insert yes or no'
                  ]
                },
                {
                  style: 'small',
                  text: [
                    {
                      text: 'Orders Faxed to: ',
                      style:'subsection'
                    },
                    'insert faxed to'
                  ]
                },
                {
                  style: 'small',
                  text: [
                    {
                      text: 'Date: ',
                      style:'subsection'
                    },
                    'insert date faxed to'
                  ]
                }
              ],
              [
                {
                  style: 'small',
                  margin: [0, 0, 0, 30],
                  colSpan: 2,
                  text: [
                    {
                      text: 'Clinician Signature: ',
                      style:'subsection'
                    }
                  ]
                },
                {},
                {
                  style: 'small',
                  text: [
                    {
                      text: 'Date: ',
                      style:'subsection'
                    },
                  ]
                }
              ],
              [
                {
                  style: 'small',
                  margin: [0, 0, 0, 30],
                  colSpan: 2,
                  text: [
                    {
                      text: 'Physician Signature: ',
                      style:'subsection'
                    }
                  ]
                },
                {},
                {
                  style: 'small',
                  text: [
                    {
                      text: 'Date: ',
                      style:'subsection'
                    },
                  ]
                }
              ],
            ]
          }
        },
      ],
      footer: {
          columns: [
            {
              text: 'PRINTED:' + new Date() + '\n' + 'Confidential Information\n Maya Healthcare, Inc. Copyright 2017',
              style: 'footer',
            },
          ]
      },
      styles: {
        header: {
          fontSize: 16,
          bold: true
        },
        subsection: {
          fontSize:10,
          bold: true
        },
        formtitle: {
          fontSize: 14,
          alignment: 'center',
          bold: true
        },
        agency: {
          fontSize:10,
        },
        small: {
          fontSize: 9
        },
        footer: {
          fontSize: 8,
          alignment: 'center'
        }
      }
    };
    pdfMake.createPdf(docDefinition).download('order'+$scope.patient.FNAME+$scope.patient.LNAME+'.pdf');
  };

  //close modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  //save modal changes
  $scope.save=function () {
    var orderChanges={
      KEYID: order.KEYID,
      RECEIVED: $filter('date')($scope.RECEIVED, 'yyyy-MM-dd'),
      SENT: $filter('date')($scope.SENT, 'yyyy-MM-dd'),
      PHYSICIANSIGNDATE: $filter('date')($scope.PHYSICIANSIGNDATE, 'yyyy-MM-dd')
    };
    console.log('orderChanges', orderChanges);
    dataService.edit('orders', orderChanges).then(function (response) {
      console.log('editing orders dates', response);
      if (response.status==='success') {
        //if there is a PHYSICIANSIGNDATE present, edit form status
        if ($scope.PHYSICIANSIGNDATE) {
          dataService.edit('form', {KEYID: order.FORMID, STATUS: 5}).then(function (response) {
            console.log('edit form', response);
            if (response.status==='success'){
              $scope.close();
            }
          });
        } else {
          $scope.close();
        }
      }
    });
  };

  $scope.getDetails();
  $scope.populateDates();
});
