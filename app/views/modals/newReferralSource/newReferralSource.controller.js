app.controller('NewReferralSourceController', function($scope, $state, $uibModalInstance, dataService) {

  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.newReferral = function(newReferral) {
    if (newReferral.$valid) {
      var addReferral = {
        NAME: $scope.NAME,
        TYPE: $scope.TYPE,
        CATEGORY: $scope.CATEGORY,
        EXTKEYID: $scope.EXT
      };
      console.log('addReferral', addReferral);
      dataService.add('referralSource', addReferral).then(function(response) {
        console.log('adding new referral', response);
        if (response.status==='success') {
          $state.go('referralSources', {}, { reload: true });
          $uibModalInstance.close();
        } else {
          alert('Error in adding new referral source.');
        }
      });
    }
  };


});
