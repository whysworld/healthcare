app.controller('OasisEstimateController', function($scope, $uibModalInstance, dataService, formService, oasisObject) {

  // We need the OASIS Key ID
  $scope.form = formService.get();
  console.log('$scope.form', $scope.form);

  dataService.get('ppsInfo', oasisObject).then(function(data) {
    $scope.estimateData = data.data;
  })
});
