app.controller('ViewVisitTypeController', function($scope, $uibModal, $uibModalInstance, CONSTANTS, dataService, visitType, getVisitTypeList) {

  $scope.visitType = visitType;
  $scope.noEdit = false;

  dataService.get('visitType', {KEYID: $scope.visitType.KEYID}).then(function(data) {
    console.log(data.data)
    $scope.visitType = data.data[0];
  });

  $scope.forms = [];
  $scope.selectedForms = [];


  $scope.startEdit = function() {
    $scope.noEdit = false;
  };

  $scope.stopEdit = function() {
    $scope.noEdit = true;
  }

  dataService.getAll('formType').then(function(data) {
    console.log(data);
    $scope.forms = data.data;
  });

  dataService.get("visitTypeForm", {VISITTYPE_ID: $scope.visitType.KEYID}).then(function(data) {
    for (var i = 0; i < data.data.length; i++) {
      data.data[i].NAME = "Unknown form name";
      for (var j = 0; j < $scope.forms.length; j++) {
        if (data.data[i].FORMTYPE_ID == $scope.forms[j].KEYID) {
          data.data[i].NAME = $scope.forms[j].NAME;
        }
      }
    }
    console.log("visit type forms");
    console.log(data.data);
    $scope.selectedForms = data.data;
  });

  $scope.addForm = function(selectForm) {
    var selectedForms = $scope.selectedForms;
    if (selectedForms.indexOf(selectForm) === -1 ) {
        $scope.selectedForms.push(selectForm);
      }
    else {
        alert("This form has already been selected");
    }
    console.log($scope.selectedForms)
  };

  $scope.removeForm = function(selectForm) {
    var index = $scope.selectedForms.indexOf(selectForm);
    $scope.selectedForms.splice(index, 1);
    console.log($scope.selectedForms)
  };

  $scope.disciplineList = CONSTANTS.disciplines;

  $scope.getServiceTypes = function() {
    dataService.getAll("visitService").then(function(visitServiceData) {
      $scope.serviceTypeList = visitServiceData.data;
    });
  };
  $scope.getServiceTypes();

  $scope.getEmployeeClasses = function() {
    dataService.getAll("employeeClass").then(function(employeeClassData) {
      $scope.employeeClassList = employeeClassData.data;
    });
  };
  $scope.getEmployeeClasses();

  $scope.close = function() {
    $uibModalInstance.close();
  };


  $scope.editVisitType = function() {
    var editedVisitType = {
      KEYID: $scope.visitType.KEYID,
      VISITCODE: $scope.visitType.VISITCODE,
      DESC: $scope.visitType.DESC,
      DISCIPLINE: $scope.visitType.DISCIPLINE,
      PERVISIT: $scope.visitType.PERVISIT,
      DFLTHRS: $scope.visitType.DFLTHRS,
      ROUND_HRS: $scope.visitType.ROUND_HRS,
      ENTERBYTIME: $scope.visitType.ENTERBYTIME,
      GETMILEAGE: $scope.visitType.GETMILEAGE,
      ADMINCOST: $scope.visitType.ADMINCOST,
      DFLTEMPPAY: $scope.visitType.DFLTEMPPAY,
      DFLTEMPPAY_PERVISIT: $scope.visitType.DFLTEMPPAY_PERVISIT,
      ACTIVE: $scope.visitType.ACTIVE,
      SERVICE: $scope.visitType.SERVICE,
      EMPCLASS: $scope.visitType.EMPCLASS
    };

    dataService.edit('visitType', editedVisitType).then(function(data) {
      console.log($scope.visitType.KEYID)
      console.log(data)
      console.log("removing visit types");
      dataService.delete("visitTypeForm", {VISITTYPE_ID: $scope.visitType.KEYID}).then(function(removed_data){
        console.log("visit types removed");
        console.log(removed_data);
        console.log($scope.selectedForms)
        for (var i = 0; i < $scope.selectedForms.length; i++) {
          if($scope.selectedForms[i].FORMTYPE_ID === undefined) {
            var visitForm = {
              VISITTYPE_ID: $scope.visitType.KEYID,
              FORMTYPE_ID: $scope.selectedForms[i].KEYID
            };
            dataService.add('visitTypeForm', visitForm).then(function(msg){
              console.log(msg);
            });
          }
          if($scope.selectedForms[i].KEYID === undefined) {
            var visitForm = {
              VISITTYPE_ID: $scope.visitType.KEYID,
              FORMTYPE_ID: $scope.selectedForms[i].FORMTYPE_ID
            };
            dataService.add('visitTypeForm', visitForm).then(function(msg){
              console.log(msg);
            });
          }
        };
        getVisitTypeList();
      });
          $uibModalInstance.close();
    });


    // return true;

  };

  // $scope.saveVisitTypeInsurance = function(data) {
  //   console.log("Editing visit type insurance:");
  //   console.log(data);
  //   if (data.TOADD) {
  //     dataService.add("visitTypeInsurance", data).then(function(response) {
  //       console.log("Visit Type Insurance added. Response:");
  //       console.log(response);
  //     });
  //   } else {
  //     dataService.edit("visitTypeInsurance", data).then(function(response) {
  //       console.log("Visit Type Insurance edited. Response:");
  //       console.log(response);
  //     });
  //   };
  // };

  $scope.saveVisitTypeInsurance = function() {
    // console.log("Editing visit type insurance:");
    // console.log(data);
    for (var i = 0; i < $scope.insuranceSpecificsList.length; i++) {
      if ($scope.insuranceSpecificsList[i].TOADD) {
        console.log($scope.insuranceSpecificsList[i])
        var data = {
          INSURANCE_ID : $scope.insuranceSpecificsList[i].KEYID,
          VISITTYPE_ID : $scope.visitType.KEYID,
          NAME : $scope.insuranceSpecificsList[i].NAME,
          DESC : $scope.insuranceSpecificsList[i].DESC,
          REV_CODE : $scope.insuranceSpecificsList[i].REV_CODE,
          HCPC : $scope.insuranceSpecificsList[i].HCPC,
          AMOUNT : $scope.insuranceSpecificsList[i].AMOUNT,
          BILLABLE : $scope.insuranceSpecificsList[i].BILLABLE,
          UNITMULTIPLIER : null,
          // TOADD : true
        }

        dataService.add("visitTypeInsurance", data).then(function(response) {
          console.log("Visit Type Insurance added. Response:");
          console.log(response);
        });
      } else {
        console.log("ASDfasdfsadfs")
        dataService.edit("visitTypeInsurance", $scope.insuranceSpecificsList[i]).then(function(response) {
          console.log("Visit Type Insurance edited. Response:");
          console.log(response);
        });
      };
    }

  };

  $scope.removeInsurance=function(insurance) {
    console.log(insurance)
    dataService.delete('visitTypeInsurance', insurance).then(function(response) {
      console.log(response)
      if(response ===''){
        console.log('sdf')
          var index = $scope.insuranceSpecificsList.indexOf(insurance);
          $scope.insuranceSpecificsList.splice(index, 1);
      } else {
        $scope.getInsuranceSpecificsList()
      }
        // $scope.getInsuranceSpecificsList()

    })
  }

  $scope.cancelAddInsurance=function (insurance) {
    var index = $scope.insuranceSpecificsList.indexOf(insurance);
        $scope.insuranceSpecificsList.splice(index, 1);
  }

  $scope.getInsuranceSpecificsList = function() {
    dataService.getAll("insurance").then(function(data) {
      $scope.insuranceList = data.data;

      dataService.get("visitTypeInsurance", {VISITTYPE_ID : $scope.visitType.KEYID}).then(function(data) {
        var data = data.data;
        console.log(data)
        var defaultfound = false;
        for (var i = 0; i < data.length; i++) {
          data[i].id = i+1;
          data[i].TOADD = false;
          if (data[i].INSURANCE_ID && data[i].INSURANCE_ID != 0) {
            for (var j = 0; j < $scope.insuranceList.length; j++) {
              if ($scope.insuranceList[j].KEYID == data[i].INSURANCE_ID) {
                data[i].NAME = $scope.insuranceList[j].NAME;
              }
            }
          } else {
            data[i].id = 0;
            data[i].NAME = "Default";
            defaultfound = true;
          }
          console.log(data)
        }
        if (defaultfound == false) {
          var defaultInsurance = {
            id : 0,
            VISITTYPE_ID : $scope.visitType.KEYID,
            NAME : "Default",
            INSURANCE_ID : 0,
            DESC : "Default",
            REV_CODE : null,
            HCPC : null,
            AMOUNT : null,
            BILLABLE : null,
            UNITMULTIPLIER : null,
            TOADD : true
          };
          data.unshift(defaultInsurance);
        }
        $scope.insuranceSpecificsList = data;
      });
    });
  };
  //
  // $scope.addInsurance = function() {
  //   $scope.inserted = {
  //     id : Math.max.apply(Math, $scope.insuranceSpecificsList.map(function(o) { return o.id } )) + 1,
  //     INSNAME : "",
  //     DESC : "",
  //     REV_CODE : "",
  //     HCPC : "",
  //     AMOUNT : 1,
  //     BILLABLE : true,
  //     UNITMULTIPLIER : "",
  //     TOADD : true
  //   };
  //
  //   $scope.insuranceSpecificsList.push($scope.inserted);
  // };

  $scope.getInsuranceSpecificsList();

  //add selected insurance to list
  $scope.addInsur = function(selectInsur){
    // console.log(selectInsur)
    // console.log(parseInt($scope.insuranceSpecificsList.indexOf(selectInsur.NAME)));
    // console.log($scope.insuranceSpecificsList)

    if ($scope.insuranceSpecificsList.indexOf(selectInsur.NAME) === -1 ) {
        $scope.insuranceSpecificsList.push(selectInsur);
        // console.log(selectInsur)
        // console.log($scope.insuranceSpecificsList.indexOf(selectInsur))
        // $scope.taco=true

      }
    else {
        alert("This form has already been selected");
    }
    console.log($scope.insuranceSpecificsList)
  };

  // $scope.removeForm = function(selectInsur) {
  //   var index = $scope.insuranceSpecificsList.indexOf(selectInsur);
  //   $scope.insuranceSpecificsList.splice(index, 1);
  // };

  $scope.addInsuranceDetails = function (insurance, index) {

    console.log(insurance);
    // console.log($scope.change)
    console.log($scope.insuranceSpecificsList)
    // var insurDetails={
    //   INSNAME : insurance.NAME,
    //   DESC : $scope.insurance.DESC,
    //   REV_CODE : $scope.insurance.REV_CODE,
    //   HCPC : $scope.insurance.HCPC,
    //   AMOUNT : $scope.insurance.AMOUNT,
    //   BILLABLE : $scope.insurance.BILLABLE,
    //   UNITMULTIPLIER : "",
    //   TOADD : true
    // };
    // console.log(insurDetails)
    console.log($scope.insurance.DESC)


    for (var i = 0; i < $scope.insuranceSpecificsList.length; i++) {
      if (($scope.insuranceSpecificsList[i].NAME === insurance.NAME) && ($scope.insuranceSpecificsList[i].TOADD === undefined)) {
        console.log($scope.insuranceSpecificsList[i])
        console.log($scope.insurance.DESC)
        $scope.insuranceSpecificsList[i].INSNAME = insurance.NAME
        $scope.insuranceSpecificsList[i].DESC = $scope.insurance.DESC
        $scope.insuranceSpecificsList[i].REV_CODE = $scope.insurance.REV_CODE
        $scope.insuranceSpecificsList[i].HCPC = $scope.insurance.HCPC
        $scope.insuranceSpecificsList[i].AMOUNT = $scope.insurance.AMOUNT
        $scope.insuranceSpecificsList[i].BILLABLE = $scope.insurance.BILLABLE
        $scope.insuranceSpecificsList[i].UNITMULTIPLIER = ""
        $scope.insuranceSpecificsList[i].TOADD = true
        break;
      }
    }
    console.log($scope.insuranceSpecificsList)

  };

  $scope.editInsurance=function(updatedInsurance) {
    console.log(updatedInsurance)
    $scope.insuranceSpecificsList.forEach(function(insurance) {
      if (insurance.id === updatedInsurance.id) {
        insurance = updatedInsurance;
      }
    })
    console.log($scope.insuranceSpecificsList)
  }

  $scope.newVisitService = function() {
    var modalInstance = $uibModal.open({
      controller: 'NewVisitServiceController',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/newVisitService/newVisitService.html',
      size: 'md',
      resolve: {
        getVisitServices: function() {
          return $scope.getServiceTypes;
        }
      }
    });
  };

  $scope.removeVisitService = function() {
    console.log("Visit Service to remove:");
    console.log(visitType.SERVICE);
    var visitServiceToRemove = {
      KEYID: visitType.SERVICE
    };
    dataService.delete("visitService", visitServiceToRemove).then(function(msg) {
      console.log("Visit service removed. Message:");
      console.log(msg);
      $scope.getServiceTypes();
    });
  };

  $scope.newEmployeeClass = function() {
    var modalInstance = $uibModal.open({
      controller: 'NewEmployeeClassController',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/newEmployeeClass/newEmployeeClass.html',
      size: 'md'
    });
  };

  $scope.removeEmployeeClass = function() {
    console.log("Employee class to remove:");
    console.log(visitType.EMPCLASS);
    var employeeClassToRemove = {
      KEYID: visitType.EMPCLASS
    };
    dataService.delete("employeeClass", employeeClassToRemove).then(function(msg) {
      console.log("Employee class removed. Message:");
      console.log(msg);
      $scope.getEmployeeClasses();
    });
  };

  $scope.removeVisitType = function(visitType) {
    console.log('remove', visitType);
    //get all visits with selected visitType
    dataService.get('visit', {VISITTYPE: visitType.KEYID}).then(function(data){
      console.log('visit', data.data);
      var visitsArr = data.data;
      // if there are visits...
      if (visitsArr.length>0) {
        for (var i = 0; i < visitsArr.length; i++) {
          // Get all forms for a visit
          getForm(visitsArr[i]);
        }
      } else {
        dataService.delete('visitType', {KEYID: visitType.KEYID}).then(function(response){
          console.log(response);
          $uibModalInstance.close();
        });
      }

    });
  };

  function getForm(visit) {
    //get all the forms for a visit
    dataService.get('form', {VISITID: visit.KEYID}).then(function(data){
      var formsArr = data.data;
      console.log('formsArr', formsArr);
      if (formsArr.length>0) {
        for (var i = 0; i < formsArr.length; i++) {
          deleteForm(formsArr[i], visit); //can't delete form if record exists in oasis table
        }
      } else {
        deleteVisit(visit);
      }
    });
  }

  //delete each of the form
  function deleteForm(form, visit) {
    console.log(form, visit);
    dataService.delete('form', {KEYID: form.KEYID}).then(function(response){
      console.log(response);
      deleteVisit(visit);
    });
  }

  //delete the visit and visitType
  function deleteVisit(visit){
    dataService.delete('visit', {KEYID: visit.KEYID}).then(function(msg) {
      console.log(msg);
      dataService.delete('visitType', {KEYID: visitType.KEYID}).then(function(response){
        console.log(response);
      });
    });
  }

  //storing copy
  $scope.edit=function(insurance) {
    $scope.copy = angular.copy(insurance);
  };

  $scope.cancel=function (index) {
    $scope.insuranceSpecificsList[index] = $scope.copy;
  };
});
