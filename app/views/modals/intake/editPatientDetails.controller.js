app.controller('EditPatientDetailsController', function($scope, $uibModal, $filter, $uibModalInstance, section, patient, dataService, patientService, admission) {

  $scope.patient = patient;
  $scope['edit' + section] = true;

  dataService.getAll('insurance').then(function(data){
    $scope.insurances = data.data;
  })

  dataService.getAll('doctor').then(function(data){
    console.log(data.data)
    $scope.doctors = data.data;
    findDOCINFO();
  })

  $scope.admission=admission;
  // dataService.get('admission', {'PATKEYID': $scope.patient.KEYID}).then(function(data){
  //   $scope.admission = data.data.pop()
  // })

  $scope.disableEditMode = function(section) {
    $scope.nonEditMode = true;
    $scope['edit' + section] = false;
  };


  function findDOCINFO() {
      var doctors = $scope.doctors;
        // console.log(icd)
        // console.log($scope.DIAGCODE)
        for (i=0; i<doctors.length; i++) {
          if ($scope.admission.DOCTOR === doctors[i].KEYID) {
            $scope.DOCTORNPI = doctors[i].NPI;
            $scope.DOCTORFNAME = doctors[i].FNAME;
            $scope.DOCTORLNAME = doctors[i].LNAME;
            $scope.DOCTORADDRESS = doctors[i].ADDRESS;
            $scope.DOCTORCITY = doctors[i].CITY;
            $scope.DOCTORZIP = doctors[i].ZIP;
            $scope.DOCTORFAX = doctors[i].FAX;
            $scope.DOCTORPHONE = doctors[i].PHONE;
            $scope.DOCTOREMAIL = doctors[i].EMAIL;
            $scope.DOCTORSTATE = doctors[i].STATE;
            $scope.DOCTORKEYID = doctors[i].KEYID;
            $scope.DOCTORF2F = doctors[i].F2F;
            $scope.DOCTORPOC = doctors[i].POC;
            $scope.DOCTORNOTES = doctors[i].NOTES;
            break;
          }
        }
  }

    $scope.updateDoctor = function() {
      var doctors = $scope.doctors;
        // console.log(icd)
        // console.log($scope.DIAGCODE)
        for (i=0; i<doctors.length; i++) {
          if ($scope.DOCTORNPI === doctors[i].NPI) {
            // $scope.DOCTORNPI = doctors[i].NPI;
            $scope.DOCTORFNAME = doctors[i].FNAME;
            $scope.DOCTORLNAME = doctors[i].LNAME;
            $scope.DOCTORADDRESS = doctors[i].ADDRESS;
            $scope.DOCTORCITY = doctors[i].CITY;
            $scope.DOCTORZIP = doctors[i].ZIP;
            $scope.DOCTORFAX = doctors[i].FAX;
            $scope.DOCTORPHONE = doctors[i].PHONE;
            $scope.DOCTOREMAIL = doctors[i].EMAIL;
            $scope.DOCTORSTATE = doctors[i].STATE;
            $scope.DOCTORKEYID = doctors[i].KEYID;
            $scope.DOCTORNOTES = doctors[i].NOTES;
            $scope.DOCTORF2F = doctors[i].F2F;
            $scope.DOCTORPOC = doctors[i].POC;
            break;
          }
        }
    };

  //SEARCH NPPES DOCTORNPI  $scope.allDoctors = [];

    $scope.allDoctors = [];
    $scope.filteredDoctors = $scope.npiSearchText ? $scope.allDoctors.filter($scope.createFilterFor($scope.npiSearchText)) : $scope.allDoctors;

    $scope.getDoctors = function() {
      dataService.getAll('doctor').then(function(data) {
        $scope.allDoctors = data.data.map(function(doctor) {
          doctor.value = doctor.NPI + " " + doctor.FNAME.toLowerCase() + " " + doctor.LNAME.toLowerCase() + " " + doctor.CITY.toLowerCase();
          // console.log(doctor)
          return doctor;
        });
      });
    }

    $scope.getDoctors();

    $scope.npiSearchTextChange = function() {
      // console.log("Query: " + query);
      // $scope.getDoctors();
      $scope.filteredDoctors = $scope.npiSearchText ? $scope.allDoctors.filter($scope.createFilterFor($scope.npiSearchText)) : $scope.allDoctors;
    }

    $scope.selectedDoctorChange = function(doctor) {
      console.log("selected doctor changed:");
      console.log(doctor);

      $scope.physicianName = doctor.FNAME + " " + doctor.MINITIAL + ". " + doctor.LNAME;
      $scope.physicianAddress = doctor.ADDRESS;
      $scope.physicianCity = doctor.CITY;
      $scope.physicianPhone = doctor.PHONE;
      $scope.physicianState = doctor.STATE;
      $scope.physicianFax = doctor.FAX;
      $scope.physicianZip = doctor.ZIP;
      $scope.physicianEmail = doctor.EMAIL;
    }

    $scope.selectPhysician = function(doctor) {
      console.log("Selected physician");
      console.log(doctor);

      $scope.npiSearchText = "";
      $scope.selectedDoctor = doctor;
    }

    $scope.createFilterFor = function(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(item) {
        return (item.value.indexOf(lowercaseQuery) > -1);
      };
    }

    $scope.addDoctor = function(event) {
      console.log("addDoctor");
      event.preventDefault();
      var modalInstance = $uibModal.open({
        controller: 'SearchDoctorController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/searchNppesDoctor/searchNppesDoctor.html',
        size: 'lg',
        scope: $scope
      });
      modalInstance.result.then(function (data) {
        $scope.noResults = false;
        // console.log(data)
        showAddedDoc(data)
      })
    };

  function showAddedDoc(data) {
    dataService.get('doctor', {'KEYID': data.keyid}).then(function(data){
      var addDoctor = data.data[0]
          $scope.DOCTORNPI = addDoctor.NPI;
          $scope.DOCTORFNAME = addDoctor.FNAME;
          $scope.DOCTORLNAME = addDoctor.LNAME;
          $scope.DOCTORADDRESS = addDoctor.ADDRESS;
          $scope.DOCTORCITY = addDoctor.CITY;
          $scope.DOCTORZIP = addDoctor.ZIP;
          $scope.DOCTORFAX = addDoctor.FAX;
          $scope.DOCTORPHONE = addDoctor.PHONE;
          $scope.DOCTOREMAIL = addDoctor.EMAIL;
          $scope.DOCTORSTATE = addDoctor.STATE;
          $scope.DOCTORKEYID = addDoctor.KEYID;
          $scope.DOCTORNOTES = addDoctor.NOTES;
          $scope.DOCTORF2F = addDoctor.F2F;
          $scope.DOCTORPOC = addDoctor.POC;
    })
  }


  $scope.saveChanges = function(){
    var patientDetailChanges = {
      KEYID: $scope.patient.KEYID,
      FNAME: $scope.FNAME,
      MINITIAL: $scope.MINITIAL,
      LNAME: $scope.LNAME,
      STATE: $scope.STATE,
      ZIP: $scope.ZIP,
      SSN: $scope.SSN,
      DOB: $filter("date")($scope.DOB, 'yyyy/MM/dd'),
      GENDER: $scope.GENDER,
      INSCO: $scope.INSCO,
      INSID: $scope.INSID,
    };

    var admitChanges = {
      KEYID: $scope.admission.KEYID,
      INS1CO: $scope.INSCO,
      INS1ID: $scope.INSID,
      INS1PLAN: $scope.INS1PLAN,
      INS1GROUP: $scope.INS1GROUP,
      INS1AUTHCODE: $scope.INS1AUTHCODE,
      INS1NOTES: $scope.INS1NOTES,
      INS2CO: $scope.INS2CO,
      INS2ID: $scope.INS2ID,
      INS2PLAN: $scope.INS2PLAN,
      INS2GROUP: $scope.INS2GROUP,
      INS2AUTHCODE: $scope.INS2AUTHCODE,
      INS2NOTES: $scope.INS2NOTES,
      DOCTOR: $scope.DOCTORKEYID
    };
    var doctorChanges = {
      KEYID: $scope.DOCTORKEYID,
      FNAME: $scope.DOCTORFNAME,
      LNAME: $scope.DOCTORLNAME,
      ADDRESS: $scope.DOCTORADDRESS,
      CITY: $scope.DOCTORCITY,
      STATE: $scope.DOCTORSTATE,
      ZIP: $scope.DOCTORZIP,
      PHONE: $scope.DOCTORPHONE,
      FAX: $scope.DOCTORFAX,
      EMAIL: $scope.DOCTOREMAIL,
      F2F: $scope.DOCTORF2F,
      POC: $scope.DOCTORPOC,
      NOTES: $scope.DOCTORNOTES
    }
    console.log(patientDetailChanges);
    dataService.edit('patient', patientDetailChanges).then(function(response){
      console.log(response);
      dataService.get('patient', {'KEYID' : patientService.get().KEYID}).then(function(data) {
        var updatedPatient = data.data[0];
        patientService.set(updatedPatient);
        $scope.updatedPatient = patientService.get();
        updateAdmission($scope.updatedPatient, admitChanges);
      });
    });
    dataService.edit('doctor', doctorChanges).then(function(response){
      console.log(response)
    })
  };

  function updateAdmission(updatedPatient, admitChanges){
    dataService.edit('admission', admitChanges).then(function(response) {
      console.log(response);
      dataService.get('admission', {'KEYID' : $scope.admission.KEYID}).then(function(data) {
          $scope.updatedAdmission = data.data[0];
          // console.log($scope.updatedAdmission);
            close(updatedPatient,$scope.updatedAdmission);
        });
    });
  }

  function close(updatedPatient,updatedAdmission) {
    // console.log("closing modal")
    $uibModalInstance.close({updatedPatient:updatedPatient, updatedAdmission:updatedAdmission});
    // $uibModalInstance.close($scope.updatedAdmission);

  }

  $scope.patient.DOB = $filter("date")($scope.patient.DOB, 'yyyy/MM/dd');
  $scope.DOB = new Date($scope.patient.DOB)

  // $scope.patient.DOB = new Date($scope.patient.DOB)
});
