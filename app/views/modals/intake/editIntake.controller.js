app.controller('EditIntakeController', function($scope, $filter, $state, $window, $uibModal, $uibModalInstance, section, patient, dataService, patientService, languages, employees, doctors, insurances, religions, admission, referralSources, ICDcodes) {


  $scope.close = function () {
    $uibModalInstance.close();
  };

  $scope.disableEditPhysician = true;

  $scope.editPhysicianDetails = function() {
    $scope.disableEditPhysician=!$scope.disableEditPhysician;
  };

  $scope.savePhysicianChanges = function() {
    $scope.disableEditPhysician=!$scope.disableEditPhysician;
  };

  $scope.doctors = doctors.map(function(doctor) {
      doctor.value = doctor.NPI + " " + doctor.FNAME.toLowerCase() + " " + doctor.LNAME.toLowerCase() + " " + doctor.NPI.toLowerCase();
      return doctor;
  });

  $scope.employees = employees;
  $scope.insurances = insurances;
  $scope.languages = languages;
  $scope.patient = patient;
  $scope.religions = religions;
  $scope.admission = admission;
  $scope.referralSources = referralSources;

  //populate secondary insurance if exists
  if ($scope.patient.ADDRESS2 || $scope.patient.LOCTYPE2 || $scope.patient.PHONE2 ||$scope.patient.ZIP2||$scope.patient.STATE2||$scope.patient.CITY2) {
    $scope.addSecondaryAddress=true;
    $scope.secondaryAddressCheckbox=true;
  }

  //populate primary care giver if exists
  if ($scope.admission.PCGNAME) {
    $scope.addPrimaryCaregiver=true;
    $scope.primaryCheckbox=true;
  }

  //populate emergency contact if exists
  if ($scope.admission.ECNAME) {
    $scope.addEmergencyCaregiver=true;
    $scope.emergencyCheckbox=true;
  }

  //if existing insurance is medicare, then hide plan, group, and auth fields
  if ($scope.patient.INSCO===1) {
    $scope.hideInsuranceInfo=true;
  }

  if ($scope.admission.INS2CO) {
    $scope.addSecondaryInsurance=true;
    $scope.secondaryInsuranceCheckbox=true;
    if ($scope.admission.INS2CO===1) {
      $scope.hideInsurance2Info=true;
    }
  }

  if ($scope.admission.REFERRDATE) {
    $scope.REFERRDATE = new Date($scope.admission.REFERRDATE);
  } else {
    $scope.REFERRDATE = new Date();
  }


  $scope.hideInfo=function () {
    //check to make sure INS2CO not same as INS1CO
    if ($scope.INSCO===$scope.INS2CO){
      $scope.isDuplicatePrimary=true;
      $scope.INSCO=null;
    } else {
      $scope.isDuplicatePrimary=false;
      //if selected insurance is Medicare, hide auth code, plan, group
      if($scope.INSCO===1){
        $scope.hideInsuranceInfo=true;
      } else {
        $scope.hideInsuranceInfo=false;
      }
    }
  };

  $scope.hide2Info=function () {
    //check to make sure INS2CO not same as INS1CO
    if ($scope.INS2CO===$scope.INSCO){
      $scope.isDuplicate=true;
      $scope.INS2CO=null;
    } else {
      $scope.isDuplicate=false;
      //if selected insurance is Medicare, hide auth code, plan, group
      if($scope.INS2CO===1){
        $scope.hideInsurance2Info=true;
      } else {
        $scope.hideInsurance2Info=false;
      }
    }
  };

  //clear secondary insurance fields if add secondary insurance checkbox is unchecked
  $scope.isSecondaryInsuranceChecked=function () {
    if($scope.secondaryInsuranceCheckbox===false) {
      $scope.INS2CO=null;
      $scope.INS2ID=null;
      $scope.INS2PLAN=null;
      $scope.INS2GROUP=null;
      $scope.INS2AUTHCODE=null;
      $scope.INS2NOTES=null;
    }
  };


  findDOCINFO();
  function findDOCINFO() {
    var doctors = $scope.doctors;
      // console.log(icd)
      // console.log($scope.DIAGCODE)
      for (i=0; i<doctors.length; i++) {
        if ($scope.admission.DOCTOR === doctors[i].KEYID) {
          $scope.DOCTORNPI = doctors[i].NPI;
          $scope.DOCTORFNAME = doctors[i].FNAME;
          $scope.DOCTORLNAME = doctors[i].LNAME;
          $scope.DOCTORADDRESS = doctors[i].ADDRESS;
          $scope.DOCTORCITY = doctors[i].CITY;
          $scope.DOCTORZIP = doctors[i].ZIP;
          $scope.DOCTORFAX = doctors[i].FAX;
          $scope.DOCTORPHONE = doctors[i].PHONE;
          $scope.DOCTOREMAIL = doctors[i].EMAIL;
          $scope.DOCTORSTATE = doctors[i].STATE;
          $scope.DOCTORKEYID = doctors[i].KEYID;
          $scope.DOCTORF2F = doctors[i].F2F;
          $scope.DOCTORPOC = doctors[i].POC;
          $scope.DOCTORNOTES = doctors[i].NOTES;
          break;
        }
      }
    }

    // $scope.doctorSearch = function() {
    //   console.log("filtering doctors");
    //   $scope.filteredDoctors = $scope.doctorSearchText ? $scope.doctors.filter($scope.createFilterFor($scope.doctorSearchText)) : $scope.doctors;
    // }
    $scope.filterDoctor = function(doctor) {
      if (!$scope.DOCTORNPI || (doctor.value && doctor.value.toLowerCase().indexOf($scope.DOCTORNPI.toLowerCase()) != -1)){
          return true;
      } else {
        return false;
      }
    }

    $scope.updateDoctor = function() {
      var doctors = $scope.doctors;
      // console.log(icd)
      // console.log($scope.DIAGCODE)
      for (i=0; i<doctors.length; i++) {
        if ($scope.DOCTORNPI === doctors[i].NPI) {
          // $scope.DOCTORNPI = doctors[i].NPI;
          $scope.DOCTORFNAME = doctors[i].FNAME;
          $scope.DOCTORLNAME = doctors[i].LNAME;
          $scope.DOCTORADDRESS = doctors[i].ADDRESS;
          $scope.DOCTORCITY = doctors[i].CITY;
          $scope.DOCTORZIP = doctors[i].ZIP;
          $scope.DOCTORFAX = doctors[i].FAX;
          $scope.DOCTORPHONE = doctors[i].PHONE;
          $scope.DOCTOREMAIL = doctors[i].EMAIL;
          $scope.DOCTORSTATE = doctors[i].STATE;
          $scope.DOCTORKEYID = doctors[i].KEYID;
          $scope.DOCTORNOTES = doctors[i].NOTES;
          $scope.DOCTORF2F = doctors[i].F2F;
          $scope.DOCTORPOC = doctors[i].POC;
          break;
        }
      }
    };

//SEARCH NPPES DOCTORNPI  $scope.allDoctors = [];

$scope.allDoctors = [];
$scope.filteredDoctors = $scope.npiSearchText ? $scope.allDoctors.filter($scope.createFilterFor($scope.npiSearchText)) : $scope.allDoctors;

$scope.getDoctors = function() {
  dataService.getAll('doctor').then(function(data) {
    $scope.allDoctors = data.data.map(function(doctor) {
      doctor.value = doctor.NPI + " " + doctor.FNAME.toLowerCase() + " " + doctor.LNAME.toLowerCase() + " " + doctor.CITY.toLowerCase();
        // console.log(doctor)
        return doctor;
      });
  });
}

$scope.getDoctors();

$scope.npiSearchTextChange = function() {
    // console.log("Query: " + query);
    // $scope.getDoctors();
    $scope.filteredDoctors = $scope.npiSearchText ? $scope.allDoctors.filter($scope.createFilterFor($scope.npiSearchText)) : $scope.allDoctors;
  }

  $scope.selectedDoctorChange = function(doctor) {
    console.log("selected doctor changed:");
    console.log(doctor);

    $scope.physicianName = doctor.FNAME + " " + doctor.MINITIAL + ". " + doctor.LNAME;
    $scope.physicianAddress = doctor.ADDRESS;
    $scope.physicianCity = doctor.CITY;
    $scope.physicianPhone = doctor.PHONE;
    $scope.physicianState = doctor.STATE;
    $scope.physicianFax = doctor.FAX;
    $scope.physicianZip = doctor.ZIP;
    $scope.physicianEmail = doctor.EMAIL;
  }

  $scope.selectPhysician = function(doctor) {
    console.log("Selected physician");
    console.log(doctor);

    $scope.npiSearchText = "";
    $scope.selectedDoctor = doctor;
  }

  $scope.createFilterFor = function(query) {
    var lowercaseQuery = angular.lowercase(query);

    return function filterFn(item) {
      return (item.value.indexOf(lowercaseQuery) > -1);
    };
  }

  $scope.addDoctor = function(event) {
    console.log("addDoctor");
    event.preventDefault();
    var modalInstance = $uibModal.open({
      controller: 'SearchDoctorController',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/searchNppesDoctor/searchNppesDoctor.html',
      size: 'lg',
      scope: $scope
    });
    modalInstance.result.then(function (data) {
      $scope.noResults = false;
      // console.log(data)
      showAddedDoc(data)
    })
  };

  function showAddedDoc(data) {
    dataService.get('doctor', {'KEYID': data.keyid}).then(function(data){
      var addDoctor = data.data[0]
      $scope.DOCTORNPI = addDoctor.NPI;
      $scope.DOCTORFNAME = addDoctor.FNAME;
      $scope.DOCTORLNAME = addDoctor.LNAME;
      $scope.DOCTORADDRESS = addDoctor.ADDRESS;
      $scope.DOCTORCITY = addDoctor.CITY;
      $scope.DOCTORZIP = addDoctor.ZIP;
      $scope.DOCTORFAX = addDoctor.FAX;
      $scope.DOCTORPHONE = addDoctor.PHONE;
      $scope.DOCTOREMAIL = addDoctor.EMAIL;
      $scope.DOCTORSTATE = addDoctor.STATE;
      $scope.DOCTORKEYID = addDoctor.KEYID;
      $scope.DOCTORNOTES = addDoctor.NOTES;
      $scope.DOCTORF2F = addDoctor.F2F;
      $scope.DOCTORPOC = addDoctor.POC;
    })
  }

  var index=1;
  function findICDINFO() {
    // $scope.addICDArray=[];
    // $scope.ICDCODES={};

    if ($scope.admission.DIAGCODE!==null) {
      $scope.ICDCODE = $scope.admission.DIAGCODE;
    }
    if ($scope.admission.DIAGDESC!==null) {
      $scope.ICDDESC = $scope.admission.DIAGDESC;
    }
    if ($scope.admission.DIAGONSET!==null){
      $scope.ICDDATE = new Date($filter("date")($scope.admission.DIAGONSET, 'yyyy/MM/dd'));
    }

    if ($scope.admission.DIAGCODE2) {
      var codeObj={
        code: "CODE2",
        date: "DATE2",
        desc: "DESC2",
        index: "2",
        codedetail: $scope.admission.DIAGCODE2,
        datedetail: new Date($filter("date")($scope.admission.DIAGONSET2, 'yyyy/MM/dd')),
        descdetail: $scope.admission.DIAGDESC2,
      };
      $scope.addICDArray.push(codeObj);
      index+=1;
      $scope.ICDCODES.CODE2=$scope.admission.DIAGCODE2;
      $scope.ICDCODES.DESC2=$scope.admission.DIAGDESC2;
      $scope.ICDCODES.DATE2=$scope.admission.DIAGONSET2;
    }

    if ($scope.admission.DIAGCODE3) {
      var codeObj={
        code: "CODE3",
        date: "DATE3",
        desc: "DESC3",
        index: "3",
        codedetail: $scope.admission.DIAGCODE3,
        datedetail: new Date($filter("date")($scope.admission.DIAGONSET3, 'yyyy/MM/dd')),
        descdetail: $scope.admission.DIAGDESC3,
      };
      $scope.addICDArray.push(codeObj);
      index+=1;
      $scope.ICDCODES.CODE3=$scope.admission.DIAGCODE3;
      $scope.ICDCODES.DESC3=$scope.admission.DIAGDESC3;
      $scope.ICDCODES.DATE3=$scope.admission.DIAGONSET3;
    }

    if ($scope.admission.DIAGCODE4) {
      var codeObj={
        code: "CODE4",
        date: "DATE4",
        desc: "DESC4",
        index: "4",
        codedetail: $scope.admission.DIAGCODE4,
        datedetail: new Date($filter("date")($scope.admission.DIAGONSET4, 'yyyy/MM/dd')),
        descdetail: $scope.admission.DIAGDESC4,
      };
      $scope.addICDArray.push(codeObj);
      index+=1;
      $scope.ICDCODES.CODE4=$scope.admission.DIAGCODE4;
      $scope.ICDCODES.DESC4=$scope.admission.DIAGDESC4;
      $scope.ICDCODES.DATE4=$scope.admission.DIAGONSET4;
    }

    if ($scope.admission.DIAGCODE5) {
      var codeObj={
        code: "CODE5",
        date: "DATE5",
        desc: "DESC5",
        index: "5",
        codedetail: $scope.admission.DIAGCODE5,
        datedetail: new Date($filter("date")($scope.admission.DIAGONSET5, 'yyyy/MM/dd')),
        descdetail: $scope.admission.DIAGDESC5,
      };
      $scope.addICDArray.push(codeObj);
      index+=1;
      $scope.ICDCODES.CODE5=$scope.admission.DIAGCODE5;
      $scope.ICDCODES.DESC5=$scope.admission.DIAGDESC5;
      $scope.ICDCODES.DATE5=$scope.admission.DIAGONSET5;
    }

    if ($scope.admission.DIAGCODE6) {
      var codeObj={
        code: "CODE6",
        date: "DATE6",
        desc: "DESC6",
        index: "6",
        codedetail: $scope.admission.DIAGCODE6,
        datedetail: new Date($filter("date")($scope.admission.DIAGONSET6, 'yyyy/MM/dd')),
        descdetail: $scope.admission.DIAGDESC6,
      };
      $scope.addICDArray.push(codeObj);
      index+=1;
      $scope.ICDCODES.CODE6=$scope.admission.DIAGCODE6;
      $scope.ICDCODES.DESC6=$scope.admission.DIAGDESC6;
      $scope.ICDCODES.DATE6=$scope.admission.DIAGONSET6;
    }

    console.log($scope.addICDArray);
    console.log($scope.ICDCODES);
  }

  //get icd desc for first icd code
  $scope.updateICDInitial=function() {
    var ICDcodes = $scope.codes;

    for (i=0; i<ICDcodes.length; i++) {
      if ($scope.ICDCODE === ICDcodes[i].CODE) {
        $scope.ICDDESC = ICDcodes[i].DESCRIPTION;
        // $scope.ICDCODE=[ICDcodes[i].CODE.slice(0, 3), '.', ICDcodes[i].CODE.slice(3,ICDcodes[i].CODE.length)].join('')
        break;
      }
    }
  };

  //get icd desc for all other added icd codes
  $scope.updateICD = function(icd) {
    console.log($scope.ICDCODES)
    // console.log(icd)
    var ICDcodes = $scope.codes;
    // console.log(ICDcodes)

    Object.keys($scope.ICDCODES).forEach(function(key) {
      for (i=0; i<ICDcodes.length; i++) {
        if ($scope.ICDCODES[key] == ICDcodes[i].CODE) {
          // console.log($scope.ICDCODES[key])
          // console.log(ICDcodes[i].DESCRIPTION)
          var desc='DESC'+ icd.index.toString();
          $scope.ICDCODES[desc]=ICDcodes[i].DESCRIPTION;
          // $scope.add
          // console.log($scope.ICDCODES)
        }
      }
    });
  };


  //search for icd codes
  $scope.getICDCodes=function(val) {
    console.log('getting icd codes');
    return dataService.search('ICD10Code', {CODE:val, DESCRIPTION:val}).then(function (data) {
      $scope.codes=$filter('limitTo')(data.data, 15);
      console.log('$scope.codes',$scope.codes);
      return $scope.codes.map(function(code) {
        return code;
      });
    });
  };

  // $scope.updateICD2 = function() {
  //   console.log('updating')
  //   var ICDcodes = $scope.codes;
  //   for (i=0; i<ICDcodes.length; i++) {
  //     if ($scope.ICDCODE2 === ICDcodes[i].CODE) {
  //       $scope.ICDDESC2 = ICDcodes[i].DESCRIPTION;
  //       break;
  //     }
  //   }
  // }
  //
  // $scope.updateICD3 = function() {
  //   var ICDcodes = $scope.ICDcodes;
  //   for (i=0; i<ICDcodes.length; i++) {
  //     if ($scope.ICDCODE3 === ICDcodes[i].CODE) {
  //       $scope.ICDDESC3 = ICDcodes[i].DESCRIPTION;
  //       break;
  //     }
  //   }
  // }

  $scope['edit' + section] = true;
  $scope.disableEditMode = function(section) {
    if (confirm("Any changes will be lost, are you sure?")) {
      //do your process of delete using angular js.
      $scope.nonEditMode = true;
      $scope['edit' + section] = false;
      close();
    }
  };

  $scope.DOB = new Date($filter("date")($scope.patient.DOB, 'yyyy/MM/dd'));
  // $scope.REFERRDATE = new Date($scope.admission.REFERRDATE);



  $scope.saveChanges = function(form) {
    console.log(form);
    console.log($scope.addICDArray);
    console.log($scope.ICDCODES);

    if (form.$valid) {
      var patientChanges = {
        KEYID: $scope.patient.KEYID,
        FNAME: $scope.FNAME,
        DOB: $filter("date")($scope.DOB, 'yyyy-MM-dd'),
        MINITIAL: $scope.MINITIAL,
        GENDER: $scope.GENDER,
        LNAME: $scope.LNAME,
        SSN: $scope.SSN,
        MARITAL: $scope.MARITAL,
        LANGUAGE: $scope.LANGUAGE,
        M0140: $scope.M0140,
        RELIGION: $scope.RELIGION,
        NOTES: $scope.NOTES,

        LOCTYPE: $scope.LOCTYPE,
        ADDRESS: $scope.ADDRESS,
        CITY: $scope.CITY,
        STATE: $scope.STATE,
        ZIP: $scope.ZIP,
        PHONE1: $scope.PHONE1,
        CELLPHONE: $scope.CELLPHONE,
        EMAIL: $scope.EMAIL,
        ADDRESS_NOTES: $scope.ADDRESS_NOTES,
        // LOCTYPE2: $scope.LOCTYPE2,
        // ADDRESS2: $scope.ADDRESS2,
        // CITY2: $scope.CITY2,
        // STATE2: $scope.STATE2,
        // ZIP2: $scope.ZIP2,
        // PHONE2: $scope.PHONE2,

        INSCO: $scope.INSCO,
        INSID: $scope.INSID,
      };

      if ($scope.secondaryAddressCheckbox) {
        patientChanges.LOCTYPE2=$scope.LOCTYPE2;
        patientChanges.ADDRESS2=$scope.ADDRESS2;
        patientChanges.CITY2=$scope.CITY2;
        patientChanges.STATE2=$scope.STATE2;
        patientChanges.ZIP2=$scope.ZIP2;
        patientChanges.PHONE2=$scope.PHONE2;
      } else {
        patientChanges.LOCTYPE2="";
        patientChanges.ADDRESS2="";
        patientChanges.CITY2="";
        patientChanges.STATE2="";
        patientChanges.ZIP2="";
        patientChanges.PHONE2="";
      }

      var admissionChanges = {
        KEYID: $scope.admission.KEYID,

        LOCTYPE: $scope.LOCTYPE,
        ADDRESS: $scope.ADDRESS,
        CITY: $scope.CITY,
        STATE: $scope.STATE,
        ZIP: $scope.ZIP,

        ACUITY: $scope.ACUITY,
        //
        // PCGNAME: $scope.PCGNAME,
        // PCGPHONE: $scope.PCGPHONE,
        // PCGNOTES: $scope.PCGNOTES,
        //
        // ECNAME: $scope.ECNAME,
        // ECPHONE: $scope.ECPHONE,
        // ECNOTES: $scope.ECNOTES,

        REFRECEIVED: $scope.REFRECEIVED,
        REFERRSRC: $scope.REFERRSRC,
        REFERRDATE: $filter("date")($scope.REFERRDATE, 'yyyy-MM-dd'),
        REFERRPHYSICIAN: $scope.REFERRPHYSICIAN,
        ADMITSRCE: $scope.ADMITSRCE,
        REFERRNOTES: $scope.REFERRNOTES,
        SERVICE_SN: $scope.SERVICE_SN,
        SERVICE_PT: $scope.SERVICE_PT,
        SERVICE_OT: $scope.SERVICE_OT,
        SERVICE_ST: $scope.SERVICE_ST,
        SERVICE_MS: $scope.SERVICE_MS,

        //npi?
        INS1CO: $scope.INSCO,
        INS1ID: $scope.INSID,
        INS1PLAN: $scope.INS1PLAN,
        INS1GROUP: $scope.INS1GROUP,
        INS1AUTHCODE: $scope.INS1AUTHCODE,
        INS1NOTES: $scope.INS1NOTES,
        // INS2CO: $scope.INS2CO,
        // INS2ID: $scope.INS2ID,
        // INS2PLAN: $scope.INS2PLAN,
        // INS2GROUP: $scope.INS2GROUP,
        // INS2AUTHCODE: $scope.INS2AUTHCODE,
        // INS2NOTES: $scope.INS2NOTES,

        //F2F
        DOCTOR: $scope.DOCTORKEYID,
        DOCTOR2: $scope.DOCTOR2,
        DOCTOR3: $scope.DOCTOR3,
        DOCTOR4: $scope.DOCTOR4,
        DOCTOR5: $scope.DOCTOR5,
        PHARMACY: $scope.PHARMACY,
        DME: $scope.DME,
        CASEMNGR: $scope.CASEMNGR,
        DISCRN: $scope.DISCRN,
        DISCLPN: $scope.DISCLPN,
        DISCHA: $scope.DISCHA,
        DISCPT: $scope.DISCPT,
        DISCMS: $scope.DISCMS,
        DISCOT: $scope.DISCOT,
        DISCST: $scope.DISCST,
        DISCRD: $scope.DISCRD,
        MKTSOURCE: $scope.MKTSOURCE,

        //icd?
        DIAGCODE: $scope.ICDCODE,
        DIAGDESC: $scope.ICDDESC,
        DIAGONSET: $filter("date")($scope.ICDDATE, 'yyyy-MM-dd'),

        DIAGCODE2: "",
        DIAGDESC2: "",
        DIAGONSET2: "",
        DIAGCODE3: "",
        DIAGDESC3: "",
        DIAGONSET3: "",
        DIAGCODE4: "",
        DIAGDESC4: "",
        DIAGONSET4: "",
        DIAGCODE5: "",
        DIAGDESC5: "",
        DIAGONSET5: "",
        DIAGCODE6: "",
        DIAGDESC6: "",
        DIAGONSET6: "",
      };

      if ($scope.primaryCheckbox) {
        admissionChanges.PCGNAME=$scope.PCGNAME;
        admissionChanges.PCGNOTES=$scope.PCGNOTES;
        admissionChanges.PCGPHONE=$scope.PCGPHONE;
      } else {
        admissionChanges.PCGNAME="";
        admissionChanges.PCGNOTES="";
        admissionChanges.PCGPHONE="";
      }

      if ($scope.emergencyCheckbox) {
        admissionChanges.ECNAME=$scope.ECNAME;
        admissionChanges.ECNOTES=$scope.ECNOTES;
        admissionChanges.ECPHONE=$scope.ECPHONE;
      } else {
        admissionChanges.ECNAME="";
        admissionChanges.ECNOTES="";
        admissionChanges.ECPHONE="";
      }

      // if ($scope.secondaryInsuranceCheckbox) {
      //   admissionChanges.INS2CO=$scope.INS2CO;
      //   admissionChanges.INS2ID=$scope.INS2ID;
      //   admissionChanges.INS2PLAN=$scope.INS2PLAN;
      //   admissionChanges.INS2GROUP=$scope.INS2GROUP;
      //   admissionChanges.INS2AUTHCODE=$scope.INS2AUTHCODE;
      //   admissionChanges.INS2NOTES=$scope.INS2NOTES;
      // } else {
      //   admissionChanges.INS2CO="";
      //   admissionChanges.INS2ID="";
      //   admissionChanges.INS2PLAN="";
      //   admissionChanges.INS2GROUP="";
      //   admissionChanges.INS2AUTHCODE="";
      //   admissionChanges.INS2NOTES="";
      // }

      //get icdarray
      $scope.addICDArray.forEach(function (icdObj) {
        // console.log($scope.ICDCODES)
        Object.keys($scope.ICDCODES).forEach(function(key) {
          // console.log(key)
          if (icdObj.code===key) {
            // console.log($scope.ICDCODES[key]);
            icdObj.codeNum=$scope.ICDCODES[key];
          }
          if (icdObj.desc===key) {
            // console.log($scope.ICDCODES[key]);
            icdObj.descDesc=$scope.ICDCODES[key];
          }
          if (icdObj.date===key) {
            // console.log($scope.ICDCODES[key]);
            icdObj.dateDesc=$scope.ICDCODES[key];
          }
        });
      });

      // console.log('$scope.toDeleteArray', $scope.icdToDelete)
      // if ($scope.icdToDelete) {
      //   console.log($scope.icdToDelete)
      //   for (var j = 0; j < $scope.icdToDelete.length; j++) {
      //     console.log($scope.icdToDelete[j]);
      //     var code='DIAG' + $scope.icdToDelete[j].code;
      //     var desc='DIAG' + $scope.icdToDelete[j].desc;
      //     var date='DIAGONSET' + $scope.icdToDelete[j].index;
      //
      //     admissionChanges[code]="";
      //     admissionChanges[desc]="";
      //     // admissionChanges[date]="";
      //
      //     // admissionChanges[desc]=$scope.addICDArray[i].descDesc;
      //     // admissionChanges[date]=$filter("date")($scope.addICDArray[0].dateDesc, 'yyyy-MM-dd');
      //   }
      // }

      console.log('$scope.toAddArray', $scope.addICDArray)
      if ($scope.addICDArray) {
        for (var i = 0; i < $scope.addICDArray.length; i++) {
          var code='DIAGCODE'+(i+2);
          var desc='DIAGDESC'+(i+2);
          var date='DIAGONSET'+(i+2);
          admissionChanges[code]=$scope.addICDArray[i].codeNum;
          admissionChanges[desc]=$scope.addICDArray[i].descDesc;
          admissionChanges[date]=$filter("date")($scope.addICDArray[i].dateDesc, 'yyyy-MM-dd');
          // console.log('admission changes', admissionChanges)
        }
      }



      var doctorChanges = {
        KEYID: $scope.DOCTORKEYID,
        FNAME: $scope.DOCTORFNAME,
        LNAME: $scope.DOCTORLNAME,
        ADDRESS: $scope.DOCTORADDRESS,
        CITY: $scope.DOCTORCITY,
        STATE: $scope.DOCTORSTATE,
        ZIP: $scope.DOCTORZIP,
        PHONE: $scope.DOCTORPHONE,
        FAX: $scope.DOCTORFAX,
        EMAIL: $scope.DOCTOREMAIL,
        F2F: $scope.DOCTORF2F,
        POC: $scope.DOCTORPOC,
        NOTES: $scope.DOCTORNOTES

      }
      console.log('patientChages', patientChanges)
      console.log('doctorChanges', doctorChanges)
      console.log('admitChanges', admissionChanges);
      dataService.edit('patient', patientChanges).then(function(response) {
        console.log(response);
        // resetPatient();
        dataService.get('patient', {'KEYID' : patientChanges.KEYID}).then(function(data) {
          // console.log(data.data[0]);
          var updatedPatient = data.data[0];
          patientService.set(updatedPatient);
          $scope.updatedPatient = patientService.get();
          updateAdmission($scope.updatedPatient, admissionChanges)
        });
      });
      dataService.edit('doctor', doctorChanges).then(function(response){
        console.log(response)
      })
    } else {
      console.log('form invalid')
    }

  };

  function updateAdmission(updatedPatient, admissionChanges) {
    console.log('admissionChanges', admissionChanges)
    dataService.edit('admission', admissionChanges).then(function(response) {
      console.log(response);
      dataService.get('admission', {'KEYID' : $scope.admission.KEYID}).then(function(data) {
        $scope.updatedAdmission = data.data[0];
        console.log('$scope.updatedAdmission', $scope.updatedAdmission);
        close(updatedPatient,$scope.updatedAdmission);
      });
    });
  }
  // function resetPatient() {
  //   dataService.get('patient', {'KEYID' : patientService.get().KEYID}).then(function(data) {
  //     // console.log(data.data[0]);
  //     var updatedPatient = data.data[0];
  //     patientService.set(updatedPatient);
  //     $scope.updatedPatient = patientService.get();
  //
  //   });
  // }

  // function updateAdmission() {
  //   dataService.get('admission', {'KEYID' : $scope.admission.KEYID}).then(function(data) {
  //     $scope.updatedAdmission = data;
  //     close();
  //   });
  // }
  //
  function close(updatedPatient,updatedAdmission) {
    // console.log("closing modal")
    $uibModalInstance.close({updatedPatient:updatedPatient, updatedAdmission:updatedAdmission});
    // $uibModalInstance.close($scope.updatedAdmission);
    $state.go('patientProfile', {}, { reload: true });


  }

  // $scope.admission.REFERRDATE = $filter("date")($scope.admission.REFERRDATE, 'yyyy-MM-dd');

  //modal confirmation
  $scope.$on('modal.closing', function(event, reason, closed) {
      switch (reason){
          // clicked outside
          case "backdrop click":
              var message = "Any changes will be lost, are you sure?";
              break;
          // escape key
          case "escape key press":
              var message = "Any changes will be lost, are you sure?";
              break;
      }

      if (reason==='backdrop click') {
        if (!confirm(message)) {
            event.preventDefault();
        }
      }

      if (reason==='escape key press') {
        if (!confirm(message)) {
            event.preventDefault();
        }
      }

  });


  //adding additional icd codes
  // var index=1;
  $scope.addICDArray=[];
  $scope.ICDCODES={};
  $scope.addICD=function() {
    console.log('adding icd');
    console.log($scope.ICDCODES);
    index+=1;
    // var showICD='addICD' + $scope.counterArray[index];
    // $scope[showICD]=true;
    //
    // $scope.counterArray.splice(index, 1);
    // console.log($scope.counterArray);
    var icdObj={
      // update: "ICD"+index.toString(),
      code:"CODE"+index,
      // loading: "ICD"+index.toString()+"Codes",
      // noResults: "Results"+index.toString(),
      date: "DATE" + index,
      // // openDate: "openedICDDATE"+index.toString(),
      desc: "DESC"+index.toString(),
      index:index.toString()
      // code:
    }

    $scope.addICDArray.push(icdObj);
    console.log('adding $scope.addICDArray', $scope.addICDArray)
  };

  $scope.icdToDelete=[];
  //deleting added icd code
  $scope.deleteICD=function(icd) {
    console.log('deleting', icd)
    console.log($scope.ICDCODES);

    if ($scope.ICDCODES) {
      console.log(icd.code)
      var code = icd.code;
      var date = icd.date;
      var desc = icd.desc;
      $scope.ICDCODES[code]=null;
      $scope.ICDCODES[date]=null;
      $scope.ICDCODES[desc]=null;
    }

    $scope.icdToDelete.push(icd);

    // console.log($scope.addICDArray);
    var i=$scope.addICDArray.indexOf(icd);
    // console.log(i)
    $scope.addICDArray.splice(i, 1);
    // $scope.addICDArray[i].code=null;
    // $scope.addICDArray[i].date=null;
    // $scope.addICDArray[i].desc=null;

    console.log($scope.addICDArray);
    console.log($scope.ICDCODES);
  }
  findICDINFO();

});
