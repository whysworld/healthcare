app.controller('ViewOrderController', function($scope, $state, $filter, $uibModal, $uibModalInstance,dataService, patientService,form, formAdded) {

  $scope.form=form;
  console.log('$scope.form', form);

  dataService.getAll('doctor').then(function(data) {
    $scope.doctors=data.data;
  });

  $scope.close=function () {
    $uibModalInstance.close();
  };

  $scope.isDisabled=true;

  $scope.populateOrder=function () {
    $scope.PATKEYID=form.PATKEYID;
    $scope.ADMITKEYID=form.ADMITKEYID;
    $scope.EPIKEYID=form.EPIKEYID;
    $scope.DOCTOR=form.DOCTORID;
    $scope.ORDERSDATE=new Date($filter("date")(form.ORDERSDATE, 'yyyy/MM/dd'));
    $scope.SUBJECT=form.SUBJECT;
    $scope.DESCRIPTION=form.DESCRIPTION;

    if (form.PHYSICIANSIGNDATE) {
      form.PHYSICIANSIGNDATE=$filter("date")(new Date(form.PHYSICIANSIGNDATE), 'MM-dd-yyyy')
    }
  };

  $scope.approve=function () {
    //edit form status to 3=approved;
    dataService.edit('form', {KEYID:$scope.form.FORMKEYID, STATUS:3}).then(function (response) {
      console.log(response);
      $scope.close();
    });
  };

  $scope.reject=function () {
    //change form status to in progress, and attach note?
    var form={
      KEYID: $scope.form.FORMKEYID,
      STATUS: 4,
    };
    dataService.edit('form', form).then(function (response) {
      console.log(response);
      $scope.close();
    });
  };

  $scope.saveChanges=function (form) {
    if (form.$valid) {
      var orderChanges={
        KEYID: $scope.form.KEYID,
        DOCTOR: $scope.DOCTOR,
        ORDERSDATE: $filter("date")($scope.ORDERSDATE, 'yyyy/MM/dd'),
        SUBJECT: $scope.SUBJECT,
        DESCRIPTION: $scope.DESCRIPTION,
      };
      console.log('order changes', orderChanges)
      dataService.edit('orders', orderChanges).then(function (response) {
        console.log(response);
        if (response.status==='success') {
          $scope.isDisabled=true;
        }
      });
    }
  };

  $scope.cancel=function () {
    $scope.isDisabled=true;
    $scope.populateOrder();
  };

  $scope.populateOrder();
});
