app.controller('NewIntermediaryController', function ($scope, $uibModalInstance, dataService) {

  $scope.close = function () {
    $uibModalInstance.close()
  }

  $scope.addIntermediary = function (form) {
    if (form.$valid) {
      var newIntermediary = {
        NAME: $scope.NAME,
        ADDRESS: $scope.ADDRESS,
        SUBMITTER: $scope.SUBMITTER,
        ECFPATH: $scope.ECFPATH,
        ECFNAME: $scope.ECFNAME,
        ECFFORMAT: $scope.ECFFORMAT,
        CONTACT: $scope.CONTACT,
        DEPT: $scope.DEPT,
        PHONEEXT: $scope.PHONEEXT,
        PHONE: $scope.PHONE,
        FAX: $scope.FAX,
        NOTES: $scope.NOTES,
        RCVRNAME: $scope.RCVRNAME
      }
      dataService.add('intermediary', newIntermediary).then(function (response) {
        if (response.status === 'success') {
          $scope.close()
        }
      })
    }
  }
})
