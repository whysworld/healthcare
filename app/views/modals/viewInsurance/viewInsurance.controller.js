app.controller('ViewInsuranceController', function($scope, $uibModalInstance, insurance, dataService) {

  $scope.insurance = insurance;
  console.log('$scope.insurance', $scope.insurance);
  //
  $scope.close = function() {
    $uibModalInstance.close();
  };

  dataService.getAll('intermediary').then(function (data) {
    $scope.intermediaries = data.data;
    // console.log('$scope.intermediaries', $scope.intermediaries);
  });

  $scope.noEdit = true;

  $scope.editInsurance = function(form) {
    if (form.$valid) {
      var insuranceChanges = {
        KEYID: $scope.insurance.KEYID,
        NAME: $scope.NAME,
        ADDRESS: $scope.ADDRESS,
        CITY: $scope.CITY,
        STATE: $scope.STATE,
        ZIP: $scope.ZIP,
        PHONE: $scope.PHONE,
        TYPE: $scope.TYPE,
        PROVIDERID: $scope.PROVIDERID,
        INTERMED: $scope.INTERMED,
        // INTERMED: null, //change this once intermed table is ready
        PAYOR_NAME: $scope.PAYOR_NAME,
        PAYOR_ID: $scope.PAYOR_ID,
        CLAIMTYPE: $scope.CLAIMTYPE,
        NOUNITRND: $scope.NOUNITRND,
        CMBNSDVSTS: $scope.CMBNSDVSTS,
        SPANEPI: $scope.SPANEPI,
        SPPLYDTL: $scope.SPPLYDTL,
        CLMOVERLAP: $scope.CLMOVERLAP,
        CLMSHRINK: $scope.CLMSHRINK,
        MULTIPAGE: $scope.MULTIPAGE,
        NOTES: $scope.NOTES,
        USENPI1: $scope.USENPI1,
        USENPI2: $scope.USENPI2
      };
      console.log('edit insurance', insuranceChanges);
      dataService.edit('insurance', insuranceChanges).then(function (response) {
        console.log(response);
        $scope.close();
      });
    }
  };

  $scope.deleteInsurance=function (insurance) {
    if (confirm('Are you sure you want to delete insurance?')) {
      console.log('deleting insurance', insurance);
      dataService.delete('insurance', {KEYID:insurance.KEYID}).then(function (response) {
        console.log(response);
        if (!response) {
          alert('Error: Cannot delete insurance - insurance is currently being used.');
        } else if (response.status==='success'){
          $scope.close();
        }
      });
    }
  };

});
