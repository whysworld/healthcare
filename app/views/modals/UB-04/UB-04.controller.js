app.controller('UB04Controller', function($scope, $uibModalInstance, dataService, claim) {
    $scope.claim = {}

    // We need to get the details for the selected claim
    dataService.get('claim', {KEYID: claim.KEYID}).then(function(data) {
        $scope.claim = data.data
    })

    $scope.close = function() {
        $uibModalInstance.close()
    }
})
