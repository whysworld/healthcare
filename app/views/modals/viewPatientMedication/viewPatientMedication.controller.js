app.controller('ViewPatientMedicationController', function($scope, $filter, $uibModal, $uibModalInstance, patientMedication, dataService, patientService, getPatientMedicationList) {

  $scope.patient = patientService.get();

  if (patientMedication.DATE_STARTED != null)
 	 $scope.dateStarted = new Date($filter("date")(patientMedication.DATE_STARTED, 'yyyy/MM/dd'));

  if (patientMedication.DATE_DISC != null)
      $scope.dateDisc = new Date($filter("date")(patientMedication.DATE_DISC, 'yyyy/MM/dd'));

  $scope.medication = patientMedication;

      $scope.getMedications=function(val) {
        console.log('getting medications:searching...', val);
        return dataService.search('Medication', {CODE:val, DESCRIPTION:val}).then(function (data) {
          $scope.codes=$filter('limitTo')(data.data, 15);
          console.log('$scope.codes',$scope.codes);
          return $scope.codes.map(function(code) {
            return code;
          });
        });
  };

  console.log($scope.medication);

  $scope.close = function() {
    $uibModalInstance.close();
  }

  $scope.editPatientMedication = function(isValid) {
    if (isValid === false) {
      console.log("Form not valid");
    } else {
      // If the medication is valid, save it.
      //
      var editedMedication = {
        KEYID : $scope.medication.KEYID,
        ADMINBY : $scope.medication.ADMINBY,
        CODE : $scope.medication.CODE,
        DATE_DISC : $filter("date")($scope.dateDisc, 'yyyy-MM-dd'),
        DATE_STARTED : $filter("date")($scope.dateStarted, 'yyyy-MM-dd'),
        DOSE : $scope.medication.DOSE,
        UNIT : $scope.medication.UNIT,
		FORM : $scope.medication.FORM,
        TAKEN : $scope.medication.TAKEN,
        FREQUENCY : $scope.medication.FREQUENCY,
        NAME : $scope.medication.NAME,
        PATID : $scope.patient.KEYID,
        PHARMACY : $scope.medication.PHARMACY,
        PURPOSE : $scope.medication.PURPOSE,
        SIDE_EFFECTS : $scope.medication.SIDE_EFFECTS,
      };

      dataService.edit('patientMedication', editedMedication).then(function(response) {
        console.log(response);
        getPatientMedicationList();
        $scope.close();
      });
    }
  }

  $scope.deletePatientMedication = function() {
    dataService.delete("patientMedication", {KEYID: $scope.medication.KEYID}).then(function(data) {
      console.log(data);
      getPatientMedicationList();
      $scope.close();
    })
  }
});
