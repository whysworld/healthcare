app.controller('ScheduleSOCController', function($scope, $filter, $uibModalInstance, dataService, patientService) {

  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.patient = patientService.get();

  dataService.get('visitType', {'ACTIVE': 1}).then(function(data){
    var visitTypes = data.data;
    $scope.visitTypes = visitTypes;
  });

  dataService.getAll('employee').then(function(data){
    $scope.employees = data.data;
  });

  dataService.get('visit', {'PATKEYID': $scope.patient.KEYID}).then(function(data){
    $scope.visits=data.data;
  });

  //Autopopulate TIMEOUT based on TIMEIN + default hours
 $scope.changedTIMEIN = function(){
   if ($scope.TIMEIN !== null) {
     var timein = new Date($scope.TIMEIN);
     $scope.TIMEOUT = timein.setHours(timein.getHours()+$scope.HOURS);
     $scope.TIMEOUT = new Date($scope.TIMEOUT);
   }
 };

 //calculate HOURS based on TIMEOUT
 $scope.changed = function() {
   //if TIMEOUT is after TIMEIN
   if ($scope.TIMEOUT.getHours()> $scope.TIMEIN.getHours()) {
     var hourDiff = $scope.TIMEOUT.getHours() - $scope.TIMEIN.getHours();
     var minDiff = $scope.TIMEOUT.getMinutes() - $scope.TIMEIN.getMinutes();
     //Round hour by half hour increments
     if ( (hourDiff===0) && (minDiff>0) ) {
       $scope.HOURS = 1;
     } else if ( (hourDiff>0) && (minDiff>29) ) {
       $scope.HOURS = hourDiff + 1;
     } else if ( (hourDiff>0) && (minDiff<30) ) {
       $scope.HOURS = hourDiff;
     }
   } else {
     $scope.HOURS = null;
   }
 };

   //validate date to make sure scheduled visit is not before today's date
  // $scope.dateOptions = {
  //   minDate: new Date(),
  // };


  $scope.newVisit = function(form) {
      console.log(form);
    // console.log($filter("date")($scope.VISITDATE, 'MM-dd-yyyy'))
    $scope.VISITDATE = $filter("date")($scope.VISITDATE, 'yyyy-MM-dd');
    console.log($scope.VISITDATE);
    if ((form.EMPKEYID.$valid) && ($scope.VISITDATE !==undefined)) {
      if ($scope.TIMEIN !==undefined) {
        //Format TIMEIN and TIMEOUT
        var hourIN = $scope.TIMEIN.getHours();
        var hourOUT = $scope.TIMEOUT.getHours();
        var minOUT = $scope.TIMEOUT.getMinutes();
        var minIN = $scope.TIMEIN.getMinutes();

        if (minIN < 10) {
          minIN = minIN.toString();
          minIN = '0' + minIN;
        } else {
          minIN = minIN.toString();
        }
        if (minOUT<10){
          minOUT = minOUT.toString();
          minOUT='0'+ minOUT;
        } else {
          minOUT = minOUT.toString();
        }
        if (hourOUT<10){
          hourOUT = hourOUT.toString();
          hourOUT='0'+ hourOUT;
        } else {
          hourOUT = hourOUT.toString();
        }
        if (hourIN<10){
          hourIN = hourIN.toString();
          hourIN='0'+ hourIN;
        } else {
          hourIN = hourIN.toString();
        }

        $scope.TIMEIN = hourIN + minIN + '00';
        $scope.TIMEOUT = hourOUT + minOUT + '00';

      } else {
          $scope.TIMEIN = null;
          $scope.TIMEOUT = null;
        }

      var createVisit = {

        PATKEYID: $scope.patient.KEYID,
        ADMITKEYID: $scope.patient.LSTADMIT,
        EPIKEYID: null, //there is no epikeyid yet b/c episode hasn't been created yet
        EMPKEYID: $scope.EMPKEYID.KEYID,
        VISITTYPE: $scope.VISITTYPE.KEYID,
        DISCIPLINE: $scope.DISCIPLINE,
        VISITDATE: $filter('date')($scope.VISITDATE, 'yyyy-MM-dd'),
        TIMEIN: $scope.TIMEIN,
        TIMEOUT: $scope.TIMEOUT,
        HOURS: $scope.HOURS,
        MILEAGE: $scope.MILEAGE,
        NOTES: $scope.NOTES,
      };
      console.log(createVisit);
      dataService.add('visit', createVisit).then(function(response){
        console.log(response);


        $scope.selectedForms.forEach(function(selectedFormType) {
          var form = {
            PATKEYID : $scope.patient.KEYID,
            // FIXME: LSTADMIT is just the last admit, we may
            // want to add a visit for a previous admit
            ADMITKEYID : $scope.patient.LSTADMIT,
            EPIKEYID : null, //there is no epikeyid yet b/c episode hasn't been created yet
            VISITID : response.keyid,
            FORMTYPE : selectedFormType.KEYID,
            FORMDATE : $filter('date')($scope.VISITDATE, 'yyyy-MM-dd'),
            ASSIGNEDTO : $scope.EMPKEYID.KEYID,
            STATUS : 0
          };
          dataService.add('form', form).then(function(msg) {
            console.log(msg);
          });

          if (selectedFormType.KEYID === 2) {
            console.log("Asdfsa")
            editAdmit();
          }
        });

        $uibModalInstance.close();
      });

    }
  };

  //Get currentAdmitKEYID
  dataService.get('admission', {'PATKEYID':$scope.patient.KEYID}).then(function(data){
    var admitArr = data.data;
    var currentAdmit = admitArr.slice(-1).pop();
    $scope.currentAdmitKEYID = currentAdmit.KEYID;
  });

  //edit Admit and Patient STATUS from 'P' to 'A'
  function editAdmit() {
    var testAdmit = {
      KEYID: $scope.currentAdmitKEYID,
      PATSTATUS: 'A',
      SOCDATE: $scope.M0030_START_CARE_DT
    };
    dataService.edit('admission', testAdmit).then(function(response){
        console.log(response);
    });
    //edit patient table to udpate PATSTATUS from 'P' to 'A'
    dataService.edit('patient', {'KEYID': $scope.patient.KEYID, 'PATSTATUS': 'A'}).then(function(response){
      console.log(response);
      //reset patient
      dataService.get('patient', {'KEYID': patientService.get().KEYID}).then(function(data) {
        var updatedPatient = data.data[0];
        patientService.set(updatedPatient);
      });
    });
  }

  $scope.selectedForms = [];

  $scope.addForm = function(selectForm) {
    var selectedForms = $scope.selectedForms;
    //If form is not already selected
    if (selectedForms.indexOf(selectForm) === -1 ) {
      $scope.selectedForms.push(selectForm);
    }
    else {
      alert("This form has already be selected");
    }
    console.log($scope.selectedForms);
  };

  $scope.removeForm = function(form) {
    var index = $scope.selectedForms.indexOf(form);
    $scope.selectedForms.splice(index, 1);
    console.log($scope.selectedForms);
  };


  $scope.updateVisitTypeDetails = function() {
    console.log("visit type to update");
    console.log($scope.VISITTYPE);

    $scope.getForms($scope.VISITTYPE.KEYID);
    $scope.HOURS = parseFloat($scope.VISITTYPE.DFLTHRS);
    $scope.changedTIMEIN();
  };

  // Get all the available form types
  dataService.getAll('formType').then(function(data){
    var formTypes = data.data;

    //if patient status is transfer, on page load, ROC form has to be selected and not able to be removed
    // if ($scope.patient.PATSTATUS==='T') {
    //   var index = formTypes.findIndex(x => x.KEYID===3);
    //   $scope.socForm = formTypes[index];
    //   $scope.selectedForms.push($scope.socForm);
    //
    //   formTypes.splice(index, 1);
    //   $scope.formTypes = formTypes;
    // } else {

    
      //on page load, SOC form has to be selected and not able to be removed
      var index = formTypes.findIndex(x => x.KEYID===2);
      $scope.socForm = formTypes[index];
      $scope.selectedForms.push($scope.socForm);

      formTypes.splice(index, 1);
      $scope.formTypes = formTypes;
    // }
  });

    //Get list of formType corresponding to selected visitType
    $scope.getForms = function(visitTypeID) {
      dataService.get('visitTypeForm', {'VISITTYPE_ID': visitTypeID}).then(function(data){
        console.log("visit type forms:", data.data);
        var visitTypeFormArr= data.data
        var index;
        for (var i = 0; i < visitTypeFormArr.length; i++) {
          //remove all forms that are FORMTYPE_ID ===2 and not VISITTYPE_ID ===1 <--not Start of Care
          if ((visitTypeFormArr[i].FORMTYPE_ID === 2) && (visitTypeFormArr[i].VISITTYPE_ID !==1)) {
            index = visitTypeFormArr.indexOf(visitTypeFormArr[i]);
            visitTypeFormArr.splice(index, 1);
          }
          // console.log(visitTypeFormArr);
        };

        $scope.selectedForms = [];
        $scope.selectedForms.push($scope.socForm);
        for (var i = 0; i < visitTypeFormArr.length; i++) {
          for (var j = 0; j < $scope.formTypes.length; j++) {
            if (data.data[i].FORMTYPE_ID == $scope.formTypes[j].KEYID) {
              console.log("adding form type: " + $scope.formTypes[j].KEYID);
              $scope.selectedForms.push($scope.formTypes[j]);

            }
          }
        }
          console.log('selectedForms', $scope.selectedForms)
      });
    };


  //
  //   //Get list of formType corresponding to selected visitType
  //   dataService.get('visitTypeForm', {'VISITTYPE_ID': 1}).then(function(data){
  //     console.log("visit type forms:");
  //     console.log(data.data);
  //
  //     $scope.selectedForms = [];
  //
  //     for (var i = 0; i < data.data.length; i++) {
  //       for (var j = 0; j < $scope.formTypes.length; j++) {
  //         if (data.data[i].FORMTYPE_ID == $scope.formTypes[j].KEYID) {
  //           console.log("adding form type: " + $scope.formTypes[j].KEYID);
  //           $scope.selectedForms.push($scope.formTypes[j]);
  //         }
  //       }
  //     }
  //   });
  // });

});
