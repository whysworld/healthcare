app.controller('ViewEmployeePayRates', function($scope, $uibModalInstance, dataService, employee, visitType) {

  $scope.close=function () {
    if (confirm("Any unsaved changes will be lost, are you sure you want to exit?")) {
      $uibModalInstance.close();
    }
  };

  $scope.visitType=visitType;
  console.log('visitType', visitType);

  $scope.editPayrate=function(form) {
    $scope.invalid=false;
    if($scope.customEmpPayrate==='') {
      $scope.invalid=true;
    } else {
      var payrateChanges={
        EMPLOYEE_ID: employee.KEYID,
        VISITTYPE_ID: visitType.KEYID,
        EMPPAY: $scope.customEmpPayrate,
        EMPPAY_PERVISIT: $scope.PERVISIT
      };
      console.log('payrateChanges', payrateChanges);
      dataService.edit('employeePayrate', payrateChanges).then(function (response) {
        console.log('edit payrate', response);
        if(response.status==='success'){
          $uibModalInstance.close();
        }
      });
    }
  };

  //closing modal confirmation
  //modal confirmation
  $scope.$on('modal.closing', function(event, reason, closed) {
      switch (reason){
          // clicked outside
          case "backdrop click":
              var message = "Any unsaved changes will be lost, are you sure you want to exit?";
              break;
          // escape key
          case "escape key press":
              var message = "Any unsaved changes will be lost, are you sure you want to exit?";
              break;
      }

      if (reason==='backdrop click') {
        if (!confirm(message)) {
            event.preventDefault();
        }
      }

      if (reason==='escape key press') {
        if (!confirm(message)) {
            event.preventDefault();
        }
      }
  });

});
