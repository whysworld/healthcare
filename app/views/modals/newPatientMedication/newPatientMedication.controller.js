app.controller('NewPatientMedicationController', function($scope, $filter, $uibModal, $uibModalInstance, dataService, patientService, getPatientMedicationList) {

  $scope.patient = patientService.get();

  $scope.close = function() {
    $uibModalInstance.close();
  }

    $scope.getMedications=function(val) {
      console.log('getting medications:searching...', val);
      return dataService.search('Medication', {CODE:val, DESCRIPTION:val}).then(function (data) {
        $scope.codes=$filter('limitTo')(data.data, 15);
        console.log('$scope.codes',$scope.codes);
        return $scope.codes.map(function(code) {
          return code;
        });
      });
  };

  $scope.addPatientMedication = function(isValid) {
    if (isValid === false) {
      console.log("Form not valid");
    } else {
      // If the medication is valid, save it.
      //
      var newMedication = {
        ADMINBY : $scope.newMedication.ADMINBY,
        CODE : $scope.newMedication.CODE,
        DATE_DISC : $filter("date")($scope.newMedication.DATE_DISC, 'yyyy-MM-dd'),
        DATE_STARTED : $filter("date")($scope.newMedication.DATE_STARTED, 'yyyy-MM-dd'),
        DOSE : $scope.newMedication.DOSE,
        UNIT : $scope.newMedication.UNIT,
        FORM : $scope.newMedication.FORM,
        TAKEN : $scope.newMedication.TAKEN,
        FREQUENCY : $scope.newMedication.FREQUENCY,
        NAME : $scope.newMedication.NAME,
        PATID : $scope.patient.KEYID,
        PHARMACY : $scope.newMedication.PHARMACY,
        PURPOSE : $scope.newMedication.PURPOSE,
        SIDE_EFFECTS : $scope.newMedication.SIDE_EFFECTS,
      };

      dataService.add('patientMedication', newMedication).then(function(response) {
        console.log(response);
        getPatientMedicationList();
        $scope.close();
      });
    }
  }
});
