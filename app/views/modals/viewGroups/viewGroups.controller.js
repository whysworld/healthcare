app.controller('ViewGroupsController', function($scope, $uibModalInstance, userGroups, dataService) {

  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.userGroups = userGroups;

  $scope.createNewUserGroup = function() {
    // console.log("form",form)
    var newUserGroup = {
      DESCRIPTION: $scope.DESCRIPTION,
      ASSESSMENTS: $scope.ASSESSMENTS,
      ORDERS: $scope.ORDERS,
      CARE_PLANS: $scope.CARE_PLANS,
      PATIENT_SCHEDULING: $scope.PATIENT_SCHEDULING,
      VISITS: $scope.VISITS,
      CLAIMS: $scope.CLAIMS,
      EMPLOYEE_INFORMATION: $scope.EMPLOYEE_INFORMATION,
      EMPLOYEE_PAYROLL: $scope.EMPLOYEE_PAYROLL,
      EMPLOYEE_SCHEDULING: $scope.EMPLOYEE_SCHEDULING,
      BILLING: $scope.BILLING,
      QA_TOOLS: $scope.QA_TOOLS,
      REPORTS: $scope.REPORTS,
      CREATE_OASIS_EXPORT: $scope.CREATE_OASIS_EXPORT,
      BACKUP: $scope.BACKUP,
      ARCHIVE_PATIENTS: $scope.ARCHIVE_PATIENTS,
      CONFIGURATION: $scope.CONFIGURATION
      // libraryFiles: $scope.libraryFiles,
      // qaTools: form.qaTools,
      // reportsMenu: form.reportsMenu,
      // utilitiesMenu: form.utilitiesMenu.$modelValue,
      // internetUpdates: form.internetUpdates.$modelValue,
      // backupDataFiles: form.backupDataFiles.$modelValue,
      // archivePatients: form.archivePatients.$modelValue,
      // rebuildDataFiles: form.rebuildDataFiles.$modelValue,
      // managementMenu: form.managementMenu.$modelValue,
      // accessControl: form.accessControl.$modelValue,
      // supportTablesMaintenance: form.supportTablesMaintenance.$modelValue,
      // reportClinical: form.reportClinical.$modelValue,
      // reportVisits: form.reportVisits.$modelValue,
      // reportAccounting: form.reportAccounting.$modelValue,
      // reportEmployee: form.reportEmployee.$modelValue,
      // reportScheduling: form.reportScheduling.$modelValue,
    };
    dataService.add('userGroup', newUserGroup).then(function(response) {
      console.log(response);
      $uibModalInstance.close();
    });
    console.log(newUserGroup);
  };

  $scope.userGroupsCopy = []

  $scope.updateUserGroup = function(selectUserGroup) {
    $scope.isCheckboxDisabled = true;
    $scope.editModeBtn = true;
    angular.copy(userGroups, $scope.userGroupsCopy);
    $scope.activeUserGroup = selectUserGroup;
    $scope.expandPatientInfo = true;
    $scope.checkEmployeeInfo = true;
    $scope.expandEmployeeInfo = true;
    $scope.checkEmployeeInfo = true;
    $scope.expandVisitEntry = true;
    $scope.checkVisitEntry = true;
    $scope.expandOasisExport = true;
    $scope.checkOasisExport = true;
    $scope.expandProgramConfig = true;
    $scope.checkProgramConfig = true;
    $scope.expandPatient = true;
    $scope.checkPatient = true;
    $scope.showEditGroupBtn = true;
  };

  $scope.isCheckboxDisabled = true
  // $scope.editMode = false;

  $scope.enableEditGroup = function() {
    $scope.isCheckboxDisabled = false;
    $scope.editModeActionBtn = true;

    $scope.editMode = true;
    $scope.newForm = false;
  };

  $scope.cancel = function() {
    $scope.isCheckboxDisabled = true;
    $scope.editModeBtn = true;
    $scope.editModeActionBtn = false
    $scope.editMode = false;
    $scope.newForm = false;
    // $scope.userGroups = angular.copy($scope.userGroupsCopy);
    // console.log($scope.groupName.pristine())
  };

  // $scope.newForm = false;
  $scope.openNewUserGroupForm = function(userGroups) {
    $scope.newForm = true;
  };

  $scope.cancelNewForm = function(newUserGroup){
    $scope.newForm = false;
  };

  $scope.saveUserGroupEdits = function () {
    $scope.editModeBtn = true;
    $scope.editModeActionBtn = false;
    $scope.editMode = false;
    $scope.isCheckboxDisabled = true;
    var editedUserGroup = {
      KEYID: $scope.activeUserGroup.KEYID,
      DESCRIPTION: $scope.activeUserGroup.DESCRIPTION,
      ASSESSMENTS: $scope.activeUserGroup.ASSESSMENTS,
      ORDERS: $scope.activeUserGroup.ORDERS,
      CARE_PLANS: $scope.activeUserGroup.CARE_PLANS,
      PATIENT_SCHEDULING: $scope.activeUserGroup.PATIENT_SCHEDULING,
      VISITS: $scope.activeUserGroup.VISITS,
      CLAIMS: $scope.activeUserGroup.CLAIMS,
      EMPLOYEE_INFORMATION: $scope.activeUserGroup.EMPLOYEE_INFORMATION,
      EMPLOYEE_PAYROLL: $scope.activeUserGroup.EMPLOYEE_PAYROLL,
      EMPLOYEE_SCHEDULING: $scope.activeUserGroup.EMPLOYEE_SCHEDULING,
      BILLING: $scope.activeUserGroup.BILLING,
      QA_TOOLS: $scope.activeUserGroup.QA_TOOLS,
      REPORTS: $scope.activeUserGroup.REPORTS,
      CREATE_OASIS_EXPORT: $scope.activeUserGroup.CREATE_OASIS_EXPORT,
      BACKUP: $scope.activeUserGroup.BACKUP,
      ARCHIVE_PATIENTS: $scope.activeUserGroup.ARCHIVE_PATIENTS,
      CONFIGURATION: $scope.activeUserGroup.CONFIGURATION
    };
    dataService.edit('userGroup', editedUserGroup).then(function(response) {
      console.log(response);
    });
    console.log(editedUserGroup);
  };

  $scope.deleteUserGroup = function (userGroup) {
    console.log(userGroup);
    dataService.delete('userGroup', userGroup).then(function(response) {
      console.log(response);
    });
  };
});
