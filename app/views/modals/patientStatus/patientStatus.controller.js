app.controller('PatientStatusController', function($scope, $state, $filter, $uibModal, $uibModalInstance, patient, admission, dataService, patientService, CONSTANTS) {

  $scope.patient = patient;
  $scope.admission = admission;

  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.nonadmitReasons=CONSTANTS.nonadmitReasons
  // $scope.discharge=false

  //check if epi has been created for current admit
  dataService.get('episode', {KEYID:$scope.patient.LSTADMIT}).then(function(data) {
    console.log(data.data)
  })

  console.log($scope.admission);
  console.log($scope.patient);

  $scope.status=function (status){
    switch(status) {
      case "P":
      if($scope.patient.PATSTATUS==='P'){
        $scope.nonadmit=false;
        $scope.scheduleSOCVisit=false;
        $scope.scheduleNewVisit=false;
        $scope.saveChanges=false;
      } else {
        $scope.saveChanges=true;
        $scope.pendingMsg=true;
      }
        break;
      case "N":
      if($scope.patient.PATSTATUS==='P'){
        $scope.scheduleNewVisit=false;
        $scope.scheduleSOCVisit=false;
        $scope.nonadmit=true;
        $scope.saveChanges=true;
      } else if ($scope.patient.PATSTATUS==='N') {
          $scope.nonadmit=false;
          $scope.scheduleNewVisit=false;
          $scope.scheduleSOCVisit=false;
            $scope.saveChanges=false;
      } else {
        $scope.nonadmit=true;
        $scope.saveChanges=true;
      }
        break;
      case "A":
        $scope.saveChanges=false;
        if($scope.patient.PATSTATUS==='A') {
          $scope.scheduleSOCVisit=false;
          $scope.scheduleNewVisit=false
        } else if($scope.patient.PATSTATUS==='T'){
          $scope.scheduleSOCVisit=false;
          $scope.scheduleNewVisit=true;
        } else {
          $scope.scheduleSOCVisit=true;
          $scope.scheduleNewVisit=false
          $scope.nonadmit=false;
        }
        break;
      case "D":
        if($scope.patient.PATSTATUS==='D'){
          $scope.scheduleSOCVisit=false;
          $scope.scheduleNewVisit=false
        } else {
         if($scope.admission.SOCDATE !== null) { //discharging patient after SOC forms saved (episode created)
            $scope.saveChanges=false;
            $scope.scheduleNewVisit=true;
            $scope.scheduleSOCVisit=false;
          } else { //discharging patient after a SOC has been scheduled but no forms saved (episode not created)
            $scope.pendingMsg=true;
            $scope.saveChanges=true;
          }
        }
        break;
      case "T":
        $scope.saveChanges=false;
        $scope.scheduleNewVisit=true;
        if($scope.patient.PATSTATUS==='T'){
          $scope.scheduleSOCVisit=false;
          $scope.scheduleNewVisit=false;
        }
        break;
    }
  };

  $scope.scheduleSOC=function() {
    if ($scope.patient.PATSTATUS ==='D') {
      openScheudleAfterDischargeModal();
    } else {
      openScheduleSOCModal();
    }
  }

  function openScheudleAfterDischargeModal () {
    event.preventDefault();
    var modalInstance = $uibModal.open({
      controller: 'ScheduleAfterDischargeController',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/scheduleAfterDischarge/scheduleAfterDischarge.html',
      size: 'lg',
      resolve: {
        selected: null
      }
    });
    modalInstance.result.then(function (updatedPatient) {
      $uibModalInstance.close(updatedPatient);
    });
  }

  function openScheduleSOCModal(){
    event.preventDefault();
    var modalInstance = $uibModal.open({
      controller: 'ScheduleSOCController',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/scheduleSOC/scheduleSOC.html',
      size: 'lg',
      resolve: {
      }
    });
    modalInstance.result.then(function () {
      $scope.save();
    });
  }

  $scope.openNewVisitModal=function() {
    dataService.get('episode', {ADMITKEYID: $scope.patient.LSTADMIT}).then(function(data) {
      var episode = data.data[data.data.length-1];
      event.preventDefault();
      var modalInstance = $uibModal.open({
        controller: 'NewVisitController',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: './views/modals/newVisit/newVisit.html',
        size: 'lg',
        resolve: {
          selected: null,
          patient: $scope.patient,
          episode: episode,
          action: function(){
            return $scope.PATSTATUS;
          }
        }
      });
      modalInstance.result.then(function () {
          //close modal and go to records page
          $uibModalInstance.close();
          $state.go('records');
      });
    });
  };

  $scope.save = function() {
    //edit patient status
    editPat();
    if ($scope.admission.ADMITNO ===1 ){
      editAdmit();
    } else {
      deleteAdmit();
    }
  };

  function editPat() {
    dataService.edit('patient', {'KEYID': $scope.patient.KEYID, 'PATSTATUS':$scope.PATSTATUS}).then(function(response){
      console.log(response);
      //get update patient info and reset patient
      dataService.get('patient', {'KEYID' : patientService.get().KEYID}).then(function(data) {
        console.log(data);
        var updatedPatient = data.data[0];
        patientService.set(updatedPatient);
        $scope.updatedPatient = patientService.get();
        //close modal and pass updatedPatient and updatedAdmission object to IntakeProfileController
        $uibModalInstance.close(updatedPatient);
      });
    });
  }

  function editAdmit() {
    if ($scope.PATSTATUS === 'D') {
      var editDischarge={
        KEYID:$scope.admission.KEYID,
        PATSTATUS: 'D',
        DCDATE: $filter('date')($scope.DCDATE, 'yyyy-MM-dd'),
        DCREASON: $scope.DCREASON
      };
      console.log(editDischarge);
      dataService.edit('admission', editDischarge).then(function(msg){
        console.log(msg);
      });
    }

    if ($scope.PATSTATUS === 'N') {
      var editNonAdmit={
        KEYID:$scope.admission.KEYID,
        PATSTATUS: 'N',
        NAREASON: $scope.NAREASON
      };
      console.log(editNonAdmit);
      dataService.edit('admission', editNonAdmit).then(function(msg){
        console.log(msg);
      });
    }

    if ($scope.PATSTATUS === 'P') {
      dataService.get('form', {ADMITKEYID:$scope.admission.KEYID}).then(function(data) {
        console.log(data.data);
        var forms = data.data;
        forms.forEach(function(form) {
          dataService.delete('form', {KEYID: form.KEYID}).then(function (response) {
            console.log(response);
            dataService.delete('visit', {KEYID: form.VISITID}).then(function (response) {
              console.log(response);
            });
          });
        });
      });
      //edit admit patstatus
      dataService.edit('admission', {KEYID:$scope.admission.KEYID, PATSTATUS:'P'}).then(function(msg){
        console.log(msg);
      });
    }
  }

  function deleteAdmit() {
    dataService.get('form', {ADMITKEYID:$scope.admission.KEYID}).then(function(data) {
      console.log(data.data);
      var forms = data.data;
      forms.forEach(function(form) {
        dataService.delete('form', {KEYID: form.KEYID}).then(function (response) {
          console.log(response);
          dataService.delete('visit', {KEYID: form.VISITID}).then(function (response) {
            console.log(response);
          });
        });
      });
    });
    //delete admit
    dataService.delete('admission', {KEYID:$scope.admission.KEYID}).then(function (response) {
      console.log(response);
    });
  };
});
