app.controller('NewOrderController', function($scope, $uibModalInstance, $filter, patientService, formService, dataService, form, formAdded) {

	$scope.patient = patientService.get();

	$scope.form=form;
  console.log('$scope.form', $scope.form);

	//close modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

	//populate doctors list
  dataService.getAll('doctor').then(function(data) {
    $scope.doctors=data.data;
    // console.log('$scope.doctors', $scope.doctors);
  });

	//populate order if exist
	$scope.isOrderObjExist=function () {
		if (form.OBJKEYID) {
			dataService.get('orders', {KEYID: form.OBJKEYID}).then(function (data) {
				var orderObj = data.data[0];
				console.log('current order', orderObj);
				$scope.PATKEYID=orderObj.PATKEYID;
				$scope.ADMITKEYID=orderObj.ADMITKEYID;
				$scope.EPIKEYID=orderObj.EPIKEYID;
				$scope.DOCTOR=orderObj.DOCTORID;
				$scope.ORDERSDATE=new Date($filter("date")(orderObj.ORDERSDATE, 'yyyy/MM/dd'));
				$scope.SUBJECT=orderObj.SUBJECT;
				$scope.DESCRIPTION=orderObj.DESCRIPTION;
			});
		}
	};

	//submit order to qa
	$scope.submitToQA=function () {
		if (!confirm("No changes are allowed once submitted to QA, are you sure you want to submit?")) { //no don't submit to qa
			console.log('no, do not submit to qa');
			event.preventDefault();
			$scope.submitQA=false;
		} else { //yes submit to qa
			$scope.submitQA=true;
			$scope.saveOrderForm();
		}
	};

	//saving order form
	$scope.saveOrderForm=function () {
		if (form.OBJKEYID) { //if record already exists, edit existing order
			$scope.editOrder();
		} else { //if record does not exist, create new order
			$scope.createNewOrder();
		}
	};

	//add new Order
  $scope.createNewOrder = function () {
		console.log('creating new order')
		//new order object
    var newOrder = {
			PATKEYID: $scope.patient.KEYID,
			ADMITKEYID: $scope.form.ADMITKEYID,
			EPIKEYID: $scope.form.EPIKEYID,
			// FORMKEYID:$scope.formkeyid,
			DOCTORID: $scope.DOCTOR,
			ORDERSDATE: $filter("date")($scope.ORDERSDATE, 'yyyy-MM-dd'),
      SUBJECT: $scope.SUBJECT,
      DESCRIPTION: $scope.DESCRIPTION,
    };
		if (formAdded===false) { //if form not created yet (coming from skilled nursing note)
			//add new order record
			console.log('adding new orders',newOrder);
		  dataService.add('orders', newOrder).then(function(response){
			  console.log(response);
				if (response.status==='success') {
					//if success in adding new orders record, then add form record
					var objkeyid=response.keyid;
					var addForm={
				    PATKEYID: $scope.patient.KEYID,
				    FORMTYPE: 17,
				    STATUS: 2,
						VISITID: $scope.form.VISITID,
						OBJKEYID: objkeyid,
						ADMITKEYID: $scope.form.ADMITKEYID,
						EPIKEYID: $scope.form.EPIKEYID,
						FORMDATE: $filter("date")($scope.newOrderDate, 'yyyy/MM/dd')
				  };

					//if submitting to QA, change status to 1 for complete
					if ($scope.submitQA===true) {
						addForm.STATUS=1;
					}

				  dataService.add('form', addForm).then(function (response) {
				    console.log('add form', response);
						//if success in adding form, edit orders to contain the formkeyid
						if (response.status==='success') {
							dataService.edit('orders', {KEYID: objkeyid, FORMKEYID: response.keyid}).then(function (response) {
								console.log('edit orders', response);
							});
						}
				  });
				}
			  $uibModalInstance.close();
		  });
		} else { //if form record created (coming from records page)
			newOrder.FORMKEYID=form.FORMID; //new order with formID
			console.log('adding new orders', newOrder);
			dataService.add('orders', newOrder).then(function(response){ //add new order
			  console.log('add new order', response);
				if (response.status==='success') { //if adding order success, edit form to contain objkeyid
					var editForm={
						KEYID: form.FORMID,
						OBJKEYID: response.keyid,
						STATUS: 2
					};
					//if submitting to QA, change status to 1 for complete
					if ($scope.submitQA===true) {
						editForm.STATUS=1;
					}
					dataService.edit('form', editForm).then(function (response) {
						console.log('edit form', response);
						$uibModalInstance.close();
					});
				}
			});
		}
  };

	$scope.editOrder=function () {
		console.log('editing order');
		var orderChanges={
			KEYID: form.OBJKEYID,
			DOCTORID: $scope.DOCTOR,
			ORDERSDATE: $filter("date")($scope.ORDERSDATE, 'yyyy-MM-dd'),
      SUBJECT: $scope.SUBJECT,
      DESCRIPTION: $scope.DESCRIPTION,
		};
		console.log('order changes', orderChanges);
		dataService.edit('orders', orderChanges).then(function (response) {
			console.log(response);

			if (response.status==='success') {
				//if submitting to qa, edit form status to 1-Submitted to QA
				if ($scope.submitQA===true){
					dataService.edit('form', {KEYID: form.FORMID, STATUS:1}).then(function (response) {
						console.log('edit form', response);
						$uibModalInstance.close();
					});
				} else {
					$uibModalInstance.close();
				}
			}
		});
	};

	$scope.isOrderObjExist();
});
