app.controller('ViewVisitController', function($scope, $filter,$http, CONSTANTS, $httpParamSerializerJQLike, $uibModalInstance, visit, patient, dataService, episode, patientService) {

  $scope.visit = visit;
  $scope.patient = patient;
  $scope.episode = episode;

  $scope.formsToAdd = [];
  $scope.formsToRemove = [];

  // Get all the available form types
  dataService.getAll('formType').then(function(data){
    console.log(data);
    $scope.formTypes = data.data;

    $scope.formTypes.forEach(function (formType) {
      formType.FORMTYPE = formType.KEYID;
    });

    // //if patient status is Transfer, then allow ROC oasis forms
    // if ($scope.patient.PATSTATUS === 'T') {
    //   $scope.formTypes = data.data;
    //   //if status is not Transfer, remove ROC oasis form
    // } else {
    //   var index = $scope.formTypes.findIndex(x => x.KEYID===3);
    //   $scope.formTypes.splice(index, 1);
    // }

    // if selected date is not within last 5 days of episode
    var selected = moment($scope.visit.VISITDATE);
    if ($scope.episode !== undefined) {
      $scope.endOfEpisode = moment($scope.episode.THRUDATE);
      $scope.lastFiveDays = moment($scope.endOfEpisode.clone().subtract(5, 'days'));
      // console.log($scope.endOfEpisode)
      // console.log($scope.lastFiveDays)
      if (selected.isBetween($scope.lastFiveDays, $scope.endOfEpisode, null, '[]') !== true) {
        //remove recertification form
        for (var j = 0; j < $scope.formTypes.length; j++) {
          if ($scope.formTypes[j].KEYID === 4) {
            var index = $scope.formTypes.indexOf($scope.formTypes[j]);
            $scope.formTypes.splice(index, 1);
          }
        }
      }
    }
    // Get the forms for the current visit
    $scope.selectedForms = [];
    dataService.get("form", {VISITID: visit.KEYID}).then(function(data) {
      console.log("forms:");
      console.log(data);
      $scope.forms = data.data;
      for (var i = 0; i < data.data.length; i++) {
        for (var j = 0; j < $scope.formTypes.length; j++) {
          if (data.data[i].FORMTYPE == $scope.formTypes[j].KEYID) {
            console.log("adding form type: " + $scope.formTypes[j].KEYID);
            data.data[i].NAME = $scope.formTypes[j].NAME;
            // console.log(data.data[i])
            $scope.selectedForms.push(data.data[i])
          }
        }
      }
      console.log('original forms', data.data)
      console.log('selectedForms', $scope.selectedForms)
    });
  });

  $scope.enableEdit = function (){
    //if there are forms associated with visit, see if forms have been submitted to qa or locked
    if ($scope.forms.length>0) {
      console.log($scope.forms);
      //check to see if forms are submitted or approved
      for (var i = 0; i < $scope.forms.length; i++) {
        console.log($scope.forms[i]);
        if ( ($scope.forms[i].STATUS===1) || ($scope.forms[i].STATUS===3) ) { //forms are submited or approved
          alert('Visit cannot be edited as it contains form(s) that has/have been submitted to QA or approved.');
          $scope.taco=true;
          break;
        }
      }

      if (!$scope.taco) {
        $scope.noEdit=false;
        $scope.edit=true;
      }
    } else { //if there are no forms associated with visit, then enable edit
      $scope.noEdit=false;
      $scope.edit=true;
    }
  };
    // console.log($scope.visit);
    // if ($scope.visit.VISITTYPE === 1){
    //   dataService.get('oasisC2_2_20_1', {'M0020_PAT_ID': $scope.patient.KEYID, 'M0100_ASSMT_REASON': '01'}).then(function(data){
    //     // console.log(data.data.length)
    //     if (data.data.length !==0 ){
    //         alert("Cannot edit SOC visit - OASIS SOC has already been submitted");
    //     } else {
    //       $scope.noEdit=false;
    //       $scope.edit=true;
    //     }
    //   });
    // } else {
    //   $scope.noEdit=false;
    //   $scope.edit=true;
    // }
  // };

  $scope.visit.VISITDATE = $filter("date")($scope.visit.VISITDATE, 'yyyy/MM/dd');
  $scope.visit.VISITDATE = new Date($scope.visit.VISITDATE);

  formatTime();
  function formatTime() {
    if ($scope.visit.TIMEIN !== null) {
      //format TIMEIN and TIMEOUT to display
      var hourIN = $scope.visit.TIMEIN.slice(0,2);
      var minIN = $scope.visit.TIMEIN.slice(3,5);
      var hourOUT = $scope.visit.TIMEOUT.slice(0,2);
      var minOUT = $scope.visit.TIMEOUT.slice(3,5);
      $scope.HOURS = visit.HOURS;
      $scope.TIMEIN = new Date().setHours(hourIN,minIN);
      $scope.TIMEIN = new Date($scope.TIMEIN);
      $scope.TIMEOUT = new Date().setHours(hourOUT, minOUT);
      $scope.TIMEOUT = new Date($scope.TIMEOUT);
    }
  }

  // $scope.calculate = function(){
  //   var hourDiff = $scope.TIMEOUT.getHours() - $scope.TIMEIN.getHours();
  //   var minDiff = $scope.TIMEOUT.getMinutes() - $scope.TIMEIN.getMinutes();
  //
  //   if ( (hourDiff===0) && (minDiff>0) ) {
  //     $scope.HOURS = 1;
  //   } else if ( (hourDiff>0) && (minDiff>29) ) {
  //     $scope.HOURS = hourDiff + 1;
  //   } else if ( (hourDiff>0) && (minDiff<30) ){
  //     $scope.HOURS = hourDiff;
  //   }
  // };



  dataService.getAll('employee').then(function(data){
    $scope.employees = data.data;
  });

  $scope.close = function() {
    $uibModalInstance.close();
  };

  // Get all the visits for the given patient
  dataService.get('visit', {'ADMITKEYID': $scope.patient.LSTADMIT}).then(function(data){
    // $scope.visits = data.data;
    var visits = data.data;
    // console.log(visits);
    // Get all the active visit types
    dataService.get('visitType', {'ACTIVE': 1}).then(function(data){
      // console.log(data.data)
      var visitTypes = data.data;
      // console.log(visits.length);
      // If there is no scheduled visit for the patient
      if (visits.length === 0) {
        $scope.visitTypes = visitTypes;
      // If there are visits for the patient
      } else {
        for (var i = 0; i < visits.length; i++) {
          // If this is a start of care visit (VISITTYPE === 1) already scheduled AND current visit isn't SOC visit, don't show SOC visit types
          if ((visits[i].VISITTYPE === 1) && ($scope.visit.VISITTYPE!==1)) {
            var index = visitTypes.findIndex(x => x.KEYID===1);
            visitTypes.splice(index, 1);
            $scope.visitTypes = visitTypes;
            console.log()
          } else {
            $scope.visitTypes = visitTypes;
          }
          // console.log($scope.visitTypes);
        }
        // console.log($scope.visitTypes);
      }
    });
  });

  //hiding SOC form in dropdown
  $scope.hideSOC = function (form) {
    return form.NAME !== 'OASIS-C2 SOC v. 2.20.1';
  };

  $scope.noEdit = true;

  //Autopopulate TIMEOUT based on TIMEIN + default hours
  $scope.changedTIMEIN = function(){
    if ($scope.TIMEIN !== null) {
      var timein = new Date($scope.TIMEIN);
      $scope.HOURS = parseFloat($scope.HOURS);
      $scope.TIMEOUT = timein.setHours(timein.getHours()+$scope.HOURS);
      $scope.TIMEOUT = new Date($scope.TIMEOUT);
    }
 };

 //calculate HOURS based on TIMEOUT
 $scope.changed = function() {
   //if TIMEOUT is after TIMEIN
   if ($scope.TIMEOUT.getHours()> $scope.TIMEIN.getHours()) {
     var hourDiff = $scope.TIMEOUT.getHours() - $scope.TIMEIN.getHours();
     var minDiff = $scope.TIMEOUT.getMinutes() - $scope.TIMEIN.getMinutes();
     //Round hour by half hour increments
     if ( (hourDiff===0) && (minDiff>0) ) {
       $scope.HOURS = 1;
     } else if ( (hourDiff>0) && (minDiff>29) ) {
       $scope.HOURS = hourDiff + 1;
     } else if ( (hourDiff>0) && (minDiff<30) ) {
       $scope.HOURS = hourDiff;
     }
   } else {
     $scope.HOURS = null;
   }
 };

  $scope.editVisit = function(visit) {
    if (confirm("Any changes made to the visit may cause changes to associated forms. Are you sure you want to save the changes made?")) {
      if($scope.TIMEIN !== undefined) {
        //Format TIMEIN and TIMEOUT
        var hourIN = $scope.TIMEIN.getHours();
        var hourOUT = $scope.TIMEOUT.getHours();
        var minOUT = $scope.TIMEOUT.getMinutes();
        var minIN = $scope.TIMEIN.getMinutes();

        if (minIN < 10) {
          minIN = minIN.toString();
          minIN = '0' + minIN;
        } else {
          minIN = minIN.toString();
        }
        if (minOUT<10){
          minOUT = minOUT.toString();
          minOUT='0'+ minOUT;
        } else {
          minOUT = minOUT.toString();
        }
        if (hourOUT<10){
          hourOUT = hourOUT.toString();
          hourOUT='0'+ hourOUT;
        } else {
          hourOUT = hourOUT.toString();
        }
        if (hourIN<10){
          hourIN = hourIN.toString();
          hourIN='0'+ hourIN;
        } else {
          hourIN = hourIN.toString();
        }
        $scope.TIMEIN = hourIN + minIN + '00';
        $scope.TIMEOUT = hourOUT + minOUT + '00';
      } else {
        $scope.TIMEIN = null;
        $scope.TIMEOUT = null;
      }

      var visitChanges = {
        KEYID: $scope.visit.KEYID,
        PATKEYID: $scope.patient.KEYID,
        EPIKEYID: $scope.visit.EPIKEYID,
        EMPKEYID: $scope.visit.EMPKEYID,
        VISITTYPE: $scope.visit.VISITTYPE,
        VISITDATE: moment($scope.visit.VISITDATE).format("YYYY-MM-DD"), // new Date($scope.visit.VISITDATE),
        DISCIPLINE: $scope.visit.DISCIPLINE,
        TIMEIN: $scope.TIMEIN,
        TIMEOUT: $scope.TIMEOUT,
        HOURS: $scope.HOURS,
        MILEAGE: $scope.visit.MILEAGE,
        NOTES: $scope.visit.NOTES,
      };
      console.log(visitChanges);
      dataService.edit('visit', visitChanges).then(function(response) {
        console.log(response);
        $scope.forms.forEach(function(form) {
          if ( (form.STATUS === 2) || (form.STATUS===4) ) { //if the form satus is either in progress or rejected
            //then edit the form
            var formEdits={
              KEYID:form.KEYID,
              FORMDATE:  moment($scope.visit.VISITDATE).format("YYYY-MM-DD"),
              ASSIGNEDTO: $scope.visit.EMPKEYID,
            };
            dataService.edit('form', formEdits).then(function (response) {
              console.log(response);
            });
            //then edit the form obj
            //find out which table the form obj is in
            dataService.get('formType', {KEYID: form.FORMTYPE}).then(function (data) {
              var formTypeTableName=data.data[0].TABLENAME;
              console.log('formtype tablename', formTypeTableName)

              //if form is oasis form
              if (formTypeTableName === 'oasis_c2_2_20_1') {
                //edit oasis
                var oasisObjEdits={
                  KEYID: form.OBJKEYID,
                  M0030_START_CARE_DT: moment($scope.visit.VISITDATE).format("YYYY-MM-DD")
                }
                console.log('oasisObjEdits', oasisObjEdits)
                $http({
                  url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['oasisC2_2_20_1'].edit,
                  method: 'POST',
                  data: $httpParamSerializerJQLike(oasisObjEdits),
                  headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                  }
                }).then(function(response){
                  console.log('editing oasis', response);
                  if (response.data.status==='success') {
                    //edit admit
                    editAdmit(form)
                  }
                }, function(data) {
                  console.log(data);
                });
              } else if (formTypeTableName==='routesheet') { //if form is routesheet
                //edit routesheet
                var routeSheetObjEdits={
                  KEYID: form.OBJKEYID,
                  VISITDATE: moment($scope.visit.VISITDATE).format("YYYY-MM-DD"),
                  EMPLOYEE:$scope.visit.EMPKEYID,
                  VISITTYPE:$scope.visit.VISITTYPE,
                }
                $http({
                  url: CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + CONSTANTS.api.actions['routeSheet'].edit,
                  method: 'POST',
                  data: $httpParamSerializerJQLike(routeSheetObjEdits),
                  headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                  }
                }).then(function(response){
                  console.log('editing routeSheet', response);
                });
              } else {
                console.log('you have failed')
              }
            })
          } else if (form.STATUS===0) { //if for status is incomplete
            dataService.delete('form', form).then(function(response) {
              console.log(response);
            });
          }
        });

        $scope.selectedForms.forEach(function(selectedForm) {
          console.log('form', selectedForm.OBJKEYID);
          if ( (selectedForm.OBJKEYID===null)  || (selectedForm.OBJKEYID===undefined)) { //if the form does not have an objkeyid;
            //add form;
            $scope.updatedForm = {
              PATKEYID : $scope.patient.KEYID,
                // FIXME: LSTADMIT is just the last admit, we may
                // want to add a visit for a previous admit
                ADMITKEYID : $scope.patient.LSTADMIT,
                EPIKEYID : $scope.visit.EPIKEYID,
                VISITID : $scope.visit.KEYID,
                SECTION: null,
                FORMTYPE : selectedForm.FORMTYPE,
                OBJKEYID:null,
                FORMDATE : moment($scope.visit.VISITDATE).format("YYYY-MM-DD"),
                ASSIGNEDTO : $scope.visit.EMPKEYID,
                STATUS : 0,
                DETAIL1: null,
                DETAIL2: null,
            };
            console.log('new form', $scope.updatedForm);
            dataService.add('form', $scope.updatedForm).then(function(msg) {
              console.log('adding new form', msg);
            });
          }  else {
            console.log('form has objkey id, not add');//else do not add form
          }
        });


          // $scope.forms.forEach(function(form) {
          //   if(form !== $scope.updatedForm) {
          //     console.log(form)
          //     dataService.delete('form', form).then(function(response) {
          //       console.log(response)
          //     })
          //   }
          // });
          //
          // var index = $scope.forms.indexOf($scope.updatedForm)
          // if(index === -1) {
          //   console.log(form)
          //   dataService.add('form', form).then(function(msg) {
          //     console.log(msg);
          //   });
          // }


        $uibModalInstance.close();
      });
    }
  };

function editAdmit(form) {
  console.log('asdfasdfas', form)
  var updatedAdmit = {
    KEYID: form.ADMITKEYID,
    // PATSTATUS: 'A',
    SOCDATE: $filter("date")($scope.visit.VISITDATE, 'yyyy/MM/dd'),
  }
  dataService.edit('admission', updatedAdmit).then(function(response){
      console.log(response)
      editEpisode(form)
  })
}

function editEpisode(form) {
  var endDate = new Date($scope.visit.VISITDATE);
  endDate.setDate(endDate.getDate()+60);
  var changesEpisode = {
    KEYID: form.EPIKEYID,
    FROMDATE: $filter("date")($scope.visit.VISITDATE, 'yyyy/MM/dd'),
    THRUDATE: $filter("date")(endDate, 'yyyy/MM/dd'),
  };
  console.log(changesEpisode);
  dataService.edit('episode', changesEpisode).then(function(response){
    console.log(response);
  });
}


  dataService.get('admission', {KEYID: $scope.patient.LSTADMIT}).then(function (data) {
    $scope.currentAdmit=data.data[0];
    console.log($scope.currentAdmit)
  })

  $scope.deleteVisit = function(visit) {
    console.log('ss', $scope.forms);
    //if there are forms for the visit -
    //check to see if form has associated OBJKEYID - if it does, can not be removed
    if ($scope.forms.length>0) {
      var index = $scope.forms.findIndex(x=>x.OBJKEYID !== null);
      if (index !== -1) {//there is a form with OBJKEYID, do not delete
        alert('Visit cannot be deleted, as it contains form(s) that has/have been submitted to QA or approved.');
      } else {//none of the forms have OBJKEYID, allow delete
        $scope.forms.forEach(function(form) {
          console.log(form);
          dataService.delete('form', {KEYID:form.KEYID}).then(function(response) {
            console.log(response);
            if ($scope.currentAdmit.ADMITNO===1) {//if this is first admit, patient coming from pending status
              if (form.FORMTYPE===2) {//if scheduled SOC form is deleted, then patient status changes back to pending
                dataService.edit('patient', {KEYID:$scope.patient.KEYID, PATSTATUS:'P'}).then(function(response) {
                  console.log(response);
                  dataService.get('patient', {KEYID:$scope.patient.KEYID}).then(function(data) {
                    patientService.set(data.data[0]);
                  });
                });
                dataService.edit('admission', {KEYID: $scope.patient.LSTADMIT, PATSTATUS:'P'}).then(function(response) {
                  console.log(response);
                });
              }
              dataService.delete('visit', {'KEYID': visit.KEYID}).then(function(response){
                console.log(response);
                $uibModalInstance.close();
              });
            } else { //else if this is not first admit, means patient coming from discharge status
              if (form.FORMTYPE===2) {//if scheduled SOC form is deleted, then patient status changes back to discharge
                dataService.get('admission', {PATKEYID:$scope.patient.KEYID}).then(function(data) {
                  var admits = data.data;
                  console.log(admits)
                  var deleteAdmitKEYID = admits[admits.length-1].KEYID;
                  console.log(deleteAdmitKEYID)
                  admits.pop();
                  console.log(admits)
                  var previousAdmitKEYID = admits[admits.length-1].KEYID;
                  console.log(previousAdmitKEYID)
                  dataService.edit('patient', {KEYID:$scope.patient.KEYID, PATSTATUS:'D', LSTADMIT:previousAdmitKEYID}).then(function (response) {
                    console.log(response);
                    dataService.get('patient', {KEYID:$scope.patient.KEYID}).then(function(data) {
                      patientService.set(data.data[0]);
                    });
                    dataService.delete('admission', {KEYID: deleteAdmitKEYID}).then(function(response) {
                      console.log(response);
                    });
                  });
                });
              }
              dataService.delete('visit', {'KEYID': visit.KEYID}).then(function(response){
                console.log(response);
                $uibModalInstance.close();
              });
            }
          });
        });
      }
    } else { //if there are no forms, visit can be deleted
      dataService.delete('visit', {'KEYID': visit.KEYID}).then(function(response){
        console.log(response);
        $uibModalInstance.close();
      });
    }
  };
  // $scope.addTask = function(newTask) {
  //   console.log(newTask);
  //   // console.log(task)
  // };

  // $scope.selectFormsArr = [];

  $scope.addForm = function(selectForm) {
    // console.log($scope.forms)
    var selectedForms = $scope.selectedForms;
    console.log(selectedForms)
    console.log(selectForm)
    var isAlreadyAdded=false;

    for (var i = 0; i < selectedForms.length; i++) {
      if (selectedForms[i].FORMTYPE===selectForm.FORMTYPE) {
        isAlreadyAdded = true;
        break;
      }
    }

    // if (selectedForms.indexOf(selectForm) === -1 ) {
    //   $scope.selectedForms.push(selectForm);
    //   // $scope.formsToAdd.push(selectForm);
    //   // console.log($scope.formsToAdd)
    //   console.log('selectedForms',$scope.selectedForms)
    //
    // }

    if (!isAlreadyAdded) {
      $scope.selectedForms.push(selectForm);
    } else {
      alert("This form has already been selected");
    }
    // console.log($scope.selectedForms);
    // console.log($scope.forms)
  };

  $scope.removeForm = function(selectForm) {
    var index = $scope.selectedForms.indexOf(selectForm);
    $scope.selectedForms.splice(index, 1);
    // $scope.formsToRemove.push(selectForm);
    console.log('selectedForms', $scope.selectedForms);
  };

  // $scope.updateVisitTypeDetails = function() {
  //   console.log("visit type to update");
  //   console.log($scope.VISITTYPE);
  //
  //   $scope.getForms($scope.VISITTYPE.KEYID);
  //   $scope.HOURS = parseFloat($scope.VISITTYPE.DFLTHRS);
  // };

  //Get formtypes for cooresponding VisitType
  $scope.updateVisitTypeDetails = function(visitType) {
    $scope.getForms($scope.visit.VISITTYPE);
    for (var i = 0; i < $scope.visitTypes.length; i++) {
      if ($scope.visitTypes[i].KEYID === $scope.visit.VISITTYPE){
        $scope.HOURS = $scope.visitTypes[i].DFLTHRS;
        $scope.changedTIMEIN();
      }
    }
  };

  $scope.getForms = function(visitTypeID) {
    dataService.get('visitTypeForm', {'VISITTYPE_ID': visitTypeID}).then(function(data){
      console.log("visit type forms:");
      console.log(data.data);
      var visitTypeFormArr= data.data
      var index;
      for (var i = 0; i < visitTypeFormArr.length; i++) {
        //remove all forms that are FORMTYPE_ID ===2 and not VISITTYPE_ID ===1 <--not Start of Care
        if ((visitTypeFormArr[i].FORMTYPE_ID === 2) && (visitTypeFormArr[i].VISITTYPE_ID !==1)) {
          index = visitTypeFormArr.indexOf(visitTypeFormArr[i]);
          visitTypeFormArr.splice(index, 1);
        }
        console.log(visitTypeFormArr);
      };

      $scope.selectedForms = [];

      for (var i = 0; i < data.data.length; i++) {
        for (var j = 0; j < $scope.formTypes.length; j++) {
          if (data.data[i].FORMTYPE_ID == $scope.formTypes[j].KEYID) {
            console.log("adding form type: " + $scope.formTypes[j].KEYID);
            $scope.selectedForms.push($scope.formTypes[j]);
            console.log('selectedForms', $scope.selectedForms);
          }
        }
      }
    });
  };

});
