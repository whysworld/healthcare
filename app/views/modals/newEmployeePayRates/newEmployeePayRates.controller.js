app.controller('NewEmployeePayRates', function($scope, $uibModalInstance, dataService, employee) {

  $scope.close=function () {
    if (confirm("Any unsaved changes will be lost, are you sure you want to exit?")) {
      $uibModalInstance.close();
    }
  };

  $scope.employee=employee;

  dataService.get('employeePayrate', {EMPLOYEE_ID: employee.KEYID}).then(function (data) {
    $scope.empPayrates=data.data;
    dataService.getAll('visitType').then(function (data) {
      $scope.visitTypes=data.data;
      //get all visit types that are not already added
      $scope.empPayrates.forEach(function (empPayrate) {
        $scope.visitTypes.forEach(function (visitType) {
          //if visitType already added, remove it from list of visitTypes
          if (empPayrate.VISITTYPE_ID===visitType.KEYID) {
            var index = $scope.visitTypes.indexOf(visitType);
            $scope.visitTypes.splice(index, 1);
          }
        });
      });
    })
  })




  $scope.addNewEmployeePay=function (form) {
    console.log('$scope.visitTypes', $scope.visitTypes);

    $scope.visitTypes.forEach(function (visitType) {
      //validating fields
      if (visitType.EMPPAY===undefined || visitType.EMPPAY==="") {
        visitType.EMPPAY_INCOMPLETE=true;
      } else {
        visitType.EMPPAY_INCOMPLETE=false;
      }

      if ( visitType.PERVISIT!==null && !visitType.EMPPAY_INCOMPLETE ) {
        visitType.invalid=false;
        visitType.TOADD=true;
      } else if (visitType.PERVISIT===null && visitType.EMPPAY_INCOMPLETE ) {
        visitType.invalid=false;
        visitType.TOADD=false;
      } else if (visitType.PERVISIT!==null &&  visitType.EMPPAY_INCOMPLETE ){
        visitType.invalid=true;
        visitType.TOADD=false;
      } else if ( !visitType.EMPPAY_INCOMPLETE  && visitType.PERVISIT===null){
        visitType.invalid=true;
        visitType.TOADD=false;
      }

    });

    for (var i = 0; i < $scope.visitTypes.length; i++) {
      if ($scope.visitTypes[i].invalid){
        $scope.formInvalid=true;
        break;
      } else {
        $scope.formInvalid=false;
      }
    }

    if ($scope.formInvalid===false) {
      $scope.visitTypes.forEach(function (visitType) {
        //visit types to add
        if (visitType.TOADD) {
          var newEmployeePay={
             EMPLOYEE_ID: $scope.employee.KEYID,
             VISITTYPE_ID: visitType.KEYID,
             EMPPAY: visitType.EMPPAY,
             EMPPAY_PERVISIT: visitType.PERVISIT
          };
          console.log('new employee pay rate', newEmployeePay);
          dataService.add('employeePayrate', newEmployeePay).then(function (response) {
            console.log('adding pay rate', response);
            $uibModalInstance.close();
          });
        }
      })
    };
  }


    // console.log($scope.visitType)

    // console.log($scope.EMPPAY)
    // console.log($scope.visitType.EMPPAY[$index])
    // if (form.$valid) {
    //   var newEmployeePay={
    //     EMPLOYEE_ID: $scope.employee.KEYID,
    //     VISITTYPE_ID: $scope.VISITTYPE_ID,
    //     EMPPAY: $scope.EMPPAY,
    //     EMPPAY_PERVISIT: $scope.EMPPAY_PERVISIT
    //   };
    //   console.log('new employee pay rate', newEmployeePay);
    //   dataService.add('employeePayrate', newEmployeePay).then(function (response) {
    //     console.log('adding pay rate', response);
    //     if (response.status==='success'){
    //       $uibModalInstance.close();
    //     }
    //   });
    // }




  //closing modal confirmation
  //modal confirmation
  $scope.$on('modal.closing', function(event, reason, closed) {
      switch (reason){
          // clicked outside
          case "backdrop click":
              var message = "Any unsaved changes will be lost, are you sure you want to exit?";
              break;
          // escape key
          case "escape key press":
              var message = "Any unsaved changes will be lost, are you sure you want to exit?";
              break;
      }

      if (reason==='backdrop click') {
        if (!confirm(message)) {
            event.preventDefault();
        }
      }

      if (reason==='escape key press') {
        if (!confirm(message)) {
            event.preventDefault();
        }
      }
  });

});
