app.controller('NewVisitTypeController', function($scope, $uibModal, $uibModalInstance, CONSTANTS, getVisitTypeList, dataService) {

  //$scope.disciplineList = CONSTANTS.disciplines;
  $scope.disciplineList = [];
  $scope.selectedForms = [];
  $scope.revcodeList = [];
  $scope.serviceTypeList = [];
  $scope.employeeClassList = [];

  $scope.getDisciplines = function() {
    dataService.getAll('visitDiscipline').then(function(data) {
      $scope.disciplineList = data.data;
    });
  };

  $scope.getRevenueCodes = function(discipline) {
    var filterObj = {
      DISCIPLINE : discipline
    };
    dataService.get('revcode', filterObj).then(function(data) {
      $scope.revcodeList = data.data;
    });
  };

  $scope.getEmployeeClasses = function(discipline) {
    var filterObj = {
      DISCIPLINE : discipline
    };
    dataService.get('employeeClass', filterObj).then(function(data) {
      $scope.employeeClassList = data.data;
    });
  }

  $scope.getFormTypes = function() {
    dataService.getAll('formType').then(function(data){
      $scope.forms = data.data;
    });
  };

  $scope.addForm = function(selectForm) {
    var selectedForms = $scope.selectedForms;
    if (selectedForms.indexOf(selectForm) === -1 ) {
      $scope.selectedForms.push(selectForm);
    }
    else {
      alert("This form has already be selected");
    };
  };

  $scope.removeForm = function(selectForm) {
    var index = $scope.selectedForms.indexOf(selectForm);
    $scope.selectedForms.splice(index, 1);
  };

  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.getServiceTypes = function(discipline) {
    var filterObj = {
      DISCIPLINE : discipline
    }
    dataService.get("visitService", filterObj).then(function(visitServiceData) {
      $scope.serviceTypeList = visitServiceData.data;
    });
  };

  // $scope.getEmployeeClasses = function() {
  //   dataService.getAll("employeeClass").then(function(employeeClassData) {
  //     $scope.employeeClassList = employeeClassData.data;
  //   });
  // };

  $scope.addVisitType = function() {
    var newVisitType = {
    	VISITCODE: $scope.VISITCODE,
    	DESC: $scope.DESC,
    	DISCIPLINE: $scope.DISCIPLINE,
    	PERVISIT: $scope.PERVISIT,
    	DFLTHRS: $scope.DFLTHRS,
    	ROUND_HRS: $scope.ROUND_HRS,
    	ENTERBYTIME: $scope.ENTERBYTIME,
    	GETMILEAGE: $scope.GETMILEAGE,
    	ADMINCOST: $scope.ADMINCOST,
    	DFLTEMPPAY: $scope.DFLTEMPPAY,
    	DFLTEMPPAY_PERVISIT: $scope.DFLTEMPPAY_PERVISIT,
    	ACTIVE: $scope.ACTIVE,
    	SERVICE: $scope.SERVICE,
    	EMPCLASS: $scope.EMPCLASS
    };
    dataService.add('visitType', newVisitType).then(function(response) {
      addVisitForm(response);
      if (response.status == "success" && response.hasOwnProperty("keyid")) {
        var addVisitTypeInsurance = {
          INSNAME : "Default",
          INSURANCE_ID : 0,
          VISITTYPE_ID : response.keyid,
          DESC : "Default",
          REV_CODE : $scope.REV_CODE,
          HCPC : $scope.HCPC,
          AMOUNT : $scope.AMOUNT,
          BILLABLE : $scope.BILLABLE,
        }
        dataService.add("visitTypeInsurance", addVisitTypeInsurance).then(function(msg) {
          getVisitTypeList();
        })
      }
    });
    $scope.close();
  };

  function addVisitForm(response) {
    console.log($scope.selectedForms)
    console.log(response.keyid)

    for (var i = 0; i < $scope.selectedForms.length; i++) {
      var visitForm = {
        VISITTYPE_ID: response.keyid,
        FORMTYPE_ID: $scope.selectedForms[i].KEYID
      };
      dataService.add('visitTypeForm', visitForm).then(function(msg){
        console.log(msg);
      });
    };
  }

  // $scope.newVisitService = function() {
  //   var modalInstance = $uibModal.open({
  //     controller: 'NewVisitServiceController',
  //     ariaLabelledBy: 'modal-title',
  //     ariaDescribedBy: 'modal-body',
  //     templateUrl: './views/modals/newVisitService/newVisitService.html',
  //     size: 'md',
  //     resolve: {
  //       getVisitServices: function() {
  //         return $scope.getServiceTypes;
  //       }
  //     }
  //   });
  // };

  // $scope.removeVisitService = function() {
  //   var visitServiceToRemove = {
  //     KEYID: $scope.SERVICE
  //   };
  //   dataService.delete("visitService", visitServiceToRemove).then(function(msg) {
  //     $scope.getServiceTypes();
  //   });
  // };

  // $scope.newEmployeeClass = function() {
  //   var modalInstance = $uibModal.open({
  //     controller: 'NewEmployeeClassController',
  //     ariaLabelledBy: 'modal-title',
  //     ariaDescribedBy: 'modal-body',
  //     templateUrl: './views/modals/newEmployeeClass/newEmployeeClass.html',
  //     size: 'md'
  //   });
  // };

  // $scope.removeEmployeeClass = function() {
  //   var employeeClassToRemove = {
  //     KEYID: $scope.EMPCLASS
  //   };
  //   dataService.delete("employeeClass", employeeClassToRemove).then(function(msg) {
  //     $scope.getEmployeeClasses();
  //   });
  // };

  $scope.disciplineUpdated = function() {
    $scope.getRevenueCodes($scope.DISCIPLINE);
    $scope.getServiceTypes($scope.DISCIPLINE);
    $scope.getEmployeeClasses($scope.DISCIPLINE);
  }

  $scope.revcodeUpdated = function() {
    $scope.REV_CODE = $scope.REVENUE_CODE.REVCODE;
    $scope.PERVISIT = $scope.REVENUE_CODE.PERVISIT;
  }

  $scope.serviceTypeUpdated = function() {
    $scope.HCPC = $scope.SERVICE.HCPC;
    $scope.BILLABLE = $scope.SERVICE.BILLABLE;
  }

  $scope.getDisciplines();

  $scope.getFormTypes();


});
