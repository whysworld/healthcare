app.directive('fileModel', function($parse) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;

			element.bind('change', function(){
				scope.$apply(function(){
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	}
});

app.controller('PatientUploadController', function($scope, parent, $uibModalInstance, selected, dataService, patientService, auth) {
    $scope.close = function() {
        $uibModalInstance.close();
    };

    $scope.close = function() {
      $uibModalInstance.close();
    };

    $scope.loading = false;

    $scope.uploadFile = function() {
    	$scope.loading = true;

    	var payload = new FormData();

    	payload.append('PATID', patientService.get().KEYID);

    	// TODO: THIS IS THE ID OF THE EMPLOYEE WHO IS UPLOADING THE FILE
    	payload.append('ADDEDBY', auth.getUser()['id']);

    	payload.append('TITLE', $scope.TITLE);
    	payload.append('NOTES', $scope.NOTES);
    	payload.append('FILE', $scope.FILE);

    	console.log('Payload: ', payload);
    	dataService.uploadFile('patientUpload', payload).then(function(response) {
    		$scope.loading = false;
            parent.loadAll();
    		$scope.close();
    	}, function(response) {
    		$scope.loading = false;
            // TODO: Show some error message when uploading file fails
    	});
    }

});