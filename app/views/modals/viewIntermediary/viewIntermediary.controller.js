app.controller('ViewIntermediaryController', function ($scope, $uibModalInstance, intermediary, dataService) {

  $scope.editIntermediary = function (form) {
    if (form.$valid) {
      var intermediaryChanges = {
        KEYID: $scope.intermediary.KEYID,
        NAME: $scope.NAME,
        ADDRESS: $scope.ADDRESS,
        SUBMITTER: $scope.SUBMITTER,
        ECFPATH: $scope.ECFPATH,
        ECFNAME: $scope.ECFNAME,
        ECFFORMAT: $scope.ECFFORMAT,
        CONTACT: $scope.CONTACT,
        DEPT: $scope.DEPT,
        PHONEEXT: $scope.PHONEEXT,
        PHONE: $scope.PHONE,
        FAX: $scope.FAX,
        NOTES: $scope.NOTES,
        RCVRNAME: $scope.RCVRNAME
      }
      dataService.edit('intermediary', intermediaryChanges).then(function (response) {
        $scope.close()
      })
    }
  }

  $scope.deleteIntermediary = function (intermediary) {
    if (confirm('Are you sure you want to delete intermediary?')) {
      dataService.delete('intermediary', {
        KEYID: intermediary.KEYID
      }).then(function (response) {
        if (!response) {
          alert('Error: Cannot delete intermediary - intermediary is currently being used.')
        } else if (response.status === 'success') {
          $scope.close()
        }
      })
    }
  }

  $scope.intermediary = intermediary
  $scope.intermediary.PHONEEXT = parseInt($scope.intermediary.PHONEEXT)

  $scope.close = function () {
    $uibModalInstance.close()
  }

  $scope.noEdit = true

})
