app.controller('NewInsuranceController', function($scope, $uibModalInstance, dataService) {

  $scope.close = function() {
    $uibModalInstance.close();
  };

  dataService.getAll('intermediary').then(function(data){
    console.log('$scope.intermediaries', data.data);
    $scope.intermediaries = data.data;
  });


  $scope.addInsurance = function(form) {
    if (form.$valid) {
      var newInsurance = {
        NAME: $scope.NAME,
        ADDRESS: $scope.ADDRESS,
        CITY: $scope.CITY,
        STATE: $scope.STATE,
        ZIP: $scope.ZIP,
        PHONE: $scope.PHONE,
        TYPE: $scope.TYPE,
        PROVIDERID: $scope.PROVIDERID,
        // INTERMED: $scope.INTERMED,
        INTERMED: $scope.INTERMED, //change this once intermed table is ready
        PAYOR_NAME: $scope.PAYOR_NAME,
        PAYOR_ID: $scope.PAYOR_ID,
        CLAIMTYPE: $scope.CLAIMTYPE,
        NOUNITRND: $scope.NOUNITRND,
        CMBNSDVSTS: $scope.CMBNSDVSTS,
        SPANEPI: $scope.SPANEPI,
        SPPLYDTL: $scope.SPPLYDTL,
        CLMOVERLAP: $scope.CLMOVERLAP,
        CLMSHRINK: $scope.CLMSHRINK,
        MULTIPAGE: $scope.MULTIPAGE,
        NOTES: $scope.NOTES,
        USENPI1: $scope.USENPI1,
        USENPI2: $scope.USENPI2
      };
      console.log('new insurance', newInsurance);
      dataService.add('insurance', newInsurance).then(function(response){
        console.log('adding insurance', response);
        if(response.status==='success') {
          $uibModalInstance.close();
        }
      });
    }
  };
});
