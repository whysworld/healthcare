app.controller('NewEmployeeClassController', function($scope, $filter, $uibModalInstance, CONSTANTS, dataService) {

  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.disciplineList = CONSTANTS.disciplines;

  $scope.addEmployeeClass = function() {
    console.log("Adding employee class");

    var newEmployeeClass = {
      CLASSCODE : $scope.CLASSCODE,
      CLASSDESC : $scope.CLASSDESC
      // DISCIPLINE : $scope.DISCIPLINE,
      // PRIMARY : $scope.PRIMARY
    };


    dataService.add("employeeClass", newEmployeeClass).then(function(msg) {
      console.log("Employee class added. Message:");
      console.log(msg);
    });
  };

});
