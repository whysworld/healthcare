app.controller('ScheduleAfterDischargeController', function($scope, $filter, $uibModalInstance, dataService, patientService, selected) {

  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.patient = patientService.get();
  $scope.selected = selected;
  console.log(selected);

  $scope.VISITDATE = null;

  dataService.get('episode', {ADMITKEYID: $scope.patient.LSTADMIT}).then(function(data) {
    //get last episode date
    $scope.episode = data.data[(data.data.length-1)];
    console.log($scope.episode)

    $scope.inlineOptions = {
      minDate: new Date($filter("date")($scope.episode.DCDATE, 'yyyy/MM/dd')),
      // maxDate: new Date($filter("date")($scope.episode.THRUDATE, 'yyyy/MM/dd'))
    };
  })



  //get all employees
  dataService.getAll('employee').then(function(data){
    $scope.employees = data.data;
  });

  //Autopopulate TIMEOUT based on TIMEIN + default hours
 $scope.changedTIMEIN = function(){
   if ($scope.TIMEIN !== null) {
     var timein = new Date($scope.TIMEIN);
     $scope.TIMEOUT = timein.setHours(timein.getHours()+$scope.HOURS);
     $scope.TIMEOUT = new Date($scope.TIMEOUT);
   }
 };

 //calculate HOURS based on TIMEOUT
 $scope.changed = function() {
   //if TIMEOUT is after TIMEIN
   if ($scope.TIMEOUT.getHours()> $scope.TIMEIN.getHours()) {
     var hourDiff = $scope.TIMEOUT.getHours() - $scope.TIMEIN.getHours();
     var minDiff = $scope.TIMEOUT.getMinutes() - $scope.TIMEIN.getMinutes();
     //Round hour by half hour increments
     if ( (hourDiff===0) && (minDiff>0) ) {
       $scope.HOURS = 1;
     } else if ( (hourDiff>0) && (minDiff>29) ) {
       $scope.HOURS = hourDiff + 1;
     } else if ( (hourDiff>0) && (minDiff<30) ) {
       $scope.HOURS = hourDiff;
     }
   } else {
     $scope.HOURS = null;
   }
 };

  //Format TIMEIN and TIMEOUT if entered
  if ($scope.TIMEIN !==undefined) {
    var hourIN = $scope.TIMEIN.getHours();
    var hourOUT = $scope.TIMEOUT.getHours();
    var minOUT = $scope.TIMEOUT.getMinutes();
    var minIN = $scope.TIMEIN.getMinutes();

    if (minIN < 10) {
      minIN = minIN.toString();
      minIN = '0' + minIN;
    } else {
      minIN = minIN.toString();
    }
    if (minOUT<10){
      minOUT = minOUT.toString();
      minOUT='0'+ minOUT;
    } else {
      minOUT = minOUT.toString();
    }
    if (hourOUT<10){
      hourOUT = hourOUT.toString();
      hourOUT='0'+ hourOUT;
    } else {
      hourOUT = hourOUT.toString();
    }
    if (hourIN<10){
      hourIN = hourIN.toString();
      hourIN='0'+ hourIN;
    } else {
      hourIN = hourIN.toString();
    }
    $scope.TIMEIN = hourIN + minIN + '00';
    $scope.TIMEOUT = hourOUT + minOUT + '00';
  } else {
     $scope.TIMEIN = null;
     $scope.TIMEOUT = null;
  }

  //
  // //check visit date to see if episode continues or new admit
  // $scope.checkDate = function () {
  //   $scope.selectedForms=[];
  //   $scope.visitTypes=[];
  //   dataService.get('episode', {ADMITKEYID: $scope.patient.LSTADMIT}).then(function(data) {
  //     //get last episode date, and check if visitDate is before last episode end dat
  //     $scope.lastEpisode = data.data[(data.data.length-1)];
  //     if (moment($scope.VISITDATE).isBefore(moment($scope.lastEpisode.THRUDATE))) {
  //       console.log("episode continues");
  //       $scope.lastEpisodeKEYID = $scope.lastEpisode.KEYID;
  //       //unlock visit types - full list of visitTypes, no SOC
  //       dataService.getAll('visitType').then(function(data) {
  //         var visitTypes = data.data;
  //         var index = visitTypes.findIndex(x => x.KEYID===1);
  //         visitTypes.splice(index, 1);
  //         $scope.visitTypes = visitTypes;
  //         $scope.taco = true;
  //       });
  //     } else {
  //       //visit type only SOC
  //       console.log("new admit, schedule SOC");
  //       $scope.lastEpisodeKEYID = null;
  //       $scope.patient.NEWADMIT= true;
  //       dataService.getAll('visitType').then(function(data) {
  //         $scope.visitTypes = data.data;
  //         console.log($scope.visitTypes);
  //         var index = $scope.visitTypes.findIndex(x => x.KEYID===1);
  //         $scope.VISITTYPE = $scope.visitTypes[0];
  //         $scope.HOURS = parseInt($scope.VISITTYPE.DFLTHRS);
  //         $scope.taco=false;
  //         console.log($scope.VISITTYPE);
  //         $scope.getDefaultForms();
  //       });
  //     }
  //   });
  // };


  if($scope.selected !== null) {
    $scope.VISITDATE = new Date($scope.selected);
    $scope.date=true;
  }

  $scope.addForm = function(selectForm) {
    var selectedForms = $scope.selectedForms;
    //If form is not already selected
    if (selectedForms.indexOf(selectForm) === -1 ) {
      $scope.selectedForms.push(selectForm);
    }
    else {
      alert("This form has already be selected");
    }
    console.log($scope.selectedForms);
  };

  $scope.removeForm = function(form) {
    var index = $scope.selectedForms.indexOf(form);
    $scope.selectedForms.splice(index, 1);
    console.log($scope.selectedForms);
  };

  $scope.selectedForms=[];

  // Get all the available form types
  dataService.getAll('formType').then(function(data){
    $scope.formTypes= data.data;
    //find soc form
    var j = $scope.formTypes.findIndex(x =>x.KEYID===2);
    $scope.socForm=$scope.formTypes[j];

    if ($scope.patient.PATSTATUS ==='D'){
      var index = $scope.formTypes.findIndex(x => x.KEYID===2); //remove soc form from drop down
      $scope.formTypes.splice(index, 1);
      // $scope.formTypes = formTypes;
      var j = $scope.formTypes.findIndex(x => x.KEYID===9); //remove discharge form from drop down
      $scope.formTypes.splice(j, 1);

      var k = $scope.formTypes.findIndex(x => x.KEYID===4); //remove recertification form from drop down
      $scope.formTypes.splice(k, 1);
    }

    $scope.selectedForms.push($scope.socForm); //select soc form

  });

  //Get list of formType corresponding to selected visitType
  $scope.getForms = function(visitTypeID) {
    dataService.get('visitTypeForm', {'VISITTYPE_ID': visitTypeID}).then(function(data){
      console.log("visit type forms:", data.data);
      var visitTypeFormArr= data.data
      var index;
      for (var i = 0; i < visitTypeFormArr.length; i++) {
        //remove all forms that are FORMTYPE_ID ===2 and not VISITTYPE_ID ===1 <--not Start of Care
        if ((visitTypeFormArr[i].FORMTYPE_ID === 2) && (visitTypeFormArr[i].VISITTYPE_ID !==1)) {
          index = visitTypeFormArr.indexOf(visitTypeFormArr[i]);
          visitTypeFormArr.splice(index, 1);
        }
        // console.log(visitTypeFormArr);
      };

      $scope.selectedForms = [];

      if ($scope.patient.PATSTATUS==='D'){
        $scope.selectedForms.push($scope.socForm);
      }


      for (var i = 0; i < visitTypeFormArr.length; i++) {
        for (var j = 0; j < $scope.formTypes.length; j++) {
          if (data.data[i].FORMTYPE_ID == $scope.formTypes[j].KEYID) {
            console.log("adding form type: " + $scope.formTypes[j].KEYID);
            $scope.selectedForms.push($scope.formTypes[j]);
          }
        }
      }
        console.log('selectedForms', $scope.selectedForms)
    });
  };

  // $scope.getDefaultForms = function() {
  //   if ($scope.VISITTYPE !== null) {
  //     $scope.HOURS = parseInt($scope.VISITTYPE.DFLTHRS);
  //     //Get list of formType corresponding to selected VISITTYPE
  //     dataService.get('visitTypeForm', {'VISITTYPE_ID': $scope.VISITTYPE.KEYID}).then(function(data){
  //       console.log("visit type forms:", data.data);
  //       var visitTypeFormArr= data.data;
  //       var index;
  //       for (var k = 0; k < visitTypeFormArr.length; k++) {
  //         //remove all forms that are FORMTYPE_ID ===2 and not VISITTYPE_ID ===1 <--not Start of Care
  //         if ((visitTypeFormArr[k].FORMTYPE_ID === 2) && (visitTypeFormArr[k].VISITTYPE_ID !==1)) {
  //           index = visitTypeFormArr.indexOf(visitTypeFormArr[k]);
  //           visitTypeFormArr.splice(index, 1);
  //         }
  //       }
  //
  //       $scope.selectedForms = [];
  //
  //       for (var i = 0; i < visitTypeFormArr.length; i++) {
  //         for (var j = 0; j < $scope.formTypes.length; j++) {
  //           if (data.data[i].FORMTYPE_ID == $scope.formTypes[j].KEYID) {
  //             console.log("adding form type: " + $scope.formTypes[j].KEYID);
  //             $scope.selectedForms.push($scope.formTypes[j]);
  //           }
  //         }
  //       }
  //       console.log('selectedForms', $scope.selectedForms);
  //     });
  //   }
  // };

  //get all active visit types
  dataService.get('visitType', {'ACTIVE': 1}).then(function(data){
    var visitTypes = data.data;
    $scope.visitTypes = visitTypes;
    console.log($scope.visitTypes);
    // var index = $scope.visitTypes.findIndex(x => x.KEYID===1);
    // $scope.VISITTYPE = $scope.visitTypes[0];
    // $scope.HOURS = parseInt($scope.VISITTYPE.DFLTHRS);
    // $scope.getDefaultForms();
  })

  $scope.updateVisitTypeDetails = function() {
    console.log("visit type to update");
    console.log($scope.VISITTYPE);

    $scope.getForms($scope.VISITTYPE.KEYID);
    $scope.HOURS = parseFloat($scope.VISITTYPE.DFLTHRS);
    $scope.changedTIMEIN();
  };

  $scope.newVisit = function(form) {
    // $scope.VISITDATE = $filter("date")($scope.VISITDATE, 'yyyy-MM-dd');
    console.log($scope.VISITDATE);
    //if empkeyid and visitdate and visittype is entered
    if ( (form.EMPKEYID.$valid) && ($scope.VISITDATE !==undefined) && ($scope.VISITTYPE !==undefined) ) {
      // //if adding new admit
      // if ($scope.patient.NEWADMIT) {
        addAdmit();
      //
      // // } else { //episdoe continues, add visit
      //   var newVisit = {
      //     PATKEYID: $scope.patient.KEYID,
      //     ADMITKEYID: $scope.patient.LSTADMIT,
      //     EPIKEYID: $scope.lastEpisodeKEYID,
      //     EMPKEYID: $scope.EMPKEYID.KEYID,
      //     VISITTYPE: $scope.VISITTYPE.KEYID,
      //     DISCIPLINE: $scope.DISCIPLINE,
      //     VISITDATE:$scope.VISITDATE,
      //     TIMEIN: $scope.TIMEIN,
      //     TIMEOUT: $scope.TIMEOUT,
      //     HOURS: $scope.HOURS,
      //     MILEAGE: $scope.MILEAGE,
      //     NOTES: $scope.NOTES,
      //   };
      //   dataService.add('visit', newVisit).then(function(response){
      //     console.log(response);
      //     //edit patient table to udpate PATSTATUS from 'D' to 'A'
      //     dataService.edit('patient', {'KEYID': $scope.patient.KEYID, 'PATSTATUS': 'A'}).then(function(response){
      //       console.log(response);
      //       //reset patient
      //       dataService.get('patient', {'KEYID': patientService.get().KEYID}).then(function(data) {
      //         $scope.updatedPatient = data.data[0];
      //         patientService.set($scope.updatedPatient);
      //       });
      //     });
      //
      //     //add selected forms
      //     $scope.selectedForms.forEach(function(selectedFormType) {
      //       var form = {
      //         PATKEYID : $scope.patient.KEYID,
      //         // FIXME: LSTADMIT is just the last admit, we may
      //         // want to add a visit for a previous admit
      //         ADMITKEYID : $scope.patient.LSTADMIT,
      //         EPIKEYID : $scope.lastEpisodeKEYID,
      //         VISITID : response.keyid,
      //         FORMTYPE : selectedFormType.KEYID,
      //         FORMDATE : $scope.VISITDATE,
      //         ASSIGNEDTO : $scope.EMPKEYID.KEYID,
      //         STATUS : 1
      //       };
      //       dataService.add('form', form).then(function(msg) {
      //         console.log(msg);
      //       });
      //     });
      //     $uibModalInstance.close($scope.updatedPatient);
      //   })
      // }
    }
  }

  //Get currentAdmitKEYID
  dataService.get('admission', {'PATKEYID':$scope.patient.KEYID}).then(function(data){
    var admitArr = data.data;
    $scope.currentAdmit = admitArr.slice(-1).pop();
    $scope.currentAdmitKEYID = $scope.currentAdmit.KEYID;
  });

  //add new admit
  function addAdmit() {
    var newAdmit = $scope.currentAdmit;
    newAdmit.NAREASON=null;
    newAdmit.PATSTATUS = 'A';
    newAdmit.SOCDATE = null;
    newAdmit.DCDATE = null;
    newAdmit.ADMITNO = newAdmit.ADMITNO + 1;
    newAdmit.DCREASON=null;
    delete newAdmit['KEYID'];
    console.log(newAdmit)
    dataService.add('admission', newAdmit).then(function(response) {
      console.log(response);

      //edit patient table to udpate PATSTATUS from 'D' to 'A'
      dataService.edit('patient', {'KEYID': $scope.patient.KEYID, 'PATSTATUS': 'A', 'LSTADMIT': response.keyid}).then(function(response){
        console.log(response);
        //reset patient
        dataService.get('patient', {'KEYID': patientService.get().KEYID}).then(function(data) {
          $scope.updatedPatient = data.data[0];
          patientService.set($scope.updatedPatient);
          $scope.patient=patientService.get();
          addVisit();
        });
      });
    });
  }

  function addVisit() {
    var newVisit = {
      PATKEYID: $scope.patient.KEYID,
      ADMITKEYID: $scope.patient.LSTADMIT,
      EPIKEYID: $scope.lastEpisodeKEYID,
      EMPKEYID: $scope.EMPKEYID.KEYID,
      VISITTYPE: $scope.VISITTYPE.KEYID,
      DISCIPLINE: $scope.DISCIPLINE,
      VISITDATE: $filter("date")($scope.VISITDATE, 'yyyy-MM-dd'),
      TIMEIN: $scope.TIMEIN,
      TIMEOUT: $scope.TIMEOUT,
      HOURS: $scope.HOURS,
      MILEAGE: $scope.MILEAGE,
      NOTES: $scope.NOTES,
    };
    console.log(newVisit)
    dataService.add('visit', newVisit).then(function(response){
      console.log(response);
      //add selected forms
      $scope.selectedForms.forEach(function(selectedFormType) {
        var form = {
          PATKEYID : $scope.patient.KEYID,
          // FIXME: LSTADMIT is just the last admit, we may
          // want to add a visit for a previous admit
          ADMITKEYID : $scope.patient.LSTADMIT,
          EPIKEYID : $scope.lastEpisodeKEYID,
          VISITID : response.keyid,
          FORMTYPE : selectedFormType.KEYID,
          FORMDATE : $filter("date")($scope.VISITDATE, 'yyyy-MM-dd'),
          ASSIGNEDTO : $scope.EMPKEYID.KEYID,
          STATUS : 0
        };
        dataService.add('form', form).then(function(msg) {
          console.log(msg);
        });
      });
      $uibModalInstance.close($scope.updatedPatient);
    });
  }



});
