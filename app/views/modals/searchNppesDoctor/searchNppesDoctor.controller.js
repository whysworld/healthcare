app.controller('SearchNppesDoctorController', function($scope, $uibModalInstance, dataService, addRefPhysician) {

	$scope.close = function() {
		$uibModalInstance.close();
	};

	$scope.errors = [];
	$scope.doctors = [];
	var allDbDoctors = [];

	$scope.getDbDoctors = function() {
	  dataService.getAll('doctor').then(function(data) {
	    allDbDoctors = data.data;
	  });
	}



	$scope.updateDoctorsList = function() {
		var doctorRequest = {
			state : $scope.state || "",
			first_name : $scope.first_name || "",
			last_name : $scope.last_name || "",
			city : $scope.city || ""
		}

		dataService.get('nppesDoctor', doctorRequest).then(function(data) {
			data = data.data;

			console.log("data:");
			console.log(data);

			if ("Errors" in data) {
				for (var data_error in data.Errors) {
					if (data.Errors.hasOwnProperty(data_error)) {
						$scope.errors.push(data.Errors[data_error]);
					}
				}
			}
			if ("results" in data) {
				$scope.doctors = [];
				for (var data_result in data.results) {
					if (data.results.hasOwnProperty(data_result)) {
						var doctor_result = data.results[data_result];
						var doctor = {
							ACTIVE : 1
						};

						if (doctor_result.hasOwnProperty("basic")) {
							if (doctor_result.basic.hasOwnProperty("first_name")) {
								doctor.FNAME = doctor_result.basic.first_name;
							}
							if (doctor_result.basic.hasOwnProperty("last_name")) {
								doctor.LNAME = doctor_result.basic.last_name;
							}
							if (doctor_result.basic.hasOwnProperty("middle_name")) {
								doctor.MINITIAL = doctor_result.basic.middle_name.charAt(0);
							}
						}

						// TODO: for the MVP we are just taking the first address from addresses,
						// which is usually the LOCATION address and not the MAILING one
						if (doctor_result.hasOwnProperty("addresses") && doctor_result.addresses.length > 0) {
							var address_result = doctor_result.addresses[0];

							if (address_result.hasOwnProperty("address_1")) {
								doctor.ADDRESS = address_result.address_1;
							}
							if (address_result.hasOwnProperty("address_2")) {
								if (doctor.hasOwnProperty("ADDRESS")) {
									doctor.ADDRESS = doctor.ADDRESS + " " + address_result.address_2;
								} else {
									doctor.ADDRESS = address_result.address_2;
								}
							}

							if (address_result.hasOwnProperty("city")) {
								doctor.CITY = address_result.city;
							}

							if (address_result.hasOwnProperty("state")) {
								doctor.STATE = address_result.state;
							}

							if (address_result.hasOwnProperty("postal_code")) {
								doctor.ZIP = address_result.postal_code;
							}

							if (address_result.hasOwnProperty("telephone_number")) {
								doctor.PHONE = address_result.telephone_number;
							}

							if (address_result.hasOwnProperty("fax_number")) {
								doctor.FAX = address_result.fax_number
							}
						}

						// The email address is currently not part of the NPPES records
						// NOTES should not be taken from any external DB

						if (doctor_result.hasOwnProperty("number")) {
							doctor.NPI = doctor_result.number;
						}

						if (doctor.hasOwnProperty("NPI")) {
							$scope.doctors.push(doctor);
						}
					}
				}
			}
		});
	};

	//add new doctor
	$scope.addNewDoctor = function(doctor) {
		//get list of doctors in db
		dataService.getAll('doctor').then(function(data) {
			allDbDoctors = data.data;
		 	//see if added doc is already in list of doctors in the db
			//return array of all objects that have same NPI as added doc
	 		var DbDoctors = allDbDoctors.filter(function(dbDoc) {
	 			return dbDoc.NPI == doctor.NPI;
	 		});

			//if doc is not already in db
			if (DbDoctors.length == 0) {
				console.log('adding new doc to db');
				//add doc to db
				dataService.add('doctor', doctor).then(function(data) {
					$scope.getDoctors();
					//if adding not adding referral physician
					if (addRefPhysician!==true) {
						//assign selected doctor as Physician
						// $scope.npiSearchTextChange();
						$scope.selectPhysician(doctor);
						// $scope.npiSearchText = "";
						// $scope.selectedDoctor = doctor;
					}
				});
			} else { //if doc is already in db
				console.log('doc is already in db');
				if (addRefPhysician!==true) {
					//assign selected doc as Physician
					$scope.selectPhysician(DbDoctors[0]);
					// $scope.npiSearchText = "";
					// $scope.selectedDoctor = DbDoctors[0];
				}
			}
			$uibModalInstance.close(doctor);
	 });
 };

	$scope.states = [
	{
		"name": "Alabama",
		"abbreviation": "AL"
	},
	{
		"name": "Alaska",
		"abbreviation": "AK"
	},
	{
		"name": "Arizona",
		"abbreviation": "AZ"
	},
	{
		"name": "Arkansas",
		"abbreviation": "AR"
	},
	{
		"name": "California",
		"abbreviation": "CA"
	},
	{
		"name": "Colorado",
		"abbreviation": "CO"
	},
	{
		"name": "Connecticut",
		"abbreviation": "CT"
	},
	{
		"name": "Delaware",
		"abbreviation": "DE"
	},
	{
		"name": "District Of Columbia",
		"abbreviation": "DC"
	},
	{
		"name": "Florida",
		"abbreviation": "FL"
	},
	{
		"name": "Georgia",
		"abbreviation": "GA"
	},
	{
		"name": "Hawaii",
		"abbreviation": "HI"
	},
	{
		"name": "Idaho",
		"abbreviation": "ID"
	},
	{
		"name": "Illinois",
		"abbreviation": "IL"
	},
	{
		"name": "Indiana",
		"abbreviation": "IN"
	},
	{
		"name": "Iowa",
		"abbreviation": "IA"
	},
	{
		"name": "Kansas",
		"abbreviation": "KS"
	},
	{
		"name": "Kentucky",
		"abbreviation": "KY"
	},
	{
		"name": "Louisiana",
		"abbreviation": "LA"
	},
	{
		"name": "Maine",
		"abbreviation": "ME"
	},
	{
		"name": "Maryland",
		"abbreviation": "MD"
	},
	{
		"name": "Massachusetts",
		"abbreviation": "MA"
	},
	{
		"name": "Michigan",
		"abbreviation": "MI"
	},
	{
		"name": "Minnesota",
		"abbreviation": "MN"
	},
	{
		"name": "Mississippi",
		"abbreviation": "MS"
	},
	{
		"name": "Missouri",
		"abbreviation": "MO"
	},
	{
		"name": "Montana",
		"abbreviation": "MT"
	},
	{
		"name": "Nebraska",
		"abbreviation": "NE"
	},
	{
		"name": "Nevada",
		"abbreviation": "NV"
	},
	{
		"name": "New Hampshire",
		"abbreviation": "NH"
	},
	{
		"name": "New Jersey",
		"abbreviation": "NJ"
	},
	{
		"name": "New Mexico",
		"abbreviation": "NM"
	},
	{
		"name": "New York",
		"abbreviation": "NY"
	},
	{
		"name": "North Carolina",
		"abbreviation": "NC"
	},
	{
		"name": "North Dakota",
		"abbreviation": "ND"
	},
	{
		"name": "Ohio",
		"abbreviation": "OH"
	},
	{
		"name": "Oklahoma",
		"abbreviation": "OK"
	},
	{
		"name": "Oregon",
		"abbreviation": "OR"
	},
	{
		"name": "Pennsylvania",
		"abbreviation": "PA"
	},
	{
		"name": "Rhode Island",
		"abbreviation": "RI"
	},
	{
		"name": "South Carolina",
		"abbreviation": "SC"
	},
	{
		"name": "South Dakota",
		"abbreviation": "SD"
	},
	{
		"name": "Tennessee",
		"abbreviation": "TN"
	},
	{
		"name": "Texas",
		"abbreviation": "TX"
	},
	{
		"name": "Utah",
		"abbreviation": "UT"
	},
	{
		"name": "Vermont",
		"abbreviation": "VT"
	},
	{
		"name": "Virginia",
		"abbreviation": "VA"
	},
	{
		"name": "Washington",
		"abbreviation": "WA"
	},
	{
		"name": "West Virginia",
		"abbreviation": "WV"
	},
	{
		"name": "Wisconsin",
		"abbreviation": "WI"
	},
	{
		"name": "Wyoming",
		"abbreviation": "WY"
	}
	];

});
