app.config(function($stateProvider) {
  $stateProvider.state('religions', {
    url: '/religions',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/religions/religions.html',
        controller: 'ReligionsController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
