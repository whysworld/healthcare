app.controller('ReligionsController', function($scope, dataService) {

  dataService.getAll('religion').then(function (data) {
    $scope.religions = data.data;
  });

  $scope.add = function () {
    dataService.add('religion', {'DESC':$scope.DESC}).then(function (response) {
      console.log(response);
      dataService.getAll('religion').then(function (data) {
        $scope.religions = data.data;
        $scope.DESC='';
      });
    });
  };

  $scope.remove = function(religion) {
    console.log(religion);
    dataService.delete('religion', {'KEYID': religion.KEYID}).then(function(response) {
      console.log(response);
      dataService.getAll('religion').then(function (data) {
        $scope.religions = data.data;
      });
    });
  };

  $scope.edit = function(religion, index) {
    console.log(religion, index)
    $scope.activeReligion = index;
  };

  $scope.save=function(religion) {
    console.log(religion);
    dataService.edit('religion', religion).then(function(response) {
      console.log(response);
      $scope.activeReligion = false;
      dataService.getAll('religion').then(function (data) {
        $scope.religions = data.data;
      });
    });
  };

  $scope.cancel=function() {
    $scope.activeReligion = false;
  };
});
