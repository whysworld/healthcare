app.controller('SchedulingController', function($scope, $filter, $uibModal, patientService, dataService) {

  $scope.patient = patientService.get();
  $scope.patient.DOB=$filter("date")($scope.patient.DOB, "MM-dd-yyyy")

  $scope.printSection = function(divName) {
    window.print();
  }

  // $scope.selected = function(day){
  //   console.log(day)
  // }
  $scope.highlight=function(visit) {
    $scope.hightlightVisit = visit.KEYID
  }

  $scope.viewVisit = function(visit, event) {
    $scope.hightlightVisit=''
    event.preventDefault();
    var modalInstance = $uibModal.open({
      controller: 'ViewVisitController',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/viewVisit/viewVisit.html',
      // windowClass: 'modal fade in',
      windowClass: 'display-visit-modal',
      size: 'md',
      resolve: {
        visit: visit,
        patient: $scope.patient,
        episode: function() {
          return $scope.EPISODE;
        },
      }
    });
    modalInstance.result.then(function () {
      dataService.get('visit',{'PATKEYID':$scope.patient.KEYID}).then(function(data){
        // console.log("modal close", data.data)
        $scope.visits = data.data;
        console.log('visits', $scope.visits);
        showVisitsByEpisode(data.data);
        getVisitTypeDesc();
        getEmployeeName();
        isLastFiveDays();
        $scope.hightlightVisit='';
      });
    });
  };

  $scope.newVisit = function(selected, event) {
    //if there is soc scheduled but no episode created yet - do not allow schedule other visits
    if ( ($scope.SOCVISITSCHEDULED !== null) && ($scope.ADMIT.SOCDATE === null) ) {
      alert('Episode must be created before scheduling visits')
    } else {

      //if current patient is already discharged
      if($scope.patient.PATSTATUS === 'D') {
        if (moment(selected).isSameOrAfter($scope.EPISODE.DCDATE)) {
          event.preventDefault();
          var modalInstance = $uibModal.open({
            controller: 'ScheduleAfterDischargeController',
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './views/modals/scheduleAfterDischarge/scheduleAfterDischarge.html',
            size: 'lg',
            resolve: {
              selected: selected,
              action:null,
            }
          });
          modalInstance.result.then(function () {
            dataService.get('visit',{'PATKEYID':$scope.patient.KEYID}).then(function(data){
              console.log("modal close", data.data);
              $scope.visits = data.data;
              showVisitsByEpisode(data.data);
              getVisitTypeDesc();
              getEmployeeName();
              isLastFiveDays();
            });
          });
        } else {
          alert('Can not schedule visit prior to patient discharge date.');
        }
      } else if ($scope.patient.PATSTATUS === 'A'){
        //if episode is create, visits are not allowed to be scheduled outside of episode date
        if (moment(selected).isBetween(moment($scope.EPISODE.FROMDATE), moment($scope.EPISODE.THRUDATE), null, '[]')) {
          openNewVisitModal(selected);
        } else {
          alert('Cannot schedule visits outside of current episode');
        }
      } else if ($scope.patient.PATSTATUS==='T'){
        dataService.get('form', {ADMITKEYID:$scope.EPISODE.ADMITKEYID, EPIKEYID:$scope.EPISODE.KEYID, FORMTYPE:6}).then(function (data) {
          var formdate = data.data[0].FORMDATE;
          //if patient is transferred, visits are only allowed to be scheduled between transfer date and end of episode
          if (moment(selected).isBetween(moment(formdate), moment($scope.EPISODE.THRUDATE), null, '[]')) {
            openNewVisitModal(selected);
          } else {
            alert('Cannot schedule visit');
          }
        });
      } else {
        openNewVisitModal(selected);
      }
    }
  };

  function openNewVisitModal (selected) {
    event.preventDefault();
    var modalInstance = $uibModal.open({
      controller: 'NewVisitController',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: './views/modals/newVisit/newVisit.html',
      size: 'md',
      resolve: {
        selected: selected,
        patient: $scope.patient,
        episode: function() {
          return $scope.EPISODE;
        },
        action:null,
      }
    });
    modalInstance.result.then(function () {
      dataService.get('visit',{'PATKEYID':$scope.patient.KEYID}).then(function(data){
        console.log("modal close", data.data);
        $scope.visits = data.data;
        showVisitsByEpisode(data.data);
        getVisitTypeDesc();
        getEmployeeName();
        isLastFiveDays();
      });
    });
  }
  console.log('$scope.patient', $scope.patient);

  dataService.get('admission', {PATKEYID:$scope.patient.KEYID}).then(function (data) {
    $scope.admissions=data.data;
    $scope.ADMIT=data.data[data.data.length-1];
  });

  dataService.get('admission', {'KEYID': $scope.patient.LSTADMIT}).then(function(data){
    // $scope.admissions=data.data;
    // console.log($scope.admissions)
    $scope.lastAdmit = data.data[0];

    // $scope.ADMIT = $scope.lastAdmit;

    dataService.get('episode', {'ADMITKEYID': $scope.patient.LSTADMIT}).then(function(data){
      console.log(data.data)
      if (data.data.length<1) {
        $scope.episode = null;
        $scope.episodes = null;
      } else {
        $scope.episodes=data.data;
        var episode = data.data.slice(-1).pop();
        episode.FROMDATE=$filter("date")(episode.FROMDATE, 'MM-dd-yyyy');
        episode.THRUDATE=$filter("date")(episode.THRUDATE, 'MM-dd-yyyy');
        $scope.episode=episode;
        $scope.EPISODE = episode;
      }

      dataService.get('visit', {ADMITKEYID: $scope.patient.LSTADMIT}).then(function (data) {
        $scope.visits = data.data;
        console.log($scope.visits);

        showVisitsByEpisode(data.data);
        getVisitTypeDesc();
        getEmployeeName();
        $scope.sortByVISITDATE();
        isStartOfCare();
        isLastFiveDays();
      });
    });
  });

  function clear() {
    $scope.allVisits = [];
    $scope.SNVisits = [];
    $scope.HAVisits = [];
    $scope.PTVisits = [];
    $scope.OTVisits = [];
    $scope.STVisits = [];
    $scope.MSWVisits = [];
    $scope.OTHERVisits = [];
  }
  // var allVisits = [];
  // var SNVisits = [];
  // var HAVisits = [];
  // var PTVisits = [];
  // var OTVisits = [];
  // var STVisits = [];
  // var MSWVisits = [];
  // var OTHERVisits = [];
  function filterVisits(visits) {
    $scope.selected="ALL";
    clear();
    $scope.allVisits = visits;
    // $scope.visits = $scope.allVisits;
    // console.log(visits);
    for (var i = 0; i < visits.length; i++) {
      // console.log(visits[i])
      switch(visits[i].DISCIPLINE) {
        case "SN":
          // patients[i].PATSTATUS = 'A';
          $scope.SNVisits.push(visits[i]);
          break;
        case "HA":
          // visits[i].PATSTATUS = 'P';
          $scope.HAVisits.push(visits[i]);
          break;
        case "PT":
          $scope.PTVisits.push(visits[i]);
          break;
        case "OT":
          $scope.OTVisits.push(visits[i]);
          break;
        case "ST":
          $scope.STVisits.push(visits[i]);
          break;
        case "MSW":
          $scope.MSWVisits.push(visits[i]);
          break;
        default:
          $scope.OTHERVisits.push(visits[i]);
          break;
      }
    }
  };

  // $scope.selected = 'ALL';

  $scope.filter = function(discipline) {
    $scope.selected = discipline
    // console.log(discipline)
    // console.log(SNVisits)
    switch(discipline) {
      case "ALL":
        $scope.visits = $scope.allVisits;
        break;
      case "SN":
        $scope.visits = $scope.SNVisits;
        break;
      case "HA":
        $scope.visits = $scope.HAVisits;
        break;
      case "PT":
        $scope.visits = $scope.PTVisits;
        break;
      case "OT":
        $scope.visits = $scope.OTVisits;
        break;
      case "ST":
        $scope.visits = $scope.STVisits;
        break;
      case "MSW":
        $scope.visits = $scope.MSWVisits;
        break;
      case "OTHER":
        $scope.visits = $scope.OTHERVisits;
        break;
    }
  };

  function getVisitTypeDesc() {
    //get all visitTypes
    dataService.getAll('visitType').then(function(data){
      $scope.visitTypes = data.data;
      //getting visit type discipline and desc
      for (var i = 0; i < $scope.showVisits.length; i++) {
        for (var j = 0; j < $scope.visitTypes.length; j++) {
          if ($scope.showVisits[i].VISITTYPE === $scope.visitTypes[j].KEYID) {
            $scope.showVisits[i].DISCIPLINE = $scope.visitTypes[j].DISCIPLINE;
            $scope.showVisits[i].DESC = $scope.visitTypes[j].DESC;
          }
        }
      }
      filterVisits($scope.showVisits);
    });
  }

  function getEmployeeName() {
    //get all visitTypes
    dataService.getAll('employee').then(function(data){
      $scope.employees = data.data;
      //getting visit type discipline and desc
      for (var i = 0; i < $scope.showVisits.length; i++) {
        for (var j = 0; j < $scope.employees.length; j++) {
          if ($scope.showVisits[i].EMPKEYID === $scope.employees[j].KEYID) {
            $scope.showVisits[i].EMPFNAME = $scope.employees[j].FNAME;
            $scope.showVisits[i].EMPLNAME = $scope.employees[j].LNAME;
          }
        }
      }
      filterVisits($scope.showVisits);
    });
  }

  function showVisitsByEpisode(visits) {
    //show visits for selected episode
    $scope.showVisits = [];
    // console.log($scope.visits);
    if ($scope.EPISODE !== undefined) {
      visits.forEach(function(visit) {
        if (visit.EPIKEYID === $scope.EPISODE.KEYID) {
          $scope.showVisits.push(visit);
        }
        visit.VISITDATE=$filter("date")(visit.VISITDATE, 'MM-dd-yyyy')
      });
    } else {
      $scope.showVisits = visits
    }

    console.log($scope.showVisits)
  }





  function isStartOfCare() {
    console.log("ASDfs")
    dataService.get('form', {ADMITKEYID:$scope.patient.LSTADMIT}).then(function (data) {
      var forms = data.data;
      console.log(forms)
      if (forms.length>0) {
        for (var i = 0; i < forms.length; i++) {
          if (forms[i].FORMTYPE === 2) {
            $scope.SOCVISITSCHEDULED = forms[i].FORMDATE;
            $scope.SOCVISITSCHEDULED = new Date($filter("date")($scope.SOCVISITSCHEDULED, 'yyyy/MM/dd'));
            console.log('start of care scheduled',$scope.SOCVISITSCHEDULED);
            break;
          } else {
            $scope.SOCVISITSCHEDULED = null;
          }
        }
      } else {
        $scope.SOCVISITSCHEDULED = null;
      }
      showCalendar();

      if ($scope.patient.PATSTATUS==='T') {
        var tempFormArr=[]
        forms.forEach(function(form) {
          if (form.FORMTYPE===6) {
            tempFormArr.push(form);
          }
        })
        console.log(tempFormArr)
        $scope.patientTransferDate=tempFormArr[tempFormArr.length-1].FORMDATE
        console.log($scope.patientTransferDate)
      }
    });
  }


  function showCalendar(){
    console.log('show calendar')
    $scope.showCalendar = true;
  }
        // console.log('visits', $scope.visits);
      // if ($scope.visits.length>0) {
      //   for (var i = 0; i < $scope.visits.length; i++) {
      //     if ($scope.visits[i].VISITTYPE === 1) {
      //       $scope.SOCVISITSCHEDULED = $scope.visits[i].VISITDATE;
      //       $scope.SOCVISITSCHEDULED = new Date($filter("date")($scope.SOCVISITSCHEDULED, 'yyyy/MM/dd'));
      //       console.log('start of care scheduled',$scope.SOCVISITSCHEDULED);
      //       // isLastFiveDays();
      //     } else {
      //       $scope.SOCVISITSCHEDULED = null;
      //     }
      //   }
      // } else {
      //   $scope.SOCVISITSCHEDULED = null;
      // }




  $scope.day = moment(new Date());
  // $scope.day = moment(new Date("06/07/2017")); //ONLY for testing purposes

  function isLastFiveDays() {
    console.log($scope.day)
    if($scope.EPISODE !== undefined) {
      var endOfEpisode = moment($scope.EPISODE.THRUDATE);
      var lastFiveDays = endOfEpisode.clone().subtract(5, 'days');
      //is it lastFiveDays of current episode?
      var isTodayBetweenLastFiveDays = ($scope.day.isBetween(lastFiveDays, endOfEpisode, null, '(]'));
      var hasRecertification;
      var hasTransferWDischarge;
      var hasTransferWODischarge;
      var hasDischarged;

      if (isTodayBetweenLastFiveDays === false) {
        $scope.showNotice = false;
      } else {
        for (var i = 0; i < $scope.visits.length; i++) {
          if( ($scope.visits[i].VISITTYPE === 84 ) && (moment($scope.visits[i].VISITDATE).isBetween(lastFiveDays, endOfEpisode, null, '(]')) ) {
            hasRecertification = true;
            $scope.showNotice = false;
            break;
          } else if ($scope.visits[i].VISITTYPE === 85) {     //has trasfer been scheduled?
            hasTransferWDischarge = true;
            $scope.showNotice = false;
            break;
          } else if ($scope.visits[i].VISITTYPE === 86) { //has trasfer been scheduled?
            hasTransferWODischarge = true;
            $scope.showNotice = false;
            break;
          } else if ($scope.visits[i].VISITTYPE === 87) { //has patient been discharge?
            hasDischarged = true;
            $scope.showNotice = false;
            break;
          } else {
            $scope.showNotice = true;
          }
        }
      }
    }
  }

  $scope.changeEpisode=function() {
    //set episode for calendar to display
    $scope.episode = $scope.EPISODE;
    //show visits for selected episode
    $scope.showVisits = [];
    $scope.visits.forEach(function(visit) {
      if (visit.EPIKEYID === $scope.EPISODE.KEYID) {
        $scope.showVisits.push(visit);
      }
    });
    getVisitTypeDesc();
    getEmployeeName()
    console.log($scope.showVisits)
  };

  $scope.changeAdmit=function() {
    console.log($scope.ADMIT)
    // $scope.EPISODE = null;
    // var admitNO = $scope.ADMIT.ADMITNO;

    dataService.get('episode', {'ADMITKEYID': $scope.ADMIT.KEYID}).then(function(data){
      if (data.data.length<1) {
        // $scope.episode = null;
        $scope.episodes = null;
      } else {
        $scope.episodes=data.data;
        var episode = data.data.slice(-1).pop();
        episode.FROMDATE=$filter("date")(episode.FROMDATE, 'MM-dd-yyyy');
        episode.THRUDATE=$filter("date")(episode.THRUDATE, 'MM-dd-yyyy');
        $scope.episode=episode;
        $scope.EPISODE = episode;
      }

      dataService.get('visit', {ADMITKEYID: $scope.ADMIT.KEYID}).then(function (data) {
        $scope.showVisits = data.data;
        // console.log($scope.visits);
      })
    })
    // dataService.get('admission', {'PATKEYID': $scope.patient.KEYID}).then(function(data){
    //   $scope.admissions=data.data;
    //   $scope.lastAdmit = data.data.slice(-1).pop();
    //
    //   $scope.ADMIT = $scope.lastAdmit.ADMITNO

  }

  //sort by visit date
  // $scope.sortVISITDATE = false;
  $scope.sortByVISITDATE = function() {
    $scope.sortDISCIPLINE = true;
    $scope.sortDESC = true;
    $scope.sortVISITDATE = !$scope.sortVISITDATE;
    if ($scope.sortVISITDATE === false) {
      $scope.showVisits.sort(function(a,b) {
        var dateA = a.VISITDATE.toLowerCase();
        var dateB = b.VISITDATE.toLowerCase();

        if(dateA < dateB)
          return -1;
        if(dateA > dateB)
          return 1;
        return 0;
      });
    } else if($scope.sortVISITDATE === true) {
      $scope.showVisits.sort(function(a,b) {
        var dateA = a.VISITDATE.toLowerCase();
        var dateB = b.VISITDATE.toLowerCase();

        if(dateA > dateB)
          return -1;
        if(dateA < dateB)
          return 1;
        return 0;
      });
    }
  };

  //sort by visit discipline
  // $scope.sortDISCIPLINE = true;
  $scope.sortByDISCIPLINE = function() {
    $scope.sortVISITDATE = true;
    $scope.sortDESC = true;
    $scope.sortDISCIPLINE = !$scope.sortDISCIPLINE;
    console.log($scope.sortDISCIPLINE);
    if ($scope.sortDISCIPLINE === false) {
      $scope.showVisits.sort(function(a,b) {
        var dateA = a.DISCIPLINE.toLowerCase();
        var dateB = b.DISCIPLINE.toLowerCase();

        if(dateA < dateB)
          return -1;
        if(dateA > dateB)
          return 1;
        return 0;
      });
    } else if($scope.sortDISCIPLINE === true) {
      $scope.showVisits.sort(function(a,b) {
        var dateA = a.DISCIPLINE.toLowerCase();
        var dateB = b.DISCIPLINE.toLowerCase();

        if(dateA > dateB)
          return -1;
        if(dateA < dateB)
          return 1;
        return 0;
      });
    }
  };
  //sort by visit type
  // $scope.sortDESC = true;
  $scope.sortByDESC = function() {
    $scope.sortVISITDATE = true;
    $scope.sortDISCIPLINE = true;
    $scope.sortDESC = !$scope.sortDESC;
    console.log($scope.sortDESC);
    if ($scope.sortDESC === false) {
      $scope.showVisits.sort(function(a,b) {
        var dateA = a.DESC.toLowerCase();
        var dateB = b.DESC.toLowerCase();

        if(dateA < dateB)
          return -1;
        if(dateA > dateB)
          return 1;
        return 0;
      });
    } else if($scope.sortDESC === true) {
      $scope.showVisits.sort(function(a,b) {
        var dateA = a.DESC.toLowerCase();
        var dateB = b.DESC.toLowerCase();

        if(dateA > dateB)
          return -1;
        if(dateA < dateB)
          return 1;
        return 0;
      });
    }
  };

  convertLANGUAGEcode();

  function convertLANGUAGEcode() {
    if ($scope.patient.LANGUAGE) {
      dataService.get('language', {'KEYID': $scope.patient.LANGUAGE}).then(function(data) {
        $scope.patient.LANGUAGEDESC = data.data[0].DESC;
      });
    }
  }

  convertM0140code();
  function convertM0140code() {
  	switch ($scope.patient.M0140) {
  		case '1':
        $scope.patient.M0140DESC = 'American Indian or Alaska Native';
  			break;
  		case '2':
        $scope.patient.M0140DESC = 'Asian';
  			break;
      case '3':
        $scope.patient.M0140DESC = 'Black or African-American';
        break;
      case '4':
        $scope.patient.M0140DESC = 'Hispanic or Latino';
        break;
      case '5':
        $scope.patient.M0140DESC = 'Native Hawaiian or Pacific Islander';
        break;
      case '6':
        $scope.patient.M0140DESC = 'White';
        break;
  	}
  }


});
