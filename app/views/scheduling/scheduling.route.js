app.config(function($stateProvider) {
  $stateProvider.state('scheduling', {
    url: '/scheduling',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/scheduling/scheduling.html',
        controller: 'SchedulingController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
