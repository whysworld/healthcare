app.config(function($stateProvider) {
  $stateProvider.state('oasisList', {
    url: '/oasisList',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/oasisList/oasisList.html',
        controller: 'OasisListController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
