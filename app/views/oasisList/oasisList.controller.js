app.controller('OasisListController', function($scope, CONSTANTS, dataService){

  //get all locked oasis
  dataService.get('oasisC2_2_20_1', {LOCKED: 1}).then(function(data){
    console.log(data.data);
    $scope.oasisList=data.data;

    $scope.oasisList.forEach(function (oasis) {
      //set link to download
      oasis.LINK = CONSTANTS.api.baseUrl + CONSTANTS.api.path + CONSTANTS.api.query + 'getOasisC2_2_20_1_xmlFile&KEYID=' + oasis.KEYID;
      //find number of days ago oasis SOC was
      var oasisDate = moment(oasis.M0090_INFO_COMPLETED_DT);
      var currentDate = Date.now();
      oasis.DAYSAGO = oasisDate.diff(currentDate, 'days');
    });
  });

  //select All checkbox
  $scope.checkAllReady = function() {
    if ($scope.selectedAllReady) {
      $scope.selectedAllReady = true;
      $scope.showExportOasisBtn = true;

    } else {
      $scope.selectedAllReady = false;
      $scope.showExportOasisBtn = false;
    }
    angular.forEach($scope.oasisList, function(oasis) {
      oasis.selectedReady = $scope.selectedAllReady;
    });
  };

  //show 'Export' Button
  $scope.exportOasisBtn = function() {
    for (var i = 0; i < $scope.oasisList.length; i++) {
      if ($scope.oasisList[i].selectedReady === true) {
          $scope.showExportOasisBtn = true;
          break;
      } else {
          $scope.showExportOasisBtn = false;
      }
    }
  };

  //download .xml
  $scope.download = function() {
    //get all selected oasis
    var selectedOasisArr = [];
    angular.forEach($scope.oasisList, function(oasis) {
        if (oasis.selectedReady === true) {
            selectedOasisArr.push(oasis);
        }
    });
    console.log('selected Oasis to export', selectedOasisArr);
    //open and save each selected oasis
    angular.forEach(selectedOasisArr, function (selOasis) {
      window.open(selOasis.LINK, '_blank');
    });
  };
});
