app.config(function($stateProvider) {
  $stateProvider.state('homeDashboard', {
    url: '/homeDashboard',
    views: {
      'primaryNavigation': {
        templateUrl: 'views/navigation/navigation.html'
      },
      'pageContent': {
        templateUrl: 'views/homeDashboard/homeDashboard.html',
        controller: 'HomeDashboardController'
      },
      'primaryFooter': {
        templateUrl: 'views/footer/footer.html'
      }
    }
  });
});
