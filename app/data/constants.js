app.constant('CONSTANTS', {
  api: {
    baseUrl: 'http://dev.mayahealthcare.com',
    path: '/dev/services/api.php',
    query: '?x=',
    actions: {
      admission: {
        add: 'addAdmissionForm',
        delete: 'deleteAdmissionForm',
        edit: 'editAdmissionForm',
        get: 'getAdmissionForm',
        getAll: 'getAdmissionForms'
      },
      agencyConfig: {
        get: 'getAgencyConfiguration',
        edit: 'editAgencyConfiguration'
      },
      claim: {
        get: 'getClaim',
        list: 'listClaims'
      },
      config: {
        add: 'addConfiguration',
        delete: 'deleteConfiguration',
        edit: 'editConfiguration',
        get: 'getConfiguration',
        getAll: 'getConfigurations'
      },
      diagnosis: {
        add: 'addDiagnosis',
        delete: 'deleteDiagnosis',
        edit: 'editDiagnosis',
        get: 'getDiagnosis',
        getAll: 'getDiagnoses',
        search: 'searchDiagnosis'
      },
      doctor: {
        add: 'addDoctor',
        delete: 'deleteDoctor',
        edit: 'editDoctor',
        get: 'getDoctor',
        getAll: 'getDoctors'
      },
      eligibilitycheck: {
        get: 'getEligiblitydetails',
        getAll: 'getEligiblitydata'
      },
      eligibilitypopup: {
        get: 'getEligiblitydetailspopup'
      },
      employee: {
        add: 'addEmployee',
        delete: 'deleteEmployee',
        edit: 'editEmployee',
        get: 'getEmployee',
        getAll: 'getEmployees'
      },
      employeeClass: {
        add: 'addEmployeeClass',
        delete: 'deleteEmployeeClass',
        edit: 'editEmployeeClass',
        get: 'getEmployeeClass',
        getAll: 'getEmployeeClasses'
      },
      employeePayrate: {
        add: 'addEmployeePayrate',
        delete: 'deleteEmployeePayrate',
        edit: 'editEmployeePayrate',
        get: 'getEmployeePayrate'
      },
      episode: {
        add: 'addEpisode',
        delete: 'deleteEpisode',
        edit: 'editEpisode',
        get: 'getEpisode',
        getAll: 'getEpisodes'
      },
      form: {
        add: 'addForm',
        delete: 'deleteForm',
        edit: 'editForm',
        get: 'getForm'
      },
      formType: {
        getAll: 'getFormTypes',
        get: 'getFormType'
      },
      ICD10Code: {
        get: 'getICD10Code',
        getAll: 'getICD10Codes',
        search: 'searchICD10Code'
      },
      Medication: {
        get: 'getMedication',
        getAll: 'getMedications',
        search: 'searchMedication'
      },
      intermediary: {
        add: 'addIntermediary',
        delete: 'deleteIntermediary',
        edit: 'editIntermediary',
        get: 'getIntermediary',
        getAll: 'getIntermediaries'
      },
      insurance: {
        add: 'addInsurance',
        delete: 'deleteInsurance',
        edit: 'editInsurance',
        get: 'getInsurance',
        getAll: 'getInsurances'
      },
      language: {
        add: 'addLanguage',
        delete: 'deleteLanguage',
        edit: 'editLanguage',
        get: 'getLanguage',
        getAll: 'getLanguages'
      },
      listPatientRecords: {
        get: 'listPatientRecords'
      },
      listPayroll: {
        get: 'listPayroll'
      },
      listQAForms: {
        get: 'listQAForms'
      },
      nppesDoctor: {
        get: 'getNppesDoctor'
      },
      oasisC2_2_20_1: {
        add: 'addOasisC2_2_20_1',
        get: 'getOasisC2_2_20_1',
        edit: 'editOasisC2_2_20_1',
        scrub: 'scrubOasisC2_2_20_1'
      },
      orders: {
        add: 'addOrders',
        get: 'getOrders',
        getAll: 'getAllOrders',
        edit: 'editOrders'
      },
      pageWelcomePage: {
        get: 'getWelcomePage'
      },
      patient: {
        add: 'addPatient',
        delete: 'deletePatient',
        edit: 'editPatient',
        get: 'getPatient',
        getAll: 'getPatients',
        list: 'listPatients',
        search: 'searchPatient'
      },
      patientBillingVisit: {
        get: 'getPatientBillingVisit'
      },
      patientConsent: {
        add: 'addPatientConsent',
        delete: 'deletePatientConsent',
        edit: 'editPatientConsent',
        get: 'getPatientConsent',
        submit: 'submitPatientConsent'
      },
      patientMedication: {
        add: 'addPatientMedication',
        get: 'getPatientMedication',
        edit: 'editPatientMedication',
        delete: 'deletePatientMedication',
        getPDF: 'getPDFPatientMedication'
      },
      payroll: {
        add: 'addPayroll',
        get: 'getPayroll'
      },
      planOfCare: {
        add: 'addPlanOfCare',
        get: 'getPlanOfCare',
        edit: 'editPlanOfCare',
        getPDF: 'getPDFPlanOfCare'
      },
      ppsInfo: {
        get: 'getPPSInfo'
      },
      referralSource: {
        add: 'addReferralSource',
        delete: 'deleteReferralSource',
        edit: 'editReferralSource',
        get: 'getReferralSource',
        getAll: 'getReferralSources'
      },
      religion: {
        add: 'addReligion',
        delete: 'deleteReligion',
        edit: 'editReligion',
        get: 'getReligion',
        getAll: 'getReligions'
      },
      revcode: {
        get: 'getRevCode',
        add: 'addRevCode',
        edit: 'editRevCode',
        delete: 'deleteRevCode'
      },
      routeSheet: {
        get: 'getRouteSheet',
        add: 'addRouteSheet',
        edit: 'editRouteSheet',
        delete: 'deleteRouteSheet'
      },
      skilledNursingNote: {
        add: 'addSkilledNursingNote',
        delete: 'deleteSkilledNursingNote',
        edit: 'editSkilledNursingNote',
        get: 'getSkilledNursingNote'
      },
      surgery: {
        add: 'addSurgery',
        delete: 'deleteSurgery',
        edit: 'editSurgery',
        get: 'getSurgery',
        getAll: 'getSurgerys',
        search: 'searchSurgery'
      },
      userGroup: {
        add: 'addUserGroup',
        delete: 'deleteUserGroup',
        edit: 'editUserGroup',
        get: 'getUserGroup',
        getAll: 'getUserGroups'
      },
      visit: {
        add: 'addVisit',
        delete: 'deleteVisit',
        edit: 'editVisit',
        get: 'getVisit',
        getAll: 'getVisits'
      },
      visitDiscipline: {
        getAll: 'getVisitDisciplines',
        add: 'addVisitDiscipline',
        edit: 'editVisitDiscipline',
        delete: 'deleteVisitDiscipline'
      },
      visitService: {
        add: 'addVisitService',
        delete: 'deleteVisitService',
        edit: 'editVisitService',
        get: 'getVisitService',
        getAll: 'getVisitServices'
      },
      visitType: {
        add: 'addVisitType',
        delete: 'deleteVisitType',
        edit: 'editVisitType',
        get: 'getVisitType',
        getAll: 'getVisitTypes',
        list: 'listVisitTypes'
      },
      visitTypeForm: {
        add: 'addVisitTypeForm',
        delete: 'deleteVisitTypeForm',
        get: 'getVisitTypeForm'
      },
      visitTypeInsurance: {
        add: 'addVisitTypeInsurance',
        delete: 'deleteVisitTypeInsurance',
        edit: 'editVisitTypeInsurance',
        get: 'getVisitTypeInsurance'
      },
      patientUpload: {
        add: 'addPatientUpload',
        delete: 'deletePatientUpload',
        get: 'getPatientUpload'
      },
      patientUploadFile: {
        get: 'getPatientUploadFile'
      },
      // TODO: check the end-points
      faceSheet: {
        getAll: 'patientDetails',
        getFormAll: 'formDetails',
        add: 'saveFaceFormData',
        get: 'getFromdata',
        list: 'getFromdata'
      },
      hhaSheet: {
        getAll: 'patientDetails',
        add: 'saveHHAData',
        get: 'getHHAFormData'
      },
      hhacareSheet: {
        getAll: 'patientDetails',
        add: 'saveHHACareData',
        get: 'getHHACareFormData'
      },
      missedvisitSheet: {
        getAll: 'patientDetails',
        add: 'saveMissedFormData',
        get: 'getMissedFormData'
      },
      lvnSheet: {
        getAll: 'patientDetails',
        add: 'saveLvnData',
        get: 'getLvnFormData'
      },
      ptvisitSheet: {
        getAll: 'patientDetails',
        add: 'savePTVisitData',
        get: 'getPTVisitFormData'
      },
      ptevalSheet: {
        getAll: 'patientDetails',
        add: 'savePTEvalData',
        get: 'getPTEvalFormData'
      },
      ptdischargeSheet: {
        getAll: 'patientDetails',
        add: 'savePTDischargeData',
        get: 'getPTDischargeFormData'
      },
      woundSheet: {
        getAll: 'patientDetails',
        add: 'saveWoundData',
        get: 'getWoundFormData'
      },
      remittanceadvice: {
        get: 'getRemittance',
        getAll: 'getRemittanceDetail'
      }
    }
  },
  disciplines: [{
    'SHORTDESC': 'SN',
    'LONGDESC': 'Nursing Care Services'
  }, {
    'SHORTDESC': 'PT',
    'LONGDESC': 'Physical Therapy Services'
  }, {
    'SHORTDESC': 'OT',
    'LONGDESC': 'Occupational Therapy Services'
  }, {
    'SHORTDESC': 'ST',
    'LONGDESC': 'Speech Pathology Services'
  }, {
    'SHORTDESC': 'MSW',
    'LONGDESC': 'Medical Social Services'
  }, {
    'SHORTDESC': 'HA',
    'LONGDESC': 'Home Health Aide Services'
  }],
  dischargeReasons: [{
    'CODE': '22',
    'DESC': 'No Further Home Health Care Needed'
  }, {
    'CODE': '25',
    'DESC': 'Family/Friends Assumed Responsibility'
  }, {
    'CODE': '26',
    'DESC': 'Patient Moved out of Area'
  }, {
    'CODE': '27',
    'DESC': 'Patient Refused Service'
  }, {
    'CODE': '28',
    'DESC': 'Transfer to Another HHA'
  }, {
    'CODE': '29',
    'DESC': 'Transfer to Outpatient Rehab'
  }, {
    'CODE': '30',
    'DESC': 'Physician Request'
  }, {
    'CODE': '32',
    'DESC': 'Lack of Funds'
  }, {
    'CODE': '33',
    'DESC': 'Lack of Progress'
  }, {
    'CODE': '34',
    'DESC': 'Transfer to Hospice'
  }, {
    'CODE': '35',
    'DESC': 'Transfer to Home Care'
  }, {
    'CODE': '36',
    'DESC': 'Admitted to Hospital'
  }, {
    'CODE': '37',
    'DESC': 'Admitted to SN/IC Facility'
  }, {
    'CODE': '38',
    'DESC': 'Death'
  }, {
    'CODE': '39',
    'DESC': 'Other'
  }],
  nonadmitReasons: [{
    'KEYID': 1,
    'DESC': 'Not Homebound'
  }, {
    'KEYID': 2,
    'DESC': 'Services Not Needed'
  }, {
    'KEYID': 3,
    'DESC': 'Patient Stabilized'
  }, {
    'KEYID': 4,
    'DESC': 'Referral Cancelled'
  }, {
    'KEYID': 5,
    'DESC': 'Refused Services'
  }, {
    'KEYID': 6,
    'DESC': 'Serviced by other HHA'
  }, {
    'KEYID': 7,
    'DESC': 'Patient Not Home'
  }, {
    'KEYID': 8,
    'DESC': 'Address Not Found'
  }, {
    'KEYID': 9,
    'DESC': 'Insurance Denial'
  }, {
    'KEYID': 10,
    'DESC': 'Admitted to Hospital'
  }, {
    'KEYID': 11,
    'DESC': 'Admitted to SNF'
  }, {
    'KEYID': 12,
    'DESC': 'Deceased'
  }, {
    'KEYID': 13,
    'DESC': 'Other'
  }],
  errors: [{
    ErrMessage: 'M0063_MEDICARE_NA: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0063_MEDICARE_NA',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0064_SSN_UK: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0064_SSN_UK',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0065_MEDICAID_NA: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0065_MEDICAID_NA',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0080_ASSESSOR_DISCIPLINE: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0080_ASSESSOR_DISCIPLINE',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0018_PHYSICIAN_UK: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0018_PHYSICIAN_UK',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0032_ROC_DT_NA: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0032_ROC_DT_NA',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0110_EPISODE_TIMING: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0110_EPISODE_TIMING',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0140_ETHNIC_AI_AN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0140_ETHNIC_AI_AN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0140_ETHNIC_ASIAN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0140_ETHNIC_ASIAN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0140_ETHNIC_BLACK: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0140_ETHNIC_BLACK',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0140_ETHNIC_HISP: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0140_ETHNIC_HISP',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0140_ETHNIC_NH_PI: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0140_ETHNIC_NH_PI',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0140_ETHNIC_WHITE: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0140_ETHNIC_WHITE',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0150_CPAY_MCAID_FFS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0150_CPAY_MCAID_FFS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0150_CPAY_OTH_GOVT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0150_CPAY_OTH_GOVT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0150_CPAY_OTHER: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0150_CPAY_OTHER',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0150_CPAY_PRIV_HMO: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0150_CPAY_PRIV_HMO',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0150_CPAY_PRIV_INS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0150_CPAY_PRIV_INS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0150_CPAY_SELFPAY: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0150_CPAY_SELFPAY',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0150_CPAY_TITLEPGMS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0150_CPAY_TITLEPGMS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0150_CPAY_UK: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0150_CPAY_UK',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0150_CPAY_WRKCOMP: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0150_CPAY_WRKCOMP',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0150_CPAY_MCAID_HMO: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0150_CPAY_MCAID_HMO',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0150_CPAY_MCARE_FFS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0150_CPAY_MCARE_FFS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1000_DC_IPPS_14_DA: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1000_DC_IPPS_14_DA',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1000_DC_IRF_14_DA: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1000_DC_IRF_14_DA',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1000_DC_LTC_14_DA: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1000_DC_LTC_14_DA',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0150_CPAY_MCARE_HMO: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0150_CPAY_MCARE_HMO',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M0150_CPAY_NONE: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M0150_CPAY_NONE',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1000_DC_LTCH_14_DA: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1000_DC_LTCH_14_DA',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1000_DC_NONE_14_DA: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1000_DC_NONE_14_DA',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1000_DC_OTH_14_DA: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1000_DC_OTH_14_DA',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1000_DC_PSYCH_14_DA: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1000_DC_PSYCH_14_DA',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1000_DC_SNF_14_DA: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1000_DC_SNF_14_DA',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1018_PRIOR_CATH: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1018_PRIOR_CATH',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1018_PRIOR_DISRUPTIVE: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1018_PRIOR_DISRUPTIVE',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1018_PRIOR_IMPR_DECSN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1018_PRIOR_IMPR_DECSN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1018_PRIOR_INTRACT_PAIN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1018_PRIOR_INTRACT_PAIN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1018_PRIOR_MEM_LOSS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1018_PRIOR_MEM_LOSS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1018_PRIOR_NOCHG_14D: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1018_PRIOR_NOCHG_14D',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1018_PRIOR_NONE: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1018_PRIOR_NONE',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1018_PRIOR_UNKNOWN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1018_PRIOR_UNKNOWN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1018_PRIOR_UR_INCON: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1018_PRIOR_UR_INCON',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1030_THH_ENT_NUTRITION: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1030_THH_ENT_NUTRITION',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1030_THH_IV_INFUSION: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1030_THH_IV_INFUSION',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1030_THH_NONE_ABOVE: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1030_THH_NONE_ABOVE',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1030_THH_PAR_NUTRITION: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1030_THH_PAR_NUTRITION',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1033_HOSP_RISK_5PLUS_MDCTN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1033_HOSP_RISK_5PLUS_MDCTN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1033_HOSP_RISK_COMPLIANCE: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1033_HOSP_RISK_COMPLIANCE',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1033_HOSP_RISK_CRNT_EXHSTN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1033_HOSP_RISK_CRNT_EXHSTN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1033_HOSP_RISK_HSTRY_FALLS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1033_HOSP_RISK_HSTRY_FALLS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1033_HOSP_RISK_MLTPL_ED_VISIT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1033_HOSP_RISK_MLTPL_ED_VISIT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1033_HOSP_RISK_MLTPL_HOSPZTN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1033_HOSP_RISK_MLTPL_HOSPZTN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1033_HOSP_RISK_MNTL_BHV_DCLN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1033_HOSP_RISK_MNTL_BHV_DCLN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1033_HOSP_RISK_NONE_ABOVE: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1033_HOSP_RISK_NONE_ABOVE',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1033_HOSP_RISK_OTHR_RISK: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1033_HOSP_RISK_OTHR_RISK',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1033_HOSP_RISK_WEIGHT_LOSS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1033_HOSP_RISK_WEIGHT_LOSS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1034_PTNT_OVRAL_STUS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1034_PTNT_OVRAL_STUS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1036_RSK_ALCOHOLISM: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1036_RSK_ALCOHOLISM',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1036_RSK_DRUGS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1036_RSK_DRUGS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1036_RSK_NONE: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1036_RSK_NONE',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1036_RSK_OBESITY: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1036_RSK_OBESITY',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1036_RSK_SMOKING: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1036_RSK_SMOKING',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1036_RSK_UNKNOWN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1036_RSK_UNKNOWN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1100_PTNT_LVG_STUTN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1100_PTNT_LVG_STUTN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1200_VISION: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1200_VISION',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1210_HEARG_ABLTY: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1210_HEARG_ABLTY',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1220_UNDRSTG_VERBAL_CNTNT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1220_UNDRSTG_VERBAL_CNTNT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1230_SPEECH: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1230_SPEECH',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1240_FRML_PAIN_ASMT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1240_FRML_PAIN_ASMT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1242_PAIN_FREQ_ACTVTY_MVMT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1242_PAIN_FREQ_ACTVTY_MVMT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1300_PRSR_ULCR_RISK_ASMT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1300_PRSR_ULCR_RISK_ASMT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1306_UNHLD_STG2_PRSR_ULCR: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1306_UNHLD_STG2_PRSR_ULCR',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1322_NBR_PRSULC_STG1: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1322_NBR_PRSULC_STG1',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1324_STG_PRBLM_ULCER: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1324_STG_PRBLM_ULCER',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1330_STAS_ULCR_PRSNT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1330_STAS_ULCR_PRSNT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1340_SRGCL_WND_PRSNT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1340_SRGCL_WND_PRSNT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1350_LESION_OPEN_WND: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1350_LESION_OPEN_WND',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1400_WHEN_DYSPNEIC: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1400_WHEN_DYSPNEIC',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1410_RESPTX_AIRPRESS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1410_RESPTX_AIRPRESS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1410_RESPTX_NONE: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1410_RESPTX_NONE',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1410_RESPTX_OXYGEN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1410_RESPTX_OXYGEN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1410_RESPTX_VENTILATOR: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1410_RESPTX_VENTILATOR',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1600_UTI: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1600_UTI',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1610_UR_INCONT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1610_UR_INCONT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1630_OSTOMY: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1630_OSTOMY',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1700_COG_FUNCTION: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1700_COG_FUNCTION',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1710_WHEN_CONFUSED: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1710_WHEN_CONFUSED',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1720_WHEN_ANXIOUS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1720_WHEN_ANXIOUS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1730_STDZ_DPRSN_SCRNG: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1730_STDZ_DPRSN_SCRNG',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1740_BD_PHYSICAL: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1740_BD_PHYSICAL',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1740_BD_SOC_INAPPRO: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1740_BD_SOC_INAPPRO',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1740_BD_VERBAL: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1740_BD_VERBAL',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1745_BEH_PROB_FREQ: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1745_BEH_PROB_FREQ',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1750_REC_PSYCH_NURS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1750_REC_PSYCH_NURS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1800_CRNT_GROOMING: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1800_CRNT_GROOMING',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1810_CRNT_DRESS_UPPER: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1810_CRNT_DRESS_UPPER',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1820_CRNT_DRESS_LOWER: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1820_CRNT_DRESS_LOWER',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1830_CRNT_BATHG: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1830_CRNT_BATHG',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1840_CRNT_TOILTG: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1840_CRNT_TOILTG',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1845_CRNT_TOILTG_HYGN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1845_CRNT_TOILTG_HYGN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1850_CRNT_TRNSFRNG: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1850_CRNT_TRNSFRNG',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1860_CRNT_AMBLTN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1860_CRNT_AMBLTN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1870_CRNT_FEEDING: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1870_CRNT_FEEDING',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1880_CRNT_PREP_LT_MEALS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1880_CRNT_PREP_LT_MEALS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1890_CRNT_PHONE_USE: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1890_CRNT_PHONE_USE',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1900_PRIOR_ADLIADL_AMBLTN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1900_PRIOR_ADLIADL_AMBLTN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1900_PRIOR_ADLIADL_HSEHOLD: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1900_PRIOR_ADLIADL_HSEHOLD',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1900_PRIOR_ADLIADL_SELF: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1900_PRIOR_ADLIADL_SELF',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1900_PRIOR_ADLIADL_TRNSFR: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1900_PRIOR_ADLIADL_TRNSFR',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1910_MLT_FCTR_FALL_RISK_ASMT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1910_MLT_FCTR_FALL_RISK_ASMT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2040_PRIOR_MGMT_INJCTN_MDCTN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2040_PRIOR_MGMT_INJCTN_MDCTN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2040_PRIOR_MGMT_ORAL_MDCTN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2040_PRIOR_MGMT_ORAL_MDCTN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2102_CARE_TYPE_SRC_ADL: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2102_CARE_TYPE_SRC_ADL',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2102_CARE_TYPE_SRC_ADVCY: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2102_CARE_TYPE_SRC_ADVCY',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2102_CARE_TYPE_SRC_EQUIP: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2102_CARE_TYPE_SRC_EQUIP',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2102_CARE_TYPE_SRC_IADL: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2102_CARE_TYPE_SRC_IADL',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2102_CARE_TYPE_SRC_MDCTN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2102_CARE_TYPE_SRC_MDCTN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2102_CARE_TYPE_SRC_PRCDR: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2102_CARE_TYPE_SRC_PRCDR',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2102_CARE_TYPE_SRC_SPRVSN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2102_CARE_TYPE_SRC_SPRVSN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2110_ADL_IADL_ASTNC_FREQ: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2110_ADL_IADL_ASTNC_FREQ',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2200_THER_NEED_NA: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2200_THER_NEED_NA',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1620_BWL_INCONT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1620_BWL_INCONT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1740_BD_DELUSIONS: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1740_BD_DELUSIONS',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1740_BD_IMP_DECISN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1740_BD_IMP_DECISN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1740_BD_MEM_DEFICIT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1740_BD_MEM_DEFICIT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1740_BD_NONE: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1740_BD_NONE',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2250_PLAN_SMRY_DBTS_FT_CARE: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2250_PLAN_SMRY_DBTS_FT_CARE',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2250_PLAN_SMRY_DPRSN_INTRVTN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2250_PLAN_SMRY_DPRSN_INTRVTN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2250_PLAN_SMRY_FALL_PRVNT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2250_PLAN_SMRY_FALL_PRVNT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2250_PLAN_SMRY_PAIN_INTRVTN: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2250_PLAN_SMRY_PAIN_INTRVTN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2250_PLAN_SMRY_PRSULC_PRVNT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2250_PLAN_SMRY_PRSULC_PRVNT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2250_PLAN_SMRY_PRSULC_TRTMT: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2250_PLAN_SMRY_PRSULC_TRTMT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M2250_PLAN_SMRY_PTNT_SPECF: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2250_PLAN_SMRY_PTNT_SPECF',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'Invalid Value: Only unsigned numeric values that are within the range of acceptable values and specified special characters are valid for Correction number.',
    Field1Name: 'CORRECTION_NUM',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'The value submitted in this item (Correction number) is invalid; it is not one of the numeric values or special characters allowed.',
    Field1Name: 'CORRECTION_NUM',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'If TRANS_TYPE_CD is equal to 1, then CORRECTION_NUM must equal 00.',
    Field1Name: 'TRANS_TYPE_CD',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '1'
  }, {
    ErrMessage: 'Patient age (M0066) at the time the assessment was completed cannot be greater than 140 years and must not be less than 18 years.',
    Field1Name: 'M0066_PAT_BIRTH_DT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '20170301'
  }, {
    ErrMessage: 'If M0102 date is equal to blank (^), then M0102-NA must equal 1.',
    Field1Name: 'M0102_PHYSN_ORDRD_SOCROC_DT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'If M0102 date is equal to blank (^), then M0104 must not equal blank (^).',
    Field1Name: 'M0102_PHYSN_ORDRD_SOCROC_DT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'If M0140 is active, THEN at least one item M0140-1 (American Indian or Alaska Native) through M0140-6 (White) must be checked.',
    Field1Name: 'M0140_ETHNIC_AI_AN',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'If M1620 is equal to 00, 01, 02, 03, 04, 05, or UK, then M1630 must equal 00.',
    Field1Name: 'M1620_BWL_INCONT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'If M0100 is equal to 01, then M0032-NA must equal 1.',
    Field1Name: 'M0100_ASSMT_REASON',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '01'
  }, {
    ErrMessage: 'M1017_CHGREG_ICD_NA: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1017_CHGREG_ICD_NA',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'The first character of M1021-a1 must be alphabetic, second character must be numeric 0 through 9, third character must be either alphabetic or numeric, fourth character must be a decimal point and characters 5 through 8 must be numeric, alphabetic or caret. If any character 5 through 8 is caret, then all subsequent characters must be caret.',
    Field1Name: 'M1021_PRIMARY_DIAG_ICD',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'Dates must be in chronological order as follows: - M0066 (birth date) < - M1005 (inpatient discharge date) < or = - M0102 (Date of Physician-ordered SOC/ROC) < or = - M0104 (Physician date of referral) < or = - M0030 (Start of Care Date) < = - M1307 (Oldest Non-epithelialized Stage II Pressure Ulcer Identified Date) < or = - M0903 (Date of Last Home Visit) < or = - M0906 (Discharge/Transfer/Death Date) < or = - M0090 (Date Assessment Completed) < or = the same as the current date. ',
    Field1Name: 'M0030_START_CARE_DT',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '20170621'
  }, {
    ErrMessage: 'M2001_DRUG_RGMN_RVW: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M2001_DRUG_RGMN_RVW',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'GG0170C_MOBILITY_SOCROC_PERF: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'GG0170C_MOBILITY_SOCROC_PERF',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'GG0170C_MOBILITY_DSCHG_GOAL: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'GG0170C_MOBILITY_DSCHG_GOAL',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1028_ACTV_DIAG_PVD_PAD: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1028_ACTV_DIAG_PVD_PAD',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'M1028_ACTV_DIAG_DM: The submitted value is not one of the options allowed for this item.',
    Field1Name: 'M1028_ACTV_DIAG_DM',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'Only the values listed in the \'Item Values\' table of the Detailed Data Specifications Report may be submitted for M1060_WEIGHT_B. The submitted value must be greater than or equal to the minimum value listed in the table and less than or equal to the maximum value listed in the table, or it must match one of the remaining special values (if any) that are listed in the table. The length of the submitted value must not exceed the allowed maximum length for the item. Signed numbers ,with a leading plus or minus sign, will not be accepted.',
    Field1Name: 'M1060_WEIGHT_B',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'Only the values listed in the \'Item Values\' table of the Detailed Data Specifications Report may be submitted for M1060_HEIGHT_A. The submitted value must be greater than or equal to the minimum value listed in the table and less than or equal to the maximum value listed in the table, or it must match one of the remaining special values (if any) that are listed in the table. The length of the submitted value must not exceed the allowed maximum length for the item. Signed numbers (with a leading plus or minus sign) will not be accepted.',
    Field1Name: 'M1060_HEIGHT_A',
    MetricTypes: ['CMS Required'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }, {
    ErrMessage: 'Payment source (M0150) is currently unanswered. It is a required item and must have at least one response checked.',
    Field1Name: 'M0150_CPAY_MCARE_FFS',
    MetricTypes: ['No-Metric'],
    M0010: '123456',
    M0020: '314',
    M0030: '20170621',
    M0090: '20170621',
    M0100: '01',
    Value1: '^'
  }]
})
